/*
 * helpers.c: Assorted routines
 *
 * (C) 2003 Ximian, Inc.
 */

#include <config.h>

#include "mini.h"
#include <ctype.h>
#include <mono/metadata/opcodes.h>

#ifndef HOST_WIN32
#include <unistd.h>
#endif

#ifndef DISABLE_JIT

#ifndef DISABLE_LOGGING

#ifdef MINI_OP
#undef MINI_OP
#endif
#ifdef MINI_OP3
#undef MINI_OP3
#endif

#ifdef HAVE_ARRAY_ELEM_INIT
#define MSGSTRFIELD(line) MSGSTRFIELD1(line)
#define MSGSTRFIELD1(line) str##line
static const struct msgstr_t {
#define MINI_OP(a,b,dest,src1,src2) char MSGSTRFIELD(__LINE__) [sizeof (b)];
#define MINI_OP3(a,b,dest,src1,src2,src3) char MSGSTRFIELD(__LINE__) [sizeof (b)];
// #include "mini-ops.h"
#undef MINI_OP
char str6 [sizeof ("load")];
char str7 [sizeof ("ldaddr")];
char str8 [sizeof ("store")];
char str9 [sizeof ("nop")];
char str10 [sizeof ("hard_nop")];
char str11 [sizeof ("relaxed_nop")];
char str12 [sizeof ("phi")];
char str13 [sizeof ("fphi")];
char str14 [sizeof ("vphi")];
char str15 [sizeof ("compare")];
char str16 [sizeof ("compare_imm")];
char str17 [sizeof ("fcompare")];
char str18 [sizeof ("lcompare")];
char str19 [sizeof ("icompare")];
char str20 [sizeof ("icompare_imm")];
char str21 [sizeof ("lcompare_imm")];
char str22 [sizeof ("local")];
char str23 [sizeof ("arg")];
char str25 [sizeof ("gsharedvt_local")];
char str26 [sizeof ("gsharedvt_arg_regoffset")];
char str31 [sizeof ("outarg_vt")];
char str32 [sizeof ("outarg_vtretaddr")];
char str33 [sizeof ("setret")];
char str34 [sizeof ("setfret")];
char str35 [sizeof ("setlret")];
char str36 [sizeof ("localloc")];
char str37 [sizeof ("localloc_imm")];
char str38 [sizeof ("checkthis")];
char str39 [sizeof ("seq_point")];
char str40 [sizeof ("implicit_exception")];
char str42 [sizeof ("voidcall")];
char str43 [sizeof ("voidcallvirt")];
char str44 [sizeof ("voidcall_reg")];
char str45 [sizeof ("voidcall_membase")];
char str46 [sizeof ("call")];
char str47 [sizeof ("call_reg")];
char str48 [sizeof ("call_membase")];
char str49 [sizeof ("callvirt")];
char str50 [sizeof ("fcall")];
char str51 [sizeof ("fcallvirt")];
char str52 [sizeof ("fcall_reg")];
char str53 [sizeof ("fcall_membase")];
char str54 [sizeof ("lcall")];
char str55 [sizeof ("lcallvirt")];
char str56 [sizeof ("lcall_reg")];
char str57 [sizeof ("lcall_membase")];
char str58 [sizeof ("vcall")];
char str59 [sizeof ("vcallvirt")];
char str60 [sizeof ("vcall_reg")];
char str61 [sizeof ("vcall_membase")];
char str63 [sizeof ("vcall2")];
char str64 [sizeof ("vcall2_reg")];
char str65 [sizeof ("vcall2_membase")];
char str66 [sizeof ("dyn_call")];
char str68 [sizeof ("iconst")];
char str69 [sizeof ("i8const")];
char str70 [sizeof ("r4const")];
char str71 [sizeof ("r8const")];
char str72 [sizeof ("regvar")];
char str73 [sizeof ("regoffset")];
char str74 [sizeof ("vtarg_addr")];
char str75 [sizeof ("label")];
char str76 [sizeof ("switch")];
char str77 [sizeof ("throw")];
char str78 [sizeof ("rethrow")];
char str95 [sizeof ("oparglist")];
char str98 [sizeof ("store_membase_reg")];
char str99 [sizeof ("storei1_membase_reg")];
char str100 [sizeof ("storei2_membase_reg")];
char str101 [sizeof ("storei4_membase_reg")];
char str102 [sizeof ("storei8_membase_reg")];
char str103 [sizeof ("storer4_membase_reg")];
char str104 [sizeof ("storer8_membase_reg")];
char str107 [sizeof ("storex_membase_reg")];
char str108 [sizeof ("storex_aligned_membase_reg")];
char str109 [sizeof ("storex_nta_membase_reg")];
char str112 [sizeof ("store_membase_imm")];
char str113 [sizeof ("storei1_membase_imm")];
char str114 [sizeof ("storei2_membase_imm")];
char str115 [sizeof ("storei4_membase_imm")];
char str116 [sizeof ("storei8_membase_imm")];
char str117 [sizeof ("storex_membase")];
char str118 [sizeof ("storev_membase")];
char str121 [sizeof ("load_membase")];
char str122 [sizeof ("loadi1_membase")];
char str123 [sizeof ("loadu1_membase")];
char str124 [sizeof ("loadi2_membase")];
char str125 [sizeof ("loadu2_membase")];
char str126 [sizeof ("loadi4_membase")];
char str127 [sizeof ("loadu4_membase")];
char str128 [sizeof ("loadi8_membase")];
char str129 [sizeof ("loadr4_membase")];
char str130 [sizeof ("loadr8_membase")];
char str132 [sizeof ("loadx_membase")];
char str135 [sizeof ("loadx_aligned_membase")];
char str138 [sizeof ("loadv_membase")];
char str141 [sizeof ("load_memindex")];
char str142 [sizeof ("loadi1_memindex")];
char str143 [sizeof ("loadu1_memindex")];
char str144 [sizeof ("loadi2_memindex")];
char str145 [sizeof ("loadu2_memindex")];
char str146 [sizeof ("loadi4_memindex")];
char str147 [sizeof ("loadu4_memindex")];
char str148 [sizeof ("loadi8_memindex")];
char str149 [sizeof ("loadr4_memindex")];
char str150 [sizeof ("loadr8_memindex")];
char str153 [sizeof ("store_memindex")];
char str154 [sizeof ("storei1_memindex")];
char str155 [sizeof ("storei2_memindex")];
char str156 [sizeof ("storei4_memindex")];
char str157 [sizeof ("storei8_memindex")];
char str158 [sizeof ("storer4_memindex")];
char str159 [sizeof ("storer8_memindex")];
char str161 [sizeof ("load_mem")];
char str162 [sizeof ("loadu1_mem")];
char str163 [sizeof ("loadu2_mem")];
char str164 [sizeof ("loadi4_mem")];
char str165 [sizeof ("loadu4_mem")];
char str166 [sizeof ("loadi8_mem")];
char str167 [sizeof ("store_mem_imm")];
char str169 [sizeof ("move")];
char str170 [sizeof ("lmove")];
char str171 [sizeof ("fmove")];
char str172 [sizeof ("vmove")];
char str174 [sizeof ("vzero")];
char str176 [sizeof ("add_imm")];
char str177 [sizeof ("sub_imm")];
char str178 [sizeof ("mul_imm")];
char str179 [sizeof ("div_imm")];
char str180 [sizeof ("div_un_imm")];
char str181 [sizeof ("rem_imm")];
char str182 [sizeof ("rem_un_imm")];
char str183 [sizeof ("and_imm")];
char str184 [sizeof ("or_imm")];
char str185 [sizeof ("xor_imm")];
char str186 [sizeof ("shl_imm")];
char str187 [sizeof ("shr_imm")];
char str188 [sizeof ("shr_un_imm")];
char str190 [sizeof ("br")];
char str191 [sizeof ("jmp")];
char str193 [sizeof ("tailcall")];
char str194 [sizeof ("break")];
char str196 [sizeof ("ceq")];
char str197 [sizeof ("cgt")];
char str198 [sizeof ("cgt.un")];
char str199 [sizeof ("clt")];
char str200 [sizeof ("clt.un")];
char str203 [sizeof ("cond_exc_eq")];
char str204 [sizeof ("cond_exc_ge")];
char str205 [sizeof ("cond_exc_gt")];
char str206 [sizeof ("cond_exc_le")];
char str207 [sizeof ("cond_exc_lt")];
char str208 [sizeof ("cond_exc_ne_un")];
char str209 [sizeof ("cond_exc_ge_un")];
char str210 [sizeof ("cond_exc_gt_un")];
char str211 [sizeof ("cond_exc_le_un")];
char str212 [sizeof ("cond_exc_lt_un")];
char str214 [sizeof ("cond_exc_ov")];
char str215 [sizeof ("cond_exc_no")];
char str216 [sizeof ("cond_exc_c")];
char str217 [sizeof ("cond_exc_nc")];
char str219 [sizeof ("cond_exc_ieq")];
char str220 [sizeof ("cond_exc_ige")];
char str221 [sizeof ("cond_exc_igt")];
char str222 [sizeof ("cond_exc_ile")];
char str223 [sizeof ("cond_exc_ilt")];
char str224 [sizeof ("cond_exc_ine_un")];
char str225 [sizeof ("cond_exc_ige_un")];
char str226 [sizeof ("cond_exc_igt_un")];
char str227 [sizeof ("cond_exc_ile_un")];
char str228 [sizeof ("cond_exc_ilt_un")];
char str230 [sizeof ("cond_exc_iov")];
char str231 [sizeof ("cond_exc_ino")];
char str232 [sizeof ("cond_exc_ic")];
char str233 [sizeof ("cond_exc_inc")];
char str236 [sizeof ("long_add")];
char str237 [sizeof ("long_sub")];
char str238 [sizeof ("long_mul")];
char str239 [sizeof ("long_div")];
char str240 [sizeof ("long_div_un")];
char str241 [sizeof ("long_rem")];
char str242 [sizeof ("long_rem_un")];
char str243 [sizeof ("long_and")];
char str244 [sizeof ("long_or")];
char str245 [sizeof ("long_xor")];
char str246 [sizeof ("long_shl")];
char str247 [sizeof ("long_shr")];
char str248 [sizeof ("long_shr_un")];
char str251 [sizeof ("long_neg")];
char str252 [sizeof ("long_not")];
char str253 [sizeof ("long_conv_to_i1")];
char str254 [sizeof ("long_conv_to_i2")];
char str255 [sizeof ("long_conv_to_i4")];
char str256 [sizeof ("long_conv_to_i8")];
char str257 [sizeof ("long_conv_to_r4")];
char str258 [sizeof ("long_conv_to_r8")];
char str259 [sizeof ("long_conv_to_u4")];
char str260 [sizeof ("long_conv_to_u8")];
char str262 [sizeof ("long_conv_to_u2")];
char str263 [sizeof ("long_conv_to_u1")];
char str264 [sizeof ("long_conv_to_i")];
char str265 [sizeof ("long_conv_to_ovf_i")];
char str266 [sizeof ("long_conv_to_ovf_u")];
char str268 [sizeof ("long_add_ovf")];
char str269 [sizeof ("long_add_ovf_un")];
char str270 [sizeof ("long_mul_ovf")];
char str271 [sizeof ("long_mul_ovf_un")];
char str272 [sizeof ("long_sub_ovf")];
char str273 [sizeof ("long_sub_ovf_un")];
char str275 [sizeof ("long_conv_to_ovf_i1_un")];
char str276 [sizeof ("long_conv_to_ovf_i2_un")];
char str277 [sizeof ("long_conv_to_ovf_i4_un")];
char str278 [sizeof ("long_conv_to_ovf_i8_un")];
char str279 [sizeof ("long_conv_to_ovf_u1_un")];
char str280 [sizeof ("long_conv_to_ovf_u2_un")];
char str281 [sizeof ("long_conv_to_ovf_u4_un")];
char str282 [sizeof ("long_conv_to_ovf_u8_un")];
char str283 [sizeof ("long_conv_to_ovf_i_un")];
char str284 [sizeof ("long_conv_to_ovf_u_un")];
char str286 [sizeof ("long_conv_to_ovf_i1")];
char str287 [sizeof ("long_conv_to_ovf_u1")];
char str288 [sizeof ("long_conv_to_ovf_i2")];
char str289 [sizeof ("long_conv_to_ovf_u2")];
char str290 [sizeof ("long_conv_to_ovf_i4")];
char str291 [sizeof ("long_conv_to_ovf_u4")];
char str292 [sizeof ("long_conv_to_ovf_i8")];
char str293 [sizeof ("long_conv_to_ovf_u8")];
char str296 [sizeof ("long_ceq")];
char str297 [sizeof ("long_cgt")];
char str298 [sizeof ("long_cgt_un")];
char str299 [sizeof ("long_clt")];
char str300 [sizeof ("long_clt_un")];
char str302 [sizeof ("long_conv_to_r_un")];
char str303 [sizeof ("long_conv_to_u")];
char str305 [sizeof ("long_add_imm")];
char str306 [sizeof ("long_sub_imm")];
char str307 [sizeof ("long_mul_imm")];
char str308 [sizeof ("long_and_imm")];
char str309 [sizeof ("long_or_imm")];
char str310 [sizeof ("long_xor_imm")];
char str311 [sizeof ("long_shl_imm")];
char str312 [sizeof ("long_shr_imm")];
char str313 [sizeof ("long_shr_un_imm")];
char str314 [sizeof ("long_div_imm")];
char str315 [sizeof ("long_div_un_imm")];
char str316 [sizeof ("long_rem_imm")];
char str317 [sizeof ("long_rem_un_imm")];
char str320 [sizeof ("long_beq")];
char str321 [sizeof ("long_bge")];
char str322 [sizeof ("long_bgt")];
char str323 [sizeof ("long_ble")];
char str324 [sizeof ("long_blt")];
char str325 [sizeof ("long_bne_un")];
char str326 [sizeof ("long_bge_un")];
char str327 [sizeof ("long_bgt_un")];
char str328 [sizeof ("long_ble_un")];
char str329 [sizeof ("long_blt_un")];
char str332 [sizeof ("long_conv_to_r8_2")];
char str333 [sizeof ("long_conv_to_r4_2")];
char str334 [sizeof ("long_conv_to_r_un_2")];
char str335 [sizeof ("long_conv_to_ovf_i4_2")];
char str338 [sizeof ("int_add")];
char str339 [sizeof ("int_sub")];
char str340 [sizeof ("int_mul")];
char str341 [sizeof ("int_div")];
char str342 [sizeof ("int_div_un")];
char str343 [sizeof ("int_rem")];
char str344 [sizeof ("int_rem_un")];
char str345 [sizeof ("int_and")];
char str346 [sizeof ("int_or")];
char str347 [sizeof ("int_xor")];
char str348 [sizeof ("int_shl")];
char str349 [sizeof ("int_shr")];
char str350 [sizeof ("int_shr_un")];
char str353 [sizeof ("int_neg")];
char str354 [sizeof ("int_not")];
char str355 [sizeof ("int_conv_to_i1")];
char str356 [sizeof ("int_conv_to_i2")];
char str357 [sizeof ("int_conv_to_i4")];
char str358 [sizeof ("int_conv_to_i8")];
char str359 [sizeof ("int_conv_to_r4")];
char str360 [sizeof ("int_conv_to_r8")];
char str361 [sizeof ("int_conv_to_u4")];
char str362 [sizeof ("int_conv_to_u8")];
char str364 [sizeof ("int_conv_to_r_un")];
char str365 [sizeof ("int_conv_to_u")];
char str368 [sizeof ("int_conv_to_u2")];
char str369 [sizeof ("int_conv_to_u1")];
char str370 [sizeof ("int_conv_to_i")];
char str371 [sizeof ("int_conv_to_ovf_i")];
char str372 [sizeof ("int_conv_to_ovf_u")];
char str373 [sizeof ("int_add_ovf")];
char str374 [sizeof ("int_add_ovf_un")];
char str375 [sizeof ("int_mul_ovf")];
char str376 [sizeof ("int_mul_ovf_un")];
char str377 [sizeof ("int_sub_ovf")];
char str378 [sizeof ("int_sub_ovf_un")];
char str381 [sizeof ("int_conv_to_ovf_i1_un")];
char str382 [sizeof ("int_conv_to_ovf_i2_un")];
char str383 [sizeof ("int_conv_to_ovf_i4_un")];
char str384 [sizeof ("int_conv_to_ovf_i8_un")];
char str385 [sizeof ("int_conv_to_ovf_u1_un")];
char str386 [sizeof ("int_conv_to_ovf_u2_un")];
char str387 [sizeof ("int_conv_to_ovf_u4_un")];
char str388 [sizeof ("int_conv_to_ovf_u8_un")];
char str389 [sizeof ("int_conv_to_ovf_i_un")];
char str390 [sizeof ("int_conv_to_ovf_u_un")];
char str393 [sizeof ("int_conv_to_ovf_i1")];
char str394 [sizeof ("int_conv_to_ovf_u1")];
char str395 [sizeof ("int_conv_to_ovf_i2")];
char str396 [sizeof ("int_conv_to_ovf_u2")];
char str397 [sizeof ("int_conv_to_ovf_i4")];
char str398 [sizeof ("int_conv_to_ovf_u4")];
char str399 [sizeof ("int_conv_to_ovf_i8")];
char str400 [sizeof ("int_conv_to_ovf_u8")];
char str402 [sizeof ("int_adc")];
char str403 [sizeof ("int_adc_imm")];
char str404 [sizeof ("int_sbb")];
char str405 [sizeof ("int_sbb_imm")];
char str406 [sizeof ("int_addcc")];
char str407 [sizeof ("int_subcc")];
char str409 [sizeof ("int_add_imm")];
char str410 [sizeof ("int_sub_imm")];
char str411 [sizeof ("int_mul_imm")];
char str412 [sizeof ("int_div_imm")];
char str413 [sizeof ("int_div_un_imm")];
char str414 [sizeof ("int_rem_imm")];
char str415 [sizeof ("int_rem_un_imm")];
char str416 [sizeof ("int_and_imm")];
char str417 [sizeof ("int_or_imm")];
char str418 [sizeof ("int_xor_imm")];
char str419 [sizeof ("int_shl_imm")];
char str420 [sizeof ("int_shr_imm")];
char str421 [sizeof ("int_shr_un_imm")];
char str423 [sizeof ("int_ceq")];
char str424 [sizeof ("int_cgt")];
char str425 [sizeof ("int_cgt_un")];
char str426 [sizeof ("int_clt")];
char str427 [sizeof ("int_clt_un")];
char str429 [sizeof ("int_beq")];
char str430 [sizeof ("int_bge")];
char str431 [sizeof ("int_bgt")];
char str432 [sizeof ("int_ble")];
char str433 [sizeof ("int_blt")];
char str434 [sizeof ("int_bne_un")];
char str435 [sizeof ("int_bge_un")];
char str436 [sizeof ("int_bgt_un")];
char str437 [sizeof ("int_ble_un")];
char str438 [sizeof ("int_blt_un")];
char str440 [sizeof ("float_beq")];
char str441 [sizeof ("float_bge")];
char str442 [sizeof ("float_bgt")];
char str443 [sizeof ("float_ble")];
char str444 [sizeof ("float_blt")];
char str445 [sizeof ("float_bne_un")];
char str446 [sizeof ("float_bge_un")];
char str447 [sizeof ("float_bgt_un")];
char str448 [sizeof ("float_ble_un")];
char str449 [sizeof ("float_blt_un")];
char str452 [sizeof ("float_add")];
char str453 [sizeof ("float_sub")];
char str454 [sizeof ("float_mul")];
char str455 [sizeof ("float_div")];
char str456 [sizeof ("float_div_un")];
char str457 [sizeof ("float_rem")];
char str458 [sizeof ("float_rem_un")];
char str461 [sizeof ("float_neg")];
char str462 [sizeof ("float_not")];
char str463 [sizeof ("float_conv_to_i1")];
char str464 [sizeof ("float_conv_to_i2")];
char str465 [sizeof ("float_conv_to_i4")];
char str466 [sizeof ("float_conv_to_i8")];
char str467 [sizeof ("float_conv_to_r4")];
char str468 [sizeof ("float_conv_to_r8")];
char str469 [sizeof ("float_conv_to_u4")];
char str470 [sizeof ("float_conv_to_u8")];
char str472 [sizeof ("float_conv_to_u2")];
char str473 [sizeof ("float_conv_to_u1")];
char str474 [sizeof ("float_conv_to_i")];
char str475 [sizeof ("float_conv_to_ovf_i")];
char str476 [sizeof ("float_conv_to_ovd_u")];
char str478 [sizeof ("float_add_ovf")];
char str479 [sizeof ("float_add_ovf_un")];
char str480 [sizeof ("float_mul_ovf")];
char str481 [sizeof ("float_mul_ovf_un")];
char str482 [sizeof ("float_sub_ovf")];
char str483 [sizeof ("float_sub_ovf_un")];
char str485 [sizeof ("float_conv_to_ovf_i1_un")];
char str486 [sizeof ("float_conv_to_ovf_i2_un")];
char str487 [sizeof ("float_conv_to_ovf_i4_un")];
char str488 [sizeof ("float_conv_to_ovf_i8_un")];
char str489 [sizeof ("float_conv_to_ovf_u1_un")];
char str490 [sizeof ("float_conv_to_ovf_u2_un")];
char str491 [sizeof ("float_conv_to_ovf_u4_un")];
char str492 [sizeof ("float_conv_to_ovf_u8_un")];
char str493 [sizeof ("float_conv_to_ovf_i_un")];
char str494 [sizeof ("float_conv_to_ovf_u_un")];
char str496 [sizeof ("float_conv_to_ovf_i1")];
char str497 [sizeof ("float_conv_to_ovf_u1")];
char str498 [sizeof ("float_conv_to_ovf_i2")];
char str499 [sizeof ("float_conv_to_ovf_u2")];
char str500 [sizeof ("float_conv_to_ovf_i4")];
char str501 [sizeof ("float_conv_to_ovf_u4")];
char str502 [sizeof ("float_conv_to_ovf_i8")];
char str503 [sizeof ("float_conv_to_ovf_u8")];
char str506 [sizeof ("float_ceq")];
char str507 [sizeof ("float_cgt")];
char str508 [sizeof ("float_cgt_un")];
char str509 [sizeof ("float_clt")];
char str510 [sizeof ("float_clt_un")];
char str512 [sizeof ("float_ceq_membase")];
char str513 [sizeof ("float_cgt_membase")];
char str514 [sizeof ("float_cgt_un_membase")];
char str515 [sizeof ("float_clt_membase")];
char str516 [sizeof ("float_clt_un_membase")];
char str518 [sizeof ("float_conv_to_u")];
char str519 [sizeof ("ckfinite")];
char str522 [sizeof ("float_getlow32")];
char str524 [sizeof ("float_gethigh32")];
char str526 [sizeof ("jump_table")];
char str529 [sizeof ("aot_const")];
char str530 [sizeof ("patch_info")];
char str531 [sizeof ("got_entry")];
char str534 [sizeof ("call_handler")];
char str535 [sizeof ("start_handler")];
char str536 [sizeof ("endfilter")];
char str537 [sizeof ("endfinally")];
char str540 [sizeof ("bigmul")];
char str541 [sizeof ("bigmul_un")];
char str542 [sizeof ("int_min_un")];
char str543 [sizeof ("int_max_un")];
char str544 [sizeof ("long_min_un")];
char str545 [sizeof ("long_max_un")];
char str547 [sizeof ("min")];
char str548 [sizeof ("max")];
char str550 [sizeof ("int_min")];
char str551 [sizeof ("int_max")];
char str552 [sizeof ("long_min")];
char str553 [sizeof ("long_max")];
char str556 [sizeof ("adc")];
char str557 [sizeof ("adc_imm")];
char str558 [sizeof ("sbb")];
char str559 [sizeof ("sbb_imm")];
char str560 [sizeof ("addcc")];
char str561 [sizeof ("addcc_imm")];
char str562 [sizeof ("subcc")];
char str563 [sizeof ("subcc_imm")];
char str564 [sizeof ("br_reg")];
char str565 [sizeof ("sext_i1")];
char str566 [sizeof ("sext_i2")];
char str567 [sizeof ("sext_i4")];
char str568 [sizeof ("zext_i1")];
char str569 [sizeof ("zext_i2")];
char str570 [sizeof ("zext_i4")];
char str571 [sizeof ("cne")];
char str572 [sizeof ("trunc_i4")];
char str574 [sizeof ("add_ovf_carry")];
char str575 [sizeof ("sub_ovf_carry")];
char str576 [sizeof ("add_ovf_un_carry")];
char str577 [sizeof ("sub_ovf_un_carry")];
char str580 [sizeof ("laddcc")];
char str581 [sizeof ("lsubcc")];
char str585 [sizeof ("sin")];
char str586 [sizeof ("cos")];
char str587 [sizeof ("abs")];
char str588 [sizeof ("tan")];
char str589 [sizeof ("atan")];
char str590 [sizeof ("sqrt")];
char str591 [sizeof ("round")];
char str593 [sizeof ("strlen")];
char str594 [sizeof ("newarr")];
char str595 [sizeof ("ldlen")];
char str596 [sizeof ("bounds_check")];
char str598 [sizeof ("getldelema2")];
char str600 [sizeof ("memcpy")];
char str602 [sizeof ("memset")];
char str603 [sizeof ("save_lmf")];
char str604 [sizeof ("restore_lmf")];
char str607 [sizeof ("card_table_wbarrier")];
char str610 [sizeof ("tls_get")];
char str611 [sizeof ("tls_get_reg")];
char str613 [sizeof ("load_gotaddr")];
char str614 [sizeof ("dummy_use")];
char str615 [sizeof ("dummy_store")];
char str616 [sizeof ("not_reached")];
char str617 [sizeof ("not_null")];
char str623 [sizeof ("addps")];
char str624 [sizeof ("divps")];
char str625 [sizeof ("mulps")];
char str626 [sizeof ("subps")];
char str627 [sizeof ("maxps")];
char str628 [sizeof ("minps")];
char str629 [sizeof ("compps")];
char str630 [sizeof ("andps")];
char str631 [sizeof ("andnps")];
char str632 [sizeof ("orps")];
char str633 [sizeof ("xorps")];
char str634 [sizeof ("haddps")];
char str635 [sizeof ("hsubps")];
char str636 [sizeof ("addsubps")];
char str637 [sizeof ("dupps_low")];
char str638 [sizeof ("dupps_high")];
char str640 [sizeof ("rsqrtps")];
char str641 [sizeof ("sqrtps")];
char str642 [sizeof ("rcpps")];
char str644 [sizeof ("pshufflew_high")];
char str645 [sizeof ("pshufflew_low")];
char str646 [sizeof ("pshuffled")];
char str647 [sizeof ("shufps")];
char str648 [sizeof ("shufpd")];
char str650 [sizeof ("addpd")];
char str651 [sizeof ("divpd")];
char str652 [sizeof ("mulpd")];
char str653 [sizeof ("subpd")];
char str654 [sizeof ("maxpd")];
char str655 [sizeof ("minpd")];
char str656 [sizeof ("comppd")];
char str657 [sizeof ("andpd")];
char str658 [sizeof ("andnpd")];
char str659 [sizeof ("orpd")];
char str660 [sizeof ("xorpd")];
char str661 [sizeof ("haddpd")];
char str662 [sizeof ("hsubpd")];
char str663 [sizeof ("addsubpd")];
char str664 [sizeof ("duppd")];
char str666 [sizeof ("sqrtpd")];
char str668 [sizeof ("extract_mask")];
char str670 [sizeof ("pand")];
char str671 [sizeof ("por")];
char str672 [sizeof ("pxor")];
char str674 [sizeof ("paddb")];
char str675 [sizeof ("paddw")];
char str676 [sizeof ("paddd")];
char str677 [sizeof ("paddq")];
char str679 [sizeof ("psubb")];
char str680 [sizeof ("psubw")];
char str681 [sizeof ("psubd")];
char str682 [sizeof ("psubq")];
char str684 [sizeof ("pmaxb_un")];
char str685 [sizeof ("pmaxw_un")];
char str686 [sizeof ("pmaxd_un")];
char str688 [sizeof ("pmaxb")];
char str689 [sizeof ("pmaxw")];
char str690 [sizeof ("pmaxd")];
char str692 [sizeof ("pavgb_un")];
char str693 [sizeof ("pavgw_un")];
char str695 [sizeof ("pminb_un")];
char str696 [sizeof ("pminw_un")];
char str697 [sizeof ("pmind_un")];
char str699 [sizeof ("pminb")];
char str700 [sizeof ("pminw")];
char str701 [sizeof ("pmind")];
char str703 [sizeof ("pcmpeqb")];
char str704 [sizeof ("pcmpeqw")];
char str705 [sizeof ("pcmpeqd")];
char str706 [sizeof ("pcmpeqq")];
char str708 [sizeof ("pcmpgtb")];
char str709 [sizeof ("pcmpgtw")];
char str710 [sizeof ("pcmpgtd")];
char str711 [sizeof ("pcmpgtq")];
char str713 [sizeof ("psumabsdiff")];
char str715 [sizeof ("unpack_lowb")];
char str716 [sizeof ("unpack_loww")];
char str717 [sizeof ("unpack_lowd")];
char str718 [sizeof ("unpack_lowq")];
char str719 [sizeof ("unpack_lowps")];
char str720 [sizeof ("unpack_lowpd")];
char str722 [sizeof ("unpack_highb")];
char str723 [sizeof ("unpack_highw")];
char str724 [sizeof ("unpack_highd")];
char str725 [sizeof ("unpack_highq")];
char str726 [sizeof ("unpack_highps")];
char str727 [sizeof ("unpack_highpd")];
char str729 [sizeof ("packw")];
char str730 [sizeof ("packd")];
char str732 [sizeof ("packw_un")];
char str733 [sizeof ("packd_un")];
char str735 [sizeof ("paddb_sat")];
char str736 [sizeof ("paddb_sat_un")];
char str738 [sizeof ("paddw_sat")];
char str739 [sizeof ("paddw_sat_un")];
char str741 [sizeof ("psubb_sat")];
char str742 [sizeof ("psubb_sat_un")];
char str744 [sizeof ("psubw_sat")];
char str745 [sizeof ("psubw_sat_un")];
char str747 [sizeof ("pmulw")];
char str748 [sizeof ("pmuld")];
char str749 [sizeof ("pmulq")];
char str751 [sizeof ("pmul_high_un")];
char str752 [sizeof ("pmul_high")];
char str755 [sizeof ("pshrw")];
char str756 [sizeof ("pshrw_reg")];
char str758 [sizeof ("psarw")];
char str759 [sizeof ("psarw_reg")];
char str761 [sizeof ("pshlw")];
char str762 [sizeof ("pshlw_reg")];
char str764 [sizeof ("pshrd")];
char str765 [sizeof ("pshrd_reg")];
char str767 [sizeof ("pshrq")];
char str768 [sizeof ("pshrq_reg")];
char str770 [sizeof ("psard")];
char str771 [sizeof ("psard_reg")];
char str773 [sizeof ("pshld")];
char str774 [sizeof ("pshld_reg")];
char str776 [sizeof ("pshlq")];
char str777 [sizeof ("pshlq_reg")];
char str779 [sizeof ("extract_i4")];
char str780 [sizeof ("iconv_to_r8_raw")];
char str782 [sizeof ("extract_i2")];
char str783 [sizeof ("extract_u2")];
char str784 [sizeof ("extract_i1")];
char str785 [sizeof ("extract_u1")];
char str786 [sizeof ("extract_r8")];
char str787 [sizeof ("extract_i8")];
char str790 [sizeof ("insert_i1")];
char str791 [sizeof ("insert_i4")];
char str792 [sizeof ("insert_i8")];
char str793 [sizeof ("insert_r4")];
char str794 [sizeof ("insert_r8")];
char str796 [sizeof ("insert_i2")];
char str798 [sizeof ("extractx_u2")];
char str802 [sizeof ("insertx_u1_slow")];
char str804 [sizeof ("insertx_i4_slow")];
char str806 [sizeof ("insertx_r4_slow")];
char str807 [sizeof ("insertx_r8_slow")];
char str808 [sizeof ("insertx_i8_slow")];
char str810 [sizeof ("fconv_to_r8_x")];
char str811 [sizeof ("xconv_r8_to_i4")];
char str812 [sizeof ("iconv_to_x")];
char str814 [sizeof ("expand_i1")];
char str815 [sizeof ("expand_i2")];
char str816 [sizeof ("expand_i4")];
char str817 [sizeof ("expand_r4")];
char str818 [sizeof ("expand_i8")];
char str819 [sizeof ("expand_r8")];
char str821 [sizeof ("prefetch_membase")];
char str823 [sizeof ("cvtdq2pd")];
char str824 [sizeof ("cvtdq2ps")];
char str825 [sizeof ("cvtpd2dq")];
char str826 [sizeof ("cvtpd2ps")];
char str827 [sizeof ("cvtps2dq")];
char str828 [sizeof ("cvtps2pd")];
char str829 [sizeof ("cvttpd2dq")];
char str830 [sizeof ("cvttps2dq")];
char str834 [sizeof ("xmove")];
char str835 [sizeof ("xzero")];
char str836 [sizeof ("xphi")];
char str849 [sizeof ("atomic_add_i4")];
char str850 [sizeof ("atomic_add_new_i4")];
char str851 [sizeof ("atomic_add_imm_i4")];
char str852 [sizeof ("atomic_add_imm_new_i4")];
char str853 [sizeof ("atomic_exchange_i4")];
char str855 [sizeof ("atomic_add_i8")];
char str856 [sizeof ("atomic_add_new_i8")];
char str857 [sizeof ("atomic_add_imm_i8")];
char str858 [sizeof ("atomic_add_imm_new_i8")];
char str859 [sizeof ("atomic_exchange_i8")];
char str860 [sizeof ("memory_barrier")];
char str862 [sizeof ("atomic_cas_i4")];
char str863 [sizeof ("atomic_cas_i8")];
char str874 [sizeof ("cmov_ieq")];
char str875 [sizeof ("cmov_ige")];
char str876 [sizeof ("cmov_igt")];
char str877 [sizeof ("cmov_ile")];
char str878 [sizeof ("cmov_ilt")];
char str879 [sizeof ("cmov_ine_un")];
char str880 [sizeof ("cmov_ige_un")];
char str881 [sizeof ("cmov_igt_un")];
char str882 [sizeof ("cmov_ile_un")];
char str883 [sizeof ("cmov_ilt_un")];
char str885 [sizeof ("cmov_leq")];
char str886 [sizeof ("cmov_lge")];
char str887 [sizeof ("cmov_lgt")];
char str888 [sizeof ("cmov_lle")];
char str889 [sizeof ("cmov_llt")];
char str890 [sizeof ("cmov_lne_un")];
char str891 [sizeof ("cmov_lge_un")];
char str892 [sizeof ("cmov_lgt_un")];
char str893 [sizeof ("cmov_lle_un")];
char str894 [sizeof ("cmov_llt_un")];
char str901 [sizeof ("liverange_start")];
char str906 [sizeof ("liverange_end")];
char str913 [sizeof ("gc_liveness_def")];
char str914 [sizeof ("gc_liveness_use")];
char str920 [sizeof ("gc_spill_slot_liveness_def")];
char str926 [sizeof ("gc_param_slot_liveness_def")];
char str933 [sizeof ("nacl_gc_safe_point")];
char str937 [sizeof ("x86_test_null")];
char str938 [sizeof ("x86_compare_membase_reg")];
char str939 [sizeof ("x86_compare_membase_imm")];
char str940 [sizeof ("x86_compare_mem_imm")];
char str941 [sizeof ("x86_compare_membase8_imm")];
char str942 [sizeof ("x86_compare_reg_membase")];
char str943 [sizeof ("x86_inc_reg")];
char str944 [sizeof ("x86_inc_membase")];
char str945 [sizeof ("x86_dec_reg")];
char str946 [sizeof ("x86_dec_membase")];
char str947 [sizeof ("x86_add_membase_imm")];
char str948 [sizeof ("x86_sub_membase_imm")];
char str949 [sizeof ("x86_and_membase_imm")];
char str950 [sizeof ("x86_or_membase_imm")];
char str951 [sizeof ("x86_xor_membase_imm")];
char str952 [sizeof ("x86_add_membase_reg")];
char str953 [sizeof ("x86_sub_membase_reg")];
char str954 [sizeof ("x86_and_membase_reg")];
char str955 [sizeof ("x86_or_membase_reg")];
char str956 [sizeof ("x86_xor_membase_reg")];
char str957 [sizeof ("x86_mul_membase_reg")];
char str959 [sizeof ("x86_add_reg_membase")];
char str960 [sizeof ("x86_sub_reg_membase")];
char str961 [sizeof ("x86_mul_reg_membase")];
char str962 [sizeof ("x86_and_reg_membase")];
char str963 [sizeof ("x86_or_reg_membase")];
char str964 [sizeof ("x86_xor_reg_membase")];
char str966 [sizeof ("x86_push_membase")];
char str967 [sizeof ("x86_push_imm")];
char str968 [sizeof ("x86_push")];
char str969 [sizeof ("x86_push_obj")];
char str970 [sizeof ("x86_push_got_entry")];
char str971 [sizeof ("x86_lea")];
char str972 [sizeof ("x86_lea_membase")];
char str973 [sizeof ("x86_xchg")];
char str974 [sizeof ("x86_fpop")];
char str975 [sizeof ("x86_fp_load_i8")];
char str976 [sizeof ("x86_fp_load_i4")];
char str977 [sizeof ("x86_seteq_membase")];
char str978 [sizeof ("x86_setne_membase")];
char str979 [sizeof ("x86_fxch")];
char str983 [sizeof ("amd64_test_null")];
char str984 [sizeof ("amd64_set_xmmreg_r4")];
char str985 [sizeof ("amd64_set_xmmreg_r8")];
char str986 [sizeof ("amd64_icompare_membase_reg")];
char str987 [sizeof ("amd64_icompare_membase_imm")];
char str988 [sizeof ("amd64_icompare_reg_membase")];
char str989 [sizeof ("amd64_compare_membase_reg")];
char str990 [sizeof ("amd64_compare_membase_imm")];
char str991 [sizeof ("amd64_compare_reg_membase")];
char str993 [sizeof ("amd64_add_membase_reg")];
char str994 [sizeof ("amd64_sub_membase_reg")];
char str995 [sizeof ("amd64_and_membase_reg")];
char str996 [sizeof ("amd64_or_membase_reg")];
char str997 [sizeof ("amd64_xor_membase_reg")];
char str998 [sizeof ("amd64_mul_membase_reg")];
char str1000 [sizeof ("amd64_add_membase_imm")];
char str1001 [sizeof ("amd64_sub_membase_imm")];
char str1002 [sizeof ("amd64_and_membase_imm")];
char str1003 [sizeof ("amd64_or_membase_imm")];
char str1004 [sizeof ("amd64_xor_membase_imm")];
char str1005 [sizeof ("amd64_mul_membase_imm")];
char str1007 [sizeof ("amd64_add_reg_membase")];
char str1008 [sizeof ("amd64_sub_reg_membase")];
char str1009 [sizeof ("amd64_and_reg_membase")];
char str1010 [sizeof ("amd64_or_reg_membase")];
char str1011 [sizeof ("amd64_xor_reg_membase")];
char str1012 [sizeof ("amd64_mul_reg_membase")];
char str1014 [sizeof ("amd64_loadi8_memindex")];
char str1015 [sizeof ("amd64_save_sp_to_lmf")];
char str1234 [sizeof ("objc_get_selector")];
#undef MINI_OP3
} opstr = {
#define MINI_OP(a,b,dest,src1,src2) b,
#define MINI_OP3(a,b,dest,src1,src2,src3) b,
// #include "mini-ops.h"
"load",
"ldaddr",
"store",
"nop",
"hard_nop",
"relaxed_nop",
"phi",
"fphi",
"vphi",
"compare",
"compare_imm",
"fcompare",
"lcompare",
"icompare",
"icompare_imm",
"lcompare_imm",
"local",
"arg",
"gsharedvt_local",
"gsharedvt_arg_regoffset",
"outarg_vt",
"outarg_vtretaddr",
"setret",
"setfret",
"setlret",
"localloc",
"localloc_imm",
"checkthis",
"seq_point",
"implicit_exception",
"voidcall",
"voidcallvirt",
"voidcall_reg",
"voidcall_membase",
"call",
"call_reg",
"call_membase",
"callvirt",
"fcall",
"fcallvirt",
"fcall_reg",
"fcall_membase",
"lcall",
"lcallvirt",
"lcall_reg",
"lcall_membase",
"vcall",
"vcallvirt",
"vcall_reg",
"vcall_membase",
"vcall2",
"vcall2_reg",
"vcall2_membase",
"dyn_call",
"iconst",
"i8const",
"r4const",
"r8const",
"regvar",
"regoffset",
"vtarg_addr",
"label",
"switch",
"throw",
"rethrow",
"oparglist",
"store_membase_reg",
"storei1_membase_reg",
"storei2_membase_reg",
"storei4_membase_reg",
"storei8_membase_reg",
"storer4_membase_reg",
"storer8_membase_reg",
"storex_membase_reg",
"storex_aligned_membase_reg",
"storex_nta_membase_reg",
"store_membase_imm",
"storei1_membase_imm",
"storei2_membase_imm",
"storei4_membase_imm",
"storei8_membase_imm",
"storex_membase",
"storev_membase",
"load_membase",
"loadi1_membase",
"loadu1_membase",
"loadi2_membase",
"loadu2_membase",
"loadi4_membase",
"loadu4_membase",
"loadi8_membase",
"loadr4_membase",
"loadr8_membase",
"loadx_membase",
"loadx_aligned_membase",
"loadv_membase",
"load_memindex",
"loadi1_memindex",
"loadu1_memindex",
"loadi2_memindex",
"loadu2_memindex",
"loadi4_memindex",
"loadu4_memindex",
"loadi8_memindex",
"loadr4_memindex",
"loadr8_memindex",
"store_memindex",
"storei1_memindex",
"storei2_memindex",
"storei4_memindex",
"storei8_memindex",
"storer4_memindex",
"storer8_memindex",
"load_mem",
"loadu1_mem",
"loadu2_mem",
"loadi4_mem",
"loadu4_mem",
"loadi8_mem",
"store_mem_imm",
"move",
"lmove",
"fmove",
"vmove",
"vzero",
"add_imm",
"sub_imm",
"mul_imm",
"div_imm",
"div_un_imm",
"rem_imm",
"rem_un_imm",
"and_imm",
"or_imm",
"xor_imm",
"shl_imm",
"shr_imm",
"shr_un_imm",
"br",
"jmp",
"tailcall",
"break",
"ceq",
"cgt",
"cgt.un",
"clt",
"clt.un",
"cond_exc_eq",
"cond_exc_ge",
"cond_exc_gt",
"cond_exc_le",
"cond_exc_lt",
"cond_exc_ne_un",
"cond_exc_ge_un",
"cond_exc_gt_un",
"cond_exc_le_un",
"cond_exc_lt_un",
"cond_exc_ov",
"cond_exc_no",
"cond_exc_c",
"cond_exc_nc",
"cond_exc_ieq",
"cond_exc_ige",
"cond_exc_igt",
"cond_exc_ile",
"cond_exc_ilt",
"cond_exc_ine_un",
"cond_exc_ige_un",
"cond_exc_igt_un",
"cond_exc_ile_un",
"cond_exc_ilt_un",
"cond_exc_iov",
"cond_exc_ino",
"cond_exc_ic",
"cond_exc_inc",
"long_add",
"long_sub",
"long_mul",
"long_div",
"long_div_un",
"long_rem",
"long_rem_un",
"long_and",
"long_or",
"long_xor",
"long_shl",
"long_shr",
"long_shr_un",
"long_neg",
"long_not",
"long_conv_to_i1",
"long_conv_to_i2",
"long_conv_to_i4",
"long_conv_to_i8",
"long_conv_to_r4",
"long_conv_to_r8",
"long_conv_to_u4",
"long_conv_to_u8",
"long_conv_to_u2",
"long_conv_to_u1",
"long_conv_to_i",
"long_conv_to_ovf_i",
"long_conv_to_ovf_u",
"long_add_ovf",
"long_add_ovf_un",
"long_mul_ovf",
"long_mul_ovf_un",
"long_sub_ovf",
"long_sub_ovf_un",
"long_conv_to_ovf_i1_un",
"long_conv_to_ovf_i2_un",
"long_conv_to_ovf_i4_un",
"long_conv_to_ovf_i8_un",
"long_conv_to_ovf_u1_un",
"long_conv_to_ovf_u2_un",
"long_conv_to_ovf_u4_un",
"long_conv_to_ovf_u8_un",
"long_conv_to_ovf_i_un",
"long_conv_to_ovf_u_un",
"long_conv_to_ovf_i1",
"long_conv_to_ovf_u1",
"long_conv_to_ovf_i2",
"long_conv_to_ovf_u2",
"long_conv_to_ovf_i4",
"long_conv_to_ovf_u4",
"long_conv_to_ovf_i8",
"long_conv_to_ovf_u8",
"long_ceq",
"long_cgt",
"long_cgt_un",
"long_clt",
"long_clt_un",
"long_conv_to_r_un",
"long_conv_to_u",
"long_add_imm",
"long_sub_imm",
"long_mul_imm",
"long_and_imm",
"long_or_imm",
"long_xor_imm",
"long_shl_imm",
"long_shr_imm",
"long_shr_un_imm",
"long_div_imm",
"long_div_un_imm",
"long_rem_imm",
"long_rem_un_imm",
"long_beq",
"long_bge",
"long_bgt",
"long_ble",
"long_blt",
"long_bne_un",
"long_bge_un",
"long_bgt_un",
"long_ble_un",
"long_blt_un",
"long_conv_to_r8_2",
"long_conv_to_r4_2",
"long_conv_to_r_un_2",
"long_conv_to_ovf_i4_2",
"int_add",
"int_sub",
"int_mul",
"int_div",
"int_div_un",
"int_rem",
"int_rem_un",
"int_and",
"int_or",
"int_xor",
"int_shl",
"int_shr",
"int_shr_un",
"int_neg",
"int_not",
"int_conv_to_i1",
"int_conv_to_i2",
"int_conv_to_i4",
"int_conv_to_i8",
"int_conv_to_r4",
"int_conv_to_r8",
"int_conv_to_u4",
"int_conv_to_u8",
"int_conv_to_r_un",
"int_conv_to_u",
"int_conv_to_u2",
"int_conv_to_u1",
"int_conv_to_i",
"int_conv_to_ovf_i",
"int_conv_to_ovf_u",
"int_add_ovf",
"int_add_ovf_un",
"int_mul_ovf",
"int_mul_ovf_un",
"int_sub_ovf",
"int_sub_ovf_un",
"int_conv_to_ovf_i1_un",
"int_conv_to_ovf_i2_un",
"int_conv_to_ovf_i4_un",
"int_conv_to_ovf_i8_un",
"int_conv_to_ovf_u1_un",
"int_conv_to_ovf_u2_un",
"int_conv_to_ovf_u4_un",
"int_conv_to_ovf_u8_un",
"int_conv_to_ovf_i_un",
"int_conv_to_ovf_u_un",
"int_conv_to_ovf_i1",
"int_conv_to_ovf_u1",
"int_conv_to_ovf_i2",
"int_conv_to_ovf_u2",
"int_conv_to_ovf_i4",
"int_conv_to_ovf_u4",
"int_conv_to_ovf_i8",
"int_conv_to_ovf_u8",
"int_adc",
"int_adc_imm",
"int_sbb",
"int_sbb_imm",
"int_addcc",
"int_subcc",
"int_add_imm",
"int_sub_imm",
"int_mul_imm",
"int_div_imm",
"int_div_un_imm",
"int_rem_imm",
"int_rem_un_imm",
"int_and_imm",
"int_or_imm",
"int_xor_imm",
"int_shl_imm",
"int_shr_imm",
"int_shr_un_imm",
"int_ceq",
"int_cgt",
"int_cgt_un",
"int_clt",
"int_clt_un",
"int_beq",
"int_bge",
"int_bgt",
"int_ble",
"int_blt",
"int_bne_un",
"int_bge_un",
"int_bgt_un",
"int_ble_un",
"int_blt_un",
"float_beq",
"float_bge",
"float_bgt",
"float_ble",
"float_blt",
"float_bne_un",
"float_bge_un",
"float_bgt_un",
"float_ble_un",
"float_blt_un",
"float_add",
"float_sub",
"float_mul",
"float_div",
"float_div_un",
"float_rem",
"float_rem_un",
"float_neg",
"float_not",
"float_conv_to_i1",
"float_conv_to_i2",
"float_conv_to_i4",
"float_conv_to_i8",
"float_conv_to_r4",
"float_conv_to_r8",
"float_conv_to_u4",
"float_conv_to_u8",
"float_conv_to_u2",
"float_conv_to_u1",
"float_conv_to_i",
"float_conv_to_ovf_i",
"float_conv_to_ovd_u",
"float_add_ovf",
"float_add_ovf_un",
"float_mul_ovf",
"float_mul_ovf_un",
"float_sub_ovf",
"float_sub_ovf_un",
"float_conv_to_ovf_i1_un",
"float_conv_to_ovf_i2_un",
"float_conv_to_ovf_i4_un",
"float_conv_to_ovf_i8_un",
"float_conv_to_ovf_u1_un",
"float_conv_to_ovf_u2_un",
"float_conv_to_ovf_u4_un",
"float_conv_to_ovf_u8_un",
"float_conv_to_ovf_i_un",
"float_conv_to_ovf_u_un",
"float_conv_to_ovf_i1",
"float_conv_to_ovf_u1",
"float_conv_to_ovf_i2",
"float_conv_to_ovf_u2",
"float_conv_to_ovf_i4",
"float_conv_to_ovf_u4",
"float_conv_to_ovf_i8",
"float_conv_to_ovf_u8",
"float_ceq",
"float_cgt",
"float_cgt_un",
"float_clt",
"float_clt_un",
"float_ceq_membase",
"float_cgt_membase",
"float_cgt_un_membase",
"float_clt_membase",
"float_clt_un_membase",
"float_conv_to_u",
"ckfinite",
"float_getlow32",
"float_gethigh32",
"jump_table",
"aot_const",
"patch_info",
"got_entry",
"call_handler",
"start_handler",
"endfilter",
"endfinally",
"bigmul",
"bigmul_un",
"int_min_un",
"int_max_un",
"long_min_un",
"long_max_un",
"min",
"max",
"int_min",
"int_max",
"long_min",
"long_max",
"adc",
"adc_imm",
"sbb",
"sbb_imm",
"addcc",
"addcc_imm",
"subcc",
"subcc_imm",
"br_reg",
"sext_i1",
"sext_i2",
"sext_i4",
"zext_i1",
"zext_i2",
"zext_i4",
"cne",
"trunc_i4",
"add_ovf_carry",
"sub_ovf_carry",
"add_ovf_un_carry",
"sub_ovf_un_carry",
"laddcc",
"lsubcc",
"sin",
"cos",
"abs",
"tan",
"atan",
"sqrt",
"round",
"strlen",
"newarr",
"ldlen",
"bounds_check",
"getldelema2",
"memcpy",
"memset",
"save_lmf",
"restore_lmf",
"card_table_wbarrier",
"tls_get",
"tls_get_reg",
"load_gotaddr",
"dummy_use",
"dummy_store",
"not_reached",
"not_null",
"addps",
"divps",
"mulps",
"subps",
"maxps",
"minps",
"compps",
"andps",
"andnps",
"orps",
"xorps",
"haddps",
"hsubps",
"addsubps",
"dupps_low",
"dupps_high",
"rsqrtps",
"sqrtps",
"rcpps",
"pshufflew_high",
"pshufflew_low",
"pshuffled",
"shufps",
"shufpd",
"addpd",
"divpd",
"mulpd",
"subpd",
"maxpd",
"minpd",
"comppd",
"andpd",
"andnpd",
"orpd",
"xorpd",
"haddpd",
"hsubpd",
"addsubpd",
"duppd",
"sqrtpd",
"extract_mask",
"pand",
"por",
"pxor",
"paddb",
"paddw",
"paddd",
"paddq",
"psubb",
"psubw",
"psubd",
"psubq",
"pmaxb_un",
"pmaxw_un",
"pmaxd_un",
"pmaxb",
"pmaxw",
"pmaxd",
"pavgb_un",
"pavgw_un",
"pminb_un",
"pminw_un",
"pmind_un",
"pminb",
"pminw",
"pmind",
"pcmpeqb",
"pcmpeqw",
"pcmpeqd",
"pcmpeqq",
"pcmpgtb",
"pcmpgtw",
"pcmpgtd",
"pcmpgtq",
"psumabsdiff",
"unpack_lowb",
"unpack_loww",
"unpack_lowd",
"unpack_lowq",
"unpack_lowps",
"unpack_lowpd",
"unpack_highb",
"unpack_highw",
"unpack_highd",
"unpack_highq",
"unpack_highps",
"unpack_highpd",
"packw",
"packd",
"packw_un",
"packd_un",
"paddb_sat",
"paddb_sat_un",
"paddw_sat",
"paddw_sat_un",
"psubb_sat",
"psubb_sat_un",
"psubw_sat",
"psubw_sat_un",
"pmulw",
"pmuld",
"pmulq",
"pmul_high_un",
"pmul_high",
"pshrw",
"pshrw_reg",
"psarw",
"psarw_reg",
"pshlw",
"pshlw_reg",
"pshrd",
"pshrd_reg",
"pshrq",
"pshrq_reg",
"psard",
"psard_reg",
"pshld",
"pshld_reg",
"pshlq",
"pshlq_reg",
"extract_i4",
"iconv_to_r8_raw",
"extract_i2",
"extract_u2",
"extract_i1",
"extract_u1",
"extract_r8",
"extract_i8",
"insert_i1",
"insert_i4",
"insert_i8",
"insert_r4",
"insert_r8",
"insert_i2",
"extractx_u2",
"insertx_u1_slow",
"insertx_i4_slow",
"insertx_r4_slow",
"insertx_r8_slow",
"insertx_i8_slow",
"fconv_to_r8_x",
"xconv_r8_to_i4",
"iconv_to_x",
"expand_i1",
"expand_i2",
"expand_i4",
"expand_r4",
"expand_i8",
"expand_r8",
"prefetch_membase",
"cvtdq2pd",
"cvtdq2ps",
"cvtpd2dq",
"cvtpd2ps",
"cvtps2dq",
"cvtps2pd",
"cvttpd2dq",
"cvttps2dq",
"xmove",
"xzero",
"xphi",
"atomic_add_i4",
"atomic_add_new_i4",
"atomic_add_imm_i4",
"atomic_add_imm_new_i4",
"atomic_exchange_i4",
"atomic_add_i8",
"atomic_add_new_i8",
"atomic_add_imm_i8",
"atomic_add_imm_new_i8",
"atomic_exchange_i8",
"memory_barrier",
"atomic_cas_i4",
"atomic_cas_i8",
"cmov_ieq",
"cmov_ige",
"cmov_igt",
"cmov_ile",
"cmov_ilt",
"cmov_ine_un",
"cmov_ige_un",
"cmov_igt_un",
"cmov_ile_un",
"cmov_ilt_un",
"cmov_leq",
"cmov_lge",
"cmov_lgt",
"cmov_lle",
"cmov_llt",
"cmov_lne_un",
"cmov_lge_un",
"cmov_lgt_un",
"cmov_lle_un",
"cmov_llt_un",
"liverange_start",
"liverange_end",
"gc_liveness_def",
"gc_liveness_use",
"gc_spill_slot_liveness_def",
"gc_param_slot_liveness_def",
"nacl_gc_safe_point",
"x86_test_null",
"x86_compare_membase_reg",
"x86_compare_membase_imm",
"x86_compare_mem_imm",
"x86_compare_membase8_imm",
"x86_compare_reg_membase",
"x86_inc_reg",
"x86_inc_membase",
"x86_dec_reg",
"x86_dec_membase",
"x86_add_membase_imm",
"x86_sub_membase_imm",
"x86_and_membase_imm",
"x86_or_membase_imm",
"x86_xor_membase_imm",
"x86_add_membase_reg",
"x86_sub_membase_reg",
"x86_and_membase_reg",
"x86_or_membase_reg",
"x86_xor_membase_reg",
"x86_mul_membase_reg",
"x86_add_reg_membase",
"x86_sub_reg_membase",
"x86_mul_reg_membase",
"x86_and_reg_membase",
"x86_or_reg_membase",
"x86_xor_reg_membase",
"x86_push_membase",
"x86_push_imm",
"x86_push",
"x86_push_obj",
"x86_push_got_entry",
"x86_lea",
"x86_lea_membase",
"x86_xchg",
"x86_fpop",
"x86_fp_load_i8",
"x86_fp_load_i4",
"x86_seteq_membase",
"x86_setne_membase",
"x86_fxch",
"amd64_test_null",
"amd64_set_xmmreg_r4",
"amd64_set_xmmreg_r8",
"amd64_icompare_membase_reg",
"amd64_icompare_membase_imm",
"amd64_icompare_reg_membase",
"amd64_compare_membase_reg",
"amd64_compare_membase_imm",
"amd64_compare_reg_membase",
"amd64_add_membase_reg",
"amd64_sub_membase_reg",
"amd64_and_membase_reg",
"amd64_or_membase_reg",
"amd64_xor_membase_reg",
"amd64_mul_membase_reg",
"amd64_add_membase_imm",
"amd64_sub_membase_imm",
"amd64_and_membase_imm",
"amd64_or_membase_imm",
"amd64_xor_membase_imm",
"amd64_mul_membase_imm",
"amd64_add_reg_membase",
"amd64_sub_reg_membase",
"amd64_and_reg_membase",
"amd64_or_reg_membase",
"amd64_xor_reg_membase",
"amd64_mul_reg_membase",
"amd64_loadi8_memindex",
"amd64_save_sp_to_lmf",
"objc_get_selector",
#undef MINI_OP
#undef MINI_OP3
};
static const gint16 opidx [] = {
#define MINI_OP(a,b,dest,src1,src2) [a - OP_LOAD] = offsetof (struct msgstr_t, MSGSTRFIELD(__LINE__)),
#define MINI_OP3(a,b,dest,src1,src2,src3) [a - OP_LOAD] = offsetof (struct msgstr_t, MSGSTRFIELD(__LINE__)),
// #include "mini-ops.h"
[OP_LOAD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str6),
[OP_LDADDR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str7),
[OP_STORE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str8),
[OP_NOP - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str9),
[OP_HARD_NOP - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str10),
[OP_RELAXED_NOP - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str11),
[OP_PHI - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str12),
[OP_FPHI - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str13),
[OP_VPHI - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str14),
[OP_COMPARE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str15),
[OP_COMPARE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str16),
[OP_FCOMPARE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str17),
[OP_LCOMPARE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str18),
[OP_ICOMPARE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str19),
[OP_ICOMPARE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str20),
[OP_LCOMPARE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str21),
[OP_LOCAL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str22),
[OP_ARG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str23),
[OP_GSHAREDVT_LOCAL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str25),
[OP_GSHAREDVT_ARG_REGOFFSET - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str26),
[OP_OUTARG_VT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str31),
[OP_OUTARG_VTRETADDR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str32),
[OP_SETRET - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str33),
[OP_SETFRET - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str34),
[OP_SETLRET - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str35),
[OP_LOCALLOC - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str36),
[OP_LOCALLOC_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str37),
[OP_CHECK_THIS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str38),
[OP_SEQ_POINT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str39),
[OP_IMPLICIT_EXCEPTION - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str40),
[OP_VOIDCALL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str42),
[OP_VOIDCALLVIRT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str43),
[OP_VOIDCALL_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str44),
[OP_VOIDCALL_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str45),
[OP_CALL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str46),
[OP_CALL_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str47),
[OP_CALL_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str48),
[OP_CALLVIRT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str49),
[OP_FCALL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str50),
[OP_FCALLVIRT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str51),
[OP_FCALL_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str52),
[OP_FCALL_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str53),
[OP_LCALL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str54),
[OP_LCALLVIRT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str55),
[OP_LCALL_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str56),
[OP_LCALL_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str57),
[OP_VCALL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str58),
[OP_VCALLVIRT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str59),
[OP_VCALL_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str60),
[OP_VCALL_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str61),
[OP_VCALL2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str63),
[OP_VCALL2_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str64),
[OP_VCALL2_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str65),
[OP_DYN_CALL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str66),
[OP_ICONST - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str68),
[OP_I8CONST - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str69),
[OP_R4CONST - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str70),
[OP_R8CONST - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str71),
[OP_REGVAR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str72),
[OP_REGOFFSET - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str73),
[OP_VTARG_ADDR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str74),
[OP_LABEL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str75),
[OP_SWITCH - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str76),
[OP_THROW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str77),
[OP_RETHROW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str78),
[OP_ARGLIST - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str95),
[OP_STORE_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str98),
[OP_STOREI1_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str99),
[OP_STOREI2_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str100),
[OP_STOREI4_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str101),
[OP_STOREI8_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str102),
[OP_STORER4_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str103),
[OP_STORER8_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str104),
[OP_STOREX_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str107),
[OP_STOREX_ALIGNED_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str108),
[OP_STOREX_NTA_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str109),
[OP_STORE_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str112),
[OP_STOREI1_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str113),
[OP_STOREI2_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str114),
[OP_STOREI4_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str115),
[OP_STOREI8_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str116),
[OP_STOREX_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str117),
[OP_STOREV_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str118),
[OP_LOAD_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str121),
[OP_LOADI1_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str122),
[OP_LOADU1_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str123),
[OP_LOADI2_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str124),
[OP_LOADU2_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str125),
[OP_LOADI4_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str126),
[OP_LOADU4_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str127),
[OP_LOADI8_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str128),
[OP_LOADR4_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str129),
[OP_LOADR8_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str130),
[OP_LOADX_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str132),
[OP_LOADX_ALIGNED_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str135),
[OP_LOADV_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str138),
[OP_LOAD_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str141),
[OP_LOADI1_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str142),
[OP_LOADU1_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str143),
[OP_LOADI2_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str144),
[OP_LOADU2_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str145),
[OP_LOADI4_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str146),
[OP_LOADU4_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str147),
[OP_LOADI8_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str148),
[OP_LOADR4_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str149),
[OP_LOADR8_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str150),
[OP_STORE_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str153),
[OP_STOREI1_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str154),
[OP_STOREI2_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str155),
[OP_STOREI4_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str156),
[OP_STOREI8_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str157),
[OP_STORER4_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str158),
[OP_STORER8_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str159),
[OP_LOAD_MEM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str161),
[OP_LOADU1_MEM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str162),
[OP_LOADU2_MEM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str163),
[OP_LOADI4_MEM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str164),
[OP_LOADU4_MEM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str165),
[OP_LOADI8_MEM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str166),
[OP_STORE_MEM_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str167),
[OP_MOVE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str169),
[OP_LMOVE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str170),
[OP_FMOVE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str171),
[OP_VMOVE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str172),
[OP_VZERO - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str174),
[OP_ADD_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str176),
[OP_SUB_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str177),
[OP_MUL_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str178),
[OP_DIV_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str179),
[OP_DIV_UN_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str180),
[OP_REM_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str181),
[OP_REM_UN_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str182),
[OP_AND_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str183),
[OP_OR_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str184),
[OP_XOR_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str185),
[OP_SHL_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str186),
[OP_SHR_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str187),
[OP_SHR_UN_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str188),
[OP_BR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str190),
[OP_JMP - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str191),
[OP_TAILCALL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str193),
[OP_BREAK - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str194),
[OP_CEQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str196),
[OP_CGT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str197),
[OP_CGT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str198),
[OP_CLT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str199),
[OP_CLT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str200),
[OP_COND_EXC_EQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str203),
[OP_COND_EXC_GE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str204),
[OP_COND_EXC_GT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str205),
[OP_COND_EXC_LE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str206),
[OP_COND_EXC_LT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str207),
[OP_COND_EXC_NE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str208),
[OP_COND_EXC_GE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str209),
[OP_COND_EXC_GT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str210),
[OP_COND_EXC_LE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str211),
[OP_COND_EXC_LT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str212),
[OP_COND_EXC_OV - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str214),
[OP_COND_EXC_NO - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str215),
[OP_COND_EXC_C - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str216),
[OP_COND_EXC_NC - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str217),
[OP_COND_EXC_IEQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str219),
[OP_COND_EXC_IGE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str220),
[OP_COND_EXC_IGT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str221),
[OP_COND_EXC_ILE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str222),
[OP_COND_EXC_ILT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str223),
[OP_COND_EXC_INE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str224),
[OP_COND_EXC_IGE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str225),
[OP_COND_EXC_IGT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str226),
[OP_COND_EXC_ILE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str227),
[OP_COND_EXC_ILT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str228),
[OP_COND_EXC_IOV - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str230),
[OP_COND_EXC_INO - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str231),
[OP_COND_EXC_IC - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str232),
[OP_COND_EXC_INC - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str233),
[OP_LADD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str236),
[OP_LSUB - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str237),
[OP_LMUL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str238),
[OP_LDIV - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str239),
[OP_LDIV_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str240),
[OP_LREM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str241),
[OP_LREM_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str242),
[OP_LAND - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str243),
[OP_LOR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str244),
[OP_LXOR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str245),
[OP_LSHL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str246),
[OP_LSHR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str247),
[OP_LSHR_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str248),
[OP_LNEG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str251),
[OP_LNOT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str252),
[OP_LCONV_TO_I1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str253),
[OP_LCONV_TO_I2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str254),
[OP_LCONV_TO_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str255),
[OP_LCONV_TO_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str256),
[OP_LCONV_TO_R4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str257),
[OP_LCONV_TO_R8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str258),
[OP_LCONV_TO_U4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str259),
[OP_LCONV_TO_U8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str260),
[OP_LCONV_TO_U2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str262),
[OP_LCONV_TO_U1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str263),
[OP_LCONV_TO_I - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str264),
[OP_LCONV_TO_OVF_I - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str265),
[OP_LCONV_TO_OVF_U - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str266),
[OP_LADD_OVF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str268),
[OP_LADD_OVF_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str269),
[OP_LMUL_OVF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str270),
[OP_LMUL_OVF_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str271),
[OP_LSUB_OVF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str272),
[OP_LSUB_OVF_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str273),
[OP_LCONV_TO_OVF_I1_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str275),
[OP_LCONV_TO_OVF_I2_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str276),
[OP_LCONV_TO_OVF_I4_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str277),
[OP_LCONV_TO_OVF_I8_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str278),
[OP_LCONV_TO_OVF_U1_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str279),
[OP_LCONV_TO_OVF_U2_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str280),
[OP_LCONV_TO_OVF_U4_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str281),
[OP_LCONV_TO_OVF_U8_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str282),
[OP_LCONV_TO_OVF_I_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str283),
[OP_LCONV_TO_OVF_U_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str284),
[OP_LCONV_TO_OVF_I1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str286),
[OP_LCONV_TO_OVF_U1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str287),
[OP_LCONV_TO_OVF_I2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str288),
[OP_LCONV_TO_OVF_U2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str289),
[OP_LCONV_TO_OVF_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str290),
[OP_LCONV_TO_OVF_U4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str291),
[OP_LCONV_TO_OVF_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str292),
[OP_LCONV_TO_OVF_U8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str293),
[OP_LCEQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str296),
[OP_LCGT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str297),
[OP_LCGT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str298),
[OP_LCLT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str299),
[OP_LCLT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str300),
[OP_LCONV_TO_R_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str302),
[OP_LCONV_TO_U - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str303),
[OP_LADD_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str305),
[OP_LSUB_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str306),
[OP_LMUL_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str307),
[OP_LAND_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str308),
[OP_LOR_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str309),
[OP_LXOR_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str310),
[OP_LSHL_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str311),
[OP_LSHR_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str312),
[OP_LSHR_UN_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str313),
[OP_LDIV_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str314),
[OP_LDIV_UN_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str315),
[OP_LREM_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str316),
[OP_LREM_UN_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str317),
[OP_LBEQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str320),
[OP_LBGE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str321),
[OP_LBGT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str322),
[OP_LBLE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str323),
[OP_LBLT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str324),
[OP_LBNE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str325),
[OP_LBGE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str326),
[OP_LBGT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str327),
[OP_LBLE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str328),
[OP_LBLT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str329),
[OP_LCONV_TO_R8_2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str332),
[OP_LCONV_TO_R4_2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str333),
[OP_LCONV_TO_R_UN_2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str334),
[OP_LCONV_TO_OVF_I4_2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str335),
[OP_IADD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str338),
[OP_ISUB - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str339),
[OP_IMUL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str340),
[OP_IDIV - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str341),
[OP_IDIV_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str342),
[OP_IREM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str343),
[OP_IREM_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str344),
[OP_IAND - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str345),
[OP_IOR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str346),
[OP_IXOR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str347),
[OP_ISHL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str348),
[OP_ISHR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str349),
[OP_ISHR_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str350),
[OP_INEG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str353),
[OP_INOT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str354),
[OP_ICONV_TO_I1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str355),
[OP_ICONV_TO_I2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str356),
[OP_ICONV_TO_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str357),
[OP_ICONV_TO_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str358),
[OP_ICONV_TO_R4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str359),
[OP_ICONV_TO_R8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str360),
[OP_ICONV_TO_U4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str361),
[OP_ICONV_TO_U8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str362),
[OP_ICONV_TO_R_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str364),
[OP_ICONV_TO_U - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str365),
[OP_ICONV_TO_U2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str368),
[OP_ICONV_TO_U1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str369),
[OP_ICONV_TO_I - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str370),
[OP_ICONV_TO_OVF_I - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str371),
[OP_ICONV_TO_OVF_U - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str372),
[OP_IADD_OVF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str373),
[OP_IADD_OVF_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str374),
[OP_IMUL_OVF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str375),
[OP_IMUL_OVF_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str376),
[OP_ISUB_OVF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str377),
[OP_ISUB_OVF_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str378),
[OP_ICONV_TO_OVF_I1_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str381),
[OP_ICONV_TO_OVF_I2_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str382),
[OP_ICONV_TO_OVF_I4_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str383),
[OP_ICONV_TO_OVF_I8_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str384),
[OP_ICONV_TO_OVF_U1_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str385),
[OP_ICONV_TO_OVF_U2_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str386),
[OP_ICONV_TO_OVF_U4_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str387),
[OP_ICONV_TO_OVF_U8_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str388),
[OP_ICONV_TO_OVF_I_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str389),
[OP_ICONV_TO_OVF_U_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str390),
[OP_ICONV_TO_OVF_I1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str393),
[OP_ICONV_TO_OVF_U1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str394),
[OP_ICONV_TO_OVF_I2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str395),
[OP_ICONV_TO_OVF_U2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str396),
[OP_ICONV_TO_OVF_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str397),
[OP_ICONV_TO_OVF_U4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str398),
[OP_ICONV_TO_OVF_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str399),
[OP_ICONV_TO_OVF_U8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str400),
[OP_IADC - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str402),
[OP_IADC_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str403),
[OP_ISBB - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str404),
[OP_ISBB_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str405),
[OP_IADDCC - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str406),
[OP_ISUBCC - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str407),
[OP_IADD_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str409),
[OP_ISUB_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str410),
[OP_IMUL_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str411),
[OP_IDIV_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str412),
[OP_IDIV_UN_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str413),
[OP_IREM_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str414),
[OP_IREM_UN_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str415),
[OP_IAND_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str416),
[OP_IOR_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str417),
[OP_IXOR_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str418),
[OP_ISHL_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str419),
[OP_ISHR_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str420),
[OP_ISHR_UN_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str421),
[OP_ICEQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str423),
[OP_ICGT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str424),
[OP_ICGT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str425),
[OP_ICLT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str426),
[OP_ICLT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str427),
[OP_IBEQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str429),
[OP_IBGE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str430),
[OP_IBGT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str431),
[OP_IBLE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str432),
[OP_IBLT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str433),
[OP_IBNE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str434),
[OP_IBGE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str435),
[OP_IBGT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str436),
[OP_IBLE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str437),
[OP_IBLT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str438),
[OP_FBEQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str440),
[OP_FBGE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str441),
[OP_FBGT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str442),
[OP_FBLE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str443),
[OP_FBLT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str444),
[OP_FBNE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str445),
[OP_FBGE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str446),
[OP_FBGT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str447),
[OP_FBLE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str448),
[OP_FBLT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str449),
[OP_FADD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str452),
[OP_FSUB - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str453),
[OP_FMUL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str454),
[OP_FDIV - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str455),
[OP_FDIV_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str456),
[OP_FREM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str457),
[OP_FREM_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str458),
[OP_FNEG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str461),
[OP_FNOT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str462),
[OP_FCONV_TO_I1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str463),
[OP_FCONV_TO_I2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str464),
[OP_FCONV_TO_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str465),
[OP_FCONV_TO_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str466),
[OP_FCONV_TO_R4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str467),
[OP_FCONV_TO_R8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str468),
[OP_FCONV_TO_U4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str469),
[OP_FCONV_TO_U8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str470),
[OP_FCONV_TO_U2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str472),
[OP_FCONV_TO_U1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str473),
[OP_FCONV_TO_I - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str474),
[OP_FCONV_TO_OVF_I - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str475),
[OP_FCONV_TO_OVF_U - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str476),
[OP_FADD_OVF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str478),
[OP_FADD_OVF_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str479),
[OP_FMUL_OVF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str480),
[OP_FMUL_OVF_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str481),
[OP_FSUB_OVF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str482),
[OP_FSUB_OVF_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str483),
[OP_FCONV_TO_OVF_I1_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str485),
[OP_FCONV_TO_OVF_I2_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str486),
[OP_FCONV_TO_OVF_I4_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str487),
[OP_FCONV_TO_OVF_I8_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str488),
[OP_FCONV_TO_OVF_U1_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str489),
[OP_FCONV_TO_OVF_U2_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str490),
[OP_FCONV_TO_OVF_U4_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str491),
[OP_FCONV_TO_OVF_U8_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str492),
[OP_FCONV_TO_OVF_I_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str493),
[OP_FCONV_TO_OVF_U_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str494),
[OP_FCONV_TO_OVF_I1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str496),
[OP_FCONV_TO_OVF_U1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str497),
[OP_FCONV_TO_OVF_I2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str498),
[OP_FCONV_TO_OVF_U2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str499),
[OP_FCONV_TO_OVF_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str500),
[OP_FCONV_TO_OVF_U4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str501),
[OP_FCONV_TO_OVF_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str502),
[OP_FCONV_TO_OVF_U8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str503),
[OP_FCEQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str506),
[OP_FCGT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str507),
[OP_FCGT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str508),
[OP_FCLT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str509),
[OP_FCLT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str510),
[OP_FCEQ_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str512),
[OP_FCGT_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str513),
[OP_FCGT_UN_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str514),
[OP_FCLT_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str515),
[OP_FCLT_UN_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str516),
[OP_FCONV_TO_U - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str518),
[OP_CKFINITE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str519),
[OP_FGETLOW32 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str522),
[OP_FGETHIGH32 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str524),
[OP_JUMP_TABLE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str526),
[OP_AOTCONST - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str529),
[OP_PATCH_INFO - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str530),
[OP_GOT_ENTRY - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str531),
[OP_CALL_HANDLER - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str534),
[OP_START_HANDLER - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str535),
[OP_ENDFILTER - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str536),
[OP_ENDFINALLY - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str537),
[OP_BIGMUL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str540),
[OP_BIGMUL_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str541),
[OP_IMIN_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str542),
[OP_IMAX_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str543),
[OP_LMIN_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str544),
[OP_LMAX_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str545),
[OP_MIN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str547),
[OP_MAX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str548),
[OP_IMIN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str550),
[OP_IMAX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str551),
[OP_LMIN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str552),
[OP_LMAX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str553),
[OP_ADC - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str556),
[OP_ADC_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str557),
[OP_SBB - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str558),
[OP_SBB_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str559),
[OP_ADDCC - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str560),
[OP_ADDCC_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str561),
[OP_SUBCC - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str562),
[OP_SUBCC_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str563),
[OP_BR_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str564),
[OP_SEXT_I1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str565),
[OP_SEXT_I2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str566),
[OP_SEXT_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str567),
[OP_ZEXT_I1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str568),
[OP_ZEXT_I2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str569),
[OP_ZEXT_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str570),
[OP_CNE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str571),
[OP_TRUNC_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str572),
[OP_ADD_OVF_CARRY - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str574),
[OP_SUB_OVF_CARRY - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str575),
[OP_ADD_OVF_UN_CARRY - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str576),
[OP_SUB_OVF_UN_CARRY - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str577),
[OP_LADDCC - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str580),
[OP_LSUBCC - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str581),
[OP_SIN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str585),
[OP_COS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str586),
[OP_ABS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str587),
[OP_TAN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str588),
[OP_ATAN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str589),
[OP_SQRT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str590),
[OP_ROUND - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str591),
[OP_STRLEN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str593),
[OP_NEWARR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str594),
[OP_LDLEN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str595),
[OP_BOUNDS_CHECK - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str596),
[OP_LDELEMA2D - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str598),
[OP_MEMCPY - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str600),
[OP_MEMSET - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str602),
[OP_SAVE_LMF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str603),
[OP_RESTORE_LMF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str604),
[OP_CARD_TABLE_WBARRIER - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str607),
[OP_TLS_GET - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str610),
[OP_TLS_GET_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str611),
[OP_LOAD_GOTADDR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str613),
[OP_DUMMY_USE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str614),
[OP_DUMMY_STORE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str615),
[OP_NOT_REACHED - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str616),
[OP_NOT_NULL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str617),
[OP_ADDPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str623),
[OP_DIVPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str624),
[OP_MULPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str625),
[OP_SUBPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str626),
[OP_MAXPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str627),
[OP_MINPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str628),
[OP_COMPPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str629),
[OP_ANDPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str630),
[OP_ANDNPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str631),
[OP_ORPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str632),
[OP_XORPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str633),
[OP_HADDPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str634),
[OP_HSUBPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str635),
[OP_ADDSUBPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str636),
[OP_DUPPS_LOW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str637),
[OP_DUPPS_HIGH - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str638),
[OP_RSQRTPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str640),
[OP_SQRTPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str641),
[OP_RCPPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str642),
[OP_PSHUFLEW_HIGH - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str644),
[OP_PSHUFLEW_LOW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str645),
[OP_PSHUFLED - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str646),
[OP_SHUFPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str647),
[OP_SHUFPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str648),
[OP_ADDPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str650),
[OP_DIVPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str651),
[OP_MULPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str652),
[OP_SUBPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str653),
[OP_MAXPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str654),
[OP_MINPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str655),
[OP_COMPPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str656),
[OP_ANDPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str657),
[OP_ANDNPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str658),
[OP_ORPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str659),
[OP_XORPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str660),
[OP_HADDPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str661),
[OP_HSUBPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str662),
[OP_ADDSUBPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str663),
[OP_DUPPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str664),
[OP_SQRTPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str666),
[OP_EXTRACT_MASK - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str668),
[OP_PAND - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str670),
[OP_POR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str671),
[OP_PXOR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str672),
[OP_PADDB - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str674),
[OP_PADDW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str675),
[OP_PADDD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str676),
[OP_PADDQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str677),
[OP_PSUBB - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str679),
[OP_PSUBW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str680),
[OP_PSUBD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str681),
[OP_PSUBQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str682),
[OP_PMAXB_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str684),
[OP_PMAXW_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str685),
[OP_PMAXD_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str686),
[OP_PMAXB - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str688),
[OP_PMAXW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str689),
[OP_PMAXD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str690),
[OP_PAVGB_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str692),
[OP_PAVGW_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str693),
[OP_PMINB_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str695),
[OP_PMINW_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str696),
[OP_PMIND_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str697),
[OP_PMINB - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str699),
[OP_PMINW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str700),
[OP_PMIND - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str701),
[OP_PCMPEQB - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str703),
[OP_PCMPEQW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str704),
[OP_PCMPEQD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str705),
[OP_PCMPEQQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str706),
[OP_PCMPGTB - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str708),
[OP_PCMPGTW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str709),
[OP_PCMPGTD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str710),
[OP_PCMPGTQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str711),
[OP_PSUM_ABS_DIFF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str713),
[OP_UNPACK_LOWB - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str715),
[OP_UNPACK_LOWW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str716),
[OP_UNPACK_LOWD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str717),
[OP_UNPACK_LOWQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str718),
[OP_UNPACK_LOWPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str719),
[OP_UNPACK_LOWPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str720),
[OP_UNPACK_HIGHB - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str722),
[OP_UNPACK_HIGHW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str723),
[OP_UNPACK_HIGHD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str724),
[OP_UNPACK_HIGHQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str725),
[OP_UNPACK_HIGHPS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str726),
[OP_UNPACK_HIGHPD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str727),
[OP_PACKW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str729),
[OP_PACKD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str730),
[OP_PACKW_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str732),
[OP_PACKD_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str733),
[OP_PADDB_SAT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str735),
[OP_PADDB_SAT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str736),
[OP_PADDW_SAT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str738),
[OP_PADDW_SAT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str739),
[OP_PSUBB_SAT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str741),
[OP_PSUBB_SAT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str742),
[OP_PSUBW_SAT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str744),
[OP_PSUBW_SAT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str745),
[OP_PMULW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str747),
[OP_PMULD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str748),
[OP_PMULQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str749),
[OP_PMULW_HIGH_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str751),
[OP_PMULW_HIGH - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str752),
[OP_PSHRW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str755),
[OP_PSHRW_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str756),
[OP_PSARW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str758),
[OP_PSARW_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str759),
[OP_PSHLW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str761),
[OP_PSHLW_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str762),
[OP_PSHRD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str764),
[OP_PSHRD_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str765),
[OP_PSHRQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str767),
[OP_PSHRQ_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str768),
[OP_PSARD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str770),
[OP_PSARD_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str771),
[OP_PSHLD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str773),
[OP_PSHLD_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str774),
[OP_PSHLQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str776),
[OP_PSHLQ_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str777),
[OP_EXTRACT_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str779),
[OP_ICONV_TO_R8_RAW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str780),
[OP_EXTRACT_I2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str782),
[OP_EXTRACT_U2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str783),
[OP_EXTRACT_I1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str784),
[OP_EXTRACT_U1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str785),
[OP_EXTRACT_R8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str786),
[OP_EXTRACT_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str787),
[OP_INSERT_I1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str790),
[OP_INSERT_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str791),
[OP_INSERT_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str792),
[OP_INSERT_R4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str793),
[OP_INSERT_R8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str794),
[OP_INSERT_I2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str796),
[OP_EXTRACTX_U2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str798),
[OP_INSERTX_U1_SLOW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str802),
[OP_INSERTX_I4_SLOW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str804),
[OP_INSERTX_R4_SLOW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str806),
[OP_INSERTX_R8_SLOW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str807),
[OP_INSERTX_I8_SLOW - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str808),
[OP_FCONV_TO_R8_X - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str810),
[OP_XCONV_R8_TO_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str811),
[OP_ICONV_TO_X - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str812),
[OP_EXPAND_I1 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str814),
[OP_EXPAND_I2 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str815),
[OP_EXPAND_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str816),
[OP_EXPAND_R4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str817),
[OP_EXPAND_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str818),
[OP_EXPAND_R8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str819),
[OP_PREFETCH_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str821),
[OP_CVTDQ2PD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str823),
[OP_CVTDQ2PS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str824),
[OP_CVTPD2DQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str825),
[OP_CVTPD2PS - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str826),
[OP_CVTPS2DQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str827),
[OP_CVTPS2PD - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str828),
[OP_CVTTPD2DQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str829),
[OP_CVTTPS2DQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str830),
[OP_XMOVE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str834),
[OP_XZERO - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str835),
[OP_XPHI - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str836),
[OP_ATOMIC_ADD_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str849),
[OP_ATOMIC_ADD_NEW_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str850),
[OP_ATOMIC_ADD_IMM_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str851),
[OP_ATOMIC_ADD_IMM_NEW_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str852),
[OP_ATOMIC_EXCHANGE_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str853),
[OP_ATOMIC_ADD_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str855),
[OP_ATOMIC_ADD_NEW_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str856),
[OP_ATOMIC_ADD_IMM_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str857),
[OP_ATOMIC_ADD_IMM_NEW_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str858),
[OP_ATOMIC_EXCHANGE_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str859),
[OP_MEMORY_BARRIER - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str860),
[OP_ATOMIC_CAS_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str862),
[OP_ATOMIC_CAS_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str863),
[OP_CMOV_IEQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str874),
[OP_CMOV_IGE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str875),
[OP_CMOV_IGT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str876),
[OP_CMOV_ILE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str877),
[OP_CMOV_ILT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str878),
[OP_CMOV_INE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str879),
[OP_CMOV_IGE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str880),
[OP_CMOV_IGT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str881),
[OP_CMOV_ILE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str882),
[OP_CMOV_ILT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str883),
[OP_CMOV_LEQ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str885),
[OP_CMOV_LGE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str886),
[OP_CMOV_LGT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str887),
[OP_CMOV_LLE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str888),
[OP_CMOV_LLT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str889),
[OP_CMOV_LNE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str890),
[OP_CMOV_LGE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str891),
[OP_CMOV_LGT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str892),
[OP_CMOV_LLE_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str893),
[OP_CMOV_LLT_UN - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str894),
[OP_LIVERANGE_START - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str901),
[OP_LIVERANGE_END - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str906),
[OP_GC_LIVENESS_DEF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str913),
[OP_GC_LIVENESS_USE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str914),
[OP_GC_SPILL_SLOT_LIVENESS_DEF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str920),
[OP_GC_PARAM_SLOT_LIVENESS_DEF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str926),
[OP_NACL_GC_SAFE_POINT - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str933),
[OP_X86_TEST_NULL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str937),
[OP_X86_COMPARE_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str938),
[OP_X86_COMPARE_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str939),
[OP_X86_COMPARE_MEM_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str940),
[OP_X86_COMPARE_MEMBASE8_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str941),
[OP_X86_COMPARE_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str942),
[OP_X86_INC_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str943),
[OP_X86_INC_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str944),
[OP_X86_DEC_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str945),
[OP_X86_DEC_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str946),
[OP_X86_ADD_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str947),
[OP_X86_SUB_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str948),
[OP_X86_AND_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str949),
[OP_X86_OR_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str950),
[OP_X86_XOR_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str951),
[OP_X86_ADD_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str952),
[OP_X86_SUB_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str953),
[OP_X86_AND_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str954),
[OP_X86_OR_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str955),
[OP_X86_XOR_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str956),
[OP_X86_MUL_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str957),
[OP_X86_ADD_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str959),
[OP_X86_SUB_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str960),
[OP_X86_MUL_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str961),
[OP_X86_AND_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str962),
[OP_X86_OR_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str963),
[OP_X86_XOR_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str964),
[OP_X86_PUSH_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str966),
[OP_X86_PUSH_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str967),
[OP_X86_PUSH - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str968),
[OP_X86_PUSH_OBJ - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str969),
[OP_X86_PUSH_GOT_ENTRY - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str970),
[OP_X86_LEA - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str971),
[OP_X86_LEA_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str972),
[OP_X86_XCHG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str973),
[OP_X86_FPOP - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str974),
[OP_X86_FP_LOAD_I8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str975),
[OP_X86_FP_LOAD_I4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str976),
[OP_X86_SETEQ_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str977),
[OP_X86_SETNE_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str978),
[OP_X86_FXCH - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str979),
[OP_AMD64_TEST_NULL - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str983),
[OP_AMD64_SET_XMMREG_R4 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str984),
[OP_AMD64_SET_XMMREG_R8 - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str985),
[OP_AMD64_ICOMPARE_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str986),
[OP_AMD64_ICOMPARE_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str987),
[OP_AMD64_ICOMPARE_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str988),
[OP_AMD64_COMPARE_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str989),
[OP_AMD64_COMPARE_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str990),
[OP_AMD64_COMPARE_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str991),
[OP_AMD64_ADD_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str993),
[OP_AMD64_SUB_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str994),
[OP_AMD64_AND_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str995),
[OP_AMD64_OR_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str996),
[OP_AMD64_XOR_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str997),
[OP_AMD64_MUL_MEMBASE_REG - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str998),
[OP_AMD64_ADD_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1000),
[OP_AMD64_SUB_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1001),
[OP_AMD64_AND_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1002),
[OP_AMD64_OR_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1003),
[OP_AMD64_XOR_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1004),
[OP_AMD64_MUL_MEMBASE_IMM - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1005),
[OP_AMD64_ADD_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1007),
[OP_AMD64_SUB_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1008),
[OP_AMD64_AND_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1009),
[OP_AMD64_OR_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1010),
[OP_AMD64_XOR_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1011),
[OP_AMD64_MUL_REG_MEMBASE - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1012),
[OP_AMD64_LOADI8_MEMINDEX - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1014),
[OP_AMD64_SAVE_SP_TO_LMF - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1015),
[OP_OBJC_GET_SELECTOR - OP_LOAD] = __builtin_offsetof (struct msgstr_t, str1234),
#undef MINI_OP
#undef MINI_OP3
};

#else

#define MINI_OP(a,b,dest,src1,src2) b,
#define MINI_OP3(a,b,dest,src1,src2,src3) b,
/* keep in sync with the enum in mini.h */
static const char* const
opnames[] = {
#include "mini-ops.h"
};
#undef MINI_OP
#undef MINI_OP3

#endif

#endif /* DISABLE_LOGGING */

#if defined(__i386__) || defined(__x86_64__)
#define emit_debug_info  TRUE
#else
#define emit_debug_info  FALSE
#endif

/*This enables us to use the right tooling when building the cross compiler for iOS.*/
#if defined (__APPLE__) && defined (TARGET_ARM) && (defined(__i386__) || defined(__x86_64__))

#define ARCH_PREFIX "/Developer/Platforms/iPhoneOS.platform/Developer/usr/bin/"

#endif

#define ARCH_PREFIX ""
//#define ARCH_PREFIX "powerpc64-linux-gnu-"

const char*
mono_inst_name (int op) {
#ifndef DISABLE_LOGGING
	if (op >= OP_LOAD && op <= OP_LAST)
#ifdef HAVE_ARRAY_ELEM_INIT
		return (const char*)&opstr + opidx [op - OP_LOAD];
#else
		return opnames [op - OP_LOAD];
#endif
	if (op < OP_LOAD)
		return mono_opcode_name (op);
	g_error ("unknown opcode name for %d", op);
	return NULL;
#else
	g_assert_not_reached ();
#endif
}

void
mono_blockset_print (MonoCompile *cfg, MonoBitSet *set, const char *name, guint idom) 
{
#ifndef DISABLE_LOGGING
	int i;

	if (name)
		g_print ("%s:", name);
	
	mono_bitset_foreach_bit (set, i, cfg->num_bblocks) {
		if (idom == i)
			g_print (" [BB%d]", cfg->bblocks [i]->block_num);
		else
			g_print (" BB%d", cfg->bblocks [i]->block_num);
		
	}
	g_print ("\n");
#endif
}

/**
 * mono_disassemble_code:
 * @cfg: compilation context
 * @code: a pointer to the code
 * @size: the code size in bytes
 *
 * Disassemble to code to stdout.
 */
void
mono_disassemble_code (MonoCompile *cfg, guint8 *code, int size, char *id)
{
#if defined(__native_client__)
	return;
#endif
#ifndef DISABLE_LOGGING
	GHashTable *offset_to_bb_hash = NULL;
	int i, cindex, bb_num;
	FILE *ofd;
#ifdef HOST_WIN32
	const char *tmp = g_get_tmp_dir ();
#endif
	const char *objdump_args = g_getenv ("MONO_OBJDUMP_ARGS");
	char *as_file;
	char *o_file;
	char *cmd;
	int unused;

#ifdef HOST_WIN32
	as_file = g_strdup_printf ("%s/test.s", tmp);    

	if (!(ofd = fopen (as_file, "w")))
		g_assert_not_reached ();
#else	
	i = g_file_open_tmp (NULL, &as_file, NULL);
	ofd = fdopen (i, "w");
	g_assert (ofd);
#endif

	for (i = 0; id [i]; ++i) {
		if (i == 0 && isdigit (id [i]))
			fprintf (ofd, "_");
		else if (!isalnum (id [i]))
			fprintf (ofd, "_");
		else
			fprintf (ofd, "%c", id [i]);
	}
	fprintf (ofd, ":\n");

	if (emit_debug_info && cfg != NULL) {
		MonoBasicBlock *bb;

		fprintf (ofd, ".stabs	\"\",100,0,0,.Ltext0\n");
		fprintf (ofd, ".stabs	\"<BB>\",100,0,0,.Ltext0\n");
		fprintf (ofd, ".Ltext0:\n");

		offset_to_bb_hash = g_hash_table_new (NULL, NULL);
		for (bb = cfg->bb_entry; bb; bb = bb->next_bb) {
			g_hash_table_insert (offset_to_bb_hash, GINT_TO_POINTER (bb->native_offset), GINT_TO_POINTER (bb->block_num + 1));
		}
	}

	cindex = 0;
	for (i = 0; i < size; ++i) {
		if (emit_debug_info && cfg != NULL) {
			bb_num = GPOINTER_TO_INT (g_hash_table_lookup (offset_to_bb_hash, GINT_TO_POINTER (i)));
			if (bb_num) {
				fprintf (ofd, "\n.stabd 68,0,%d\n", bb_num - 1);
				cindex = 0;
			}
		}
		if (cindex == 0) {
			fprintf (ofd, "\n.byte %d", (unsigned int) code [i]);
		} else {
			fprintf (ofd, ",%d", (unsigned int) code [i]);
		}
		cindex++;
		if (cindex == 64)
			cindex = 0;
	}
	fprintf (ofd, "\n");
	fclose (ofd);

#ifdef __APPLE__
#ifdef __ppc64__
#define DIS_CMD "otool64 -v -t"
#else
#define DIS_CMD "otool -v -t"
#endif
#else
#if defined(sparc) && !defined(__GNUC__)
#define DIS_CMD "dis"
#elif defined(__i386__) || defined(__x86_64__)
#define DIS_CMD "objdump -l -d"
#else
#define DIS_CMD "objdump -d"
#endif
#endif

#if defined(sparc)
#define AS_CMD "as -xarch=v9"
#elif defined (TARGET_X86)
#  if defined(__APPLE__)
#    define AS_CMD "as -arch i386"
#  else
#    define AS_CMD "as -gstabs"
#  endif
#elif defined (TARGET_AMD64)
#  if defined (__APPLE__)
#    define AS_CMD "as -arch x86_64"
#  else
#    define AS_CMD "as -gstabs"
#  endif
#elif defined (TARGET_ARM)
#  if defined (__APPLE__)
#    define AS_CMD "as -arch arm"
#  else
#    define AS_CMD "as -gstabs"
#  endif
#elif defined(__mips__) && (_MIPS_SIM == _ABIO32)
#define AS_CMD "as -mips32"
#elif defined(__ppc64__)
#define AS_CMD "as -arch ppc64"
#elif defined(__powerpc64__)
#define AS_CMD "as -mppc64"
#else
#define AS_CMD "as"
#endif

#ifdef HOST_WIN32
	o_file = g_strdup_printf ("%s/test.o", tmp);
#else	
	i = g_file_open_tmp (NULL, &o_file, NULL);
	close (i);
#endif

	cmd = g_strdup_printf (ARCH_PREFIX AS_CMD " %s -o %s", as_file, o_file);
	unused = system (cmd); 
	g_free (cmd);
	if (!objdump_args)
		objdump_args = "";

	fflush (stdout);

#ifdef __arm__
	/* 
	 * The arm assembler inserts ELF directives instructing objdump to display 
	 * everything as data.
	 */
	cmd = g_strdup_printf (ARCH_PREFIX "strip -x %s", o_file);
	unused = system (cmd);
	g_free (cmd);
#endif
	
	cmd = g_strdup_printf (ARCH_PREFIX DIS_CMD " %s %s", objdump_args, o_file);
	unused = system (cmd);
	g_free (cmd);
	
#ifndef HOST_WIN32
	unlink (o_file);
	unlink (as_file);
#endif
	g_free (o_file);
	g_free (as_file);
#endif
}

#else /* DISABLE_JIT */

void
mono_blockset_print (MonoCompile *cfg, MonoBitSet *set, const char *name, guint idom)
{
}

#endif /* DISABLE_JIT */
