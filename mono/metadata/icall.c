/*
 * icall.c:
 *
 * Authors:
 *   Dietmar Maurer (dietmar@ximian.com)
 *   Paolo Molaro (lupus@ximian.com)
 *	 Patrik Torstensson (patrik.torstensson@labs2.com)
 *
 * Copyright 2001-2003 Ximian, Inc (http://www.ximian.com)
 * Copyright 2004-2009 Novell, Inc (http://www.novell.com)
 * Copyright 2011-2012 Xamarin Inc (http://www.xamarin.com).
 */

#include <config.h>
#include <glib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#ifdef HAVE_ALLOCA_H
#include <alloca.h>
#endif
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#if defined (HOST_WIN32)
#include <stdlib.h>
#endif
#if defined (HAVE_WCHAR_H)
#include <wchar.h>
#endif

#include "mono/utils/mono-membar.h"
#include <mono/metadata/object.h>
#include <mono/metadata/threads.h>
#include <mono/metadata/threads-types.h>
#include <mono/metadata/threadpool.h>
#include <mono/metadata/monitor.h>
#include <mono/metadata/reflection.h>
#include <mono/metadata/assembly.h>
#include <mono/metadata/tabledefs.h>
#include <mono/metadata/exception.h>
#include <mono/metadata/file-io.h>
#include <mono/metadata/console-io.h>
#include <mono/metadata/socket-io.h>
#include <mono/metadata/mono-endian.h>
#include <mono/metadata/tokentype.h>
#include <mono/metadata/domain-internals.h>
#include <mono/metadata/metadata-internals.h>
#include <mono/metadata/class-internals.h>
#include <mono/metadata/marshal.h>
#include <mono/metadata/gc-internal.h>
#include <mono/metadata/mono-gc.h>
#include <mono/metadata/rand.h>
#include <mono/metadata/sysmath.h>
#include <mono/metadata/string-icalls.h>
#include <mono/metadata/debug-helpers.h>
#include <mono/metadata/process.h>
#include <mono/metadata/environment.h>
#include <mono/metadata/profiler-private.h>
#include <mono/metadata/locales.h>
#include <mono/metadata/filewatcher.h>
#include <mono/metadata/char-conversions.h>
#include <mono/metadata/security.h>
#include <mono/metadata/mono-config.h>
#include <mono/metadata/cil-coff.h>
#include <mono/metadata/number-formatter.h>
#include <mono/metadata/security-manager.h>
#include <mono/metadata/security-core-clr.h>
#include <mono/metadata/mono-perfcounters.h>
#include <mono/metadata/mono-debug.h>
#include <mono/metadata/mono-ptr-array.h>
#include <mono/metadata/verify-internals.h>
#include <mono/metadata/runtime.h>
#include <mono/io-layer/io-layer.h>
#include <mono/utils/strtod.h>
#include <mono/utils/monobitset.h>
#include <mono/utils/mono-time.h>
#include <mono/utils/mono-proclib.h>
#include <mono/utils/mono-string.h>
#include <mono/utils/mono-error-internals.h>
#include <mono/utils/mono-mmap.h>
#include <mono/utils/mono-io-portability.h>
#include <mono/utils/mono-digest.h>
#include <mono/utils/bsearch.h>

#if defined (HOST_WIN32)
#include <windows.h>
#include <shlobj.h>
#endif
#include "decimal.h"

extern MonoString* ves_icall_System_Environment_GetOSVersionString (void) MONO_INTERNAL;

ICALL_EXPORT MonoReflectionAssembly* ves_icall_System_Reflection_Assembly_GetCallingAssembly (void);

static MonoArray*
type_array_from_modifiers (MonoImage *image, MonoType *type, int optional);

static inline MonoBoolean
is_generic_parameter (MonoType *type)
{
	return !type->byref && (type->type == MONO_TYPE_VAR || type->type == MONO_TYPE_MVAR);
}

static void
mono_class_init_or_throw (MonoClass *klass)
{
	if (!mono_class_init (klass))
		mono_raise_exception (mono_class_get_exception_for_failure (klass));
}

/*
 * We expect a pointer to a char, not a string
 */
ICALL_EXPORT gboolean
mono_double_ParseImpl (char *ptr, double *result)
{
	gchar *endptr = NULL;
	*result = 0.0;

	MONO_ARCH_SAVE_REGS;

#ifdef __arm__
	if (*ptr)
		*result = strtod (ptr, &endptr);
#else
	if (*ptr){
		/* mono_strtod () is not thread-safe */
		EnterCriticalSection (&mono_strtod_mutex);
		*result = mono_strtod (ptr, &endptr);
		LeaveCriticalSection (&mono_strtod_mutex);
	}
#endif

	if (!*ptr || (endptr && *endptr))
		return FALSE;
	
	return TRUE;
}

ICALL_EXPORT MonoObject *
ves_icall_System_Array_GetValueImpl (MonoObject *this, guint32 pos)
{
	MonoClass *ac;
	MonoArray *ao;
	gint32 esize;
	gpointer *ea;

	MONO_ARCH_SAVE_REGS;

	ao = (MonoArray *)this;
	ac = (MonoClass *)ao->obj.vtable->klass;

	esize = mono_array_element_size (ac);
	ea = (gpointer*)((char*)ao->vector + (pos * esize));

	if (ac->element_class->valuetype)
		return mono_value_box (this->vtable->domain, ac->element_class, ea);
	else
		return *ea;
}

ICALL_EXPORT MonoObject *
ves_icall_System_Array_GetValue (MonoObject *this, MonoObject *idxs)
{
	MonoClass *ac, *ic;
	MonoArray *ao, *io;
	gint32 i, pos, *ind;

	MONO_ARCH_SAVE_REGS;

	MONO_CHECK_ARG_NULL (idxs);

	io = (MonoArray *)idxs;
	ic = (MonoClass *)io->obj.vtable->klass;
	
	ao = (MonoArray *)this;
	ac = (MonoClass *)ao->obj.vtable->klass;

	g_assert (ic->rank == 1);
	if (io->bounds != NULL || io->max_length !=  ac->rank)
		mono_raise_exception (mono_get_exception_argument (NULL, NULL));

	ind = (gint32 *)io->vector;

	if (ao->bounds == NULL) {
		if (*ind < 0 || *ind >= ao->max_length)
			mono_raise_exception (mono_get_exception_index_out_of_range ());

		return ves_icall_System_Array_GetValueImpl (this, *ind);
	}
	
	for (i = 0; i < ac->rank; i++)
		if ((ind [i] < ao->bounds [i].lower_bound) ||
		    (ind [i] >=  (mono_array_lower_bound_t)ao->bounds [i].length + ao->bounds [i].lower_bound))
			mono_raise_exception (mono_get_exception_index_out_of_range ());

	pos = ind [0] - ao->bounds [0].lower_bound;
	for (i = 1; i < ac->rank; i++)
		pos = pos*ao->bounds [i].length + ind [i] - 
			ao->bounds [i].lower_bound;

	return ves_icall_System_Array_GetValueImpl (this, pos);
}

ICALL_EXPORT void
ves_icall_System_Array_SetValueImpl (MonoArray *this, MonoObject *value, guint32 pos)
{
	MonoClass *ac, *vc, *ec;
	gint32 esize, vsize;
	gpointer *ea, *va;
	int et, vt;

	guint64 u64 = 0;
	gint64 i64 = 0;
	gdouble r64 = 0;

	MONO_ARCH_SAVE_REGS;

	if (value)
		vc = value->vtable->klass;
	else
		vc = NULL;

	ac = this->obj.vtable->klass;
	ec = ac->element_class;

	esize = mono_array_element_size (ac);
	ea = (gpointer*)((char*)this->vector + (pos * esize));
	va = (gpointer*)((char*)value + sizeof (MonoObject));

	if (mono_class_is_nullable (ec)) {
		mono_nullable_init ((guint8*)ea, value, ec);
		return;
	}

	if (!value) {
		mono_gc_bzero (ea, esize);
		return;
	}

#define NO_WIDENING_CONVERSION G_STMT_START{\
	mono_raise_exception (mono_get_exception_argument ( \
		"value", "not a widening conversion")); \
}G_STMT_END

#define CHECK_WIDENING_CONVERSION(extra) G_STMT_START{\
	if (esize < vsize + (extra)) \
		mono_raise_exception (mono_get_exception_argument ( \
			"value", "not a widening conversion")); \
}G_STMT_END

#define INVALID_CAST G_STMT_START{ \
		mono_get_runtime_callbacks ()->set_cast_details (vc, ec); \
	mono_raise_exception (mono_get_exception_invalid_cast ()); \
}G_STMT_END

	/* Check element (destination) type. */
	switch (ec->byval_arg.type) {
	case MONO_TYPE_STRING:
		switch (vc->byval_arg.type) {
		case MONO_TYPE_STRING:
			break;
		default:
			INVALID_CAST;
		}
		break;
	case MONO_TYPE_BOOLEAN:
		switch (vc->byval_arg.type) {
		case MONO_TYPE_BOOLEAN:
			break;
		case MONO_TYPE_CHAR:
		case MONO_TYPE_U1:
		case MONO_TYPE_U2:
		case MONO_TYPE_U4:
		case MONO_TYPE_U8:
		case MONO_TYPE_I1:
		case MONO_TYPE_I2:
		case MONO_TYPE_I4:
		case MONO_TYPE_I8:
		case MONO_TYPE_R4:
		case MONO_TYPE_R8:
			NO_WIDENING_CONVERSION;
		default:
			INVALID_CAST;
		}
		break;
	}

	if (!ec->valuetype) {
		if (!mono_object_isinst (value, ec))
			INVALID_CAST;
		mono_gc_wbarrier_set_arrayref (this, ea, (MonoObject*)value);
		return;
	}

	if (mono_object_isinst (value, ec)) {
		if (ec->has_references)
			mono_value_copy (ea, (char*)value + sizeof (MonoObject), ec);
		else
			mono_gc_memmove (ea, (char *)value + sizeof (MonoObject), esize);
		return;
	}

	if (!vc->valuetype)
		INVALID_CAST;

	vsize = mono_class_instance_size (vc) - sizeof (MonoObject);

	et = ec->byval_arg.type;
	if (et == MONO_TYPE_VALUETYPE && ec->byval_arg.data.klass->enumtype)
		et = mono_class_enum_basetype (ec->byval_arg.data.klass)->type;

	vt = vc->byval_arg.type;
	if (vt == MONO_TYPE_VALUETYPE && vc->byval_arg.data.klass->enumtype)
		vt = mono_class_enum_basetype (vc->byval_arg.data.klass)->type;

#define ASSIGN_UNSIGNED(etype) G_STMT_START{\
	switch (vt) { \
	case MONO_TYPE_U1: \
	case MONO_TYPE_U2: \
	case MONO_TYPE_U4: \
	case MONO_TYPE_U8: \
	case MONO_TYPE_CHAR: \
		CHECK_WIDENING_CONVERSION(0); \
		*(etype *) ea = (etype) u64; \
		return; \
	/* You can't assign a signed value to an unsigned array. */ \
	case MONO_TYPE_I1: \
	case MONO_TYPE_I2: \
	case MONO_TYPE_I4: \
	case MONO_TYPE_I8: \
	/* You can't assign a floating point number to an integer array. */ \
	case MONO_TYPE_R4: \
	case MONO_TYPE_R8: \
		NO_WIDENING_CONVERSION; \
	} \
}G_STMT_END

#define ASSIGN_SIGNED(etype) G_STMT_START{\
	switch (vt) { \
	case MONO_TYPE_I1: \
	case MONO_TYPE_I2: \
	case MONO_TYPE_I4: \
	case MONO_TYPE_I8: \
		CHECK_WIDENING_CONVERSION(0); \
		*(etype *) ea = (etype) i64; \
		return; \
	/* You can assign an unsigned value to a signed array if the array's */ \
	/* element size is larger than the value size. */ \
	case MONO_TYPE_U1: \
	case MONO_TYPE_U2: \
	case MONO_TYPE_U4: \
	case MONO_TYPE_U8: \
	case MONO_TYPE_CHAR: \
		CHECK_WIDENING_CONVERSION(1); \
		*(etype *) ea = (etype) u64; \
		return; \
	/* You can't assign a floating point number to an integer array. */ \
	case MONO_TYPE_R4: \
	case MONO_TYPE_R8: \
		NO_WIDENING_CONVERSION; \
	} \
}G_STMT_END

#define ASSIGN_REAL(etype) G_STMT_START{\
	switch (vt) { \
	case MONO_TYPE_R4: \
	case MONO_TYPE_R8: \
		CHECK_WIDENING_CONVERSION(0); \
		*(etype *) ea = (etype) r64; \
		return; \
	/* All integer values fit into a floating point array, so we don't */ \
	/* need to CHECK_WIDENING_CONVERSION here. */ \
	case MONO_TYPE_I1: \
	case MONO_TYPE_I2: \
	case MONO_TYPE_I4: \
	case MONO_TYPE_I8: \
		*(etype *) ea = (etype) i64; \
		return; \
	case MONO_TYPE_U1: \
	case MONO_TYPE_U2: \
	case MONO_TYPE_U4: \
	case MONO_TYPE_U8: \
	case MONO_TYPE_CHAR: \
		*(etype *) ea = (etype) u64; \
		return; \
	} \
}G_STMT_END

	switch (vt) {
	case MONO_TYPE_U1:
		u64 = *(guint8 *) va;
		break;
	case MONO_TYPE_U2:
		u64 = *(guint16 *) va;
		break;
	case MONO_TYPE_U4:
		u64 = *(guint32 *) va;
		break;
	case MONO_TYPE_U8:
		u64 = *(guint64 *) va;
		break;
	case MONO_TYPE_I1:
		i64 = *(gint8 *) va;
		break;
	case MONO_TYPE_I2:
		i64 = *(gint16 *) va;
		break;
	case MONO_TYPE_I4:
		i64 = *(gint32 *) va;
		break;
	case MONO_TYPE_I8:
		i64 = *(gint64 *) va;
		break;
	case MONO_TYPE_R4:
		r64 = *(gfloat *) va;
		break;
	case MONO_TYPE_R8:
		r64 = *(gdouble *) va;
		break;
	case MONO_TYPE_CHAR:
		u64 = *(guint16 *) va;
		break;
	case MONO_TYPE_BOOLEAN:
		/* Boolean is only compatible with itself. */
		switch (et) {
		case MONO_TYPE_CHAR:
		case MONO_TYPE_U1:
		case MONO_TYPE_U2:
		case MONO_TYPE_U4:
		case MONO_TYPE_U8:
		case MONO_TYPE_I1:
		case MONO_TYPE_I2:
		case MONO_TYPE_I4:
		case MONO_TYPE_I8:
		case MONO_TYPE_R4:
		case MONO_TYPE_R8:
			NO_WIDENING_CONVERSION;
		default:
			INVALID_CAST;
		}
		break;
	}

	/* If we can't do a direct copy, let's try a widening conversion. */
	switch (et) {
	case MONO_TYPE_CHAR:
		ASSIGN_UNSIGNED (guint16);
	case MONO_TYPE_U1:
		ASSIGN_UNSIGNED (guint8);
	case MONO_TYPE_U2:
		ASSIGN_UNSIGNED (guint16);
	case MONO_TYPE_U4:
		ASSIGN_UNSIGNED (guint32);
	case MONO_TYPE_U8:
		ASSIGN_UNSIGNED (guint64);
	case MONO_TYPE_I1:
		ASSIGN_SIGNED (gint8);
	case MONO_TYPE_I2:
		ASSIGN_SIGNED (gint16);
	case MONO_TYPE_I4:
		ASSIGN_SIGNED (gint32);
	case MONO_TYPE_I8:
		ASSIGN_SIGNED (gint64);
	case MONO_TYPE_R4:
		ASSIGN_REAL (gfloat);
	case MONO_TYPE_R8:
		ASSIGN_REAL (gdouble);
	}

	INVALID_CAST;
	/* Not reached, INVALID_CAST does not return. Just to avoid a compiler warning ... */
	return;

#undef INVALID_CAST
#undef NO_WIDENING_CONVERSION
#undef CHECK_WIDENING_CONVERSION
#undef ASSIGN_UNSIGNED
#undef ASSIGN_SIGNED
#undef ASSIGN_REAL
}

ICALL_EXPORT void 
ves_icall_System_Array_SetValue (MonoArray *this, MonoObject *value,
				 MonoArray *idxs)
{
	MonoClass *ac, *ic;
	gint32 i, pos, *ind;

	MONO_ARCH_SAVE_REGS;

	MONO_CHECK_ARG_NULL (idxs);

	ic = idxs->obj.vtable->klass;
	ac = this->obj.vtable->klass;

	g_assert (ic->rank == 1);
	if (idxs->bounds != NULL || idxs->max_length != ac->rank)
		mono_raise_exception (mono_get_exception_argument (NULL, NULL));

	ind = (gint32 *)idxs->vector;

	if (this->bounds == NULL) {
		if (*ind < 0 || *ind >= this->max_length)
			mono_raise_exception (mono_get_exception_index_out_of_range ());

		ves_icall_System_Array_SetValueImpl (this, value, *ind);
		return;
	}
	
	for (i = 0; i < ac->rank; i++)
		if ((ind [i] < this->bounds [i].lower_bound) ||
		    (ind [i] >= (mono_array_lower_bound_t)this->bounds [i].length + this->bounds [i].lower_bound))
			mono_raise_exception (mono_get_exception_index_out_of_range ());

	pos = ind [0] - this->bounds [0].lower_bound;
	for (i = 1; i < ac->rank; i++)
		pos = pos * this->bounds [i].length + ind [i] - 
			this->bounds [i].lower_bound;

	ves_icall_System_Array_SetValueImpl (this, value, pos);
}

ICALL_EXPORT MonoArray *
ves_icall_System_Array_CreateInstanceImpl (MonoReflectionType *type, MonoArray *lengths, MonoArray *bounds)
{
	MonoClass *aklass, *klass;
	MonoArray *array;
	uintptr_t *sizes, i;
	gboolean bounded = FALSE;

	MONO_ARCH_SAVE_REGS;

	MONO_CHECK_ARG_NULL (type);
	MONO_CHECK_ARG_NULL (lengths);

	MONO_CHECK_ARG (lengths, mono_array_length (lengths) > 0);
	if (bounds)
		MONO_CHECK_ARG (bounds, mono_array_length (lengths) == mono_array_length (bounds));

	for (i = 0; i < mono_array_length (lengths); i++)
		if (mono_array_get (lengths, gint32, i) < 0)
			mono_raise_exception (mono_get_exception_argument_out_of_range (NULL));

	klass = mono_class_from_mono_type (type->type);
	mono_class_init_or_throw (klass);

	if (bounds && (mono_array_length (bounds) == 1) && (mono_array_get (bounds, gint32, 0) != 0))
		/* vectors are not the same as one dimensional arrays with no-zero bounds */
		bounded = TRUE;
	else
		bounded = FALSE;

	aklass = mono_bounded_array_class_get (klass, mono_array_length (lengths), bounded);

	sizes = alloca (aklass->rank * sizeof(intptr_t) * 2);
	for (i = 0; i < aklass->rank; ++i) {
		sizes [i] = mono_array_get (lengths, guint32, i);
		if (bounds)
			sizes [i + aklass->rank] = mono_array_get (bounds, gint32, i);
		else
			sizes [i + aklass->rank] = 0;
	}

	array = mono_array_new_full (mono_object_domain (type), aklass, sizes, (intptr_t*)sizes + aklass->rank);

	return array;
}

ICALL_EXPORT MonoArray *
ves_icall_System_Array_CreateInstanceImpl64 (MonoReflectionType *type, MonoArray *lengths, MonoArray *bounds)
{
	MonoClass *aklass, *klass;
	MonoArray *array;
	uintptr_t *sizes, i;
	gboolean bounded = FALSE;

	MONO_ARCH_SAVE_REGS;

	MONO_CHECK_ARG_NULL (type);
	MONO_CHECK_ARG_NULL (lengths);

	MONO_CHECK_ARG (lengths, mono_array_length (lengths) > 0);
	if (bounds)
		MONO_CHECK_ARG (bounds, mono_array_length (lengths) == mono_array_length (bounds));

	for (i = 0; i < mono_array_length (lengths); i++) 
		if ((mono_array_get (lengths, gint64, i) < 0) ||
		    (mono_array_get (lengths, gint64, i) > MONO_ARRAY_MAX_INDEX))
			mono_raise_exception (mono_get_exception_argument_out_of_range (NULL));

	klass = mono_class_from_mono_type (type->type);
	mono_class_init_or_throw (klass);

	if (bounds && (mono_array_length (bounds) == 1) && (mono_array_get (bounds, gint64, 0) != 0))
		/* vectors are not the same as one dimensional arrays with no-zero bounds */
		bounded = TRUE;
	else
		bounded = FALSE;

	aklass = mono_bounded_array_class_get (klass, mono_array_length (lengths), bounded);

	sizes = alloca (aklass->rank * sizeof(intptr_t) * 2);
	for (i = 0; i < aklass->rank; ++i) {
		sizes [i] = mono_array_get (lengths, guint64, i);
		if (bounds)
			sizes [i + aklass->rank] = (mono_array_size_t) mono_array_get (bounds, guint64, i);
		else
			sizes [i + aklass->rank] = 0;
	}

	array = mono_array_new_full (mono_object_domain (type), aklass, sizes, (intptr_t*)sizes + aklass->rank);

	return array;
}

ICALL_EXPORT gint32 
ves_icall_System_Array_GetRank (MonoObject *this)
{
	MONO_ARCH_SAVE_REGS;

	return this->vtable->klass->rank;
}

ICALL_EXPORT gint32
ves_icall_System_Array_GetLength (MonoArray *this, gint32 dimension)
{
	gint32 rank = ((MonoObject *)this)->vtable->klass->rank;
	uintptr_t length;

	MONO_ARCH_SAVE_REGS;

	if ((dimension < 0) || (dimension >= rank))
		mono_raise_exception (mono_get_exception_index_out_of_range ());
	
	if (this->bounds == NULL)
		length = this->max_length;
	else
		length = this->bounds [dimension].length;

#ifdef MONO_BIG_ARRAYS
	if (length > G_MAXINT32)
	        mono_raise_exception (mono_get_exception_overflow ());
#endif
	return length;
}

ICALL_EXPORT gint64
ves_icall_System_Array_GetLongLength (MonoArray *this, gint32 dimension)
{
	gint32 rank = ((MonoObject *)this)->vtable->klass->rank;

	MONO_ARCH_SAVE_REGS;

	if ((dimension < 0) || (dimension >= rank))
		mono_raise_exception (mono_get_exception_index_out_of_range ());
	
	if (this->bounds == NULL)
 		return this->max_length;
 	
 	return this->bounds [dimension].length;
}

ICALL_EXPORT gint32
ves_icall_System_Array_GetLowerBound (MonoArray *this, gint32 dimension)
{
	gint32 rank = ((MonoObject *)this)->vtable->klass->rank;

	MONO_ARCH_SAVE_REGS;

	if ((dimension < 0) || (dimension >= rank))
		mono_raise_exception (mono_get_exception_index_out_of_range ());
	
	if (this->bounds == NULL)
		return 0;
	
	return this->bounds [dimension].lower_bound;
}

ICALL_EXPORT void
ves_icall_System_Array_ClearInternal (MonoArray *arr, int idx, int length)
{
	int sz = mono_array_element_size (mono_object_class (arr));
	mono_gc_bzero (mono_array_addr_with_size (arr, sz, idx), length * sz);
}

ICALL_EXPORT gboolean
ves_icall_System_Array_FastCopy (MonoArray *source, int source_idx, MonoArray* dest, int dest_idx, int length)
{
	int element_size;
	void * dest_addr;
	void * source_addr;
	MonoClass *src_class;
	MonoClass *dest_class;

	MONO_ARCH_SAVE_REGS;

	if (source->obj.vtable->klass->rank != dest->obj.vtable->klass->rank)
		return FALSE;

	if (source->bounds || dest->bounds)
		return FALSE;

	/* there's no integer overflow since mono_array_length returns an unsigned integer */
	if ((dest_idx + length > mono_array_length (dest)) ||
		(source_idx + length > mono_array_length (source)))
		return FALSE;

	src_class = source->obj.vtable->klass->element_class;
	dest_class = dest->obj.vtable->klass->element_class;

	/*
	 * Handle common cases.
	 */

	/* Case1: object[] -> valuetype[] (ArrayList::ToArray) */
	if (src_class == mono_defaults.object_class && dest_class->valuetype) {
		// FIXME: This is racy
		return FALSE;
		/*
		  int i;
		int has_refs = dest_class->has_references;
		for (i = source_idx; i < source_idx + length; ++i) {
			MonoObject *elem = mono_array_get (source, MonoObject*, i);
			if (elem && !mono_object_isinst (elem, dest_class))
				return FALSE;
		}

		element_size = mono_array_element_size (dest->obj.vtable->klass);
		memset (mono_array_addr_with_size (dest, element_size, dest_idx), 0, element_size * length);
		for (i = 0; i < length; ++i) {
			MonoObject *elem = mono_array_get (source, MonoObject*, source_idx + i);
			void *addr = mono_array_addr_with_size (dest, element_size, dest_idx + i);
			if (!elem)
				continue;
			if (has_refs)
				mono_value_copy (addr, (char *)elem + sizeof (MonoObject), dest_class);
			else
				memcpy (addr, (char *)elem + sizeof (MonoObject), element_size);
		}
		return TRUE;
		*/
	}

	/* Check if we're copying a char[] <==> (u)short[] */
	if (src_class != dest_class) {
		if (dest_class->valuetype || dest_class->enumtype || src_class->valuetype || src_class->enumtype)
			return FALSE;

		if (mono_class_is_subclass_of (src_class, dest_class, FALSE))
			;
		/* Case2: object[] -> reftype[] (ArrayList::ToArray) */
		else if (mono_class_is_subclass_of (dest_class, src_class, FALSE)) {
			// FIXME: This is racy
			return FALSE;
			/*
			  int i;
			for (i = source_idx; i < source_idx + length; ++i) {
				MonoObject *elem = mono_array_get (source, MonoObject*, i);
				if (elem && !mono_object_isinst (elem, dest_class))
					return FALSE;
			}
			*/
		} else
			return FALSE;
	}

	if (dest_class->valuetype) {
		element_size = mono_array_element_size (source->obj.vtable->klass);
		source_addr = mono_array_addr_with_size (source, element_size, source_idx);
		if (dest_class->has_references) {
			mono_value_copy_array (dest, dest_idx, source_addr, length);
		} else {
			dest_addr = mono_array_addr_with_size (dest, element_size, dest_idx);
			mono_gc_memmove (dest_addr, source_addr, element_size * length);
		}
	} else {
		mono_array_memcpy_refs (dest, dest_idx, source, source_idx, length);
	}

	return TRUE;
}

ICALL_EXPORT void
ves_icall_System_Array_GetGenericValueImpl (MonoObject *this, guint32 pos, gpointer value)
{
	MonoClass *ac;
	MonoArray *ao;
	gint32 esize;
	gpointer *ea;

	MONO_ARCH_SAVE_REGS;

	ao = (MonoArray *)this;
	ac = (MonoClass *)ao->obj.vtable->klass;

	esize = mono_array_element_size (ac);
	ea = (gpointer*)((char*)ao->vector + (pos * esize));

	mono_gc_memmove (value, ea, esize);
}

ICALL_EXPORT void
ves_icall_System_Array_SetGenericValueImpl (MonoObject *this, guint32 pos, gpointer value)
{
	MonoClass *ac, *ec;
	MonoArray *ao;
	gint32 esize;
	gpointer *ea;

	MONO_ARCH_SAVE_REGS;

	ao = (MonoArray *)this;
	ac = (MonoClass *)ao->obj.vtable->klass;
	ec = ac->element_class;

	esize = mono_array_element_size (ac);
	ea = (gpointer*)((char*)ao->vector + (pos * esize));

	if (MONO_TYPE_IS_REFERENCE (&ec->byval_arg)) {
		g_assert (esize == sizeof (gpointer));
		mono_gc_wbarrier_generic_store (ea, *(gpointer*)value);
	} else {
		g_assert (ec->inited);
		g_assert (esize == mono_class_value_size (ec, NULL));
		if (ec->has_references)
			mono_gc_wbarrier_value_copy (ea, value, 1, ec);
		else
			mono_gc_memmove (ea, value, esize);
	}
}

ICALL_EXPORT void
ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_InitializeArray (MonoArray *array, MonoClassField *field_handle)
{
	MonoClass *klass = array->obj.vtable->klass;
	guint32 size = mono_array_element_size (klass);
	MonoType *type = mono_type_get_underlying_type (&klass->element_class->byval_arg);
	int align;
	const char *field_data;

	if (MONO_TYPE_IS_REFERENCE (type) || type->type == MONO_TYPE_VALUETYPE) {
		MonoException *exc = mono_get_exception_argument("array",
			"Cannot initialize array of non-primitive type.");
		mono_raise_exception (exc);
	}

	if (!(field_handle->type->attrs & FIELD_ATTRIBUTE_HAS_FIELD_RVA)) {
		MonoException *exc = mono_get_exception_argument("field_handle",
			"Field doesn't have an RVA");
		mono_raise_exception (exc);
	}

	size *= array->max_length;
	field_data = mono_field_get_data (field_handle);

	if (size > mono_type_size (field_handle->type, &align)) {
		MonoException *exc = mono_get_exception_argument("field_handle",
			"Field not large enough to fill array");
		mono_raise_exception (exc);
	}

#if G_BYTE_ORDER != G_LITTLE_ENDIAN
#define SWAP(n) {\
	guint ## n *data = (guint ## n *) mono_array_addr (array, char, 0); \
	guint ## n *src = (guint ## n *) field_data; \
	guint ## n *end = (guint ## n *)((char*)src + size); \
\
	for (; src < end; data++, src++) { \
		*data = read ## n (src); \
	} \
}

	/* printf ("Initialize array with elements of %s type\n", klass->element_class->name); */

	switch (type->type) {
	case MONO_TYPE_CHAR:
	case MONO_TYPE_I2:
	case MONO_TYPE_U2:
		SWAP (16);
		break;
	case MONO_TYPE_I4:
	case MONO_TYPE_U4:
	case MONO_TYPE_R4:
		SWAP (32);
		break;
	case MONO_TYPE_I8:
	case MONO_TYPE_U8:
	case MONO_TYPE_R8:
		SWAP (64);
		break;
	default:
		memcpy (mono_array_addr (array, char, 0), field_data, size);
		break;
	}
#else
	memcpy (mono_array_addr (array, char, 0), field_data, size);
#endif
}

ICALL_EXPORT gint
ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData (void)
{
	MONO_ARCH_SAVE_REGS;

	return offsetof (MonoString, chars);
}

ICALL_EXPORT MonoObject *
ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetObjectValue (MonoObject *obj)
{
	MONO_ARCH_SAVE_REGS;

	if ((obj == NULL) || (! (obj->vtable->klass->valuetype)))
		return obj;
	else
		return mono_object_clone (obj);
}

ICALL_EXPORT void
ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunClassConstructor (MonoType *handle)
{
	MonoClass *klass;
	MonoVTable *vtable;

	MONO_CHECK_ARG_NULL (handle);

	klass = mono_class_from_mono_type (handle);
	MONO_CHECK_ARG (handle, klass);

	vtable = mono_class_vtable_full (mono_domain_get (), klass, TRUE);

	/* This will call the type constructor */
	mono_runtime_class_init (vtable);
}

ICALL_EXPORT void
ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunModuleConstructor (MonoImage *image)
{
	MONO_ARCH_SAVE_REGS;

	mono_image_check_for_module_cctor (image);
	if (image->has_module_cctor) {
		MonoClass *module_klass = mono_class_get (image, MONO_TOKEN_TYPE_DEF | 1);
		/*It's fine to raise the exception here*/
		mono_runtime_class_init (mono_class_vtable_full (mono_domain_get (), module_klass, TRUE));
	}
}

ICALL_EXPORT MonoBoolean
ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_SufficientExecutionStack (void)
{
	guint8 *stack_addr;
	guint8 *current;
	size_t stack_size;
	/* later make this configurable and per-arch */
	int min_size = 4096 * 4 * sizeof (void*);
	mono_thread_get_stack_bounds (&stack_addr, &stack_size);
	/* if we have no info we are optimistic and assume there is enough room */
	if (!stack_addr)
		return TRUE;
	current = (guint8 *)&stack_addr;
	if (current > stack_addr) {
		if ((current - stack_addr) < min_size)
			return FALSE;
	} else {
		if (current - (stack_addr - stack_size) < min_size)
			return FALSE;
	}
	return TRUE;
}

ICALL_EXPORT MonoObject *
ves_icall_System_Object_MemberwiseClone (MonoObject *this)
{
	MONO_ARCH_SAVE_REGS;

	return mono_object_clone (this);
}

ICALL_EXPORT gint32
ves_icall_System_ValueType_InternalGetHashCode (MonoObject *this, MonoArray **fields)
{
	MonoClass *klass;
	MonoObject **values = NULL;
	MonoObject *o;
	int count = 0;
	gint32 result = 0;
	MonoClassField* field;
	gpointer iter;

	MONO_ARCH_SAVE_REGS;

	klass = mono_object_class (this);

	if (mono_class_num_fields (klass) == 0)
		return mono_object_hash (this);

	/*
	 * Compute the starting value of the hashcode for fields of primitive
	 * types, and return the remaining fields in an array to the managed side.
	 * This way, we can avoid costly reflection operations in managed code.
	 */
	iter = NULL;
	while ((field = mono_class_get_fields (klass, &iter))) {
		if (field->type->attrs & FIELD_ATTRIBUTE_STATIC)
			continue;
		if (mono_field_is_deleted (field))
			continue;
		/* FIXME: Add more types */
		switch (field->type->type) {
		case MONO_TYPE_I4:
			result ^= *(gint32*)((guint8*)this + field->offset);
			break;
		case MONO_TYPE_STRING: {
			MonoString *s;
			s = *(MonoString**)((guint8*)this + field->offset);
			if (s != NULL)
				result ^= mono_string_hash (s);
			break;
		}
		default:
			if (!values)
				values = g_newa (MonoObject*, mono_class_num_fields (klass));
			o = mono_field_get_value_object (mono_object_domain (this), field, this);
			values [count++] = o;
		}
	}

	if (values) {
		int i;
		mono_gc_wbarrier_generic_store (fields, (MonoObject*) mono_array_new (mono_domain_get (), mono_defaults.object_class, count));
		for (i = 0; i < count; ++i)
			mono_array_setref (*fields, i, values [i]);
	} else {
		*fields = NULL;
	}
	return result;
}

ICALL_EXPORT MonoBoolean
ves_icall_System_ValueType_Equals (MonoObject *this, MonoObject *that, MonoArray **fields)
{
	MonoClass *klass;
	MonoObject **values = NULL;
	MonoObject *o;
	MonoClassField* field;
	gpointer iter;
	int count = 0;

	MONO_ARCH_SAVE_REGS;

	MONO_CHECK_ARG_NULL (that);

	if (this->vtable != that->vtable)
		return FALSE;

	klass = mono_object_class (this);

	if (klass->enumtype && mono_class_enum_basetype (klass) && mono_class_enum_basetype (klass)->type == MONO_TYPE_I4)
		return (*(gint32*)((guint8*)this + sizeof (MonoObject)) == *(gint32*)((guint8*)that + sizeof (MonoObject)));

	/*
	 * Do the comparison for fields of primitive type and return a result if
	 * possible. Otherwise, return the remaining fields in an array to the 
	 * managed side. This way, we can avoid costly reflection operations in 
	 * managed code.
	 */
	*fields = NULL;
	iter = NULL;
	while ((field = mono_class_get_fields (klass, &iter))) {
		if (field->type->attrs & FIELD_ATTRIBUTE_STATIC)
			continue;
		if (mono_field_is_deleted (field))
			continue;
		/* FIXME: Add more types */
		switch (field->type->type) {
		case MONO_TYPE_U1:
		case MONO_TYPE_I1:
		case MONO_TYPE_BOOLEAN:
			if (*((guint8*)this + field->offset) != *((guint8*)that + field->offset))
				return FALSE;
			break;
		case MONO_TYPE_U2:
		case MONO_TYPE_I2:
		case MONO_TYPE_CHAR:
			if (*(gint16*)((guint8*)this + field->offset) != *(gint16*)((guint8*)that + field->offset))
				return FALSE;
			break;
		case MONO_TYPE_U4:
		case MONO_TYPE_I4:
			if (*(gint32*)((guint8*)this + field->offset) != *(gint32*)((guint8*)that + field->offset))
				return FALSE;
			break;
		case MONO_TYPE_U8:
		case MONO_TYPE_I8:
			if (*(gint64*)((guint8*)this + field->offset) != *(gint64*)((guint8*)that + field->offset))
				return FALSE;
			break;
		case MONO_TYPE_R4:
			if (*(float*)((guint8*)this + field->offset) != *(float*)((guint8*)that + field->offset))
				return FALSE;
			break;
		case MONO_TYPE_R8:
			if (*(double*)((guint8*)this + field->offset) != *(double*)((guint8*)that + field->offset))
				return FALSE;
			break;


		case MONO_TYPE_STRING: {
			MonoString *s1, *s2;
			guint32 s1len, s2len;
			s1 = *(MonoString**)((guint8*)this + field->offset);
			s2 = *(MonoString**)((guint8*)that + field->offset);
			if (s1 == s2)
				break;
			if ((s1 == NULL) || (s2 == NULL))
				return FALSE;
			s1len = mono_string_length (s1);
			s2len = mono_string_length (s2);
			if (s1len != s2len)
				return FALSE;

			if (memcmp (mono_string_chars (s1), mono_string_chars (s2), s1len * sizeof (gunichar2)) != 0)
				return FALSE;
			break;
		}
		default:
			if (!values)
				values = g_newa (MonoObject*, mono_class_num_fields (klass) * 2);
			o = mono_field_get_value_object (mono_object_domain (this), field, this);
			values [count++] = o;
			o = mono_field_get_value_object (mono_object_domain (this), field, that);
			values [count++] = o;
		}

		if (klass->enumtype)
			/* enums only have one non-static field */
			break;
	}

	if (values) {
		int i;
		mono_gc_wbarrier_generic_store (fields, (MonoObject*) mono_array_new (mono_domain_get (), mono_defaults.object_class, count));
		for (i = 0; i < count; ++i)
			mono_array_setref (*fields, i, values [i]);
		return FALSE;
	} else {
		return TRUE;
	}
}

ICALL_EXPORT MonoReflectionType *
ves_icall_System_Object_GetType (MonoObject *obj)
{
	MONO_ARCH_SAVE_REGS;

#ifndef DISABLE_REMOTING
	if (obj->vtable->klass == mono_defaults.transparent_proxy_class)
		return mono_type_get_object (mono_object_domain (obj), &((MonoTransparentProxy*)obj)->remote_class->proxy_class->byval_arg);
	else
#endif
		return mono_type_get_object (mono_object_domain (obj), &obj->vtable->klass->byval_arg);
}

ICALL_EXPORT void
mono_type_type_from_obj (MonoReflectionType *mtype, MonoObject *obj)
{
	MONO_ARCH_SAVE_REGS;

	mtype->type = &obj->vtable->klass->byval_arg;
	g_assert (mtype->type->type);
}

ICALL_EXPORT gint32
ves_icall_ModuleBuilder_getToken (MonoReflectionModuleBuilder *mb, MonoObject *obj, gboolean create_open_instance)
{
	MONO_ARCH_SAVE_REGS;
	
	MONO_CHECK_ARG_NULL (obj);
	
	return mono_image_create_token (mb->dynamic_image, obj, create_open_instance, TRUE);
}

ICALL_EXPORT gint32
ves_icall_ModuleBuilder_getMethodToken (MonoReflectionModuleBuilder *mb,
					MonoReflectionMethod *method,
					MonoArray *opt_param_types)
{
	MONO_ARCH_SAVE_REGS;

	MONO_CHECK_ARG_NULL (method);
	
	return mono_image_create_method_token (
		mb->dynamic_image, (MonoObject *) method, opt_param_types);
}

ICALL_EXPORT void
ves_icall_ModuleBuilder_WriteToFile (MonoReflectionModuleBuilder *mb, HANDLE file)
{
	MONO_ARCH_SAVE_REGS;

	mono_image_create_pefile (mb, file);
}

ICALL_EXPORT void
ves_icall_ModuleBuilder_build_metadata (MonoReflectionModuleBuilder *mb)
{
	MONO_ARCH_SAVE_REGS;

	mono_image_build_metadata (mb);
}

ICALL_EXPORT void
ves_icall_ModuleBuilder_RegisterToken (MonoReflectionModuleBuilder *mb, MonoObject *obj, guint32 token)
{
	MONO_ARCH_SAVE_REGS;

	mono_image_register_token (mb->dynamic_image, token, obj);
}

static gboolean
get_caller (MonoMethod *m, gint32 no, gint32 ilo, gboolean managed, gpointer data)
{
	MonoMethod **dest = data;

	/* skip unmanaged frames */
	if (!managed)
		return FALSE;

	if (m == *dest) {
		*dest = NULL;
		return FALSE;
	}
	if (!(*dest)) {
		*dest = m;
		return TRUE;
	}
	return FALSE;
}

static gboolean
get_executing (MonoMethod *m, gint32 no, gint32 ilo, gboolean managed, gpointer data)
{
	MonoMethod **dest = data;

	/* skip unmanaged frames */
	if (!managed)
		return FALSE;

	if (!(*dest)) {
		if (!strcmp (m->klass->name_space, "System.Reflection"))
			return FALSE;
		*dest = m;
		return TRUE;
	}
	return FALSE;
}

static gboolean
get_caller_no_reflection (MonoMethod *m, gint32 no, gint32 ilo, gboolean managed, gpointer data)
{
	MonoMethod **dest = data;

	/* skip unmanaged frames */
	if (!managed)
		return FALSE;

	if (m->wrapper_type != MONO_WRAPPER_NONE)
		return FALSE;

	if (m->klass->image == mono_defaults.corlib && !strcmp (m->klass->name_space, "System.Reflection"))
		return FALSE;

	if (m == *dest) {
		*dest = NULL;
		return FALSE;
	}
	if (!(*dest)) {
		*dest = m;
		return TRUE;
	}
	return FALSE;
}

static MonoReflectionType *
type_from_name (const char *str, MonoBoolean ignoreCase)
{
	MonoType *type = NULL;
	MonoAssembly *assembly = NULL;
	MonoTypeNameParse info;
	char *temp_str = g_strdup (str);
	gboolean type_resolve = FALSE;

	MONO_ARCH_SAVE_REGS;

	/* mono_reflection_parse_type() mangles the string */
	if (!mono_reflection_parse_type (temp_str, &info)) {
		mono_reflection_free_type_info (&info);
		g_free (temp_str);
		return NULL;
	}

	if (info.assembly.name) {
		assembly = mono_assembly_load (&info.assembly, NULL, NULL);
	} else {
		MonoMethod *m = mono_method_get_last_managed ();
		MonoMethod *dest = m;

		mono_stack_walk_no_il (get_caller_no_reflection, &dest);
		if (!dest)
			dest = m;

		/*
		 * FIXME: mono_method_get_last_managed() sometimes returns NULL, thus
		 *        causing ves_icall_System_Reflection_Assembly_GetCallingAssembly()
		 *        to crash.  This only seems to happen in some strange remoting
		 *        scenarios and I was unable to figure out what's happening there.
		 *        Dec 10, 2005 - Martin.
		 */

		if (dest) {
			assembly = dest->klass->image->assembly;
			type_resolve = TRUE;
		} else {
			g_warning (G_STRLOC);
		}
	}

	if (assembly) {
		/* When loading from the current assembly, AppDomain.TypeResolve will not be called yet */
		type = mono_reflection_get_type (assembly->image, &info, ignoreCase, &type_resolve);
	}

	if (!info.assembly.name && !type) /* try mscorlib */
		type = mono_reflection_get_type (NULL, &info, ignoreCase, &type_resolve);

	if (assembly && !type && type_resolve) {
		type_resolve = FALSE; /* This will invoke TypeResolve if not done in the first 'if' */
		type = mono_reflection_get_type (assembly->image, &info, ignoreCase, &type_resolve);
	}

	mono_reflection_free_type_info (&info);
	g_free (temp_str);

	if (!type) 
		return NULL;

	return mono_type_get_object (mono_domain_get (), type);
}

#ifdef UNUSED
MonoReflectionType *
mono_type_get (const char *str)
{
	char *copy = g_strdup (str);
	MonoReflectionType *type = type_from_name (copy, FALSE);

	g_free (copy);
	return type;
}
#endif

ICALL_EXPORT MonoReflectionType*
ves_icall_type_from_name (MonoString *name,
			  MonoBoolean throwOnError,
			  MonoBoolean ignoreCase)
{
	char *str = mono_string_to_utf8 (name);
	MonoReflectionType *type;

	type = type_from_name (str, ignoreCase);
	g_free (str);
	if (type == NULL){
		MonoException *e = NULL;
		
		if (throwOnError)
			e = mono_get_exception_type_load (name, NULL);

		mono_loader_clear_error ();
		if (e != NULL)
			mono_raise_exception (e);
	}
	
	return type;
}


ICALL_EXPORT MonoReflectionType*
ves_icall_type_from_handle (MonoType *handle)
{
	MonoDomain *domain = mono_domain_get (); 

	MONO_ARCH_SAVE_REGS;

	return mono_type_get_object (domain, handle);
}

ICALL_EXPORT MonoBoolean
ves_icall_System_Type_EqualsInternal (MonoReflectionType *type, MonoReflectionType *c)
{
	MONO_ARCH_SAVE_REGS;

	if (c && type->type && c->type)
		return mono_metadata_type_equal (type->type, c->type);
	else
		return (type == c) ? TRUE : FALSE;
}

/* System.TypeCode */
typedef enum {
	TYPECODE_EMPTY,
	TYPECODE_OBJECT,
	TYPECODE_DBNULL,
	TYPECODE_BOOLEAN,
	TYPECODE_CHAR,
	TYPECODE_SBYTE,
	TYPECODE_BYTE,
	TYPECODE_INT16,
	TYPECODE_UINT16,
	TYPECODE_INT32,
	TYPECODE_UINT32,
	TYPECODE_INT64,
	TYPECODE_UINT64,
	TYPECODE_SINGLE,
	TYPECODE_DOUBLE,
	TYPECODE_DECIMAL,
	TYPECODE_DATETIME,
	TYPECODE_STRING = 18
} TypeCode;

ICALL_EXPORT guint32
ves_icall_type_GetTypeCodeInternal (MonoReflectionType *type)
{
	int t = type->type->type;

	MONO_ARCH_SAVE_REGS;

	if (type->type->byref)
		return TYPECODE_OBJECT;

handle_enum:
	switch (t) {
	case MONO_TYPE_VOID:
		return TYPECODE_OBJECT;
	case MONO_TYPE_BOOLEAN:
		return TYPECODE_BOOLEAN;
	case MONO_TYPE_U1:
		return TYPECODE_BYTE;
	case MONO_TYPE_I1:
		return TYPECODE_SBYTE;
	case MONO_TYPE_U2:
		return TYPECODE_UINT16;
	case MONO_TYPE_I2:
		return TYPECODE_INT16;
	case MONO_TYPE_CHAR:
		return TYPECODE_CHAR;
	case MONO_TYPE_PTR:
	case MONO_TYPE_U:
	case MONO_TYPE_I:
		return TYPECODE_OBJECT;
	case MONO_TYPE_U4:
		return TYPECODE_UINT32;
	case MONO_TYPE_I4:
		return TYPECODE_INT32;
	case MONO_TYPE_U8:
		return TYPECODE_UINT64;
	case MONO_TYPE_I8:
		return TYPECODE_INT64;
	case MONO_TYPE_R4:
		return TYPECODE_SINGLE;
	case MONO_TYPE_R8:
		return TYPECODE_DOUBLE;
	case MONO_TYPE_VALUETYPE: {
		MonoClass *klass = type->type->data.klass;
		
		if (klass->enumtype) {
			t = mono_class_enum_basetype (klass)->type;
			goto handle_enum;
		} else if (mono_is_corlib_image (klass->image)) {
			if (strcmp (klass->name_space, "System") == 0) {
				if (strcmp (klass->name, "Decimal") == 0)
					return TYPECODE_DECIMAL;
				else if (strcmp (klass->name, "DateTime") == 0)
					return TYPECODE_DATETIME;
			}
		}
		return TYPECODE_OBJECT;
	}
	case MONO_TYPE_STRING:
		return TYPECODE_STRING;
	case MONO_TYPE_SZARRAY:
	case MONO_TYPE_ARRAY:
	case MONO_TYPE_OBJECT:
	case MONO_TYPE_VAR:
	case MONO_TYPE_MVAR:
	case MONO_TYPE_TYPEDBYREF:
		return TYPECODE_OBJECT;
	case MONO_TYPE_CLASS:
		{
			MonoClass *klass =  type->type->data.klass;
			if (klass->image == mono_defaults.corlib && strcmp (klass->name_space, "System") == 0) {
				if (strcmp (klass->name, "DBNull") == 0)
					return TYPECODE_DBNULL;
			}
		}
		return TYPECODE_OBJECT;
	case MONO_TYPE_GENERICINST:
		return TYPECODE_OBJECT;
	default:
		g_error ("type 0x%02x not handled in GetTypeCode()", t);
	}
	return 0;
}

ICALL_EXPORT guint32
ves_icall_type_is_subtype_of (MonoReflectionType *type, MonoReflectionType *c, MonoBoolean check_interfaces)
{
	MonoDomain *domain; 
	MonoClass *klass;
	MonoClass *klassc;

	MONO_ARCH_SAVE_REGS;

	g_assert (type != NULL);
	
	domain = ((MonoObject *)type)->vtable->domain;

	if (!c) /* FIXME: dont know what do do here */
		return 0;

	klass = mono_class_from_mono_type (type->type);
	klassc = mono_class_from_mono_type (c->type);

	/* Interface check requires a more complex setup so we
	 * only do for them. Otherwise we simply avoid mono_class_init.
	 */
	if (check_interfaces) {
		mono_class_init_or_throw (klass);
		mono_class_init_or_throw (klassc);
	} else if (!klass->supertypes || !klassc->supertypes) {
		mono_loader_lock ();
		mono_class_setup_supertypes (klass);
		mono_class_setup_supertypes (klassc);
		mono_loader_unlock ();
	}

	if (type->type->byref)
		return klassc == mono_defaults.object_class;

	return mono_class_is_subclass_of (klass, klassc, check_interfaces);
}

static gboolean
mono_type_is_primitive (MonoType *type)
{
	return (type->type >= MONO_TYPE_BOOLEAN && type->type <= MONO_TYPE_R8) ||
			type-> type == MONO_TYPE_I || type->type == MONO_TYPE_U;
}

static MonoType*
mono_type_get_underlying_type_ignore_byref (MonoType *type)
{
	if (type->type == MONO_TYPE_VALUETYPE && type->data.klass->enumtype)
		return mono_class_enum_basetype (type->data.klass);
	if (type->type == MONO_TYPE_GENERICINST && type->data.generic_class->container_class->enumtype)
		return mono_class_enum_basetype (type->data.generic_class->container_class);
	return type;
}

ICALL_EXPORT guint32
ves_icall_type_is_assignable_from (MonoReflectionType *type, MonoReflectionType *c)
{
	MonoDomain *domain; 
	MonoClass *klass;
	MonoClass *klassc;

	MONO_ARCH_SAVE_REGS;

	g_assert (type != NULL);
	
	domain = ((MonoObject *)type)->vtable->domain;

	klass = mono_class_from_mono_type (type->type);
	klassc = mono_class_from_mono_type (c->type);

	mono_class_init_or_throw (klass);
	mono_class_init_or_throw (klassc);

	if (type->type->byref ^ c->type->byref)
		return FALSE;

	if (type->type->byref) {
		MonoType *t = mono_type_get_underlying_type_ignore_byref (type->type);
		MonoType *ot = mono_type_get_underlying_type_ignore_byref (c->type);

		klass = mono_class_from_mono_type (t);
		klassc = mono_class_from_mono_type (ot);

		if (mono_type_is_primitive (t)) {
			return mono_type_is_primitive (ot) && klass->instance_size == klassc->instance_size;
		} else if (t->type == MONO_TYPE_VAR || t->type == MONO_TYPE_MVAR) {
			return t->type == ot->type && t->data.generic_param->num == ot->data.generic_param->num;
		} else if (t->type == MONO_TYPE_PTR || t->type == MONO_TYPE_FNPTR) {
			return t->type == ot->type;
		} else {
			 if (ot->type == MONO_TYPE_VAR || ot->type == MONO_TYPE_MVAR)
				 return FALSE;

			 if (klass->valuetype)
				return klass == klassc;
			return klass->valuetype == klassc->valuetype;
		}
	}
	return mono_class_is_assignable_from (klass, klassc);
}

ICALL_EXPORT guint32
ves_icall_type_IsInstanceOfType (MonoReflectionType *type, MonoObject *obj)
{
	MonoClass *klass = mono_class_from_mono_type (type->type);
	mono_class_init_or_throw (klass);
	return mono_object_isinst (obj, klass) != NULL;
}

ICALL_EXPORT guint32
ves_icall_get_attributes (MonoReflectionType *type)
{
	MonoClass *klass = mono_class_from_mono_type (type->type);
	return klass->flags;
}

ICALL_EXPORT MonoReflectionMarshalAsAttribute*
ves_icall_System_Reflection_FieldInfo_get_marshal_info (MonoReflectionField *field)
{
	MonoClass *klass = field->field->parent;
	MonoMarshalType *info;
	int i;

	if (klass->generic_container ||
	    (klass->generic_class && klass->generic_class->context.class_inst->is_open))
		return NULL;

	info = mono_marshal_load_type_info (klass);

	for (i = 0; i < info->num_fields; ++i) {
		if (info->fields [i].field == field->field) {
			if (!info->fields [i].mspec)
				return NULL;
			else
				return mono_reflection_marshal_as_attribute_from_marshal_spec (field->object.vtable->domain, klass, info->fields [i].mspec);
		}
	}

	return NULL;
}

ICALL_EXPORT MonoReflectionField*
ves_icall_System_Reflection_FieldInfo_internal_from_handle_type (MonoClassField *handle, MonoType *type)
{
	gboolean found = FALSE;
	MonoClass *klass;
	MonoClass *k;

	g_assert (handle);

	if (!type) {
		klass = handle->parent;
	} else {
		klass = mono_class_from_mono_type (type);

		/* Check that the field belongs to the class */
		for (k = klass; k; k = k->parent) {
			if (k == handle->parent) {
				found = TRUE;
				break;
			}
		}

		if (!found)
			/* The managed code will throw the exception */
			return NULL;
	}

	return mono_field_get_object (mono_domain_get (), klass, handle);
}

ICALL_EXPORT MonoArray*
ves_icall_System_Reflection_FieldInfo_GetTypeModifiers (MonoReflectionField *field, MonoBoolean optional)
{
	MonoError error;
	MonoType *type = mono_field_get_type_checked (field->field, &error);
	if (!mono_error_ok (&error))
		mono_error_raise_exception (&error);

	return type_array_from_modifiers (field->field->parent->image, type, optional);
}

ICALL_EXPORT int
vell_icall_get_method_attributes (MonoMethod *method)
{
	return method->flags;
}

ICALL_EXPORT void
ves_icall_get_method_info (MonoMethod *method, MonoMethodInfo *info)
{
	MonoError error;
	MonoDomain *domain = mono_domain_get ();
	MonoMethodSignature* sig;
	MONO_ARCH_SAVE_REGS;

	sig = mono_method_signature_checked (method, &error);
	if (!mono_error_ok (&error))
		mono_error_raise_exception (&error);


	MONO_STRUCT_SETREF (info, parent, mono_type_get_object (domain, &method->klass->byval_arg));
	MONO_STRUCT_SETREF (info, ret, mono_type_get_object (domain, sig->ret));
	info->attrs = method->flags;
	info->implattrs = method->iflags;
	if (sig->call_convention == MONO_CALL_DEFAULT)
		info->callconv = sig->sentinelpos >= 0 ? 2 : 1;
	else {
		if (sig->call_convention == MONO_CALL_VARARG || sig->sentinelpos >= 0)
			info->callconv = 2;
		else
			info->callconv = 1;
	}
	info->callconv |= (sig->hasthis << 5) | (sig->explicit_this << 6); 
}

ICALL_EXPORT MonoArray*
ves_icall_get_parameter_info (MonoMethod *method, MonoReflectionMethod *member)
{
	MonoDomain *domain = mono_domain_get (); 

	return mono_param_get_objects_internal (domain, method, member->reftype ? mono_class_from_mono_type (member->reftype->type) : NULL);
}

ICALL_EXPORT MonoReflectionMarshalAsAttribute*
ves_icall_System_MonoMethodInfo_get_retval_marshal (MonoMethod *method)
{
	MonoDomain *domain = mono_domain_get (); 
	MonoReflectionMarshalAsAttribute* res = NULL;
	MonoMarshalSpec **mspecs;
	int i;

	mspecs = g_new (MonoMarshalSpec*, mono_method_signature (method)->param_count + 1);
	mono_method_get_marshal_info (method, mspecs);

	if (mspecs [0])
		res = mono_reflection_marshal_as_attribute_from_marshal_spec (domain, method->klass, mspecs [0]);
		
	for (i = mono_method_signature (method)->param_count; i >= 0; i--)
		if (mspecs [i])
			mono_metadata_free_marshal_spec (mspecs [i]);
	g_free (mspecs);

	return res;
}

ICALL_EXPORT gint32
ves_icall_MonoField_GetFieldOffset (MonoReflectionField *field)
{
	MonoClass *parent = field->field->parent;
	if (!parent->size_inited)
		mono_class_init (parent);

	return field->field->offset - sizeof (MonoObject);
}

ICALL_EXPORT MonoReflectionType*
ves_icall_MonoField_GetParentType (MonoReflectionField *field, MonoBoolean declaring)
{
	MonoClass *parent;
	MONO_ARCH_SAVE_REGS;

	parent = declaring? field->field->parent: field->klass;

	return mono_type_get_object (mono_object_domain (field), &parent->byval_arg);
}

ICALL_EXPORT MonoObject *
ves_icall_MonoField_GetValueInternal (MonoReflectionField *field, MonoObject *obj)
{	
	MonoClass *fklass = field->klass;
	MonoClassField *cf = field->field;
	MonoDomain *domain = mono_object_domain (field);

	if (fklass->image->assembly->ref_only)
		mono_raise_exception (mono_get_exception_invalid_operation (
					"It is illegal to get the value on a field on a type loaded using the ReflectionOnly methods."));

	if (mono_security_core_clr_enabled ())
		mono_security_core_clr_ensure_reflection_access_field (cf);

	return mono_field_get_value_object (domain, cf, obj);
}

ICALL_EXPORT void
ves_icall_MonoField_SetValueInternal (MonoReflectionField *field, MonoObject *obj, MonoObject *value)
{
	MonoError error;
	MonoClassField *cf = field->field;
	MonoType *type;
	gchar *v;

	MONO_ARCH_SAVE_REGS;

	if (field->klass->image->assembly->ref_only)
		mono_raise_exception (mono_get_exception_invalid_operation (
					"It is illegal to set the value on a field on a type loaded using the ReflectionOnly methods."));

	if (mono_security_core_clr_enabled ())
		mono_security_core_clr_ensure_reflection_access_field (cf);

	type = mono_field_get_type_checked (cf, &error);
	if (!mono_error_ok (&error))
		mono_error_raise_exception (&error);

	v = (gchar *) value;
	if (!type->byref) {
		switch (type->type) {
		case MONO_TYPE_U1:
		case MONO_TYPE_I1:
		case MONO_TYPE_BOOLEAN:
		case MONO_TYPE_U2:
		case MONO_TYPE_I2:
		case MONO_TYPE_CHAR:
		case MONO_TYPE_U:
		case MONO_TYPE_I:
		case MONO_TYPE_U4:
		case MONO_TYPE_I4:
		case MONO_TYPE_R4:
		case MONO_TYPE_U8:
		case MONO_TYPE_I8:
		case MONO_TYPE_R8:
		case MONO_TYPE_VALUETYPE:
		case MONO_TYPE_PTR:
			if (v != NULL)
				v += sizeof (MonoObject);
			break;
		case MONO_TYPE_STRING:
		case MONO_TYPE_OBJECT:
		case MONO_TYPE_CLASS:
		case MONO_TYPE_ARRAY:
		case MONO_TYPE_SZARRAY:
			/* Do nothing */
			break;
		case MONO_TYPE_GENERICINST: {
			MonoGenericClass *gclass = type->data.generic_class;
			g_assert (!gclass->context.class_inst->is_open);

			if (mono_class_is_nullable (mono_class_from_mono_type (type))) {
				MonoClass *nklass = mono_class_from_mono_type (type);
				MonoObject *nullable;

				/* 
				 * Convert the boxed vtype into a Nullable structure.
				 * This is complicated by the fact that Nullables have
				 * a variable structure.
				 */
				nullable = mono_object_new (mono_domain_get (), nklass);

				mono_nullable_init (mono_object_unbox (nullable), value, nklass);

				v = mono_object_unbox (nullable);
			}
			else 
				if (gclass->container_class->valuetype && (v != NULL))
					v += sizeof (MonoObject);
			break;
		}
		default:
			g_error ("type 0x%x not handled in "
				 "ves_icall_FieldInfo_SetValueInternal", type->type);
			return;
		}
	}

	if (type->attrs & FIELD_ATTRIBUTE_STATIC) {
		MonoVTable *vtable = mono_class_vtable_full (mono_object_domain (field), cf->parent, TRUE);
		if (!vtable->initialized)
			mono_runtime_class_init (vtable);
		mono_field_static_set_value (vtable, cf, v);
	} else {
		mono_field_set_value (obj, cf, v);
	}
}

ICALL_EXPORT MonoObject *
ves_icall_MonoField_GetRawConstantValue (MonoReflectionField *this)
{	
	MonoObject *o = NULL;
	MonoClassField *field = this->field;
	MonoClass *klass;
	MonoDomain *domain = mono_object_domain (this); 
	gchar *v;
	MonoTypeEnum def_type;
	const char *def_value;
	MonoType *t;
	MonoError error;

	MONO_ARCH_SAVE_REGS;
	
	mono_class_init (field->parent);

	t = mono_field_get_type_checked (field, &error);
	if (!mono_error_ok (&error))
		mono_error_raise_exception (&error);

	if (!(t->attrs & FIELD_ATTRIBUTE_HAS_DEFAULT))
		mono_raise_exception (mono_get_exception_invalid_operation (NULL));

	if (field->parent->image->dynamic) {
		/* FIXME: */
		g_assert_not_reached ();
	}

	def_value = mono_class_get_field_default_value (field, &def_type);
	if (!def_value) /*FIXME, maybe we should try to raise TLE if field->parent is broken */
		mono_raise_exception (mono_get_exception_invalid_operation (NULL));

	/*FIXME unify this with reflection.c:mono_get_object_from_blob*/
	switch (def_type) {
	case MONO_TYPE_U1:
	case MONO_TYPE_I1:
	case MONO_TYPE_BOOLEAN:
	case MONO_TYPE_U2:
	case MONO_TYPE_I2:
	case MONO_TYPE_CHAR:
	case MONO_TYPE_U:
	case MONO_TYPE_I:
	case MONO_TYPE_U4:
	case MONO_TYPE_I4:
	case MONO_TYPE_R4:
	case MONO_TYPE_U8:
	case MONO_TYPE_I8:
	case MONO_TYPE_R8: {
		MonoType *t;

		/* boxed value type */
		t = g_new0 (MonoType, 1);
		t->type = def_type;
		klass = mono_class_from_mono_type (t);
		g_free (t);
		o = mono_object_new (domain, klass);
		v = ((gchar *) o) + sizeof (MonoObject);
		mono_get_constant_value_from_blob (domain, def_type, def_value, v);
		break;
	}
	case MONO_TYPE_STRING:
	case MONO_TYPE_CLASS:
		mono_get_constant_value_from_blob (domain, def_type, def_value, &o);
		break;
	default:
		g_assert_not_reached ();
	}

	return o;
}

ICALL_EXPORT MonoReflectionType*
ves_icall_MonoField_ResolveType (MonoReflectionField *ref_field)
{
	MonoError error;
	MonoClassField *field = ref_field->field;
	MonoType *type = mono_field_get_type_checked (field, &error);
	if (!mono_error_ok (&error))
		mono_error_raise_exception (&error);
	return mono_type_get_object (mono_object_domain (ref_field), type);
}

ICALL_EXPORT MonoReflectionType*
ves_icall_MonoGenericMethod_get_ReflectedType (MonoReflectionGenericMethod *rmethod)
{
	MonoMethod *method = rmethod->method.method;

	return mono_type_get_object (mono_object_domain (rmethod), &method->klass->byval_arg);
}

/* From MonoProperty.cs */
typedef enum {
	PInfo_Attributes = 1,
	PInfo_GetMethod  = 1 << 1,
	PInfo_SetMethod  = 1 << 2,
	PInfo_ReflectedType = 1 << 3,
	PInfo_DeclaringType = 1 << 4,
	PInfo_Name = 1 << 5
} PInfo;

ICALL_EXPORT void
ves_icall_get_property_info (MonoReflectionProperty *property, MonoPropertyInfo *info, PInfo req_info)
{
	MonoDomain *domain = mono_object_domain (property); 

	MONO_ARCH_SAVE_REGS;

	if ((req_info & PInfo_ReflectedType) != 0)
		MONO_STRUCT_SETREF (info, parent, mono_type_get_object (domain, &property->klass->byval_arg));
	if ((req_info & PInfo_DeclaringType) != 0)
		MONO_STRUCT_SETREF (info, declaring_type, mono_type_get_object (domain, &property->property->parent->byval_arg));

	if ((req_info & PInfo_Name) != 0)
		MONO_STRUCT_SETREF (info, name, mono_string_new (domain, property->property->name));

	if ((req_info & PInfo_Attributes) != 0)
		info->attrs = property->property->attrs;

	if ((req_info & PInfo_GetMethod) != 0)
		MONO_STRUCT_SETREF (info, get, property->property->get ?
							mono_method_get_object (domain, property->property->get, property->klass): NULL);
	
	if ((req_info & PInfo_SetMethod) != 0)
		MONO_STRUCT_SETREF (info, set, property->property->set ?
							mono_method_get_object (domain, property->property->set, property->klass): NULL);
	/* 
	 * There may be other methods defined for properties, though, it seems they are not exposed 
	 * in the reflection API 
	 */
}

ICALL_EXPORT void
ves_icall_get_event_info (MonoReflectionMonoEvent *event, MonoEventInfo *info)
{
	MonoDomain *domain = mono_object_domain (event); 

	MONO_ARCH_SAVE_REGS;

	MONO_STRUCT_SETREF (info, reflected_type, mono_type_get_object (domain, &event->klass->byval_arg));
	MONO_STRUCT_SETREF (info, declaring_type, mono_type_get_object (domain, &event->event->parent->byval_arg));

	MONO_STRUCT_SETREF (info, name, mono_string_new (domain, event->event->name));
	info->attrs = event->event->attrs;
	MONO_STRUCT_SETREF (info, add_method, event->event->add ? mono_method_get_object (domain, event->event->add, NULL): NULL);
	MONO_STRUCT_SETREF (info, remove_method, event->event->remove ? mono_method_get_object (domain, event->event->remove, NULL): NULL);
	MONO_STRUCT_SETREF (info, raise_method, event->event->raise ? mono_method_get_object (domain, event->event->raise, NULL): NULL);

#ifndef MONO_SMALL_CONFIG
	if (event->event->other) {
		int i, n = 0;
		while (event->event->other [n])
			n++;
		MONO_STRUCT_SETREF (info, other_methods, mono_array_new (domain, mono_defaults.method_info_class, n));

		for (i = 0; i < n; i++)
			mono_array_setref (info->other_methods, i, mono_method_get_object (domain, event->event->other [i], NULL));
	}		
#endif
}

static void
collect_interfaces (MonoClass *klass, GHashTable *ifaces, MonoError *error)
{
	int i;
	MonoClass *ic;

	mono_class_setup_interfaces (klass, error);
	if (!mono_error_ok (error))
		return;

	for (i = 0; i < klass->interface_count; i++) {
		ic = klass->interfaces [i];
		g_hash_table_insert (ifaces, ic, ic);

		collect_interfaces (ic, ifaces, error);
		if (!mono_error_ok (error))
			return;
	}
}

typedef struct {
	MonoArray *iface_array;
	MonoGenericContext *context;
	MonoError *error;
	MonoDomain *domain;
	int next_idx;
} FillIfaceArrayData;

static void
fill_iface_array (gpointer key, gpointer value, gpointer user_data)
{
	FillIfaceArrayData *data = user_data;
	MonoClass *ic = key;
	MonoType *ret = &ic->byval_arg, *inflated = NULL;

	if (!mono_error_ok (data->error))
		return;

	if (data->context && ic->generic_class && ic->generic_class->context.class_inst->is_open) {
		inflated = ret = mono_class_inflate_generic_type_checked (ret, data->context, data->error);
		if (!mono_error_ok (data->error))
			return;
	}

	mono_array_setref (data->iface_array, data->next_idx++, mono_type_get_object (data->domain, ret));

	if (inflated)
		mono_metadata_free_type (inflated);
}

ICALL_EXPORT MonoArray*
ves_icall_Type_GetInterfaces (MonoReflectionType* type)
{
	MonoError error;
	MonoClass *class = mono_class_from_mono_type (type->type);
	MonoClass *parent;
	FillIfaceArrayData data = { 0 };
	int len;

	GHashTable *iface_hash = g_hash_table_new (NULL, NULL);

	if (class->generic_class && class->generic_class->context.class_inst->is_open) {
		data.context = mono_class_get_context (class);
		class = class->generic_class->container_class;
	}

	for (parent = class; parent; parent = parent->parent) {
		mono_class_setup_interfaces (parent, &error);
		if (!mono_error_ok (&error))
			goto fail;
		collect_interfaces (parent, iface_hash, &error);
		if (!mono_error_ok (&error))
			goto fail;
	}

	data.error = &error;
	data.domain = mono_object_domain (type);

	len = g_hash_table_size (iface_hash);
	if (len == 0) {
		g_hash_table_destroy (iface_hash);
		if (!data.domain->empty_types)
			data.domain->empty_types = mono_array_new_cached (data.domain, mono_defaults.monotype_class, 0);
		return data.domain->empty_types;
	}

	data.iface_array = mono_array_new_cached (data.domain, mono_defaults.monotype_class, len);
	g_hash_table_foreach (iface_hash, fill_iface_array, &data);
	if (!mono_error_ok (&error))
		goto fail;

	g_hash_table_destroy (iface_hash);
	return data.iface_array;

fail:
	g_hash_table_destroy (iface_hash);
	mono_error_raise_exception (&error);
	return NULL;
}

ICALL_EXPORT void
ves_icall_Type_GetInterfaceMapData (MonoReflectionType *type, MonoReflectionType *iface, MonoArray **targets, MonoArray **methods)
{
	gboolean variance_used;
	MonoClass *class = mono_class_from_mono_type (type->type);
	MonoClass *iclass = mono_class_from_mono_type (iface->type);
	MonoReflectionMethod *member;
	MonoMethod* method;
	gpointer iter;
	int i = 0, len, ioffset;
	MonoDomain *domain;

	MONO_ARCH_SAVE_REGS;
	mono_class_init_or_throw (class);
	mono_class_init_or_throw (iclass);

	mono_class_setup_vtable (class);

	ioffset = mono_class_interface_offset_with_variance (class, iclass, &variance_used);
	if (ioffset == -1)
		return;

	len = mono_class_num_methods (iclass);
	domain = mono_object_domain (type);
	mono_gc_wbarrier_generic_store (targets, (MonoObject*) mono_array_new (domain, mono_defaults.method_info_class, len));
	mono_gc_wbarrier_generic_store (methods, (MonoObject*) mono_array_new (domain, mono_defaults.method_info_class, len));
	iter = NULL;
	while ((method = mono_class_get_methods (iclass, &iter))) {
		member = mono_method_get_object (domain, method, iclass);
		mono_array_setref (*methods, i, member);
		member = mono_method_get_object (domain, class->vtable [i + ioffset], class);
		mono_array_setref (*targets, i, member);
		
		i ++;
	}
}

ICALL_EXPORT void
ves_icall_Type_GetPacking (MonoReflectionType *type, guint32 *packing, guint32 *size)
{
	MonoClass *klass = mono_class_from_mono_type (type->type);
	mono_class_init_or_throw (klass);

	if (klass->image->dynamic) {
		MonoReflectionTypeBuilder *tb = (MonoReflectionTypeBuilder*)type;
		*packing = tb->packing_size;
		*size = tb->class_size;
	} else {
		mono_metadata_packing_from_typedef (klass->image, klass->type_token, packing, size);
	}
}

ICALL_EXPORT MonoReflectionType*
ves_icall_MonoType_GetElementType (MonoReflectionType *type)
{
	MonoClass *class;

	MONO_ARCH_SAVE_REGS;

	if (!type->type->byref && type->type->type == MONO_TYPE_SZARRAY)
		return mono_type_get_object (mono_object_domain (type), &type->type->data.klass->byval_arg);

	class = mono_class_from_mono_type (type->type);
	mono_class_init_or_throw (class);

	// GetElementType should only return a type for:
	// Array Pointer PassedByRef
	if (type->type->byref)
		return mono_type_get_object (mono_object_domain (type), &class->byval_arg);
	else if (class->element_class && MONO_CLASS_IS_ARRAY (class))
		return mono_type_get_object (mono_object_domain (type), &class->element_class->byval_arg);
	else if (class->element_class && type->type->type == MONO_TYPE_PTR)
		return mono_type_get_object (mono_object_domain (type), &class->element_class->byval_arg);
	else
		return NULL;
}

ICALL_EXPORT MonoReflectionType*
ves_icall_get_type_parent (MonoReflectionType *type)
{
	MonoClass *class = mono_class_from_mono_type (type->type);
	return class->parent ? mono_type_get_object (mono_object_domain (type), &class->parent->byval_arg): NULL;
}

ICALL_EXPORT MonoBoolean
ves_icall_type_ispointer (MonoReflectionType *type)
{
	MONO_ARCH_SAVE_REGS;

	return type->type->type == MONO_TYPE_PTR;
}

ICALL_EXPORT MonoBoolean
ves_icall_type_isprimitive (MonoReflectionType *type)
{
	MONO_ARCH_SAVE_REGS;

	return (!type->type->byref && (((type->type->type >= MONO_TYPE_BOOLEAN) && (type->type->type <= MONO_TYPE_R8)) || (type->type->type == MONO_TYPE_I) || (type->type->type == MONO_TYPE_U)));
}

ICALL_EXPORT MonoBoolean
ves_icall_type_isbyref (MonoReflectionType *type)
{
	MONO_ARCH_SAVE_REGS;

	return type->type->byref;
}

ICALL_EXPORT MonoBoolean
ves_icall_type_iscomobject (MonoReflectionType *type)
{
	MonoClass *klass = mono_class_from_mono_type (type->type);
	mono_class_init_or_throw (klass);

	return mono_class_is_com_object (klass);
}

ICALL_EXPORT MonoReflectionModule*
ves_icall_MonoType_get_Module (MonoReflectionType *type)
{
	MonoClass *class = mono_class_from_mono_type (type->type);
	return mono_module_get_object (mono_object_domain (type), class->image);
}

ICALL_EXPORT MonoReflectionAssembly*
ves_icall_MonoType_get_Assembly (MonoReflectionType *type)
{
	MonoDomain *domain = mono_domain_get (); 
	MonoClass *class = mono_class_from_mono_type (type->type);
	return mono_assembly_get_object (domain, class->image->assembly);
}

ICALL_EXPORT MonoReflectionType*
ves_icall_MonoType_get_DeclaringType (MonoReflectionType *type)
{
	MonoDomain *domain = mono_domain_get ();
	MonoClass *class;

	MONO_ARCH_SAVE_REGS;

	if (type->type->byref)
		return NULL;
	if (type->type->type == MONO_TYPE_VAR)
		class = mono_type_get_generic_param_owner (type->type)->owner.klass;
	else if (type->type->type == MONO_TYPE_MVAR)
		class = mono_type_get_generic_param_owner (type->type)->owner.method->klass;
	else
		class = mono_class_from_mono_type (type->type)->nested_in;

	return class ? mono_type_get_object (domain, &class->byval_arg) : NULL;
}

ICALL_EXPORT MonoString*
ves_icall_MonoType_get_Name (MonoReflectionType *type)
{
	MonoDomain *domain = mono_domain_get (); 
	MonoClass *class = mono_class_from_mono_type (type->type);

	if (type->type->byref) {
		char *n = g_strdup_printf ("%s&", class->name);
		MonoString *res = mono_string_new (domain, n);

		g_free (n);

		return res;
	} else {
		return mono_string_new (domain, class->name);
	}
}

ICALL_EXPORT MonoString*
ves_icall_MonoType_get_Namespace (MonoReflectionType *type)
{
	MonoDomain *domain = mono_domain_get (); 
	MonoClass *class = mono_class_from_mono_type (type->type);

	while (class->nested_in)
		class = class->nested_in;

	if (class->name_space [0] == '\0')
		return NULL;
	else
		return mono_string_new (domain, class->name_space);
}

ICALL_EXPORT gint32
ves_icall_MonoType_GetArrayRank (MonoReflectionType *type)
{
	MonoClass *class;

	if (type->type->type != MONO_TYPE_ARRAY && type->type->type != MONO_TYPE_SZARRAY)
		mono_raise_exception (mono_get_exception_argument ("type", "Type must be an array type"));

	class = mono_class_from_mono_type (type->type);

	return class->rank;
}

ICALL_EXPORT MonoArray*
ves_icall_MonoType_GetGenericArguments (MonoReflectionType *type)
{
	MonoArray *res;
	MonoClass *klass, *pklass;
	MonoDomain *domain = mono_object_domain (type);
	MonoVTable *array_vtable = mono_class_vtable_full (domain, mono_array_class_get_cached (mono_defaults.systemtype_class, 1), TRUE);
	int i;
	MONO_ARCH_SAVE_REGS;

	klass = mono_class_from_mono_type (type->type);

	if (klass->generic_container) {
		MonoGenericContainer *container = klass->generic_container;
		res = mono_array_new_specific (array_vtable, container->type_argc);
		for (i = 0; i < container->type_argc; ++i) {
			pklass = mono_class_from_generic_parameter (mono_generic_container_get_param (container, i), klass->image, FALSE);
			mono_array_setref (res, i, mono_type_get_object (domain, &pklass->byval_arg));
		}
	} else if (klass->generic_class) {
		MonoGenericInst *inst = klass->generic_class->context.class_inst;
		res = mono_array_new_specific (array_vtable, inst->type_argc);
		for (i = 0; i < inst->type_argc; ++i)
			mono_array_setref (res, i, mono_type_get_object (domain, inst->type_argv [i]));
	} else {
		res = mono_array_new_specific (array_vtable, 0);
	}
	return res;
}

ICALL_EXPORT gboolean
ves_icall_Type_get_IsGenericTypeDefinition (MonoReflectionType *type)
{
	MonoClass *klass;
	MONO_ARCH_SAVE_REGS;

	if (!IS_MONOTYPE (type))
		return FALSE;

	if (type->type->byref)
		return FALSE;

	klass = mono_class_from_mono_type (type->type);
	return klass->generic_container != NULL;
}

ICALL_EXPORT MonoReflectionType*
ves_icall_Type_GetGenericTypeDefinition_impl (MonoReflectionType *type)
{
	MonoClass *klass;
	MONO_ARCH_SAVE_REGS;

	if (type->type->byref)
		return NULL;

	klass = mono_class_from_mono_type (type->type);

	if (klass->generic_container) {
		return type; /* check this one */
	}
	if (klass->generic_class) {
		MonoClass *generic_class = klass->generic_class->container_class;
		gpointer tb;

		tb = mono_class_get_ref_info (generic_class);

		if (generic_class->wastypebuilder && tb)
			return tb;
		else
			return mono_type_get_object (mono_object_domain (type), &generic_class->byval_arg);
	}
	return NULL;
}

ICALL_EXPORT MonoReflectionType*
ves_icall_Type_MakeGenericType (MonoReflectionType *type, MonoArray *type_array)
{
	MonoClass *class;
	MonoType *geninst, **types;
	int i, count;

	g_assert (IS_MONOTYPE (type));
	mono_class_init_or_throw (mono_class_from_mono_type (type->type));

	count = mono_array_length (type_array);
	types = g_new0 (MonoType *, count);

	for (i = 0; i < count; i++) {
		MonoReflectionType *t = mono_array_get (type_array, gpointer, i);
		types [i] = t->type;
	}

	geninst = mono_reflection_bind_generic_parameters (type, count, types);
	g_free (types);
	if (!geninst)
		return NULL;

	class = mono_class_from_mono_type (geninst);

	/*we might inflate to the GTD*/
	if (class->generic_class && !mono_verifier_class_is_valid_generic_instantiation (class))
		mono_raise_exception (mono_get_exception_argument ("typeArguments", "Invalid generic arguments"));

	return mono_type_get_object (mono_object_domain (type), geninst);
}

ICALL_EXPORT gboolean
ves_icall_Type_get_IsGenericInstance (MonoReflectionType *type)
{
	MonoClass *klass;
	MONO_ARCH_SAVE_REGS;

	if (type->type->byref)
		return FALSE;

	klass = mono_class_from_mono_type (type->type);

	return klass->generic_class != NULL;
}

ICALL_EXPORT gboolean
ves_icall_Type_get_IsGenericType (MonoReflectionType *type)
{
	MonoClass *klass;
	MONO_ARCH_SAVE_REGS;

	if (!IS_MONOTYPE (type))
		return FALSE;

	if (type->type->byref)
		return FALSE;

	klass = mono_class_from_mono_type (type->type);
	return klass->generic_class != NULL || klass->generic_container != NULL;
}

ICALL_EXPORT gint32
ves_icall_Type_GetGenericParameterPosition (MonoReflectionType *type)
{
	MONO_ARCH_SAVE_REGS;

	if (!IS_MONOTYPE (type))
		return -1;

	if (is_generic_parameter (type->type))
		return mono_type_get_generic_param_num (type->type);
	return -1;
}

ICALL_EXPORT GenericParameterAttributes
ves_icall_Type_GetGenericParameterAttributes (MonoReflectionType *type)
{
	MONO_ARCH_SAVE_REGS;

	g_assert (IS_MONOTYPE (type));
	g_assert (is_generic_parameter (type->type));
	return mono_generic_param_info (type->type->data.generic_param)->flags;
}

ICALL_EXPORT MonoArray *
ves_icall_Type_GetGenericParameterConstraints (MonoReflectionType *type)
{
	MonoGenericParamInfo *param_info;
	MonoDomain *domain;
	MonoClass **ptr;
	MonoArray *res;
	int i, count;

	MONO_ARCH_SAVE_REGS;

	g_assert (IS_MONOTYPE (type));

	domain = mono_object_domain (type);
	param_info = mono_generic_param_info (type->type->data.generic_param);
	for (count = 0, ptr = param_info->constraints; ptr && *ptr; ptr++, count++)
		;

	res = mono_array_new (domain, mono_defaults.monotype_class, count);
	for (i = 0; i < count; i++)
		mono_array_setref (res, i, mono_type_get_object (domain, &param_info->constraints [i]->byval_arg));


	return res;
}

ICALL_EXPORT MonoBoolean
ves_icall_MonoType_get_IsGenericParameter (MonoReflectionType *type)
{
	MONO_ARCH_SAVE_REGS;
	return is_generic_parameter (type->type);
}

ICALL_EXPORT MonoBoolean
ves_icall_TypeBuilder_get_IsGenericParameter (MonoReflectionTypeBuilder *tb)
{
	MONO_ARCH_SAVE_REGS;
	return is_generic_parameter (tb->type.type);
}

ICALL_EXPORT void
ves_icall_EnumBuilder_setup_enum_type (MonoReflectionType *enumtype,
									   MonoReflectionType *t)
{
	enumtype->type = t->type;
}

ICALL_EXPORT MonoReflectionMethod*
ves_icall_MonoType_GetCorrespondingInflatedMethod (MonoReflectionType *type, 
                                                   MonoReflectionMethod* generic)
{
	MonoDomain *domain; 
	MonoClass *klass;
	MonoMethod *method;
	gpointer iter;
		
	MONO_ARCH_SAVE_REGS;

	domain = ((MonoObject *)type)->vtable->domain;

	klass = mono_class_from_mono_type (type->type);
	mono_class_init_or_throw (klass);

	iter = NULL;
	while ((method = mono_class_get_methods (klass, &iter))) {
                if (method->token == generic->method->token)
                        return mono_method_get_object (domain, method, klass);
        }

        return NULL;
}



ICALL_EXPORT MonoReflectionMethod *
ves_icall_MonoType_get_DeclaringMethod (MonoReflectionType *ref_type)
{
	MonoMethod *method;
	MonoType *type = ref_type->type;

	MONO_ARCH_SAVE_REGS;

	if (type->byref || (type->type != MONO_TYPE_MVAR && type->type != MONO_TYPE_VAR))
		mono_raise_exception (mono_get_exception_invalid_operation ("DeclaringMethod can only be used on generic arguments"));
	if (type->type == MONO_TYPE_VAR)
		return NULL;

	method = mono_type_get_generic_param_owner (type)->owner.method;
	g_assert (method);
	return mono_method_get_object (mono_object_domain (ref_type), method, method->klass);
}

ICALL_EXPORT MonoReflectionDllImportAttribute*
ves_icall_MonoMethod_GetDllImportAttribute (MonoMethod *method)
{
	static MonoClass *DllImportAttributeClass = NULL;
	MonoDomain *domain = mono_domain_get ();
	MonoReflectionDllImportAttribute *attr;
	MonoImage *image = method->klass->image;
	MonoMethodPInvoke *piinfo = (MonoMethodPInvoke *)method;
	MonoTableInfo *tables = image->tables;
	MonoTableInfo *im = &tables [MONO_TABLE_IMPLMAP];
	MonoTableInfo *mr = &tables [MONO_TABLE_MODULEREF];
	guint32 im_cols [MONO_IMPLMAP_SIZE];
	guint32 scope_token;
	const char *import = NULL;
	const char *scope = NULL;
	guint32 flags;

	if (!(method->flags & METHOD_ATTRIBUTE_PINVOKE_IMPL))
		return NULL;

	if (!DllImportAttributeClass) {
		DllImportAttributeClass = 
			mono_class_from_name (mono_defaults.corlib,
								  "System.Runtime.InteropServices", "DllImportAttribute");
		g_assert (DllImportAttributeClass);
	}
														
	if (method->klass->image->dynamic) {
		MonoReflectionMethodAux *method_aux = 
			g_hash_table_lookup (
									  ((MonoDynamicImage*)method->klass->image)->method_aux_hash, method);
		if (method_aux) {
			import = method_aux->dllentry;
			scope = method_aux->dll;
		}

		if (!import || !scope) {
			mono_raise_exception (mono_get_exception_argument ("method", "System.Reflection.Emit method with invalid pinvoke information"));
			return NULL;
		}
	}
	else {
		if (piinfo->implmap_idx) {
			mono_metadata_decode_row (im, piinfo->implmap_idx - 1, im_cols, MONO_IMPLMAP_SIZE);
			
			piinfo->piflags = im_cols [MONO_IMPLMAP_FLAGS];
			import = mono_metadata_string_heap (image, im_cols [MONO_IMPLMAP_NAME]);
			scope_token = mono_metadata_decode_row_col (mr, im_cols [MONO_IMPLMAP_SCOPE] - 1, MONO_MODULEREF_NAME);
			scope = mono_metadata_string_heap (image, scope_token);
		}
	}
	flags = piinfo->piflags;
	
	attr = (MonoReflectionDllImportAttribute*)mono_object_new (domain, DllImportAttributeClass);

	MONO_OBJECT_SETREF (attr, dll, mono_string_new (domain, scope));
	MONO_OBJECT_SETREF (attr, entry_point, mono_string_new (domain, import));
	attr->call_conv = (flags & 0x700) >> 8;
	attr->charset = ((flags & 0x6) >> 1) + 1;
	if (attr->charset == 1)
		attr->charset = 2;
	attr->exact_spelling = (flags & 0x1) != 0;
	attr->set_last_error = (flags & 0x40) != 0;
	attr->best_fit_mapping = (flags & 0x30) == 0x10;
	attr->throw_on_unmappable = (flags & 0x3000) == 0x1000;
	attr->preserve_sig = FALSE;

	return attr;
}

ICALL_EXPORT MonoReflectionMethod *
ves_icall_MonoMethod_GetGenericMethodDefinition (MonoReflectionMethod *method)
{
	MonoMethodInflated *imethod;
	MonoMethod *result;

	MONO_ARCH_SAVE_REGS;

	if (method->method->is_generic)
		return method;

	if (!method->method->is_inflated)
		return NULL;

	imethod = (MonoMethodInflated *) method->method;

	result = imethod->declaring;
	/* Not a generic method.  */
	if (!result->is_generic)
		return NULL;

	if (method->method->klass->image->dynamic) {
		MonoDynamicImage *image = (MonoDynamicImage*)method->method->klass->image;
		MonoReflectionMethod *res;

		/*
		 * FIXME: Why is this stuff needed at all ? Why can't the code below work for
		 * the dynamic case as well ?
		 */
		mono_loader_lock ();
		res = mono_g_hash_table_lookup (image->generic_def_objects, imethod);
		mono_loader_unlock ();

		if (res)
			return res;
	}

	if (imethod->context.class_inst) {
		MonoClass *klass = ((MonoMethod *) imethod)->klass;
		/*Generic methods gets the context of the GTD.*/
		if (mono_class_get_context (klass))
			result = mono_class_inflate_generic_method_full (result, klass, mono_class_get_context (klass));
	}

	return mono_method_get_object (mono_object_domain (method), result, NULL);
}

ICALL_EXPORT gboolean
ves_icall_MonoMethod_get_IsGenericMethod (MonoReflectionMethod *method)
{
	MONO_ARCH_SAVE_REGS;

	return mono_method_signature (method->method)->generic_param_count != 0;
}

ICALL_EXPORT gboolean
ves_icall_MonoMethod_get_IsGenericMethodDefinition (MonoReflectionMethod *method)
{
	MONO_ARCH_SAVE_REGS;

	return method->method->is_generic;
}

ICALL_EXPORT MonoArray*
ves_icall_MonoMethod_GetGenericArguments (MonoReflectionMethod *method)
{
	MonoArray *res;
	MonoDomain *domain;
	int count, i;
	MONO_ARCH_SAVE_REGS;

	domain = mono_object_domain (method);

	if (method->method->is_inflated) {
		MonoGenericInst *inst = mono_method_get_context (method->method)->method_inst;

		if (inst) {
			count = inst->type_argc;
			res = mono_array_new (domain, mono_defaults.systemtype_class, count);

			for (i = 0; i < count; i++)
				mono_array_setref (res, i, mono_type_get_object (domain, inst->type_argv [i]));

			return res;
		}
	}

	count = mono_method_signature (method->method)->generic_param_count;
	res = mono_array_new (domain, mono_defaults.systemtype_class, count);

	for (i = 0; i < count; i++) {
		MonoGenericContainer *container = mono_method_get_generic_container (method->method);
		MonoGenericParam *param = mono_generic_container_get_param (container, i);
		MonoClass *pklass = mono_class_from_generic_parameter (
			param, method->method->klass->image, TRUE);
		mono_array_setref (res, i,
				mono_type_get_object (domain, &pklass->byval_arg));
	}

	return res;
}

ICALL_EXPORT MonoObject *
ves_icall_InternalInvoke (MonoReflectionMethod *method, MonoObject *this, MonoArray *params, MonoException **exc) 
{
	/* 
	 * Invoke from reflection is supposed to always be a virtual call (the API
	 * is stupid), mono_runtime_invoke_*() calls the provided method, allowing
	 * greater flexibility.
	 */
	MonoMethod *m = method->method;
	MonoMethodSignature *sig = mono_method_signature (m);
	MonoImage *image;
	int pcount;
	void *obj = this;

	MONO_ARCH_SAVE_REGS;

	*exc = NULL;

	if (mono_security_core_clr_enabled ())
		mono_security_core_clr_ensure_reflection_access_method (m);

	if (!(m->flags & METHOD_ATTRIBUTE_STATIC)) {
		if (!mono_class_vtable_full (mono_object_domain (method), m->klass, FALSE)) {
			mono_gc_wbarrier_generic_store (exc, (MonoObject*) mono_class_get_exception_for_failure (m->klass));
			return NULL;
		}

		if (this) {
			if (!mono_object_isinst (this, m->klass)) {
				char *this_name = mono_type_get_full_name (mono_object_get_class (this));
				char *target_name = mono_type_get_full_name (m->klass);
				char *msg = g_strdup_printf ("Object of type '%s' doesn't match target type '%s'", this_name, target_name);
				mono_gc_wbarrier_generic_store (exc, (MonoObject*) mono_exception_from_name_msg (mono_defaults.corlib, "System.Reflection", "TargetException", msg));
				g_free (msg);
				g_free (target_name);
				g_free (this_name);
				return NULL;
			}
			m = mono_object_get_virtual_method (this, m);
			/* must pass the pointer to the value for valuetype methods */
			if (m->klass->valuetype)
				obj = mono_object_unbox (this);
		} else if (strcmp (m->name, ".ctor") && !m->wrapper_type) {
			mono_gc_wbarrier_generic_store (exc, (MonoObject*) mono_exception_from_name_msg (mono_defaults.corlib, "System.Reflection", "TargetException", "Non-static method requires a target."));
			return NULL;
		}
	}

	if (sig->ret->byref) {
		mono_gc_wbarrier_generic_store (exc, (MonoObject*) mono_exception_from_name_msg (mono_defaults.corlib, "System", "NotSupportedException", "Cannot invoke method returning ByRef type via reflection"));
		return NULL;
	}

	pcount = params? mono_array_length (params): 0;
	if (pcount != sig->param_count) {
		mono_gc_wbarrier_generic_store (exc, (MonoObject*) mono_exception_from_name (mono_defaults.corlib, "System.Reflection", "TargetParameterCountException"));
		return NULL;
	}

	if ((m->klass->flags & TYPE_ATTRIBUTE_ABSTRACT) && !strcmp (m->name, ".ctor") && !this) {
		mono_gc_wbarrier_generic_store (exc, (MonoObject*) mono_exception_from_name_msg (mono_defaults.corlib, "System.Reflection", "TargetException", "Cannot invoke constructor of an abstract class."));
		return NULL;
	}

	image = m->klass->image;
	if (image->assembly->ref_only) {
		mono_gc_wbarrier_generic_store (exc, (MonoObject*) mono_get_exception_invalid_operation ("It is illegal to invoke a method on a type loaded using the ReflectionOnly api."));
		return NULL;
	}

	if (image->dynamic && !((MonoDynamicImage*)image)->run) {
		mono_gc_wbarrier_generic_store (exc, (MonoObject*) mono_get_exception_not_supported ("Cannot invoke a method in a dynamic assembly without run access."));
		return NULL;
	}
	
	if (m->klass->rank && !strcmp (m->name, ".ctor")) {
		int i;
		uintptr_t *lengths;
		intptr_t *lower_bounds;
		pcount = mono_array_length (params);
		lengths = alloca (sizeof (uintptr_t) * pcount);
		/* Note: the synthetized array .ctors have int32 as argument type */
		for (i = 0; i < pcount; ++i)
			lengths [i] = *(int32_t*) ((char*)mono_array_get (params, gpointer, i) + sizeof (MonoObject));

		if (m->klass->rank == pcount) {
			/* Only lengths provided. */
			lower_bounds = NULL;
		} else {
			g_assert (pcount == (m->klass->rank * 2));
			/* lower bounds are first. */
			lower_bounds = (intptr_t*)lengths;
			lengths += m->klass->rank;
		}

		return (MonoObject*)mono_array_new_full (mono_object_domain (params), m->klass, lengths, lower_bounds);
	}
	return mono_runtime_invoke_array (m, obj, params, NULL);
}

#ifndef DISABLE_REMOTING
ICALL_EXPORT MonoObject *
ves_icall_InternalExecute (MonoReflectionMethod *method, MonoObject *this, MonoArray *params, MonoArray **outArgs) 
{
	MonoDomain *domain = mono_object_domain (method); 
	MonoMethod *m = method->method;
	MonoMethodSignature *sig = mono_method_signature (m);
	MonoArray *out_args;
	MonoObject *result;
	int i, j, outarg_count = 0;

	MONO_ARCH_SAVE_REGS;

	if (m->klass == mono_defaults.object_class) {

		if (!strcmp (m->name, "FieldGetter")) {
			MonoClass *k = this->vtable->klass;
			MonoString *name;
			char *str;
			
			/* If this is a proxy, then it must be a CBO */
			if (k == mono_defaults.transparent_proxy_class) {
				MonoTransparentProxy *tp = (MonoTransparentProxy*) this;
				this = tp->rp->unwrapped_server;
				g_assert (this);
				k = this->vtable->klass;
			}
			
			name = mono_array_get (params, MonoString *, 1);
			str = mono_string_to_utf8 (name);
		
			do {
				MonoClassField* field = mono_class_get_field_from_name (k, str);
				if (field) {
					MonoClass *field_klass =  mono_class_from_mono_type (field->type);
					if (field_klass->valuetype)
						result = mono_value_box (domain, field_klass, (char *)this + field->offset);
					else 
						result = *((gpointer *)((char *)this + field->offset));
				
					out_args = mono_array_new (domain, mono_defaults.object_class, 1);
					mono_gc_wbarrier_generic_store (outArgs, (MonoObject*) out_args);
					mono_array_setref (out_args, 0, result);
					g_free (str);
					return NULL;
				}
				k = k->parent;
			} while (k);

			g_free (str);
			g_assert_not_reached ();

		} else if (!strcmp (m->name, "FieldSetter")) {
			MonoClass *k = this->vtable->klass;
			MonoString *name;
			guint32 size;
			gint32 align;
			char *str;
			
			/* If this is a proxy, then it must be a CBO */
			if (k == mono_defaults.transparent_proxy_class) {
				MonoTransparentProxy *tp = (MonoTransparentProxy*) this;
				this = tp->rp->unwrapped_server;
				g_assert (this);
				k = this->vtable->klass;
			}
			
			name = mono_array_get (params, MonoString *, 1);
			str = mono_string_to_utf8 (name);
		
			do {
				MonoClassField* field = mono_class_get_field_from_name (k, str);
				if (field) {
					MonoClass *field_klass =  mono_class_from_mono_type (field->type);
					MonoObject *val = mono_array_get (params, gpointer, 2);

					if (field_klass->valuetype) {
						size = mono_type_size (field->type, &align);
						g_assert (size == mono_class_value_size (field_klass, NULL));
						mono_gc_wbarrier_value_copy ((char *)this + field->offset, (char*)val + sizeof (MonoObject), 1, field_klass);
					} else {
						mono_gc_wbarrier_set_field (this, (char*)this + field->offset, val);
					}
				
					out_args = mono_array_new (domain, mono_defaults.object_class, 0);
					mono_gc_wbarrier_generic_store (outArgs, (MonoObject*) out_args);

					g_free (str);
					return NULL;
				}
				
				k = k->parent;
			} while (k);

			g_free (str);
			g_assert_not_reached ();

		}
	}

	for (i = 0; i < mono_array_length (params); i++) {
		if (sig->params [i]->byref) 
			outarg_count++;
	}

	out_args = mono_array_new (domain, mono_defaults.object_class, outarg_count);
	
	/* handle constructors only for objects already allocated */
	if (!strcmp (method->method->name, ".ctor"))
		g_assert (this);

	/* This can be called only on MBR objects, so no need to unbox for valuetypes. */
	g_assert (!method->method->klass->valuetype);
	result = mono_runtime_invoke_array (method->method, this, params, NULL);

	for (i = 0, j = 0; i < mono_array_length (params); i++) {
		if (sig->params [i]->byref) {
			gpointer arg;
			arg = mono_array_get (params, gpointer, i);
			mono_array_setref (out_args, j, arg);
			j++;
		}
	}

	mono_gc_wbarrier_generic_store (outArgs, (MonoObject*) out_args);

	return result;
}
#endif

static guint64
read_enum_value (char *mem, int type)
{
	switch (type) {
	case MONO_TYPE_U1:
		return *(guint8*)mem;
	case MONO_TYPE_I1:
		return *(gint8*)mem;
	case MONO_TYPE_U2:
		return *(guint16*)mem;
	case MONO_TYPE_I2:
		return *(gint16*)mem;
	case MONO_TYPE_U4:
		return *(guint32*)mem;
	case MONO_TYPE_I4:
		return *(gint32*)mem;
	case MONO_TYPE_U8:
		return *(guint64*)mem;
	case MONO_TYPE_I8:
		return *(gint64*)mem;
	default:
		g_assert_not_reached ();
	}
	return 0;
}

static void
write_enum_value (char *mem, int type, guint64 value)
{
	switch (type) {
	case MONO_TYPE_U1:
	case MONO_TYPE_I1: {
		guint8 *p = (guint8*)mem;
		*p = value;
		break;
	}
	case MONO_TYPE_U2:
	case MONO_TYPE_I2: {
		guint16 *p = (void*)mem;
		*p = value;
		break;
	}
	case MONO_TYPE_U4:
	case MONO_TYPE_I4: {
		guint32 *p = (void*)mem;
		*p = value;
		break;
	}
	case MONO_TYPE_U8:
	case MONO_TYPE_I8: {
		guint64 *p = (void*)mem;
		*p = value;
		break;
	}
	default:
		g_assert_not_reached ();
	}
	return;
}

ICALL_EXPORT MonoObject *
ves_icall_System_Enum_ToObject (MonoReflectionType *enumType, MonoObject *value)
{
	MonoDomain *domain; 
	MonoClass *enumc, *objc;
	MonoObject *res;
	MonoType *etype;
	guint64 val;
	
	MONO_ARCH_SAVE_REGS;

	MONO_CHECK_ARG_NULL (enumType);
	MONO_CHECK_ARG_NULL (value);

	domain = mono_object_domain (enumType); 
	enumc = mono_class_from_mono_type (enumType->type);

	mono_class_init_or_throw (enumc);

	objc = value->vtable->klass;

	if (!enumc->enumtype)
		mono_raise_exception (mono_get_exception_argument ("enumType", "Type provided must be an Enum."));
	if (!((objc->enumtype) || (objc->byval_arg.type >= MONO_TYPE_I1 && objc->byval_arg.type <= MONO_TYPE_U8)))
		mono_raise_exception (mono_get_exception_argument ("value", "The value passed in must be an enum base or an underlying type for an enum, such as an Int32."));

	etype = mono_class_enum_basetype (enumc);
	if (!etype)
		/* MS throws this for typebuilders */
		mono_raise_exception (mono_get_exception_argument ("Type must be a type provided by the runtime.", "enumType"));

	res = mono_object_new (domain, enumc);
	val = read_enum_value ((char *)value + sizeof (MonoObject), objc->enumtype? mono_class_enum_basetype (objc)->type: objc->byval_arg.type);
	write_enum_value ((char *)res + sizeof (MonoObject), etype->type, val);

	return res;
}

ICALL_EXPORT MonoObject *
ves_icall_System_Enum_get_value (MonoObject *this)
{
	MonoObject *res;
	MonoClass *enumc;
	gpointer dst;
	gpointer src;
	int size;

	MONO_ARCH_SAVE_REGS;

	if (!this)
		return NULL;

	g_assert (this->vtable->klass->enumtype);
	
	enumc = mono_class_from_mono_type (mono_class_enum_basetype (this->vtable->klass));
	res = mono_object_new (mono_object_domain (this), enumc);
	dst = (char *)res + sizeof (MonoObject);
	src = (char *)this + sizeof (MonoObject);
	size = mono_class_value_size (enumc, NULL);

	memcpy (dst, src, size);

	return res;
}

ICALL_EXPORT MonoReflectionType *
ves_icall_System_Enum_get_underlying_type (MonoReflectionType *type)
{
	MonoType *etype;
	MonoClass *klass;

	MONO_ARCH_SAVE_REGS;

	klass = mono_class_from_mono_type (type->type);
	mono_class_init_or_throw (klass);

	etype = mono_class_enum_basetype (klass);
	if (!etype)
		/* MS throws this for typebuilders */
		mono_raise_exception (mono_get_exception_argument ("Type must be a type provided by the runtime.", "enumType"));

	return mono_type_get_object (mono_object_domain (type), etype);
}

ICALL_EXPORT int
ves_icall_System_Enum_compare_value_to (MonoObject *this, MonoObject *other)
{
	gpointer tdata = (char *)this + sizeof (MonoObject);
	gpointer odata = (char *)other + sizeof (MonoObject);
	MonoType *basetype = mono_class_enum_basetype (this->vtable->klass);
	g_assert (basetype);

#define COMPARE_ENUM_VALUES(ENUM_TYPE) do { \
		ENUM_TYPE me = *((ENUM_TYPE*)tdata); \
		ENUM_TYPE other = *((ENUM_TYPE*)odata); \
		if (me == other) \
			return 0; \
		return me > other ? 1 : -1; \
	} while (0)

#define COMPARE_ENUM_VALUES_RANGE(ENUM_TYPE) do { \
		ENUM_TYPE me = *((ENUM_TYPE*)tdata); \
		ENUM_TYPE other = *((ENUM_TYPE*)odata); \
		if (me == other) \
			return 0; \
		return me - other; \
	} while (0)

	switch (basetype->type) {
		case MONO_TYPE_U1:
			COMPARE_ENUM_VALUES (guint8);
		case MONO_TYPE_I1:
			COMPARE_ENUM_VALUES (gint8);
		case MONO_TYPE_CHAR:
		case MONO_TYPE_U2:
			COMPARE_ENUM_VALUES_RANGE (guint16);
		case MONO_TYPE_I2:
			COMPARE_ENUM_VALUES (gint16);
		case MONO_TYPE_U4:
			COMPARE_ENUM_VALUES (guint32);
		case MONO_TYPE_I4:
			COMPARE_ENUM_VALUES (gint32);
		case MONO_TYPE_U8:
			COMPARE_ENUM_VALUES (guint64);
		case MONO_TYPE_I8:
			COMPARE_ENUM_VALUES (gint64);
		default:
			g_error ("Implement type 0x%02x in get_hashcode", basetype->type);
	}
#undef COMPARE_ENUM_VALUES_RANGE
#undef COMPARE_ENUM_VALUES
	return 0;
}

ICALL_EXPORT int
ves_icall_System_Enum_get_hashcode (MonoObject *this)
{
	gpointer data = (char *)this + sizeof (MonoObject);
	MonoType *basetype = mono_class_enum_basetype (this->vtable->klass);
	g_assert (basetype);

	switch (basetype->type) {
		case MONO_TYPE_I1:	
			return *((gint8*)data);
		case MONO_TYPE_U1:
			return *((guint8*)data);
		case MONO_TYPE_CHAR:
		case MONO_TYPE_U2:
			return *((guint16*)data);
		
		case MONO_TYPE_I2:
			return *((gint16*)data);
		case MONO_TYPE_U4:
			return *((guint32*)data);
		case MONO_TYPE_I4:
			return *((gint32*)data);
		case MONO_TYPE_U8:
		case MONO_TYPE_I8: {
			gint64 value = *((gint64*)data);
			return (gint)(value & 0xffffffff) ^ (int)(value >> 32);
		}
		default:
			g_error ("Implement type 0x%02x in get_hashcode", basetype->type);
	}
	return 0;
}

ICALL_EXPORT void
ves_icall_get_enum_info (MonoReflectionType *type, MonoEnumInfo *info)
{
	MonoDomain *domain = mono_object_domain (type); 
	MonoClass *enumc = mono_class_from_mono_type (type->type);
	guint j = 0, nvalues, crow;
	gpointer iter;
	MonoClassField *field;

	MONO_ARCH_SAVE_REGS;

	mono_class_init_or_throw (enumc);

	MONO_STRUCT_SETREF (info, utype, mono_type_get_object (domain, mono_class_enum_basetype (enumc)));
	nvalues = mono_class_num_fields (enumc) ? mono_class_num_fields (enumc) - 1 : 0;
	MONO_STRUCT_SETREF (info, names, mono_array_new (domain, mono_defaults.string_class, nvalues));
	MONO_STRUCT_SETREF (info, values, mono_array_new (domain, enumc, nvalues));

	crow = -1;
	iter = NULL;
	while ((field = mono_class_get_fields (enumc, &iter))) {
		const char *p;
		int len;
		MonoTypeEnum def_type;
		
		if (!(field->type->attrs & FIELD_ATTRIBUTE_STATIC))
			continue;
		if (strcmp ("value__", mono_field_get_name (field)) == 0)
			continue;
		if (mono_field_is_deleted (field))
			continue;
		mono_array_setref (info->names, j, mono_string_new (domain, mono_field_get_name (field)));

		p = mono_class_get_field_default_value (field, &def_type);
		len = mono_metadata_decode_blob_size (p, &p);
		switch (mono_class_enum_basetype (enumc)->type) {
		case MONO_TYPE_U1:
		case MONO_TYPE_I1:
			mono_array_set (info->values, gchar, j, *p);
			break;
		case MONO_TYPE_CHAR:
		case MONO_TYPE_U2:
		case MONO_TYPE_I2:
			mono_array_set (info->values, gint16, j, read16 (p));
			break;
		case MONO_TYPE_U4:
		case MONO_TYPE_I4:
			mono_array_set (info->values, gint32, j, read32 (p));
			break;
		case MONO_TYPE_U8:
		case MONO_TYPE_I8:
			mono_array_set (info->values, gint64, j, read64 (p));
			break;
		default:
			g_error ("Implement type 0x%02x in get_enum_info", mono_class_enum_basetype (enumc)->type);
		}
		++j;
	}
}

enum {
	BFLAGS_IgnoreCase = 1,
	BFLAGS_DeclaredOnly = 2,
	BFLAGS_Instance = 4,
	BFLAGS_Static = 8,
	BFLAGS_Public = 0x10,
	BFLAGS_NonPublic = 0x20,
	BFLAGS_FlattenHierarchy = 0x40,
	BFLAGS_InvokeMethod = 0x100,
	BFLAGS_CreateInstance = 0x200,
	BFLAGS_GetField = 0x400,
	BFLAGS_SetField = 0x800,
	BFLAGS_GetProperty = 0x1000,
	BFLAGS_SetProperty = 0x2000,
	BFLAGS_ExactBinding = 0x10000,
	BFLAGS_SuppressChangeType = 0x20000,
	BFLAGS_OptionalParamBinding = 0x40000
};

ICALL_EXPORT MonoReflectionField *
ves_icall_Type_GetField (MonoReflectionType *type, MonoString *name, guint32 bflags)
{
	MonoDomain *domain; 
	MonoClass *startklass, *klass;
	int match;
	MonoClassField *field;
	gpointer iter;
	char *utf8_name;
	int (*compare_func) (const char *s1, const char *s2) = NULL;
	domain = ((MonoObject *)type)->vtable->domain;
	klass = startklass = mono_class_from_mono_type (type->type);

	if (!name)
		mono_raise_exception (mono_get_exception_argument_null ("name"));
	if (type->type->byref)
		return NULL;

	compare_func = (bflags & BFLAGS_IgnoreCase) ? mono_utf8_strcasecmp : strcmp;

handle_parent:
	if (klass->exception_type != MONO_EXCEPTION_NONE)
		mono_raise_exception (mono_class_get_exception_for_failure (klass));

	iter = NULL;
	while ((field = mono_class_get_fields_lazy (klass, &iter))) {
		guint32 flags = mono_field_get_flags (field);
		match = 0;

		if (mono_field_is_deleted_with_flags (field, flags))
			continue;
		if ((flags & FIELD_ATTRIBUTE_FIELD_ACCESS_MASK) == FIELD_ATTRIBUTE_PUBLIC) {
			if (bflags & BFLAGS_Public)
				match++;
		} else if ((klass == startklass) || (flags & FIELD_ATTRIBUTE_FIELD_ACCESS_MASK) != FIELD_ATTRIBUTE_PRIVATE) {
			if (bflags & BFLAGS_NonPublic) {
				match++;
			}
		}
		if (!match)
			continue;
		match = 0;
		if (flags & FIELD_ATTRIBUTE_STATIC) {
			if (bflags & BFLAGS_Static)
				if ((bflags & BFLAGS_FlattenHierarchy) || (klass == startklass))
					match++;
		} else {
			if (bflags & BFLAGS_Instance)
				match++;
		}

		if (!match)
			continue;
		
		utf8_name = mono_string_to_utf8 (name);

		if (compare_func (mono_field_get_name (field), utf8_name)) {
			g_free (utf8_name);
			continue;
		}
		g_free (utf8_name);
		
		return mono_field_get_object (domain, klass, field);
	}
	if (!(bflags & BFLAGS_DeclaredOnly) && (klass = klass->parent))
		goto handle_parent;

	return NULL;
}

ICALL_EXPORT MonoArray*
ves_icall_Type_GetFields_internal (MonoReflectionType *type, guint32 bflags, MonoReflectionType *reftype)
{
	MonoDomain *domain; 
	MonoClass *startklass, *klass, *refklass;
	MonoArray *res;
	MonoObject *member;
	int i, match;
	gpointer iter;
	MonoClassField *field;
	MonoPtrArray tmp_array;

	MONO_ARCH_SAVE_REGS;

	domain = ((MonoObject *)type)->vtable->domain;
	if (type->type->byref)
		return mono_array_new (domain, mono_defaults.field_info_class, 0);
	klass = startklass = mono_class_from_mono_type (type->type);
	refklass = mono_class_from_mono_type (reftype->type);

	mono_ptr_array_init (tmp_array, 2);
	
handle_parent:	
	if (klass->exception_type != MONO_EXCEPTION_NONE) {
		mono_ptr_array_destroy (tmp_array);
		mono_raise_exception (mono_class_get_exception_for_failure (klass));
	}

	iter = NULL;
	while ((field = mono_class_get_fields_lazy (klass, &iter))) {
		guint32 flags = mono_field_get_flags (field);
		match = 0;
		if (mono_field_is_deleted_with_flags (field, flags))
			continue;
		if ((flags & FIELD_ATTRIBUTE_FIELD_ACCESS_MASK) == FIELD_ATTRIBUTE_PUBLIC) {
			if (bflags & BFLAGS_Public)
				match++;
		} else if ((klass == startklass) || (flags & FIELD_ATTRIBUTE_FIELD_ACCESS_MASK) != FIELD_ATTRIBUTE_PRIVATE) {
			if (bflags & BFLAGS_NonPublic) {
				match++;
			}
		}
		if (!match)
			continue;
		match = 0;
		if (flags & FIELD_ATTRIBUTE_STATIC) {
			if (bflags & BFLAGS_Static)
				if ((bflags & BFLAGS_FlattenHierarchy) || (klass == startklass))
					match++;
		} else {
			if (bflags & BFLAGS_Instance)
				match++;
		}

		if (!match)
			continue;
		member = (MonoObject*)mono_field_get_object (domain, refklass, field);
		mono_ptr_array_append (tmp_array, member);
	}
	if (!(bflags & BFLAGS_DeclaredOnly) && (klass = klass->parent))
		goto handle_parent;

	res = mono_array_new_cached (domain, mono_defaults.field_info_class, mono_ptr_array_size (tmp_array));

	for (i = 0; i < mono_ptr_array_size (tmp_array); ++i)
		mono_array_setref (res, i, mono_ptr_array_get (tmp_array, i));

	mono_ptr_array_destroy (tmp_array);

	return res;
}

static gboolean
method_nonpublic (MonoMethod* method, gboolean start_klass)
{
	switch (method->flags & METHOD_ATTRIBUTE_MEMBER_ACCESS_MASK) {
		case METHOD_ATTRIBUTE_ASSEM:
			return (start_klass || mono_defaults.generic_ilist_class);
		case METHOD_ATTRIBUTE_PRIVATE:
			return start_klass;
		case METHOD_ATTRIBUTE_PUBLIC:
			return FALSE;
		default:
			return TRUE;
	}
}

GPtrArray*
mono_class_get_methods_by_name (MonoClass *klass, const char *name, guint32 bflags, gboolean ignore_case, gboolean allow_ctors, MonoException **ex)
{
	GPtrArray *array;
	MonoClass *startklass;
	MonoMethod *method;
	gpointer iter;
	int len, match, nslots;
	/*FIXME, use MonoBitSet*/
	guint32 method_slots_default [8];
	guint32 *method_slots = NULL;
	int (*compare_func) (const char *s1, const char *s2) = NULL;

	array = g_ptr_array_new ();
	startklass = klass;
	*ex = NULL;

	len = 0;
	if (name != NULL)
		compare_func = (ignore_case) ? mono_utf8_strcasecmp : strcmp;

	/* An optimization for calls made from Delegate:CreateDelegate () */
	if (klass->delegate && name && !strcmp (name, "Invoke") && (bflags == (BFLAGS_Public | BFLAGS_Static | BFLAGS_Instance))) {
		method = mono_get_delegate_invoke (klass);
		if (mono_loader_get_last_error ())
			goto loader_error;

		g_ptr_array_add (array, method);
		return array;
	}

	mono_class_setup_vtable (klass);
	if (klass->exception_type != MONO_EXCEPTION_NONE || mono_loader_get_last_error ())
		goto loader_error;

	if (is_generic_parameter (&klass->byval_arg))
		nslots = mono_class_get_vtable_size (klass->parent);
	else
		nslots = MONO_CLASS_IS_INTERFACE (klass) ? mono_class_num_methods (klass) : mono_class_get_vtable_size (klass);
	if (nslots >= sizeof (method_slots_default) * 8) {
		method_slots = g_new0 (guint32, nslots / 32 + 1);
	} else {
		method_slots = method_slots_default;
		memset (method_slots, 0, sizeof (method_slots_default));
	}
handle_parent:
	mono_class_setup_vtable (klass);
	if (klass->exception_type != MONO_EXCEPTION_NONE || mono_loader_get_last_error ())
		goto loader_error;		

	iter = NULL;
	while ((method = mono_class_get_methods (klass, &iter))) {
		match = 0;
		if (method->slot != -1) {
			g_assert (method->slot < nslots);
			if (method_slots [method->slot >> 5] & (1 << (method->slot & 0x1f)))
				continue;
			if (!(method->flags & METHOD_ATTRIBUTE_NEW_SLOT))
				method_slots [method->slot >> 5] |= 1 << (method->slot & 0x1f);
		}

		if (!allow_ctors && method->name [0] == '.' && (strcmp (method->name, ".ctor") == 0 || strcmp (method->name, ".cctor") == 0))
			continue;
		if ((method->flags & METHOD_ATTRIBUTE_MEMBER_ACCESS_MASK) == METHOD_ATTRIBUTE_PUBLIC) {
			if (bflags & BFLAGS_Public)
				match++;
		} else if ((bflags & BFLAGS_NonPublic) && method_nonpublic (method, (klass == startklass))) {
				match++;
		}
		if (!match)
			continue;
		match = 0;
		if (method->flags & METHOD_ATTRIBUTE_STATIC) {
			if (bflags & BFLAGS_Static)
				if ((bflags & BFLAGS_FlattenHierarchy) || (klass == startklass))
					match++;
		} else {
			if (bflags & BFLAGS_Instance)
				match++;
		}

		if (!match)
			continue;

		if (name != NULL) {
			if (compare_func (name, method->name))
				continue;
		}
		
		match = 0;
		g_ptr_array_add (array, method);
	}
	if (!(bflags & BFLAGS_DeclaredOnly) && (klass = klass->parent))
		goto handle_parent;
	if (method_slots != method_slots_default)
		g_free (method_slots);

	return array;

loader_error:
	if (method_slots != method_slots_default)
		g_free (method_slots);
	g_ptr_array_free (array, TRUE);

	if (klass->exception_type != MONO_EXCEPTION_NONE) {
		*ex = mono_class_get_exception_for_failure (klass);
	} else {
		*ex = mono_loader_error_prepare_exception (mono_loader_get_last_error ());
		mono_loader_clear_error ();
	}
	return NULL;
}

ICALL_EXPORT MonoArray*
ves_icall_Type_GetMethodsByName (MonoReflectionType *type, MonoString *name, guint32 bflags, MonoBoolean ignore_case, MonoReflectionType *reftype)
{
	static MonoClass *MethodInfo_array;
	MonoDomain *domain; 
	MonoArray *res;
	MonoVTable *array_vtable;
	MonoException *ex = NULL;
	const char *mname = NULL;
	GPtrArray *method_array;
	MonoClass *klass, *refklass;
	int i;

	if (!MethodInfo_array) {
		MonoClass *klass = mono_array_class_get (mono_defaults.method_info_class, 1);
		mono_memory_barrier ();
		MethodInfo_array = klass;
	}

	klass = mono_class_from_mono_type (type->type);
	refklass = mono_class_from_mono_type (reftype->type);
	domain = ((MonoObject *)type)->vtable->domain;
	array_vtable = mono_class_vtable_full (domain, MethodInfo_array, TRUE);
	if (type->type->byref)
		return mono_array_new_specific (array_vtable, 0);

	if (name)
		mname = mono_string_to_utf8 (name);

	method_array = mono_class_get_methods_by_name (klass, mname, bflags, ignore_case, FALSE, &ex);
	g_free ((char*)mname);
	if (ex)
		mono_raise_exception (ex);

	res = mono_array_new_specific (array_vtable, method_array->len);


	for (i = 0; i < method_array->len; ++i) {
		MonoMethod *method = g_ptr_array_index (method_array, i);
		mono_array_setref (res, i, mono_method_get_object (domain, method, refklass));
	}

	g_ptr_array_free (method_array, TRUE);
	return res;
}

ICALL_EXPORT MonoArray*
ves_icall_Type_GetConstructors_internal (MonoReflectionType *type, guint32 bflags, MonoReflectionType *reftype)
{
	MonoDomain *domain; 
	static MonoClass *System_Reflection_ConstructorInfo;
	MonoClass *startklass, *klass, *refklass;
	MonoArray *res;
	MonoMethod *method;
	MonoObject *member;
	int i, match;
	gpointer iter = NULL;
	MonoPtrArray tmp_array;
	
	MONO_ARCH_SAVE_REGS;

	mono_ptr_array_init (tmp_array, 4); /*FIXME, guestimating*/

	domain = ((MonoObject *)type)->vtable->domain;
	if (type->type->byref)
		return mono_array_new_cached (domain, mono_defaults.method_info_class, 0);
	klass = startklass = mono_class_from_mono_type (type->type);
	refklass = mono_class_from_mono_type (reftype->type);

	if (!System_Reflection_ConstructorInfo)
		System_Reflection_ConstructorInfo = mono_class_from_name (
			mono_defaults.corlib, "System.Reflection", "ConstructorInfo");

	iter = NULL;
	while ((method = mono_class_get_methods (klass, &iter))) {
		match = 0;
		if (strcmp (method->name, ".ctor") && strcmp (method->name, ".cctor"))
			continue;
		if ((method->flags & METHOD_ATTRIBUTE_MEMBER_ACCESS_MASK) == METHOD_ATTRIBUTE_PUBLIC) {
			if (bflags & BFLAGS_Public)
				match++;
		} else {
			if (bflags & BFLAGS_NonPublic)
				match++;
		}
		if (!match)
			continue;
		match = 0;
		if (method->flags & METHOD_ATTRIBUTE_STATIC) {
			if (bflags & BFLAGS_Static)
				if ((bflags & BFLAGS_FlattenHierarchy) || (klass == startklass))
					match++;
		} else {
			if (bflags & BFLAGS_Instance)
				match++;
		}

		if (!match)
			continue;
		member = (MonoObject*)mono_method_get_object (domain, method, refklass);

		mono_ptr_array_append (tmp_array, member);
	}

	res = mono_array_new_cached (domain, System_Reflection_ConstructorInfo, mono_ptr_array_size (tmp_array));

	for (i = 0; i < mono_ptr_array_size (tmp_array); ++i)
		mono_array_setref (res, i, mono_ptr_array_get (tmp_array, i));

	mono_ptr_array_destroy (tmp_array);

	return res;
}

static guint
property_hash (gconstpointer data)
{
	MonoProperty *prop = (MonoProperty*)data;

	return g_str_hash (prop->name);
}

static gboolean
property_equal (MonoProperty *prop1, MonoProperty *prop2)
{
	// Properties are hide-by-name-and-signature
	if (!g_str_equal (prop1->name, prop2->name))
		return FALSE;

	if (prop1->get && prop2->get && !mono_metadata_signature_equal (mono_method_signature (prop1->get), mono_method_signature (prop2->get)))
		return FALSE;
	if (prop1->set && prop2->set && !mono_metadata_signature_equal (mono_method_signature (prop1->set), mono_method_signature (prop2->set)))
		return FALSE;
	return TRUE;
}

static gboolean
property_accessor_nonpublic (MonoMethod* accessor, gboolean start_klass)
{
	if (!accessor)
		return FALSE;

	return method_nonpublic (accessor, start_klass);
}

ICALL_EXPORT MonoArray*
ves_icall_Type_GetPropertiesByName (MonoReflectionType *type, MonoString *name, guint32 bflags, MonoBoolean ignore_case, MonoReflectionType *reftype)
{
	MonoException *ex;
	MonoDomain *domain; 
	static MonoClass *System_Reflection_PropertyInfo;
	MonoClass *startklass, *klass;
	MonoArray *res;
	MonoMethod *method;
	MonoProperty *prop;
	int i, match;
	guint32 flags;
	gchar *propname = NULL;
	int (*compare_func) (const char *s1, const char *s2) = NULL;
	gpointer iter;
	GHashTable *properties = NULL;
	MonoPtrArray tmp_array;

	MONO_ARCH_SAVE_REGS;

	mono_ptr_array_init (tmp_array, 8); /*This the average for ASP.NET types*/

	if (!System_Reflection_PropertyInfo)
		System_Reflection_PropertyInfo = mono_class_from_name (
			mono_defaults.corlib, "System.Reflection", "PropertyInfo");

	domain = ((MonoObject *)type)->vtable->domain;
	if (type->type->byref)
		return mono_array_new_cached (domain, System_Reflection_PropertyInfo, 0);
	klass = startklass = mono_class_from_mono_type (type->type);

	if (name != NULL) {
		propname = mono_string_to_utf8 (name);
		compare_func = (ignore_case) ? mono_utf8_strcasecmp : strcmp;
	}

	properties = g_hash_table_new (property_hash, (GEqualFunc)property_equal);
handle_parent:
	mono_class_setup_vtable (klass);
	if (klass->exception_type != MONO_EXCEPTION_NONE || mono_loader_get_last_error ())
		goto loader_error;

	iter = NULL;
	while ((prop = mono_class_get_properties (klass, &iter))) {
		match = 0;
		method = prop->get;
		if (!method)
			method = prop->set;
		if (method)
			flags = method->flags;
		else
			flags = 0;
		if ((prop->get && ((prop->get->flags & METHOD_ATTRIBUTE_MEMBER_ACCESS_MASK) == METHOD_ATTRIBUTE_PUBLIC)) ||
			(prop->set && ((prop->set->flags & METHOD_ATTRIBUTE_MEMBER_ACCESS_MASK) == METHOD_ATTRIBUTE_PUBLIC))) {
			if (bflags & BFLAGS_Public)
				match++;
		} else if (bflags & BFLAGS_NonPublic) {
			if (property_accessor_nonpublic(prop->get, startklass == klass) ||
				property_accessor_nonpublic(prop->set, startklass == klass)) {
				match++;
			}
		}
		if (!match)
			continue;
		match = 0;
		if (flags & METHOD_ATTRIBUTE_STATIC) {
			if (bflags & BFLAGS_Static)
				if ((bflags & BFLAGS_FlattenHierarchy) || (klass == startklass))
					match++;
		} else {
			if (bflags & BFLAGS_Instance)
				match++;
		}

		if (!match)
			continue;
		match = 0;

		if (name != NULL) {
			if (compare_func (propname, prop->name))
				continue;
		}
		
		if (g_hash_table_lookup (properties, prop))
			continue;

		mono_ptr_array_append (tmp_array, mono_property_get_object (domain, startklass, prop));
		
		g_hash_table_insert (properties, prop, prop);
	}
	if ((!(bflags & BFLAGS_DeclaredOnly) && (klass = klass->parent)))
		goto handle_parent;

	g_hash_table_destroy (properties);
	g_free (propname);

	res = mono_array_new_cached (domain, System_Reflection_PropertyInfo, mono_ptr_array_size (tmp_array));
	for (i = 0; i < mono_ptr_array_size (tmp_array); ++i)
		mono_array_setref (res, i, mono_ptr_array_get (tmp_array, i));

	mono_ptr_array_destroy (tmp_array);

	return res;

loader_error:
	if (properties)
		g_hash_table_destroy (properties);
	if (name)
		g_free (propname);
	mono_ptr_array_destroy (tmp_array);

	if (klass->exception_type != MONO_EXCEPTION_NONE) {
		ex = mono_class_get_exception_for_failure (klass);
	} else {
		ex = mono_loader_error_prepare_exception (mono_loader_get_last_error ());
		mono_loader_clear_error ();
	}
	mono_raise_exception (ex);
	return NULL;
}

ICALL_EXPORT MonoReflectionEvent *
ves_icall_MonoType_GetEvent (MonoReflectionType *type, MonoString *name, guint32 bflags)
{
	MonoDomain *domain;
	MonoClass *klass, *startklass;
	gpointer iter;
	MonoEvent *event;
	MonoMethod *method;
	gchar *event_name;
	int (*compare_func) (const char *s1, const char *s2);

	MONO_ARCH_SAVE_REGS;

	event_name = mono_string_to_utf8 (name);
	if (type->type->byref)
		return NULL;
	klass = startklass = mono_class_from_mono_type (type->type);
	domain = mono_object_domain (type);

	mono_class_init_or_throw (klass);

	compare_func = (bflags & BFLAGS_IgnoreCase) ? mono_utf8_strcasecmp : strcmp;
handle_parent:	
	if (klass->exception_type != MONO_EXCEPTION_NONE)
		mono_raise_exception (mono_class_get_exception_for_failure (klass));

	iter = NULL;
	while ((event = mono_class_get_events (klass, &iter))) {
		if (compare_func (event->name, event_name))
			continue;

		method = event->add;
		if (!method)
			method = event->remove;
		if (!method)
			method = event->raise;
		if (method) {
			if ((method->flags & METHOD_ATTRIBUTE_MEMBER_ACCESS_MASK) == METHOD_ATTRIBUTE_PUBLIC) {
				if (!(bflags & BFLAGS_Public))
					continue;
			} else {
				if (!(bflags & BFLAGS_NonPublic))
					continue;
				if ((klass != startklass) && (method->flags & METHOD_ATTRIBUTE_MEMBER_ACCESS_MASK) == METHOD_ATTRIBUTE_PRIVATE)
					continue;
			}

			if (method->flags & METHOD_ATTRIBUTE_STATIC) {
				if (!(bflags & BFLAGS_Static))
					continue;
				if (!(bflags & BFLAGS_FlattenHierarchy) && (klass != startklass))
					continue;
			} else {
				if (!(bflags & BFLAGS_Instance))
					continue;
			}
		} else 
			if (!(bflags & BFLAGS_NonPublic))
				continue;
		
		g_free (event_name);
		return mono_event_get_object (domain, startklass, event);
	}

	if (!(bflags & BFLAGS_DeclaredOnly) && (klass = klass->parent))
		goto handle_parent;

	g_free (event_name);
	return NULL;
}

ICALL_EXPORT MonoArray*
ves_icall_Type_GetEvents_internal (MonoReflectionType *type, guint32 bflags, MonoReflectionType *reftype)
{
	MonoException *ex;
	MonoDomain *domain; 
	static MonoClass *System_Reflection_EventInfo;
	MonoClass *startklass, *klass;
	MonoArray *res;
	MonoMethod *method;
	MonoEvent *event;
	int i, match;
	gpointer iter;
	
	MonoPtrArray tmp_array;

	MONO_ARCH_SAVE_REGS;

	mono_ptr_array_init (tmp_array, 4);

	if (!System_Reflection_EventInfo)
		System_Reflection_EventInfo = mono_class_from_name (
			mono_defaults.corlib, "System.Reflection", "EventInfo");

	domain = mono_object_domain (type);
	if (type->type->byref)
		return mono_array_new_cached (domain, System_Reflection_EventInfo, 0);
	klass = startklass = mono_class_from_mono_type (type->type);

handle_parent:
	mono_class_setup_vtable (klass);
	if (klass->exception_type != MONO_EXCEPTION_NONE || mono_loader_get_last_error ())
		goto loader_error;

	iter = NULL;
	while ((event = mono_class_get_events (klass, &iter))) {
		match = 0;
		method = event->add;
		if (!method)
			method = event->remove;
		if (!method)
			method = event->raise;
		if (method) {
			if ((method->flags & METHOD_ATTRIBUTE_MEMBER_ACCESS_MASK) == METHOD_ATTRIBUTE_PUBLIC) {
				if (bflags & BFLAGS_Public)
					match++;
			} else if ((klass == startklass) || (method->flags & METHOD_ATTRIBUTE_MEMBER_ACCESS_MASK) != METHOD_ATTRIBUTE_PRIVATE) {
				if (bflags & BFLAGS_NonPublic)
					match++;
			}
		}
		else
			if (bflags & BFLAGS_NonPublic)
				match ++;
		if (!match)
			continue;
		match = 0;
		if (method) {
			if (method->flags & METHOD_ATTRIBUTE_STATIC) {
				if (bflags & BFLAGS_Static)
					if ((bflags & BFLAGS_FlattenHierarchy) || (klass == startklass))
						match++;
			} else {
				if (bflags & BFLAGS_Instance)
					match++;
			}
		}
		else
			if (bflags & BFLAGS_Instance)
				match ++;
		if (!match)
			continue;
		mono_ptr_array_append (tmp_array, mono_event_get_object (domain, startklass, event));
	}
	if (!(bflags & BFLAGS_DeclaredOnly) && (klass = klass->parent))
		goto handle_parent;

	res = mono_array_new_cached (domain, System_Reflection_EventInfo, mono_ptr_array_size (tmp_array));

	for (i = 0; i < mono_ptr_array_size (tmp_array); ++i)
		mono_array_setref (res, i, mono_ptr_array_get (tmp_array, i));

	mono_ptr_array_destroy (tmp_array);

	return res;

loader_error:
	mono_ptr_array_destroy (tmp_array);
	if (klass->exception_type != MONO_EXCEPTION_NONE) {
		ex = mono_class_get_exception_for_failure (klass);
	} else {
		ex = mono_loader_error_prepare_exception (mono_loader_get_last_error ());
		mono_loader_clear_error ();
	}
	mono_raise_exception (ex);
	return NULL;
}

ICALL_EXPORT MonoReflectionType *
ves_icall_Type_GetNestedType (MonoReflectionType *type, MonoString *name, guint32 bflags)
{
	MonoDomain *domain; 
	MonoClass *klass;
	MonoClass *nested;
	char *str;
	gpointer iter;
	
	MONO_ARCH_SAVE_REGS;

	if (name == NULL)
		mono_raise_exception (mono_get_exception_argument_null ("name"));
	
	domain = ((MonoObject *)type)->vtable->domain;
	if (type->type->byref)
		return NULL;
	klass = mono_class_from_mono_type (type->type);

	str = mono_string_to_utf8 (name);

 handle_parent:
	if (klass->exception_type != MONO_EXCEPTION_NONE)
		mono_raise_exception (mono_class_get_exception_for_failure (klass));

	/*
	 * If a nested type is generic, return its generic type definition.
	 * Note that this means that the return value is essentially a
	 * nested type of the generic type definition of @klass.
	 *
	 * A note in MSDN claims that a generic type definition can have
	 * nested types that aren't generic.  In any case, the container of that
	 * nested type would be the generic type definition.
	 */
	if (klass->generic_class)
		klass = klass->generic_class->container_class;

	iter = NULL;
	while ((nested = mono_class_get_nested_types (klass, &iter))) {
		int match = 0;
		if ((nested->flags & TYPE_ATTRIBUTE_VISIBILITY_MASK) == TYPE_ATTRIBUTE_NESTED_PUBLIC) {
			if (bflags & BFLAGS_Public)
				match++;
		} else {
			if (bflags & BFLAGS_NonPublic)
				match++;
		}
		if (!match)
			continue;
		if (strcmp (nested->name, str) == 0){
			g_free (str);
			return mono_type_get_object (domain, &nested->byval_arg);
		}
	}
	if (!(bflags & BFLAGS_DeclaredOnly) && (klass = klass->parent))
		goto handle_parent;
	g_free (str);
	return NULL;
}

ICALL_EXPORT MonoArray*
ves_icall_Type_GetNestedTypes (MonoReflectionType *type, guint32 bflags)
{
	MonoDomain *domain; 
	MonoClass *klass;
	MonoArray *res;
	MonoObject *member;
	int i, match;
	MonoClass *nested;
	gpointer iter;
	MonoPtrArray tmp_array;

	MONO_ARCH_SAVE_REGS;

	domain = ((MonoObject *)type)->vtable->domain;
	if (type->type->byref)
		return mono_array_new (domain, mono_defaults.monotype_class, 0);
	klass = mono_class_from_mono_type (type->type);

	/*
	 * If a nested type is generic, return its generic type definition.
	 * Note that this means that the return value is essentially the set
	 * of nested types of the generic type definition of @klass.
	 *
	 * A note in MSDN claims that a generic type definition can have
	 * nested types that aren't generic.  In any case, the container of that
	 * nested type would be the generic type definition.
	 */
	if (klass->generic_class)
		klass = klass->generic_class->container_class;

	mono_ptr_array_init (tmp_array, 1);
	iter = NULL;
	while ((nested = mono_class_get_nested_types (klass, &iter))) {
		match = 0;
		if ((nested->flags & TYPE_ATTRIBUTE_VISIBILITY_MASK) == TYPE_ATTRIBUTE_NESTED_PUBLIC) {
			if (bflags & BFLAGS_Public)
				match++;
		} else {
			if (bflags & BFLAGS_NonPublic)
				match++;
		}
		if (!match)
			continue;
		member = (MonoObject*)mono_type_get_object (domain, &nested->byval_arg);
		mono_ptr_array_append (tmp_array, member);
	}

	res = mono_array_new_cached (domain, mono_defaults.monotype_class, mono_ptr_array_size (tmp_array));

	for (i = 0; i < mono_ptr_array_size (tmp_array); ++i)
		mono_array_setref (res, i, mono_ptr_array_get (tmp_array, i));

	mono_ptr_array_destroy (tmp_array);

	return res;
}

ICALL_EXPORT MonoReflectionType*
ves_icall_System_Reflection_Assembly_InternalGetType (MonoReflectionAssembly *assembly, MonoReflectionModule *module, MonoString *name, MonoBoolean throwOnError, MonoBoolean ignoreCase)
{
	gchar *str;
	MonoType *type = NULL;
	MonoTypeNameParse info;
	gboolean type_resolve;

	MONO_ARCH_SAVE_REGS;

	/* On MS.NET, this does not fire a TypeResolve event */
	type_resolve = TRUE;
	str = mono_string_to_utf8 (name);
	/*g_print ("requested type %s in %s\n", str, assembly->assembly->aname.name);*/
	if (!mono_reflection_parse_type (str, &info)) {
		g_free (str);
		mono_reflection_free_type_info (&info);
		if (throwOnError) /* uhm: this is a parse error, though... */
			mono_raise_exception (mono_get_exception_type_load (name, NULL));
		/*g_print ("failed parse\n");*/
		return NULL;
	}

	if (info.assembly.name) {
		g_free (str);
		mono_reflection_free_type_info (&info);
		if (throwOnError) {
			/* 1.0 and 2.0 throw different exceptions */
			if (mono_defaults.generic_ilist_class)
				mono_raise_exception (mono_get_exception_argument (NULL, "Type names passed to Assembly.GetType() must not specify an assembly."));
			else
				mono_raise_exception (mono_get_exception_type_load (name, NULL));
		}
		return NULL;
	}

	if (module != NULL) {
		if (module->image)
			type = mono_reflection_get_type (module->image, &info, ignoreCase, &type_resolve);
		else
			type = NULL;
	}
	else
		if (assembly->assembly->dynamic) {
			/* Enumerate all modules */
			MonoReflectionAssemblyBuilder *abuilder = (MonoReflectionAssemblyBuilder*)assembly;
			int i;

			type = NULL;
			if (abuilder->modules) {
				for (i = 0; i < mono_array_length (abuilder->modules); ++i) {
					MonoReflectionModuleBuilder *mb = mono_array_get (abuilder->modules, MonoReflectionModuleBuilder*, i);
					type = mono_reflection_get_type (&mb->dynamic_image->image, &info, ignoreCase, &type_resolve);
					if (type)
						break;
				}
			}

			if (!type && abuilder->loaded_modules) {
				for (i = 0; i < mono_array_length (abuilder->loaded_modules); ++i) {
					MonoReflectionModule *mod = mono_array_get (abuilder->loaded_modules, MonoReflectionModule*, i);
					type = mono_reflection_get_type (mod->image, &info, ignoreCase, &type_resolve);
					if (type)
						break;
				}
			}
		}
		else
			type = mono_reflection_get_type (assembly->assembly->image, &info, ignoreCase, &type_resolve);
	g_free (str);
	mono_reflection_free_type_info (&info);
	if (!type) {
		MonoException *e = NULL;
		
		if (throwOnError)
			e = mono_get_exception_type_load (name, NULL);

		if (mono_loader_get_last_error () && mono_defaults.generic_ilist_class)
			e = mono_loader_error_prepare_exception (mono_loader_get_last_error ());

		mono_loader_clear_error ();

		if (e != NULL)
			mono_raise_exception (e);

		return NULL;
	} else if (mono_loader_get_last_error ()) {
		if (throwOnError)
			mono_raise_exception (mono_loader_error_prepare_exception (mono_loader_get_last_error ()));
		mono_loader_clear_error ();
	}

	if (type->type == MONO_TYPE_CLASS) {
		MonoClass *klass = mono_type_get_class (type);

		if (mono_security_enabled () && !klass->exception_type)
			/* Some security problems are detected during generic vtable construction */
			mono_class_setup_vtable (klass);

		/* need to report exceptions ? */
		if (throwOnError && klass->exception_type) {
			/* report SecurityException (or others) that occured when loading the assembly */
			MonoException *exc = mono_class_get_exception_for_failure (klass);
			mono_loader_clear_error ();
			mono_raise_exception (exc);
		} else if (mono_security_enabled () && klass->exception_type == MONO_EXCEPTION_SECURITY_INHERITANCEDEMAND) {
			return NULL;
		}
	}

	/* g_print ("got it\n"); */
	return mono_type_get_object (mono_object_domain (assembly), type);
}

static gboolean
replace_shadow_path (MonoDomain *domain, gchar *dirname, gchar **filename)
{
	gchar *content;
	gchar *shadow_ini_file;
	gsize len;

	/* Check for shadow-copied assembly */
	if (mono_is_shadow_copy_enabled (domain, dirname)) {
		shadow_ini_file = g_build_filename (dirname, "__AssemblyInfo__.ini", NULL);
		content = NULL;
		if (!g_file_get_contents (shadow_ini_file, &content, &len, NULL) ||
			!g_file_test (content, G_FILE_TEST_IS_REGULAR)) {
			if (content) {
				g_free (content);
				content = NULL;
			}
		}
		g_free (shadow_ini_file);
		if (content != NULL) {
			if (*filename)
				g_free (*filename);
			*filename = content;
			return TRUE;
		}
	}
	return FALSE;
}

ICALL_EXPORT MonoString *
ves_icall_System_Reflection_Assembly_get_code_base (MonoReflectionAssembly *assembly, MonoBoolean escaped)
{
	MonoDomain *domain = mono_object_domain (assembly); 
	MonoAssembly *mass = assembly->assembly;
	MonoString *res = NULL;
	gchar *uri;
	gchar *absolute;
	gchar *dirname;
	
	MONO_ARCH_SAVE_REGS;

	if (g_path_is_absolute (mass->image->name)) {
		absolute = g_strdup (mass->image->name);
		dirname = g_path_get_dirname (absolute);
	} else {
		absolute = g_build_filename (mass->basedir, mass->image->name, NULL);
		dirname = g_strdup (mass->basedir);
	}

	replace_shadow_path (domain, dirname, &absolute);
	g_free (dirname);
#if HOST_WIN32
	{
		gint i;
		for (i = strlen (absolute) - 1; i >= 0; i--)
			if (absolute [i] == '\\')
				absolute [i] = '/';
	}
#endif
	if (escaped) {
		uri = g_filename_to_uri (absolute, NULL, NULL);
	} else {
		const char *prepend = "file://";
#if HOST_WIN32
		if (*absolute == '/' && *(absolute + 1) == '/') {
			prepend = "file:";
		} else {
			prepend = "file:///";
		}
#endif
		uri = g_strconcat (prepend, absolute, NULL);
	}

	if (uri) {
		res = mono_string_new (domain, uri);
		g_free (uri);
	}
	g_free (absolute);
	return res;
}

ICALL_EXPORT MonoBoolean
ves_icall_System_Reflection_Assembly_get_global_assembly_cache (MonoReflectionAssembly *assembly)
{
	MonoAssembly *mass = assembly->assembly;

	MONO_ARCH_SAVE_REGS;

	return mass->in_gac;
}

ICALL_EXPORT MonoReflectionAssembly*
ves_icall_System_Reflection_Assembly_load_with_partial_name (MonoString *mname, MonoObject *evidence)
{
	gchar *name;
	MonoAssembly *res;
	MonoImageOpenStatus status;
	
	MONO_ARCH_SAVE_REGS;

	name = mono_string_to_utf8 (mname);
	res = mono_assembly_load_with_partial_name (name, &status);

	g_free (name);

	if (res == NULL)
		return NULL;
	return mono_assembly_get_object (mono_domain_get (), res);
}

ICALL_EXPORT MonoString *
ves_icall_System_Reflection_Assembly_get_location (MonoReflectionAssembly *assembly)
{
	MonoDomain *domain = mono_object_domain (assembly); 
	MonoString *res;

	MONO_ARCH_SAVE_REGS;

	res = mono_string_new (domain, mono_image_get_filename (assembly->assembly->image));

	return res;
}

ICALL_EXPORT MonoBoolean
ves_icall_System_Reflection_Assembly_get_ReflectionOnly (MonoReflectionAssembly *assembly)
{
	MONO_ARCH_SAVE_REGS;

	return assembly->assembly->ref_only;
}

ICALL_EXPORT MonoString *
ves_icall_System_Reflection_Assembly_InternalImageRuntimeVersion (MonoReflectionAssembly *assembly)
{
	MonoDomain *domain = mono_object_domain (assembly); 

	MONO_ARCH_SAVE_REGS;

	return mono_string_new (domain, assembly->assembly->image->version);
}

ICALL_EXPORT MonoReflectionMethod*
ves_icall_System_Reflection_Assembly_get_EntryPoint (MonoReflectionAssembly *assembly) 
{
	guint32 token = mono_image_get_entry_point (assembly->assembly->image);

	MONO_ARCH_SAVE_REGS;

	if (!token)
		return NULL;
	return mono_method_get_object (mono_object_domain (assembly), mono_get_method (assembly->assembly->image, token, NULL), NULL);
}

ICALL_EXPORT MonoReflectionModule*
ves_icall_System_Reflection_Assembly_GetManifestModuleInternal (MonoReflectionAssembly *assembly) 
{
	return mono_module_get_object (mono_object_domain (assembly), assembly->assembly->image);
}

ICALL_EXPORT MonoArray*
ves_icall_System_Reflection_Assembly_GetManifestResourceNames (MonoReflectionAssembly *assembly) 
{
	MonoTableInfo *table = &assembly->assembly->image->tables [MONO_TABLE_MANIFESTRESOURCE];
	MonoArray *result = mono_array_new (mono_object_domain (assembly), mono_defaults.string_class, table->rows);
	int i;
	const char *val;

	MONO_ARCH_SAVE_REGS;

	for (i = 0; i < table->rows; ++i) {
		val = mono_metadata_string_heap (assembly->assembly->image, mono_metadata_decode_row_col (table, i, MONO_MANIFEST_NAME));
		mono_array_setref (result, i, mono_string_new (mono_object_domain (assembly), val));
	}
	return result;
}

static MonoObject*
create_version (MonoDomain *domain, guint32 major, guint32 minor, guint32 build, guint32 revision)
{
	static MonoClass *System_Version = NULL;
	static MonoMethod *create_version = NULL;
	MonoObject *result;
	gpointer args [4];
	
	if (!System_Version) {
		System_Version = mono_class_from_name (mono_defaults.corlib, "System", "Version");
		g_assert (System_Version);
	}

	if (!create_version) {
		MonoMethodDesc *desc = mono_method_desc_new (":.ctor(int,int,int,int)", FALSE);
		create_version = mono_method_desc_search_in_class (desc, System_Version);
		g_assert (create_version);
		mono_method_desc_free (desc);
	}

	args [0] = &major;
	args [1] = &minor;
	args [2] = &build;
	args [3] = &revision;
	result = mono_object_new (domain, System_Version);
	mono_runtime_invoke (create_version, result, args, NULL);

	return result;
}

ICALL_EXPORT MonoArray*
ves_icall_System_Reflection_Assembly_GetReferencedAssemblies (MonoReflectionAssembly *assembly) 
{
	static MonoClass *System_Reflection_AssemblyName;
	MonoArray *result;
	MonoDomain *domain = mono_object_domain (assembly);
	int i, count = 0;
	static MonoMethod *create_culture = NULL;
	MonoImage *image = assembly->assembly->image;
	MonoTableInfo *t;

	MONO_ARCH_SAVE_REGS;

	if (!System_Reflection_AssemblyName)
		System_Reflection_AssemblyName = mono_class_from_name (
			mono_defaults.corlib, "System.Reflection", "AssemblyName");

	t = &assembly->assembly->image->tables [MONO_TABLE_ASSEMBLYREF];
	count = t->rows;

	result = mono_array_new (domain, System_Reflection_AssemblyName, count);

	if (count > 0 && !create_culture) {
		MonoMethodDesc *desc = mono_method_desc_new (
			"System.Globalization.CultureInfo:CreateCulture(string,bool)", TRUE);
		create_culture = mono_method_desc_search_in_image (desc, mono_defaults.corlib);
		g_assert (create_culture);
		mono_method_desc_free (desc);
	}

	for (i = 0; i < count; i++) {
		MonoReflectionAssemblyName *aname;
		guint32 cols [MONO_ASSEMBLYREF_SIZE];

		mono_metadata_decode_row (t, i, cols, MONO_ASSEMBLYREF_SIZE);

		aname = (MonoReflectionAssemblyName *) mono_object_new (
			domain, System_Reflection_AssemblyName);

		MONO_OBJECT_SETREF (aname, name, mono_string_new (domain, mono_metadata_string_heap (image, cols [MONO_ASSEMBLYREF_NAME])));

		aname->major = cols [MONO_ASSEMBLYREF_MAJOR_VERSION];
		aname->minor = cols [MONO_ASSEMBLYREF_MINOR_VERSION];
		aname->build = cols [MONO_ASSEMBLYREF_BUILD_NUMBER];
		aname->revision = cols [MONO_ASSEMBLYREF_REV_NUMBER];
		aname->flags = cols [MONO_ASSEMBLYREF_FLAGS];
		aname->versioncompat = 1; /* SameMachine (default) */
		aname->hashalg = ASSEMBLY_HASH_SHA1; /* SHA1 (default) */
		MONO_OBJECT_SETREF (aname, version, create_version (domain, aname->major, aname->minor, aname->build, aname->revision));

		if (create_culture) {
			gpointer args [2];
			MonoBoolean assembly_ref = 1;
			args [0] = mono_string_new (domain, mono_metadata_string_heap (image, cols [MONO_ASSEMBLYREF_CULTURE]));
			args [1] = &assembly_ref;
			MONO_OBJECT_SETREF (aname, cultureInfo, mono_runtime_invoke (create_culture, NULL, args, NULL));
		}
		
		if (cols [MONO_ASSEMBLYREF_PUBLIC_KEY]) {
			const gchar *pkey_ptr = mono_metadata_blob_heap (image, cols [MONO_ASSEMBLYREF_PUBLIC_KEY]);
			guint32 pkey_len = mono_metadata_decode_blob_size (pkey_ptr, &pkey_ptr);

			if ((cols [MONO_ASSEMBLYREF_FLAGS] & ASSEMBLYREF_FULL_PUBLIC_KEY_FLAG)) {
				/* public key token isn't copied - the class library will 
		   		automatically generate it from the public key if required */
				MONO_OBJECT_SETREF (aname, publicKey, mono_array_new (domain, mono_defaults.byte_class, pkey_len));
				memcpy (mono_array_addr (aname->publicKey, guint8, 0), pkey_ptr, pkey_len);
			} else {
				MONO_OBJECT_SETREF (aname, keyToken, mono_array_new (domain, mono_defaults.byte_class, pkey_len));
				memcpy (mono_array_addr (aname->keyToken, guint8, 0), pkey_ptr, pkey_len);
			}
		} else {
			MONO_OBJECT_SETREF (aname, keyToken, mono_array_new (domain, mono_defaults.byte_class, 0));
		}
		
		/* note: this function doesn't return the codebase on purpose (i.e. it can
		         be used under partial trust as path information isn't present). */

		mono_array_setref (result, i, aname);
	}
	return result;
}

/* move this in some file in mono/util/ */
static char *
g_concat_dir_and_file (const char *dir, const char *file)
{
	g_return_val_if_fail (dir != NULL, NULL);
	g_return_val_if_fail (file != NULL, NULL);

        /*
	 * If the directory name doesn't have a / on the end, we need
	 * to add one so we get a proper path to the file
	 */
	if (dir [strlen(dir) - 1] != G_DIR_SEPARATOR)
		return g_strconcat (dir, G_DIR_SEPARATOR_S, file, NULL);
	else
		return g_strconcat (dir, file, NULL);
}

ICALL_EXPORT void *
ves_icall_System_Reflection_Assembly_GetManifestResourceInternal (MonoReflectionAssembly *assembly, MonoString *name, gint32 *size, MonoReflectionModule **ref_module) 
{
	char *n = mono_string_to_utf8 (name);
	MonoTableInfo *table = &assembly->assembly->image->tables [MONO_TABLE_MANIFESTRESOURCE];
	guint32 i;
	guint32 cols [MONO_MANIFEST_SIZE];
	guint32 impl, file_idx;
	const char *val;
	MonoImage *module;

	MONO_ARCH_SAVE_REGS;

	for (i = 0; i < table->rows; ++i) {
		mono_metadata_decode_row (table, i, cols, MONO_MANIFEST_SIZE);
		val = mono_metadata_string_heap (assembly->assembly->image, cols [MONO_MANIFEST_NAME]);
		if (strcmp (val, n) == 0)
			break;
	}
	g_free (n);
	if (i == table->rows)
		return NULL;
	/* FIXME */
	impl = cols [MONO_MANIFEST_IMPLEMENTATION];
	if (impl) {
		/*
		 * this code should only be called after obtaining the 
		 * ResourceInfo and handling the other cases.
		 */
		g_assert ((impl & MONO_IMPLEMENTATION_MASK) == MONO_IMPLEMENTATION_FILE);
		file_idx = impl >> MONO_IMPLEMENTATION_BITS;

		module = mono_image_load_file_for_image (assembly->assembly->image, file_idx);
		if (!module)
			return NULL;
	}
	else
		module = assembly->assembly->image;

	mono_gc_wbarrier_generic_store (ref_module, (MonoObject*) mono_module_get_object (mono_domain_get (), module));

	return (void*)mono_image_get_resource (module, cols [MONO_MANIFEST_OFFSET], (guint32*)size);
}

ICALL_EXPORT gboolean
ves_icall_System_Reflection_Assembly_GetManifestResourceInfoInternal (MonoReflectionAssembly *assembly, MonoString *name, MonoManifestResourceInfo *info)
{
	MonoTableInfo *table = &assembly->assembly->image->tables [MONO_TABLE_MANIFESTRESOURCE];
	int i;
	guint32 cols [MONO_MANIFEST_SIZE];
	guint32 file_cols [MONO_FILE_SIZE];
	const char *val;
	char *n;

	MONO_ARCH_SAVE_REGS;

	n = mono_string_to_utf8 (name);
	for (i = 0; i < table->rows; ++i) {
		mono_metadata_decode_row (table, i, cols, MONO_MANIFEST_SIZE);
		val = mono_metadata_string_heap (assembly->assembly->image, cols [MONO_MANIFEST_NAME]);
		if (strcmp (val, n) == 0)
			break;
	}
	g_free (n);
	if (i == table->rows)
		return FALSE;

	if (!cols [MONO_MANIFEST_IMPLEMENTATION]) {
		info->location = RESOURCE_LOCATION_EMBEDDED | RESOURCE_LOCATION_IN_MANIFEST;
	}
	else {
		switch (cols [MONO_MANIFEST_IMPLEMENTATION] & MONO_IMPLEMENTATION_MASK) {
		case MONO_IMPLEMENTATION_FILE:
			i = cols [MONO_MANIFEST_IMPLEMENTATION] >> MONO_IMPLEMENTATION_BITS;
			table = &assembly->assembly->image->tables [MONO_TABLE_FILE];
			mono_metadata_decode_row (table, i - 1, file_cols, MONO_FILE_SIZE);
			val = mono_metadata_string_heap (assembly->assembly->image, file_cols [MONO_FILE_NAME]);
			MONO_OBJECT_SETREF (info, filename, mono_string_new (mono_object_domain (assembly), val));
			if (file_cols [MONO_FILE_FLAGS] && FILE_CONTAINS_NO_METADATA)
				info->location = 0;
			else
				info->location = RESOURCE_LOCATION_EMBEDDED;
			break;

		case MONO_IMPLEMENTATION_ASSEMBLYREF:
			i = cols [MONO_MANIFEST_IMPLEMENTATION] >> MONO_IMPLEMENTATION_BITS;
			mono_assembly_load_reference (assembly->assembly->image, i - 1);
			if (assembly->assembly->image->references [i - 1] == (gpointer)-1) {
				char *msg = g_strdup_printf ("Assembly %d referenced from assembly %s not found ", i - 1, assembly->assembly->image->name);
				MonoException *ex = mono_get_exception_file_not_found2 (msg, NULL);
				g_free (msg);
				mono_raise_exception (ex);
			}
			MONO_OBJECT_SETREF (info, assembly, mono_assembly_get_object (mono_domain_get (), assembly->assembly->image->references [i - 1]));

			/* Obtain info recursively */
			ves_icall_System_Reflection_Assembly_GetManifestResourceInfoInternal (info->assembly, name, info);
			info->location |= RESOURCE_LOCATION_ANOTHER_ASSEMBLY;
			break;

		case MONO_IMPLEMENTATION_EXP_TYPE:
			g_assert_not_reached ();
			break;
		}
	}

	return TRUE;
}

ICALL_EXPORT MonoObject*
ves_icall_System_Reflection_Assembly_GetFilesInternal (MonoReflectionAssembly *assembly, MonoString *name, MonoBoolean resource_modules) 
{
	MonoTableInfo *table = &assembly->assembly->image->tables [MONO_TABLE_FILE];
	MonoArray *result = NULL;
	int i, count;
	const char *val;
	char *n;

	MONO_ARCH_SAVE_REGS;

	/* check hash if needed */
	if (name) {
		n = mono_string_to_utf8 (name);
		for (i = 0; i < table->rows; ++i) {
			val = mono_metadata_string_heap (assembly->assembly->image, mono_metadata_decode_row_col (table, i, MONO_FILE_NAME));
			if (strcmp (val, n) == 0) {
				MonoString *fn;
				g_free (n);
				n = g_concat_dir_and_file (assembly->assembly->basedir, val);
				fn = mono_string_new (mono_object_domain (assembly), n);
				g_free (n);
				return (MonoObject*)fn;
			}
		}
		g_free (n);
		return NULL;
	}

	count = 0;
	for (i = 0; i < table->rows; ++i) {
		if (resource_modules || !(mono_metadata_decode_row_col (table, i, MONO_FILE_FLAGS) & FILE_CONTAINS_NO_METADATA))
			count ++;
	}

	result = mono_array_new (mono_object_domain (assembly), mono_defaults.string_class, count);

	count = 0;
	for (i = 0; i < table->rows; ++i) {
		if (resource_modules || !(mono_metadata_decode_row_col (table, i, MONO_FILE_FLAGS) & FILE_CONTAINS_NO_METADATA)) {
			val = mono_metadata_string_heap (assembly->assembly->image, mono_metadata_decode_row_col (table, i, MONO_FILE_NAME));
			n = g_concat_dir_and_file (assembly->assembly->basedir, val);
			mono_array_setref (result, count, mono_string_new (mono_object_domain (assembly), n));
			g_free (n);
			count ++;
		}
	}
	return (MonoObject*)result;
}

ICALL_EXPORT MonoArray*
ves_icall_System_Reflection_Assembly_GetModulesInternal (MonoReflectionAssembly *assembly)
{
	MonoDomain *domain = mono_domain_get();
	MonoArray *res;
	MonoClass *klass;
	int i, j, file_count = 0;
	MonoImage **modules;
	guint32 module_count, real_module_count;
	MonoTableInfo *table;
	guint32 cols [MONO_FILE_SIZE];
	MonoImage *image = assembly->assembly->image;

	g_assert (image != NULL);
	g_assert (!assembly->assembly->dynamic);

	table = &image->tables [MONO_TABLE_FILE];
	file_count = table->rows;

	modules = image->modules;
	module_count = image->module_count;

	real_module_count = 0;
	for (i = 0; i < module_count; ++i)
		if (modules [i])
			real_module_count ++;

	klass = mono_class_from_name (mono_defaults.corlib, "System.Reflection", "Module");
	res = mono_array_new (domain, klass, 1 + real_module_count + file_count);

	mono_array_setref (res, 0, mono_module_get_object (domain, image));
	j = 1;
	for (i = 0; i < module_count; ++i)
		if (modules [i]) {
			mono_array_setref (res, j, mono_module_get_object (domain, modules[i]));
			++j;
		}

	for (i = 0; i < file_count; ++i, ++j) {
		mono_metadata_decode_row (table, i, cols, MONO_FILE_SIZE);
		if (cols [MONO_FILE_FLAGS] && FILE_CONTAINS_NO_METADATA)
			mono_array_setref (res, j, mono_module_file_get_object (domain, image, i));
		else {
			MonoImage *m = mono_image_load_file_for_image (image, i + 1);
			if (!m) {
				MonoString *fname = mono_string_new (mono_domain_get (), mono_metadata_string_heap (image, cols [MONO_FILE_NAME]));
				mono_raise_exception (mono_get_exception_file_not_found2 (NULL, fname));
			}
			mono_array_setref (res, j, mono_module_get_object (domain, m));
		}
	}

	return res;
}

ICALL_EXPORT MonoReflectionMethod*
ves_icall_GetCurrentMethod (void) 
{
	MonoMethod *m = mono_method_get_last_managed ();

	while (m->is_inflated)
		m = ((MonoMethodInflated*)m)->declaring;

	return mono_method_get_object (mono_domain_get (), m, NULL);
}


static MonoMethod*
mono_method_get_equivalent_method (MonoMethod *method, MonoClass *klass)
{
	int offset = -1, i;
	if (method->is_inflated && ((MonoMethodInflated*)method)->context.method_inst) {
		MonoMethodInflated *inflated = (MonoMethodInflated*)method;
		//method is inflated, we should inflate it on the other class
		MonoGenericContext ctx;
		ctx.method_inst = inflated->context.method_inst;
		ctx.class_inst = inflated->context.class_inst;
		if (klass->generic_class)
			ctx.class_inst = klass->generic_class->context.class_inst;
		else if (klass->generic_container)
			ctx.class_inst = klass->generic_container->context.class_inst;
		return mono_class_inflate_generic_method_full (inflated->declaring, klass, &ctx);
	}

	mono_class_setup_methods (method->klass);
	if (method->klass->exception_type)
		return NULL;
	for (i = 0; i < method->klass->method.count; ++i) {
		if (method->klass->methods [i] == method) {
			offset = i;
			break;
		}	
	}
	mono_class_setup_methods (klass);
	if (klass->exception_type)
		return NULL;
	g_assert (offset >= 0 && offset < klass->method.count);
	return klass->methods [offset];
}

ICALL_EXPORT MonoReflectionMethod*
ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternalType (MonoMethod *method, MonoType *type)
{
	MonoClass *klass;
	if (type) {
		klass = mono_class_from_mono_type (type);
		if (mono_class_get_generic_type_definition (method->klass) != mono_class_get_generic_type_definition (klass)) 
			return NULL;
		if (method->klass != klass) {
			method = mono_method_get_equivalent_method (method, klass);
			if (!method)
				return NULL;
		}
	} else
		klass = method->klass;
	return mono_method_get_object (mono_domain_get (), method, klass);
}

ICALL_EXPORT MonoReflectionMethod*
ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternal (MonoMethod *method)
{
	return mono_method_get_object (mono_domain_get (), method, NULL);
}

ICALL_EXPORT MonoReflectionMethodBody*
ves_icall_System_Reflection_MethodBase_GetMethodBodyInternal (MonoMethod *method)
{
	return mono_method_body_get_object (mono_domain_get (), method);
}

ICALL_EXPORT MonoReflectionAssembly*
ves_icall_System_Reflection_Assembly_GetExecutingAssembly (void)
{
	MonoMethod *dest = NULL;

	MONO_ARCH_SAVE_REGS;

	mono_stack_walk_no_il (get_executing, &dest);
	g_assert (dest);
	return mono_assembly_get_object (mono_domain_get (), dest->klass->image->assembly);
}


ICALL_EXPORT MonoReflectionAssembly*
ves_icall_System_Reflection_Assembly_GetEntryAssembly (void)
{
	MonoDomain* domain = mono_domain_get ();

	MONO_ARCH_SAVE_REGS;

	if (!domain->entry_assembly)
		return NULL;

	return mono_assembly_get_object (domain, domain->entry_assembly);
}

ICALL_EXPORT MonoReflectionAssembly*
ves_icall_System_Reflection_Assembly_GetCallingAssembly (void)
{
	MonoMethod *m;
	MonoMethod *dest;

	MONO_ARCH_SAVE_REGS;

	dest = NULL;
	mono_stack_walk_no_il (get_executing, &dest);
	m = dest;
	mono_stack_walk_no_il (get_caller, &dest);
	if (!dest)
		dest = m;
	return mono_assembly_get_object (mono_domain_get (), dest->klass->image->assembly);
}

ICALL_EXPORT MonoString *
ves_icall_System_MonoType_getFullName (MonoReflectionType *object, gboolean full_name,
				       gboolean assembly_qualified)
{
	MonoDomain *domain = mono_object_domain (object); 
	MonoTypeNameFormat format;
	MonoString *res;
	gchar *name;

	if (full_name)
		format = assembly_qualified ?
			MONO_TYPE_NAME_FORMAT_ASSEMBLY_QUALIFIED :
			MONO_TYPE_NAME_FORMAT_FULL_NAME;
	else
		format = MONO_TYPE_NAME_FORMAT_REFLECTION;
 
	name = mono_type_get_name_full (object->type, format);
	if (!name)
		return NULL;

	if (full_name && (object->type->type == MONO_TYPE_VAR || object->type->type == MONO_TYPE_MVAR)) {
		g_free (name);
		return NULL;
	}

	res = mono_string_new (domain, name);
	g_free (name);

	return res;
}

ICALL_EXPORT int
vell_icall_MonoType_get_core_clr_security_level (MonoReflectionType *this)
{
	MonoClass *klass = mono_class_from_mono_type (this->type);
	mono_class_init_or_throw (klass);
	return mono_security_core_clr_class_level (klass);
}

static void
fill_reflection_assembly_name (MonoDomain *domain, MonoReflectionAssemblyName *aname, MonoAssemblyName *name, const char *absolute, gboolean by_default_version, gboolean default_publickey, gboolean default_token)
{
	static MonoMethod *create_culture = NULL;
	gpointer args [2];
	guint32 pkey_len;
	const char *pkey_ptr;
	gchar *codebase;
	MonoBoolean assembly_ref = 0;

	MONO_ARCH_SAVE_REGS;

	MONO_OBJECT_SETREF (aname, name, mono_string_new (domain, name->name));
	aname->major = name->major;
	aname->minor = name->minor;
	aname->build = name->build;
	aname->flags = name->flags;
	aname->revision = name->revision;
	aname->hashalg = name->hash_alg;
	aname->versioncompat = 1; /* SameMachine (default) */
	aname->processor_architecture = name->arch;

	if (by_default_version)
		MONO_OBJECT_SETREF (aname, version, create_version (domain, name->major, name->minor, name->build, name->revision));

	codebase = NULL;
	if (absolute != NULL && *absolute != '\0') {
		const gchar *prepend = "file://";
		gchar *result;

		codebase = g_strdup (absolute);

#if HOST_WIN32
		{
			gint i;
			for (i = strlen (codebase) - 1; i >= 0; i--)
				if (codebase [i] == '\\')
					codebase [i] = '/';

			if (*codebase == '/' && *(codebase + 1) == '/') {
				prepend = "file:";
			} else {
				prepend = "file:///";
			}
		}
#endif
		result = g_strconcat (prepend, codebase, NULL);
		g_free (codebase);
		codebase = result;
	}

	if (codebase) {
		MONO_OBJECT_SETREF (aname, codebase, mono_string_new (domain, codebase));
		g_free (codebase);
	}

	if (!create_culture) {
		MonoMethodDesc *desc = mono_method_desc_new ("System.Globalization.CultureInfo:CreateCulture(string,bool)", TRUE);
		create_culture = mono_method_desc_search_in_image (desc, mono_defaults.corlib);
		g_assert (create_culture);
		mono_method_desc_free (desc);
	}

	if (name->culture) {
		args [0] = mono_string_new (domain, name->culture);
		args [1] = &assembly_ref;
		MONO_OBJECT_SETREF (aname, cultureInfo, mono_runtime_invoke (create_culture, NULL, args, NULL));
	}

	if (name->public_key) {
		pkey_ptr = (char*)name->public_key;
		pkey_len = mono_metadata_decode_blob_size (pkey_ptr, &pkey_ptr);

		MONO_OBJECT_SETREF (aname, publicKey, mono_array_new (domain, mono_defaults.byte_class, pkey_len));
		memcpy (mono_array_addr (aname->publicKey, guint8, 0), pkey_ptr, pkey_len);
		aname->flags |= ASSEMBLYREF_FULL_PUBLIC_KEY_FLAG;
	} else if (default_publickey) {
		MONO_OBJECT_SETREF (aname, publicKey, mono_array_new (domain, mono_defaults.byte_class, 0));
		aname->flags |= ASSEMBLYREF_FULL_PUBLIC_KEY_FLAG;
	}

	/* MonoAssemblyName keeps the public key token as an hexadecimal string */
	if (name->public_key_token [0]) {
		int i, j;
		char *p;

		MONO_OBJECT_SETREF (aname, keyToken, mono_array_new (domain, mono_defaults.byte_class, 8));
		p = mono_array_addr (aname->keyToken, char, 0);

		for (i = 0, j = 0; i < 8; i++) {
			*p = g_ascii_xdigit_value (name->public_key_token [j++]) << 4;
			*p |= g_ascii_xdigit_value (name->public_key_token [j++]);
			p++;
		}
	} else if (default_token) {
		MONO_OBJECT_SETREF (aname, keyToken, mono_array_new (domain, mono_defaults.byte_class, 0));
	}
}

ICALL_EXPORT MonoString *
ves_icall_System_Reflection_Assembly_get_fullName (MonoReflectionAssembly *assembly)
{
	MonoDomain *domain = mono_object_domain (assembly); 
	MonoAssembly *mass = assembly->assembly;
	MonoString *res;
	gchar *name;

	name = mono_stringify_assembly_name (&mass->aname);
	res = mono_string_new (domain, name);
	g_free (name);

	return res;
}

ICALL_EXPORT void
ves_icall_System_Reflection_Assembly_FillName (MonoReflectionAssembly *assembly, MonoReflectionAssemblyName *aname)
{
	gchar *absolute;
	MonoAssembly *mass = assembly->assembly;

	MONO_ARCH_SAVE_REGS;

	if (g_path_is_absolute (mass->image->name)) {
		fill_reflection_assembly_name (mono_object_domain (assembly),
			aname, &mass->aname, mass->image->name, TRUE,
			TRUE, TRUE);
		return;
	}
	absolute = g_build_filename (mass->basedir, mass->image->name, NULL);

	fill_reflection_assembly_name (mono_object_domain (assembly),
		aname, &mass->aname, absolute, TRUE, TRUE,
		TRUE);

	g_free (absolute);
}

ICALL_EXPORT void
ves_icall_System_Reflection_Assembly_InternalGetAssemblyName (MonoString *fname, MonoReflectionAssemblyName *aname)
{
	char *filename;
	MonoImageOpenStatus status = MONO_IMAGE_OK;
	gboolean res;
	MonoImage *image;
	MonoAssemblyName name;
	char *dirname

	MONO_ARCH_SAVE_REGS;

	filename = mono_string_to_utf8 (fname);

	dirname = g_path_get_dirname (filename);
	replace_shadow_path (mono_domain_get (), dirname, &filename);
	g_free (dirname);

	image = mono_image_open (filename, &status);

	if (!image){
		MonoException *exc;

		g_free (filename);
		if (status == MONO_IMAGE_IMAGE_INVALID)
			exc = mono_get_exception_bad_image_format2 (NULL, fname);
		else
			exc = mono_get_exception_file_not_found2 (NULL, fname);
		mono_raise_exception (exc);
	}

	res = mono_assembly_fill_assembly_name (image, &name);
	if (!res) {
		mono_image_close (image);
		g_free (filename);
		mono_raise_exception (mono_get_exception_argument ("assemblyFile", "The file does not contain a manifest"));
	}

	fill_reflection_assembly_name (mono_domain_get (), aname, &name, filename,
		TRUE, FALSE, TRUE);

	g_free (filename);
	mono_image_close (image);
}

ICALL_EXPORT MonoBoolean
ves_icall_System_Reflection_Assembly_LoadPermissions (MonoReflectionAssembly *assembly,
	char **minimum, guint32 *minLength, char **optional, guint32 *optLength, char **refused, guint32 *refLength)
{
	MonoBoolean result = FALSE;
	MonoDeclSecurityEntry entry;

	/* SecurityAction.RequestMinimum */
	if (mono_declsec_get_assembly_action (assembly->assembly, SECURITY_ACTION_REQMIN, &entry)) {
		*minimum = entry.blob;
		*minLength = entry.size;
		result = TRUE;
	}
	/* SecurityAction.RequestOptional */
	if (mono_declsec_get_assembly_action (assembly->assembly, SECURITY_ACTION_REQOPT, &entry)) {
		*optional = entry.blob;
		*optLength = entry.size;
		result = TRUE;
	}
	/* SecurityAction.RequestRefuse */
	if (mono_declsec_get_assembly_action (assembly->assembly, SECURITY_ACTION_REQREFUSE, &entry)) {
		*refused = entry.blob;
		*refLength = entry.size;
		result = TRUE;
	}

	return result;	
}

static MonoArray*
mono_module_get_types (MonoDomain *domain, MonoImage *image, MonoArray **exceptions, MonoBoolean exportedOnly)
{
	MonoArray *res;
	MonoClass *klass;
	MonoTableInfo *tdef = &image->tables [MONO_TABLE_TYPEDEF];
	int i, count;
	guint32 attrs, visibility;

	/* we start the count from 1 because we skip the special type <Module> */
	if (exportedOnly) {
		count = 0;
		for (i = 1; i < tdef->rows; ++i) {
			attrs = mono_metadata_decode_row_col (tdef, i, MONO_TYPEDEF_FLAGS);
			visibility = attrs & TYPE_ATTRIBUTE_VISIBILITY_MASK;
			if (visibility == TYPE_ATTRIBUTE_PUBLIC || visibility == TYPE_ATTRIBUTE_NESTED_PUBLIC)
				count++;
		}
	} else {
		count = tdef->rows - 1;
	}
	res = mono_array_new (domain, mono_defaults.monotype_class, count);
	*exceptions = mono_array_new (domain, mono_defaults.exception_class, count);
	count = 0;
	for (i = 1; i < tdef->rows; ++i) {
		attrs = mono_metadata_decode_row_col (tdef, i, MONO_TYPEDEF_FLAGS);
		visibility = attrs & TYPE_ATTRIBUTE_VISIBILITY_MASK;
		if (!exportedOnly || (visibility == TYPE_ATTRIBUTE_PUBLIC || visibility == TYPE_ATTRIBUTE_NESTED_PUBLIC)) {
			klass = mono_class_get (image, (i + 1) | MONO_TOKEN_TYPE_DEF);
			if (klass) {
				mono_array_setref (res, count, mono_type_get_object (domain, &klass->byval_arg));
			} else {
				MonoLoaderError *error;
				MonoException *ex;
				
				error = mono_loader_get_last_error ();
				g_assert (error != NULL);
	
				ex = mono_loader_error_prepare_exception (error);
				mono_array_setref (*exceptions, count, ex);
			}
			if (mono_loader_get_last_error ())
				mono_loader_clear_error ();
			count++;
		}
	}
	
	return res;
}

ICALL_EXPORT MonoArray*
ves_icall_System_Reflection_Assembly_GetTypes (MonoReflectionAssembly *assembly, MonoBoolean exportedOnly)
{
	MonoArray *res = NULL;
	MonoArray *exceptions = NULL;
	MonoImage *image = NULL;
	MonoTableInfo *table = NULL;
	MonoDomain *domain;
	GList *list = NULL;
	int i, len, ex_count;

	MONO_ARCH_SAVE_REGS;

	domain = mono_object_domain (assembly);

	g_assert (!assembly->assembly->dynamic);
	image = assembly->assembly->image;
	table = &image->tables [MONO_TABLE_FILE];
	res = mono_module_get_types (domain, image, &exceptions, exportedOnly);

	/* Append data from all modules in the assembly */
	for (i = 0; i < table->rows; ++i) {
		if (!(mono_metadata_decode_row_col (table, i, MONO_FILE_FLAGS) & FILE_CONTAINS_NO_METADATA)) {
			MonoImage *loaded_image = mono_assembly_load_module (image->assembly, i + 1);
			if (loaded_image) {
				MonoArray *ex2;
				MonoArray *res2 = mono_module_get_types (domain, loaded_image, &ex2, exportedOnly);
				/* Append the new types to the end of the array */
				if (mono_array_length (res2) > 0) {
					guint32 len1, len2;
					MonoArray *res3, *ex3;

					len1 = mono_array_length (res);
					len2 = mono_array_length (res2);

					res3 = mono_array_new (domain, mono_defaults.monotype_class, len1 + len2);
					mono_array_memcpy_refs (res3, 0, res, 0, len1);
					mono_array_memcpy_refs (res3, len1, res2, 0, len2);
					res = res3;

					ex3 = mono_array_new (domain, mono_defaults.monotype_class, len1 + len2);
					mono_array_memcpy_refs (ex3, 0, exceptions, 0, len1);
					mono_array_memcpy_refs (ex3, len1, ex2, 0, len2);
					exceptions = ex3;
				}
			}
		}
	}

	/* the ReflectionTypeLoadException must have all the types (Types property), 
	 * NULL replacing types which throws an exception. The LoaderException must
	 * contain all exceptions for NULL items.
	 */

	len = mono_array_length (res);

	ex_count = 0;
	for (i = 0; i < len; i++) {
		MonoReflectionType *t = mono_array_get (res, gpointer, i);
		MonoClass *klass;

		if (t) {
			klass = mono_type_get_class (t->type);
			if ((klass != NULL) && klass->exception_type) {
				/* keep the class in the list */
				list = g_list_append (list, klass);
				/* and replace Type with NULL */
				mono_array_setref (res, i, NULL);
			}
		} else {
			ex_count ++;
		}
	}

	if (list || ex_count) {
		GList *tmp = NULL;
		MonoException *exc = NULL;
		MonoArray *exl = NULL;
		int j, length = g_list_length (list) + ex_count;

		mono_loader_clear_error ();

		exl = mono_array_new (domain, mono_defaults.exception_class, length);
		/* Types for which mono_class_get () succeeded */
		for (i = 0, tmp = list; tmp; i++, tmp = tmp->next) {
			MonoException *exc = mono_class_get_exception_for_failure (tmp->data);
			mono_array_setref (exl, i, exc);
		}
		/* Types for which it don't */
		for (j = 0; j < mono_array_length (exceptions); ++j) {
			MonoException *exc = mono_array_get (exceptions, MonoException*, j);
			if (exc) {
				g_assert (i < length);
				mono_array_setref (exl, i, exc);
				i ++;
			}
		}
		g_list_free (list);
		list = NULL;

		exc = mono_get_exception_reflection_type_load (res, exl);
		mono_loader_clear_error ();
		mono_raise_exception (exc);
	}
		
	return res;
}

ICALL_EXPORT gboolean
ves_icall_System_Reflection_AssemblyName_ParseName (MonoReflectionAssemblyName *name, MonoString *assname)
{
	MonoAssemblyName aname;
	MonoDomain *domain = mono_object_domain (name);
	char *val;
	gboolean is_version_defined;
	gboolean is_token_defined;

	aname.public_key = NULL;
	val = mono_string_to_utf8 (assname);
	if (!mono_assembly_name_parse_full (val, &aname, TRUE, &is_version_defined, &is_token_defined)) {
		g_free ((guint8*) aname.public_key);
		g_free (val);
		return FALSE;
	}
	
	fill_reflection_assembly_name (domain, name, &aname, "", is_version_defined,
		FALSE, is_token_defined);

	mono_assembly_name_free (&aname);
	g_free ((guint8*) aname.public_key);
	g_free (val);

	return TRUE;
}

ICALL_EXPORT MonoReflectionType*
ves_icall_System_Reflection_Module_GetGlobalType (MonoReflectionModule *module)
{
	MonoDomain *domain = mono_object_domain (module); 
	MonoClass *klass;

	MONO_ARCH_SAVE_REGS;

	g_assert (module->image);

	if (module->image->dynamic && ((MonoDynamicImage*)(module->image))->initial_image)
		/* These images do not have a global type */
		return NULL;

	klass = mono_class_get (module->image, 1 | MONO_TOKEN_TYPE_DEF);
	return mono_type_get_object (domain, &klass->byval_arg);
}

ICALL_EXPORT void
ves_icall_System_Reflection_Module_Close (MonoReflectionModule *module)
{
	/*if (module->image)
		mono_image_close (module->image);*/
}

ICALL_EXPORT MonoString*
ves_icall_System_Reflection_Module_GetGuidInternal (MonoReflectionModule *module)
{
	MonoDomain *domain = mono_object_domain (module); 

	MONO_ARCH_SAVE_REGS;

	g_assert (module->image);
	return mono_string_new (domain, module->image->guid);
}

ICALL_EXPORT gpointer
ves_icall_System_Reflection_Module_GetHINSTANCE (MonoReflectionModule *module)
{
#ifdef HOST_WIN32
	if (module->image && module->image->is_module_handle)
		return module->image->raw_data;
#endif

	return (gpointer) (-1);
}

ICALL_EXPORT void
ves_icall_System_Reflection_Module_GetPEKind (MonoImage *image, gint32 *pe_kind, gint32 *machine)
{
	if (image->dynamic) {
		MonoDynamicImage *dyn = (MonoDynamicImage*)image;
		*pe_kind = dyn->pe_kind;
		*machine = dyn->machine;
	}
	else {
		*pe_kind = ((MonoCLIImageInfo*)(image->image_info))->cli_cli_header.ch_flags & 0x3;
		*machine = ((MonoCLIImageInfo*)(image->image_info))->cli_header.coff.coff_machine;
	}
}

ICALL_EXPORT gint32
ves_icall_System_Reflection_Module_GetMDStreamVersion (MonoImage *image)
{
	return (image->md_version_major << 16) | (image->md_version_minor);
}

ICALL_EXPORT MonoArray*
ves_icall_System_Reflection_Module_InternalGetTypes (MonoReflectionModule *module)
{
	MonoArray *exceptions;
	int i;

	MONO_ARCH_SAVE_REGS;

	if (!module->image)
		return mono_array_new (mono_object_domain (module), mono_defaults.monotype_class, 0);
	else {
		MonoArray *res = mono_module_get_types (mono_object_domain (module), module->image, &exceptions, FALSE);
		for (i = 0; i < mono_array_length (exceptions); ++i) {
			MonoException *ex = mono_array_get (exceptions, MonoException *, i);
			if (ex)
				mono_raise_exception (ex);
		}
		return res;
	}
}

static gboolean
mono_memberref_is_method (MonoImage *image, guint32 token)
{
	if (!image->dynamic) {
		guint32 cols [MONO_MEMBERREF_SIZE];
		const char *sig;
		mono_metadata_decode_row (&image->tables [MONO_TABLE_MEMBERREF], mono_metadata_token_index (token) - 1, cols, MONO_MEMBERREF_SIZE);
		sig = mono_metadata_blob_heap (image, cols [MONO_MEMBERREF_SIGNATURE]);
		mono_metadata_decode_blob_size (sig, &sig);
		return (*sig != 0x6);
	} else {
		MonoClass *handle_class;

		if (!mono_lookup_dynamic_token_class (image, token, FALSE, &handle_class, NULL))
			return FALSE;

		return mono_defaults.methodhandle_class == handle_class;
	}
}

static void
init_generic_context_from_args (MonoGenericContext *context, MonoArray *type_args, MonoArray *method_args)
{
	if (type_args)
		context->class_inst = mono_metadata_get_generic_inst (mono_array_length (type_args),
								      mono_array_addr (type_args, MonoType*, 0));
	else
		context->class_inst = NULL;
	if (method_args)
		context->method_inst = mono_metadata_get_generic_inst (mono_array_length (method_args),
								       mono_array_addr (method_args, MonoType*, 0));
	else
		context->method_inst = NULL;
}

ICALL_EXPORT MonoType*
ves_icall_System_Reflection_Module_ResolveTypeToken (MonoImage *image, guint32 token, MonoArray *type_args, MonoArray *method_args, MonoResolveTokenError *error)
{
	MonoClass *klass;
	int table = mono_metadata_token_table (token);
	int index = mono_metadata_token_index (token);
	MonoGenericContext context;

	*error = ResolveTokenError_Other;

	/* Validate token */
	if ((table != MONO_TABLE_TYPEDEF) && (table != MONO_TABLE_TYPEREF) && 
		(table != MONO_TABLE_TYPESPEC)) {
		*error = ResolveTokenError_BadTable;
		return NULL;
	}

	if (image->dynamic) {
		if ((table == MONO_TABLE_TYPEDEF) || (table == MONO_TABLE_TYPEREF)) {
			klass = mono_lookup_dynamic_token_class (image, token, FALSE, NULL, NULL);
			return klass ? &klass->byval_arg : NULL;
		}

		init_generic_context_from_args (&context, type_args, method_args);
		klass = mono_lookup_dynamic_token_class (image, token, FALSE, NULL, &context);
		return klass ? &klass->byval_arg : NULL;
	}

	if ((index <= 0) || (index > image->tables [table].rows)) {
		*error = ResolveTokenError_OutOfRange;
		return NULL;
	}

	init_generic_context_from_args (&context, type_args, method_args);
	klass = mono_class_get_full (image, token, &context);

	if (mono_loader_get_last_error ())
		mono_raise_exception (mono_loader_error_prepare_exception (mono_loader_get_last_error ()));

	if (klass)
		return &klass->byval_arg;
	else
		return NULL;
}

ICALL_EXPORT MonoMethod*
ves_icall_System_Reflection_Module_ResolveMethodToken (MonoImage *image, guint32 token, MonoArray *type_args, MonoArray *method_args, MonoResolveTokenError *error)
{
	int table = mono_metadata_token_table (token);
	int index = mono_metadata_token_index (token);
	MonoGenericContext context;
	MonoMethod *method;

	*error = ResolveTokenError_Other;

	/* Validate token */
	if ((table != MONO_TABLE_METHOD) && (table != MONO_TABLE_METHODSPEC) && 
		(table != MONO_TABLE_MEMBERREF)) {
		*error = ResolveTokenError_BadTable;
		return NULL;
	}

	if (image->dynamic) {
		if (table == MONO_TABLE_METHOD)
			return mono_lookup_dynamic_token_class (image, token, FALSE, NULL, NULL);

		if ((table == MONO_TABLE_MEMBERREF) && !(mono_memberref_is_method (image, token))) {
			*error = ResolveTokenError_BadTable;
			return NULL;
		}

		init_generic_context_from_args (&context, type_args, method_args);
		return mono_lookup_dynamic_token_class (image, token, FALSE, NULL, &context);
	}

	if ((index <= 0) || (index > image->tables [table].rows)) {
		*error = ResolveTokenError_OutOfRange;
		return NULL;
	}
	if ((table == MONO_TABLE_MEMBERREF) && (!mono_memberref_is_method (image, token))) {
		*error = ResolveTokenError_BadTable;
		return NULL;
	}

	init_generic_context_from_args (&context, type_args, method_args);
	method = mono_get_method_full (image, token, NULL, &context);

	if (mono_loader_get_last_error ())
		mono_raise_exception (mono_loader_error_prepare_exception (mono_loader_get_last_error ()));

	return method;
}

ICALL_EXPORT MonoString*
ves_icall_System_Reflection_Module_ResolveStringToken (MonoImage *image, guint32 token, MonoResolveTokenError *error)
{
	int index = mono_metadata_token_index (token);

	*error = ResolveTokenError_Other;

	/* Validate token */
	if (mono_metadata_token_code (token) != MONO_TOKEN_STRING) {
		*error = ResolveTokenError_BadTable;
		return NULL;
	}

	if (image->dynamic)
		return mono_lookup_dynamic_token_class (image, token, FALSE, NULL, NULL);

	if ((index <= 0) || (index >= image->heap_us.size)) {
		*error = ResolveTokenError_OutOfRange;
		return NULL;
	}

	/* FIXME: What to do if the index points into the middle of a string ? */

	return mono_ldstr (mono_domain_get (), image, index);
}

ICALL_EXPORT MonoClassField*
ves_icall_System_Reflection_Module_ResolveFieldToken (MonoImage *image, guint32 token, MonoArray *type_args, MonoArray *method_args, MonoResolveTokenError *error)
{
	MonoClass *klass;
	int table = mono_metadata_token_table (token);
	int index = mono_metadata_token_index (token);
	MonoGenericContext context;
	MonoClassField *field;

	*error = ResolveTokenError_Other;

	/* Validate token */
	if ((table != MONO_TABLE_FIELD) && (table != MONO_TABLE_MEMBERREF)) {
		*error = ResolveTokenError_BadTable;
		return NULL;
	}

	if (image->dynamic) {
		if (table == MONO_TABLE_FIELD)
			return mono_lookup_dynamic_token_class (image, token, FALSE, NULL, NULL);

		if (mono_memberref_is_method (image, token)) {
			*error = ResolveTokenError_BadTable;
			return NULL;
		}

		init_generic_context_from_args (&context, type_args, method_args);
		return mono_lookup_dynamic_token_class (image, token, FALSE, NULL, &context);
	}

	if ((index <= 0) || (index > image->tables [table].rows)) {
		*error = ResolveTokenError_OutOfRange;
		return NULL;
	}
	if ((table == MONO_TABLE_MEMBERREF) && (mono_memberref_is_method (image, token))) {
		*error = ResolveTokenError_BadTable;
		return NULL;
	}

	init_generic_context_from_args (&context, type_args, method_args);
	field = mono_field_from_token (image, token, &klass, &context);

	if (mono_loader_get_last_error ())
		mono_raise_exception (mono_loader_error_prepare_exception (mono_loader_get_last_error ()));
	
	return field;
}


ICALL_EXPORT MonoObject*
ves_icall_System_Reflection_Module_ResolveMemberToken (MonoImage *image, guint32 token, MonoArray *type_args, MonoArray *method_args, MonoResolveTokenError *error)
{
	int table = mono_metadata_token_table (token);

	*error = ResolveTokenError_Other;

	switch (table) {
	case MONO_TABLE_TYPEDEF:
	case MONO_TABLE_TYPEREF:
	case MONO_TABLE_TYPESPEC: {
		MonoType *t = ves_icall_System_Reflection_Module_ResolveTypeToken (image, token, type_args, method_args, error);
		if (t)
			return (MonoObject*)mono_type_get_object (mono_domain_get (), t);
		else
			return NULL;
	}
	case MONO_TABLE_METHOD:
	case MONO_TABLE_METHODSPEC: {
		MonoMethod *m = ves_icall_System_Reflection_Module_ResolveMethodToken (image, token, type_args, method_args, error);
		if (m)
			return (MonoObject*)mono_method_get_object (mono_domain_get (), m, m->klass);
		else
			return NULL;
	}		
	case MONO_TABLE_FIELD: {
		MonoClassField *f = ves_icall_System_Reflection_Module_ResolveFieldToken (image, token, type_args, method_args, error);
		if (f)
			return (MonoObject*)mono_field_get_object (mono_domain_get (), f->parent, f);
		else
			return NULL;
	}
	case MONO_TABLE_MEMBERREF:
		if (mono_memberref_is_method (image, token)) {
			MonoMethod *m = ves_icall_System_Reflection_Module_ResolveMethodToken (image, token, type_args, method_args, error);
			if (m)
				return (MonoObject*)mono_method_get_object (mono_domain_get (), m, m->klass);
			else
				return NULL;
		}
		else {
			MonoClassField *f = ves_icall_System_Reflection_Module_ResolveFieldToken (image, token, type_args, method_args, error);
			if (f)
				return (MonoObject*)mono_field_get_object (mono_domain_get (), f->parent, f);
			else
				return NULL;
		}
		break;

	default:
		*error = ResolveTokenError_BadTable;
	}

	return NULL;
}

ICALL_EXPORT MonoArray*
ves_icall_System_Reflection_Module_ResolveSignature (MonoImage *image, guint32 token, MonoResolveTokenError *error)
{
	int table = mono_metadata_token_table (token);
	int idx = mono_metadata_token_index (token);
	MonoTableInfo *tables = image->tables;
	guint32 sig, len;
	const char *ptr;
	MonoArray *res;

	*error = ResolveTokenError_OutOfRange;

	/* FIXME: Support other tables ? */
	if (table != MONO_TABLE_STANDALONESIG)
		return NULL;

	if (image->dynamic)
		return NULL;

	if ((idx == 0) || (idx > tables [MONO_TABLE_STANDALONESIG].rows))
		return NULL;

	sig = mono_metadata_decode_row_col (&tables [MONO_TABLE_STANDALONESIG], idx - 1, 0);

	ptr = mono_metadata_blob_heap (image, sig);
	len = mono_metadata_decode_blob_size (ptr, &ptr);

	res = mono_array_new (mono_domain_get (), mono_defaults.byte_class, len);
	memcpy (mono_array_addr (res, guint8, 0), ptr, len);
	return res;
}

ICALL_EXPORT MonoReflectionType*
ves_icall_ModuleBuilder_create_modified_type (MonoReflectionTypeBuilder *tb, MonoString *smodifiers)
{
	MonoClass *klass;
	int isbyref = 0, rank;
	char *str = mono_string_to_utf8 (smodifiers);
	char *p;

	MONO_ARCH_SAVE_REGS;

	klass = mono_class_from_mono_type (tb->type.type);
	p = str;
	/* logic taken from mono_reflection_parse_type(): keep in sync */
	while (*p) {
		switch (*p) {
		case '&':
			if (isbyref) { /* only one level allowed by the spec */
				g_free (str);
				return NULL;
			}
			isbyref = 1;
			p++;
			g_free (str);
			return mono_type_get_object (mono_object_domain (tb), &klass->this_arg);
			break;
		case '*':
			klass = mono_ptr_class_get (&klass->byval_arg);
			mono_class_init (klass);
			p++;
			break;
		case '[':
			rank = 1;
			p++;
			while (*p) {
				if (*p == ']')
					break;
				if (*p == ',')
					rank++;
				else if (*p != '*') { /* '*' means unknown lower bound */
					g_free (str);
					return NULL;
				}
				++p;
			}
			if (*p != ']') {
				g_free (str);
				return NULL;
			}
			p++;
			klass = mono_array_class_get (klass, rank);
			mono_class_init (klass);
			break;
		default:
			break;
		}
	}
	g_free (str);
	return mono_type_get_object (mono_object_domain (tb), &klass->byval_arg);
}

ICALL_EXPORT MonoBoolean
ves_icall_Type_IsArrayImpl (MonoReflectionType *t)
{
	MonoType *type;
	MonoBoolean res;

	MONO_ARCH_SAVE_REGS;

	type = t->type;
	res = !type->byref && (type->type == MONO_TYPE_ARRAY || type->type == MONO_TYPE_SZARRAY);

	return res;
}

static void
check_for_invalid_type (MonoClass *klass)
{
	char *name;
	MonoString *str;
	if (klass->byval_arg.type != MONO_TYPE_TYPEDBYREF)
		return;

	name = mono_type_get_full_name (klass);
	str =  mono_string_new (mono_domain_get (), name);
	g_free (name);
	mono_raise_exception ((MonoException*)mono_get_exception_type_load (str, NULL));

}
ICALL_EXPORT MonoReflectionType *
ves_icall_Type_make_array_type (MonoReflectionType *type, int rank)
{
	MonoClass *klass, *aklass;

	MONO_ARCH_SAVE_REGS;

	klass = mono_class_from_mono_type (type->type);
	check_for_invalid_type (klass);

	if (rank == 0) //single dimentional array
		aklass = mono_array_class_get (klass, 1);
	else
		aklass = mono_bounded_array_class_get (klass, rank, TRUE);

	return mono_type_get_object (mono_object_domain (type), &aklass->byval_arg);
}

ICALL_EXPORT MonoReflectionType *
ves_icall_Type_make_byref_type (MonoReflectionType *type)
{
	MonoClass *klass;

	MONO_ARCH_SAVE_REGS;

	klass = mono_class_from_mono_type (type->type);
	mono_class_init_or_throw (klass);
	check_for_invalid_type (klass);

	return mono_type_get_object (mono_object_domain (type), &klass->this_arg);
}

ICALL_EXPORT MonoReflectionType *
ves_icall_Type_MakePointerType (MonoReflectionType *type)
{
	MonoClass *klass, *pklass;

	klass = mono_class_from_mono_type (type->type);
	mono_class_init_or_throw (klass);
	check_for_invalid_type (klass);

	pklass = mono_ptr_class_get (type->type);

	return mono_type_get_object (mono_object_domain (type), &pklass->byval_arg);
}

ICALL_EXPORT MonoObject *
ves_icall_System_Delegate_CreateDelegate_internal (MonoReflectionType *type, MonoObject *target,
						   MonoReflectionMethod *info, MonoBoolean throwOnBindFailure)
{
	MonoClass *delegate_class = mono_class_from_mono_type (type->type);
	MonoObject *delegate;
	gpointer func;
	MonoMethod *method = info->method;

	MONO_ARCH_SAVE_REGS;

	mono_class_init_or_throw (delegate_class);

	mono_assert (delegate_class->parent == mono_defaults.multicastdelegate_class);

	if (mono_security_core_clr_enabled ()) {
		if (!mono_security_core_clr_ensure_delegate_creation (method, throwOnBindFailure))
			return NULL;
	}

	delegate = mono_object_new (mono_object_domain (type), delegate_class);

	if (method->dynamic) {
		/* Creating a trampoline would leak memory */
		func = mono_compile_method (method);
	} else {
		if (target && method->flags & METHOD_ATTRIBUTE_VIRTUAL && method->klass != mono_object_class (target))
			method = mono_object_get_virtual_method (target, method);
		func = mono_create_ftnptr (mono_domain_get (),
			mono_runtime_create_jump_trampoline (mono_domain_get (), method, TRUE));
	}

	mono_delegate_ctor_with_method (delegate, target, func, method);

	return delegate;
}

ICALL_EXPORT void
ves_icall_System_Delegate_SetMulticastInvoke (MonoDelegate *this)
{
	/* Reset the invoke impl to the default one */
	this->invoke_impl = mono_runtime_create_delegate_trampoline (this->object.vtable->klass);
}

/*
 * Magic number to convert a time which is relative to
 * Jan 1, 1970 into a value which is relative to Jan 1, 0001.
 */
#define	EPOCH_ADJUST	((guint64)62135596800LL)

/*
 * Magic number to convert FILETIME base Jan 1, 1601 to DateTime - base Jan, 1, 0001
 */
#define FILETIME_ADJUST ((guint64)504911232000000000LL)

#ifdef HOST_WIN32
/* convert a SYSTEMTIME which is of the form "last thursday in october" to a real date */
static void
convert_to_absolute_date(SYSTEMTIME *date)
{
#define IS_LEAP(y) ((y % 4) == 0 && ((y % 100) != 0 || (y % 400) == 0))
	static int days_in_month[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	static int leap_days_in_month[] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	/* from the calendar FAQ */
	int a = (14 - date->wMonth) / 12;
	int y = date->wYear - a;
	int m = date->wMonth + 12 * a - 2;
	int d = (1 + y + y/4 - y/100 + y/400 + (31*m)/12) % 7;

	/* d is now the day of the week for the first of the month (0 == Sunday) */

	int day_of_week = date->wDayOfWeek;

	/* set day_in_month to the first day in the month which falls on day_of_week */    
	int day_in_month = 1 + (day_of_week - d);
	if (day_in_month <= 0)
		day_in_month += 7;

	/* wDay is 1 for first weekday in month, 2 for 2nd ... 5 means last - so work that out allowing for days in the month */
	date->wDay = day_in_month + (date->wDay - 1) * 7;
	if (date->wDay > (IS_LEAP(date->wYear) ? leap_days_in_month[date->wMonth - 1] : days_in_month[date->wMonth - 1]))
		date->wDay -= 7;
}
#endif

#ifndef HOST_WIN32
/*
 * Return's the offset from GMT of a local time.
 * 
 *  tm is a local time
 *  t  is the same local time as seconds.
 */
static int 
gmt_offset(struct tm *tm, time_t t)
{
#if defined (HAVE_TM_GMTOFF)
	return tm->tm_gmtoff;
#else
	struct tm g;
	time_t t2;
	g = *gmtime(&t);
	g.tm_isdst = tm->tm_isdst;
	t2 = mktime(&g);
	return (int)difftime(t, t2);
#endif
}
#endif
/*
 * This is heavily based on zdump.c from glibc 2.2.
 *
 *  * data[0]:  start of daylight saving time (in DateTime ticks).
 *  * data[1]:  end of daylight saving time (in DateTime ticks).
 *  * data[2]:  utcoffset (in TimeSpan ticks).
 *  * data[3]:  additional offset when daylight saving (in TimeSpan ticks).
 *  * name[0]:  name of this timezone when not daylight saving.
 *  * name[1]:  name of this timezone when daylight saving.
 *
 *  FIXME: This only works with "standard" Unix dates (years between 1900 and 2100) while
 *         the class library allows years between 1 and 9999.
 *
 *  Returns true on success and zero on failure.
 */
ICALL_EXPORT guint32
ves_icall_System_CurrentSystemTimeZone_GetTimeZoneData (guint32 year, MonoArray **data, MonoArray **names)
{
#ifndef HOST_WIN32
	MonoDomain *domain = mono_domain_get ();
	struct tm start, tt;
	time_t t;

	long int gmtoff;
	int is_daylight = 0, day;
	char tzone [64];

	MONO_ARCH_SAVE_REGS;

	MONO_CHECK_ARG_NULL (data);
	MONO_CHECK_ARG_NULL (names);

	mono_gc_wbarrier_generic_store (data, (MonoObject*) mono_array_new (domain, mono_defaults.int64_class, 4));
	mono_gc_wbarrier_generic_store (names, (MonoObject*) mono_array_new (domain, mono_defaults.string_class, 2));

	/* 
	 * no info is better than crashing: we'll need our own tz data
	 * to make this work properly, anyway. The range is probably
	 * reduced to 1970 .. 2037 because that is what mktime is
	 * guaranteed to support (we get into an infinite loop
	 * otherwise).
	 */

	memset (&start, 0, sizeof (start));

	start.tm_mday = 1;
	start.tm_year = year-1900;

	t = mktime (&start);

	if ((year < 1970) || (year > 2037) || (t == -1)) {
		t = time (NULL);
		tt = *localtime (&t);
		strftime (tzone, sizeof (tzone), "%Z", &tt);
		mono_array_setref ((*names), 0, mono_string_new (domain, tzone));
		mono_array_setref ((*names), 1, mono_string_new (domain, tzone));
		return 1;
	}

	gmtoff = gmt_offset (&start, t);

	/* For each day of the year, calculate the tm_gmtoff. */
	for (day = 0; day < 365; day++) {

		t += 3600*24;
		tt = *localtime (&t);

		/* Daylight saving starts or ends here. */
		if (gmt_offset (&tt, t) != gmtoff) {
			struct tm tt1;
			time_t t1;

			/* Try to find the exact hour when daylight saving starts/ends. */
			t1 = t;
			do {
				t1 -= 3600;
				tt1 = *localtime (&t1);
			} while (gmt_offset (&tt1, t1) != gmtoff);

			/* Try to find the exact minute when daylight saving starts/ends. */
			do {
				t1 += 60;
				tt1 = *localtime (&t1);
			} while (gmt_offset (&tt1, t1) == gmtoff);
			t1+=gmtoff;
			strftime (tzone, sizeof (tzone), "%Z", &tt);
			
			/* Write data, if we're already in daylight saving, we're done. */
			if (is_daylight) {
				mono_array_setref ((*names), 0, mono_string_new (domain, tzone));
				mono_array_set ((*data), gint64, 1, ((gint64)t1 + EPOCH_ADJUST) * 10000000L);
				return 1;
			} else {
				struct tm end;
				time_t te;
				
				memset (&end, 0, sizeof (end));
				end.tm_year = year-1900 + 1;
				end.tm_mday = 1;
				
				te = mktime (&end);
				
				mono_array_setref ((*names), 1, mono_string_new (domain, tzone));
				mono_array_set ((*data), gint64, 0, ((gint64)t1 + EPOCH_ADJUST) * 10000000L);
				mono_array_setref ((*names), 0, mono_string_new (domain, tzone));
				mono_array_set ((*data), gint64, 1, ((gint64)te + EPOCH_ADJUST) * 10000000L);
				is_daylight = 1;
			}

			/* This is only set once when we enter daylight saving. */
			mono_array_set ((*data), gint64, 2, (gint64)gmtoff * 10000000L);
			mono_array_set ((*data), gint64, 3, (gint64)(gmt_offset (&tt, t) - gmtoff) * 10000000L);

			gmtoff = gmt_offset (&tt, t);
		}
	}

	if (!is_daylight) {
		strftime (tzone, sizeof (tzone), "%Z", &tt);
		mono_array_setref ((*names), 0, mono_string_new (domain, tzone));
		mono_array_setref ((*names), 1, mono_string_new (domain, tzone));
		mono_array_set ((*data), gint64, 0, 0);
		mono_array_set ((*data), gint64, 1, 0);
		mono_array_set ((*data), gint64, 2, (gint64) gmtoff * 10000000L);
		mono_array_set ((*data), gint64, 3, 0);
	}

	return 1;
#else
	MonoDomain *domain = mono_domain_get ();
	TIME_ZONE_INFORMATION tz_info;
	FILETIME ft;
	int i;
	int err, tz_id;

	tz_id = GetTimeZoneInformation (&tz_info);
	if (tz_id == TIME_ZONE_ID_INVALID)
		return 0;

	MONO_CHECK_ARG_NULL (data);
	MONO_CHECK_ARG_NULL (names);

	mono_gc_wbarrier_generic_store (data, mono_array_new (domain, mono_defaults.int64_class, 4));
	mono_gc_wbarrier_generic_store (names, mono_array_new (domain, mono_defaults.string_class, 2));

	for (i = 0; i < 32; ++i)
		if (!tz_info.DaylightName [i])
			break;
	mono_array_setref ((*names), 1, mono_string_new_utf16 (domain, tz_info.DaylightName, i));
	for (i = 0; i < 32; ++i)
		if (!tz_info.StandardName [i])
			break;
	mono_array_setref ((*names), 0, mono_string_new_utf16 (domain, tz_info.StandardName, i));

	if ((year <= 1601) || (year > 30827)) {
		/*
		 * According to MSDN, the MS time functions can't handle dates outside
		 * this interval.
		 */
		return 1;
	}

	/* even if the timezone has no daylight savings it may have Bias (e.g. GMT+13 it seems) */
	if (tz_id != TIME_ZONE_ID_UNKNOWN) {
		tz_info.StandardDate.wYear = year;
		convert_to_absolute_date(&tz_info.StandardDate);
		err = SystemTimeToFileTime (&tz_info.StandardDate, &ft);
		//g_assert(err);
		if (err == 0)
			return 0;
		
		mono_array_set ((*data), gint64, 1, FILETIME_ADJUST + (((guint64)ft.dwHighDateTime<<32) | ft.dwLowDateTime));
		tz_info.DaylightDate.wYear = year;
		convert_to_absolute_date(&tz_info.DaylightDate);
		err = SystemTimeToFileTime (&tz_info.DaylightDate, &ft);
		//g_assert(err);
		if (err == 0)
			return 0;
		
		mono_array_set ((*data), gint64, 0, FILETIME_ADJUST + (((guint64)ft.dwHighDateTime<<32) | ft.dwLowDateTime));
	}
	mono_array_set ((*data), gint64, 2, (tz_info.Bias + tz_info.StandardBias) * -600000000LL);
	mono_array_set ((*data), gint64, 3, (tz_info.DaylightBias - tz_info.StandardBias) * -600000000LL);

	return 1;
#endif
}

ICALL_EXPORT gpointer
ves_icall_System_Object_obj_address (MonoObject *this) 
{
	MONO_ARCH_SAVE_REGS;

	return this;
}

/* System.Buffer */

static inline gint32 
mono_array_get_byte_length (MonoArray *array)
{
	MonoClass *klass;
	int length;
	int i;

	klass = array->obj.vtable->klass;

	if (array->bounds == NULL)
		length = array->max_length;
	else {
		length = 1;
		for (i = 0; i < klass->rank; ++ i)
			length *= array->bounds [i].length;
	}

	switch (klass->element_class->byval_arg.type) {
	case MONO_TYPE_I1:
	case MONO_TYPE_U1:
	case MONO_TYPE_BOOLEAN:
		return length;
	case MONO_TYPE_I2:
	case MONO_TYPE_U2:
	case MONO_TYPE_CHAR:
		return length << 1;
	case MONO_TYPE_I4:
	case MONO_TYPE_U4:
	case MONO_TYPE_R4:
		return length << 2;
	case MONO_TYPE_I:
	case MONO_TYPE_U:
		return length * sizeof (gpointer);
	case MONO_TYPE_I8:
	case MONO_TYPE_U8:
	case MONO_TYPE_R8:
		return length << 3;
	default:
		return -1;
	}
}

ICALL_EXPORT gint32 
ves_icall_System_Buffer_ByteLengthInternal (MonoArray *array) 
{
	MONO_ARCH_SAVE_REGS;

	return mono_array_get_byte_length (array);
}

ICALL_EXPORT gint8 
ves_icall_System_Buffer_GetByteInternal (MonoArray *array, gint32 idx) 
{
	MONO_ARCH_SAVE_REGS;

	return mono_array_get (array, gint8, idx);
}

ICALL_EXPORT void 
ves_icall_System_Buffer_SetByteInternal (MonoArray *array, gint32 idx, gint8 value) 
{
	MONO_ARCH_SAVE_REGS;

	mono_array_set (array, gint8, idx, value);
}

ICALL_EXPORT MonoBoolean
ves_icall_System_Buffer_BlockCopyInternal (MonoArray *src, gint32 src_offset, MonoArray *dest, gint32 dest_offset, gint32 count) 
{
	guint8 *src_buf, *dest_buf;

	MONO_ARCH_SAVE_REGS;

	/* This is called directly from the class libraries without going through the managed wrapper */
	MONO_CHECK_ARG_NULL (src);
	MONO_CHECK_ARG_NULL (dest);

	/* watch out for integer overflow */
	if ((src_offset > mono_array_get_byte_length (src) - count) || (dest_offset > mono_array_get_byte_length (dest) - count))
		return FALSE;

	src_buf = (guint8 *)src->vector + src_offset;
	dest_buf = (guint8 *)dest->vector + dest_offset;

	if (src != dest)
		memcpy (dest_buf, src_buf, count);
	else
		mono_gc_memmove (dest_buf, src_buf, count); /* Source and dest are the same array */

	return TRUE;
}

#ifndef DISABLE_REMOTING
ICALL_EXPORT MonoObject *
ves_icall_Remoting_RealProxy_GetTransparentProxy (MonoObject *this, MonoString *class_name)
{
	MonoDomain *domain = mono_object_domain (this); 
	MonoObject *res;
	MonoRealProxy *rp = ((MonoRealProxy *)this);
	MonoTransparentProxy *tp;
	MonoType *type;
	MonoClass *klass;

	MONO_ARCH_SAVE_REGS;

	res = mono_object_new (domain, mono_defaults.transparent_proxy_class);
	tp = (MonoTransparentProxy*) res;
	
	MONO_OBJECT_SETREF (tp, rp, rp);
	type = ((MonoReflectionType *)rp->class_to_proxy)->type;
	klass = mono_class_from_mono_type (type);

	tp->custom_type_info = (mono_object_isinst (this, mono_defaults.iremotingtypeinfo_class) != NULL);
	tp->remote_class = mono_remote_class (domain, class_name, klass);

	res->vtable = mono_remote_class_vtable (domain, tp->remote_class, rp);
	return res;
}

ICALL_EXPORT MonoReflectionType *
ves_icall_Remoting_RealProxy_InternalGetProxyType (MonoTransparentProxy *tp)
{
	return mono_type_get_object (mono_object_domain (tp), &tp->remote_class->proxy_class->byval_arg);
}
#endif

/* System.Environment */

MonoString*
ves_icall_System_Environment_get_UserName (void)
{
	MONO_ARCH_SAVE_REGS;

	/* using glib is more portable */
	return mono_string_new (mono_domain_get (), g_get_user_name ());
}


ICALL_EXPORT MonoString *
ves_icall_System_Environment_get_MachineName (void)
{
#if defined (HOST_WIN32)
	gunichar2 *buf;
	guint32 len;
	MonoString *result;

	len = MAX_COMPUTERNAME_LENGTH + 1;
	buf = g_new (gunichar2, len);

	result = NULL;
	if (GetComputerName (buf, (PDWORD) &len))
		result = mono_string_new_utf16 (mono_domain_get (), buf, len);

	g_free (buf);
	return result;
#elif !defined(DISABLE_SOCKETS)
	gchar buf [256];
	MonoString *result;

	if (gethostname (buf, sizeof (buf)) == 0)
		result = mono_string_new (mono_domain_get (), buf);
	else
		result = NULL;
	
	return result;
#else
	return mono_string_new (mono_domain_get (), "mono");
#endif
}

ICALL_EXPORT int
ves_icall_System_Environment_get_Platform (void)
{
#if defined (TARGET_WIN32)
	/* Win32NT */
	return 2;
#elif defined(__MACH__)
	/* OSX */
	//
	// Notice that the value is hidden from user code, and only exposed
	// to mscorlib.   This is due to Mono's Unix/MacOS code predating the
	// define and making assumptions based on Unix/128/4 values before there
	// was a MacOS define.    Lots of code would assume that not-Unix meant
	// Windows, but in this case, it would be OSX. 
	//
	return 6;
#else
	/* Unix */
	return 4;
#endif
}

ICALL_EXPORT MonoString *
ves_icall_System_Environment_get_NewLine (void)
{
	MONO_ARCH_SAVE_REGS;

#if defined (HOST_WIN32)
	return mono_string_new (mono_domain_get (), "\r\n");
#else
	return mono_string_new (mono_domain_get (), "\n");
#endif
}

ICALL_EXPORT MonoString *
ves_icall_System_Environment_GetEnvironmentVariable (MonoString *name)
{
	const gchar *value;
	gchar *utf8_name;

	MONO_ARCH_SAVE_REGS;

	if (name == NULL)
		return NULL;

	utf8_name = mono_string_to_utf8 (name);	/* FIXME: this should be ascii */
	value = g_getenv (utf8_name);

	g_free (utf8_name);

	if (value == 0)
		return NULL;
	
	return mono_string_new (mono_domain_get (), value);
}

/*
 * There is no standard way to get at environ.
 */
#ifndef _MSC_VER
#ifndef __MINGW32_VERSION
#if defined(__APPLE__) && !defined (__arm__)
/* Apple defines this in crt_externs.h but doesn't provide that header for 
 * arm-apple-darwin9.  We'll manually define the symbol on Apple as it does
 * in fact exist on all implementations (so far) 
 */
gchar ***_NSGetEnviron(void);
#define environ (*_NSGetEnviron())
#else
extern
char **environ;
#endif
#endif
#endif

ICALL_EXPORT MonoArray *
ves_icall_System_Environment_GetEnvironmentVariableNames (void)
{
#ifdef HOST_WIN32
	MonoArray *names;
	MonoDomain *domain;
	MonoString *str;
	WCHAR* env_strings;
	WCHAR* env_string;
	WCHAR* equal_str;
	int n = 0;

	env_strings = GetEnvironmentStrings();

	if (env_strings) {
		env_string = env_strings;
		while (*env_string != '\0') {
		/* weird case that MS seems to skip */
			if (*env_string != '=')
				n++;
			while (*env_string != '\0')
				env_string++;
			env_string++;
		}
	}

	domain = mono_domain_get ();
	names = mono_array_new (domain, mono_defaults.string_class, n);

	if (env_strings) {
		n = 0;
		env_string = env_strings;
		while (*env_string != '\0') {
			/* weird case that MS seems to skip */
			if (*env_string != '=') {
				equal_str = wcschr(env_string, '=');
				g_assert(equal_str);
				str = mono_string_new_utf16 (domain, env_string, equal_str-env_string);
				mono_array_setref (names, n, str);
				n++;
			}
			while (*env_string != '\0')
				env_string++;
			env_string++;
		}

		FreeEnvironmentStrings (env_strings);
	}

	return names;

#else
	MonoArray *names;
	MonoDomain *domain;
	MonoString *str;
	gchar **e, **parts;
	int n;

	MONO_ARCH_SAVE_REGS;

	n = 0;
	for (e = environ; *e != 0; ++ e)
		++ n;

	domain = mono_domain_get ();
	names = mono_array_new (domain, mono_defaults.string_class, n);

	n = 0;
	for (e = environ; *e != 0; ++ e) {
		parts = g_strsplit (*e, "=", 2);
		if (*parts != 0) {
			str = mono_string_new (domain, *parts);
			mono_array_setref (names, n, str);
		}

		g_strfreev (parts);

		++ n;
	}

	return names;
#endif
}

/*
 * If your platform lacks setenv/unsetenv, you must upgrade your glib.
 */
#if !GLIB_CHECK_VERSION(2,4,0)
#define g_setenv(a,b,c)   setenv(a,b,c)
#define g_unsetenv(a) unsetenv(a)
#endif

ICALL_EXPORT void
ves_icall_System_Environment_InternalSetEnvironmentVariable (MonoString *name, MonoString *value)
{
	MonoError error;
#ifdef HOST_WIN32

	gunichar2 *utf16_name, *utf16_value;
#else
	gchar *utf8_name, *utf8_value;
#endif

	MONO_ARCH_SAVE_REGS;
	
#ifdef HOST_WIN32
	utf16_name = mono_string_to_utf16 (name);
	if ((value == NULL) || (mono_string_length (value) == 0) || (mono_string_chars (value)[0] == 0)) {
		SetEnvironmentVariable (utf16_name, NULL);
		g_free (utf16_name);
		return;
	}

	utf16_value = mono_string_to_utf16 (value);

	SetEnvironmentVariable (utf16_name, utf16_value);

	g_free (utf16_name);
	g_free (utf16_value);
#else
	utf8_name = mono_string_to_utf8 (name);	/* FIXME: this should be ascii */

	if ((value == NULL) || (mono_string_length (value) == 0) || (mono_string_chars (value)[0] == 0)) {
		g_unsetenv (utf8_name);
		g_free (utf8_name);
		return;
	}

	utf8_value = mono_string_to_utf8_checked (value, &error);
	if (!mono_error_ok (&error)) {
		g_free (utf8_name);
		mono_error_raise_exception (&error);
	}
	g_setenv (utf8_name, utf8_value, TRUE);

	g_free (utf8_name);
	g_free (utf8_value);
#endif
}

ICALL_EXPORT void
ves_icall_System_Environment_Exit (int result)
{
	MONO_ARCH_SAVE_REGS;

/* FIXME: There are some cleanup hangs that should be worked out, but
 * if the program is going to exit, everything will be cleaned up when
 * NaCl exits anyway.
 */
#ifndef __native_client__
	if (!mono_runtime_try_shutdown ())
		mono_thread_exit ();

	/* Suspend all managed threads since the runtime is going away */
	mono_thread_suspend_all_other_threads ();

	mono_runtime_quit ();
#endif

	/* we may need to do some cleanup here... */
	exit (result);
}

ICALL_EXPORT MonoString*
ves_icall_System_Environment_GetGacPath (void)
{
	return mono_string_new (mono_domain_get (), mono_assembly_getrootdir ());
}

ICALL_EXPORT MonoString*
ves_icall_System_Environment_GetWindowsFolderPath (int folder)
{
#if defined (HOST_WIN32)
	#ifndef CSIDL_FLAG_CREATE
		#define CSIDL_FLAG_CREATE	0x8000
	#endif

	WCHAR path [MAX_PATH];
	/* Create directory if no existing */
	if (SUCCEEDED (SHGetFolderPathW (NULL, folder | CSIDL_FLAG_CREATE, NULL, 0, path))) {
		int len = 0;
		while (path [len])
			++ len;
		return mono_string_new_utf16 (mono_domain_get (), path, len);
	}
#else
	g_warning ("ves_icall_System_Environment_GetWindowsFolderPath should only be called on Windows!");
#endif
	return mono_string_new (mono_domain_get (), "");
}

ICALL_EXPORT MonoArray *
ves_icall_System_Environment_GetLogicalDrives (void)
{
        gunichar2 buf [256], *ptr, *dname;
	gunichar2 *u16;
	guint initial_size = 127, size = 128;
	gint ndrives;
	MonoArray *result;
	MonoString *drivestr;
	MonoDomain *domain = mono_domain_get ();
	gint len;

	MONO_ARCH_SAVE_REGS;

        buf [0] = '\0';
	ptr = buf;

	while (size > initial_size) {
		size = (guint) GetLogicalDriveStrings (initial_size, ptr);
		if (size > initial_size) {
			if (ptr != buf)
				g_free (ptr);
			ptr = g_malloc0 ((size + 1) * sizeof (gunichar2));
			initial_size = size;
			size++;
		}
	}

	/* Count strings */
	dname = ptr;
	ndrives = 0;
	do {
		while (*dname++);
		ndrives++;
	} while (*dname);

	dname = ptr;
	result = mono_array_new (domain, mono_defaults.string_class, ndrives);
	ndrives = 0;
	do {
		len = 0;
		u16 = dname;
		while (*u16) { u16++; len ++; }
		drivestr = mono_string_new_utf16 (domain, dname, len);
		mono_array_setref (result, ndrives++, drivestr);
		while (*dname++);
	} while (*dname);

	if (ptr != buf)
		g_free (ptr);

	return result;
}

ICALL_EXPORT MonoString *
ves_icall_System_IO_DriveInfo_GetDriveFormat (MonoString *path)
{
	gunichar2 volume_name [MAX_PATH + 1];
	
	if (GetVolumeInformation (mono_string_chars (path), NULL, 0, NULL, NULL, NULL, volume_name, MAX_PATH + 1) == FALSE)
		return NULL;
	return mono_string_from_utf16 (volume_name);
}

ICALL_EXPORT MonoString *
ves_icall_System_Environment_InternalGetHome (void)
{
	MONO_ARCH_SAVE_REGS;

	return mono_string_new (mono_domain_get (), g_get_home_dir ());
}

static const char *encodings [] = {
	(char *) 1,
		"ascii", "us_ascii", "us", "ansi_x3.4_1968",
		"ansi_x3.4_1986", "cp367", "csascii", "ibm367",
		"iso_ir_6", "iso646_us", "iso_646.irv:1991",
	(char *) 2,
		"utf_7", "csunicode11utf7", "unicode_1_1_utf_7",
		"unicode_2_0_utf_7", "x_unicode_1_1_utf_7",
		"x_unicode_2_0_utf_7",
	(char *) 3,
		"utf_8", "unicode_1_1_utf_8", "unicode_2_0_utf_8",
		"x_unicode_1_1_utf_8", "x_unicode_2_0_utf_8",
	(char *) 4,
		"utf_16", "UTF_16LE", "ucs_2", "unicode",
		"iso_10646_ucs2",
	(char *) 5,
		"unicodefffe", "utf_16be",
	(char *) 6,
		"iso_8859_1",
	(char *) 0
};

/*
 * Returns the internal codepage, if the value of "int_code_page" is
 * 1 at entry, and we can not compute a suitable code page number,
 * returns the code page as a string
 */
ICALL_EXPORT MonoString*
ves_icall_System_Text_Encoding_InternalCodePage (gint32 *int_code_page) 
{
	const char *cset;
	const char *p;
	char *c;
	char *codepage = NULL;
	int code;
	int want_name = *int_code_page;
	int i;
	
	*int_code_page = -1;
	MONO_ARCH_SAVE_REGS;

	g_get_charset (&cset);
	c = codepage = strdup (cset);
	for (c = codepage; *c; c++){
		if (isascii (*c) && isalpha (*c))
			*c = tolower (*c);
		if (*c == '-')
			*c = '_';
	}
	/* g_print ("charset: %s\n", cset); */
	
	/* handle some common aliases */
	p = encodings [0];
	code = 0;
	for (i = 0; p != 0; ){
		if ((gssize) p < 7){
			code = (gssize) p;
			p = encodings [++i];
			continue;
		}
		if (strcmp (p, codepage) == 0){
			*int_code_page = code;
			break;
		}
		p = encodings [++i];
	}
	
	if (strstr (codepage, "utf_8") != NULL)
		*int_code_page |= 0x10000000;
	free (codepage);
	
	if (want_name && *int_code_page == -1)
		return mono_string_new (mono_domain_get (), cset);
	else
		return NULL;
}

ICALL_EXPORT MonoBoolean
ves_icall_System_Environment_get_HasShutdownStarted (void)
{
	if (mono_runtime_is_shutting_down ())
		return TRUE;

	if (mono_domain_is_unloading (mono_domain_get ()))
		return TRUE;

	return FALSE;
}

ICALL_EXPORT void
ves_icall_System_Environment_BroadcastSettingChange (void)
{
#ifdef HOST_WIN32
	SendMessageTimeout (HWND_BROADCAST, WM_SETTINGCHANGE, NULL, L"Environment", SMTO_ABORTIFHUNG, 2000, 0);
#endif
}

ICALL_EXPORT void
ves_icall_MonoMethodMessage_InitMessage (MonoMethodMessage *this, 
					 MonoReflectionMethod *method,
					 MonoArray *out_args)
{
	MONO_ARCH_SAVE_REGS;

	mono_message_init (mono_object_domain (this), this, method, out_args);
}

#ifndef DISABLE_REMOTING
ICALL_EXPORT MonoBoolean
ves_icall_IsTransparentProxy (MonoObject *proxy)
{
	MONO_ARCH_SAVE_REGS;

	if (!proxy)
		return 0;

	if (proxy->vtable->klass == mono_defaults.transparent_proxy_class)
		return 1;

	return 0;
}

ICALL_EXPORT MonoReflectionMethod *
ves_icall_Remoting_RemotingServices_GetVirtualMethod (
	MonoReflectionType *rtype, MonoReflectionMethod *rmethod)
{
	MonoClass *klass;
	MonoMethod *method;
	MonoMethod **vtable;
	MonoMethod *res = NULL;

	MONO_CHECK_ARG_NULL (rtype);
	MONO_CHECK_ARG_NULL (rmethod);

	method = rmethod->method;
	klass = mono_class_from_mono_type (rtype->type);
	mono_class_init_or_throw (klass);

	if (MONO_CLASS_IS_INTERFACE (klass))
		return NULL;

	if (method->flags & METHOD_ATTRIBUTE_STATIC)
		return NULL;

	if ((method->flags & METHOD_ATTRIBUTE_FINAL) || !(method->flags & METHOD_ATTRIBUTE_VIRTUAL)) {
		if (klass == method->klass || mono_class_is_subclass_of (klass, method->klass, FALSE))
			return rmethod;
		else
			return NULL;
	}

	mono_class_setup_vtable (klass);
	vtable = klass->vtable;

	if (method->klass->flags & TYPE_ATTRIBUTE_INTERFACE) {
		gboolean variance_used = FALSE;
		/*MS fails with variant interfaces but it's the right thing to do anyway.*/
		int offs = mono_class_interface_offset_with_variance (klass, method->klass, &variance_used);
		if (offs >= 0)
			res = vtable [offs + method->slot];
	} else {
		if (!(klass == method->klass || mono_class_is_subclass_of (klass, method->klass, FALSE)))
			return NULL;

		if (method->slot != -1)
			res = vtable [method->slot];
	}

	if (!res)
		return NULL;

	return mono_method_get_object (mono_domain_get (), res, NULL);
}

ICALL_EXPORT void
ves_icall_System_Runtime_Activation_ActivationServices_EnableProxyActivation (MonoReflectionType *type, MonoBoolean enable)
{
	MonoClass *klass;
	MonoVTable* vtable;

	MONO_ARCH_SAVE_REGS;

	klass = mono_class_from_mono_type (type->type);
	vtable = mono_class_vtable_full (mono_domain_get (), klass, TRUE);

	mono_vtable_set_is_remote (vtable, enable);
}

#else /* DISABLE_REMOTING */

ICALL_EXPORT void
ves_icall_System_Runtime_Activation_ActivationServices_EnableProxyActivation (MonoReflectionType *type, MonoBoolean enable)
{
	g_assert_not_reached ();
}

#endif

ICALL_EXPORT MonoObject *
ves_icall_System_Runtime_Activation_ActivationServices_AllocateUninitializedClassInstance (MonoReflectionType *type)
{
	MonoClass *klass;
	MonoDomain *domain;
	
	MONO_ARCH_SAVE_REGS;

	domain = mono_object_domain (type);
	klass = mono_class_from_mono_type (type->type);
	mono_class_init_or_throw (klass);

	if (MONO_CLASS_IS_INTERFACE (klass) || (klass->flags & TYPE_ATTRIBUTE_ABSTRACT))
		mono_raise_exception (mono_get_exception_argument ("type", "Type cannot be instantiated"));

	if (klass->rank >= 1) {
		g_assert (klass->rank == 1);
		return (MonoObject *) mono_array_new (domain, klass->element_class, 0);
	} else {
		/* Bypass remoting object creation check */
		return mono_object_new_alloc_specific (mono_class_vtable_full (domain, klass, TRUE));
	}
}

ICALL_EXPORT MonoString *
ves_icall_System_IO_get_temp_path (void)
{
	MONO_ARCH_SAVE_REGS;

	return mono_string_new (mono_domain_get (), g_get_tmp_dir ());
}

#ifndef PLATFORM_NO_DRIVEINFO
ICALL_EXPORT MonoBoolean
ves_icall_System_IO_DriveInfo_GetDiskFreeSpace (MonoString *path_name, guint64 *free_bytes_avail,
						guint64 *total_number_of_bytes, guint64 *total_number_of_free_bytes,
						gint32 *error)
{
	gboolean result;
	ULARGE_INTEGER wapi_free_bytes_avail;
	ULARGE_INTEGER wapi_total_number_of_bytes;
	ULARGE_INTEGER wapi_total_number_of_free_bytes;

	MONO_ARCH_SAVE_REGS;

	*error = ERROR_SUCCESS;
	result = GetDiskFreeSpaceEx (mono_string_chars (path_name), &wapi_free_bytes_avail, &wapi_total_number_of_bytes,
				     &wapi_total_number_of_free_bytes);

	if (result) {
		*free_bytes_avail = wapi_free_bytes_avail.QuadPart;
		*total_number_of_bytes = wapi_total_number_of_bytes.QuadPart;
		*total_number_of_free_bytes = wapi_total_number_of_free_bytes.QuadPart;
	} else {
		*free_bytes_avail = 0;
		*total_number_of_bytes = 0;
		*total_number_of_free_bytes = 0;
		*error = GetLastError ();
	}

	return result;
}

ICALL_EXPORT guint32
ves_icall_System_IO_DriveInfo_GetDriveType (MonoString *root_path_name)
{
	MONO_ARCH_SAVE_REGS;

	return GetDriveType (mono_string_chars (root_path_name));
}
#endif

ICALL_EXPORT gpointer
ves_icall_RuntimeMethod_GetFunctionPointer (MonoMethod *method)
{
	MONO_ARCH_SAVE_REGS;

	return mono_compile_method (method);
}

ICALL_EXPORT MonoString *
ves_icall_System_Configuration_DefaultConfig_get_machine_config_path (void)
{
	MonoString *mcpath;
	gchar *path;

	MONO_ARCH_SAVE_REGS;

	path = g_build_path (G_DIR_SEPARATOR_S, mono_get_config_dir (), "mono", mono_get_runtime_info ()->framework_version, "machine.config", NULL);

#if defined (HOST_WIN32)
	/* Avoid mixing '/' and '\\' */
	{
		gint i;
		for (i = strlen (path) - 1; i >= 0; i--)
			if (path [i] == '/')
				path [i] = '\\';
	}
#endif
	mcpath = mono_string_new (mono_domain_get (), path);
	g_free (path);

	return mcpath;
}

static MonoString *
get_bundled_app_config (void)
{
	const gchar *app_config;
	MonoDomain *domain;
	MonoString *file;
	gchar *config_file_name, *config_file_path;
	gsize len;
	gchar *module;

	MONO_ARCH_SAVE_REGS;

	domain = mono_domain_get ();
	file = domain->setup->configuration_file;
	if (!file)
		return NULL;

	// Retrieve config file and remove the extension
	config_file_name = mono_string_to_utf8 (file);
	config_file_path = mono_portability_find_file (config_file_name, TRUE);
	if (!config_file_path)
		config_file_path = config_file_name;
	len = strlen (config_file_path) - strlen (".config");
	module = g_malloc0 (len + 1);
	memcpy (module, config_file_path, len);
	// Get the config file from the module name
	app_config = mono_config_string_for_assembly_file (module);
	// Clean-up
	g_free (module);
	if (config_file_name != config_file_path)
		g_free (config_file_name);
	g_free (config_file_path);

	if (!app_config)
		return NULL;

	return mono_string_new (mono_domain_get (), app_config);
}

static MonoString *
get_bundled_machine_config (void)
{
	const gchar *machine_config;

	MONO_ARCH_SAVE_REGS;

	machine_config = mono_get_machine_config ();

	if (!machine_config)
		return NULL;

	return mono_string_new (mono_domain_get (), machine_config);
}

ICALL_EXPORT MonoString *
ves_icall_System_Web_Util_ICalls_get_machine_install_dir (void)
{
	MonoString *ipath;
	gchar *path;

	MONO_ARCH_SAVE_REGS;

	path = g_path_get_dirname (mono_get_config_dir ());

#if defined (HOST_WIN32)
	/* Avoid mixing '/' and '\\' */
	{
		gint i;
		for (i = strlen (path) - 1; i >= 0; i--)
			if (path [i] == '/')
				path [i] = '\\';
	}
#endif
	ipath = mono_string_new (mono_domain_get (), path);
	g_free (path);

	return ipath;
}

ICALL_EXPORT gboolean
ves_icall_get_resources_ptr (MonoReflectionAssembly *assembly, gpointer *result, gint32 *size)
{
	MonoPEResourceDataEntry *entry;
	MonoImage *image;

	MONO_ARCH_SAVE_REGS;

	if (!assembly || !result || !size)
		return FALSE;

	*result = NULL;
	*size = 0;
	image = assembly->assembly->image;
	entry = mono_image_lookup_resource (image, MONO_PE_RESOURCE_ID_ASPNET_STRING, 0, NULL);
	if (!entry)
		return FALSE;

	*result = mono_image_rva_map (image, entry->rde_data_offset);
	if (!(*result)) {
		g_free (entry);
		return FALSE;
	}
	*size = entry->rde_size;
	g_free (entry);
	return TRUE;
}

ICALL_EXPORT MonoBoolean
ves_icall_System_Diagnostics_Debugger_IsAttached_internal (void)
{
	return mono_debug_using_mono_debugger () || mono_is_debugger_attached ();
}

ICALL_EXPORT MonoBoolean
ves_icall_System_Diagnostics_Debugger_IsLogging (void)
{
	if (mono_get_runtime_callbacks ()->debug_log_is_enabled)
		return mono_get_runtime_callbacks ()->debug_log_is_enabled ();
	else
		return FALSE;
}

ICALL_EXPORT void
ves_icall_System_Diagnostics_Debugger_Log (int level, MonoString *category, MonoString *message)
{
	if (mono_get_runtime_callbacks ()->debug_log)
		mono_get_runtime_callbacks ()->debug_log (level, category, message);
}

ICALL_EXPORT void
ves_icall_System_Diagnostics_DefaultTraceListener_WriteWindowsDebugString (MonoString *message)
{
#if defined (HOST_WIN32)
	OutputDebugString (mono_string_chars (message));
#else
	g_warning ("WriteWindowsDebugString called and HOST_WIN32 not defined!\n");
#endif
}

/* Only used for value types */
ICALL_EXPORT MonoObject *
ves_icall_System_Activator_CreateInstanceInternal (MonoReflectionType *type)
{
	MonoClass *klass;
	MonoDomain *domain;
	
	MONO_ARCH_SAVE_REGS;

	domain = mono_object_domain (type);
	klass = mono_class_from_mono_type (type->type);
	mono_class_init_or_throw (klass);

	if (mono_class_is_nullable (klass))
		/* No arguments -> null */
		return NULL;

	return mono_object_new (domain, klass);
}

ICALL_EXPORT MonoReflectionMethod *
ves_icall_MonoMethod_get_base_method (MonoReflectionMethod *m, gboolean definition)
{
	MonoClass *klass, *parent;
	MonoMethod *method = m->method;
	MonoMethod *result = NULL;
	int slot;

	MONO_ARCH_SAVE_REGS;

	if (method->klass == NULL)
		return m;

	if (!(method->flags & METHOD_ATTRIBUTE_VIRTUAL) ||
	    MONO_CLASS_IS_INTERFACE (method->klass) ||
	    method->flags & METHOD_ATTRIBUTE_NEW_SLOT)
		return m;

	slot = mono_method_get_vtable_slot (method);
	if (slot == -1)
		return m;

	klass = method->klass;
	if (klass->generic_class)
		klass = klass->generic_class->container_class;

	if (definition) {
		/* At the end of the loop, klass points to the eldest class that has this virtual function slot. */
		for (parent = klass->parent; parent != NULL; parent = parent->parent) {
			mono_class_setup_vtable (parent);
			if (parent->vtable_size <= slot)
				break;
			klass = parent;
		}
	} else {
		klass = klass->parent;
		if (!klass)
			return m;
	}

	if (klass == method->klass)
		return m;

	/*This is possible if definition == FALSE.
	 * Do it here to be really sure we don't read invalid memory.
	 */
	if (slot >= klass->vtable_size)
		return m;

	mono_class_setup_vtable (klass);

	result = klass->vtable [slot];
	if (result == NULL) {
		/* It is an abstract method */
		gpointer iter = NULL;
		while ((result = mono_class_get_methods (klass, &iter)))
			if (result->slot == slot)
				break;
	}

	if (result == NULL)
		return m;

	return mono_method_get_object (mono_domain_get (), result, NULL);
}

ICALL_EXPORT MonoString*
ves_icall_MonoMethod_get_name (MonoReflectionMethod *m)
{
	MonoMethod *method = m->method;

	MONO_OBJECT_SETREF (m, name, mono_string_new (mono_object_domain (m), method->name));
	return m->name;
}

ICALL_EXPORT void
mono_ArgIterator_Setup (MonoArgIterator *iter, char* argsp, char* start)
{
	MONO_ARCH_SAVE_REGS;

	iter->sig = *(MonoMethodSignature**)argsp;
	
	g_assert (iter->sig->sentinelpos <= iter->sig->param_count);
	g_assert (iter->sig->call_convention == MONO_CALL_VARARG);

	iter->next_arg = 0;
	/* FIXME: it's not documented what start is exactly... */
	if (start) {
		iter->args = start;
	} else {
		iter->args = argsp + sizeof (gpointer);
	}
	iter->num_args = iter->sig->param_count - iter->sig->sentinelpos;

	/* g_print ("sig %p, param_count: %d, sent: %d\n", iter->sig, iter->sig->param_count, iter->sig->sentinelpos); */
}

ICALL_EXPORT MonoTypedRef
mono_ArgIterator_IntGetNextArg (MonoArgIterator *iter)
{
	guint32 i, arg_size;
	gint32 align;
	MonoTypedRef res;
	MONO_ARCH_SAVE_REGS;

	i = iter->sig->sentinelpos + iter->next_arg;

	g_assert (i < iter->sig->param_count);

	res.type = iter->sig->params [i];
	res.klass = mono_class_from_mono_type (res.type);
	arg_size = mono_type_stack_size (res.type, &align);
#if defined(__arm__) || defined(__mips__)
	iter->args = (guint8*)(((gsize)iter->args + (align) - 1) & ~(align - 1));
#endif
	res.value = iter->args;
#if defined(__native_client__) && SIZEOF_REGISTER == 8
	/* Values are stored as 8 byte register sized objects, but 'value'
	 * is dereferenced as a pointer in other routines.
	 */
	res.value = (char*)res.value + 4;
#endif
#if G_BYTE_ORDER != G_LITTLE_ENDIAN
	if (arg_size <= sizeof (gpointer)) {
		int dummy;
		int padding = arg_size - mono_type_size (res.type, &dummy);
		res.value = (guint8*)res.value + padding;
	}
#endif
	iter->args = (char*)iter->args + arg_size;
	iter->next_arg++;

	/* g_print ("returning arg %d, type 0x%02x of size %d at %p\n", i, res.type->type, arg_size, res.value); */

	return res;
}

ICALL_EXPORT MonoTypedRef
mono_ArgIterator_IntGetNextArgT (MonoArgIterator *iter, MonoType *type)
{
	guint32 i, arg_size;
	gint32 align;
	MonoTypedRef res;
	MONO_ARCH_SAVE_REGS;

	i = iter->sig->sentinelpos + iter->next_arg;

	g_assert (i < iter->sig->param_count);

	while (i < iter->sig->param_count) {
		if (!mono_metadata_type_equal (type, iter->sig->params [i]))
			continue;
		res.type = iter->sig->params [i];
		res.klass = mono_class_from_mono_type (res.type);
		/* FIXME: endianess issue... */
		arg_size = mono_type_stack_size (res.type, &align);
#if defined(__arm__) || defined(__mips__)
		iter->args = (guint8*)(((gsize)iter->args + (align) - 1) & ~(align - 1));
#endif
		res.value = iter->args;
		iter->args = (char*)iter->args + arg_size;
		iter->next_arg++;
		/* g_print ("returning arg %d, type 0x%02x of size %d at %p\n", i, res.type->type, arg_size, res.value); */
		return res;
	}
	/* g_print ("arg type 0x%02x not found\n", res.type->type); */

	res.type = NULL;
	res.value = NULL;
	res.klass = NULL;
	return res;
}

ICALL_EXPORT MonoType*
mono_ArgIterator_IntGetNextArgType (MonoArgIterator *iter)
{
	gint i;
	MONO_ARCH_SAVE_REGS;
	
	i = iter->sig->sentinelpos + iter->next_arg;

	g_assert (i < iter->sig->param_count);

	return iter->sig->params [i];
}

ICALL_EXPORT MonoObject*
mono_TypedReference_ToObject (MonoTypedRef tref)
{
	MONO_ARCH_SAVE_REGS;

	if (MONO_TYPE_IS_REFERENCE (tref.type)) {
		MonoObject** objp = tref.value;
		return *objp;
	}

	return mono_value_box (mono_domain_get (), tref.klass, tref.value);
}

ICALL_EXPORT MonoObject*
mono_TypedReference_ToObjectInternal (MonoType *type, gpointer value, MonoClass *klass)
{
	MONO_ARCH_SAVE_REGS;

	if (MONO_TYPE_IS_REFERENCE (type)) {
		MonoObject** objp = value;
		return *objp;
	}

	return mono_value_box (mono_domain_get (), klass, value);
}

static void
prelink_method (MonoMethod *method)
{
	const char *exc_class, *exc_arg;
	if (!(method->flags & METHOD_ATTRIBUTE_PINVOKE_IMPL))
		return;
	mono_lookup_pinvoke_call (method, &exc_class, &exc_arg);
	if (exc_class) {
		mono_raise_exception( 
			mono_exception_from_name_msg (mono_defaults.corlib, "System", exc_class, exc_arg ) );
	}
	/* create the wrapper, too? */
}

ICALL_EXPORT void
ves_icall_System_Runtime_InteropServices_Marshal_Prelink (MonoReflectionMethod *method)
{
	MONO_ARCH_SAVE_REGS;
	prelink_method (method->method);
}

ICALL_EXPORT void
ves_icall_System_Runtime_InteropServices_Marshal_PrelinkAll (MonoReflectionType *type)
{
	MonoClass *klass = mono_class_from_mono_type (type->type);
	MonoMethod* m;
	gpointer iter = NULL;
	MONO_ARCH_SAVE_REGS;

	mono_class_init_or_throw (klass);

	while ((m = mono_class_get_methods (klass, &iter)))
		prelink_method (m);
}

/* These parameters are "readonly" in corlib/System/NumberFormatter.cs */
ICALL_EXPORT void
ves_icall_System_NumberFormatter_GetFormatterTables (guint64 const **mantissas,
					    gint32 const **exponents,
					    gunichar2 const **digitLowerTable,
					    gunichar2 const **digitUpperTable,
					    gint64 const **tenPowersList,
					    gint32 const **decHexDigits)
{
	*mantissas = Formatter_MantissaBitsTable;
	*exponents = Formatter_TensExponentTable;
	*digitLowerTable = Formatter_DigitLowerTable;
	*digitUpperTable = Formatter_DigitUpperTable;
	*tenPowersList = Formatter_TenPowersList;
	*decHexDigits = Formatter_DecHexDigits;
}

ICALL_EXPORT void
get_category_data (int version,
		   guint8 const **category_data,
		   guint16 const **category_astral_index)
{
	*category_astral_index = NULL;

#ifndef DISABLE_NET_4_0
	if (version == 4) {
		*category_data = CategoryData_v4;
#ifndef DISABLE_ASTRAL
		*category_astral_index = CategoryData_v4_astral_index;
#endif
		return;
	}
#endif

	*category_data = CategoryData_v2;
#ifndef DISABLE_ASTRAL
	*category_astral_index = CategoryData_v2_astral_index;
#endif
}

/* These parameters are "readonly" in corlib/System/Char.cs */
ICALL_EXPORT void
ves_icall_System_Char_GetDataTablePointers (int category_data_version,
					    guint8 const **category_data,
					    guint16 const **category_astral_index,
					    guint8 const **numeric_data,
					    gdouble const **numeric_data_values,
					    guint16 const **to_lower_data_low,
					    guint16 const **to_lower_data_high,
					    guint16 const **to_upper_data_low,
					    guint16 const **to_upper_data_high)
{
	get_category_data (category_data_version, category_data, category_astral_index);
	*numeric_data = NumericData;
	*numeric_data_values = NumericDataValues;
	*to_lower_data_low = ToLowerDataLow;
	*to_lower_data_high = ToLowerDataHigh;
	*to_upper_data_low = ToUpperDataLow;
	*to_upper_data_high = ToUpperDataHigh;
}

ICALL_EXPORT gint32
ves_icall_MonoDebugger_GetMethodToken (MonoReflectionMethod *method)
{
	return method->method->token;
}

/*
 * We return NULL for no modifiers so the corlib code can return Type.EmptyTypes
 * and avoid useless allocations.
 */
static MonoArray*
type_array_from_modifiers (MonoImage *image, MonoType *type, int optional)
{
	MonoArray *res;
	int i, count = 0;
	for (i = 0; i < type->num_mods; ++i) {
		if ((optional && !type->modifiers [i].required) || (!optional && type->modifiers [i].required))
			count++;
	}
	if (!count)
		return NULL;
	res = mono_array_new (mono_domain_get (), mono_defaults.systemtype_class, count);
	count = 0;
	for (i = 0; i < type->num_mods; ++i) {
		if ((optional && !type->modifiers [i].required) || (!optional && type->modifiers [i].required)) {
			MonoClass *klass = mono_class_get (image, type->modifiers [i].token);
			mono_array_setref (res, count, mono_type_get_object (mono_domain_get (), &klass->byval_arg));
			count++;
		}
	}
	return res;
}

ICALL_EXPORT MonoArray*
param_info_get_type_modifiers (MonoReflectionParameter *param, MonoBoolean optional)
{
	MonoType *type = param->ClassImpl->type;
	MonoClass *member_class = mono_object_class (param->MemberImpl);
	MonoMethod *method = NULL;
	MonoImage *image;
	int pos;
	MonoMethodSignature *sig;

	if (mono_class_is_reflection_method_or_constructor (member_class)) {
		MonoReflectionMethod *rmethod = (MonoReflectionMethod*)param->MemberImpl;
		method = rmethod->method;
	} else if (member_class->image == mono_defaults.corlib && !strcmp ("MonoProperty", member_class->name)) {
		MonoReflectionProperty *prop = (MonoReflectionProperty *)param->MemberImpl;
		if (!(method = prop->property->get))
			method = prop->property->set;
		g_assert (method);	
	} else {
		char *type_name = mono_type_get_full_name (member_class);
		char *msg = g_strdup_printf ("Custom modifiers on a ParamInfo with member %s are not supported", type_name);
		MonoException *ex = mono_get_exception_not_supported  (msg);
		g_free (type_name);
		g_free (msg);
		mono_raise_exception (ex);
	}

	image = method->klass->image;
	pos = param->PositionImpl;
	sig = mono_method_signature (method);
	if (pos == -1)
		type = sig->ret;
	else
		type = sig->params [pos];

	return type_array_from_modifiers (image, type, optional);
}

static MonoType*
get_property_type (MonoProperty *prop)
{
	MonoMethodSignature *sig;
	if (prop->get) {
		sig = mono_method_signature (prop->get);
		return sig->ret;
	} else if (prop->set) {
		sig = mono_method_signature (prop->set);
		return sig->params [sig->param_count - 1];
	}
	return NULL;
}

ICALL_EXPORT MonoArray*
property_info_get_type_modifiers (MonoReflectionProperty *property, MonoBoolean optional)
{
	MonoType *type = get_property_type (property->property);
	MonoImage *image = property->klass->image;

	if (!type)
		return NULL;
	return type_array_from_modifiers (image, type, optional);
}

/*
 *Construct a MonoType suited to be used to decode a constant blob object.
 *
 * @type is the target type which will be constructed
 * @blob_type is the blob type, for example, that comes from the constant table
 * @real_type is the expected constructed type.
 */
static void
mono_type_from_blob_type (MonoType *type, MonoTypeEnum blob_type, MonoType *real_type)
{
	type->type = blob_type;
	type->data.klass = NULL;
	if (blob_type == MONO_TYPE_CLASS)
		type->data.klass = mono_defaults.object_class;
	else if (real_type->type == MONO_TYPE_VALUETYPE && real_type->data.klass->enumtype) {
		/* For enums, we need to use the base type */
		type->type = MONO_TYPE_VALUETYPE;
		type->data.klass = mono_class_from_mono_type (real_type);
	} else
		type->data.klass = mono_class_from_mono_type (real_type);
}

ICALL_EXPORT MonoObject*
property_info_get_default_value (MonoReflectionProperty *property)
{
	MonoType blob_type;
	MonoProperty *prop = property->property;
	MonoType *type = get_property_type (prop);
	MonoDomain *domain = mono_object_domain (property); 
	MonoTypeEnum def_type;
	const char *def_value;
	MonoObject *o;

	mono_class_init (prop->parent);

	if (!(prop->attrs & PROPERTY_ATTRIBUTE_HAS_DEFAULT))
		mono_raise_exception (mono_get_exception_invalid_operation (NULL));

	def_value = mono_class_get_property_default_value (prop, &def_type);

	mono_type_from_blob_type (&blob_type, def_type, type);
	o = mono_get_object_from_blob (domain, &blob_type, def_value);

	return o;
}

ICALL_EXPORT MonoBoolean
custom_attrs_defined_internal (MonoObject *obj, MonoReflectionType *attr_type)
{
	MonoClass *attr_class = mono_class_from_mono_type (attr_type->type);
	MonoCustomAttrInfo *cinfo;
	gboolean found;

	mono_class_init_or_throw (attr_class);

	cinfo = mono_reflection_get_custom_attrs_info (obj);
	if (!cinfo)
		return FALSE;
	found = mono_custom_attrs_has_attr (cinfo, attr_class);
	if (!cinfo->cached)
		mono_custom_attrs_free (cinfo);
	return found;
}

ICALL_EXPORT MonoArray*
custom_attrs_get_by_type (MonoObject *obj, MonoReflectionType *attr_type)
{
	MonoClass *attr_class = attr_type ? mono_class_from_mono_type (attr_type->type) : NULL;
	MonoArray *res;
	MonoError error;

	if (attr_class)
		mono_class_init_or_throw (attr_class);

	res = mono_reflection_get_custom_attrs_by_type (obj, attr_class, &error);
	if (!mono_error_ok (&error))
		mono_error_raise_exception (&error);
	if (mono_loader_get_last_error ()) {
		mono_raise_exception (mono_loader_error_prepare_exception (mono_loader_get_last_error ()));
		g_assert_not_reached ();
		/* Not reached */
		return NULL;
	} else {
		return res;
	}
}

ICALL_EXPORT MonoString*
ves_icall_Mono_Runtime_GetDisplayName (void)
{
	char *info;
	MonoString *display_name;

	info = mono_get_runtime_callbacks ()->get_runtime_build_info ();
	display_name = mono_string_new (mono_domain_get (), info);
	g_free (info);
	return display_name;
}

ICALL_EXPORT MonoString*
ves_icall_System_ComponentModel_Win32Exception_W32ErrorMessage (guint32 code)
{
	MonoString *message;
	guint32 ret;
	gunichar2 buf[256];
	
	ret = FormatMessage (FORMAT_MESSAGE_FROM_SYSTEM |
			     FORMAT_MESSAGE_IGNORE_INSERTS, NULL, code, 0,
			     buf, 255, NULL);
	if (ret == 0) {
		message = mono_string_new (mono_domain_get (), "Error looking up error string");
	} else {
		message = mono_string_new_utf16 (mono_domain_get (), buf, ret);
	}
	
	return message;
}

const static guchar
dbase64 [] = {
	128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
	128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
	128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 62, 128, 128, 128, 63,
	52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 128, 128, 128, 0, 128, 128,
	128, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
	15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 128, 128, 128, 128, 128,
	128, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51
};

static MonoArray *
base64_to_byte_array (gunichar2 *start, gint ilength, MonoBoolean allowWhitespaceOnly)
{
	gint ignored;
	gint i;
	gunichar2 c;
	gunichar2 last, prev_last, prev2_last;
	gint olength;
	MonoArray *result;
	guchar *res_ptr;
	gint a [4], b [4];
	MonoException *exc;

	int havePadding = 0;
	ignored = 0;
	last = prev_last = 0, prev2_last = 0;
	for (i = 0; i < ilength; i++) {
		c = start [i];
		if (c >= sizeof (dbase64)) {
			exc = mono_exception_from_name_msg (mono_get_corlib (),
				"System", "FormatException",
				"Invalid character found.");
			mono_raise_exception (exc);
		} else if (isspace (c)) {
			ignored++;
		} else if (havePadding && c != '=') {
			exc = mono_exception_from_name_msg (mono_get_corlib (),
				"System", "FormatException",
				"Invalid character found.");
			mono_raise_exception (exc);
		} else {
			if (c == '=') havePadding = 1;
			prev2_last = prev_last;
			prev_last = last;
			last = c;
		}
	}

	olength = ilength - ignored;

	if (allowWhitespaceOnly && olength == 0) {
		return mono_array_new (mono_domain_get (), mono_defaults.byte_class, 0);
	}

	if ((olength & 3) != 0 || olength <= 0) {
		exc = mono_exception_from_name_msg (mono_get_corlib (), "System",
					"FormatException", "Invalid length.");
		mono_raise_exception (exc);
	}

	if (prev2_last == '=') {
		exc = mono_exception_from_name_msg (mono_get_corlib (), "System", "FormatException", "Invalid format.");
		mono_raise_exception (exc);
	}

	olength = (olength * 3) / 4;
	if (last == '=')
		olength--;

	if (prev_last == '=')
		olength--;

	result = mono_array_new (mono_domain_get (), mono_defaults.byte_class, olength);
	res_ptr = mono_array_addr (result, guchar, 0);
	for (i = 0; i < ilength; ) {
		int k;

		for (k = 0; k < 4 && i < ilength;) {
			c = start [i++];
			if (isspace (c))
				continue;

			a [k] = (guchar) c;
			if (((b [k] = dbase64 [c]) & 0x80) != 0) {
				exc = mono_exception_from_name_msg (mono_get_corlib (),
					"System", "FormatException",
					"Invalid character found.");
				mono_raise_exception (exc);
			}
			k++;
		}

		*res_ptr++ = (b [0] << 2) | (b [1] >> 4);
		if (a [2] != '=')
			*res_ptr++ = (b [1] << 4) | (b [2] >> 2);
		if (a [3] != '=')
			*res_ptr++ = (b [2] << 6) | b [3];

		while (i < ilength && isspace (start [i]))
			i++;
	}

	return result;
}

ICALL_EXPORT MonoArray *
InternalFromBase64String (MonoString *str, MonoBoolean allowWhitespaceOnly)
{
	MONO_ARCH_SAVE_REGS;

	return base64_to_byte_array (mono_string_chars (str), 
		mono_string_length (str), allowWhitespaceOnly);
}

ICALL_EXPORT MonoArray *
InternalFromBase64CharArray (MonoArray *input, gint offset, gint length)
{
	MONO_ARCH_SAVE_REGS;

	return base64_to_byte_array (mono_array_addr (input, gunichar2, offset),
		length, FALSE);
}

#ifndef DISABLE_ICALL_TABLES

#define ICALL_TYPE(id,name,first)
#define ICALL(id,name,func) Icall_ ## id,

enum {
// #include "metadata/icall-def.h"
ICALL_TYPE(UNORM, "Mono.Globalization.Unicode.Normalization", UNORM_1)
ICALL(UNORM_1, "load_normalization_resource", load_normalization_resource)

#ifndef DISABLE_COM
ICALL_TYPE(COMPROX, "Mono.Interop.ComInteropProxy", COMPROX_1)
ICALL(COMPROX_1, "AddProxy", ves_icall_Mono_Interop_ComInteropProxy_AddProxy)
ICALL(COMPROX_2, "FindProxy", ves_icall_Mono_Interop_ComInteropProxy_FindProxy)
#endif

ICALL_TYPE(RUNTIME, "Mono.Runtime", RUNTIME_1)
ICALL(RUNTIME_1, "GetDisplayName", ves_icall_Mono_Runtime_GetDisplayName)
ICALL(RUNTIME_12, "GetNativeStackTrace", ves_icall_Mono_Runtime_GetNativeStackTrace)
ICALL(RUNTIME_13, "SetGCAllowSynchronousMajor", ves_icall_Mono_Runtime_SetGCAllowSynchronousMajor)

#ifndef PLATFORM_RO_FS
ICALL_TYPE(KPAIR, "Mono.Security.Cryptography.KeyPairPersistence", KPAIR_1)
ICALL(KPAIR_1, "_CanSecure", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_CanSecure)
ICALL(KPAIR_2, "_IsMachineProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsMachineProtected)
ICALL(KPAIR_3, "_IsUserProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsUserProtected)
ICALL(KPAIR_4, "_ProtectMachine", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectMachine)
ICALL(KPAIR_5, "_ProtectUser", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectUser)
#endif /* !PLATFORM_RO_FS */

ICALL_TYPE(ACTIV, "System.Activator", ACTIV_1)
ICALL(ACTIV_1, "CreateInstanceInternal", ves_icall_System_Activator_CreateInstanceInternal)

ICALL_TYPE(APPDOM, "System.AppDomain", APPDOM_1)
ICALL(APPDOM_1, "ExecuteAssembly", ves_icall_System_AppDomain_ExecuteAssembly)
ICALL(APPDOM_2, "GetAssemblies", ves_icall_System_AppDomain_GetAssemblies)
ICALL(APPDOM_3, "GetData", ves_icall_System_AppDomain_GetData)
ICALL(APPDOM_4, "InternalGetContext", ves_icall_System_AppDomain_InternalGetContext)
ICALL(APPDOM_5, "InternalGetDefaultContext", ves_icall_System_AppDomain_InternalGetDefaultContext)
ICALL(APPDOM_6, "InternalGetProcessGuid", ves_icall_System_AppDomain_InternalGetProcessGuid)
ICALL(APPDOM_7, "InternalIsFinalizingForUnload", ves_icall_System_AppDomain_InternalIsFinalizingForUnload)
ICALL(APPDOM_8, "InternalPopDomainRef", ves_icall_System_AppDomain_InternalPopDomainRef)
ICALL(APPDOM_9, "InternalPushDomainRef", ves_icall_System_AppDomain_InternalPushDomainRef)
ICALL(APPDOM_10, "InternalPushDomainRefByID", ves_icall_System_AppDomain_InternalPushDomainRefByID)
ICALL(APPDOM_11, "InternalSetContext", ves_icall_System_AppDomain_InternalSetContext)
ICALL(APPDOM_12, "InternalSetDomain", ves_icall_System_AppDomain_InternalSetDomain)
ICALL(APPDOM_13, "InternalSetDomainByID", ves_icall_System_AppDomain_InternalSetDomainByID)
ICALL(APPDOM_14, "InternalUnload", ves_icall_System_AppDomain_InternalUnload)
ICALL(APPDOM_15, "LoadAssembly", ves_icall_System_AppDomain_LoadAssembly)
ICALL(APPDOM_16, "LoadAssemblyRaw", ves_icall_System_AppDomain_LoadAssemblyRaw)
ICALL(APPDOM_17, "SetData", ves_icall_System_AppDomain_SetData)
ICALL(APPDOM_18, "createDomain", ves_icall_System_AppDomain_createDomain)
ICALL(APPDOM_19, "getCurDomain", ves_icall_System_AppDomain_getCurDomain)
ICALL(APPDOM_20, "getFriendlyName", ves_icall_System_AppDomain_getFriendlyName)
ICALL(APPDOM_21, "getRootDomain", ves_icall_System_AppDomain_getRootDomain)
ICALL(APPDOM_22, "getSetup", ves_icall_System_AppDomain_getSetup)

ICALL_TYPE(ARGI, "System.ArgIterator", ARGI_1)
ICALL(ARGI_1, "IntGetNextArg()",                  mono_ArgIterator_IntGetNextArg)
ICALL(ARGI_2, "IntGetNextArg(intptr)", mono_ArgIterator_IntGetNextArgT)
ICALL(ARGI_3, "IntGetNextArgType",                mono_ArgIterator_IntGetNextArgType)
ICALL(ARGI_4, "Setup",                            mono_ArgIterator_Setup)

ICALL_TYPE(ARRAY, "System.Array", ARRAY_1)
ICALL(ARRAY_1, "ClearInternal",    ves_icall_System_Array_ClearInternal)
ICALL(ARRAY_2, "Clone",            mono_array_clone)
ICALL(ARRAY_3, "CreateInstanceImpl",   ves_icall_System_Array_CreateInstanceImpl)
ICALL(ARRAY_14, "CreateInstanceImpl64",   ves_icall_System_Array_CreateInstanceImpl64)
ICALL(ARRAY_4, "FastCopy",         ves_icall_System_Array_FastCopy)
ICALL(ARRAY_5, "GetGenericValueImpl", ves_icall_System_Array_GetGenericValueImpl)
ICALL(ARRAY_6, "GetLength",        ves_icall_System_Array_GetLength)
ICALL(ARRAY_15, "GetLongLength",        ves_icall_System_Array_GetLongLength)
ICALL(ARRAY_7, "GetLowerBound",    ves_icall_System_Array_GetLowerBound)
ICALL(ARRAY_8, "GetRank",          ves_icall_System_Array_GetRank)
ICALL(ARRAY_9, "GetValue",         ves_icall_System_Array_GetValue)
ICALL(ARRAY_10, "GetValueImpl",     ves_icall_System_Array_GetValueImpl)
ICALL(ARRAY_11, "SetGenericValueImpl", ves_icall_System_Array_SetGenericValueImpl)
ICALL(ARRAY_12, "SetValue",         ves_icall_System_Array_SetValue)
ICALL(ARRAY_13, "SetValueImpl",     ves_icall_System_Array_SetValueImpl)

ICALL_TYPE(BUFFER, "System.Buffer", BUFFER_1)
ICALL(BUFFER_1, "BlockCopyInternal", ves_icall_System_Buffer_BlockCopyInternal)
ICALL(BUFFER_2, "ByteLengthInternal", ves_icall_System_Buffer_ByteLengthInternal)
ICALL(BUFFER_3, "GetByteInternal", ves_icall_System_Buffer_GetByteInternal)
ICALL(BUFFER_4, "SetByteInternal", ves_icall_System_Buffer_SetByteInternal)

ICALL_TYPE(CHAR, "System.Char", CHAR_1)
ICALL(CHAR_1, "GetDataTablePointers", ves_icall_System_Char_GetDataTablePointers)

ICALL_TYPE (COMPO_W, "System.ComponentModel.Win32Exception", COMPO_W_1)
ICALL (COMPO_W_1, "W32ErrorMessage", ves_icall_System_ComponentModel_Win32Exception_W32ErrorMessage)

ICALL_TYPE(DEFAULTC, "System.Configuration.DefaultConfig", DEFAULTC_1)
ICALL(DEFAULTC_1, "get_bundled_machine_config", get_bundled_machine_config)
ICALL(DEFAULTC_2, "get_machine_config_path", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)

/* Note that the below icall shares the same function as DefaultConfig uses */
ICALL_TYPE(INTCFGHOST, "System.Configuration.InternalConfigurationHost", INTCFGHOST_1)
ICALL(INTCFGHOST_1, "get_bundled_app_config", get_bundled_app_config)
ICALL(INTCFGHOST_2, "get_bundled_machine_config", get_bundled_machine_config)

ICALL_TYPE(CONSOLE, "System.ConsoleDriver", CONSOLE_1)
ICALL(CONSOLE_1, "InternalKeyAvailable", ves_icall_System_ConsoleDriver_InternalKeyAvailable )
ICALL(CONSOLE_2, "Isatty", ves_icall_System_ConsoleDriver_Isatty )
ICALL(CONSOLE_3, "SetBreak", ves_icall_System_ConsoleDriver_SetBreak )
ICALL(CONSOLE_4, "SetEcho", ves_icall_System_ConsoleDriver_SetEcho )
ICALL(CONSOLE_5, "TtySetup", ves_icall_System_ConsoleDriver_TtySetup )

ICALL_TYPE(CONVERT, "System.Convert", CONVERT_1)
ICALL(CONVERT_1, "InternalFromBase64CharArray", InternalFromBase64CharArray )
ICALL(CONVERT_2, "InternalFromBase64String", InternalFromBase64String )

ICALL_TYPE(TZONE, "System.CurrentSystemTimeZone", TZONE_1)
ICALL(TZONE_1, "GetTimeZoneData", ves_icall_System_CurrentSystemTimeZone_GetTimeZoneData)

ICALL_TYPE(DTIME, "System.DateTime", DTIME_1)
ICALL(DTIME_1, "GetNow", mono_100ns_datetime)
ICALL(DTIME_2, "GetTimeMonotonic", mono_100ns_ticks)

#ifndef DISABLE_DECIMAL
ICALL_TYPE(DECIMAL, "System.Decimal", DECIMAL_1)
ICALL(DECIMAL_1, "decimal2Int64", mono_decimal2Int64)
ICALL(DECIMAL_2, "decimal2UInt64", mono_decimal2UInt64)
ICALL(DECIMAL_3, "decimal2double", mono_decimal2double)
//ICALL(DECIMAL_4, "decimal2string", mono_decimal2string)
ICALL(DECIMAL_5, "decimalCompare", mono_decimalCompare)
ICALL(DECIMAL_6, "decimalDiv", mono_decimalDiv)
ICALL(DECIMAL_7, "decimalFloorAndTrunc", mono_decimalFloorAndTrunc)
ICALL(DECIMAL_8, "decimalIncr", mono_decimalIncr)
ICALL(DECIMAL_9, "decimalIntDiv", mono_decimalIntDiv)
ICALL(DECIMAL_10, "decimalMult", mono_decimalMult)
ICALL(DECIMAL_11, "decimalRound", mono_decimalRound)
ICALL(DECIMAL_12, "decimalSetExponent", mono_decimalSetExponent)
ICALL(DECIMAL_13, "double2decimal", mono_double2decimal) /* FIXME: wrong signature. */
ICALL(DECIMAL_14, "string2decimal", mono_string2decimal)
#endif

ICALL_TYPE(DELEGATE, "System.Delegate", DELEGATE_1)
ICALL(DELEGATE_1, "CreateDelegate_internal", ves_icall_System_Delegate_CreateDelegate_internal)
ICALL(DELEGATE_2, "SetMulticastInvoke", ves_icall_System_Delegate_SetMulticastInvoke)

ICALL_TYPE(DEBUGR, "System.Diagnostics.Debugger", DEBUGR_1)
ICALL(DEBUGR_1, "IsAttached_internal", ves_icall_System_Diagnostics_Debugger_IsAttached_internal)
ICALL(DEBUGR_2, "IsLogging", ves_icall_System_Diagnostics_Debugger_IsLogging)
ICALL(DEBUGR_3, "Log", ves_icall_System_Diagnostics_Debugger_Log)

ICALL_TYPE(TRACEL, "System.Diagnostics.DefaultTraceListener", TRACEL_1)
ICALL(TRACEL_1, "WriteWindowsDebugString", ves_icall_System_Diagnostics_DefaultTraceListener_WriteWindowsDebugString)

ICALL_TYPE(FILEV, "System.Diagnostics.FileVersionInfo", FILEV_1)
ICALL(FILEV_1, "GetVersionInfo_internal(string)", ves_icall_System_Diagnostics_FileVersionInfo_GetVersionInfo_internal)

#ifndef DISABLE_PROCESS_HANDLING
ICALL_TYPE(PERFCTR, "System.Diagnostics.PerformanceCounter", PERFCTR_1)
ICALL(PERFCTR_1, "FreeData", mono_perfcounter_free_data)
ICALL(PERFCTR_2, "GetImpl", mono_perfcounter_get_impl)
ICALL(PERFCTR_3, "GetSample", mono_perfcounter_get_sample)
ICALL(PERFCTR_4, "UpdateValue", mono_perfcounter_update_value)

ICALL_TYPE(PERFCTRCAT, "System.Diagnostics.PerformanceCounterCategory", PERFCTRCAT_1)
ICALL(PERFCTRCAT_1, "CategoryDelete", mono_perfcounter_category_del)
ICALL(PERFCTRCAT_2, "CategoryHelpInternal",   mono_perfcounter_category_help)
ICALL(PERFCTRCAT_3, "CounterCategoryExists", mono_perfcounter_category_exists)
ICALL(PERFCTRCAT_4, "Create",         mono_perfcounter_create)
ICALL(PERFCTRCAT_5, "GetCategoryNames", mono_perfcounter_category_names)
ICALL(PERFCTRCAT_6, "GetCounterNames", mono_perfcounter_counter_names)
ICALL(PERFCTRCAT_7, "GetInstanceNames", mono_perfcounter_instance_names)
ICALL(PERFCTRCAT_8, "InstanceExistsInternal", mono_perfcounter_instance_exists)

ICALL_TYPE(PROCESS, "System.Diagnostics.Process", PROCESS_1)
ICALL(PROCESS_1, "CreateProcess_internal(System.Diagnostics.ProcessStartInfo,intptr,intptr,intptr,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_CreateProcess_internal)
ICALL(PROCESS_2, "ExitCode_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitCode_internal)
ICALL(PROCESS_3, "ExitTime_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitTime_internal)
ICALL(PROCESS_4, "GetModules_internal(intptr)", ves_icall_System_Diagnostics_Process_GetModules_internal)
ICALL(PROCESS_5, "GetPid_internal()", ves_icall_System_Diagnostics_Process_GetPid_internal)
ICALL(PROCESS_5B, "GetPriorityClass(intptr,int&)", ves_icall_System_Diagnostics_Process_GetPriorityClass)
ICALL(PROCESS_5H, "GetProcessData", ves_icall_System_Diagnostics_Process_GetProcessData)
ICALL(PROCESS_6, "GetProcess_internal(int)", ves_icall_System_Diagnostics_Process_GetProcess_internal)
ICALL(PROCESS_7, "GetProcesses_internal()", ves_icall_System_Diagnostics_Process_GetProcesses_internal)
ICALL(PROCESS_8, "GetWorkingSet_internal(intptr,int&,int&)", ves_icall_System_Diagnostics_Process_GetWorkingSet_internal)
ICALL(PROCESS_9, "Kill_internal", ves_icall_System_Diagnostics_Process_Kill_internal)
ICALL(PROCESS_10, "ProcessName_internal(intptr)", ves_icall_System_Diagnostics_Process_ProcessName_internal)
ICALL(PROCESS_11, "Process_free_internal(intptr)", ves_icall_System_Diagnostics_Process_Process_free_internal)
ICALL(PROCESS_11B, "SetPriorityClass(intptr,int,int&)", ves_icall_System_Diagnostics_Process_SetPriorityClass)
ICALL(PROCESS_12, "SetWorkingSet_internal(intptr,int,int,bool)", ves_icall_System_Diagnostics_Process_SetWorkingSet_internal)
ICALL(PROCESS_13, "ShellExecuteEx_internal(System.Diagnostics.ProcessStartInfo,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_ShellExecuteEx_internal)
ICALL(PROCESS_14, "StartTime_internal(intptr)", ves_icall_System_Diagnostics_Process_StartTime_internal)
ICALL(PROCESS_14M, "Times", ves_icall_System_Diagnostics_Process_Times)
ICALL(PROCESS_15, "WaitForExit_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForExit_internal)
ICALL(PROCESS_16, "WaitForInputIdle_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForInputIdle_internal)

ICALL_TYPE (PROCESSHANDLE, "System.Diagnostics.Process/ProcessWaitHandle", PROCESSHANDLE_1)
ICALL (PROCESSHANDLE_1, "ProcessHandle_close(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_close)
ICALL (PROCESSHANDLE_2, "ProcessHandle_duplicate(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_duplicate)
#endif /* !DISABLE_PROCESS_HANDLING */

ICALL_TYPE(STOPWATCH, "System.Diagnostics.Stopwatch", STOPWATCH_1)
ICALL(STOPWATCH_1, "GetTimestamp", mono_100ns_ticks)

ICALL_TYPE(DOUBLE, "System.Double", DOUBLE_1)
ICALL(DOUBLE_1, "ParseImpl",    mono_double_ParseImpl)

ICALL_TYPE(ENUM, "System.Enum", ENUM_1)
ICALL(ENUM_1, "ToObject", ves_icall_System_Enum_ToObject)
ICALL(ENUM_5, "compare_value_to", ves_icall_System_Enum_compare_value_to)
ICALL(ENUM_4, "get_hashcode", ves_icall_System_Enum_get_hashcode)
ICALL(ENUM_3, "get_underlying_type", ves_icall_System_Enum_get_underlying_type)
ICALL(ENUM_2, "get_value", ves_icall_System_Enum_get_value)

ICALL_TYPE(ENV, "System.Environment", ENV_1)
ICALL(ENV_1, "Exit", ves_icall_System_Environment_Exit)
ICALL(ENV_2, "GetCommandLineArgs", mono_runtime_get_main_args)
ICALL(ENV_3, "GetEnvironmentVariableNames", ves_icall_System_Environment_GetEnvironmentVariableNames)
ICALL(ENV_4, "GetLogicalDrivesInternal", ves_icall_System_Environment_GetLogicalDrives )
ICALL(ENV_5, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(ENV_51, "GetNewLine", ves_icall_System_Environment_get_NewLine)
ICALL(ENV_6, "GetOSVersionString", ves_icall_System_Environment_GetOSVersionString)
ICALL(ENV_6a, "GetPageSize", mono_pagesize)
ICALL(ENV_7, "GetWindowsFolderPath", ves_icall_System_Environment_GetWindowsFolderPath)
ICALL(ENV_8, "InternalSetEnvironmentVariable", ves_icall_System_Environment_InternalSetEnvironmentVariable)
ICALL(ENV_9, "get_ExitCode", mono_environment_exitcode_get)
ICALL(ENV_10, "get_HasShutdownStarted", ves_icall_System_Environment_get_HasShutdownStarted)
ICALL(ENV_11, "get_MachineName", ves_icall_System_Environment_get_MachineName)
ICALL(ENV_13, "get_Platform", ves_icall_System_Environment_get_Platform)
ICALL(ENV_14, "get_ProcessorCount", mono_cpu_count)
ICALL(ENV_15, "get_TickCount", mono_msec_ticks)
ICALL(ENV_16, "get_UserName", ves_icall_System_Environment_get_UserName)
ICALL(ENV_16m, "internalBroadcastSettingChange", ves_icall_System_Environment_BroadcastSettingChange)
ICALL(ENV_17, "internalGetEnvironmentVariable", ves_icall_System_Environment_GetEnvironmentVariable)
ICALL(ENV_18, "internalGetGacPath", ves_icall_System_Environment_GetGacPath)
ICALL(ENV_19, "internalGetHome", ves_icall_System_Environment_InternalGetHome)
ICALL(ENV_20, "set_ExitCode", mono_environment_exitcode_set)

ICALL_TYPE(GC, "System.GC", GC_0)
ICALL(GC_0, "CollectionCount", mono_gc_collection_count)
ICALL(GC_0a, "GetGeneration", mono_gc_get_generation)
ICALL(GC_1, "GetTotalMemory", ves_icall_System_GC_GetTotalMemory)
ICALL(GC_2, "InternalCollect", ves_icall_System_GC_InternalCollect)
ICALL(GC_3, "KeepAlive", ves_icall_System_GC_KeepAlive)
ICALL(GC_4, "ReRegisterForFinalize", ves_icall_System_GC_ReRegisterForFinalize)
ICALL(GC_4a, "RecordPressure", mono_gc_add_memory_pressure)
ICALL(GC_5, "SuppressFinalize", ves_icall_System_GC_SuppressFinalize)
ICALL(GC_6, "WaitForPendingFinalizers", ves_icall_System_GC_WaitForPendingFinalizers)
ICALL(GC_7, "get_MaxGeneration", mono_gc_max_generation)
ICALL(GC_9, "get_ephemeron_tombstone", ves_icall_System_GC_get_ephemeron_tombstone)
ICALL(GC_8, "register_ephemeron_array", ves_icall_System_GC_register_ephemeron_array)

ICALL_TYPE(COMPINF, "System.Globalization.CompareInfo", COMPINF_1)
ICALL(COMPINF_1, "assign_sortkey(object,string,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_assign_sortkey)
ICALL(COMPINF_2, "construct_compareinfo(string)", ves_icall_System_Globalization_CompareInfo_construct_compareinfo)
ICALL(COMPINF_3, "free_internal_collator()", ves_icall_System_Globalization_CompareInfo_free_internal_collator)
ICALL(COMPINF_4, "internal_compare(string,int,int,string,int,int,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_internal_compare)
ICALL(COMPINF_5, "internal_index(string,int,int,char,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index_char)
ICALL(COMPINF_6, "internal_index(string,int,int,string,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index)

ICALL_TYPE(CULINF, "System.Globalization.CultureInfo", CULINF_2)
ICALL(CULINF_2, "construct_datetime_format", ves_icall_System_Globalization_CultureInfo_construct_datetime_format)
ICALL(CULINF_4, "construct_internal_locale_from_current_locale", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_current_locale)
ICALL(CULINF_5, "construct_internal_locale_from_lcid", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_lcid)
ICALL(CULINF_6, "construct_internal_locale_from_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_name)
ICALL(CULINF_7, "construct_internal_locale_from_specific_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_specific_name)
ICALL(CULINF_8, "construct_number_format", ves_icall_System_Globalization_CultureInfo_construct_number_format)
ICALL(CULINF_9, "internal_get_cultures", ves_icall_System_Globalization_CultureInfo_internal_get_cultures)
//ICALL(CULINF_10, "internal_is_lcid_neutral", ves_icall_System_Globalization_CultureInfo_internal_is_lcid_neutral)

ICALL_TYPE(REGINF, "System.Globalization.RegionInfo", REGINF_1)
ICALL(REGINF_1, "construct_internal_region_from_lcid", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_lcid)
ICALL(REGINF_2, "construct_internal_region_from_name", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_name)

#ifndef PLATFORM_NO_DRIVEINFO
ICALL_TYPE(IODRIVEINFO, "System.IO.DriveInfo", IODRIVEINFO_1)
ICALL(IODRIVEINFO_1, "GetDiskFreeSpaceInternal", ves_icall_System_IO_DriveInfo_GetDiskFreeSpace)
ICALL(IODRIVEINFO_2, "GetDriveFormat", ves_icall_System_IO_DriveInfo_GetDriveFormat)
ICALL(IODRIVEINFO_3, "GetDriveTypeInternal", ves_icall_System_IO_DriveInfo_GetDriveType)
#endif

ICALL_TYPE(FAMW, "System.IO.FAMWatcher", FAMW_1)
ICALL(FAMW_1, "InternalFAMNextEvent", ves_icall_System_IO_FAMW_InternalFAMNextEvent)

ICALL_TYPE(FILEW, "System.IO.FileSystemWatcher", FILEW_4)
ICALL(FILEW_4, "InternalSupportsFSW", ves_icall_System_IO_FSW_SupportsFSW)

ICALL_TYPE(INOW, "System.IO.InotifyWatcher", INOW_1)
ICALL(INOW_1, "AddWatch", ves_icall_System_IO_InotifyWatcher_AddWatch)
ICALL(INOW_2, "GetInotifyInstance", ves_icall_System_IO_InotifyWatcher_GetInotifyInstance)
ICALL(INOW_3, "RemoveWatch", ves_icall_System_IO_InotifyWatcher_RemoveWatch)

#if defined (TARGET_IOS) || defined (TARGET_ANDROID)
ICALL_TYPE(MMAPIMPL, "System.IO.MemoryMappedFiles.MemoryMapImpl", MMAPIMPL_1)
ICALL(MMAPIMPL_1, "mono_filesize_from_fd", mono_filesize_from_fd)
ICALL(MMAPIMPL_2, "mono_filesize_from_path", mono_filesize_from_path)
#endif


ICALL_TYPE(MONOIO, "System.IO.MonoIO", MONOIO_1)
ICALL(MONOIO_1, "Close(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Close)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_2, "CopyFile(string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CopyFile)
ICALL(MONOIO_3, "CreateDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CreateDirectory)
ICALL(MONOIO_4, "CreatePipe(intptr&,intptr&)", ves_icall_System_IO_MonoIO_CreatePipe)
ICALL(MONOIO_5, "DeleteFile(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_DeleteFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_34, "DuplicateHandle", ves_icall_System_IO_MonoIO_DuplicateHandle)
ICALL(MONOIO_37, "FindClose", ves_icall_System_IO_MonoIO_FindClose)
ICALL(MONOIO_35, "FindFirst", ves_icall_System_IO_MonoIO_FindFirst)
ICALL(MONOIO_36, "FindNext", ves_icall_System_IO_MonoIO_FindNext)
ICALL(MONOIO_6, "Flush(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Flush)
ICALL(MONOIO_7, "GetCurrentDirectory(System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetCurrentDirectory)
ICALL(MONOIO_8, "GetFileAttributes(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileAttributes)
ICALL(MONOIO_9, "GetFileStat(string,System.IO.MonoIOStat&,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileStat)
ICALL(MONOIO_10, "GetFileSystemEntries", ves_icall_System_IO_MonoIO_GetFileSystemEntries)
ICALL(MONOIO_11, "GetFileType(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileType)
ICALL(MONOIO_12, "GetLength(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_13, "GetTempPath(string&)", ves_icall_System_IO_MonoIO_GetTempPath)
ICALL(MONOIO_14, "Lock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Lock)
ICALL(MONOIO_15, "MoveFile(string,string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_MoveFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_16, "Open(string,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.IO.FileOptions,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Open)
ICALL(MONOIO_17, "Read(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Read)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_18, "RemoveDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_RemoveDirectory)
ICALL(MONOIO_18M, "ReplaceFile(string,string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_ReplaceFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_19, "Seek(intptr,long,System.IO.SeekOrigin,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Seek)
ICALL(MONOIO_20, "SetCurrentDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetCurrentDirectory)
ICALL(MONOIO_21, "SetFileAttributes(string,System.IO.FileAttributes,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileAttributes)
ICALL(MONOIO_22, "SetFileTime(intptr,long,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileTime)
ICALL(MONOIO_23, "SetLength(intptr,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_24, "Unlock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Unlock)
#endif
ICALL(MONOIO_25, "Write(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Write)
ICALL(MONOIO_26, "get_AltDirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_AltDirectorySeparatorChar)
ICALL(MONOIO_27, "get_ConsoleError", ves_icall_System_IO_MonoIO_get_ConsoleError)
ICALL(MONOIO_28, "get_ConsoleInput", ves_icall_System_IO_MonoIO_get_ConsoleInput)
ICALL(MONOIO_29, "get_ConsoleOutput", ves_icall_System_IO_MonoIO_get_ConsoleOutput)
ICALL(MONOIO_30, "get_DirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_DirectorySeparatorChar)
ICALL(MONOIO_31, "get_InvalidPathChars", ves_icall_System_IO_MonoIO_get_InvalidPathChars)
ICALL(MONOIO_32, "get_PathSeparator", ves_icall_System_IO_MonoIO_get_PathSeparator)
ICALL(MONOIO_33, "get_VolumeSeparatorChar", ves_icall_System_IO_MonoIO_get_VolumeSeparatorChar)

ICALL_TYPE(IOPATH, "System.IO.Path", IOPATH_1)
ICALL(IOPATH_1, "get_temp_path", ves_icall_System_IO_get_temp_path)

ICALL_TYPE(MATH, "System.Math", MATH_1)
ICALL(MATH_1, "Acos", ves_icall_System_Math_Acos)
ICALL(MATH_2, "Asin", ves_icall_System_Math_Asin)
ICALL(MATH_3, "Atan", ves_icall_System_Math_Atan)
ICALL(MATH_4, "Atan2", ves_icall_System_Math_Atan2)
ICALL(MATH_5, "Cos", ves_icall_System_Math_Cos)
ICALL(MATH_6, "Cosh", ves_icall_System_Math_Cosh)
ICALL(MATH_7, "Exp", ves_icall_System_Math_Exp)
ICALL(MATH_8, "Floor", ves_icall_System_Math_Floor)
ICALL(MATH_9, "Log", ves_icall_System_Math_Log)
ICALL(MATH_10, "Log10", ves_icall_System_Math_Log10)
ICALL(MATH_11, "Pow", ves_icall_System_Math_Pow)
ICALL(MATH_12, "Round", ves_icall_System_Math_Round)
ICALL(MATH_13, "Round2", ves_icall_System_Math_Round2)
ICALL(MATH_14, "Sin", ves_icall_System_Math_Sin)
ICALL(MATH_15, "Sinh", ves_icall_System_Math_Sinh)
ICALL(MATH_16, "Sqrt", ves_icall_System_Math_Sqrt)
ICALL(MATH_17, "Tan", ves_icall_System_Math_Tan)
ICALL(MATH_18, "Tanh", ves_icall_System_Math_Tanh)

ICALL_TYPE(MCATTR, "System.MonoCustomAttrs", MCATTR_1)
ICALL(MCATTR_1, "GetCustomAttributesDataInternal", mono_reflection_get_custom_attrs_data)
ICALL(MCATTR_2, "GetCustomAttributesInternal", custom_attrs_get_by_type)
ICALL(MCATTR_3, "IsDefinedInternal", custom_attrs_defined_internal)

ICALL_TYPE(MENUM, "System.MonoEnumInfo", MENUM_1)
ICALL(MENUM_1, "get_enum_info", ves_icall_get_enum_info)

ICALL_TYPE(MTYPE, "System.MonoType", MTYPE_1)
ICALL(MTYPE_1, "GetArrayRank", ves_icall_MonoType_GetArrayRank)
ICALL(MTYPE_2, "GetConstructors", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_3, "GetConstructors_internal", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_4, "GetCorrespondingInflatedConstructor", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_5, "GetCorrespondingInflatedMethod", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_6, "GetElementType", ves_icall_MonoType_GetElementType)
ICALL(MTYPE_7, "GetEvents_internal", ves_icall_Type_GetEvents_internal)
ICALL(MTYPE_8, "GetField", ves_icall_Type_GetField)
ICALL(MTYPE_9, "GetFields_internal", ves_icall_Type_GetFields_internal)
ICALL(MTYPE_10, "GetGenericArguments", ves_icall_MonoType_GetGenericArguments)
ICALL(MTYPE_11, "GetInterfaces", ves_icall_Type_GetInterfaces)
ICALL(MTYPE_12, "GetMethodsByName", ves_icall_Type_GetMethodsByName)
ICALL(MTYPE_13, "GetNestedType", ves_icall_Type_GetNestedType)
ICALL(MTYPE_14, "GetNestedTypes", ves_icall_Type_GetNestedTypes)
ICALL(MTYPE_15, "GetPropertiesByName", ves_icall_Type_GetPropertiesByName)
ICALL(MTYPE_16, "InternalGetEvent", ves_icall_MonoType_GetEvent)
ICALL(MTYPE_17, "IsByRefImpl", ves_icall_type_isbyref)
ICALL(MTYPE_18, "IsCOMObjectImpl", ves_icall_type_iscomobject)
ICALL(MTYPE_19, "IsPointerImpl", ves_icall_type_ispointer)
ICALL(MTYPE_20, "IsPrimitiveImpl", ves_icall_type_isprimitive)
ICALL(MTYPE_21, "getFullName", ves_icall_System_MonoType_getFullName)
ICALL(MTYPE_22, "get_Assembly", ves_icall_MonoType_get_Assembly)
ICALL(MTYPE_23, "get_BaseType", ves_icall_get_type_parent)
ICALL(MTYPE_24, "get_DeclaringMethod", ves_icall_MonoType_get_DeclaringMethod)
ICALL(MTYPE_25, "get_DeclaringType", ves_icall_MonoType_get_DeclaringType)
ICALL(MTYPE_26, "get_IsGenericParameter", ves_icall_MonoType_get_IsGenericParameter)
ICALL(MTYPE_27, "get_Module", ves_icall_MonoType_get_Module)
ICALL(MTYPE_28, "get_Name", ves_icall_MonoType_get_Name)
ICALL(MTYPE_29, "get_Namespace", ves_icall_MonoType_get_Namespace)
ICALL(MTYPE_31, "get_attributes", ves_icall_get_attributes)
ICALL(MTYPE_33, "get_core_clr_security_level", vell_icall_MonoType_get_core_clr_security_level)
ICALL(MTYPE_32, "type_from_obj", mono_type_type_from_obj)

#ifndef DISABLE_SOCKETS
ICALL_TYPE(NDNS, "System.Net.Dns", NDNS_1)
ICALL(NDNS_1, "GetHostByAddr_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByAddr_internal)
ICALL(NDNS_2, "GetHostByName_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByName_internal)
ICALL(NDNS_3, "GetHostName_internal(string&)", ves_icall_System_Net_Dns_GetHostName_internal)

ICALL_TYPE(SOCK, "System.Net.Sockets.Socket", SOCK_1)
ICALL(SOCK_1, "Accept_internal(intptr,int&,bool)", ves_icall_System_Net_Sockets_Socket_Accept_internal)
ICALL(SOCK_2, "Available_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Available_internal)
ICALL(SOCK_3, "Bind_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Bind_internal)
ICALL(SOCK_4, "Blocking_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Blocking_internal)
ICALL(SOCK_5, "Close_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Close_internal)
ICALL(SOCK_6, "Connect_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Connect_internal)
ICALL (SOCK_6a, "Disconnect_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Disconnect_internal)
ICALL(SOCK_7, "GetSocketOption_arr_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,byte[]&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_arr_internal)
ICALL(SOCK_8, "GetSocketOption_obj_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_obj_internal)
ICALL(SOCK_9, "Listen_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_Listen_internal)
ICALL(SOCK_10, "LocalEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_LocalEndPoint_internal)
ICALL(SOCK_11, "Poll_internal", ves_icall_System_Net_Sockets_Socket_Poll_internal)
ICALL(SOCK_11a, "Receive_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_array_internal)
ICALL(SOCK_12, "Receive_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_internal)
ICALL(SOCK_13, "RecvFrom_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress&,int&)", ves_icall_System_Net_Sockets_Socket_RecvFrom_internal)
ICALL(SOCK_14, "RemoteEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_RemoteEndPoint_internal)
ICALL(SOCK_15, "Select_internal(System.Net.Sockets.Socket[]&,int,int&)", ves_icall_System_Net_Sockets_Socket_Select_internal)
ICALL(SOCK_15a, "SendFile(intptr,string,byte[],byte[],System.Net.Sockets.TransmitFileOptions)", ves_icall_System_Net_Sockets_Socket_SendFile)
ICALL(SOCK_16, "SendTo_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_SendTo_internal)
ICALL(SOCK_16a, "Send_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_array_internal)
ICALL(SOCK_17, "Send_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_internal)
ICALL(SOCK_18, "SetSocketOption_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object,byte[],int,int&)", ves_icall_System_Net_Sockets_Socket_SetSocketOption_internal)
ICALL(SOCK_19, "Shutdown_internal(intptr,System.Net.Sockets.SocketShutdown,int&)", ves_icall_System_Net_Sockets_Socket_Shutdown_internal)
ICALL(SOCK_20, "Socket_internal(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType,int&)", ves_icall_System_Net_Sockets_Socket_Socket_internal)
ICALL(SOCK_21, "WSAIoctl(intptr,int,byte[],byte[],int&)", ves_icall_System_Net_Sockets_Socket_WSAIoctl)
ICALL(SOCK_21a, "cancel_blocking_socket_operation", icall_cancel_blocking_socket_operation)
ICALL(SOCK_22, "socket_pool_queue", icall_append_io_job)

ICALL_TYPE(SOCKEX, "System.Net.Sockets.SocketException", SOCKEX_1)
ICALL(SOCKEX_1, "WSAGetLastError_internal", ves_icall_System_Net_Sockets_SocketException_WSAGetLastError_internal)
#endif /* !DISABLE_SOCKETS */

ICALL_TYPE(NUMBER_FORMATTER, "System.NumberFormatter", NUMBER_FORMATTER_1)
ICALL(NUMBER_FORMATTER_1, "GetFormatterTables", ves_icall_System_NumberFormatter_GetFormatterTables)

ICALL_TYPE(OBJ, "System.Object", OBJ_1)
ICALL(OBJ_1, "GetType", ves_icall_System_Object_GetType)
ICALL(OBJ_2, "InternalGetHashCode", mono_object_hash)
ICALL(OBJ_3, "MemberwiseClone", ves_icall_System_Object_MemberwiseClone)
ICALL(OBJ_4, "obj_address", ves_icall_System_Object_obj_address)

ICALL_TYPE(ASSEM, "System.Reflection.Assembly", ASSEM_1)
ICALL(ASSEM_1, "FillName", ves_icall_System_Reflection_Assembly_FillName)
ICALL(ASSEM_2, "GetCallingAssembly", ves_icall_System_Reflection_Assembly_GetCallingAssembly)
ICALL(ASSEM_3, "GetEntryAssembly", ves_icall_System_Reflection_Assembly_GetEntryAssembly)
ICALL(ASSEM_4, "GetExecutingAssembly", ves_icall_System_Reflection_Assembly_GetExecutingAssembly)
ICALL(ASSEM_5, "GetFilesInternal", ves_icall_System_Reflection_Assembly_GetFilesInternal)
ICALL(ASSEM_6, "GetManifestModuleInternal", ves_icall_System_Reflection_Assembly_GetManifestModuleInternal)
ICALL(ASSEM_7, "GetManifestResourceInfoInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInfoInternal)
ICALL(ASSEM_8, "GetManifestResourceInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInternal)
ICALL(ASSEM_9, "GetManifestResourceNames", ves_icall_System_Reflection_Assembly_GetManifestResourceNames)
ICALL(ASSEM_10, "GetModulesInternal", ves_icall_System_Reflection_Assembly_GetModulesInternal)
//ICALL(ASSEM_11, "GetNamespaces", ves_icall_System_Reflection_Assembly_GetNamespaces)
ICALL(ASSEM_12, "GetReferencedAssemblies", ves_icall_System_Reflection_Assembly_GetReferencedAssemblies)
ICALL(ASSEM_13, "GetTypes", ves_icall_System_Reflection_Assembly_GetTypes)
ICALL(ASSEM_14, "InternalGetAssemblyName", ves_icall_System_Reflection_Assembly_InternalGetAssemblyName)
ICALL(ASSEM_15, "InternalGetType", ves_icall_System_Reflection_Assembly_InternalGetType)
ICALL(ASSEM_16, "InternalImageRuntimeVersion", ves_icall_System_Reflection_Assembly_InternalImageRuntimeVersion)
ICALL(ASSEM_17, "LoadFrom", ves_icall_System_Reflection_Assembly_LoadFrom)
ICALL(ASSEM_18, "LoadPermissions", ves_icall_System_Reflection_Assembly_LoadPermissions)
	/*
	 * Private icalls for the Mono Debugger
	 */
ICALL(ASSEM_19, "MonoDebugger_GetMethodToken", ves_icall_MonoDebugger_GetMethodToken)

	/* normal icalls again */
ICALL(ASSEM_20, "get_EntryPoint", ves_icall_System_Reflection_Assembly_get_EntryPoint)
ICALL(ASSEM_21, "get_ReflectionOnly", ves_icall_System_Reflection_Assembly_get_ReflectionOnly)
ICALL(ASSEM_22, "get_code_base", ves_icall_System_Reflection_Assembly_get_code_base)
ICALL(ASSEM_23, "get_fullname", ves_icall_System_Reflection_Assembly_get_fullName)
ICALL(ASSEM_24, "get_global_assembly_cache", ves_icall_System_Reflection_Assembly_get_global_assembly_cache)
ICALL(ASSEM_25, "get_location", ves_icall_System_Reflection_Assembly_get_location)
ICALL(ASSEM_26, "load_with_partial_name", ves_icall_System_Reflection_Assembly_load_with_partial_name)

ICALL_TYPE(ASSEMN, "System.Reflection.AssemblyName", ASSEMN_1)
ICALL(ASSEMN_1, "ParseName", ves_icall_System_Reflection_AssemblyName_ParseName)
ICALL(ASSEMN_2, "get_public_token", mono_digest_get_public_token)

ICALL_TYPE(CATTR_DATA, "System.Reflection.CustomAttributeData", CATTR_DATA_1)
ICALL(CATTR_DATA_1, "ResolveArgumentsInternal", mono_reflection_resolve_custom_attribute_data)

ICALL_TYPE(ASSEMB, "System.Reflection.Emit.AssemblyBuilder", ASSEMB_1)
ICALL(ASSEMB_1, "InternalAddModule", mono_image_load_module_dynamic)
ICALL(ASSEMB_2, "basic_init", mono_image_basic_init)

ICALL_TYPE(CATTRB, "System.Reflection.Emit.CustomAttributeBuilder", CATTRB_1)
ICALL(CATTRB_1, "GetBlob", mono_reflection_get_custom_attrs_blob)

#ifndef DISABLE_REFLECTION_EMIT
ICALL_TYPE(DERIVEDTYPE, "System.Reflection.Emit.DerivedType", DERIVEDTYPE_1)
ICALL(DERIVEDTYPE_1, "create_unmanaged_type", mono_reflection_create_unmanaged_type)
#endif

ICALL_TYPE(DYNM, "System.Reflection.Emit.DynamicMethod", DYNM_1)
ICALL(DYNM_1, "create_dynamic_method", mono_reflection_create_dynamic_method)

ICALL_TYPE(ENUMB, "System.Reflection.Emit.EnumBuilder", ENUMB_1)
ICALL(ENUMB_1, "setup_enum_type", ves_icall_EnumBuilder_setup_enum_type)

ICALL_TYPE(GPARB, "System.Reflection.Emit.GenericTypeParameterBuilder", GPARB_1)
ICALL(GPARB_1, "initialize", mono_reflection_initialize_generic_parameter)

ICALL_TYPE(METHODB, "System.Reflection.Emit.MethodBuilder", METHODB_1)
ICALL(METHODB_1, "MakeGenericMethod", mono_reflection_bind_generic_method_parameters)

ICALL_TYPE(MODULEB, "System.Reflection.Emit.ModuleBuilder", MODULEB_8)
ICALL(MODULEB_8, "RegisterToken", ves_icall_ModuleBuilder_RegisterToken)
ICALL(MODULEB_1, "WriteToFile", ves_icall_ModuleBuilder_WriteToFile)
ICALL(MODULEB_2, "basic_init", mono_image_module_basic_init)
ICALL(MODULEB_3, "build_metadata", ves_icall_ModuleBuilder_build_metadata)
ICALL(MODULEB_4, "create_modified_type", ves_icall_ModuleBuilder_create_modified_type)
ICALL(MODULEB_5, "getMethodToken", ves_icall_ModuleBuilder_getMethodToken)
ICALL(MODULEB_6, "getToken", ves_icall_ModuleBuilder_getToken)
ICALL(MODULEB_7, "getUSIndex", mono_image_insert_string)
ICALL(MODULEB_9, "set_wrappers_type", mono_image_set_wrappers_type)

ICALL_TYPE(SIGH, "System.Reflection.Emit.SignatureHelper", SIGH_1)
ICALL(SIGH_1, "get_signature_field", mono_reflection_sighelper_get_signature_field)
ICALL(SIGH_2, "get_signature_local", mono_reflection_sighelper_get_signature_local)

ICALL_TYPE(TYPEB, "System.Reflection.Emit.TypeBuilder", TYPEB_1)
ICALL(TYPEB_1, "create_generic_class", mono_reflection_create_generic_class)
ICALL(TYPEB_2, "create_internal_class", mono_reflection_create_internal_class)
ICALL(TYPEB_3, "create_runtime_class", mono_reflection_create_runtime_class)
ICALL(TYPEB_4, "get_IsGenericParameter", ves_icall_TypeBuilder_get_IsGenericParameter)
ICALL(TYPEB_5, "get_event_info", mono_reflection_event_builder_get_event_info)
ICALL(TYPEB_6, "setup_generic_class", mono_reflection_setup_generic_class)
ICALL(TYPEB_7, "setup_internal_class", mono_reflection_setup_internal_class)

ICALL_TYPE(FIELDI, "System.Reflection.FieldInfo", FILEDI_1)
ICALL(FILEDI_1, "GetTypeModifiers", ves_icall_System_Reflection_FieldInfo_GetTypeModifiers)
ICALL(FILEDI_2, "get_marshal_info", ves_icall_System_Reflection_FieldInfo_get_marshal_info)
ICALL(FILEDI_3, "internal_from_handle_type", ves_icall_System_Reflection_FieldInfo_internal_from_handle_type)

ICALL_TYPE(MEMBERI, "System.Reflection.MemberInfo", MEMBERI_1)
ICALL(MEMBERI_1, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MBASE, "System.Reflection.MethodBase", MBASE_1)
ICALL(MBASE_1, "GetCurrentMethod", ves_icall_GetCurrentMethod)
ICALL(MBASE_2, "GetMethodBodyInternal", ves_icall_System_Reflection_MethodBase_GetMethodBodyInternal)
ICALL(MBASE_3, "GetMethodFromHandleInternal", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternal)
ICALL(MBASE_4, "GetMethodFromHandleInternalType", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternalType)

ICALL_TYPE(MODULE, "System.Reflection.Module", MODULE_1)
ICALL(MODULE_1, "Close", ves_icall_System_Reflection_Module_Close)
ICALL(MODULE_2, "GetGlobalType", ves_icall_System_Reflection_Module_GetGlobalType)
ICALL(MODULE_3, "GetGuidInternal", ves_icall_System_Reflection_Module_GetGuidInternal)
ICALL(MODULE_14, "GetHINSTANCE", ves_icall_System_Reflection_Module_GetHINSTANCE)
ICALL(MODULE_4, "GetMDStreamVersion", ves_icall_System_Reflection_Module_GetMDStreamVersion)
ICALL(MODULE_5, "GetPEKind", ves_icall_System_Reflection_Module_GetPEKind)
ICALL(MODULE_6, "InternalGetTypes", ves_icall_System_Reflection_Module_InternalGetTypes)
ICALL(MODULE_7, "ResolveFieldToken", ves_icall_System_Reflection_Module_ResolveFieldToken)
ICALL(MODULE_8, "ResolveMemberToken", ves_icall_System_Reflection_Module_ResolveMemberToken)
ICALL(MODULE_9, "ResolveMethodToken", ves_icall_System_Reflection_Module_ResolveMethodToken)
ICALL(MODULE_10, "ResolveSignature", ves_icall_System_Reflection_Module_ResolveSignature)
ICALL(MODULE_11, "ResolveStringToken", ves_icall_System_Reflection_Module_ResolveStringToken)
ICALL(MODULE_12, "ResolveTypeToken", ves_icall_System_Reflection_Module_ResolveTypeToken)
ICALL(MODULE_13, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MCMETH, "System.Reflection.MonoCMethod", MCMETH_1)
ICALL(MCMETH_1, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MCMETH_2, "InternalInvoke", ves_icall_InternalInvoke)

ICALL_TYPE(MEVIN, "System.Reflection.MonoEventInfo", MEVIN_1)
ICALL(MEVIN_1, "get_event_info", ves_icall_get_event_info)

ICALL_TYPE(MFIELD, "System.Reflection.MonoField", MFIELD_1)
ICALL(MFIELD_1, "GetFieldOffset", ves_icall_MonoField_GetFieldOffset)
ICALL(MFIELD_2, "GetParentType", ves_icall_MonoField_GetParentType)
ICALL(MFIELD_5, "GetRawConstantValue", ves_icall_MonoField_GetRawConstantValue)
ICALL(MFIELD_3, "GetValueInternal", ves_icall_MonoField_GetValueInternal)
ICALL(MFIELD_6, "ResolveType", ves_icall_MonoField_ResolveType)
ICALL(MFIELD_4, "SetValueInternal", ves_icall_MonoField_SetValueInternal)

ICALL_TYPE(MGENCM, "System.Reflection.MonoGenericCMethod", MGENCM_1)
ICALL(MGENCM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MGENCL, "System.Reflection.MonoGenericClass", MGENCL_5)
ICALL(MGENCL_5, "initialize", mono_reflection_generic_class_initialize)
ICALL(MGENCL_6, "register_with_runtime", mono_reflection_register_with_runtime)

/* note this is the same as above: unify */
ICALL_TYPE(MGENM, "System.Reflection.MonoGenericMethod", MGENM_1)
ICALL(MGENM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MMETH, "System.Reflection.MonoMethod", MMETH_1)
ICALL(MMETH_1, "GetDllImportAttribute", ves_icall_MonoMethod_GetDllImportAttribute)
ICALL(MMETH_2, "GetGenericArguments", ves_icall_MonoMethod_GetGenericArguments)
ICALL(MMETH_3, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MMETH_4, "InternalInvoke", ves_icall_InternalInvoke)
ICALL(MMETH_5, "MakeGenericMethod_impl", mono_reflection_bind_generic_method_parameters)
ICALL(MMETH_6, "get_IsGenericMethod", ves_icall_MonoMethod_get_IsGenericMethod)
ICALL(MMETH_7, "get_IsGenericMethodDefinition", ves_icall_MonoMethod_get_IsGenericMethodDefinition)
ICALL(MMETH_8, "get_base_method", ves_icall_MonoMethod_get_base_method)
ICALL(MMETH_9, "get_name", ves_icall_MonoMethod_get_name)

ICALL_TYPE(MMETHI, "System.Reflection.MonoMethodInfo", MMETHI_4)
ICALL(MMETHI_4, "get_method_attributes", vell_icall_get_method_attributes)
ICALL(MMETHI_1, "get_method_info", ves_icall_get_method_info)
ICALL(MMETHI_2, "get_parameter_info", ves_icall_get_parameter_info)
ICALL(MMETHI_3, "get_retval_marshal", ves_icall_System_MonoMethodInfo_get_retval_marshal)

ICALL_TYPE(MPROPI, "System.Reflection.MonoPropertyInfo", MPROPI_1)
ICALL(MPROPI_1, "GetTypeModifiers", property_info_get_type_modifiers)
ICALL(MPROPI_3, "get_default_value", property_info_get_default_value)
ICALL(MPROPI_2, "get_property_info", ves_icall_get_property_info)

ICALL_TYPE(PARAMI, "System.Reflection.ParameterInfo", PARAMI_1)
ICALL(PARAMI_1, "GetMetadataToken", mono_reflection_get_token)
ICALL(PARAMI_2, "GetTypeModifiers", param_info_get_type_modifiers)

ICALL_TYPE(RUNH, "System.Runtime.CompilerServices.RuntimeHelpers", RUNH_1)
ICALL(RUNH_1, "GetObjectValue", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetObjectValue)
	 /* REMOVEME: no longer needed, just so we dont break things when not needed */
ICALL(RUNH_2, "GetOffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)
ICALL(RUNH_3, "InitializeArray", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_InitializeArray)
ICALL(RUNH_4, "RunClassConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunClassConstructor)
ICALL(RUNH_5, "RunModuleConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunModuleConstructor)
ICALL(RUNH_5h, "SufficientExecutionStack", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_SufficientExecutionStack)
ICALL(RUNH_6, "get_OffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)

ICALL_TYPE(GCH, "System.Runtime.InteropServices.GCHandle", GCH_1)
ICALL(GCH_1, "CheckCurrentDomain", GCHandle_CheckCurrentDomain)
ICALL(GCH_2, "FreeHandle", ves_icall_System_GCHandle_FreeHandle)
ICALL(GCH_3, "GetAddrOfPinnedObject", ves_icall_System_GCHandle_GetAddrOfPinnedObject)
ICALL(GCH_4, "GetTarget", ves_icall_System_GCHandle_GetTarget)
ICALL(GCH_5, "GetTargetHandle", ves_icall_System_GCHandle_GetTargetHandle)

#ifndef DISABLE_COM
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_1)
ICALL(MARSHAL_1, "AddRefInternal", ves_icall_System_Runtime_InteropServices_Marshal_AddRefInternal)
#else
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_2)
#endif
ICALL(MARSHAL_2, "AllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_AllocCoTaskMem)
ICALL(MARSHAL_3, "AllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_AllocHGlobal)
ICALL(MARSHAL_4, "DestroyStructure", ves_icall_System_Runtime_InteropServices_Marshal_DestroyStructure)
ICALL(MARSHAL_5, "FreeBSTR", ves_icall_System_Runtime_InteropServices_Marshal_FreeBSTR)
ICALL(MARSHAL_6, "FreeCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_FreeCoTaskMem)
ICALL(MARSHAL_7, "FreeHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_FreeHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_44, "GetCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetCCW)
ICALL(MARSHAL_8, "GetComSlotForMethodInfoInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetComSlotForMethodInfoInternal)
#endif
ICALL(MARSHAL_9, "GetDelegateForFunctionPointerInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetDelegateForFunctionPointerInternal)
ICALL(MARSHAL_10, "GetFunctionPointerForDelegateInternal", mono_delegate_to_ftnptr)
#ifndef DISABLE_COM
ICALL(MARSHAL_45, "GetIDispatchForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIDispatchForObjectInternal)
ICALL(MARSHAL_46, "GetIUnknownForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIUnknownForObjectInternal)
#endif
ICALL(MARSHAL_11, "GetLastWin32Error", ves_icall_System_Runtime_InteropServices_Marshal_GetLastWin32Error)
#ifndef DISABLE_COM
ICALL(MARSHAL_47, "GetObjectForCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetObjectForCCW)
ICALL(MARSHAL_48, "IsComObject", ves_icall_System_Runtime_InteropServices_Marshal_IsComObject)
#endif
ICALL(MARSHAL_12, "OffsetOf", ves_icall_System_Runtime_InteropServices_Marshal_OffsetOf)
ICALL(MARSHAL_13, "Prelink", ves_icall_System_Runtime_InteropServices_Marshal_Prelink)
ICALL(MARSHAL_14, "PrelinkAll", ves_icall_System_Runtime_InteropServices_Marshal_PrelinkAll)
ICALL(MARSHAL_15, "PtrToStringAnsi(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi)
ICALL(MARSHAL_16, "PtrToStringAnsi(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi_len)
#ifndef DISABLE_COM
ICALL(MARSHAL_17, "PtrToStringBSTR", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringBSTR)
#endif
ICALL(MARSHAL_18, "PtrToStringUni(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni)
ICALL(MARSHAL_19, "PtrToStringUni(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni_len)
ICALL(MARSHAL_20, "PtrToStructure(intptr,System.Type)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure_type)
ICALL(MARSHAL_21, "PtrToStructure(intptr,object)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure)
#ifndef DISABLE_COM
ICALL(MARSHAL_22, "QueryInterfaceInternal", ves_icall_System_Runtime_InteropServices_Marshal_QueryInterfaceInternal)
#endif
ICALL(MARSHAL_43, "ReAllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocCoTaskMem)
ICALL(MARSHAL_23, "ReAllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_49, "ReleaseComObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseComObjectInternal)
ICALL(MARSHAL_29, "ReleaseInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseInternal)
#endif
ICALL(MARSHAL_30, "SizeOf", ves_icall_System_Runtime_InteropServices_Marshal_SizeOf)
ICALL(MARSHAL_31, "StringToBSTR", ves_icall_System_Runtime_InteropServices_Marshal_StringToBSTR)
ICALL(MARSHAL_32, "StringToHGlobalAnsi", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalAnsi)
ICALL(MARSHAL_33, "StringToHGlobalUni", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalUni)
ICALL(MARSHAL_34, "StructureToPtr", ves_icall_System_Runtime_InteropServices_Marshal_StructureToPtr)
ICALL(MARSHAL_35, "UnsafeAddrOfPinnedArrayElement", ves_icall_System_Runtime_InteropServices_Marshal_UnsafeAddrOfPinnedArrayElement)
ICALL(MARSHAL_41, "copy_from_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_from_unmanaged)
ICALL(MARSHAL_42, "copy_to_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_to_unmanaged)

ICALL_TYPE(ACTS, "System.Runtime.Remoting.Activation.ActivationServices", ACTS_1)
ICALL(ACTS_1, "AllocateUninitializedClassInstance", ves_icall_System_Runtime_Activation_ActivationServices_AllocateUninitializedClassInstance)
ICALL(ACTS_2, "EnableProxyActivation", ves_icall_System_Runtime_Activation_ActivationServices_EnableProxyActivation)

ICALL_TYPE(MONOMM, "System.Runtime.Remoting.Messaging.MonoMethodMessage", MONOMM_1)
ICALL(MONOMM_1, "InitMessage", ves_icall_MonoMethodMessage_InitMessage)

#ifndef DISABLE_REMOTING
ICALL_TYPE(REALP, "System.Runtime.Remoting.Proxies.RealProxy", REALP_1)
ICALL(REALP_1, "InternalGetProxyType", ves_icall_Remoting_RealProxy_InternalGetProxyType)
ICALL(REALP_2, "InternalGetTransparentProxy", ves_icall_Remoting_RealProxy_GetTransparentProxy)

ICALL_TYPE(REMSER, "System.Runtime.Remoting.RemotingServices", REMSER_0)
ICALL(REMSER_0, "GetVirtualMethod", ves_icall_Remoting_RemotingServices_GetVirtualMethod)
ICALL(REMSER_1, "InternalExecute", ves_icall_InternalExecute)
ICALL(REMSER_2, "IsTransparentProxy", ves_icall_IsTransparentProxy)
#endif

ICALL_TYPE(MHAN, "System.RuntimeMethodHandle", MHAN_1)
ICALL(MHAN_1, "GetFunctionPointer", ves_icall_RuntimeMethod_GetFunctionPointer)

ICALL_TYPE(RNG, "System.Security.Cryptography.RNGCryptoServiceProvider", RNG_1)
ICALL(RNG_1, "RngClose", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngClose)
ICALL(RNG_2, "RngGetBytes", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngGetBytes)
ICALL(RNG_3, "RngInitialize", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngInitialize)
ICALL(RNG_4, "RngOpen", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngOpen)

#ifndef DISABLE_POLICY_EVIDENCE
ICALL_TYPE(EVID, "System.Security.Policy.Evidence", EVID_1)
ICALL(EVID_1, "IsAuthenticodePresent", ves_icall_System_Security_Policy_Evidence_IsAuthenticodePresent)

ICALL_TYPE(WINID, "System.Security.Principal.WindowsIdentity", WINID_1)
ICALL(WINID_1, "GetCurrentToken", ves_icall_System_Security_Principal_WindowsIdentity_GetCurrentToken)
ICALL(WINID_2, "GetTokenName", ves_icall_System_Security_Principal_WindowsIdentity_GetTokenName)
ICALL(WINID_3, "GetUserToken", ves_icall_System_Security_Principal_WindowsIdentity_GetUserToken)
ICALL(WINID_4, "_GetRoles", ves_icall_System_Security_Principal_WindowsIdentity_GetRoles)

ICALL_TYPE(WINIMP, "System.Security.Principal.WindowsImpersonationContext", WINIMP_1)
ICALL(WINIMP_1, "CloseToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_CloseToken)
ICALL(WINIMP_2, "DuplicateToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_DuplicateToken)
ICALL(WINIMP_3, "RevertToSelf", ves_icall_System_Security_Principal_WindowsImpersonationContext_RevertToSelf)
ICALL(WINIMP_4, "SetCurrentToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_SetCurrentToken)

ICALL_TYPE(WINPRIN, "System.Security.Principal.WindowsPrincipal", WINPRIN_1)
ICALL(WINPRIN_1, "IsMemberOfGroupId", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupId)
ICALL(WINPRIN_2, "IsMemberOfGroupName", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupName)

ICALL_TYPE(SECSTRING, "System.Security.SecureString", SECSTRING_1)
ICALL(SECSTRING_1, "DecryptInternal", ves_icall_System_Security_SecureString_DecryptInternal)
ICALL(SECSTRING_2, "EncryptInternal", ves_icall_System_Security_SecureString_EncryptInternal)
#endif /* !DISABLE_POLICY_EVIDENCE */

ICALL_TYPE(SECMAN, "System.Security.SecurityManager", SECMAN_1)
ICALL(SECMAN_1, "GetLinkDemandSecurity", ves_icall_System_Security_SecurityManager_GetLinkDemandSecurity)
ICALL(SECMAN_2, "get_CheckExecutionRights", ves_icall_System_Security_SecurityManager_get_CheckExecutionRights)
ICALL(SECMAN_3, "get_RequiresElevatedPermissions", mono_security_core_clr_require_elevated_permissions)
ICALL(SECMAN_4, "get_SecurityEnabled", ves_icall_System_Security_SecurityManager_get_SecurityEnabled)
ICALL(SECMAN_5, "set_CheckExecutionRights", ves_icall_System_Security_SecurityManager_set_CheckExecutionRights)
ICALL(SECMAN_6, "set_SecurityEnabled", ves_icall_System_Security_SecurityManager_set_SecurityEnabled)

ICALL_TYPE(STRING, "System.String", STRING_1)
ICALL(STRING_1, ".ctor(char*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_2, ".ctor(char*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_3, ".ctor(char,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_4, ".ctor(char[])", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_5, ".ctor(char[],int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_6, ".ctor(sbyte*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_7, ".ctor(sbyte*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8, ".ctor(sbyte*,int,int,System.Text.Encoding)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8a, "GetLOSLimit", ves_icall_System_String_GetLOSLimit)
ICALL(STRING_9, "InternalAllocateStr", ves_icall_System_String_InternalAllocateStr)
ICALL(STRING_10, "InternalIntern", ves_icall_System_String_InternalIntern)
ICALL(STRING_11, "InternalIsInterned", ves_icall_System_String_InternalIsInterned)

ICALL_TYPE(TENC, "System.Text.Encoding", TENC_1)
ICALL(TENC_1, "InternalCodePage", ves_icall_System_Text_Encoding_InternalCodePage)

ICALL_TYPE(ILOCK, "System.Threading.Interlocked", ILOCK_1)
ICALL(ILOCK_1, "Add(int&,int)", ves_icall_System_Threading_Interlocked_Add_Int)
ICALL(ILOCK_2, "Add(long&,long)", ves_icall_System_Threading_Interlocked_Add_Long)
ICALL(ILOCK_3, "CompareExchange(T&,T,T)", ves_icall_System_Threading_Interlocked_CompareExchange_T)
ICALL(ILOCK_4, "CompareExchange(double&,double,double)", ves_icall_System_Threading_Interlocked_CompareExchange_Double)
ICALL(ILOCK_5, "CompareExchange(int&,int,int)", ves_icall_System_Threading_Interlocked_CompareExchange_Int)
ICALL(ILOCK_6, "CompareExchange(intptr&,intptr,intptr)", ves_icall_System_Threading_Interlocked_CompareExchange_IntPtr)
ICALL(ILOCK_7, "CompareExchange(long&,long,long)", ves_icall_System_Threading_Interlocked_CompareExchange_Long)
ICALL(ILOCK_8, "CompareExchange(object&,object,object)", ves_icall_System_Threading_Interlocked_CompareExchange_Object)
ICALL(ILOCK_9, "CompareExchange(single&,single,single)", ves_icall_System_Threading_Interlocked_CompareExchange_Single)
ICALL(ILOCK_10, "Decrement(int&)", ves_icall_System_Threading_Interlocked_Decrement_Int)
ICALL(ILOCK_11, "Decrement(long&)", ves_icall_System_Threading_Interlocked_Decrement_Long)
ICALL(ILOCK_12, "Exchange(T&,T)", ves_icall_System_Threading_Interlocked_Exchange_T)
ICALL(ILOCK_13, "Exchange(double&,double)", ves_icall_System_Threading_Interlocked_Exchange_Double)
ICALL(ILOCK_14, "Exchange(int&,int)", ves_icall_System_Threading_Interlocked_Exchange_Int)
ICALL(ILOCK_15, "Exchange(intptr&,intptr)", ves_icall_System_Threading_Interlocked_Exchange_IntPtr)
ICALL(ILOCK_16, "Exchange(long&,long)", ves_icall_System_Threading_Interlocked_Exchange_Long)
ICALL(ILOCK_17, "Exchange(object&,object)", ves_icall_System_Threading_Interlocked_Exchange_Object)
ICALL(ILOCK_18, "Exchange(single&,single)", ves_icall_System_Threading_Interlocked_Exchange_Single)
ICALL(ILOCK_19, "Increment(int&)", ves_icall_System_Threading_Interlocked_Increment_Int)
ICALL(ILOCK_20, "Increment(long&)", ves_icall_System_Threading_Interlocked_Increment_Long)
ICALL(ILOCK_21, "Read(long&)", ves_icall_System_Threading_Interlocked_Read_Long)

ICALL_TYPE(ITHREAD, "System.Threading.InternalThread", ITHREAD_1)
ICALL(ITHREAD_1, "Thread_free_internal", ves_icall_System_Threading_InternalThread_Thread_free_internal)

ICALL_TYPE(MONIT, "System.Threading.Monitor", MONIT_8)
ICALL(MONIT_8, "Enter", mono_monitor_enter)
ICALL(MONIT_1, "Exit", mono_monitor_exit)
ICALL(MONIT_2, "Monitor_pulse", ves_icall_System_Threading_Monitor_Monitor_pulse)
ICALL(MONIT_3, "Monitor_pulse_all", ves_icall_System_Threading_Monitor_Monitor_pulse_all)
ICALL(MONIT_4, "Monitor_test_owner", ves_icall_System_Threading_Monitor_Monitor_test_owner)
ICALL(MONIT_5, "Monitor_test_synchronised", ves_icall_System_Threading_Monitor_Monitor_test_synchronised)
ICALL(MONIT_6, "Monitor_try_enter", ves_icall_System_Threading_Monitor_Monitor_try_enter)
ICALL(MONIT_7, "Monitor_wait", ves_icall_System_Threading_Monitor_Monitor_wait)
ICALL(MONIT_9, "try_enter_with_atomic_var", ves_icall_System_Threading_Monitor_Monitor_try_enter_with_atomic_var)

ICALL_TYPE(MUTEX, "System.Threading.Mutex", MUTEX_1)
ICALL(MUTEX_1, "CreateMutex_internal(bool,string,bool&)", ves_icall_System_Threading_Mutex_CreateMutex_internal)
ICALL(MUTEX_2, "OpenMutex_internal(string,System.Security.AccessControl.MutexRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Mutex_OpenMutex_internal)
ICALL(MUTEX_3, "ReleaseMutex_internal(intptr)", ves_icall_System_Threading_Mutex_ReleaseMutex_internal)

ICALL_TYPE(NATIVEC, "System.Threading.NativeEventCalls", NATIVEC_1)
ICALL(NATIVEC_1, "CloseEvent_internal", ves_icall_System_Threading_Events_CloseEvent_internal)
ICALL(NATIVEC_2, "CreateEvent_internal(bool,bool,string,bool&)", ves_icall_System_Threading_Events_CreateEvent_internal)
ICALL(NATIVEC_3, "OpenEvent_internal(string,System.Security.AccessControl.EventWaitHandleRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Events_OpenEvent_internal)
ICALL(NATIVEC_4, "ResetEvent_internal",  ves_icall_System_Threading_Events_ResetEvent_internal)
ICALL(NATIVEC_5, "SetEvent_internal",    ves_icall_System_Threading_Events_SetEvent_internal)

ICALL_TYPE(SEMA, "System.Threading.Semaphore", SEMA_1)
ICALL(SEMA_1, "CreateSemaphore_internal(int,int,string,bool&)", ves_icall_System_Threading_Semaphore_CreateSemaphore_internal)
ICALL(SEMA_2, "OpenSemaphore_internal(string,System.Security.AccessControl.SemaphoreRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Semaphore_OpenSemaphore_internal)
ICALL(SEMA_3, "ReleaseSemaphore_internal(intptr,int,bool&)", ves_icall_System_Threading_Semaphore_ReleaseSemaphore_internal)

ICALL_TYPE(THREAD, "System.Threading.Thread", THREAD_1)
ICALL(THREAD_1, "Abort_internal(System.Threading.InternalThread,object)", ves_icall_System_Threading_Thread_Abort)
ICALL(THREAD_1aa, "AllocTlsData", mono_thread_alloc_tls)
ICALL(THREAD_1a, "ByteArrayToCurrentDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToCurrentDomain)
ICALL(THREAD_1b, "ByteArrayToRootDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToRootDomain)
ICALL(THREAD_2, "ClrState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_ClrState)
ICALL(THREAD_2a, "ConstructInternalThread", ves_icall_System_Threading_Thread_ConstructInternalThread)
ICALL(THREAD_3, "CurrentInternalThread_internal", mono_thread_internal_current)
ICALL(THREAD_3a, "DestroyTlsData", mono_thread_destroy_tls)
ICALL(THREAD_4, "FreeLocalSlotValues", mono_thread_free_local_slot_values)
ICALL(THREAD_55, "GetAbortExceptionState", ves_icall_System_Threading_Thread_GetAbortExceptionState)
ICALL(THREAD_7, "GetDomainID", ves_icall_System_Threading_Thread_GetDomainID)
ICALL(THREAD_8, "GetName_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetName_internal)
ICALL(THREAD_11, "GetState(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetState)
ICALL(THREAD_53, "Interrupt_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Interrupt_internal)
ICALL(THREAD_12, "Join_internal(System.Threading.InternalThread,int,intptr)", ves_icall_System_Threading_Thread_Join_internal)
ICALL(THREAD_13, "MemoryBarrier", ves_icall_System_Threading_Thread_MemoryBarrier)
ICALL(THREAD_14, "ResetAbort_internal()", ves_icall_System_Threading_Thread_ResetAbort)
ICALL(THREAD_15, "Resume_internal()", ves_icall_System_Threading_Thread_Resume)
ICALL(THREAD_18, "SetName_internal(System.Threading.InternalThread,string)", ves_icall_System_Threading_Thread_SetName_internal)
ICALL(THREAD_21, "SetState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_SetState)
ICALL(THREAD_22, "Sleep_internal", ves_icall_System_Threading_Thread_Sleep_internal)
ICALL(THREAD_54, "SpinWait_nop", ves_icall_System_Threading_Thread_SpinWait_nop)
ICALL(THREAD_23, "Suspend_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Suspend)
ICALL(THREAD_25, "Thread_internal", ves_icall_System_Threading_Thread_Thread_internal)
ICALL(THREAD_26, "VolatileRead(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_27, "VolatileRead(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(THREAD_28, "VolatileRead(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_29, "VolatileRead(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_30, "VolatileRead(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_31, "VolatileRead(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_32, "VolatileRead(object&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_33, "VolatileRead(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_34, "VolatileRead(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(THREAD_35, "VolatileRead(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_36, "VolatileRead(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_37, "VolatileRead(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_38, "VolatileRead(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_39, "VolatileWrite(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_40, "VolatileWrite(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(THREAD_41, "VolatileWrite(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_42, "VolatileWrite(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_43, "VolatileWrite(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_44, "VolatileWrite(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_45, "VolatileWrite(object&,object)", ves_icall_System_Threading_Thread_VolatileWriteObject)
ICALL(THREAD_46, "VolatileWrite(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_47, "VolatileWrite(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(THREAD_48, "VolatileWrite(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_49, "VolatileWrite(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_50, "VolatileWrite(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_51, "VolatileWrite(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_9, "Yield", ves_icall_System_Threading_Thread_Yield)
ICALL(THREAD_52, "current_lcid()", ves_icall_System_Threading_Thread_current_lcid)

ICALL_TYPE(THREADP, "System.Threading.ThreadPool", THREADP_1)
ICALL(THREADP_1, "GetAvailableThreads", ves_icall_System_Threading_ThreadPool_GetAvailableThreads)
ICALL(THREADP_2, "GetMaxThreads", ves_icall_System_Threading_ThreadPool_GetMaxThreads)
ICALL(THREADP_3, "GetMinThreads", ves_icall_System_Threading_ThreadPool_GetMinThreads)
ICALL(THREADP_35, "SetMaxThreads", ves_icall_System_Threading_ThreadPool_SetMaxThreads)
ICALL(THREADP_4, "SetMinThreads", ves_icall_System_Threading_ThreadPool_SetMinThreads)
ICALL(THREADP_5, "pool_queue", icall_append_job)

ICALL_TYPE(VOLATILE, "System.Threading.Volatile", VOLATILE_28)
ICALL(VOLATILE_28, "Read(T&)", ves_icall_System_Threading_Volatile_Read_T)
ICALL(VOLATILE_1, "Read(bool&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_2, "Read(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_3, "Read(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(VOLATILE_4, "Read(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_5, "Read(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_6, "Read(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_7, "Read(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_8, "Read(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_9, "Read(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(VOLATILE_10, "Read(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_11, "Read(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_12, "Read(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_13, "Read(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_27, "Write(T&,T)", ves_icall_System_Threading_Volatile_Write_T)
ICALL(VOLATILE_14, "Write(bool&,bool)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_15, "Write(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_16, "Write(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(VOLATILE_17, "Write(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_18, "Write(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_19, "Write(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_20, "Write(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(VOLATILE_21, "Write(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_22, "Write(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(VOLATILE_23, "Write(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_24, "Write(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_25, "Write(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_26, "Write(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)

ICALL_TYPE(WAITH, "System.Threading.WaitHandle", WAITH_1)
ICALL(WAITH_1, "SignalAndWait_Internal", ves_icall_System_Threading_WaitHandle_SignalAndWait_Internal)
ICALL(WAITH_2, "WaitAll_internal", ves_icall_System_Threading_WaitHandle_WaitAll_internal)
ICALL(WAITH_3, "WaitAny_internal", ves_icall_System_Threading_WaitHandle_WaitAny_internal)
ICALL(WAITH_4, "WaitOne_internal", ves_icall_System_Threading_WaitHandle_WaitOne_internal)

ICALL_TYPE(TYPE, "System.Type", TYPE_1)
ICALL(TYPE_1, "EqualsInternal", ves_icall_System_Type_EqualsInternal)
ICALL(TYPE_2, "GetGenericParameterAttributes", ves_icall_Type_GetGenericParameterAttributes)
ICALL(TYPE_3, "GetGenericParameterConstraints_impl", ves_icall_Type_GetGenericParameterConstraints)
ICALL(TYPE_4, "GetGenericParameterPosition", ves_icall_Type_GetGenericParameterPosition)
ICALL(TYPE_5, "GetGenericTypeDefinition_impl", ves_icall_Type_GetGenericTypeDefinition_impl)
ICALL(TYPE_6, "GetInterfaceMapData", ves_icall_Type_GetInterfaceMapData)
ICALL(TYPE_7, "GetPacking", ves_icall_Type_GetPacking)
ICALL(TYPE_8, "GetTypeCode", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_9, "GetTypeCodeInternal", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_10, "IsArrayImpl", ves_icall_Type_IsArrayImpl)
ICALL(TYPE_11, "IsInstanceOfType", ves_icall_type_IsInstanceOfType)
ICALL(TYPE_12, "MakeGenericType", ves_icall_Type_MakeGenericType)
ICALL(TYPE_13, "MakePointerType", ves_icall_Type_MakePointerType)
ICALL(TYPE_14, "get_IsGenericInstance", ves_icall_Type_get_IsGenericInstance)
ICALL(TYPE_15, "get_IsGenericType", ves_icall_Type_get_IsGenericType)
ICALL(TYPE_16, "get_IsGenericTypeDefinition", ves_icall_Type_get_IsGenericTypeDefinition)
ICALL(TYPE_17, "internal_from_handle", ves_icall_type_from_handle)
ICALL(TYPE_18, "internal_from_name", ves_icall_type_from_name)
ICALL(TYPE_19, "make_array_type", ves_icall_Type_make_array_type)
ICALL(TYPE_20, "make_byref_type", ves_icall_Type_make_byref_type)
ICALL(TYPE_21, "type_is_assignable_from", ves_icall_type_is_assignable_from)
ICALL(TYPE_22, "type_is_subtype_of", ves_icall_type_is_subtype_of)

ICALL_TYPE(TYPEDR, "System.TypedReference", TYPEDR_1)
ICALL(TYPEDR_1, "ToObject",	mono_TypedReference_ToObject)
ICALL(TYPEDR_2, "ToObjectInternal",	mono_TypedReference_ToObjectInternal)

ICALL_TYPE(VALUET, "System.ValueType", VALUET_1)
ICALL(VALUET_1, "InternalEquals", ves_icall_System_ValueType_Equals)
ICALL(VALUET_2, "InternalGetHashCode", ves_icall_System_ValueType_InternalGetHashCode)

ICALL_TYPE(WEBIC, "System.Web.Util.ICalls", WEBIC_1)
ICALL(WEBIC_1, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(WEBIC_2, "GetMachineInstallDirectory", ves_icall_System_Web_Util_ICalls_get_machine_install_dir)
ICALL(WEBIC_3, "GetUnmanagedResourcesPtr", ves_icall_get_resources_ptr)

#ifndef DISABLE_COM
ICALL_TYPE(COMOBJ, "System.__ComObject", COMOBJ_1)
ICALL(COMOBJ_1, "CreateRCW", ves_icall_System_ComObject_CreateRCW)
ICALL(COMOBJ_2, "GetInterfaceInternal", ves_icall_System_ComObject_GetInterfaceInternal)
ICALL(COMOBJ_3, "ReleaseInterfaces", ves_icall_System_ComObject_ReleaseInterfaces)
#endif

	Icall_last
};

#undef ICALL_TYPE
#undef ICALL
#define ICALL_TYPE(id,name,first) Icall_type_ ## id,
#define ICALL(id,name,func)
enum {
// #include "metadata/icall-def.h"
ICALL_TYPE(UNORM, "Mono.Globalization.Unicode.Normalization", UNORM_1)
ICALL(UNORM_1, "load_normalization_resource", load_normalization_resource)

#ifndef DISABLE_COM
ICALL_TYPE(COMPROX, "Mono.Interop.ComInteropProxy", COMPROX_1)
ICALL(COMPROX_1, "AddProxy", ves_icall_Mono_Interop_ComInteropProxy_AddProxy)
ICALL(COMPROX_2, "FindProxy", ves_icall_Mono_Interop_ComInteropProxy_FindProxy)
#endif

ICALL_TYPE(RUNTIME, "Mono.Runtime", RUNTIME_1)
ICALL(RUNTIME_1, "GetDisplayName", ves_icall_Mono_Runtime_GetDisplayName)
ICALL(RUNTIME_12, "GetNativeStackTrace", ves_icall_Mono_Runtime_GetNativeStackTrace)
ICALL(RUNTIME_13, "SetGCAllowSynchronousMajor", ves_icall_Mono_Runtime_SetGCAllowSynchronousMajor)

#ifndef PLATFORM_RO_FS
ICALL_TYPE(KPAIR, "Mono.Security.Cryptography.KeyPairPersistence", KPAIR_1)
ICALL(KPAIR_1, "_CanSecure", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_CanSecure)
ICALL(KPAIR_2, "_IsMachineProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsMachineProtected)
ICALL(KPAIR_3, "_IsUserProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsUserProtected)
ICALL(KPAIR_4, "_ProtectMachine", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectMachine)
ICALL(KPAIR_5, "_ProtectUser", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectUser)
#endif /* !PLATFORM_RO_FS */

ICALL_TYPE(ACTIV, "System.Activator", ACTIV_1)
ICALL(ACTIV_1, "CreateInstanceInternal", ves_icall_System_Activator_CreateInstanceInternal)

ICALL_TYPE(APPDOM, "System.AppDomain", APPDOM_1)
ICALL(APPDOM_1, "ExecuteAssembly", ves_icall_System_AppDomain_ExecuteAssembly)
ICALL(APPDOM_2, "GetAssemblies", ves_icall_System_AppDomain_GetAssemblies)
ICALL(APPDOM_3, "GetData", ves_icall_System_AppDomain_GetData)
ICALL(APPDOM_4, "InternalGetContext", ves_icall_System_AppDomain_InternalGetContext)
ICALL(APPDOM_5, "InternalGetDefaultContext", ves_icall_System_AppDomain_InternalGetDefaultContext)
ICALL(APPDOM_6, "InternalGetProcessGuid", ves_icall_System_AppDomain_InternalGetProcessGuid)
ICALL(APPDOM_7, "InternalIsFinalizingForUnload", ves_icall_System_AppDomain_InternalIsFinalizingForUnload)
ICALL(APPDOM_8, "InternalPopDomainRef", ves_icall_System_AppDomain_InternalPopDomainRef)
ICALL(APPDOM_9, "InternalPushDomainRef", ves_icall_System_AppDomain_InternalPushDomainRef)
ICALL(APPDOM_10, "InternalPushDomainRefByID", ves_icall_System_AppDomain_InternalPushDomainRefByID)
ICALL(APPDOM_11, "InternalSetContext", ves_icall_System_AppDomain_InternalSetContext)
ICALL(APPDOM_12, "InternalSetDomain", ves_icall_System_AppDomain_InternalSetDomain)
ICALL(APPDOM_13, "InternalSetDomainByID", ves_icall_System_AppDomain_InternalSetDomainByID)
ICALL(APPDOM_14, "InternalUnload", ves_icall_System_AppDomain_InternalUnload)
ICALL(APPDOM_15, "LoadAssembly", ves_icall_System_AppDomain_LoadAssembly)
ICALL(APPDOM_16, "LoadAssemblyRaw", ves_icall_System_AppDomain_LoadAssemblyRaw)
ICALL(APPDOM_17, "SetData", ves_icall_System_AppDomain_SetData)
ICALL(APPDOM_18, "createDomain", ves_icall_System_AppDomain_createDomain)
ICALL(APPDOM_19, "getCurDomain", ves_icall_System_AppDomain_getCurDomain)
ICALL(APPDOM_20, "getFriendlyName", ves_icall_System_AppDomain_getFriendlyName)
ICALL(APPDOM_21, "getRootDomain", ves_icall_System_AppDomain_getRootDomain)
ICALL(APPDOM_22, "getSetup", ves_icall_System_AppDomain_getSetup)

ICALL_TYPE(ARGI, "System.ArgIterator", ARGI_1)
ICALL(ARGI_1, "IntGetNextArg()",                  mono_ArgIterator_IntGetNextArg)
ICALL(ARGI_2, "IntGetNextArg(intptr)", mono_ArgIterator_IntGetNextArgT)
ICALL(ARGI_3, "IntGetNextArgType",                mono_ArgIterator_IntGetNextArgType)
ICALL(ARGI_4, "Setup",                            mono_ArgIterator_Setup)

ICALL_TYPE(ARRAY, "System.Array", ARRAY_1)
ICALL(ARRAY_1, "ClearInternal",    ves_icall_System_Array_ClearInternal)
ICALL(ARRAY_2, "Clone",            mono_array_clone)
ICALL(ARRAY_3, "CreateInstanceImpl",   ves_icall_System_Array_CreateInstanceImpl)
ICALL(ARRAY_14, "CreateInstanceImpl64",   ves_icall_System_Array_CreateInstanceImpl64)
ICALL(ARRAY_4, "FastCopy",         ves_icall_System_Array_FastCopy)
ICALL(ARRAY_5, "GetGenericValueImpl", ves_icall_System_Array_GetGenericValueImpl)
ICALL(ARRAY_6, "GetLength",        ves_icall_System_Array_GetLength)
ICALL(ARRAY_15, "GetLongLength",        ves_icall_System_Array_GetLongLength)
ICALL(ARRAY_7, "GetLowerBound",    ves_icall_System_Array_GetLowerBound)
ICALL(ARRAY_8, "GetRank",          ves_icall_System_Array_GetRank)
ICALL(ARRAY_9, "GetValue",         ves_icall_System_Array_GetValue)
ICALL(ARRAY_10, "GetValueImpl",     ves_icall_System_Array_GetValueImpl)
ICALL(ARRAY_11, "SetGenericValueImpl", ves_icall_System_Array_SetGenericValueImpl)
ICALL(ARRAY_12, "SetValue",         ves_icall_System_Array_SetValue)
ICALL(ARRAY_13, "SetValueImpl",     ves_icall_System_Array_SetValueImpl)

ICALL_TYPE(BUFFER, "System.Buffer", BUFFER_1)
ICALL(BUFFER_1, "BlockCopyInternal", ves_icall_System_Buffer_BlockCopyInternal)
ICALL(BUFFER_2, "ByteLengthInternal", ves_icall_System_Buffer_ByteLengthInternal)
ICALL(BUFFER_3, "GetByteInternal", ves_icall_System_Buffer_GetByteInternal)
ICALL(BUFFER_4, "SetByteInternal", ves_icall_System_Buffer_SetByteInternal)

ICALL_TYPE(CHAR, "System.Char", CHAR_1)
ICALL(CHAR_1, "GetDataTablePointers", ves_icall_System_Char_GetDataTablePointers)

ICALL_TYPE (COMPO_W, "System.ComponentModel.Win32Exception", COMPO_W_1)
ICALL (COMPO_W_1, "W32ErrorMessage", ves_icall_System_ComponentModel_Win32Exception_W32ErrorMessage)

ICALL_TYPE(DEFAULTC, "System.Configuration.DefaultConfig", DEFAULTC_1)
ICALL(DEFAULTC_1, "get_bundled_machine_config", get_bundled_machine_config)
ICALL(DEFAULTC_2, "get_machine_config_path", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)

/* Note that the below icall shares the same function as DefaultConfig uses */
ICALL_TYPE(INTCFGHOST, "System.Configuration.InternalConfigurationHost", INTCFGHOST_1)
ICALL(INTCFGHOST_1, "get_bundled_app_config", get_bundled_app_config)
ICALL(INTCFGHOST_2, "get_bundled_machine_config", get_bundled_machine_config)

ICALL_TYPE(CONSOLE, "System.ConsoleDriver", CONSOLE_1)
ICALL(CONSOLE_1, "InternalKeyAvailable", ves_icall_System_ConsoleDriver_InternalKeyAvailable )
ICALL(CONSOLE_2, "Isatty", ves_icall_System_ConsoleDriver_Isatty )
ICALL(CONSOLE_3, "SetBreak", ves_icall_System_ConsoleDriver_SetBreak )
ICALL(CONSOLE_4, "SetEcho", ves_icall_System_ConsoleDriver_SetEcho )
ICALL(CONSOLE_5, "TtySetup", ves_icall_System_ConsoleDriver_TtySetup )

ICALL_TYPE(CONVERT, "System.Convert", CONVERT_1)
ICALL(CONVERT_1, "InternalFromBase64CharArray", InternalFromBase64CharArray )
ICALL(CONVERT_2, "InternalFromBase64String", InternalFromBase64String )

ICALL_TYPE(TZONE, "System.CurrentSystemTimeZone", TZONE_1)
ICALL(TZONE_1, "GetTimeZoneData", ves_icall_System_CurrentSystemTimeZone_GetTimeZoneData)

ICALL_TYPE(DTIME, "System.DateTime", DTIME_1)
ICALL(DTIME_1, "GetNow", mono_100ns_datetime)
ICALL(DTIME_2, "GetTimeMonotonic", mono_100ns_ticks)

#ifndef DISABLE_DECIMAL
ICALL_TYPE(DECIMAL, "System.Decimal", DECIMAL_1)
ICALL(DECIMAL_1, "decimal2Int64", mono_decimal2Int64)
ICALL(DECIMAL_2, "decimal2UInt64", mono_decimal2UInt64)
ICALL(DECIMAL_3, "decimal2double", mono_decimal2double)
//ICALL(DECIMAL_4, "decimal2string", mono_decimal2string)
ICALL(DECIMAL_5, "decimalCompare", mono_decimalCompare)
ICALL(DECIMAL_6, "decimalDiv", mono_decimalDiv)
ICALL(DECIMAL_7, "decimalFloorAndTrunc", mono_decimalFloorAndTrunc)
ICALL(DECIMAL_8, "decimalIncr", mono_decimalIncr)
ICALL(DECIMAL_9, "decimalIntDiv", mono_decimalIntDiv)
ICALL(DECIMAL_10, "decimalMult", mono_decimalMult)
ICALL(DECIMAL_11, "decimalRound", mono_decimalRound)
ICALL(DECIMAL_12, "decimalSetExponent", mono_decimalSetExponent)
ICALL(DECIMAL_13, "double2decimal", mono_double2decimal) /* FIXME: wrong signature. */
ICALL(DECIMAL_14, "string2decimal", mono_string2decimal)
#endif

ICALL_TYPE(DELEGATE, "System.Delegate", DELEGATE_1)
ICALL(DELEGATE_1, "CreateDelegate_internal", ves_icall_System_Delegate_CreateDelegate_internal)
ICALL(DELEGATE_2, "SetMulticastInvoke", ves_icall_System_Delegate_SetMulticastInvoke)

ICALL_TYPE(DEBUGR, "System.Diagnostics.Debugger", DEBUGR_1)
ICALL(DEBUGR_1, "IsAttached_internal", ves_icall_System_Diagnostics_Debugger_IsAttached_internal)
ICALL(DEBUGR_2, "IsLogging", ves_icall_System_Diagnostics_Debugger_IsLogging)
ICALL(DEBUGR_3, "Log", ves_icall_System_Diagnostics_Debugger_Log)

ICALL_TYPE(TRACEL, "System.Diagnostics.DefaultTraceListener", TRACEL_1)
ICALL(TRACEL_1, "WriteWindowsDebugString", ves_icall_System_Diagnostics_DefaultTraceListener_WriteWindowsDebugString)

ICALL_TYPE(FILEV, "System.Diagnostics.FileVersionInfo", FILEV_1)
ICALL(FILEV_1, "GetVersionInfo_internal(string)", ves_icall_System_Diagnostics_FileVersionInfo_GetVersionInfo_internal)

#ifndef DISABLE_PROCESS_HANDLING
ICALL_TYPE(PERFCTR, "System.Diagnostics.PerformanceCounter", PERFCTR_1)
ICALL(PERFCTR_1, "FreeData", mono_perfcounter_free_data)
ICALL(PERFCTR_2, "GetImpl", mono_perfcounter_get_impl)
ICALL(PERFCTR_3, "GetSample", mono_perfcounter_get_sample)
ICALL(PERFCTR_4, "UpdateValue", mono_perfcounter_update_value)

ICALL_TYPE(PERFCTRCAT, "System.Diagnostics.PerformanceCounterCategory", PERFCTRCAT_1)
ICALL(PERFCTRCAT_1, "CategoryDelete", mono_perfcounter_category_del)
ICALL(PERFCTRCAT_2, "CategoryHelpInternal",   mono_perfcounter_category_help)
ICALL(PERFCTRCAT_3, "CounterCategoryExists", mono_perfcounter_category_exists)
ICALL(PERFCTRCAT_4, "Create",         mono_perfcounter_create)
ICALL(PERFCTRCAT_5, "GetCategoryNames", mono_perfcounter_category_names)
ICALL(PERFCTRCAT_6, "GetCounterNames", mono_perfcounter_counter_names)
ICALL(PERFCTRCAT_7, "GetInstanceNames", mono_perfcounter_instance_names)
ICALL(PERFCTRCAT_8, "InstanceExistsInternal", mono_perfcounter_instance_exists)

ICALL_TYPE(PROCESS, "System.Diagnostics.Process", PROCESS_1)
ICALL(PROCESS_1, "CreateProcess_internal(System.Diagnostics.ProcessStartInfo,intptr,intptr,intptr,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_CreateProcess_internal)
ICALL(PROCESS_2, "ExitCode_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitCode_internal)
ICALL(PROCESS_3, "ExitTime_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitTime_internal)
ICALL(PROCESS_4, "GetModules_internal(intptr)", ves_icall_System_Diagnostics_Process_GetModules_internal)
ICALL(PROCESS_5, "GetPid_internal()", ves_icall_System_Diagnostics_Process_GetPid_internal)
ICALL(PROCESS_5B, "GetPriorityClass(intptr,int&)", ves_icall_System_Diagnostics_Process_GetPriorityClass)
ICALL(PROCESS_5H, "GetProcessData", ves_icall_System_Diagnostics_Process_GetProcessData)
ICALL(PROCESS_6, "GetProcess_internal(int)", ves_icall_System_Diagnostics_Process_GetProcess_internal)
ICALL(PROCESS_7, "GetProcesses_internal()", ves_icall_System_Diagnostics_Process_GetProcesses_internal)
ICALL(PROCESS_8, "GetWorkingSet_internal(intptr,int&,int&)", ves_icall_System_Diagnostics_Process_GetWorkingSet_internal)
ICALL(PROCESS_9, "Kill_internal", ves_icall_System_Diagnostics_Process_Kill_internal)
ICALL(PROCESS_10, "ProcessName_internal(intptr)", ves_icall_System_Diagnostics_Process_ProcessName_internal)
ICALL(PROCESS_11, "Process_free_internal(intptr)", ves_icall_System_Diagnostics_Process_Process_free_internal)
ICALL(PROCESS_11B, "SetPriorityClass(intptr,int,int&)", ves_icall_System_Diagnostics_Process_SetPriorityClass)
ICALL(PROCESS_12, "SetWorkingSet_internal(intptr,int,int,bool)", ves_icall_System_Diagnostics_Process_SetWorkingSet_internal)
ICALL(PROCESS_13, "ShellExecuteEx_internal(System.Diagnostics.ProcessStartInfo,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_ShellExecuteEx_internal)
ICALL(PROCESS_14, "StartTime_internal(intptr)", ves_icall_System_Diagnostics_Process_StartTime_internal)
ICALL(PROCESS_14M, "Times", ves_icall_System_Diagnostics_Process_Times)
ICALL(PROCESS_15, "WaitForExit_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForExit_internal)
ICALL(PROCESS_16, "WaitForInputIdle_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForInputIdle_internal)

ICALL_TYPE (PROCESSHANDLE, "System.Diagnostics.Process/ProcessWaitHandle", PROCESSHANDLE_1)
ICALL (PROCESSHANDLE_1, "ProcessHandle_close(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_close)
ICALL (PROCESSHANDLE_2, "ProcessHandle_duplicate(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_duplicate)
#endif /* !DISABLE_PROCESS_HANDLING */

ICALL_TYPE(STOPWATCH, "System.Diagnostics.Stopwatch", STOPWATCH_1)
ICALL(STOPWATCH_1, "GetTimestamp", mono_100ns_ticks)

ICALL_TYPE(DOUBLE, "System.Double", DOUBLE_1)
ICALL(DOUBLE_1, "ParseImpl",    mono_double_ParseImpl)

ICALL_TYPE(ENUM, "System.Enum", ENUM_1)
ICALL(ENUM_1, "ToObject", ves_icall_System_Enum_ToObject)
ICALL(ENUM_5, "compare_value_to", ves_icall_System_Enum_compare_value_to)
ICALL(ENUM_4, "get_hashcode", ves_icall_System_Enum_get_hashcode)
ICALL(ENUM_3, "get_underlying_type", ves_icall_System_Enum_get_underlying_type)
ICALL(ENUM_2, "get_value", ves_icall_System_Enum_get_value)

ICALL_TYPE(ENV, "System.Environment", ENV_1)
ICALL(ENV_1, "Exit", ves_icall_System_Environment_Exit)
ICALL(ENV_2, "GetCommandLineArgs", mono_runtime_get_main_args)
ICALL(ENV_3, "GetEnvironmentVariableNames", ves_icall_System_Environment_GetEnvironmentVariableNames)
ICALL(ENV_4, "GetLogicalDrivesInternal", ves_icall_System_Environment_GetLogicalDrives )
ICALL(ENV_5, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(ENV_51, "GetNewLine", ves_icall_System_Environment_get_NewLine)
ICALL(ENV_6, "GetOSVersionString", ves_icall_System_Environment_GetOSVersionString)
ICALL(ENV_6a, "GetPageSize", mono_pagesize)
ICALL(ENV_7, "GetWindowsFolderPath", ves_icall_System_Environment_GetWindowsFolderPath)
ICALL(ENV_8, "InternalSetEnvironmentVariable", ves_icall_System_Environment_InternalSetEnvironmentVariable)
ICALL(ENV_9, "get_ExitCode", mono_environment_exitcode_get)
ICALL(ENV_10, "get_HasShutdownStarted", ves_icall_System_Environment_get_HasShutdownStarted)
ICALL(ENV_11, "get_MachineName", ves_icall_System_Environment_get_MachineName)
ICALL(ENV_13, "get_Platform", ves_icall_System_Environment_get_Platform)
ICALL(ENV_14, "get_ProcessorCount", mono_cpu_count)
ICALL(ENV_15, "get_TickCount", mono_msec_ticks)
ICALL(ENV_16, "get_UserName", ves_icall_System_Environment_get_UserName)
ICALL(ENV_16m, "internalBroadcastSettingChange", ves_icall_System_Environment_BroadcastSettingChange)
ICALL(ENV_17, "internalGetEnvironmentVariable", ves_icall_System_Environment_GetEnvironmentVariable)
ICALL(ENV_18, "internalGetGacPath", ves_icall_System_Environment_GetGacPath)
ICALL(ENV_19, "internalGetHome", ves_icall_System_Environment_InternalGetHome)
ICALL(ENV_20, "set_ExitCode", mono_environment_exitcode_set)

ICALL_TYPE(GC, "System.GC", GC_0)
ICALL(GC_0, "CollectionCount", mono_gc_collection_count)
ICALL(GC_0a, "GetGeneration", mono_gc_get_generation)
ICALL(GC_1, "GetTotalMemory", ves_icall_System_GC_GetTotalMemory)
ICALL(GC_2, "InternalCollect", ves_icall_System_GC_InternalCollect)
ICALL(GC_3, "KeepAlive", ves_icall_System_GC_KeepAlive)
ICALL(GC_4, "ReRegisterForFinalize", ves_icall_System_GC_ReRegisterForFinalize)
ICALL(GC_4a, "RecordPressure", mono_gc_add_memory_pressure)
ICALL(GC_5, "SuppressFinalize", ves_icall_System_GC_SuppressFinalize)
ICALL(GC_6, "WaitForPendingFinalizers", ves_icall_System_GC_WaitForPendingFinalizers)
ICALL(GC_7, "get_MaxGeneration", mono_gc_max_generation)
ICALL(GC_9, "get_ephemeron_tombstone", ves_icall_System_GC_get_ephemeron_tombstone)
ICALL(GC_8, "register_ephemeron_array", ves_icall_System_GC_register_ephemeron_array)

ICALL_TYPE(COMPINF, "System.Globalization.CompareInfo", COMPINF_1)
ICALL(COMPINF_1, "assign_sortkey(object,string,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_assign_sortkey)
ICALL(COMPINF_2, "construct_compareinfo(string)", ves_icall_System_Globalization_CompareInfo_construct_compareinfo)
ICALL(COMPINF_3, "free_internal_collator()", ves_icall_System_Globalization_CompareInfo_free_internal_collator)
ICALL(COMPINF_4, "internal_compare(string,int,int,string,int,int,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_internal_compare)
ICALL(COMPINF_5, "internal_index(string,int,int,char,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index_char)
ICALL(COMPINF_6, "internal_index(string,int,int,string,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index)

ICALL_TYPE(CULINF, "System.Globalization.CultureInfo", CULINF_2)
ICALL(CULINF_2, "construct_datetime_format", ves_icall_System_Globalization_CultureInfo_construct_datetime_format)
ICALL(CULINF_4, "construct_internal_locale_from_current_locale", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_current_locale)
ICALL(CULINF_5, "construct_internal_locale_from_lcid", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_lcid)
ICALL(CULINF_6, "construct_internal_locale_from_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_name)
ICALL(CULINF_7, "construct_internal_locale_from_specific_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_specific_name)
ICALL(CULINF_8, "construct_number_format", ves_icall_System_Globalization_CultureInfo_construct_number_format)
ICALL(CULINF_9, "internal_get_cultures", ves_icall_System_Globalization_CultureInfo_internal_get_cultures)
//ICALL(CULINF_10, "internal_is_lcid_neutral", ves_icall_System_Globalization_CultureInfo_internal_is_lcid_neutral)

ICALL_TYPE(REGINF, "System.Globalization.RegionInfo", REGINF_1)
ICALL(REGINF_1, "construct_internal_region_from_lcid", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_lcid)
ICALL(REGINF_2, "construct_internal_region_from_name", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_name)

#ifndef PLATFORM_NO_DRIVEINFO
ICALL_TYPE(IODRIVEINFO, "System.IO.DriveInfo", IODRIVEINFO_1)
ICALL(IODRIVEINFO_1, "GetDiskFreeSpaceInternal", ves_icall_System_IO_DriveInfo_GetDiskFreeSpace)
ICALL(IODRIVEINFO_2, "GetDriveFormat", ves_icall_System_IO_DriveInfo_GetDriveFormat)
ICALL(IODRIVEINFO_3, "GetDriveTypeInternal", ves_icall_System_IO_DriveInfo_GetDriveType)
#endif

ICALL_TYPE(FAMW, "System.IO.FAMWatcher", FAMW_1)
ICALL(FAMW_1, "InternalFAMNextEvent", ves_icall_System_IO_FAMW_InternalFAMNextEvent)

ICALL_TYPE(FILEW, "System.IO.FileSystemWatcher", FILEW_4)
ICALL(FILEW_4, "InternalSupportsFSW", ves_icall_System_IO_FSW_SupportsFSW)

ICALL_TYPE(INOW, "System.IO.InotifyWatcher", INOW_1)
ICALL(INOW_1, "AddWatch", ves_icall_System_IO_InotifyWatcher_AddWatch)
ICALL(INOW_2, "GetInotifyInstance", ves_icall_System_IO_InotifyWatcher_GetInotifyInstance)
ICALL(INOW_3, "RemoveWatch", ves_icall_System_IO_InotifyWatcher_RemoveWatch)

#if defined (TARGET_IOS) || defined (TARGET_ANDROID)
ICALL_TYPE(MMAPIMPL, "System.IO.MemoryMappedFiles.MemoryMapImpl", MMAPIMPL_1)
ICALL(MMAPIMPL_1, "mono_filesize_from_fd", mono_filesize_from_fd)
ICALL(MMAPIMPL_2, "mono_filesize_from_path", mono_filesize_from_path)
#endif


ICALL_TYPE(MONOIO, "System.IO.MonoIO", MONOIO_1)
ICALL(MONOIO_1, "Close(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Close)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_2, "CopyFile(string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CopyFile)
ICALL(MONOIO_3, "CreateDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CreateDirectory)
ICALL(MONOIO_4, "CreatePipe(intptr&,intptr&)", ves_icall_System_IO_MonoIO_CreatePipe)
ICALL(MONOIO_5, "DeleteFile(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_DeleteFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_34, "DuplicateHandle", ves_icall_System_IO_MonoIO_DuplicateHandle)
ICALL(MONOIO_37, "FindClose", ves_icall_System_IO_MonoIO_FindClose)
ICALL(MONOIO_35, "FindFirst", ves_icall_System_IO_MonoIO_FindFirst)
ICALL(MONOIO_36, "FindNext", ves_icall_System_IO_MonoIO_FindNext)
ICALL(MONOIO_6, "Flush(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Flush)
ICALL(MONOIO_7, "GetCurrentDirectory(System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetCurrentDirectory)
ICALL(MONOIO_8, "GetFileAttributes(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileAttributes)
ICALL(MONOIO_9, "GetFileStat(string,System.IO.MonoIOStat&,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileStat)
ICALL(MONOIO_10, "GetFileSystemEntries", ves_icall_System_IO_MonoIO_GetFileSystemEntries)
ICALL(MONOIO_11, "GetFileType(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileType)
ICALL(MONOIO_12, "GetLength(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_13, "GetTempPath(string&)", ves_icall_System_IO_MonoIO_GetTempPath)
ICALL(MONOIO_14, "Lock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Lock)
ICALL(MONOIO_15, "MoveFile(string,string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_MoveFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_16, "Open(string,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.IO.FileOptions,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Open)
ICALL(MONOIO_17, "Read(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Read)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_18, "RemoveDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_RemoveDirectory)
ICALL(MONOIO_18M, "ReplaceFile(string,string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_ReplaceFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_19, "Seek(intptr,long,System.IO.SeekOrigin,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Seek)
ICALL(MONOIO_20, "SetCurrentDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetCurrentDirectory)
ICALL(MONOIO_21, "SetFileAttributes(string,System.IO.FileAttributes,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileAttributes)
ICALL(MONOIO_22, "SetFileTime(intptr,long,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileTime)
ICALL(MONOIO_23, "SetLength(intptr,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_24, "Unlock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Unlock)
#endif
ICALL(MONOIO_25, "Write(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Write)
ICALL(MONOIO_26, "get_AltDirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_AltDirectorySeparatorChar)
ICALL(MONOIO_27, "get_ConsoleError", ves_icall_System_IO_MonoIO_get_ConsoleError)
ICALL(MONOIO_28, "get_ConsoleInput", ves_icall_System_IO_MonoIO_get_ConsoleInput)
ICALL(MONOIO_29, "get_ConsoleOutput", ves_icall_System_IO_MonoIO_get_ConsoleOutput)
ICALL(MONOIO_30, "get_DirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_DirectorySeparatorChar)
ICALL(MONOIO_31, "get_InvalidPathChars", ves_icall_System_IO_MonoIO_get_InvalidPathChars)
ICALL(MONOIO_32, "get_PathSeparator", ves_icall_System_IO_MonoIO_get_PathSeparator)
ICALL(MONOIO_33, "get_VolumeSeparatorChar", ves_icall_System_IO_MonoIO_get_VolumeSeparatorChar)

ICALL_TYPE(IOPATH, "System.IO.Path", IOPATH_1)
ICALL(IOPATH_1, "get_temp_path", ves_icall_System_IO_get_temp_path)

ICALL_TYPE(MATH, "System.Math", MATH_1)
ICALL(MATH_1, "Acos", ves_icall_System_Math_Acos)
ICALL(MATH_2, "Asin", ves_icall_System_Math_Asin)
ICALL(MATH_3, "Atan", ves_icall_System_Math_Atan)
ICALL(MATH_4, "Atan2", ves_icall_System_Math_Atan2)
ICALL(MATH_5, "Cos", ves_icall_System_Math_Cos)
ICALL(MATH_6, "Cosh", ves_icall_System_Math_Cosh)
ICALL(MATH_7, "Exp", ves_icall_System_Math_Exp)
ICALL(MATH_8, "Floor", ves_icall_System_Math_Floor)
ICALL(MATH_9, "Log", ves_icall_System_Math_Log)
ICALL(MATH_10, "Log10", ves_icall_System_Math_Log10)
ICALL(MATH_11, "Pow", ves_icall_System_Math_Pow)
ICALL(MATH_12, "Round", ves_icall_System_Math_Round)
ICALL(MATH_13, "Round2", ves_icall_System_Math_Round2)
ICALL(MATH_14, "Sin", ves_icall_System_Math_Sin)
ICALL(MATH_15, "Sinh", ves_icall_System_Math_Sinh)
ICALL(MATH_16, "Sqrt", ves_icall_System_Math_Sqrt)
ICALL(MATH_17, "Tan", ves_icall_System_Math_Tan)
ICALL(MATH_18, "Tanh", ves_icall_System_Math_Tanh)

ICALL_TYPE(MCATTR, "System.MonoCustomAttrs", MCATTR_1)
ICALL(MCATTR_1, "GetCustomAttributesDataInternal", mono_reflection_get_custom_attrs_data)
ICALL(MCATTR_2, "GetCustomAttributesInternal", custom_attrs_get_by_type)
ICALL(MCATTR_3, "IsDefinedInternal", custom_attrs_defined_internal)

ICALL_TYPE(MENUM, "System.MonoEnumInfo", MENUM_1)
ICALL(MENUM_1, "get_enum_info", ves_icall_get_enum_info)

ICALL_TYPE(MTYPE, "System.MonoType", MTYPE_1)
ICALL(MTYPE_1, "GetArrayRank", ves_icall_MonoType_GetArrayRank)
ICALL(MTYPE_2, "GetConstructors", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_3, "GetConstructors_internal", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_4, "GetCorrespondingInflatedConstructor", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_5, "GetCorrespondingInflatedMethod", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_6, "GetElementType", ves_icall_MonoType_GetElementType)
ICALL(MTYPE_7, "GetEvents_internal", ves_icall_Type_GetEvents_internal)
ICALL(MTYPE_8, "GetField", ves_icall_Type_GetField)
ICALL(MTYPE_9, "GetFields_internal", ves_icall_Type_GetFields_internal)
ICALL(MTYPE_10, "GetGenericArguments", ves_icall_MonoType_GetGenericArguments)
ICALL(MTYPE_11, "GetInterfaces", ves_icall_Type_GetInterfaces)
ICALL(MTYPE_12, "GetMethodsByName", ves_icall_Type_GetMethodsByName)
ICALL(MTYPE_13, "GetNestedType", ves_icall_Type_GetNestedType)
ICALL(MTYPE_14, "GetNestedTypes", ves_icall_Type_GetNestedTypes)
ICALL(MTYPE_15, "GetPropertiesByName", ves_icall_Type_GetPropertiesByName)
ICALL(MTYPE_16, "InternalGetEvent", ves_icall_MonoType_GetEvent)
ICALL(MTYPE_17, "IsByRefImpl", ves_icall_type_isbyref)
ICALL(MTYPE_18, "IsCOMObjectImpl", ves_icall_type_iscomobject)
ICALL(MTYPE_19, "IsPointerImpl", ves_icall_type_ispointer)
ICALL(MTYPE_20, "IsPrimitiveImpl", ves_icall_type_isprimitive)
ICALL(MTYPE_21, "getFullName", ves_icall_System_MonoType_getFullName)
ICALL(MTYPE_22, "get_Assembly", ves_icall_MonoType_get_Assembly)
ICALL(MTYPE_23, "get_BaseType", ves_icall_get_type_parent)
ICALL(MTYPE_24, "get_DeclaringMethod", ves_icall_MonoType_get_DeclaringMethod)
ICALL(MTYPE_25, "get_DeclaringType", ves_icall_MonoType_get_DeclaringType)
ICALL(MTYPE_26, "get_IsGenericParameter", ves_icall_MonoType_get_IsGenericParameter)
ICALL(MTYPE_27, "get_Module", ves_icall_MonoType_get_Module)
ICALL(MTYPE_28, "get_Name", ves_icall_MonoType_get_Name)
ICALL(MTYPE_29, "get_Namespace", ves_icall_MonoType_get_Namespace)
ICALL(MTYPE_31, "get_attributes", ves_icall_get_attributes)
ICALL(MTYPE_33, "get_core_clr_security_level", vell_icall_MonoType_get_core_clr_security_level)
ICALL(MTYPE_32, "type_from_obj", mono_type_type_from_obj)

#ifndef DISABLE_SOCKETS
ICALL_TYPE(NDNS, "System.Net.Dns", NDNS_1)
ICALL(NDNS_1, "GetHostByAddr_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByAddr_internal)
ICALL(NDNS_2, "GetHostByName_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByName_internal)
ICALL(NDNS_3, "GetHostName_internal(string&)", ves_icall_System_Net_Dns_GetHostName_internal)

ICALL_TYPE(SOCK, "System.Net.Sockets.Socket", SOCK_1)
ICALL(SOCK_1, "Accept_internal(intptr,int&,bool)", ves_icall_System_Net_Sockets_Socket_Accept_internal)
ICALL(SOCK_2, "Available_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Available_internal)
ICALL(SOCK_3, "Bind_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Bind_internal)
ICALL(SOCK_4, "Blocking_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Blocking_internal)
ICALL(SOCK_5, "Close_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Close_internal)
ICALL(SOCK_6, "Connect_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Connect_internal)
ICALL (SOCK_6a, "Disconnect_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Disconnect_internal)
ICALL(SOCK_7, "GetSocketOption_arr_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,byte[]&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_arr_internal)
ICALL(SOCK_8, "GetSocketOption_obj_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_obj_internal)
ICALL(SOCK_9, "Listen_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_Listen_internal)
ICALL(SOCK_10, "LocalEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_LocalEndPoint_internal)
ICALL(SOCK_11, "Poll_internal", ves_icall_System_Net_Sockets_Socket_Poll_internal)
ICALL(SOCK_11a, "Receive_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_array_internal)
ICALL(SOCK_12, "Receive_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_internal)
ICALL(SOCK_13, "RecvFrom_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress&,int&)", ves_icall_System_Net_Sockets_Socket_RecvFrom_internal)
ICALL(SOCK_14, "RemoteEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_RemoteEndPoint_internal)
ICALL(SOCK_15, "Select_internal(System.Net.Sockets.Socket[]&,int,int&)", ves_icall_System_Net_Sockets_Socket_Select_internal)
ICALL(SOCK_15a, "SendFile(intptr,string,byte[],byte[],System.Net.Sockets.TransmitFileOptions)", ves_icall_System_Net_Sockets_Socket_SendFile)
ICALL(SOCK_16, "SendTo_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_SendTo_internal)
ICALL(SOCK_16a, "Send_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_array_internal)
ICALL(SOCK_17, "Send_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_internal)
ICALL(SOCK_18, "SetSocketOption_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object,byte[],int,int&)", ves_icall_System_Net_Sockets_Socket_SetSocketOption_internal)
ICALL(SOCK_19, "Shutdown_internal(intptr,System.Net.Sockets.SocketShutdown,int&)", ves_icall_System_Net_Sockets_Socket_Shutdown_internal)
ICALL(SOCK_20, "Socket_internal(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType,int&)", ves_icall_System_Net_Sockets_Socket_Socket_internal)
ICALL(SOCK_21, "WSAIoctl(intptr,int,byte[],byte[],int&)", ves_icall_System_Net_Sockets_Socket_WSAIoctl)
ICALL(SOCK_21a, "cancel_blocking_socket_operation", icall_cancel_blocking_socket_operation)
ICALL(SOCK_22, "socket_pool_queue", icall_append_io_job)

ICALL_TYPE(SOCKEX, "System.Net.Sockets.SocketException", SOCKEX_1)
ICALL(SOCKEX_1, "WSAGetLastError_internal", ves_icall_System_Net_Sockets_SocketException_WSAGetLastError_internal)
#endif /* !DISABLE_SOCKETS */

ICALL_TYPE(NUMBER_FORMATTER, "System.NumberFormatter", NUMBER_FORMATTER_1)
ICALL(NUMBER_FORMATTER_1, "GetFormatterTables", ves_icall_System_NumberFormatter_GetFormatterTables)

ICALL_TYPE(OBJ, "System.Object", OBJ_1)
ICALL(OBJ_1, "GetType", ves_icall_System_Object_GetType)
ICALL(OBJ_2, "InternalGetHashCode", mono_object_hash)
ICALL(OBJ_3, "MemberwiseClone", ves_icall_System_Object_MemberwiseClone)
ICALL(OBJ_4, "obj_address", ves_icall_System_Object_obj_address)

ICALL_TYPE(ASSEM, "System.Reflection.Assembly", ASSEM_1)
ICALL(ASSEM_1, "FillName", ves_icall_System_Reflection_Assembly_FillName)
ICALL(ASSEM_2, "GetCallingAssembly", ves_icall_System_Reflection_Assembly_GetCallingAssembly)
ICALL(ASSEM_3, "GetEntryAssembly", ves_icall_System_Reflection_Assembly_GetEntryAssembly)
ICALL(ASSEM_4, "GetExecutingAssembly", ves_icall_System_Reflection_Assembly_GetExecutingAssembly)
ICALL(ASSEM_5, "GetFilesInternal", ves_icall_System_Reflection_Assembly_GetFilesInternal)
ICALL(ASSEM_6, "GetManifestModuleInternal", ves_icall_System_Reflection_Assembly_GetManifestModuleInternal)
ICALL(ASSEM_7, "GetManifestResourceInfoInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInfoInternal)
ICALL(ASSEM_8, "GetManifestResourceInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInternal)
ICALL(ASSEM_9, "GetManifestResourceNames", ves_icall_System_Reflection_Assembly_GetManifestResourceNames)
ICALL(ASSEM_10, "GetModulesInternal", ves_icall_System_Reflection_Assembly_GetModulesInternal)
//ICALL(ASSEM_11, "GetNamespaces", ves_icall_System_Reflection_Assembly_GetNamespaces)
ICALL(ASSEM_12, "GetReferencedAssemblies", ves_icall_System_Reflection_Assembly_GetReferencedAssemblies)
ICALL(ASSEM_13, "GetTypes", ves_icall_System_Reflection_Assembly_GetTypes)
ICALL(ASSEM_14, "InternalGetAssemblyName", ves_icall_System_Reflection_Assembly_InternalGetAssemblyName)
ICALL(ASSEM_15, "InternalGetType", ves_icall_System_Reflection_Assembly_InternalGetType)
ICALL(ASSEM_16, "InternalImageRuntimeVersion", ves_icall_System_Reflection_Assembly_InternalImageRuntimeVersion)
ICALL(ASSEM_17, "LoadFrom", ves_icall_System_Reflection_Assembly_LoadFrom)
ICALL(ASSEM_18, "LoadPermissions", ves_icall_System_Reflection_Assembly_LoadPermissions)
	/*
	 * Private icalls for the Mono Debugger
	 */
ICALL(ASSEM_19, "MonoDebugger_GetMethodToken", ves_icall_MonoDebugger_GetMethodToken)

	/* normal icalls again */
ICALL(ASSEM_20, "get_EntryPoint", ves_icall_System_Reflection_Assembly_get_EntryPoint)
ICALL(ASSEM_21, "get_ReflectionOnly", ves_icall_System_Reflection_Assembly_get_ReflectionOnly)
ICALL(ASSEM_22, "get_code_base", ves_icall_System_Reflection_Assembly_get_code_base)
ICALL(ASSEM_23, "get_fullname", ves_icall_System_Reflection_Assembly_get_fullName)
ICALL(ASSEM_24, "get_global_assembly_cache", ves_icall_System_Reflection_Assembly_get_global_assembly_cache)
ICALL(ASSEM_25, "get_location", ves_icall_System_Reflection_Assembly_get_location)
ICALL(ASSEM_26, "load_with_partial_name", ves_icall_System_Reflection_Assembly_load_with_partial_name)

ICALL_TYPE(ASSEMN, "System.Reflection.AssemblyName", ASSEMN_1)
ICALL(ASSEMN_1, "ParseName", ves_icall_System_Reflection_AssemblyName_ParseName)
ICALL(ASSEMN_2, "get_public_token", mono_digest_get_public_token)

ICALL_TYPE(CATTR_DATA, "System.Reflection.CustomAttributeData", CATTR_DATA_1)
ICALL(CATTR_DATA_1, "ResolveArgumentsInternal", mono_reflection_resolve_custom_attribute_data)

ICALL_TYPE(ASSEMB, "System.Reflection.Emit.AssemblyBuilder", ASSEMB_1)
ICALL(ASSEMB_1, "InternalAddModule", mono_image_load_module_dynamic)
ICALL(ASSEMB_2, "basic_init", mono_image_basic_init)

ICALL_TYPE(CATTRB, "System.Reflection.Emit.CustomAttributeBuilder", CATTRB_1)
ICALL(CATTRB_1, "GetBlob", mono_reflection_get_custom_attrs_blob)

#ifndef DISABLE_REFLECTION_EMIT
ICALL_TYPE(DERIVEDTYPE, "System.Reflection.Emit.DerivedType", DERIVEDTYPE_1)
ICALL(DERIVEDTYPE_1, "create_unmanaged_type", mono_reflection_create_unmanaged_type)
#endif

ICALL_TYPE(DYNM, "System.Reflection.Emit.DynamicMethod", DYNM_1)
ICALL(DYNM_1, "create_dynamic_method", mono_reflection_create_dynamic_method)

ICALL_TYPE(ENUMB, "System.Reflection.Emit.EnumBuilder", ENUMB_1)
ICALL(ENUMB_1, "setup_enum_type", ves_icall_EnumBuilder_setup_enum_type)

ICALL_TYPE(GPARB, "System.Reflection.Emit.GenericTypeParameterBuilder", GPARB_1)
ICALL(GPARB_1, "initialize", mono_reflection_initialize_generic_parameter)

ICALL_TYPE(METHODB, "System.Reflection.Emit.MethodBuilder", METHODB_1)
ICALL(METHODB_1, "MakeGenericMethod", mono_reflection_bind_generic_method_parameters)

ICALL_TYPE(MODULEB, "System.Reflection.Emit.ModuleBuilder", MODULEB_8)
ICALL(MODULEB_8, "RegisterToken", ves_icall_ModuleBuilder_RegisterToken)
ICALL(MODULEB_1, "WriteToFile", ves_icall_ModuleBuilder_WriteToFile)
ICALL(MODULEB_2, "basic_init", mono_image_module_basic_init)
ICALL(MODULEB_3, "build_metadata", ves_icall_ModuleBuilder_build_metadata)
ICALL(MODULEB_4, "create_modified_type", ves_icall_ModuleBuilder_create_modified_type)
ICALL(MODULEB_5, "getMethodToken", ves_icall_ModuleBuilder_getMethodToken)
ICALL(MODULEB_6, "getToken", ves_icall_ModuleBuilder_getToken)
ICALL(MODULEB_7, "getUSIndex", mono_image_insert_string)
ICALL(MODULEB_9, "set_wrappers_type", mono_image_set_wrappers_type)

ICALL_TYPE(SIGH, "System.Reflection.Emit.SignatureHelper", SIGH_1)
ICALL(SIGH_1, "get_signature_field", mono_reflection_sighelper_get_signature_field)
ICALL(SIGH_2, "get_signature_local", mono_reflection_sighelper_get_signature_local)

ICALL_TYPE(TYPEB, "System.Reflection.Emit.TypeBuilder", TYPEB_1)
ICALL(TYPEB_1, "create_generic_class", mono_reflection_create_generic_class)
ICALL(TYPEB_2, "create_internal_class", mono_reflection_create_internal_class)
ICALL(TYPEB_3, "create_runtime_class", mono_reflection_create_runtime_class)
ICALL(TYPEB_4, "get_IsGenericParameter", ves_icall_TypeBuilder_get_IsGenericParameter)
ICALL(TYPEB_5, "get_event_info", mono_reflection_event_builder_get_event_info)
ICALL(TYPEB_6, "setup_generic_class", mono_reflection_setup_generic_class)
ICALL(TYPEB_7, "setup_internal_class", mono_reflection_setup_internal_class)

ICALL_TYPE(FIELDI, "System.Reflection.FieldInfo", FILEDI_1)
ICALL(FILEDI_1, "GetTypeModifiers", ves_icall_System_Reflection_FieldInfo_GetTypeModifiers)
ICALL(FILEDI_2, "get_marshal_info", ves_icall_System_Reflection_FieldInfo_get_marshal_info)
ICALL(FILEDI_3, "internal_from_handle_type", ves_icall_System_Reflection_FieldInfo_internal_from_handle_type)

ICALL_TYPE(MEMBERI, "System.Reflection.MemberInfo", MEMBERI_1)
ICALL(MEMBERI_1, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MBASE, "System.Reflection.MethodBase", MBASE_1)
ICALL(MBASE_1, "GetCurrentMethod", ves_icall_GetCurrentMethod)
ICALL(MBASE_2, "GetMethodBodyInternal", ves_icall_System_Reflection_MethodBase_GetMethodBodyInternal)
ICALL(MBASE_3, "GetMethodFromHandleInternal", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternal)
ICALL(MBASE_4, "GetMethodFromHandleInternalType", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternalType)

ICALL_TYPE(MODULE, "System.Reflection.Module", MODULE_1)
ICALL(MODULE_1, "Close", ves_icall_System_Reflection_Module_Close)
ICALL(MODULE_2, "GetGlobalType", ves_icall_System_Reflection_Module_GetGlobalType)
ICALL(MODULE_3, "GetGuidInternal", ves_icall_System_Reflection_Module_GetGuidInternal)
ICALL(MODULE_14, "GetHINSTANCE", ves_icall_System_Reflection_Module_GetHINSTANCE)
ICALL(MODULE_4, "GetMDStreamVersion", ves_icall_System_Reflection_Module_GetMDStreamVersion)
ICALL(MODULE_5, "GetPEKind", ves_icall_System_Reflection_Module_GetPEKind)
ICALL(MODULE_6, "InternalGetTypes", ves_icall_System_Reflection_Module_InternalGetTypes)
ICALL(MODULE_7, "ResolveFieldToken", ves_icall_System_Reflection_Module_ResolveFieldToken)
ICALL(MODULE_8, "ResolveMemberToken", ves_icall_System_Reflection_Module_ResolveMemberToken)
ICALL(MODULE_9, "ResolveMethodToken", ves_icall_System_Reflection_Module_ResolveMethodToken)
ICALL(MODULE_10, "ResolveSignature", ves_icall_System_Reflection_Module_ResolveSignature)
ICALL(MODULE_11, "ResolveStringToken", ves_icall_System_Reflection_Module_ResolveStringToken)
ICALL(MODULE_12, "ResolveTypeToken", ves_icall_System_Reflection_Module_ResolveTypeToken)
ICALL(MODULE_13, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MCMETH, "System.Reflection.MonoCMethod", MCMETH_1)
ICALL(MCMETH_1, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MCMETH_2, "InternalInvoke", ves_icall_InternalInvoke)

ICALL_TYPE(MEVIN, "System.Reflection.MonoEventInfo", MEVIN_1)
ICALL(MEVIN_1, "get_event_info", ves_icall_get_event_info)

ICALL_TYPE(MFIELD, "System.Reflection.MonoField", MFIELD_1)
ICALL(MFIELD_1, "GetFieldOffset", ves_icall_MonoField_GetFieldOffset)
ICALL(MFIELD_2, "GetParentType", ves_icall_MonoField_GetParentType)
ICALL(MFIELD_5, "GetRawConstantValue", ves_icall_MonoField_GetRawConstantValue)
ICALL(MFIELD_3, "GetValueInternal", ves_icall_MonoField_GetValueInternal)
ICALL(MFIELD_6, "ResolveType", ves_icall_MonoField_ResolveType)
ICALL(MFIELD_4, "SetValueInternal", ves_icall_MonoField_SetValueInternal)

ICALL_TYPE(MGENCM, "System.Reflection.MonoGenericCMethod", MGENCM_1)
ICALL(MGENCM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MGENCL, "System.Reflection.MonoGenericClass", MGENCL_5)
ICALL(MGENCL_5, "initialize", mono_reflection_generic_class_initialize)
ICALL(MGENCL_6, "register_with_runtime", mono_reflection_register_with_runtime)

/* note this is the same as above: unify */
ICALL_TYPE(MGENM, "System.Reflection.MonoGenericMethod", MGENM_1)
ICALL(MGENM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MMETH, "System.Reflection.MonoMethod", MMETH_1)
ICALL(MMETH_1, "GetDllImportAttribute", ves_icall_MonoMethod_GetDllImportAttribute)
ICALL(MMETH_2, "GetGenericArguments", ves_icall_MonoMethod_GetGenericArguments)
ICALL(MMETH_3, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MMETH_4, "InternalInvoke", ves_icall_InternalInvoke)
ICALL(MMETH_5, "MakeGenericMethod_impl", mono_reflection_bind_generic_method_parameters)
ICALL(MMETH_6, "get_IsGenericMethod", ves_icall_MonoMethod_get_IsGenericMethod)
ICALL(MMETH_7, "get_IsGenericMethodDefinition", ves_icall_MonoMethod_get_IsGenericMethodDefinition)
ICALL(MMETH_8, "get_base_method", ves_icall_MonoMethod_get_base_method)
ICALL(MMETH_9, "get_name", ves_icall_MonoMethod_get_name)

ICALL_TYPE(MMETHI, "System.Reflection.MonoMethodInfo", MMETHI_4)
ICALL(MMETHI_4, "get_method_attributes", vell_icall_get_method_attributes)
ICALL(MMETHI_1, "get_method_info", ves_icall_get_method_info)
ICALL(MMETHI_2, "get_parameter_info", ves_icall_get_parameter_info)
ICALL(MMETHI_3, "get_retval_marshal", ves_icall_System_MonoMethodInfo_get_retval_marshal)

ICALL_TYPE(MPROPI, "System.Reflection.MonoPropertyInfo", MPROPI_1)
ICALL(MPROPI_1, "GetTypeModifiers", property_info_get_type_modifiers)
ICALL(MPROPI_3, "get_default_value", property_info_get_default_value)
ICALL(MPROPI_2, "get_property_info", ves_icall_get_property_info)

ICALL_TYPE(PARAMI, "System.Reflection.ParameterInfo", PARAMI_1)
ICALL(PARAMI_1, "GetMetadataToken", mono_reflection_get_token)
ICALL(PARAMI_2, "GetTypeModifiers", param_info_get_type_modifiers)

ICALL_TYPE(RUNH, "System.Runtime.CompilerServices.RuntimeHelpers", RUNH_1)
ICALL(RUNH_1, "GetObjectValue", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetObjectValue)
	 /* REMOVEME: no longer needed, just so we dont break things when not needed */
ICALL(RUNH_2, "GetOffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)
ICALL(RUNH_3, "InitializeArray", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_InitializeArray)
ICALL(RUNH_4, "RunClassConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunClassConstructor)
ICALL(RUNH_5, "RunModuleConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunModuleConstructor)
ICALL(RUNH_5h, "SufficientExecutionStack", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_SufficientExecutionStack)
ICALL(RUNH_6, "get_OffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)

ICALL_TYPE(GCH, "System.Runtime.InteropServices.GCHandle", GCH_1)
ICALL(GCH_1, "CheckCurrentDomain", GCHandle_CheckCurrentDomain)
ICALL(GCH_2, "FreeHandle", ves_icall_System_GCHandle_FreeHandle)
ICALL(GCH_3, "GetAddrOfPinnedObject", ves_icall_System_GCHandle_GetAddrOfPinnedObject)
ICALL(GCH_4, "GetTarget", ves_icall_System_GCHandle_GetTarget)
ICALL(GCH_5, "GetTargetHandle", ves_icall_System_GCHandle_GetTargetHandle)

#ifndef DISABLE_COM
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_1)
ICALL(MARSHAL_1, "AddRefInternal", ves_icall_System_Runtime_InteropServices_Marshal_AddRefInternal)
#else
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_2)
#endif
ICALL(MARSHAL_2, "AllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_AllocCoTaskMem)
ICALL(MARSHAL_3, "AllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_AllocHGlobal)
ICALL(MARSHAL_4, "DestroyStructure", ves_icall_System_Runtime_InteropServices_Marshal_DestroyStructure)
ICALL(MARSHAL_5, "FreeBSTR", ves_icall_System_Runtime_InteropServices_Marshal_FreeBSTR)
ICALL(MARSHAL_6, "FreeCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_FreeCoTaskMem)
ICALL(MARSHAL_7, "FreeHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_FreeHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_44, "GetCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetCCW)
ICALL(MARSHAL_8, "GetComSlotForMethodInfoInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetComSlotForMethodInfoInternal)
#endif
ICALL(MARSHAL_9, "GetDelegateForFunctionPointerInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetDelegateForFunctionPointerInternal)
ICALL(MARSHAL_10, "GetFunctionPointerForDelegateInternal", mono_delegate_to_ftnptr)
#ifndef DISABLE_COM
ICALL(MARSHAL_45, "GetIDispatchForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIDispatchForObjectInternal)
ICALL(MARSHAL_46, "GetIUnknownForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIUnknownForObjectInternal)
#endif
ICALL(MARSHAL_11, "GetLastWin32Error", ves_icall_System_Runtime_InteropServices_Marshal_GetLastWin32Error)
#ifndef DISABLE_COM
ICALL(MARSHAL_47, "GetObjectForCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetObjectForCCW)
ICALL(MARSHAL_48, "IsComObject", ves_icall_System_Runtime_InteropServices_Marshal_IsComObject)
#endif
ICALL(MARSHAL_12, "OffsetOf", ves_icall_System_Runtime_InteropServices_Marshal_OffsetOf)
ICALL(MARSHAL_13, "Prelink", ves_icall_System_Runtime_InteropServices_Marshal_Prelink)
ICALL(MARSHAL_14, "PrelinkAll", ves_icall_System_Runtime_InteropServices_Marshal_PrelinkAll)
ICALL(MARSHAL_15, "PtrToStringAnsi(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi)
ICALL(MARSHAL_16, "PtrToStringAnsi(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi_len)
#ifndef DISABLE_COM
ICALL(MARSHAL_17, "PtrToStringBSTR", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringBSTR)
#endif
ICALL(MARSHAL_18, "PtrToStringUni(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni)
ICALL(MARSHAL_19, "PtrToStringUni(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni_len)
ICALL(MARSHAL_20, "PtrToStructure(intptr,System.Type)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure_type)
ICALL(MARSHAL_21, "PtrToStructure(intptr,object)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure)
#ifndef DISABLE_COM
ICALL(MARSHAL_22, "QueryInterfaceInternal", ves_icall_System_Runtime_InteropServices_Marshal_QueryInterfaceInternal)
#endif
ICALL(MARSHAL_43, "ReAllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocCoTaskMem)
ICALL(MARSHAL_23, "ReAllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_49, "ReleaseComObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseComObjectInternal)
ICALL(MARSHAL_29, "ReleaseInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseInternal)
#endif
ICALL(MARSHAL_30, "SizeOf", ves_icall_System_Runtime_InteropServices_Marshal_SizeOf)
ICALL(MARSHAL_31, "StringToBSTR", ves_icall_System_Runtime_InteropServices_Marshal_StringToBSTR)
ICALL(MARSHAL_32, "StringToHGlobalAnsi", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalAnsi)
ICALL(MARSHAL_33, "StringToHGlobalUni", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalUni)
ICALL(MARSHAL_34, "StructureToPtr", ves_icall_System_Runtime_InteropServices_Marshal_StructureToPtr)
ICALL(MARSHAL_35, "UnsafeAddrOfPinnedArrayElement", ves_icall_System_Runtime_InteropServices_Marshal_UnsafeAddrOfPinnedArrayElement)
ICALL(MARSHAL_41, "copy_from_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_from_unmanaged)
ICALL(MARSHAL_42, "copy_to_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_to_unmanaged)

ICALL_TYPE(ACTS, "System.Runtime.Remoting.Activation.ActivationServices", ACTS_1)
ICALL(ACTS_1, "AllocateUninitializedClassInstance", ves_icall_System_Runtime_Activation_ActivationServices_AllocateUninitializedClassInstance)
ICALL(ACTS_2, "EnableProxyActivation", ves_icall_System_Runtime_Activation_ActivationServices_EnableProxyActivation)

ICALL_TYPE(MONOMM, "System.Runtime.Remoting.Messaging.MonoMethodMessage", MONOMM_1)
ICALL(MONOMM_1, "InitMessage", ves_icall_MonoMethodMessage_InitMessage)

#ifndef DISABLE_REMOTING
ICALL_TYPE(REALP, "System.Runtime.Remoting.Proxies.RealProxy", REALP_1)
ICALL(REALP_1, "InternalGetProxyType", ves_icall_Remoting_RealProxy_InternalGetProxyType)
ICALL(REALP_2, "InternalGetTransparentProxy", ves_icall_Remoting_RealProxy_GetTransparentProxy)

ICALL_TYPE(REMSER, "System.Runtime.Remoting.RemotingServices", REMSER_0)
ICALL(REMSER_0, "GetVirtualMethod", ves_icall_Remoting_RemotingServices_GetVirtualMethod)
ICALL(REMSER_1, "InternalExecute", ves_icall_InternalExecute)
ICALL(REMSER_2, "IsTransparentProxy", ves_icall_IsTransparentProxy)
#endif

ICALL_TYPE(MHAN, "System.RuntimeMethodHandle", MHAN_1)
ICALL(MHAN_1, "GetFunctionPointer", ves_icall_RuntimeMethod_GetFunctionPointer)

ICALL_TYPE(RNG, "System.Security.Cryptography.RNGCryptoServiceProvider", RNG_1)
ICALL(RNG_1, "RngClose", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngClose)
ICALL(RNG_2, "RngGetBytes", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngGetBytes)
ICALL(RNG_3, "RngInitialize", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngInitialize)
ICALL(RNG_4, "RngOpen", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngOpen)

#ifndef DISABLE_POLICY_EVIDENCE
ICALL_TYPE(EVID, "System.Security.Policy.Evidence", EVID_1)
ICALL(EVID_1, "IsAuthenticodePresent", ves_icall_System_Security_Policy_Evidence_IsAuthenticodePresent)

ICALL_TYPE(WINID, "System.Security.Principal.WindowsIdentity", WINID_1)
ICALL(WINID_1, "GetCurrentToken", ves_icall_System_Security_Principal_WindowsIdentity_GetCurrentToken)
ICALL(WINID_2, "GetTokenName", ves_icall_System_Security_Principal_WindowsIdentity_GetTokenName)
ICALL(WINID_3, "GetUserToken", ves_icall_System_Security_Principal_WindowsIdentity_GetUserToken)
ICALL(WINID_4, "_GetRoles", ves_icall_System_Security_Principal_WindowsIdentity_GetRoles)

ICALL_TYPE(WINIMP, "System.Security.Principal.WindowsImpersonationContext", WINIMP_1)
ICALL(WINIMP_1, "CloseToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_CloseToken)
ICALL(WINIMP_2, "DuplicateToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_DuplicateToken)
ICALL(WINIMP_3, "RevertToSelf", ves_icall_System_Security_Principal_WindowsImpersonationContext_RevertToSelf)
ICALL(WINIMP_4, "SetCurrentToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_SetCurrentToken)

ICALL_TYPE(WINPRIN, "System.Security.Principal.WindowsPrincipal", WINPRIN_1)
ICALL(WINPRIN_1, "IsMemberOfGroupId", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupId)
ICALL(WINPRIN_2, "IsMemberOfGroupName", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupName)

ICALL_TYPE(SECSTRING, "System.Security.SecureString", SECSTRING_1)
ICALL(SECSTRING_1, "DecryptInternal", ves_icall_System_Security_SecureString_DecryptInternal)
ICALL(SECSTRING_2, "EncryptInternal", ves_icall_System_Security_SecureString_EncryptInternal)
#endif /* !DISABLE_POLICY_EVIDENCE */

ICALL_TYPE(SECMAN, "System.Security.SecurityManager", SECMAN_1)
ICALL(SECMAN_1, "GetLinkDemandSecurity", ves_icall_System_Security_SecurityManager_GetLinkDemandSecurity)
ICALL(SECMAN_2, "get_CheckExecutionRights", ves_icall_System_Security_SecurityManager_get_CheckExecutionRights)
ICALL(SECMAN_3, "get_RequiresElevatedPermissions", mono_security_core_clr_require_elevated_permissions)
ICALL(SECMAN_4, "get_SecurityEnabled", ves_icall_System_Security_SecurityManager_get_SecurityEnabled)
ICALL(SECMAN_5, "set_CheckExecutionRights", ves_icall_System_Security_SecurityManager_set_CheckExecutionRights)
ICALL(SECMAN_6, "set_SecurityEnabled", ves_icall_System_Security_SecurityManager_set_SecurityEnabled)

ICALL_TYPE(STRING, "System.String", STRING_1)
ICALL(STRING_1, ".ctor(char*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_2, ".ctor(char*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_3, ".ctor(char,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_4, ".ctor(char[])", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_5, ".ctor(char[],int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_6, ".ctor(sbyte*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_7, ".ctor(sbyte*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8, ".ctor(sbyte*,int,int,System.Text.Encoding)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8a, "GetLOSLimit", ves_icall_System_String_GetLOSLimit)
ICALL(STRING_9, "InternalAllocateStr", ves_icall_System_String_InternalAllocateStr)
ICALL(STRING_10, "InternalIntern", ves_icall_System_String_InternalIntern)
ICALL(STRING_11, "InternalIsInterned", ves_icall_System_String_InternalIsInterned)

ICALL_TYPE(TENC, "System.Text.Encoding", TENC_1)
ICALL(TENC_1, "InternalCodePage", ves_icall_System_Text_Encoding_InternalCodePage)

ICALL_TYPE(ILOCK, "System.Threading.Interlocked", ILOCK_1)
ICALL(ILOCK_1, "Add(int&,int)", ves_icall_System_Threading_Interlocked_Add_Int)
ICALL(ILOCK_2, "Add(long&,long)", ves_icall_System_Threading_Interlocked_Add_Long)
ICALL(ILOCK_3, "CompareExchange(T&,T,T)", ves_icall_System_Threading_Interlocked_CompareExchange_T)
ICALL(ILOCK_4, "CompareExchange(double&,double,double)", ves_icall_System_Threading_Interlocked_CompareExchange_Double)
ICALL(ILOCK_5, "CompareExchange(int&,int,int)", ves_icall_System_Threading_Interlocked_CompareExchange_Int)
ICALL(ILOCK_6, "CompareExchange(intptr&,intptr,intptr)", ves_icall_System_Threading_Interlocked_CompareExchange_IntPtr)
ICALL(ILOCK_7, "CompareExchange(long&,long,long)", ves_icall_System_Threading_Interlocked_CompareExchange_Long)
ICALL(ILOCK_8, "CompareExchange(object&,object,object)", ves_icall_System_Threading_Interlocked_CompareExchange_Object)
ICALL(ILOCK_9, "CompareExchange(single&,single,single)", ves_icall_System_Threading_Interlocked_CompareExchange_Single)
ICALL(ILOCK_10, "Decrement(int&)", ves_icall_System_Threading_Interlocked_Decrement_Int)
ICALL(ILOCK_11, "Decrement(long&)", ves_icall_System_Threading_Interlocked_Decrement_Long)
ICALL(ILOCK_12, "Exchange(T&,T)", ves_icall_System_Threading_Interlocked_Exchange_T)
ICALL(ILOCK_13, "Exchange(double&,double)", ves_icall_System_Threading_Interlocked_Exchange_Double)
ICALL(ILOCK_14, "Exchange(int&,int)", ves_icall_System_Threading_Interlocked_Exchange_Int)
ICALL(ILOCK_15, "Exchange(intptr&,intptr)", ves_icall_System_Threading_Interlocked_Exchange_IntPtr)
ICALL(ILOCK_16, "Exchange(long&,long)", ves_icall_System_Threading_Interlocked_Exchange_Long)
ICALL(ILOCK_17, "Exchange(object&,object)", ves_icall_System_Threading_Interlocked_Exchange_Object)
ICALL(ILOCK_18, "Exchange(single&,single)", ves_icall_System_Threading_Interlocked_Exchange_Single)
ICALL(ILOCK_19, "Increment(int&)", ves_icall_System_Threading_Interlocked_Increment_Int)
ICALL(ILOCK_20, "Increment(long&)", ves_icall_System_Threading_Interlocked_Increment_Long)
ICALL(ILOCK_21, "Read(long&)", ves_icall_System_Threading_Interlocked_Read_Long)

ICALL_TYPE(ITHREAD, "System.Threading.InternalThread", ITHREAD_1)
ICALL(ITHREAD_1, "Thread_free_internal", ves_icall_System_Threading_InternalThread_Thread_free_internal)

ICALL_TYPE(MONIT, "System.Threading.Monitor", MONIT_8)
ICALL(MONIT_8, "Enter", mono_monitor_enter)
ICALL(MONIT_1, "Exit", mono_monitor_exit)
ICALL(MONIT_2, "Monitor_pulse", ves_icall_System_Threading_Monitor_Monitor_pulse)
ICALL(MONIT_3, "Monitor_pulse_all", ves_icall_System_Threading_Monitor_Monitor_pulse_all)
ICALL(MONIT_4, "Monitor_test_owner", ves_icall_System_Threading_Monitor_Monitor_test_owner)
ICALL(MONIT_5, "Monitor_test_synchronised", ves_icall_System_Threading_Monitor_Monitor_test_synchronised)
ICALL(MONIT_6, "Monitor_try_enter", ves_icall_System_Threading_Monitor_Monitor_try_enter)
ICALL(MONIT_7, "Monitor_wait", ves_icall_System_Threading_Monitor_Monitor_wait)
ICALL(MONIT_9, "try_enter_with_atomic_var", ves_icall_System_Threading_Monitor_Monitor_try_enter_with_atomic_var)

ICALL_TYPE(MUTEX, "System.Threading.Mutex", MUTEX_1)
ICALL(MUTEX_1, "CreateMutex_internal(bool,string,bool&)", ves_icall_System_Threading_Mutex_CreateMutex_internal)
ICALL(MUTEX_2, "OpenMutex_internal(string,System.Security.AccessControl.MutexRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Mutex_OpenMutex_internal)
ICALL(MUTEX_3, "ReleaseMutex_internal(intptr)", ves_icall_System_Threading_Mutex_ReleaseMutex_internal)

ICALL_TYPE(NATIVEC, "System.Threading.NativeEventCalls", NATIVEC_1)
ICALL(NATIVEC_1, "CloseEvent_internal", ves_icall_System_Threading_Events_CloseEvent_internal)
ICALL(NATIVEC_2, "CreateEvent_internal(bool,bool,string,bool&)", ves_icall_System_Threading_Events_CreateEvent_internal)
ICALL(NATIVEC_3, "OpenEvent_internal(string,System.Security.AccessControl.EventWaitHandleRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Events_OpenEvent_internal)
ICALL(NATIVEC_4, "ResetEvent_internal",  ves_icall_System_Threading_Events_ResetEvent_internal)
ICALL(NATIVEC_5, "SetEvent_internal",    ves_icall_System_Threading_Events_SetEvent_internal)

ICALL_TYPE(SEMA, "System.Threading.Semaphore", SEMA_1)
ICALL(SEMA_1, "CreateSemaphore_internal(int,int,string,bool&)", ves_icall_System_Threading_Semaphore_CreateSemaphore_internal)
ICALL(SEMA_2, "OpenSemaphore_internal(string,System.Security.AccessControl.SemaphoreRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Semaphore_OpenSemaphore_internal)
ICALL(SEMA_3, "ReleaseSemaphore_internal(intptr,int,bool&)", ves_icall_System_Threading_Semaphore_ReleaseSemaphore_internal)

ICALL_TYPE(THREAD, "System.Threading.Thread", THREAD_1)
ICALL(THREAD_1, "Abort_internal(System.Threading.InternalThread,object)", ves_icall_System_Threading_Thread_Abort)
ICALL(THREAD_1aa, "AllocTlsData", mono_thread_alloc_tls)
ICALL(THREAD_1a, "ByteArrayToCurrentDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToCurrentDomain)
ICALL(THREAD_1b, "ByteArrayToRootDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToRootDomain)
ICALL(THREAD_2, "ClrState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_ClrState)
ICALL(THREAD_2a, "ConstructInternalThread", ves_icall_System_Threading_Thread_ConstructInternalThread)
ICALL(THREAD_3, "CurrentInternalThread_internal", mono_thread_internal_current)
ICALL(THREAD_3a, "DestroyTlsData", mono_thread_destroy_tls)
ICALL(THREAD_4, "FreeLocalSlotValues", mono_thread_free_local_slot_values)
ICALL(THREAD_55, "GetAbortExceptionState", ves_icall_System_Threading_Thread_GetAbortExceptionState)
ICALL(THREAD_7, "GetDomainID", ves_icall_System_Threading_Thread_GetDomainID)
ICALL(THREAD_8, "GetName_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetName_internal)
ICALL(THREAD_11, "GetState(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetState)
ICALL(THREAD_53, "Interrupt_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Interrupt_internal)
ICALL(THREAD_12, "Join_internal(System.Threading.InternalThread,int,intptr)", ves_icall_System_Threading_Thread_Join_internal)
ICALL(THREAD_13, "MemoryBarrier", ves_icall_System_Threading_Thread_MemoryBarrier)
ICALL(THREAD_14, "ResetAbort_internal()", ves_icall_System_Threading_Thread_ResetAbort)
ICALL(THREAD_15, "Resume_internal()", ves_icall_System_Threading_Thread_Resume)
ICALL(THREAD_18, "SetName_internal(System.Threading.InternalThread,string)", ves_icall_System_Threading_Thread_SetName_internal)
ICALL(THREAD_21, "SetState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_SetState)
ICALL(THREAD_22, "Sleep_internal", ves_icall_System_Threading_Thread_Sleep_internal)
ICALL(THREAD_54, "SpinWait_nop", ves_icall_System_Threading_Thread_SpinWait_nop)
ICALL(THREAD_23, "Suspend_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Suspend)
ICALL(THREAD_25, "Thread_internal", ves_icall_System_Threading_Thread_Thread_internal)
ICALL(THREAD_26, "VolatileRead(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_27, "VolatileRead(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(THREAD_28, "VolatileRead(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_29, "VolatileRead(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_30, "VolatileRead(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_31, "VolatileRead(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_32, "VolatileRead(object&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_33, "VolatileRead(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_34, "VolatileRead(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(THREAD_35, "VolatileRead(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_36, "VolatileRead(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_37, "VolatileRead(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_38, "VolatileRead(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_39, "VolatileWrite(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_40, "VolatileWrite(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(THREAD_41, "VolatileWrite(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_42, "VolatileWrite(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_43, "VolatileWrite(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_44, "VolatileWrite(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_45, "VolatileWrite(object&,object)", ves_icall_System_Threading_Thread_VolatileWriteObject)
ICALL(THREAD_46, "VolatileWrite(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_47, "VolatileWrite(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(THREAD_48, "VolatileWrite(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_49, "VolatileWrite(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_50, "VolatileWrite(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_51, "VolatileWrite(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_9, "Yield", ves_icall_System_Threading_Thread_Yield)
ICALL(THREAD_52, "current_lcid()", ves_icall_System_Threading_Thread_current_lcid)

ICALL_TYPE(THREADP, "System.Threading.ThreadPool", THREADP_1)
ICALL(THREADP_1, "GetAvailableThreads", ves_icall_System_Threading_ThreadPool_GetAvailableThreads)
ICALL(THREADP_2, "GetMaxThreads", ves_icall_System_Threading_ThreadPool_GetMaxThreads)
ICALL(THREADP_3, "GetMinThreads", ves_icall_System_Threading_ThreadPool_GetMinThreads)
ICALL(THREADP_35, "SetMaxThreads", ves_icall_System_Threading_ThreadPool_SetMaxThreads)
ICALL(THREADP_4, "SetMinThreads", ves_icall_System_Threading_ThreadPool_SetMinThreads)
ICALL(THREADP_5, "pool_queue", icall_append_job)

ICALL_TYPE(VOLATILE, "System.Threading.Volatile", VOLATILE_28)
ICALL(VOLATILE_28, "Read(T&)", ves_icall_System_Threading_Volatile_Read_T)
ICALL(VOLATILE_1, "Read(bool&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_2, "Read(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_3, "Read(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(VOLATILE_4, "Read(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_5, "Read(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_6, "Read(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_7, "Read(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_8, "Read(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_9, "Read(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(VOLATILE_10, "Read(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_11, "Read(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_12, "Read(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_13, "Read(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_27, "Write(T&,T)", ves_icall_System_Threading_Volatile_Write_T)
ICALL(VOLATILE_14, "Write(bool&,bool)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_15, "Write(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_16, "Write(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(VOLATILE_17, "Write(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_18, "Write(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_19, "Write(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_20, "Write(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(VOLATILE_21, "Write(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_22, "Write(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(VOLATILE_23, "Write(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_24, "Write(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_25, "Write(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_26, "Write(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)

ICALL_TYPE(WAITH, "System.Threading.WaitHandle", WAITH_1)
ICALL(WAITH_1, "SignalAndWait_Internal", ves_icall_System_Threading_WaitHandle_SignalAndWait_Internal)
ICALL(WAITH_2, "WaitAll_internal", ves_icall_System_Threading_WaitHandle_WaitAll_internal)
ICALL(WAITH_3, "WaitAny_internal", ves_icall_System_Threading_WaitHandle_WaitAny_internal)
ICALL(WAITH_4, "WaitOne_internal", ves_icall_System_Threading_WaitHandle_WaitOne_internal)

ICALL_TYPE(TYPE, "System.Type", TYPE_1)
ICALL(TYPE_1, "EqualsInternal", ves_icall_System_Type_EqualsInternal)
ICALL(TYPE_2, "GetGenericParameterAttributes", ves_icall_Type_GetGenericParameterAttributes)
ICALL(TYPE_3, "GetGenericParameterConstraints_impl", ves_icall_Type_GetGenericParameterConstraints)
ICALL(TYPE_4, "GetGenericParameterPosition", ves_icall_Type_GetGenericParameterPosition)
ICALL(TYPE_5, "GetGenericTypeDefinition_impl", ves_icall_Type_GetGenericTypeDefinition_impl)
ICALL(TYPE_6, "GetInterfaceMapData", ves_icall_Type_GetInterfaceMapData)
ICALL(TYPE_7, "GetPacking", ves_icall_Type_GetPacking)
ICALL(TYPE_8, "GetTypeCode", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_9, "GetTypeCodeInternal", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_10, "IsArrayImpl", ves_icall_Type_IsArrayImpl)
ICALL(TYPE_11, "IsInstanceOfType", ves_icall_type_IsInstanceOfType)
ICALL(TYPE_12, "MakeGenericType", ves_icall_Type_MakeGenericType)
ICALL(TYPE_13, "MakePointerType", ves_icall_Type_MakePointerType)
ICALL(TYPE_14, "get_IsGenericInstance", ves_icall_Type_get_IsGenericInstance)
ICALL(TYPE_15, "get_IsGenericType", ves_icall_Type_get_IsGenericType)
ICALL(TYPE_16, "get_IsGenericTypeDefinition", ves_icall_Type_get_IsGenericTypeDefinition)
ICALL(TYPE_17, "internal_from_handle", ves_icall_type_from_handle)
ICALL(TYPE_18, "internal_from_name", ves_icall_type_from_name)
ICALL(TYPE_19, "make_array_type", ves_icall_Type_make_array_type)
ICALL(TYPE_20, "make_byref_type", ves_icall_Type_make_byref_type)
ICALL(TYPE_21, "type_is_assignable_from", ves_icall_type_is_assignable_from)
ICALL(TYPE_22, "type_is_subtype_of", ves_icall_type_is_subtype_of)

ICALL_TYPE(TYPEDR, "System.TypedReference", TYPEDR_1)
ICALL(TYPEDR_1, "ToObject",	mono_TypedReference_ToObject)
ICALL(TYPEDR_2, "ToObjectInternal",	mono_TypedReference_ToObjectInternal)

ICALL_TYPE(VALUET, "System.ValueType", VALUET_1)
ICALL(VALUET_1, "InternalEquals", ves_icall_System_ValueType_Equals)
ICALL(VALUET_2, "InternalGetHashCode", ves_icall_System_ValueType_InternalGetHashCode)

ICALL_TYPE(WEBIC, "System.Web.Util.ICalls", WEBIC_1)
ICALL(WEBIC_1, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(WEBIC_2, "GetMachineInstallDirectory", ves_icall_System_Web_Util_ICalls_get_machine_install_dir)
ICALL(WEBIC_3, "GetUnmanagedResourcesPtr", ves_icall_get_resources_ptr)

#ifndef DISABLE_COM
ICALL_TYPE(COMOBJ, "System.__ComObject", COMOBJ_1)
ICALL(COMOBJ_1, "CreateRCW", ves_icall_System_ComObject_CreateRCW)
ICALL(COMOBJ_2, "GetInterfaceInternal", ves_icall_System_ComObject_GetInterfaceInternal)
ICALL(COMOBJ_3, "ReleaseInterfaces", ves_icall_System_ComObject_ReleaseInterfaces)
#endif
	Icall_type_num
};

#undef ICALL_TYPE
#undef ICALL
#define ICALL_TYPE(id,name,firstic) {(Icall_ ## firstic)},
#define ICALL(id,name,func)
typedef struct {
	guint16 first_icall;
} IcallTypeDesc;

static const IcallTypeDesc
icall_type_descs [] = {
// #include "metadata/icall-def.h"
ICALL_TYPE(UNORM, "Mono.Globalization.Unicode.Normalization", UNORM_1)
ICALL(UNORM_1, "load_normalization_resource", load_normalization_resource)

#ifndef DISABLE_COM
ICALL_TYPE(COMPROX, "Mono.Interop.ComInteropProxy", COMPROX_1)
ICALL(COMPROX_1, "AddProxy", ves_icall_Mono_Interop_ComInteropProxy_AddProxy)
ICALL(COMPROX_2, "FindProxy", ves_icall_Mono_Interop_ComInteropProxy_FindProxy)
#endif

ICALL_TYPE(RUNTIME, "Mono.Runtime", RUNTIME_1)
ICALL(RUNTIME_1, "GetDisplayName", ves_icall_Mono_Runtime_GetDisplayName)
ICALL(RUNTIME_12, "GetNativeStackTrace", ves_icall_Mono_Runtime_GetNativeStackTrace)
ICALL(RUNTIME_13, "SetGCAllowSynchronousMajor", ves_icall_Mono_Runtime_SetGCAllowSynchronousMajor)

#ifndef PLATFORM_RO_FS
ICALL_TYPE(KPAIR, "Mono.Security.Cryptography.KeyPairPersistence", KPAIR_1)
ICALL(KPAIR_1, "_CanSecure", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_CanSecure)
ICALL(KPAIR_2, "_IsMachineProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsMachineProtected)
ICALL(KPAIR_3, "_IsUserProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsUserProtected)
ICALL(KPAIR_4, "_ProtectMachine", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectMachine)
ICALL(KPAIR_5, "_ProtectUser", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectUser)
#endif /* !PLATFORM_RO_FS */

ICALL_TYPE(ACTIV, "System.Activator", ACTIV_1)
ICALL(ACTIV_1, "CreateInstanceInternal", ves_icall_System_Activator_CreateInstanceInternal)

ICALL_TYPE(APPDOM, "System.AppDomain", APPDOM_1)
ICALL(APPDOM_1, "ExecuteAssembly", ves_icall_System_AppDomain_ExecuteAssembly)
ICALL(APPDOM_2, "GetAssemblies", ves_icall_System_AppDomain_GetAssemblies)
ICALL(APPDOM_3, "GetData", ves_icall_System_AppDomain_GetData)
ICALL(APPDOM_4, "InternalGetContext", ves_icall_System_AppDomain_InternalGetContext)
ICALL(APPDOM_5, "InternalGetDefaultContext", ves_icall_System_AppDomain_InternalGetDefaultContext)
ICALL(APPDOM_6, "InternalGetProcessGuid", ves_icall_System_AppDomain_InternalGetProcessGuid)
ICALL(APPDOM_7, "InternalIsFinalizingForUnload", ves_icall_System_AppDomain_InternalIsFinalizingForUnload)
ICALL(APPDOM_8, "InternalPopDomainRef", ves_icall_System_AppDomain_InternalPopDomainRef)
ICALL(APPDOM_9, "InternalPushDomainRef", ves_icall_System_AppDomain_InternalPushDomainRef)
ICALL(APPDOM_10, "InternalPushDomainRefByID", ves_icall_System_AppDomain_InternalPushDomainRefByID)
ICALL(APPDOM_11, "InternalSetContext", ves_icall_System_AppDomain_InternalSetContext)
ICALL(APPDOM_12, "InternalSetDomain", ves_icall_System_AppDomain_InternalSetDomain)
ICALL(APPDOM_13, "InternalSetDomainByID", ves_icall_System_AppDomain_InternalSetDomainByID)
ICALL(APPDOM_14, "InternalUnload", ves_icall_System_AppDomain_InternalUnload)
ICALL(APPDOM_15, "LoadAssembly", ves_icall_System_AppDomain_LoadAssembly)
ICALL(APPDOM_16, "LoadAssemblyRaw", ves_icall_System_AppDomain_LoadAssemblyRaw)
ICALL(APPDOM_17, "SetData", ves_icall_System_AppDomain_SetData)
ICALL(APPDOM_18, "createDomain", ves_icall_System_AppDomain_createDomain)
ICALL(APPDOM_19, "getCurDomain", ves_icall_System_AppDomain_getCurDomain)
ICALL(APPDOM_20, "getFriendlyName", ves_icall_System_AppDomain_getFriendlyName)
ICALL(APPDOM_21, "getRootDomain", ves_icall_System_AppDomain_getRootDomain)
ICALL(APPDOM_22, "getSetup", ves_icall_System_AppDomain_getSetup)

ICALL_TYPE(ARGI, "System.ArgIterator", ARGI_1)
ICALL(ARGI_1, "IntGetNextArg()",                  mono_ArgIterator_IntGetNextArg)
ICALL(ARGI_2, "IntGetNextArg(intptr)", mono_ArgIterator_IntGetNextArgT)
ICALL(ARGI_3, "IntGetNextArgType",                mono_ArgIterator_IntGetNextArgType)
ICALL(ARGI_4, "Setup",                            mono_ArgIterator_Setup)

ICALL_TYPE(ARRAY, "System.Array", ARRAY_1)
ICALL(ARRAY_1, "ClearInternal",    ves_icall_System_Array_ClearInternal)
ICALL(ARRAY_2, "Clone",            mono_array_clone)
ICALL(ARRAY_3, "CreateInstanceImpl",   ves_icall_System_Array_CreateInstanceImpl)
ICALL(ARRAY_14, "CreateInstanceImpl64",   ves_icall_System_Array_CreateInstanceImpl64)
ICALL(ARRAY_4, "FastCopy",         ves_icall_System_Array_FastCopy)
ICALL(ARRAY_5, "GetGenericValueImpl", ves_icall_System_Array_GetGenericValueImpl)
ICALL(ARRAY_6, "GetLength",        ves_icall_System_Array_GetLength)
ICALL(ARRAY_15, "GetLongLength",        ves_icall_System_Array_GetLongLength)
ICALL(ARRAY_7, "GetLowerBound",    ves_icall_System_Array_GetLowerBound)
ICALL(ARRAY_8, "GetRank",          ves_icall_System_Array_GetRank)
ICALL(ARRAY_9, "GetValue",         ves_icall_System_Array_GetValue)
ICALL(ARRAY_10, "GetValueImpl",     ves_icall_System_Array_GetValueImpl)
ICALL(ARRAY_11, "SetGenericValueImpl", ves_icall_System_Array_SetGenericValueImpl)
ICALL(ARRAY_12, "SetValue",         ves_icall_System_Array_SetValue)
ICALL(ARRAY_13, "SetValueImpl",     ves_icall_System_Array_SetValueImpl)

ICALL_TYPE(BUFFER, "System.Buffer", BUFFER_1)
ICALL(BUFFER_1, "BlockCopyInternal", ves_icall_System_Buffer_BlockCopyInternal)
ICALL(BUFFER_2, "ByteLengthInternal", ves_icall_System_Buffer_ByteLengthInternal)
ICALL(BUFFER_3, "GetByteInternal", ves_icall_System_Buffer_GetByteInternal)
ICALL(BUFFER_4, "SetByteInternal", ves_icall_System_Buffer_SetByteInternal)

ICALL_TYPE(CHAR, "System.Char", CHAR_1)
ICALL(CHAR_1, "GetDataTablePointers", ves_icall_System_Char_GetDataTablePointers)

ICALL_TYPE (COMPO_W, "System.ComponentModel.Win32Exception", COMPO_W_1)
ICALL (COMPO_W_1, "W32ErrorMessage", ves_icall_System_ComponentModel_Win32Exception_W32ErrorMessage)

ICALL_TYPE(DEFAULTC, "System.Configuration.DefaultConfig", DEFAULTC_1)
ICALL(DEFAULTC_1, "get_bundled_machine_config", get_bundled_machine_config)
ICALL(DEFAULTC_2, "get_machine_config_path", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)

/* Note that the below icall shares the same function as DefaultConfig uses */
ICALL_TYPE(INTCFGHOST, "System.Configuration.InternalConfigurationHost", INTCFGHOST_1)
ICALL(INTCFGHOST_1, "get_bundled_app_config", get_bundled_app_config)
ICALL(INTCFGHOST_2, "get_bundled_machine_config", get_bundled_machine_config)

ICALL_TYPE(CONSOLE, "System.ConsoleDriver", CONSOLE_1)
ICALL(CONSOLE_1, "InternalKeyAvailable", ves_icall_System_ConsoleDriver_InternalKeyAvailable )
ICALL(CONSOLE_2, "Isatty", ves_icall_System_ConsoleDriver_Isatty )
ICALL(CONSOLE_3, "SetBreak", ves_icall_System_ConsoleDriver_SetBreak )
ICALL(CONSOLE_4, "SetEcho", ves_icall_System_ConsoleDriver_SetEcho )
ICALL(CONSOLE_5, "TtySetup", ves_icall_System_ConsoleDriver_TtySetup )

ICALL_TYPE(CONVERT, "System.Convert", CONVERT_1)
ICALL(CONVERT_1, "InternalFromBase64CharArray", InternalFromBase64CharArray )
ICALL(CONVERT_2, "InternalFromBase64String", InternalFromBase64String )

ICALL_TYPE(TZONE, "System.CurrentSystemTimeZone", TZONE_1)
ICALL(TZONE_1, "GetTimeZoneData", ves_icall_System_CurrentSystemTimeZone_GetTimeZoneData)

ICALL_TYPE(DTIME, "System.DateTime", DTIME_1)
ICALL(DTIME_1, "GetNow", mono_100ns_datetime)
ICALL(DTIME_2, "GetTimeMonotonic", mono_100ns_ticks)

#ifndef DISABLE_DECIMAL
ICALL_TYPE(DECIMAL, "System.Decimal", DECIMAL_1)
ICALL(DECIMAL_1, "decimal2Int64", mono_decimal2Int64)
ICALL(DECIMAL_2, "decimal2UInt64", mono_decimal2UInt64)
ICALL(DECIMAL_3, "decimal2double", mono_decimal2double)
//ICALL(DECIMAL_4, "decimal2string", mono_decimal2string)
ICALL(DECIMAL_5, "decimalCompare", mono_decimalCompare)
ICALL(DECIMAL_6, "decimalDiv", mono_decimalDiv)
ICALL(DECIMAL_7, "decimalFloorAndTrunc", mono_decimalFloorAndTrunc)
ICALL(DECIMAL_8, "decimalIncr", mono_decimalIncr)
ICALL(DECIMAL_9, "decimalIntDiv", mono_decimalIntDiv)
ICALL(DECIMAL_10, "decimalMult", mono_decimalMult)
ICALL(DECIMAL_11, "decimalRound", mono_decimalRound)
ICALL(DECIMAL_12, "decimalSetExponent", mono_decimalSetExponent)
ICALL(DECIMAL_13, "double2decimal", mono_double2decimal) /* FIXME: wrong signature. */
ICALL(DECIMAL_14, "string2decimal", mono_string2decimal)
#endif

ICALL_TYPE(DELEGATE, "System.Delegate", DELEGATE_1)
ICALL(DELEGATE_1, "CreateDelegate_internal", ves_icall_System_Delegate_CreateDelegate_internal)
ICALL(DELEGATE_2, "SetMulticastInvoke", ves_icall_System_Delegate_SetMulticastInvoke)

ICALL_TYPE(DEBUGR, "System.Diagnostics.Debugger", DEBUGR_1)
ICALL(DEBUGR_1, "IsAttached_internal", ves_icall_System_Diagnostics_Debugger_IsAttached_internal)
ICALL(DEBUGR_2, "IsLogging", ves_icall_System_Diagnostics_Debugger_IsLogging)
ICALL(DEBUGR_3, "Log", ves_icall_System_Diagnostics_Debugger_Log)

ICALL_TYPE(TRACEL, "System.Diagnostics.DefaultTraceListener", TRACEL_1)
ICALL(TRACEL_1, "WriteWindowsDebugString", ves_icall_System_Diagnostics_DefaultTraceListener_WriteWindowsDebugString)

ICALL_TYPE(FILEV, "System.Diagnostics.FileVersionInfo", FILEV_1)
ICALL(FILEV_1, "GetVersionInfo_internal(string)", ves_icall_System_Diagnostics_FileVersionInfo_GetVersionInfo_internal)

#ifndef DISABLE_PROCESS_HANDLING
ICALL_TYPE(PERFCTR, "System.Diagnostics.PerformanceCounter", PERFCTR_1)
ICALL(PERFCTR_1, "FreeData", mono_perfcounter_free_data)
ICALL(PERFCTR_2, "GetImpl", mono_perfcounter_get_impl)
ICALL(PERFCTR_3, "GetSample", mono_perfcounter_get_sample)
ICALL(PERFCTR_4, "UpdateValue", mono_perfcounter_update_value)

ICALL_TYPE(PERFCTRCAT, "System.Diagnostics.PerformanceCounterCategory", PERFCTRCAT_1)
ICALL(PERFCTRCAT_1, "CategoryDelete", mono_perfcounter_category_del)
ICALL(PERFCTRCAT_2, "CategoryHelpInternal",   mono_perfcounter_category_help)
ICALL(PERFCTRCAT_3, "CounterCategoryExists", mono_perfcounter_category_exists)
ICALL(PERFCTRCAT_4, "Create",         mono_perfcounter_create)
ICALL(PERFCTRCAT_5, "GetCategoryNames", mono_perfcounter_category_names)
ICALL(PERFCTRCAT_6, "GetCounterNames", mono_perfcounter_counter_names)
ICALL(PERFCTRCAT_7, "GetInstanceNames", mono_perfcounter_instance_names)
ICALL(PERFCTRCAT_8, "InstanceExistsInternal", mono_perfcounter_instance_exists)

ICALL_TYPE(PROCESS, "System.Diagnostics.Process", PROCESS_1)
ICALL(PROCESS_1, "CreateProcess_internal(System.Diagnostics.ProcessStartInfo,intptr,intptr,intptr,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_CreateProcess_internal)
ICALL(PROCESS_2, "ExitCode_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitCode_internal)
ICALL(PROCESS_3, "ExitTime_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitTime_internal)
ICALL(PROCESS_4, "GetModules_internal(intptr)", ves_icall_System_Diagnostics_Process_GetModules_internal)
ICALL(PROCESS_5, "GetPid_internal()", ves_icall_System_Diagnostics_Process_GetPid_internal)
ICALL(PROCESS_5B, "GetPriorityClass(intptr,int&)", ves_icall_System_Diagnostics_Process_GetPriorityClass)
ICALL(PROCESS_5H, "GetProcessData", ves_icall_System_Diagnostics_Process_GetProcessData)
ICALL(PROCESS_6, "GetProcess_internal(int)", ves_icall_System_Diagnostics_Process_GetProcess_internal)
ICALL(PROCESS_7, "GetProcesses_internal()", ves_icall_System_Diagnostics_Process_GetProcesses_internal)
ICALL(PROCESS_8, "GetWorkingSet_internal(intptr,int&,int&)", ves_icall_System_Diagnostics_Process_GetWorkingSet_internal)
ICALL(PROCESS_9, "Kill_internal", ves_icall_System_Diagnostics_Process_Kill_internal)
ICALL(PROCESS_10, "ProcessName_internal(intptr)", ves_icall_System_Diagnostics_Process_ProcessName_internal)
ICALL(PROCESS_11, "Process_free_internal(intptr)", ves_icall_System_Diagnostics_Process_Process_free_internal)
ICALL(PROCESS_11B, "SetPriorityClass(intptr,int,int&)", ves_icall_System_Diagnostics_Process_SetPriorityClass)
ICALL(PROCESS_12, "SetWorkingSet_internal(intptr,int,int,bool)", ves_icall_System_Diagnostics_Process_SetWorkingSet_internal)
ICALL(PROCESS_13, "ShellExecuteEx_internal(System.Diagnostics.ProcessStartInfo,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_ShellExecuteEx_internal)
ICALL(PROCESS_14, "StartTime_internal(intptr)", ves_icall_System_Diagnostics_Process_StartTime_internal)
ICALL(PROCESS_14M, "Times", ves_icall_System_Diagnostics_Process_Times)
ICALL(PROCESS_15, "WaitForExit_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForExit_internal)
ICALL(PROCESS_16, "WaitForInputIdle_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForInputIdle_internal)

ICALL_TYPE (PROCESSHANDLE, "System.Diagnostics.Process/ProcessWaitHandle", PROCESSHANDLE_1)
ICALL (PROCESSHANDLE_1, "ProcessHandle_close(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_close)
ICALL (PROCESSHANDLE_2, "ProcessHandle_duplicate(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_duplicate)
#endif /* !DISABLE_PROCESS_HANDLING */

ICALL_TYPE(STOPWATCH, "System.Diagnostics.Stopwatch", STOPWATCH_1)
ICALL(STOPWATCH_1, "GetTimestamp", mono_100ns_ticks)

ICALL_TYPE(DOUBLE, "System.Double", DOUBLE_1)
ICALL(DOUBLE_1, "ParseImpl",    mono_double_ParseImpl)

ICALL_TYPE(ENUM, "System.Enum", ENUM_1)
ICALL(ENUM_1, "ToObject", ves_icall_System_Enum_ToObject)
ICALL(ENUM_5, "compare_value_to", ves_icall_System_Enum_compare_value_to)
ICALL(ENUM_4, "get_hashcode", ves_icall_System_Enum_get_hashcode)
ICALL(ENUM_3, "get_underlying_type", ves_icall_System_Enum_get_underlying_type)
ICALL(ENUM_2, "get_value", ves_icall_System_Enum_get_value)

ICALL_TYPE(ENV, "System.Environment", ENV_1)
ICALL(ENV_1, "Exit", ves_icall_System_Environment_Exit)
ICALL(ENV_2, "GetCommandLineArgs", mono_runtime_get_main_args)
ICALL(ENV_3, "GetEnvironmentVariableNames", ves_icall_System_Environment_GetEnvironmentVariableNames)
ICALL(ENV_4, "GetLogicalDrivesInternal", ves_icall_System_Environment_GetLogicalDrives )
ICALL(ENV_5, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(ENV_51, "GetNewLine", ves_icall_System_Environment_get_NewLine)
ICALL(ENV_6, "GetOSVersionString", ves_icall_System_Environment_GetOSVersionString)
ICALL(ENV_6a, "GetPageSize", mono_pagesize)
ICALL(ENV_7, "GetWindowsFolderPath", ves_icall_System_Environment_GetWindowsFolderPath)
ICALL(ENV_8, "InternalSetEnvironmentVariable", ves_icall_System_Environment_InternalSetEnvironmentVariable)
ICALL(ENV_9, "get_ExitCode", mono_environment_exitcode_get)
ICALL(ENV_10, "get_HasShutdownStarted", ves_icall_System_Environment_get_HasShutdownStarted)
ICALL(ENV_11, "get_MachineName", ves_icall_System_Environment_get_MachineName)
ICALL(ENV_13, "get_Platform", ves_icall_System_Environment_get_Platform)
ICALL(ENV_14, "get_ProcessorCount", mono_cpu_count)
ICALL(ENV_15, "get_TickCount", mono_msec_ticks)
ICALL(ENV_16, "get_UserName", ves_icall_System_Environment_get_UserName)
ICALL(ENV_16m, "internalBroadcastSettingChange", ves_icall_System_Environment_BroadcastSettingChange)
ICALL(ENV_17, "internalGetEnvironmentVariable", ves_icall_System_Environment_GetEnvironmentVariable)
ICALL(ENV_18, "internalGetGacPath", ves_icall_System_Environment_GetGacPath)
ICALL(ENV_19, "internalGetHome", ves_icall_System_Environment_InternalGetHome)
ICALL(ENV_20, "set_ExitCode", mono_environment_exitcode_set)

ICALL_TYPE(GC, "System.GC", GC_0)
ICALL(GC_0, "CollectionCount", mono_gc_collection_count)
ICALL(GC_0a, "GetGeneration", mono_gc_get_generation)
ICALL(GC_1, "GetTotalMemory", ves_icall_System_GC_GetTotalMemory)
ICALL(GC_2, "InternalCollect", ves_icall_System_GC_InternalCollect)
ICALL(GC_3, "KeepAlive", ves_icall_System_GC_KeepAlive)
ICALL(GC_4, "ReRegisterForFinalize", ves_icall_System_GC_ReRegisterForFinalize)
ICALL(GC_4a, "RecordPressure", mono_gc_add_memory_pressure)
ICALL(GC_5, "SuppressFinalize", ves_icall_System_GC_SuppressFinalize)
ICALL(GC_6, "WaitForPendingFinalizers", ves_icall_System_GC_WaitForPendingFinalizers)
ICALL(GC_7, "get_MaxGeneration", mono_gc_max_generation)
ICALL(GC_9, "get_ephemeron_tombstone", ves_icall_System_GC_get_ephemeron_tombstone)
ICALL(GC_8, "register_ephemeron_array", ves_icall_System_GC_register_ephemeron_array)

ICALL_TYPE(COMPINF, "System.Globalization.CompareInfo", COMPINF_1)
ICALL(COMPINF_1, "assign_sortkey(object,string,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_assign_sortkey)
ICALL(COMPINF_2, "construct_compareinfo(string)", ves_icall_System_Globalization_CompareInfo_construct_compareinfo)
ICALL(COMPINF_3, "free_internal_collator()", ves_icall_System_Globalization_CompareInfo_free_internal_collator)
ICALL(COMPINF_4, "internal_compare(string,int,int,string,int,int,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_internal_compare)
ICALL(COMPINF_5, "internal_index(string,int,int,char,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index_char)
ICALL(COMPINF_6, "internal_index(string,int,int,string,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index)

ICALL_TYPE(CULINF, "System.Globalization.CultureInfo", CULINF_2)
ICALL(CULINF_2, "construct_datetime_format", ves_icall_System_Globalization_CultureInfo_construct_datetime_format)
ICALL(CULINF_4, "construct_internal_locale_from_current_locale", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_current_locale)
ICALL(CULINF_5, "construct_internal_locale_from_lcid", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_lcid)
ICALL(CULINF_6, "construct_internal_locale_from_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_name)
ICALL(CULINF_7, "construct_internal_locale_from_specific_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_specific_name)
ICALL(CULINF_8, "construct_number_format", ves_icall_System_Globalization_CultureInfo_construct_number_format)
ICALL(CULINF_9, "internal_get_cultures", ves_icall_System_Globalization_CultureInfo_internal_get_cultures)
//ICALL(CULINF_10, "internal_is_lcid_neutral", ves_icall_System_Globalization_CultureInfo_internal_is_lcid_neutral)

ICALL_TYPE(REGINF, "System.Globalization.RegionInfo", REGINF_1)
ICALL(REGINF_1, "construct_internal_region_from_lcid", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_lcid)
ICALL(REGINF_2, "construct_internal_region_from_name", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_name)

#ifndef PLATFORM_NO_DRIVEINFO
ICALL_TYPE(IODRIVEINFO, "System.IO.DriveInfo", IODRIVEINFO_1)
ICALL(IODRIVEINFO_1, "GetDiskFreeSpaceInternal", ves_icall_System_IO_DriveInfo_GetDiskFreeSpace)
ICALL(IODRIVEINFO_2, "GetDriveFormat", ves_icall_System_IO_DriveInfo_GetDriveFormat)
ICALL(IODRIVEINFO_3, "GetDriveTypeInternal", ves_icall_System_IO_DriveInfo_GetDriveType)
#endif

ICALL_TYPE(FAMW, "System.IO.FAMWatcher", FAMW_1)
ICALL(FAMW_1, "InternalFAMNextEvent", ves_icall_System_IO_FAMW_InternalFAMNextEvent)

ICALL_TYPE(FILEW, "System.IO.FileSystemWatcher", FILEW_4)
ICALL(FILEW_4, "InternalSupportsFSW", ves_icall_System_IO_FSW_SupportsFSW)

ICALL_TYPE(INOW, "System.IO.InotifyWatcher", INOW_1)
ICALL(INOW_1, "AddWatch", ves_icall_System_IO_InotifyWatcher_AddWatch)
ICALL(INOW_2, "GetInotifyInstance", ves_icall_System_IO_InotifyWatcher_GetInotifyInstance)
ICALL(INOW_3, "RemoveWatch", ves_icall_System_IO_InotifyWatcher_RemoveWatch)

#if defined (TARGET_IOS) || defined (TARGET_ANDROID)
ICALL_TYPE(MMAPIMPL, "System.IO.MemoryMappedFiles.MemoryMapImpl", MMAPIMPL_1)
ICALL(MMAPIMPL_1, "mono_filesize_from_fd", mono_filesize_from_fd)
ICALL(MMAPIMPL_2, "mono_filesize_from_path", mono_filesize_from_path)
#endif


ICALL_TYPE(MONOIO, "System.IO.MonoIO", MONOIO_1)
ICALL(MONOIO_1, "Close(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Close)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_2, "CopyFile(string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CopyFile)
ICALL(MONOIO_3, "CreateDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CreateDirectory)
ICALL(MONOIO_4, "CreatePipe(intptr&,intptr&)", ves_icall_System_IO_MonoIO_CreatePipe)
ICALL(MONOIO_5, "DeleteFile(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_DeleteFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_34, "DuplicateHandle", ves_icall_System_IO_MonoIO_DuplicateHandle)
ICALL(MONOIO_37, "FindClose", ves_icall_System_IO_MonoIO_FindClose)
ICALL(MONOIO_35, "FindFirst", ves_icall_System_IO_MonoIO_FindFirst)
ICALL(MONOIO_36, "FindNext", ves_icall_System_IO_MonoIO_FindNext)
ICALL(MONOIO_6, "Flush(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Flush)
ICALL(MONOIO_7, "GetCurrentDirectory(System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetCurrentDirectory)
ICALL(MONOIO_8, "GetFileAttributes(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileAttributes)
ICALL(MONOIO_9, "GetFileStat(string,System.IO.MonoIOStat&,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileStat)
ICALL(MONOIO_10, "GetFileSystemEntries", ves_icall_System_IO_MonoIO_GetFileSystemEntries)
ICALL(MONOIO_11, "GetFileType(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileType)
ICALL(MONOIO_12, "GetLength(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_13, "GetTempPath(string&)", ves_icall_System_IO_MonoIO_GetTempPath)
ICALL(MONOIO_14, "Lock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Lock)
ICALL(MONOIO_15, "MoveFile(string,string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_MoveFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_16, "Open(string,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.IO.FileOptions,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Open)
ICALL(MONOIO_17, "Read(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Read)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_18, "RemoveDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_RemoveDirectory)
ICALL(MONOIO_18M, "ReplaceFile(string,string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_ReplaceFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_19, "Seek(intptr,long,System.IO.SeekOrigin,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Seek)
ICALL(MONOIO_20, "SetCurrentDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetCurrentDirectory)
ICALL(MONOIO_21, "SetFileAttributes(string,System.IO.FileAttributes,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileAttributes)
ICALL(MONOIO_22, "SetFileTime(intptr,long,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileTime)
ICALL(MONOIO_23, "SetLength(intptr,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_24, "Unlock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Unlock)
#endif
ICALL(MONOIO_25, "Write(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Write)
ICALL(MONOIO_26, "get_AltDirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_AltDirectorySeparatorChar)
ICALL(MONOIO_27, "get_ConsoleError", ves_icall_System_IO_MonoIO_get_ConsoleError)
ICALL(MONOIO_28, "get_ConsoleInput", ves_icall_System_IO_MonoIO_get_ConsoleInput)
ICALL(MONOIO_29, "get_ConsoleOutput", ves_icall_System_IO_MonoIO_get_ConsoleOutput)
ICALL(MONOIO_30, "get_DirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_DirectorySeparatorChar)
ICALL(MONOIO_31, "get_InvalidPathChars", ves_icall_System_IO_MonoIO_get_InvalidPathChars)
ICALL(MONOIO_32, "get_PathSeparator", ves_icall_System_IO_MonoIO_get_PathSeparator)
ICALL(MONOIO_33, "get_VolumeSeparatorChar", ves_icall_System_IO_MonoIO_get_VolumeSeparatorChar)

ICALL_TYPE(IOPATH, "System.IO.Path", IOPATH_1)
ICALL(IOPATH_1, "get_temp_path", ves_icall_System_IO_get_temp_path)

ICALL_TYPE(MATH, "System.Math", MATH_1)
ICALL(MATH_1, "Acos", ves_icall_System_Math_Acos)
ICALL(MATH_2, "Asin", ves_icall_System_Math_Asin)
ICALL(MATH_3, "Atan", ves_icall_System_Math_Atan)
ICALL(MATH_4, "Atan2", ves_icall_System_Math_Atan2)
ICALL(MATH_5, "Cos", ves_icall_System_Math_Cos)
ICALL(MATH_6, "Cosh", ves_icall_System_Math_Cosh)
ICALL(MATH_7, "Exp", ves_icall_System_Math_Exp)
ICALL(MATH_8, "Floor", ves_icall_System_Math_Floor)
ICALL(MATH_9, "Log", ves_icall_System_Math_Log)
ICALL(MATH_10, "Log10", ves_icall_System_Math_Log10)
ICALL(MATH_11, "Pow", ves_icall_System_Math_Pow)
ICALL(MATH_12, "Round", ves_icall_System_Math_Round)
ICALL(MATH_13, "Round2", ves_icall_System_Math_Round2)
ICALL(MATH_14, "Sin", ves_icall_System_Math_Sin)
ICALL(MATH_15, "Sinh", ves_icall_System_Math_Sinh)
ICALL(MATH_16, "Sqrt", ves_icall_System_Math_Sqrt)
ICALL(MATH_17, "Tan", ves_icall_System_Math_Tan)
ICALL(MATH_18, "Tanh", ves_icall_System_Math_Tanh)

ICALL_TYPE(MCATTR, "System.MonoCustomAttrs", MCATTR_1)
ICALL(MCATTR_1, "GetCustomAttributesDataInternal", mono_reflection_get_custom_attrs_data)
ICALL(MCATTR_2, "GetCustomAttributesInternal", custom_attrs_get_by_type)
ICALL(MCATTR_3, "IsDefinedInternal", custom_attrs_defined_internal)

ICALL_TYPE(MENUM, "System.MonoEnumInfo", MENUM_1)
ICALL(MENUM_1, "get_enum_info", ves_icall_get_enum_info)

ICALL_TYPE(MTYPE, "System.MonoType", MTYPE_1)
ICALL(MTYPE_1, "GetArrayRank", ves_icall_MonoType_GetArrayRank)
ICALL(MTYPE_2, "GetConstructors", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_3, "GetConstructors_internal", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_4, "GetCorrespondingInflatedConstructor", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_5, "GetCorrespondingInflatedMethod", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_6, "GetElementType", ves_icall_MonoType_GetElementType)
ICALL(MTYPE_7, "GetEvents_internal", ves_icall_Type_GetEvents_internal)
ICALL(MTYPE_8, "GetField", ves_icall_Type_GetField)
ICALL(MTYPE_9, "GetFields_internal", ves_icall_Type_GetFields_internal)
ICALL(MTYPE_10, "GetGenericArguments", ves_icall_MonoType_GetGenericArguments)
ICALL(MTYPE_11, "GetInterfaces", ves_icall_Type_GetInterfaces)
ICALL(MTYPE_12, "GetMethodsByName", ves_icall_Type_GetMethodsByName)
ICALL(MTYPE_13, "GetNestedType", ves_icall_Type_GetNestedType)
ICALL(MTYPE_14, "GetNestedTypes", ves_icall_Type_GetNestedTypes)
ICALL(MTYPE_15, "GetPropertiesByName", ves_icall_Type_GetPropertiesByName)
ICALL(MTYPE_16, "InternalGetEvent", ves_icall_MonoType_GetEvent)
ICALL(MTYPE_17, "IsByRefImpl", ves_icall_type_isbyref)
ICALL(MTYPE_18, "IsCOMObjectImpl", ves_icall_type_iscomobject)
ICALL(MTYPE_19, "IsPointerImpl", ves_icall_type_ispointer)
ICALL(MTYPE_20, "IsPrimitiveImpl", ves_icall_type_isprimitive)
ICALL(MTYPE_21, "getFullName", ves_icall_System_MonoType_getFullName)
ICALL(MTYPE_22, "get_Assembly", ves_icall_MonoType_get_Assembly)
ICALL(MTYPE_23, "get_BaseType", ves_icall_get_type_parent)
ICALL(MTYPE_24, "get_DeclaringMethod", ves_icall_MonoType_get_DeclaringMethod)
ICALL(MTYPE_25, "get_DeclaringType", ves_icall_MonoType_get_DeclaringType)
ICALL(MTYPE_26, "get_IsGenericParameter", ves_icall_MonoType_get_IsGenericParameter)
ICALL(MTYPE_27, "get_Module", ves_icall_MonoType_get_Module)
ICALL(MTYPE_28, "get_Name", ves_icall_MonoType_get_Name)
ICALL(MTYPE_29, "get_Namespace", ves_icall_MonoType_get_Namespace)
ICALL(MTYPE_31, "get_attributes", ves_icall_get_attributes)
ICALL(MTYPE_33, "get_core_clr_security_level", vell_icall_MonoType_get_core_clr_security_level)
ICALL(MTYPE_32, "type_from_obj", mono_type_type_from_obj)

#ifndef DISABLE_SOCKETS
ICALL_TYPE(NDNS, "System.Net.Dns", NDNS_1)
ICALL(NDNS_1, "GetHostByAddr_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByAddr_internal)
ICALL(NDNS_2, "GetHostByName_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByName_internal)
ICALL(NDNS_3, "GetHostName_internal(string&)", ves_icall_System_Net_Dns_GetHostName_internal)

ICALL_TYPE(SOCK, "System.Net.Sockets.Socket", SOCK_1)
ICALL(SOCK_1, "Accept_internal(intptr,int&,bool)", ves_icall_System_Net_Sockets_Socket_Accept_internal)
ICALL(SOCK_2, "Available_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Available_internal)
ICALL(SOCK_3, "Bind_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Bind_internal)
ICALL(SOCK_4, "Blocking_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Blocking_internal)
ICALL(SOCK_5, "Close_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Close_internal)
ICALL(SOCK_6, "Connect_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Connect_internal)
ICALL (SOCK_6a, "Disconnect_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Disconnect_internal)
ICALL(SOCK_7, "GetSocketOption_arr_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,byte[]&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_arr_internal)
ICALL(SOCK_8, "GetSocketOption_obj_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_obj_internal)
ICALL(SOCK_9, "Listen_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_Listen_internal)
ICALL(SOCK_10, "LocalEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_LocalEndPoint_internal)
ICALL(SOCK_11, "Poll_internal", ves_icall_System_Net_Sockets_Socket_Poll_internal)
ICALL(SOCK_11a, "Receive_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_array_internal)
ICALL(SOCK_12, "Receive_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_internal)
ICALL(SOCK_13, "RecvFrom_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress&,int&)", ves_icall_System_Net_Sockets_Socket_RecvFrom_internal)
ICALL(SOCK_14, "RemoteEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_RemoteEndPoint_internal)
ICALL(SOCK_15, "Select_internal(System.Net.Sockets.Socket[]&,int,int&)", ves_icall_System_Net_Sockets_Socket_Select_internal)
ICALL(SOCK_15a, "SendFile(intptr,string,byte[],byte[],System.Net.Sockets.TransmitFileOptions)", ves_icall_System_Net_Sockets_Socket_SendFile)
ICALL(SOCK_16, "SendTo_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_SendTo_internal)
ICALL(SOCK_16a, "Send_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_array_internal)
ICALL(SOCK_17, "Send_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_internal)
ICALL(SOCK_18, "SetSocketOption_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object,byte[],int,int&)", ves_icall_System_Net_Sockets_Socket_SetSocketOption_internal)
ICALL(SOCK_19, "Shutdown_internal(intptr,System.Net.Sockets.SocketShutdown,int&)", ves_icall_System_Net_Sockets_Socket_Shutdown_internal)
ICALL(SOCK_20, "Socket_internal(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType,int&)", ves_icall_System_Net_Sockets_Socket_Socket_internal)
ICALL(SOCK_21, "WSAIoctl(intptr,int,byte[],byte[],int&)", ves_icall_System_Net_Sockets_Socket_WSAIoctl)
ICALL(SOCK_21a, "cancel_blocking_socket_operation", icall_cancel_blocking_socket_operation)
ICALL(SOCK_22, "socket_pool_queue", icall_append_io_job)

ICALL_TYPE(SOCKEX, "System.Net.Sockets.SocketException", SOCKEX_1)
ICALL(SOCKEX_1, "WSAGetLastError_internal", ves_icall_System_Net_Sockets_SocketException_WSAGetLastError_internal)
#endif /* !DISABLE_SOCKETS */

ICALL_TYPE(NUMBER_FORMATTER, "System.NumberFormatter", NUMBER_FORMATTER_1)
ICALL(NUMBER_FORMATTER_1, "GetFormatterTables", ves_icall_System_NumberFormatter_GetFormatterTables)

ICALL_TYPE(OBJ, "System.Object", OBJ_1)
ICALL(OBJ_1, "GetType", ves_icall_System_Object_GetType)
ICALL(OBJ_2, "InternalGetHashCode", mono_object_hash)
ICALL(OBJ_3, "MemberwiseClone", ves_icall_System_Object_MemberwiseClone)
ICALL(OBJ_4, "obj_address", ves_icall_System_Object_obj_address)

ICALL_TYPE(ASSEM, "System.Reflection.Assembly", ASSEM_1)
ICALL(ASSEM_1, "FillName", ves_icall_System_Reflection_Assembly_FillName)
ICALL(ASSEM_2, "GetCallingAssembly", ves_icall_System_Reflection_Assembly_GetCallingAssembly)
ICALL(ASSEM_3, "GetEntryAssembly", ves_icall_System_Reflection_Assembly_GetEntryAssembly)
ICALL(ASSEM_4, "GetExecutingAssembly", ves_icall_System_Reflection_Assembly_GetExecutingAssembly)
ICALL(ASSEM_5, "GetFilesInternal", ves_icall_System_Reflection_Assembly_GetFilesInternal)
ICALL(ASSEM_6, "GetManifestModuleInternal", ves_icall_System_Reflection_Assembly_GetManifestModuleInternal)
ICALL(ASSEM_7, "GetManifestResourceInfoInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInfoInternal)
ICALL(ASSEM_8, "GetManifestResourceInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInternal)
ICALL(ASSEM_9, "GetManifestResourceNames", ves_icall_System_Reflection_Assembly_GetManifestResourceNames)
ICALL(ASSEM_10, "GetModulesInternal", ves_icall_System_Reflection_Assembly_GetModulesInternal)
//ICALL(ASSEM_11, "GetNamespaces", ves_icall_System_Reflection_Assembly_GetNamespaces)
ICALL(ASSEM_12, "GetReferencedAssemblies", ves_icall_System_Reflection_Assembly_GetReferencedAssemblies)
ICALL(ASSEM_13, "GetTypes", ves_icall_System_Reflection_Assembly_GetTypes)
ICALL(ASSEM_14, "InternalGetAssemblyName", ves_icall_System_Reflection_Assembly_InternalGetAssemblyName)
ICALL(ASSEM_15, "InternalGetType", ves_icall_System_Reflection_Assembly_InternalGetType)
ICALL(ASSEM_16, "InternalImageRuntimeVersion", ves_icall_System_Reflection_Assembly_InternalImageRuntimeVersion)
ICALL(ASSEM_17, "LoadFrom", ves_icall_System_Reflection_Assembly_LoadFrom)
ICALL(ASSEM_18, "LoadPermissions", ves_icall_System_Reflection_Assembly_LoadPermissions)
	/*
	 * Private icalls for the Mono Debugger
	 */
ICALL(ASSEM_19, "MonoDebugger_GetMethodToken", ves_icall_MonoDebugger_GetMethodToken)

	/* normal icalls again */
ICALL(ASSEM_20, "get_EntryPoint", ves_icall_System_Reflection_Assembly_get_EntryPoint)
ICALL(ASSEM_21, "get_ReflectionOnly", ves_icall_System_Reflection_Assembly_get_ReflectionOnly)
ICALL(ASSEM_22, "get_code_base", ves_icall_System_Reflection_Assembly_get_code_base)
ICALL(ASSEM_23, "get_fullname", ves_icall_System_Reflection_Assembly_get_fullName)
ICALL(ASSEM_24, "get_global_assembly_cache", ves_icall_System_Reflection_Assembly_get_global_assembly_cache)
ICALL(ASSEM_25, "get_location", ves_icall_System_Reflection_Assembly_get_location)
ICALL(ASSEM_26, "load_with_partial_name", ves_icall_System_Reflection_Assembly_load_with_partial_name)

ICALL_TYPE(ASSEMN, "System.Reflection.AssemblyName", ASSEMN_1)
ICALL(ASSEMN_1, "ParseName", ves_icall_System_Reflection_AssemblyName_ParseName)
ICALL(ASSEMN_2, "get_public_token", mono_digest_get_public_token)

ICALL_TYPE(CATTR_DATA, "System.Reflection.CustomAttributeData", CATTR_DATA_1)
ICALL(CATTR_DATA_1, "ResolveArgumentsInternal", mono_reflection_resolve_custom_attribute_data)

ICALL_TYPE(ASSEMB, "System.Reflection.Emit.AssemblyBuilder", ASSEMB_1)
ICALL(ASSEMB_1, "InternalAddModule", mono_image_load_module_dynamic)
ICALL(ASSEMB_2, "basic_init", mono_image_basic_init)

ICALL_TYPE(CATTRB, "System.Reflection.Emit.CustomAttributeBuilder", CATTRB_1)
ICALL(CATTRB_1, "GetBlob", mono_reflection_get_custom_attrs_blob)

#ifndef DISABLE_REFLECTION_EMIT
ICALL_TYPE(DERIVEDTYPE, "System.Reflection.Emit.DerivedType", DERIVEDTYPE_1)
ICALL(DERIVEDTYPE_1, "create_unmanaged_type", mono_reflection_create_unmanaged_type)
#endif

ICALL_TYPE(DYNM, "System.Reflection.Emit.DynamicMethod", DYNM_1)
ICALL(DYNM_1, "create_dynamic_method", mono_reflection_create_dynamic_method)

ICALL_TYPE(ENUMB, "System.Reflection.Emit.EnumBuilder", ENUMB_1)
ICALL(ENUMB_1, "setup_enum_type", ves_icall_EnumBuilder_setup_enum_type)

ICALL_TYPE(GPARB, "System.Reflection.Emit.GenericTypeParameterBuilder", GPARB_1)
ICALL(GPARB_1, "initialize", mono_reflection_initialize_generic_parameter)

ICALL_TYPE(METHODB, "System.Reflection.Emit.MethodBuilder", METHODB_1)
ICALL(METHODB_1, "MakeGenericMethod", mono_reflection_bind_generic_method_parameters)

ICALL_TYPE(MODULEB, "System.Reflection.Emit.ModuleBuilder", MODULEB_8)
ICALL(MODULEB_8, "RegisterToken", ves_icall_ModuleBuilder_RegisterToken)
ICALL(MODULEB_1, "WriteToFile", ves_icall_ModuleBuilder_WriteToFile)
ICALL(MODULEB_2, "basic_init", mono_image_module_basic_init)
ICALL(MODULEB_3, "build_metadata", ves_icall_ModuleBuilder_build_metadata)
ICALL(MODULEB_4, "create_modified_type", ves_icall_ModuleBuilder_create_modified_type)
ICALL(MODULEB_5, "getMethodToken", ves_icall_ModuleBuilder_getMethodToken)
ICALL(MODULEB_6, "getToken", ves_icall_ModuleBuilder_getToken)
ICALL(MODULEB_7, "getUSIndex", mono_image_insert_string)
ICALL(MODULEB_9, "set_wrappers_type", mono_image_set_wrappers_type)

ICALL_TYPE(SIGH, "System.Reflection.Emit.SignatureHelper", SIGH_1)
ICALL(SIGH_1, "get_signature_field", mono_reflection_sighelper_get_signature_field)
ICALL(SIGH_2, "get_signature_local", mono_reflection_sighelper_get_signature_local)

ICALL_TYPE(TYPEB, "System.Reflection.Emit.TypeBuilder", TYPEB_1)
ICALL(TYPEB_1, "create_generic_class", mono_reflection_create_generic_class)
ICALL(TYPEB_2, "create_internal_class", mono_reflection_create_internal_class)
ICALL(TYPEB_3, "create_runtime_class", mono_reflection_create_runtime_class)
ICALL(TYPEB_4, "get_IsGenericParameter", ves_icall_TypeBuilder_get_IsGenericParameter)
ICALL(TYPEB_5, "get_event_info", mono_reflection_event_builder_get_event_info)
ICALL(TYPEB_6, "setup_generic_class", mono_reflection_setup_generic_class)
ICALL(TYPEB_7, "setup_internal_class", mono_reflection_setup_internal_class)

ICALL_TYPE(FIELDI, "System.Reflection.FieldInfo", FILEDI_1)
ICALL(FILEDI_1, "GetTypeModifiers", ves_icall_System_Reflection_FieldInfo_GetTypeModifiers)
ICALL(FILEDI_2, "get_marshal_info", ves_icall_System_Reflection_FieldInfo_get_marshal_info)
ICALL(FILEDI_3, "internal_from_handle_type", ves_icall_System_Reflection_FieldInfo_internal_from_handle_type)

ICALL_TYPE(MEMBERI, "System.Reflection.MemberInfo", MEMBERI_1)
ICALL(MEMBERI_1, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MBASE, "System.Reflection.MethodBase", MBASE_1)
ICALL(MBASE_1, "GetCurrentMethod", ves_icall_GetCurrentMethod)
ICALL(MBASE_2, "GetMethodBodyInternal", ves_icall_System_Reflection_MethodBase_GetMethodBodyInternal)
ICALL(MBASE_3, "GetMethodFromHandleInternal", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternal)
ICALL(MBASE_4, "GetMethodFromHandleInternalType", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternalType)

ICALL_TYPE(MODULE, "System.Reflection.Module", MODULE_1)
ICALL(MODULE_1, "Close", ves_icall_System_Reflection_Module_Close)
ICALL(MODULE_2, "GetGlobalType", ves_icall_System_Reflection_Module_GetGlobalType)
ICALL(MODULE_3, "GetGuidInternal", ves_icall_System_Reflection_Module_GetGuidInternal)
ICALL(MODULE_14, "GetHINSTANCE", ves_icall_System_Reflection_Module_GetHINSTANCE)
ICALL(MODULE_4, "GetMDStreamVersion", ves_icall_System_Reflection_Module_GetMDStreamVersion)
ICALL(MODULE_5, "GetPEKind", ves_icall_System_Reflection_Module_GetPEKind)
ICALL(MODULE_6, "InternalGetTypes", ves_icall_System_Reflection_Module_InternalGetTypes)
ICALL(MODULE_7, "ResolveFieldToken", ves_icall_System_Reflection_Module_ResolveFieldToken)
ICALL(MODULE_8, "ResolveMemberToken", ves_icall_System_Reflection_Module_ResolveMemberToken)
ICALL(MODULE_9, "ResolveMethodToken", ves_icall_System_Reflection_Module_ResolveMethodToken)
ICALL(MODULE_10, "ResolveSignature", ves_icall_System_Reflection_Module_ResolveSignature)
ICALL(MODULE_11, "ResolveStringToken", ves_icall_System_Reflection_Module_ResolveStringToken)
ICALL(MODULE_12, "ResolveTypeToken", ves_icall_System_Reflection_Module_ResolveTypeToken)
ICALL(MODULE_13, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MCMETH, "System.Reflection.MonoCMethod", MCMETH_1)
ICALL(MCMETH_1, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MCMETH_2, "InternalInvoke", ves_icall_InternalInvoke)

ICALL_TYPE(MEVIN, "System.Reflection.MonoEventInfo", MEVIN_1)
ICALL(MEVIN_1, "get_event_info", ves_icall_get_event_info)

ICALL_TYPE(MFIELD, "System.Reflection.MonoField", MFIELD_1)
ICALL(MFIELD_1, "GetFieldOffset", ves_icall_MonoField_GetFieldOffset)
ICALL(MFIELD_2, "GetParentType", ves_icall_MonoField_GetParentType)
ICALL(MFIELD_5, "GetRawConstantValue", ves_icall_MonoField_GetRawConstantValue)
ICALL(MFIELD_3, "GetValueInternal", ves_icall_MonoField_GetValueInternal)
ICALL(MFIELD_6, "ResolveType", ves_icall_MonoField_ResolveType)
ICALL(MFIELD_4, "SetValueInternal", ves_icall_MonoField_SetValueInternal)

ICALL_TYPE(MGENCM, "System.Reflection.MonoGenericCMethod", MGENCM_1)
ICALL(MGENCM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MGENCL, "System.Reflection.MonoGenericClass", MGENCL_5)
ICALL(MGENCL_5, "initialize", mono_reflection_generic_class_initialize)
ICALL(MGENCL_6, "register_with_runtime", mono_reflection_register_with_runtime)

/* note this is the same as above: unify */
ICALL_TYPE(MGENM, "System.Reflection.MonoGenericMethod", MGENM_1)
ICALL(MGENM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MMETH, "System.Reflection.MonoMethod", MMETH_1)
ICALL(MMETH_1, "GetDllImportAttribute", ves_icall_MonoMethod_GetDllImportAttribute)
ICALL(MMETH_2, "GetGenericArguments", ves_icall_MonoMethod_GetGenericArguments)
ICALL(MMETH_3, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MMETH_4, "InternalInvoke", ves_icall_InternalInvoke)
ICALL(MMETH_5, "MakeGenericMethod_impl", mono_reflection_bind_generic_method_parameters)
ICALL(MMETH_6, "get_IsGenericMethod", ves_icall_MonoMethod_get_IsGenericMethod)
ICALL(MMETH_7, "get_IsGenericMethodDefinition", ves_icall_MonoMethod_get_IsGenericMethodDefinition)
ICALL(MMETH_8, "get_base_method", ves_icall_MonoMethod_get_base_method)
ICALL(MMETH_9, "get_name", ves_icall_MonoMethod_get_name)

ICALL_TYPE(MMETHI, "System.Reflection.MonoMethodInfo", MMETHI_4)
ICALL(MMETHI_4, "get_method_attributes", vell_icall_get_method_attributes)
ICALL(MMETHI_1, "get_method_info", ves_icall_get_method_info)
ICALL(MMETHI_2, "get_parameter_info", ves_icall_get_parameter_info)
ICALL(MMETHI_3, "get_retval_marshal", ves_icall_System_MonoMethodInfo_get_retval_marshal)

ICALL_TYPE(MPROPI, "System.Reflection.MonoPropertyInfo", MPROPI_1)
ICALL(MPROPI_1, "GetTypeModifiers", property_info_get_type_modifiers)
ICALL(MPROPI_3, "get_default_value", property_info_get_default_value)
ICALL(MPROPI_2, "get_property_info", ves_icall_get_property_info)

ICALL_TYPE(PARAMI, "System.Reflection.ParameterInfo", PARAMI_1)
ICALL(PARAMI_1, "GetMetadataToken", mono_reflection_get_token)
ICALL(PARAMI_2, "GetTypeModifiers", param_info_get_type_modifiers)

ICALL_TYPE(RUNH, "System.Runtime.CompilerServices.RuntimeHelpers", RUNH_1)
ICALL(RUNH_1, "GetObjectValue", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetObjectValue)
	 /* REMOVEME: no longer needed, just so we dont break things when not needed */
ICALL(RUNH_2, "GetOffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)
ICALL(RUNH_3, "InitializeArray", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_InitializeArray)
ICALL(RUNH_4, "RunClassConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunClassConstructor)
ICALL(RUNH_5, "RunModuleConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunModuleConstructor)
ICALL(RUNH_5h, "SufficientExecutionStack", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_SufficientExecutionStack)
ICALL(RUNH_6, "get_OffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)

ICALL_TYPE(GCH, "System.Runtime.InteropServices.GCHandle", GCH_1)
ICALL(GCH_1, "CheckCurrentDomain", GCHandle_CheckCurrentDomain)
ICALL(GCH_2, "FreeHandle", ves_icall_System_GCHandle_FreeHandle)
ICALL(GCH_3, "GetAddrOfPinnedObject", ves_icall_System_GCHandle_GetAddrOfPinnedObject)
ICALL(GCH_4, "GetTarget", ves_icall_System_GCHandle_GetTarget)
ICALL(GCH_5, "GetTargetHandle", ves_icall_System_GCHandle_GetTargetHandle)

#ifndef DISABLE_COM
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_1)
ICALL(MARSHAL_1, "AddRefInternal", ves_icall_System_Runtime_InteropServices_Marshal_AddRefInternal)
#else
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_2)
#endif
ICALL(MARSHAL_2, "AllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_AllocCoTaskMem)
ICALL(MARSHAL_3, "AllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_AllocHGlobal)
ICALL(MARSHAL_4, "DestroyStructure", ves_icall_System_Runtime_InteropServices_Marshal_DestroyStructure)
ICALL(MARSHAL_5, "FreeBSTR", ves_icall_System_Runtime_InteropServices_Marshal_FreeBSTR)
ICALL(MARSHAL_6, "FreeCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_FreeCoTaskMem)
ICALL(MARSHAL_7, "FreeHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_FreeHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_44, "GetCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetCCW)
ICALL(MARSHAL_8, "GetComSlotForMethodInfoInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetComSlotForMethodInfoInternal)
#endif
ICALL(MARSHAL_9, "GetDelegateForFunctionPointerInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetDelegateForFunctionPointerInternal)
ICALL(MARSHAL_10, "GetFunctionPointerForDelegateInternal", mono_delegate_to_ftnptr)
#ifndef DISABLE_COM
ICALL(MARSHAL_45, "GetIDispatchForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIDispatchForObjectInternal)
ICALL(MARSHAL_46, "GetIUnknownForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIUnknownForObjectInternal)
#endif
ICALL(MARSHAL_11, "GetLastWin32Error", ves_icall_System_Runtime_InteropServices_Marshal_GetLastWin32Error)
#ifndef DISABLE_COM
ICALL(MARSHAL_47, "GetObjectForCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetObjectForCCW)
ICALL(MARSHAL_48, "IsComObject", ves_icall_System_Runtime_InteropServices_Marshal_IsComObject)
#endif
ICALL(MARSHAL_12, "OffsetOf", ves_icall_System_Runtime_InteropServices_Marshal_OffsetOf)
ICALL(MARSHAL_13, "Prelink", ves_icall_System_Runtime_InteropServices_Marshal_Prelink)
ICALL(MARSHAL_14, "PrelinkAll", ves_icall_System_Runtime_InteropServices_Marshal_PrelinkAll)
ICALL(MARSHAL_15, "PtrToStringAnsi(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi)
ICALL(MARSHAL_16, "PtrToStringAnsi(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi_len)
#ifndef DISABLE_COM
ICALL(MARSHAL_17, "PtrToStringBSTR", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringBSTR)
#endif
ICALL(MARSHAL_18, "PtrToStringUni(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni)
ICALL(MARSHAL_19, "PtrToStringUni(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni_len)
ICALL(MARSHAL_20, "PtrToStructure(intptr,System.Type)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure_type)
ICALL(MARSHAL_21, "PtrToStructure(intptr,object)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure)
#ifndef DISABLE_COM
ICALL(MARSHAL_22, "QueryInterfaceInternal", ves_icall_System_Runtime_InteropServices_Marshal_QueryInterfaceInternal)
#endif
ICALL(MARSHAL_43, "ReAllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocCoTaskMem)
ICALL(MARSHAL_23, "ReAllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_49, "ReleaseComObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseComObjectInternal)
ICALL(MARSHAL_29, "ReleaseInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseInternal)
#endif
ICALL(MARSHAL_30, "SizeOf", ves_icall_System_Runtime_InteropServices_Marshal_SizeOf)
ICALL(MARSHAL_31, "StringToBSTR", ves_icall_System_Runtime_InteropServices_Marshal_StringToBSTR)
ICALL(MARSHAL_32, "StringToHGlobalAnsi", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalAnsi)
ICALL(MARSHAL_33, "StringToHGlobalUni", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalUni)
ICALL(MARSHAL_34, "StructureToPtr", ves_icall_System_Runtime_InteropServices_Marshal_StructureToPtr)
ICALL(MARSHAL_35, "UnsafeAddrOfPinnedArrayElement", ves_icall_System_Runtime_InteropServices_Marshal_UnsafeAddrOfPinnedArrayElement)
ICALL(MARSHAL_41, "copy_from_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_from_unmanaged)
ICALL(MARSHAL_42, "copy_to_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_to_unmanaged)

ICALL_TYPE(ACTS, "System.Runtime.Remoting.Activation.ActivationServices", ACTS_1)
ICALL(ACTS_1, "AllocateUninitializedClassInstance", ves_icall_System_Runtime_Activation_ActivationServices_AllocateUninitializedClassInstance)
ICALL(ACTS_2, "EnableProxyActivation", ves_icall_System_Runtime_Activation_ActivationServices_EnableProxyActivation)

ICALL_TYPE(MONOMM, "System.Runtime.Remoting.Messaging.MonoMethodMessage", MONOMM_1)
ICALL(MONOMM_1, "InitMessage", ves_icall_MonoMethodMessage_InitMessage)

#ifndef DISABLE_REMOTING
ICALL_TYPE(REALP, "System.Runtime.Remoting.Proxies.RealProxy", REALP_1)
ICALL(REALP_1, "InternalGetProxyType", ves_icall_Remoting_RealProxy_InternalGetProxyType)
ICALL(REALP_2, "InternalGetTransparentProxy", ves_icall_Remoting_RealProxy_GetTransparentProxy)

ICALL_TYPE(REMSER, "System.Runtime.Remoting.RemotingServices", REMSER_0)
ICALL(REMSER_0, "GetVirtualMethod", ves_icall_Remoting_RemotingServices_GetVirtualMethod)
ICALL(REMSER_1, "InternalExecute", ves_icall_InternalExecute)
ICALL(REMSER_2, "IsTransparentProxy", ves_icall_IsTransparentProxy)
#endif

ICALL_TYPE(MHAN, "System.RuntimeMethodHandle", MHAN_1)
ICALL(MHAN_1, "GetFunctionPointer", ves_icall_RuntimeMethod_GetFunctionPointer)

ICALL_TYPE(RNG, "System.Security.Cryptography.RNGCryptoServiceProvider", RNG_1)
ICALL(RNG_1, "RngClose", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngClose)
ICALL(RNG_2, "RngGetBytes", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngGetBytes)
ICALL(RNG_3, "RngInitialize", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngInitialize)
ICALL(RNG_4, "RngOpen", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngOpen)

#ifndef DISABLE_POLICY_EVIDENCE
ICALL_TYPE(EVID, "System.Security.Policy.Evidence", EVID_1)
ICALL(EVID_1, "IsAuthenticodePresent", ves_icall_System_Security_Policy_Evidence_IsAuthenticodePresent)

ICALL_TYPE(WINID, "System.Security.Principal.WindowsIdentity", WINID_1)
ICALL(WINID_1, "GetCurrentToken", ves_icall_System_Security_Principal_WindowsIdentity_GetCurrentToken)
ICALL(WINID_2, "GetTokenName", ves_icall_System_Security_Principal_WindowsIdentity_GetTokenName)
ICALL(WINID_3, "GetUserToken", ves_icall_System_Security_Principal_WindowsIdentity_GetUserToken)
ICALL(WINID_4, "_GetRoles", ves_icall_System_Security_Principal_WindowsIdentity_GetRoles)

ICALL_TYPE(WINIMP, "System.Security.Principal.WindowsImpersonationContext", WINIMP_1)
ICALL(WINIMP_1, "CloseToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_CloseToken)
ICALL(WINIMP_2, "DuplicateToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_DuplicateToken)
ICALL(WINIMP_3, "RevertToSelf", ves_icall_System_Security_Principal_WindowsImpersonationContext_RevertToSelf)
ICALL(WINIMP_4, "SetCurrentToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_SetCurrentToken)

ICALL_TYPE(WINPRIN, "System.Security.Principal.WindowsPrincipal", WINPRIN_1)
ICALL(WINPRIN_1, "IsMemberOfGroupId", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupId)
ICALL(WINPRIN_2, "IsMemberOfGroupName", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupName)

ICALL_TYPE(SECSTRING, "System.Security.SecureString", SECSTRING_1)
ICALL(SECSTRING_1, "DecryptInternal", ves_icall_System_Security_SecureString_DecryptInternal)
ICALL(SECSTRING_2, "EncryptInternal", ves_icall_System_Security_SecureString_EncryptInternal)
#endif /* !DISABLE_POLICY_EVIDENCE */

ICALL_TYPE(SECMAN, "System.Security.SecurityManager", SECMAN_1)
ICALL(SECMAN_1, "GetLinkDemandSecurity", ves_icall_System_Security_SecurityManager_GetLinkDemandSecurity)
ICALL(SECMAN_2, "get_CheckExecutionRights", ves_icall_System_Security_SecurityManager_get_CheckExecutionRights)
ICALL(SECMAN_3, "get_RequiresElevatedPermissions", mono_security_core_clr_require_elevated_permissions)
ICALL(SECMAN_4, "get_SecurityEnabled", ves_icall_System_Security_SecurityManager_get_SecurityEnabled)
ICALL(SECMAN_5, "set_CheckExecutionRights", ves_icall_System_Security_SecurityManager_set_CheckExecutionRights)
ICALL(SECMAN_6, "set_SecurityEnabled", ves_icall_System_Security_SecurityManager_set_SecurityEnabled)

ICALL_TYPE(STRING, "System.String", STRING_1)
ICALL(STRING_1, ".ctor(char*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_2, ".ctor(char*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_3, ".ctor(char,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_4, ".ctor(char[])", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_5, ".ctor(char[],int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_6, ".ctor(sbyte*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_7, ".ctor(sbyte*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8, ".ctor(sbyte*,int,int,System.Text.Encoding)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8a, "GetLOSLimit", ves_icall_System_String_GetLOSLimit)
ICALL(STRING_9, "InternalAllocateStr", ves_icall_System_String_InternalAllocateStr)
ICALL(STRING_10, "InternalIntern", ves_icall_System_String_InternalIntern)
ICALL(STRING_11, "InternalIsInterned", ves_icall_System_String_InternalIsInterned)

ICALL_TYPE(TENC, "System.Text.Encoding", TENC_1)
ICALL(TENC_1, "InternalCodePage", ves_icall_System_Text_Encoding_InternalCodePage)

ICALL_TYPE(ILOCK, "System.Threading.Interlocked", ILOCK_1)
ICALL(ILOCK_1, "Add(int&,int)", ves_icall_System_Threading_Interlocked_Add_Int)
ICALL(ILOCK_2, "Add(long&,long)", ves_icall_System_Threading_Interlocked_Add_Long)
ICALL(ILOCK_3, "CompareExchange(T&,T,T)", ves_icall_System_Threading_Interlocked_CompareExchange_T)
ICALL(ILOCK_4, "CompareExchange(double&,double,double)", ves_icall_System_Threading_Interlocked_CompareExchange_Double)
ICALL(ILOCK_5, "CompareExchange(int&,int,int)", ves_icall_System_Threading_Interlocked_CompareExchange_Int)
ICALL(ILOCK_6, "CompareExchange(intptr&,intptr,intptr)", ves_icall_System_Threading_Interlocked_CompareExchange_IntPtr)
ICALL(ILOCK_7, "CompareExchange(long&,long,long)", ves_icall_System_Threading_Interlocked_CompareExchange_Long)
ICALL(ILOCK_8, "CompareExchange(object&,object,object)", ves_icall_System_Threading_Interlocked_CompareExchange_Object)
ICALL(ILOCK_9, "CompareExchange(single&,single,single)", ves_icall_System_Threading_Interlocked_CompareExchange_Single)
ICALL(ILOCK_10, "Decrement(int&)", ves_icall_System_Threading_Interlocked_Decrement_Int)
ICALL(ILOCK_11, "Decrement(long&)", ves_icall_System_Threading_Interlocked_Decrement_Long)
ICALL(ILOCK_12, "Exchange(T&,T)", ves_icall_System_Threading_Interlocked_Exchange_T)
ICALL(ILOCK_13, "Exchange(double&,double)", ves_icall_System_Threading_Interlocked_Exchange_Double)
ICALL(ILOCK_14, "Exchange(int&,int)", ves_icall_System_Threading_Interlocked_Exchange_Int)
ICALL(ILOCK_15, "Exchange(intptr&,intptr)", ves_icall_System_Threading_Interlocked_Exchange_IntPtr)
ICALL(ILOCK_16, "Exchange(long&,long)", ves_icall_System_Threading_Interlocked_Exchange_Long)
ICALL(ILOCK_17, "Exchange(object&,object)", ves_icall_System_Threading_Interlocked_Exchange_Object)
ICALL(ILOCK_18, "Exchange(single&,single)", ves_icall_System_Threading_Interlocked_Exchange_Single)
ICALL(ILOCK_19, "Increment(int&)", ves_icall_System_Threading_Interlocked_Increment_Int)
ICALL(ILOCK_20, "Increment(long&)", ves_icall_System_Threading_Interlocked_Increment_Long)
ICALL(ILOCK_21, "Read(long&)", ves_icall_System_Threading_Interlocked_Read_Long)

ICALL_TYPE(ITHREAD, "System.Threading.InternalThread", ITHREAD_1)
ICALL(ITHREAD_1, "Thread_free_internal", ves_icall_System_Threading_InternalThread_Thread_free_internal)

ICALL_TYPE(MONIT, "System.Threading.Monitor", MONIT_8)
ICALL(MONIT_8, "Enter", mono_monitor_enter)
ICALL(MONIT_1, "Exit", mono_monitor_exit)
ICALL(MONIT_2, "Monitor_pulse", ves_icall_System_Threading_Monitor_Monitor_pulse)
ICALL(MONIT_3, "Monitor_pulse_all", ves_icall_System_Threading_Monitor_Monitor_pulse_all)
ICALL(MONIT_4, "Monitor_test_owner", ves_icall_System_Threading_Monitor_Monitor_test_owner)
ICALL(MONIT_5, "Monitor_test_synchronised", ves_icall_System_Threading_Monitor_Monitor_test_synchronised)
ICALL(MONIT_6, "Monitor_try_enter", ves_icall_System_Threading_Monitor_Monitor_try_enter)
ICALL(MONIT_7, "Monitor_wait", ves_icall_System_Threading_Monitor_Monitor_wait)
ICALL(MONIT_9, "try_enter_with_atomic_var", ves_icall_System_Threading_Monitor_Monitor_try_enter_with_atomic_var)

ICALL_TYPE(MUTEX, "System.Threading.Mutex", MUTEX_1)
ICALL(MUTEX_1, "CreateMutex_internal(bool,string,bool&)", ves_icall_System_Threading_Mutex_CreateMutex_internal)
ICALL(MUTEX_2, "OpenMutex_internal(string,System.Security.AccessControl.MutexRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Mutex_OpenMutex_internal)
ICALL(MUTEX_3, "ReleaseMutex_internal(intptr)", ves_icall_System_Threading_Mutex_ReleaseMutex_internal)

ICALL_TYPE(NATIVEC, "System.Threading.NativeEventCalls", NATIVEC_1)
ICALL(NATIVEC_1, "CloseEvent_internal", ves_icall_System_Threading_Events_CloseEvent_internal)
ICALL(NATIVEC_2, "CreateEvent_internal(bool,bool,string,bool&)", ves_icall_System_Threading_Events_CreateEvent_internal)
ICALL(NATIVEC_3, "OpenEvent_internal(string,System.Security.AccessControl.EventWaitHandleRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Events_OpenEvent_internal)
ICALL(NATIVEC_4, "ResetEvent_internal",  ves_icall_System_Threading_Events_ResetEvent_internal)
ICALL(NATIVEC_5, "SetEvent_internal",    ves_icall_System_Threading_Events_SetEvent_internal)

ICALL_TYPE(SEMA, "System.Threading.Semaphore", SEMA_1)
ICALL(SEMA_1, "CreateSemaphore_internal(int,int,string,bool&)", ves_icall_System_Threading_Semaphore_CreateSemaphore_internal)
ICALL(SEMA_2, "OpenSemaphore_internal(string,System.Security.AccessControl.SemaphoreRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Semaphore_OpenSemaphore_internal)
ICALL(SEMA_3, "ReleaseSemaphore_internal(intptr,int,bool&)", ves_icall_System_Threading_Semaphore_ReleaseSemaphore_internal)

ICALL_TYPE(THREAD, "System.Threading.Thread", THREAD_1)
ICALL(THREAD_1, "Abort_internal(System.Threading.InternalThread,object)", ves_icall_System_Threading_Thread_Abort)
ICALL(THREAD_1aa, "AllocTlsData", mono_thread_alloc_tls)
ICALL(THREAD_1a, "ByteArrayToCurrentDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToCurrentDomain)
ICALL(THREAD_1b, "ByteArrayToRootDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToRootDomain)
ICALL(THREAD_2, "ClrState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_ClrState)
ICALL(THREAD_2a, "ConstructInternalThread", ves_icall_System_Threading_Thread_ConstructInternalThread)
ICALL(THREAD_3, "CurrentInternalThread_internal", mono_thread_internal_current)
ICALL(THREAD_3a, "DestroyTlsData", mono_thread_destroy_tls)
ICALL(THREAD_4, "FreeLocalSlotValues", mono_thread_free_local_slot_values)
ICALL(THREAD_55, "GetAbortExceptionState", ves_icall_System_Threading_Thread_GetAbortExceptionState)
ICALL(THREAD_7, "GetDomainID", ves_icall_System_Threading_Thread_GetDomainID)
ICALL(THREAD_8, "GetName_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetName_internal)
ICALL(THREAD_11, "GetState(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetState)
ICALL(THREAD_53, "Interrupt_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Interrupt_internal)
ICALL(THREAD_12, "Join_internal(System.Threading.InternalThread,int,intptr)", ves_icall_System_Threading_Thread_Join_internal)
ICALL(THREAD_13, "MemoryBarrier", ves_icall_System_Threading_Thread_MemoryBarrier)
ICALL(THREAD_14, "ResetAbort_internal()", ves_icall_System_Threading_Thread_ResetAbort)
ICALL(THREAD_15, "Resume_internal()", ves_icall_System_Threading_Thread_Resume)
ICALL(THREAD_18, "SetName_internal(System.Threading.InternalThread,string)", ves_icall_System_Threading_Thread_SetName_internal)
ICALL(THREAD_21, "SetState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_SetState)
ICALL(THREAD_22, "Sleep_internal", ves_icall_System_Threading_Thread_Sleep_internal)
ICALL(THREAD_54, "SpinWait_nop", ves_icall_System_Threading_Thread_SpinWait_nop)
ICALL(THREAD_23, "Suspend_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Suspend)
ICALL(THREAD_25, "Thread_internal", ves_icall_System_Threading_Thread_Thread_internal)
ICALL(THREAD_26, "VolatileRead(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_27, "VolatileRead(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(THREAD_28, "VolatileRead(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_29, "VolatileRead(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_30, "VolatileRead(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_31, "VolatileRead(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_32, "VolatileRead(object&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_33, "VolatileRead(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_34, "VolatileRead(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(THREAD_35, "VolatileRead(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_36, "VolatileRead(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_37, "VolatileRead(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_38, "VolatileRead(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_39, "VolatileWrite(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_40, "VolatileWrite(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(THREAD_41, "VolatileWrite(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_42, "VolatileWrite(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_43, "VolatileWrite(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_44, "VolatileWrite(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_45, "VolatileWrite(object&,object)", ves_icall_System_Threading_Thread_VolatileWriteObject)
ICALL(THREAD_46, "VolatileWrite(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_47, "VolatileWrite(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(THREAD_48, "VolatileWrite(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_49, "VolatileWrite(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_50, "VolatileWrite(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_51, "VolatileWrite(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_9, "Yield", ves_icall_System_Threading_Thread_Yield)
ICALL(THREAD_52, "current_lcid()", ves_icall_System_Threading_Thread_current_lcid)

ICALL_TYPE(THREADP, "System.Threading.ThreadPool", THREADP_1)
ICALL(THREADP_1, "GetAvailableThreads", ves_icall_System_Threading_ThreadPool_GetAvailableThreads)
ICALL(THREADP_2, "GetMaxThreads", ves_icall_System_Threading_ThreadPool_GetMaxThreads)
ICALL(THREADP_3, "GetMinThreads", ves_icall_System_Threading_ThreadPool_GetMinThreads)
ICALL(THREADP_35, "SetMaxThreads", ves_icall_System_Threading_ThreadPool_SetMaxThreads)
ICALL(THREADP_4, "SetMinThreads", ves_icall_System_Threading_ThreadPool_SetMinThreads)
ICALL(THREADP_5, "pool_queue", icall_append_job)

ICALL_TYPE(VOLATILE, "System.Threading.Volatile", VOLATILE_28)
ICALL(VOLATILE_28, "Read(T&)", ves_icall_System_Threading_Volatile_Read_T)
ICALL(VOLATILE_1, "Read(bool&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_2, "Read(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_3, "Read(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(VOLATILE_4, "Read(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_5, "Read(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_6, "Read(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_7, "Read(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_8, "Read(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_9, "Read(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(VOLATILE_10, "Read(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_11, "Read(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_12, "Read(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_13, "Read(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_27, "Write(T&,T)", ves_icall_System_Threading_Volatile_Write_T)
ICALL(VOLATILE_14, "Write(bool&,bool)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_15, "Write(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_16, "Write(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(VOLATILE_17, "Write(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_18, "Write(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_19, "Write(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_20, "Write(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(VOLATILE_21, "Write(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_22, "Write(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(VOLATILE_23, "Write(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_24, "Write(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_25, "Write(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_26, "Write(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)

ICALL_TYPE(WAITH, "System.Threading.WaitHandle", WAITH_1)
ICALL(WAITH_1, "SignalAndWait_Internal", ves_icall_System_Threading_WaitHandle_SignalAndWait_Internal)
ICALL(WAITH_2, "WaitAll_internal", ves_icall_System_Threading_WaitHandle_WaitAll_internal)
ICALL(WAITH_3, "WaitAny_internal", ves_icall_System_Threading_WaitHandle_WaitAny_internal)
ICALL(WAITH_4, "WaitOne_internal", ves_icall_System_Threading_WaitHandle_WaitOne_internal)

ICALL_TYPE(TYPE, "System.Type", TYPE_1)
ICALL(TYPE_1, "EqualsInternal", ves_icall_System_Type_EqualsInternal)
ICALL(TYPE_2, "GetGenericParameterAttributes", ves_icall_Type_GetGenericParameterAttributes)
ICALL(TYPE_3, "GetGenericParameterConstraints_impl", ves_icall_Type_GetGenericParameterConstraints)
ICALL(TYPE_4, "GetGenericParameterPosition", ves_icall_Type_GetGenericParameterPosition)
ICALL(TYPE_5, "GetGenericTypeDefinition_impl", ves_icall_Type_GetGenericTypeDefinition_impl)
ICALL(TYPE_6, "GetInterfaceMapData", ves_icall_Type_GetInterfaceMapData)
ICALL(TYPE_7, "GetPacking", ves_icall_Type_GetPacking)
ICALL(TYPE_8, "GetTypeCode", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_9, "GetTypeCodeInternal", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_10, "IsArrayImpl", ves_icall_Type_IsArrayImpl)
ICALL(TYPE_11, "IsInstanceOfType", ves_icall_type_IsInstanceOfType)
ICALL(TYPE_12, "MakeGenericType", ves_icall_Type_MakeGenericType)
ICALL(TYPE_13, "MakePointerType", ves_icall_Type_MakePointerType)
ICALL(TYPE_14, "get_IsGenericInstance", ves_icall_Type_get_IsGenericInstance)
ICALL(TYPE_15, "get_IsGenericType", ves_icall_Type_get_IsGenericType)
ICALL(TYPE_16, "get_IsGenericTypeDefinition", ves_icall_Type_get_IsGenericTypeDefinition)
ICALL(TYPE_17, "internal_from_handle", ves_icall_type_from_handle)
ICALL(TYPE_18, "internal_from_name", ves_icall_type_from_name)
ICALL(TYPE_19, "make_array_type", ves_icall_Type_make_array_type)
ICALL(TYPE_20, "make_byref_type", ves_icall_Type_make_byref_type)
ICALL(TYPE_21, "type_is_assignable_from", ves_icall_type_is_assignable_from)
ICALL(TYPE_22, "type_is_subtype_of", ves_icall_type_is_subtype_of)

ICALL_TYPE(TYPEDR, "System.TypedReference", TYPEDR_1)
ICALL(TYPEDR_1, "ToObject",	mono_TypedReference_ToObject)
ICALL(TYPEDR_2, "ToObjectInternal",	mono_TypedReference_ToObjectInternal)

ICALL_TYPE(VALUET, "System.ValueType", VALUET_1)
ICALL(VALUET_1, "InternalEquals", ves_icall_System_ValueType_Equals)
ICALL(VALUET_2, "InternalGetHashCode", ves_icall_System_ValueType_InternalGetHashCode)

ICALL_TYPE(WEBIC, "System.Web.Util.ICalls", WEBIC_1)
ICALL(WEBIC_1, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(WEBIC_2, "GetMachineInstallDirectory", ves_icall_System_Web_Util_ICalls_get_machine_install_dir)
ICALL(WEBIC_3, "GetUnmanagedResourcesPtr", ves_icall_get_resources_ptr)

#ifndef DISABLE_COM
ICALL_TYPE(COMOBJ, "System.__ComObject", COMOBJ_1)
ICALL(COMOBJ_1, "CreateRCW", ves_icall_System_ComObject_CreateRCW)
ICALL(COMOBJ_2, "GetInterfaceInternal", ves_icall_System_ComObject_GetInterfaceInternal)
ICALL(COMOBJ_3, "ReleaseInterfaces", ves_icall_System_ComObject_ReleaseInterfaces)
#endif
	{Icall_last}
};

#define icall_desc_num_icalls(desc) ((desc) [1].first_icall - (desc) [0].first_icall)

#undef ICALL_TYPE
#define ICALL_TYPE(id,name,first)
#undef ICALL

#ifdef HAVE_ARRAY_ELEM_INIT
#define MSGSTRFIELD(line) MSGSTRFIELD1(line)
#define MSGSTRFIELD1(line) str##line

static const struct msgstrtn_t {
#define ICALL(id,name,func)
#undef ICALL_TYPE
#define ICALL_TYPE(id,name,first) char MSGSTRFIELD(__LINE__) [sizeof (name)];
// #include "metadata/icall-def.h"
char str39 [sizeof ("Mono.Globalization.Unicode.Normalization")];

char str43 [sizeof ("Mono.Interop.ComInteropProxy")];


char str48 [sizeof ("Mono.Runtime")];



char str54 [sizeof ("Mono.Security.Cryptography.KeyPairPersistence")];





char str62 [sizeof ("System.Activator")];

char str65 [sizeof ("System.AppDomain")];






















char str89 [sizeof ("System.ArgIterator")];




char str95 [sizeof ("System.Array")];















char str112 [sizeof ("System.Buffer")];




char str118 [sizeof ("System.Char")];

char str121 [sizeof ("System.ComponentModel.Win32Exception")];

char str124 [sizeof ("System.Configuration.DefaultConfig")];


char str129 [sizeof ("System.Configuration.InternalConfigurationHost")];


char str133 [sizeof ("System.ConsoleDriver")];





char str140 [sizeof ("System.Convert")];


char str144 [sizeof ("System.CurrentSystemTimeZone")];

char str147 [sizeof ("System.DateTime")];


char str152 [sizeof ("System.Decimal")];













char str169 [sizeof ("System.Delegate")];


char str173 [sizeof ("System.Diagnostics.Debugger")];



char str178 [sizeof ("System.Diagnostics.DefaultTraceListener")];

char str181 [sizeof ("System.Diagnostics.FileVersionInfo")];

char str185 [sizeof ("System.Diagnostics.PerformanceCounter")];




char str191 [sizeof ("System.Diagnostics.PerformanceCounterCategory")];








char str201 [sizeof ("System.Diagnostics.Process")];




















char str223 [sizeof ("System.Diagnostics.Process/ProcessWaitHandle")];


char str228 [sizeof ("System.Diagnostics.Stopwatch")];

char str231 [sizeof ("System.Double")];

char str234 [sizeof ("System.Enum")];





char str241 [sizeof ("System.Environment")];






















char str265 [sizeof ("System.GC")];












char str279 [sizeof ("System.Globalization.CompareInfo")];






char str287 [sizeof ("System.Globalization.CultureInfo")];







char str297 [sizeof ("System.Globalization.RegionInfo")];


char str302 [sizeof ("System.IO.DriveInfo")];



char str308 [sizeof ("System.IO.FAMWatcher")];

char str311 [sizeof ("System.IO.FileSystemWatcher")];

char str314 [sizeof ("System.IO.InotifyWatcher")];



char str326 [sizeof ("System.IO.MonoIO")];






































char str374 [sizeof ("System.IO.Path")];

char str377 [sizeof ("System.Math")];


















char str397 [sizeof ("System.MonoCustomAttrs")];



char str402 [sizeof ("System.MonoEnumInfo")];

char str405 [sizeof ("System.MonoType")];
































char str440 [sizeof ("System.Net.Dns")];



char str445 [sizeof ("System.Net.Sockets.Socket")];



























char str474 [sizeof ("System.Net.Sockets.SocketException")];

char str478 [sizeof ("System.NumberFormatter")];

char str481 [sizeof ("System.Object")];




char str487 [sizeof ("System.Reflection.Assembly")];

























char str520 [sizeof ("System.Reflection.AssemblyName")];


char str524 [sizeof ("System.Reflection.CustomAttributeData")];

char str527 [sizeof ("System.Reflection.Emit.AssemblyBuilder")];


char str531 [sizeof ("System.Reflection.Emit.CustomAttributeBuilder")];

char str535 [sizeof ("System.Reflection.Emit.DerivedType")];

char str539 [sizeof ("System.Reflection.Emit.DynamicMethod")];

char str542 [sizeof ("System.Reflection.Emit.EnumBuilder")];

char str545 [sizeof ("System.Reflection.Emit.GenericTypeParameterBuilder")];

char str548 [sizeof ("System.Reflection.Emit.MethodBuilder")];

char str551 [sizeof ("System.Reflection.Emit.ModuleBuilder")];









char str562 [sizeof ("System.Reflection.Emit.SignatureHelper")];


char str566 [sizeof ("System.Reflection.Emit.TypeBuilder")];







char str575 [sizeof ("System.Reflection.FieldInfo")];



char str580 [sizeof ("System.Reflection.MemberInfo")];

char str583 [sizeof ("System.Reflection.MethodBase")];




char str589 [sizeof ("System.Reflection.Module")];














char str605 [sizeof ("System.Reflection.MonoCMethod")];


char str609 [sizeof ("System.Reflection.MonoEventInfo")];

char str612 [sizeof ("System.Reflection.MonoField")];






char str620 [sizeof ("System.Reflection.MonoGenericCMethod")];

char str623 [sizeof ("System.Reflection.MonoGenericClass")];


char str628 [sizeof ("System.Reflection.MonoGenericMethod")];

char str631 [sizeof ("System.Reflection.MonoMethod")];









char str642 [sizeof ("System.Reflection.MonoMethodInfo")];




char str648 [sizeof ("System.Reflection.MonoPropertyInfo")];



char str653 [sizeof ("System.Reflection.ParameterInfo")];


char str657 [sizeof ("System.Runtime.CompilerServices.RuntimeHelpers")];







char str667 [sizeof ("System.Runtime.InteropServices.GCHandle")];





char str675 [sizeof ("System.Runtime.InteropServices.Marshal")];







































char str731 [sizeof ("System.Runtime.Remoting.Activation.ActivationServices")];


char str735 [sizeof ("System.Runtime.Remoting.Messaging.MonoMethodMessage")];

char str739 [sizeof ("System.Runtime.Remoting.Proxies.RealProxy")];


char str743 [sizeof ("System.Runtime.Remoting.RemotingServices")];



char str749 [sizeof ("System.RuntimeMethodHandle")];

char str752 [sizeof ("System.Security.Cryptography.RNGCryptoServiceProvider")];




char str759 [sizeof ("System.Security.Policy.Evidence")];

char str762 [sizeof ("System.Security.Principal.WindowsIdentity")];




char str768 [sizeof ("System.Security.Principal.WindowsImpersonationContext")];




char str774 [sizeof ("System.Security.Principal.WindowsPrincipal")];


char str778 [sizeof ("System.Security.SecureString")];


char str783 [sizeof ("System.Security.SecurityManager")];






char str791 [sizeof ("System.String")];












char str805 [sizeof ("System.Text.Encoding")];

char str808 [sizeof ("System.Threading.Interlocked")];





















char str831 [sizeof ("System.Threading.InternalThread")];

char str834 [sizeof ("System.Threading.Monitor")];









char str845 [sizeof ("System.Threading.Mutex")];



char str850 [sizeof ("System.Threading.NativeEventCalls")];





char str857 [sizeof ("System.Threading.Semaphore")];



char str862 [sizeof ("System.Threading.Thread")];




















































char str916 [sizeof ("System.Threading.ThreadPool")];






char str924 [sizeof ("System.Threading.Volatile")];




























char str954 [sizeof ("System.Threading.WaitHandle")];




char str960 [sizeof ("System.Type")];






















char str984 [sizeof ("System.TypedReference")];


char str988 [sizeof ("System.ValueType")];


char str992 [sizeof ("System.Web.Util.ICalls")];



char str998 [sizeof ("System.__ComObject")];

#undef ICALL_TYPE
} icall_type_names_str = {
#define ICALL_TYPE(id,name,first) (name),
// #include "metadata/icall-def.h"
ICALL_TYPE(UNORM, "Mono.Globalization.Unicode.Normalization", UNORM_1)
ICALL(UNORM_1, "load_normalization_resource", load_normalization_resource)

#ifndef DISABLE_COM
ICALL_TYPE(COMPROX, "Mono.Interop.ComInteropProxy", COMPROX_1)
ICALL(COMPROX_1, "AddProxy", ves_icall_Mono_Interop_ComInteropProxy_AddProxy)
ICALL(COMPROX_2, "FindProxy", ves_icall_Mono_Interop_ComInteropProxy_FindProxy)
#endif

ICALL_TYPE(RUNTIME, "Mono.Runtime", RUNTIME_1)
ICALL(RUNTIME_1, "GetDisplayName", ves_icall_Mono_Runtime_GetDisplayName)
ICALL(RUNTIME_12, "GetNativeStackTrace", ves_icall_Mono_Runtime_GetNativeStackTrace)
ICALL(RUNTIME_13, "SetGCAllowSynchronousMajor", ves_icall_Mono_Runtime_SetGCAllowSynchronousMajor)

#ifndef PLATFORM_RO_FS
ICALL_TYPE(KPAIR, "Mono.Security.Cryptography.KeyPairPersistence", KPAIR_1)
ICALL(KPAIR_1, "_CanSecure", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_CanSecure)
ICALL(KPAIR_2, "_IsMachineProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsMachineProtected)
ICALL(KPAIR_3, "_IsUserProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsUserProtected)
ICALL(KPAIR_4, "_ProtectMachine", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectMachine)
ICALL(KPAIR_5, "_ProtectUser", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectUser)
#endif /* !PLATFORM_RO_FS */

ICALL_TYPE(ACTIV, "System.Activator", ACTIV_1)
ICALL(ACTIV_1, "CreateInstanceInternal", ves_icall_System_Activator_CreateInstanceInternal)

ICALL_TYPE(APPDOM, "System.AppDomain", APPDOM_1)
ICALL(APPDOM_1, "ExecuteAssembly", ves_icall_System_AppDomain_ExecuteAssembly)
ICALL(APPDOM_2, "GetAssemblies", ves_icall_System_AppDomain_GetAssemblies)
ICALL(APPDOM_3, "GetData", ves_icall_System_AppDomain_GetData)
ICALL(APPDOM_4, "InternalGetContext", ves_icall_System_AppDomain_InternalGetContext)
ICALL(APPDOM_5, "InternalGetDefaultContext", ves_icall_System_AppDomain_InternalGetDefaultContext)
ICALL(APPDOM_6, "InternalGetProcessGuid", ves_icall_System_AppDomain_InternalGetProcessGuid)
ICALL(APPDOM_7, "InternalIsFinalizingForUnload", ves_icall_System_AppDomain_InternalIsFinalizingForUnload)
ICALL(APPDOM_8, "InternalPopDomainRef", ves_icall_System_AppDomain_InternalPopDomainRef)
ICALL(APPDOM_9, "InternalPushDomainRef", ves_icall_System_AppDomain_InternalPushDomainRef)
ICALL(APPDOM_10, "InternalPushDomainRefByID", ves_icall_System_AppDomain_InternalPushDomainRefByID)
ICALL(APPDOM_11, "InternalSetContext", ves_icall_System_AppDomain_InternalSetContext)
ICALL(APPDOM_12, "InternalSetDomain", ves_icall_System_AppDomain_InternalSetDomain)
ICALL(APPDOM_13, "InternalSetDomainByID", ves_icall_System_AppDomain_InternalSetDomainByID)
ICALL(APPDOM_14, "InternalUnload", ves_icall_System_AppDomain_InternalUnload)
ICALL(APPDOM_15, "LoadAssembly", ves_icall_System_AppDomain_LoadAssembly)
ICALL(APPDOM_16, "LoadAssemblyRaw", ves_icall_System_AppDomain_LoadAssemblyRaw)
ICALL(APPDOM_17, "SetData", ves_icall_System_AppDomain_SetData)
ICALL(APPDOM_18, "createDomain", ves_icall_System_AppDomain_createDomain)
ICALL(APPDOM_19, "getCurDomain", ves_icall_System_AppDomain_getCurDomain)
ICALL(APPDOM_20, "getFriendlyName", ves_icall_System_AppDomain_getFriendlyName)
ICALL(APPDOM_21, "getRootDomain", ves_icall_System_AppDomain_getRootDomain)
ICALL(APPDOM_22, "getSetup", ves_icall_System_AppDomain_getSetup)

ICALL_TYPE(ARGI, "System.ArgIterator", ARGI_1)
ICALL(ARGI_1, "IntGetNextArg()",                  mono_ArgIterator_IntGetNextArg)
ICALL(ARGI_2, "IntGetNextArg(intptr)", mono_ArgIterator_IntGetNextArgT)
ICALL(ARGI_3, "IntGetNextArgType",                mono_ArgIterator_IntGetNextArgType)
ICALL(ARGI_4, "Setup",                            mono_ArgIterator_Setup)

ICALL_TYPE(ARRAY, "System.Array", ARRAY_1)
ICALL(ARRAY_1, "ClearInternal",    ves_icall_System_Array_ClearInternal)
ICALL(ARRAY_2, "Clone",            mono_array_clone)
ICALL(ARRAY_3, "CreateInstanceImpl",   ves_icall_System_Array_CreateInstanceImpl)
ICALL(ARRAY_14, "CreateInstanceImpl64",   ves_icall_System_Array_CreateInstanceImpl64)
ICALL(ARRAY_4, "FastCopy",         ves_icall_System_Array_FastCopy)
ICALL(ARRAY_5, "GetGenericValueImpl", ves_icall_System_Array_GetGenericValueImpl)
ICALL(ARRAY_6, "GetLength",        ves_icall_System_Array_GetLength)
ICALL(ARRAY_15, "GetLongLength",        ves_icall_System_Array_GetLongLength)
ICALL(ARRAY_7, "GetLowerBound",    ves_icall_System_Array_GetLowerBound)
ICALL(ARRAY_8, "GetRank",          ves_icall_System_Array_GetRank)
ICALL(ARRAY_9, "GetValue",         ves_icall_System_Array_GetValue)
ICALL(ARRAY_10, "GetValueImpl",     ves_icall_System_Array_GetValueImpl)
ICALL(ARRAY_11, "SetGenericValueImpl", ves_icall_System_Array_SetGenericValueImpl)
ICALL(ARRAY_12, "SetValue",         ves_icall_System_Array_SetValue)
ICALL(ARRAY_13, "SetValueImpl",     ves_icall_System_Array_SetValueImpl)

ICALL_TYPE(BUFFER, "System.Buffer", BUFFER_1)
ICALL(BUFFER_1, "BlockCopyInternal", ves_icall_System_Buffer_BlockCopyInternal)
ICALL(BUFFER_2, "ByteLengthInternal", ves_icall_System_Buffer_ByteLengthInternal)
ICALL(BUFFER_3, "GetByteInternal", ves_icall_System_Buffer_GetByteInternal)
ICALL(BUFFER_4, "SetByteInternal", ves_icall_System_Buffer_SetByteInternal)

ICALL_TYPE(CHAR, "System.Char", CHAR_1)
ICALL(CHAR_1, "GetDataTablePointers", ves_icall_System_Char_GetDataTablePointers)

ICALL_TYPE (COMPO_W, "System.ComponentModel.Win32Exception", COMPO_W_1)
ICALL (COMPO_W_1, "W32ErrorMessage", ves_icall_System_ComponentModel_Win32Exception_W32ErrorMessage)

ICALL_TYPE(DEFAULTC, "System.Configuration.DefaultConfig", DEFAULTC_1)
ICALL(DEFAULTC_1, "get_bundled_machine_config", get_bundled_machine_config)
ICALL(DEFAULTC_2, "get_machine_config_path", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)

/* Note that the below icall shares the same function as DefaultConfig uses */
ICALL_TYPE(INTCFGHOST, "System.Configuration.InternalConfigurationHost", INTCFGHOST_1)
ICALL(INTCFGHOST_1, "get_bundled_app_config", get_bundled_app_config)
ICALL(INTCFGHOST_2, "get_bundled_machine_config", get_bundled_machine_config)

ICALL_TYPE(CONSOLE, "System.ConsoleDriver", CONSOLE_1)
ICALL(CONSOLE_1, "InternalKeyAvailable", ves_icall_System_ConsoleDriver_InternalKeyAvailable )
ICALL(CONSOLE_2, "Isatty", ves_icall_System_ConsoleDriver_Isatty )
ICALL(CONSOLE_3, "SetBreak", ves_icall_System_ConsoleDriver_SetBreak )
ICALL(CONSOLE_4, "SetEcho", ves_icall_System_ConsoleDriver_SetEcho )
ICALL(CONSOLE_5, "TtySetup", ves_icall_System_ConsoleDriver_TtySetup )

ICALL_TYPE(CONVERT, "System.Convert", CONVERT_1)
ICALL(CONVERT_1, "InternalFromBase64CharArray", InternalFromBase64CharArray )
ICALL(CONVERT_2, "InternalFromBase64String", InternalFromBase64String )

ICALL_TYPE(TZONE, "System.CurrentSystemTimeZone", TZONE_1)
ICALL(TZONE_1, "GetTimeZoneData", ves_icall_System_CurrentSystemTimeZone_GetTimeZoneData)

ICALL_TYPE(DTIME, "System.DateTime", DTIME_1)
ICALL(DTIME_1, "GetNow", mono_100ns_datetime)
ICALL(DTIME_2, "GetTimeMonotonic", mono_100ns_ticks)

#ifndef DISABLE_DECIMAL
ICALL_TYPE(DECIMAL, "System.Decimal", DECIMAL_1)
ICALL(DECIMAL_1, "decimal2Int64", mono_decimal2Int64)
ICALL(DECIMAL_2, "decimal2UInt64", mono_decimal2UInt64)
ICALL(DECIMAL_3, "decimal2double", mono_decimal2double)
//ICALL(DECIMAL_4, "decimal2string", mono_decimal2string)
ICALL(DECIMAL_5, "decimalCompare", mono_decimalCompare)
ICALL(DECIMAL_6, "decimalDiv", mono_decimalDiv)
ICALL(DECIMAL_7, "decimalFloorAndTrunc", mono_decimalFloorAndTrunc)
ICALL(DECIMAL_8, "decimalIncr", mono_decimalIncr)
ICALL(DECIMAL_9, "decimalIntDiv", mono_decimalIntDiv)
ICALL(DECIMAL_10, "decimalMult", mono_decimalMult)
ICALL(DECIMAL_11, "decimalRound", mono_decimalRound)
ICALL(DECIMAL_12, "decimalSetExponent", mono_decimalSetExponent)
ICALL(DECIMAL_13, "double2decimal", mono_double2decimal) /* FIXME: wrong signature. */
ICALL(DECIMAL_14, "string2decimal", mono_string2decimal)
#endif

ICALL_TYPE(DELEGATE, "System.Delegate", DELEGATE_1)
ICALL(DELEGATE_1, "CreateDelegate_internal", ves_icall_System_Delegate_CreateDelegate_internal)
ICALL(DELEGATE_2, "SetMulticastInvoke", ves_icall_System_Delegate_SetMulticastInvoke)

ICALL_TYPE(DEBUGR, "System.Diagnostics.Debugger", DEBUGR_1)
ICALL(DEBUGR_1, "IsAttached_internal", ves_icall_System_Diagnostics_Debugger_IsAttached_internal)
ICALL(DEBUGR_2, "IsLogging", ves_icall_System_Diagnostics_Debugger_IsLogging)
ICALL(DEBUGR_3, "Log", ves_icall_System_Diagnostics_Debugger_Log)

ICALL_TYPE(TRACEL, "System.Diagnostics.DefaultTraceListener", TRACEL_1)
ICALL(TRACEL_1, "WriteWindowsDebugString", ves_icall_System_Diagnostics_DefaultTraceListener_WriteWindowsDebugString)

ICALL_TYPE(FILEV, "System.Diagnostics.FileVersionInfo", FILEV_1)
ICALL(FILEV_1, "GetVersionInfo_internal(string)", ves_icall_System_Diagnostics_FileVersionInfo_GetVersionInfo_internal)

#ifndef DISABLE_PROCESS_HANDLING
ICALL_TYPE(PERFCTR, "System.Diagnostics.PerformanceCounter", PERFCTR_1)
ICALL(PERFCTR_1, "FreeData", mono_perfcounter_free_data)
ICALL(PERFCTR_2, "GetImpl", mono_perfcounter_get_impl)
ICALL(PERFCTR_3, "GetSample", mono_perfcounter_get_sample)
ICALL(PERFCTR_4, "UpdateValue", mono_perfcounter_update_value)

ICALL_TYPE(PERFCTRCAT, "System.Diagnostics.PerformanceCounterCategory", PERFCTRCAT_1)
ICALL(PERFCTRCAT_1, "CategoryDelete", mono_perfcounter_category_del)
ICALL(PERFCTRCAT_2, "CategoryHelpInternal",   mono_perfcounter_category_help)
ICALL(PERFCTRCAT_3, "CounterCategoryExists", mono_perfcounter_category_exists)
ICALL(PERFCTRCAT_4, "Create",         mono_perfcounter_create)
ICALL(PERFCTRCAT_5, "GetCategoryNames", mono_perfcounter_category_names)
ICALL(PERFCTRCAT_6, "GetCounterNames", mono_perfcounter_counter_names)
ICALL(PERFCTRCAT_7, "GetInstanceNames", mono_perfcounter_instance_names)
ICALL(PERFCTRCAT_8, "InstanceExistsInternal", mono_perfcounter_instance_exists)

ICALL_TYPE(PROCESS, "System.Diagnostics.Process", PROCESS_1)
ICALL(PROCESS_1, "CreateProcess_internal(System.Diagnostics.ProcessStartInfo,intptr,intptr,intptr,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_CreateProcess_internal)
ICALL(PROCESS_2, "ExitCode_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitCode_internal)
ICALL(PROCESS_3, "ExitTime_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitTime_internal)
ICALL(PROCESS_4, "GetModules_internal(intptr)", ves_icall_System_Diagnostics_Process_GetModules_internal)
ICALL(PROCESS_5, "GetPid_internal()", ves_icall_System_Diagnostics_Process_GetPid_internal)
ICALL(PROCESS_5B, "GetPriorityClass(intptr,int&)", ves_icall_System_Diagnostics_Process_GetPriorityClass)
ICALL(PROCESS_5H, "GetProcessData", ves_icall_System_Diagnostics_Process_GetProcessData)
ICALL(PROCESS_6, "GetProcess_internal(int)", ves_icall_System_Diagnostics_Process_GetProcess_internal)
ICALL(PROCESS_7, "GetProcesses_internal()", ves_icall_System_Diagnostics_Process_GetProcesses_internal)
ICALL(PROCESS_8, "GetWorkingSet_internal(intptr,int&,int&)", ves_icall_System_Diagnostics_Process_GetWorkingSet_internal)
ICALL(PROCESS_9, "Kill_internal", ves_icall_System_Diagnostics_Process_Kill_internal)
ICALL(PROCESS_10, "ProcessName_internal(intptr)", ves_icall_System_Diagnostics_Process_ProcessName_internal)
ICALL(PROCESS_11, "Process_free_internal(intptr)", ves_icall_System_Diagnostics_Process_Process_free_internal)
ICALL(PROCESS_11B, "SetPriorityClass(intptr,int,int&)", ves_icall_System_Diagnostics_Process_SetPriorityClass)
ICALL(PROCESS_12, "SetWorkingSet_internal(intptr,int,int,bool)", ves_icall_System_Diagnostics_Process_SetWorkingSet_internal)
ICALL(PROCESS_13, "ShellExecuteEx_internal(System.Diagnostics.ProcessStartInfo,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_ShellExecuteEx_internal)
ICALL(PROCESS_14, "StartTime_internal(intptr)", ves_icall_System_Diagnostics_Process_StartTime_internal)
ICALL(PROCESS_14M, "Times", ves_icall_System_Diagnostics_Process_Times)
ICALL(PROCESS_15, "WaitForExit_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForExit_internal)
ICALL(PROCESS_16, "WaitForInputIdle_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForInputIdle_internal)

ICALL_TYPE (PROCESSHANDLE, "System.Diagnostics.Process/ProcessWaitHandle", PROCESSHANDLE_1)
ICALL (PROCESSHANDLE_1, "ProcessHandle_close(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_close)
ICALL (PROCESSHANDLE_2, "ProcessHandle_duplicate(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_duplicate)
#endif /* !DISABLE_PROCESS_HANDLING */

ICALL_TYPE(STOPWATCH, "System.Diagnostics.Stopwatch", STOPWATCH_1)
ICALL(STOPWATCH_1, "GetTimestamp", mono_100ns_ticks)

ICALL_TYPE(DOUBLE, "System.Double", DOUBLE_1)
ICALL(DOUBLE_1, "ParseImpl",    mono_double_ParseImpl)

ICALL_TYPE(ENUM, "System.Enum", ENUM_1)
ICALL(ENUM_1, "ToObject", ves_icall_System_Enum_ToObject)
ICALL(ENUM_5, "compare_value_to", ves_icall_System_Enum_compare_value_to)
ICALL(ENUM_4, "get_hashcode", ves_icall_System_Enum_get_hashcode)
ICALL(ENUM_3, "get_underlying_type", ves_icall_System_Enum_get_underlying_type)
ICALL(ENUM_2, "get_value", ves_icall_System_Enum_get_value)

ICALL_TYPE(ENV, "System.Environment", ENV_1)
ICALL(ENV_1, "Exit", ves_icall_System_Environment_Exit)
ICALL(ENV_2, "GetCommandLineArgs", mono_runtime_get_main_args)
ICALL(ENV_3, "GetEnvironmentVariableNames", ves_icall_System_Environment_GetEnvironmentVariableNames)
ICALL(ENV_4, "GetLogicalDrivesInternal", ves_icall_System_Environment_GetLogicalDrives )
ICALL(ENV_5, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(ENV_51, "GetNewLine", ves_icall_System_Environment_get_NewLine)
ICALL(ENV_6, "GetOSVersionString", ves_icall_System_Environment_GetOSVersionString)
ICALL(ENV_6a, "GetPageSize", mono_pagesize)
ICALL(ENV_7, "GetWindowsFolderPath", ves_icall_System_Environment_GetWindowsFolderPath)
ICALL(ENV_8, "InternalSetEnvironmentVariable", ves_icall_System_Environment_InternalSetEnvironmentVariable)
ICALL(ENV_9, "get_ExitCode", mono_environment_exitcode_get)
ICALL(ENV_10, "get_HasShutdownStarted", ves_icall_System_Environment_get_HasShutdownStarted)
ICALL(ENV_11, "get_MachineName", ves_icall_System_Environment_get_MachineName)
ICALL(ENV_13, "get_Platform", ves_icall_System_Environment_get_Platform)
ICALL(ENV_14, "get_ProcessorCount", mono_cpu_count)
ICALL(ENV_15, "get_TickCount", mono_msec_ticks)
ICALL(ENV_16, "get_UserName", ves_icall_System_Environment_get_UserName)
ICALL(ENV_16m, "internalBroadcastSettingChange", ves_icall_System_Environment_BroadcastSettingChange)
ICALL(ENV_17, "internalGetEnvironmentVariable", ves_icall_System_Environment_GetEnvironmentVariable)
ICALL(ENV_18, "internalGetGacPath", ves_icall_System_Environment_GetGacPath)
ICALL(ENV_19, "internalGetHome", ves_icall_System_Environment_InternalGetHome)
ICALL(ENV_20, "set_ExitCode", mono_environment_exitcode_set)

ICALL_TYPE(GC, "System.GC", GC_0)
ICALL(GC_0, "CollectionCount", mono_gc_collection_count)
ICALL(GC_0a, "GetGeneration", mono_gc_get_generation)
ICALL(GC_1, "GetTotalMemory", ves_icall_System_GC_GetTotalMemory)
ICALL(GC_2, "InternalCollect", ves_icall_System_GC_InternalCollect)
ICALL(GC_3, "KeepAlive", ves_icall_System_GC_KeepAlive)
ICALL(GC_4, "ReRegisterForFinalize", ves_icall_System_GC_ReRegisterForFinalize)
ICALL(GC_4a, "RecordPressure", mono_gc_add_memory_pressure)
ICALL(GC_5, "SuppressFinalize", ves_icall_System_GC_SuppressFinalize)
ICALL(GC_6, "WaitForPendingFinalizers", ves_icall_System_GC_WaitForPendingFinalizers)
ICALL(GC_7, "get_MaxGeneration", mono_gc_max_generation)
ICALL(GC_9, "get_ephemeron_tombstone", ves_icall_System_GC_get_ephemeron_tombstone)
ICALL(GC_8, "register_ephemeron_array", ves_icall_System_GC_register_ephemeron_array)

ICALL_TYPE(COMPINF, "System.Globalization.CompareInfo", COMPINF_1)
ICALL(COMPINF_1, "assign_sortkey(object,string,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_assign_sortkey)
ICALL(COMPINF_2, "construct_compareinfo(string)", ves_icall_System_Globalization_CompareInfo_construct_compareinfo)
ICALL(COMPINF_3, "free_internal_collator()", ves_icall_System_Globalization_CompareInfo_free_internal_collator)
ICALL(COMPINF_4, "internal_compare(string,int,int,string,int,int,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_internal_compare)
ICALL(COMPINF_5, "internal_index(string,int,int,char,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index_char)
ICALL(COMPINF_6, "internal_index(string,int,int,string,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index)

ICALL_TYPE(CULINF, "System.Globalization.CultureInfo", CULINF_2)
ICALL(CULINF_2, "construct_datetime_format", ves_icall_System_Globalization_CultureInfo_construct_datetime_format)
ICALL(CULINF_4, "construct_internal_locale_from_current_locale", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_current_locale)
ICALL(CULINF_5, "construct_internal_locale_from_lcid", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_lcid)
ICALL(CULINF_6, "construct_internal_locale_from_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_name)
ICALL(CULINF_7, "construct_internal_locale_from_specific_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_specific_name)
ICALL(CULINF_8, "construct_number_format", ves_icall_System_Globalization_CultureInfo_construct_number_format)
ICALL(CULINF_9, "internal_get_cultures", ves_icall_System_Globalization_CultureInfo_internal_get_cultures)
//ICALL(CULINF_10, "internal_is_lcid_neutral", ves_icall_System_Globalization_CultureInfo_internal_is_lcid_neutral)

ICALL_TYPE(REGINF, "System.Globalization.RegionInfo", REGINF_1)
ICALL(REGINF_1, "construct_internal_region_from_lcid", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_lcid)
ICALL(REGINF_2, "construct_internal_region_from_name", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_name)

#ifndef PLATFORM_NO_DRIVEINFO
ICALL_TYPE(IODRIVEINFO, "System.IO.DriveInfo", IODRIVEINFO_1)
ICALL(IODRIVEINFO_1, "GetDiskFreeSpaceInternal", ves_icall_System_IO_DriveInfo_GetDiskFreeSpace)
ICALL(IODRIVEINFO_2, "GetDriveFormat", ves_icall_System_IO_DriveInfo_GetDriveFormat)
ICALL(IODRIVEINFO_3, "GetDriveTypeInternal", ves_icall_System_IO_DriveInfo_GetDriveType)
#endif

ICALL_TYPE(FAMW, "System.IO.FAMWatcher", FAMW_1)
ICALL(FAMW_1, "InternalFAMNextEvent", ves_icall_System_IO_FAMW_InternalFAMNextEvent)

ICALL_TYPE(FILEW, "System.IO.FileSystemWatcher", FILEW_4)
ICALL(FILEW_4, "InternalSupportsFSW", ves_icall_System_IO_FSW_SupportsFSW)

ICALL_TYPE(INOW, "System.IO.InotifyWatcher", INOW_1)
ICALL(INOW_1, "AddWatch", ves_icall_System_IO_InotifyWatcher_AddWatch)
ICALL(INOW_2, "GetInotifyInstance", ves_icall_System_IO_InotifyWatcher_GetInotifyInstance)
ICALL(INOW_3, "RemoveWatch", ves_icall_System_IO_InotifyWatcher_RemoveWatch)

#if defined (TARGET_IOS) || defined (TARGET_ANDROID)
ICALL_TYPE(MMAPIMPL, "System.IO.MemoryMappedFiles.MemoryMapImpl", MMAPIMPL_1)
ICALL(MMAPIMPL_1, "mono_filesize_from_fd", mono_filesize_from_fd)
ICALL(MMAPIMPL_2, "mono_filesize_from_path", mono_filesize_from_path)
#endif


ICALL_TYPE(MONOIO, "System.IO.MonoIO", MONOIO_1)
ICALL(MONOIO_1, "Close(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Close)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_2, "CopyFile(string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CopyFile)
ICALL(MONOIO_3, "CreateDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CreateDirectory)
ICALL(MONOIO_4, "CreatePipe(intptr&,intptr&)", ves_icall_System_IO_MonoIO_CreatePipe)
ICALL(MONOIO_5, "DeleteFile(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_DeleteFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_34, "DuplicateHandle", ves_icall_System_IO_MonoIO_DuplicateHandle)
ICALL(MONOIO_37, "FindClose", ves_icall_System_IO_MonoIO_FindClose)
ICALL(MONOIO_35, "FindFirst", ves_icall_System_IO_MonoIO_FindFirst)
ICALL(MONOIO_36, "FindNext", ves_icall_System_IO_MonoIO_FindNext)
ICALL(MONOIO_6, "Flush(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Flush)
ICALL(MONOIO_7, "GetCurrentDirectory(System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetCurrentDirectory)
ICALL(MONOIO_8, "GetFileAttributes(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileAttributes)
ICALL(MONOIO_9, "GetFileStat(string,System.IO.MonoIOStat&,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileStat)
ICALL(MONOIO_10, "GetFileSystemEntries", ves_icall_System_IO_MonoIO_GetFileSystemEntries)
ICALL(MONOIO_11, "GetFileType(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileType)
ICALL(MONOIO_12, "GetLength(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_13, "GetTempPath(string&)", ves_icall_System_IO_MonoIO_GetTempPath)
ICALL(MONOIO_14, "Lock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Lock)
ICALL(MONOIO_15, "MoveFile(string,string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_MoveFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_16, "Open(string,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.IO.FileOptions,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Open)
ICALL(MONOIO_17, "Read(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Read)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_18, "RemoveDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_RemoveDirectory)
ICALL(MONOIO_18M, "ReplaceFile(string,string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_ReplaceFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_19, "Seek(intptr,long,System.IO.SeekOrigin,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Seek)
ICALL(MONOIO_20, "SetCurrentDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetCurrentDirectory)
ICALL(MONOIO_21, "SetFileAttributes(string,System.IO.FileAttributes,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileAttributes)
ICALL(MONOIO_22, "SetFileTime(intptr,long,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileTime)
ICALL(MONOIO_23, "SetLength(intptr,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_24, "Unlock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Unlock)
#endif
ICALL(MONOIO_25, "Write(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Write)
ICALL(MONOIO_26, "get_AltDirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_AltDirectorySeparatorChar)
ICALL(MONOIO_27, "get_ConsoleError", ves_icall_System_IO_MonoIO_get_ConsoleError)
ICALL(MONOIO_28, "get_ConsoleInput", ves_icall_System_IO_MonoIO_get_ConsoleInput)
ICALL(MONOIO_29, "get_ConsoleOutput", ves_icall_System_IO_MonoIO_get_ConsoleOutput)
ICALL(MONOIO_30, "get_DirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_DirectorySeparatorChar)
ICALL(MONOIO_31, "get_InvalidPathChars", ves_icall_System_IO_MonoIO_get_InvalidPathChars)
ICALL(MONOIO_32, "get_PathSeparator", ves_icall_System_IO_MonoIO_get_PathSeparator)
ICALL(MONOIO_33, "get_VolumeSeparatorChar", ves_icall_System_IO_MonoIO_get_VolumeSeparatorChar)

ICALL_TYPE(IOPATH, "System.IO.Path", IOPATH_1)
ICALL(IOPATH_1, "get_temp_path", ves_icall_System_IO_get_temp_path)

ICALL_TYPE(MATH, "System.Math", MATH_1)
ICALL(MATH_1, "Acos", ves_icall_System_Math_Acos)
ICALL(MATH_2, "Asin", ves_icall_System_Math_Asin)
ICALL(MATH_3, "Atan", ves_icall_System_Math_Atan)
ICALL(MATH_4, "Atan2", ves_icall_System_Math_Atan2)
ICALL(MATH_5, "Cos", ves_icall_System_Math_Cos)
ICALL(MATH_6, "Cosh", ves_icall_System_Math_Cosh)
ICALL(MATH_7, "Exp", ves_icall_System_Math_Exp)
ICALL(MATH_8, "Floor", ves_icall_System_Math_Floor)
ICALL(MATH_9, "Log", ves_icall_System_Math_Log)
ICALL(MATH_10, "Log10", ves_icall_System_Math_Log10)
ICALL(MATH_11, "Pow", ves_icall_System_Math_Pow)
ICALL(MATH_12, "Round", ves_icall_System_Math_Round)
ICALL(MATH_13, "Round2", ves_icall_System_Math_Round2)
ICALL(MATH_14, "Sin", ves_icall_System_Math_Sin)
ICALL(MATH_15, "Sinh", ves_icall_System_Math_Sinh)
ICALL(MATH_16, "Sqrt", ves_icall_System_Math_Sqrt)
ICALL(MATH_17, "Tan", ves_icall_System_Math_Tan)
ICALL(MATH_18, "Tanh", ves_icall_System_Math_Tanh)

ICALL_TYPE(MCATTR, "System.MonoCustomAttrs", MCATTR_1)
ICALL(MCATTR_1, "GetCustomAttributesDataInternal", mono_reflection_get_custom_attrs_data)
ICALL(MCATTR_2, "GetCustomAttributesInternal", custom_attrs_get_by_type)
ICALL(MCATTR_3, "IsDefinedInternal", custom_attrs_defined_internal)

ICALL_TYPE(MENUM, "System.MonoEnumInfo", MENUM_1)
ICALL(MENUM_1, "get_enum_info", ves_icall_get_enum_info)

ICALL_TYPE(MTYPE, "System.MonoType", MTYPE_1)
ICALL(MTYPE_1, "GetArrayRank", ves_icall_MonoType_GetArrayRank)
ICALL(MTYPE_2, "GetConstructors", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_3, "GetConstructors_internal", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_4, "GetCorrespondingInflatedConstructor", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_5, "GetCorrespondingInflatedMethod", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_6, "GetElementType", ves_icall_MonoType_GetElementType)
ICALL(MTYPE_7, "GetEvents_internal", ves_icall_Type_GetEvents_internal)
ICALL(MTYPE_8, "GetField", ves_icall_Type_GetField)
ICALL(MTYPE_9, "GetFields_internal", ves_icall_Type_GetFields_internal)
ICALL(MTYPE_10, "GetGenericArguments", ves_icall_MonoType_GetGenericArguments)
ICALL(MTYPE_11, "GetInterfaces", ves_icall_Type_GetInterfaces)
ICALL(MTYPE_12, "GetMethodsByName", ves_icall_Type_GetMethodsByName)
ICALL(MTYPE_13, "GetNestedType", ves_icall_Type_GetNestedType)
ICALL(MTYPE_14, "GetNestedTypes", ves_icall_Type_GetNestedTypes)
ICALL(MTYPE_15, "GetPropertiesByName", ves_icall_Type_GetPropertiesByName)
ICALL(MTYPE_16, "InternalGetEvent", ves_icall_MonoType_GetEvent)
ICALL(MTYPE_17, "IsByRefImpl", ves_icall_type_isbyref)
ICALL(MTYPE_18, "IsCOMObjectImpl", ves_icall_type_iscomobject)
ICALL(MTYPE_19, "IsPointerImpl", ves_icall_type_ispointer)
ICALL(MTYPE_20, "IsPrimitiveImpl", ves_icall_type_isprimitive)
ICALL(MTYPE_21, "getFullName", ves_icall_System_MonoType_getFullName)
ICALL(MTYPE_22, "get_Assembly", ves_icall_MonoType_get_Assembly)
ICALL(MTYPE_23, "get_BaseType", ves_icall_get_type_parent)
ICALL(MTYPE_24, "get_DeclaringMethod", ves_icall_MonoType_get_DeclaringMethod)
ICALL(MTYPE_25, "get_DeclaringType", ves_icall_MonoType_get_DeclaringType)
ICALL(MTYPE_26, "get_IsGenericParameter", ves_icall_MonoType_get_IsGenericParameter)
ICALL(MTYPE_27, "get_Module", ves_icall_MonoType_get_Module)
ICALL(MTYPE_28, "get_Name", ves_icall_MonoType_get_Name)
ICALL(MTYPE_29, "get_Namespace", ves_icall_MonoType_get_Namespace)
ICALL(MTYPE_31, "get_attributes", ves_icall_get_attributes)
ICALL(MTYPE_33, "get_core_clr_security_level", vell_icall_MonoType_get_core_clr_security_level)
ICALL(MTYPE_32, "type_from_obj", mono_type_type_from_obj)

#ifndef DISABLE_SOCKETS
ICALL_TYPE(NDNS, "System.Net.Dns", NDNS_1)
ICALL(NDNS_1, "GetHostByAddr_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByAddr_internal)
ICALL(NDNS_2, "GetHostByName_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByName_internal)
ICALL(NDNS_3, "GetHostName_internal(string&)", ves_icall_System_Net_Dns_GetHostName_internal)

ICALL_TYPE(SOCK, "System.Net.Sockets.Socket", SOCK_1)
ICALL(SOCK_1, "Accept_internal(intptr,int&,bool)", ves_icall_System_Net_Sockets_Socket_Accept_internal)
ICALL(SOCK_2, "Available_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Available_internal)
ICALL(SOCK_3, "Bind_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Bind_internal)
ICALL(SOCK_4, "Blocking_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Blocking_internal)
ICALL(SOCK_5, "Close_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Close_internal)
ICALL(SOCK_6, "Connect_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Connect_internal)
ICALL (SOCK_6a, "Disconnect_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Disconnect_internal)
ICALL(SOCK_7, "GetSocketOption_arr_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,byte[]&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_arr_internal)
ICALL(SOCK_8, "GetSocketOption_obj_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_obj_internal)
ICALL(SOCK_9, "Listen_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_Listen_internal)
ICALL(SOCK_10, "LocalEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_LocalEndPoint_internal)
ICALL(SOCK_11, "Poll_internal", ves_icall_System_Net_Sockets_Socket_Poll_internal)
ICALL(SOCK_11a, "Receive_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_array_internal)
ICALL(SOCK_12, "Receive_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_internal)
ICALL(SOCK_13, "RecvFrom_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress&,int&)", ves_icall_System_Net_Sockets_Socket_RecvFrom_internal)
ICALL(SOCK_14, "RemoteEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_RemoteEndPoint_internal)
ICALL(SOCK_15, "Select_internal(System.Net.Sockets.Socket[]&,int,int&)", ves_icall_System_Net_Sockets_Socket_Select_internal)
ICALL(SOCK_15a, "SendFile(intptr,string,byte[],byte[],System.Net.Sockets.TransmitFileOptions)", ves_icall_System_Net_Sockets_Socket_SendFile)
ICALL(SOCK_16, "SendTo_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_SendTo_internal)
ICALL(SOCK_16a, "Send_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_array_internal)
ICALL(SOCK_17, "Send_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_internal)
ICALL(SOCK_18, "SetSocketOption_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object,byte[],int,int&)", ves_icall_System_Net_Sockets_Socket_SetSocketOption_internal)
ICALL(SOCK_19, "Shutdown_internal(intptr,System.Net.Sockets.SocketShutdown,int&)", ves_icall_System_Net_Sockets_Socket_Shutdown_internal)
ICALL(SOCK_20, "Socket_internal(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType,int&)", ves_icall_System_Net_Sockets_Socket_Socket_internal)
ICALL(SOCK_21, "WSAIoctl(intptr,int,byte[],byte[],int&)", ves_icall_System_Net_Sockets_Socket_WSAIoctl)
ICALL(SOCK_21a, "cancel_blocking_socket_operation", icall_cancel_blocking_socket_operation)
ICALL(SOCK_22, "socket_pool_queue", icall_append_io_job)

ICALL_TYPE(SOCKEX, "System.Net.Sockets.SocketException", SOCKEX_1)
ICALL(SOCKEX_1, "WSAGetLastError_internal", ves_icall_System_Net_Sockets_SocketException_WSAGetLastError_internal)
#endif /* !DISABLE_SOCKETS */

ICALL_TYPE(NUMBER_FORMATTER, "System.NumberFormatter", NUMBER_FORMATTER_1)
ICALL(NUMBER_FORMATTER_1, "GetFormatterTables", ves_icall_System_NumberFormatter_GetFormatterTables)

ICALL_TYPE(OBJ, "System.Object", OBJ_1)
ICALL(OBJ_1, "GetType", ves_icall_System_Object_GetType)
ICALL(OBJ_2, "InternalGetHashCode", mono_object_hash)
ICALL(OBJ_3, "MemberwiseClone", ves_icall_System_Object_MemberwiseClone)
ICALL(OBJ_4, "obj_address", ves_icall_System_Object_obj_address)

ICALL_TYPE(ASSEM, "System.Reflection.Assembly", ASSEM_1)
ICALL(ASSEM_1, "FillName", ves_icall_System_Reflection_Assembly_FillName)
ICALL(ASSEM_2, "GetCallingAssembly", ves_icall_System_Reflection_Assembly_GetCallingAssembly)
ICALL(ASSEM_3, "GetEntryAssembly", ves_icall_System_Reflection_Assembly_GetEntryAssembly)
ICALL(ASSEM_4, "GetExecutingAssembly", ves_icall_System_Reflection_Assembly_GetExecutingAssembly)
ICALL(ASSEM_5, "GetFilesInternal", ves_icall_System_Reflection_Assembly_GetFilesInternal)
ICALL(ASSEM_6, "GetManifestModuleInternal", ves_icall_System_Reflection_Assembly_GetManifestModuleInternal)
ICALL(ASSEM_7, "GetManifestResourceInfoInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInfoInternal)
ICALL(ASSEM_8, "GetManifestResourceInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInternal)
ICALL(ASSEM_9, "GetManifestResourceNames", ves_icall_System_Reflection_Assembly_GetManifestResourceNames)
ICALL(ASSEM_10, "GetModulesInternal", ves_icall_System_Reflection_Assembly_GetModulesInternal)
//ICALL(ASSEM_11, "GetNamespaces", ves_icall_System_Reflection_Assembly_GetNamespaces)
ICALL(ASSEM_12, "GetReferencedAssemblies", ves_icall_System_Reflection_Assembly_GetReferencedAssemblies)
ICALL(ASSEM_13, "GetTypes", ves_icall_System_Reflection_Assembly_GetTypes)
ICALL(ASSEM_14, "InternalGetAssemblyName", ves_icall_System_Reflection_Assembly_InternalGetAssemblyName)
ICALL(ASSEM_15, "InternalGetType", ves_icall_System_Reflection_Assembly_InternalGetType)
ICALL(ASSEM_16, "InternalImageRuntimeVersion", ves_icall_System_Reflection_Assembly_InternalImageRuntimeVersion)
ICALL(ASSEM_17, "LoadFrom", ves_icall_System_Reflection_Assembly_LoadFrom)
ICALL(ASSEM_18, "LoadPermissions", ves_icall_System_Reflection_Assembly_LoadPermissions)
	/*
	 * Private icalls for the Mono Debugger
	 */
ICALL(ASSEM_19, "MonoDebugger_GetMethodToken", ves_icall_MonoDebugger_GetMethodToken)

	/* normal icalls again */
ICALL(ASSEM_20, "get_EntryPoint", ves_icall_System_Reflection_Assembly_get_EntryPoint)
ICALL(ASSEM_21, "get_ReflectionOnly", ves_icall_System_Reflection_Assembly_get_ReflectionOnly)
ICALL(ASSEM_22, "get_code_base", ves_icall_System_Reflection_Assembly_get_code_base)
ICALL(ASSEM_23, "get_fullname", ves_icall_System_Reflection_Assembly_get_fullName)
ICALL(ASSEM_24, "get_global_assembly_cache", ves_icall_System_Reflection_Assembly_get_global_assembly_cache)
ICALL(ASSEM_25, "get_location", ves_icall_System_Reflection_Assembly_get_location)
ICALL(ASSEM_26, "load_with_partial_name", ves_icall_System_Reflection_Assembly_load_with_partial_name)

ICALL_TYPE(ASSEMN, "System.Reflection.AssemblyName", ASSEMN_1)
ICALL(ASSEMN_1, "ParseName", ves_icall_System_Reflection_AssemblyName_ParseName)
ICALL(ASSEMN_2, "get_public_token", mono_digest_get_public_token)

ICALL_TYPE(CATTR_DATA, "System.Reflection.CustomAttributeData", CATTR_DATA_1)
ICALL(CATTR_DATA_1, "ResolveArgumentsInternal", mono_reflection_resolve_custom_attribute_data)

ICALL_TYPE(ASSEMB, "System.Reflection.Emit.AssemblyBuilder", ASSEMB_1)
ICALL(ASSEMB_1, "InternalAddModule", mono_image_load_module_dynamic)
ICALL(ASSEMB_2, "basic_init", mono_image_basic_init)

ICALL_TYPE(CATTRB, "System.Reflection.Emit.CustomAttributeBuilder", CATTRB_1)
ICALL(CATTRB_1, "GetBlob", mono_reflection_get_custom_attrs_blob)

#ifndef DISABLE_REFLECTION_EMIT
ICALL_TYPE(DERIVEDTYPE, "System.Reflection.Emit.DerivedType", DERIVEDTYPE_1)
ICALL(DERIVEDTYPE_1, "create_unmanaged_type", mono_reflection_create_unmanaged_type)
#endif

ICALL_TYPE(DYNM, "System.Reflection.Emit.DynamicMethod", DYNM_1)
ICALL(DYNM_1, "create_dynamic_method", mono_reflection_create_dynamic_method)

ICALL_TYPE(ENUMB, "System.Reflection.Emit.EnumBuilder", ENUMB_1)
ICALL(ENUMB_1, "setup_enum_type", ves_icall_EnumBuilder_setup_enum_type)

ICALL_TYPE(GPARB, "System.Reflection.Emit.GenericTypeParameterBuilder", GPARB_1)
ICALL(GPARB_1, "initialize", mono_reflection_initialize_generic_parameter)

ICALL_TYPE(METHODB, "System.Reflection.Emit.MethodBuilder", METHODB_1)
ICALL(METHODB_1, "MakeGenericMethod", mono_reflection_bind_generic_method_parameters)

ICALL_TYPE(MODULEB, "System.Reflection.Emit.ModuleBuilder", MODULEB_8)
ICALL(MODULEB_8, "RegisterToken", ves_icall_ModuleBuilder_RegisterToken)
ICALL(MODULEB_1, "WriteToFile", ves_icall_ModuleBuilder_WriteToFile)
ICALL(MODULEB_2, "basic_init", mono_image_module_basic_init)
ICALL(MODULEB_3, "build_metadata", ves_icall_ModuleBuilder_build_metadata)
ICALL(MODULEB_4, "create_modified_type", ves_icall_ModuleBuilder_create_modified_type)
ICALL(MODULEB_5, "getMethodToken", ves_icall_ModuleBuilder_getMethodToken)
ICALL(MODULEB_6, "getToken", ves_icall_ModuleBuilder_getToken)
ICALL(MODULEB_7, "getUSIndex", mono_image_insert_string)
ICALL(MODULEB_9, "set_wrappers_type", mono_image_set_wrappers_type)

ICALL_TYPE(SIGH, "System.Reflection.Emit.SignatureHelper", SIGH_1)
ICALL(SIGH_1, "get_signature_field", mono_reflection_sighelper_get_signature_field)
ICALL(SIGH_2, "get_signature_local", mono_reflection_sighelper_get_signature_local)

ICALL_TYPE(TYPEB, "System.Reflection.Emit.TypeBuilder", TYPEB_1)
ICALL(TYPEB_1, "create_generic_class", mono_reflection_create_generic_class)
ICALL(TYPEB_2, "create_internal_class", mono_reflection_create_internal_class)
ICALL(TYPEB_3, "create_runtime_class", mono_reflection_create_runtime_class)
ICALL(TYPEB_4, "get_IsGenericParameter", ves_icall_TypeBuilder_get_IsGenericParameter)
ICALL(TYPEB_5, "get_event_info", mono_reflection_event_builder_get_event_info)
ICALL(TYPEB_6, "setup_generic_class", mono_reflection_setup_generic_class)
ICALL(TYPEB_7, "setup_internal_class", mono_reflection_setup_internal_class)

ICALL_TYPE(FIELDI, "System.Reflection.FieldInfo", FILEDI_1)
ICALL(FILEDI_1, "GetTypeModifiers", ves_icall_System_Reflection_FieldInfo_GetTypeModifiers)
ICALL(FILEDI_2, "get_marshal_info", ves_icall_System_Reflection_FieldInfo_get_marshal_info)
ICALL(FILEDI_3, "internal_from_handle_type", ves_icall_System_Reflection_FieldInfo_internal_from_handle_type)

ICALL_TYPE(MEMBERI, "System.Reflection.MemberInfo", MEMBERI_1)
ICALL(MEMBERI_1, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MBASE, "System.Reflection.MethodBase", MBASE_1)
ICALL(MBASE_1, "GetCurrentMethod", ves_icall_GetCurrentMethod)
ICALL(MBASE_2, "GetMethodBodyInternal", ves_icall_System_Reflection_MethodBase_GetMethodBodyInternal)
ICALL(MBASE_3, "GetMethodFromHandleInternal", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternal)
ICALL(MBASE_4, "GetMethodFromHandleInternalType", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternalType)

ICALL_TYPE(MODULE, "System.Reflection.Module", MODULE_1)
ICALL(MODULE_1, "Close", ves_icall_System_Reflection_Module_Close)
ICALL(MODULE_2, "GetGlobalType", ves_icall_System_Reflection_Module_GetGlobalType)
ICALL(MODULE_3, "GetGuidInternal", ves_icall_System_Reflection_Module_GetGuidInternal)
ICALL(MODULE_14, "GetHINSTANCE", ves_icall_System_Reflection_Module_GetHINSTANCE)
ICALL(MODULE_4, "GetMDStreamVersion", ves_icall_System_Reflection_Module_GetMDStreamVersion)
ICALL(MODULE_5, "GetPEKind", ves_icall_System_Reflection_Module_GetPEKind)
ICALL(MODULE_6, "InternalGetTypes", ves_icall_System_Reflection_Module_InternalGetTypes)
ICALL(MODULE_7, "ResolveFieldToken", ves_icall_System_Reflection_Module_ResolveFieldToken)
ICALL(MODULE_8, "ResolveMemberToken", ves_icall_System_Reflection_Module_ResolveMemberToken)
ICALL(MODULE_9, "ResolveMethodToken", ves_icall_System_Reflection_Module_ResolveMethodToken)
ICALL(MODULE_10, "ResolveSignature", ves_icall_System_Reflection_Module_ResolveSignature)
ICALL(MODULE_11, "ResolveStringToken", ves_icall_System_Reflection_Module_ResolveStringToken)
ICALL(MODULE_12, "ResolveTypeToken", ves_icall_System_Reflection_Module_ResolveTypeToken)
ICALL(MODULE_13, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MCMETH, "System.Reflection.MonoCMethod", MCMETH_1)
ICALL(MCMETH_1, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MCMETH_2, "InternalInvoke", ves_icall_InternalInvoke)

ICALL_TYPE(MEVIN, "System.Reflection.MonoEventInfo", MEVIN_1)
ICALL(MEVIN_1, "get_event_info", ves_icall_get_event_info)

ICALL_TYPE(MFIELD, "System.Reflection.MonoField", MFIELD_1)
ICALL(MFIELD_1, "GetFieldOffset", ves_icall_MonoField_GetFieldOffset)
ICALL(MFIELD_2, "GetParentType", ves_icall_MonoField_GetParentType)
ICALL(MFIELD_5, "GetRawConstantValue", ves_icall_MonoField_GetRawConstantValue)
ICALL(MFIELD_3, "GetValueInternal", ves_icall_MonoField_GetValueInternal)
ICALL(MFIELD_6, "ResolveType", ves_icall_MonoField_ResolveType)
ICALL(MFIELD_4, "SetValueInternal", ves_icall_MonoField_SetValueInternal)

ICALL_TYPE(MGENCM, "System.Reflection.MonoGenericCMethod", MGENCM_1)
ICALL(MGENCM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MGENCL, "System.Reflection.MonoGenericClass", MGENCL_5)
ICALL(MGENCL_5, "initialize", mono_reflection_generic_class_initialize)
ICALL(MGENCL_6, "register_with_runtime", mono_reflection_register_with_runtime)

/* note this is the same as above: unify */
ICALL_TYPE(MGENM, "System.Reflection.MonoGenericMethod", MGENM_1)
ICALL(MGENM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MMETH, "System.Reflection.MonoMethod", MMETH_1)
ICALL(MMETH_1, "GetDllImportAttribute", ves_icall_MonoMethod_GetDllImportAttribute)
ICALL(MMETH_2, "GetGenericArguments", ves_icall_MonoMethod_GetGenericArguments)
ICALL(MMETH_3, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MMETH_4, "InternalInvoke", ves_icall_InternalInvoke)
ICALL(MMETH_5, "MakeGenericMethod_impl", mono_reflection_bind_generic_method_parameters)
ICALL(MMETH_6, "get_IsGenericMethod", ves_icall_MonoMethod_get_IsGenericMethod)
ICALL(MMETH_7, "get_IsGenericMethodDefinition", ves_icall_MonoMethod_get_IsGenericMethodDefinition)
ICALL(MMETH_8, "get_base_method", ves_icall_MonoMethod_get_base_method)
ICALL(MMETH_9, "get_name", ves_icall_MonoMethod_get_name)

ICALL_TYPE(MMETHI, "System.Reflection.MonoMethodInfo", MMETHI_4)
ICALL(MMETHI_4, "get_method_attributes", vell_icall_get_method_attributes)
ICALL(MMETHI_1, "get_method_info", ves_icall_get_method_info)
ICALL(MMETHI_2, "get_parameter_info", ves_icall_get_parameter_info)
ICALL(MMETHI_3, "get_retval_marshal", ves_icall_System_MonoMethodInfo_get_retval_marshal)

ICALL_TYPE(MPROPI, "System.Reflection.MonoPropertyInfo", MPROPI_1)
ICALL(MPROPI_1, "GetTypeModifiers", property_info_get_type_modifiers)
ICALL(MPROPI_3, "get_default_value", property_info_get_default_value)
ICALL(MPROPI_2, "get_property_info", ves_icall_get_property_info)

ICALL_TYPE(PARAMI, "System.Reflection.ParameterInfo", PARAMI_1)
ICALL(PARAMI_1, "GetMetadataToken", mono_reflection_get_token)
ICALL(PARAMI_2, "GetTypeModifiers", param_info_get_type_modifiers)

ICALL_TYPE(RUNH, "System.Runtime.CompilerServices.RuntimeHelpers", RUNH_1)
ICALL(RUNH_1, "GetObjectValue", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetObjectValue)
	 /* REMOVEME: no longer needed, just so we dont break things when not needed */
ICALL(RUNH_2, "GetOffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)
ICALL(RUNH_3, "InitializeArray", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_InitializeArray)
ICALL(RUNH_4, "RunClassConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunClassConstructor)
ICALL(RUNH_5, "RunModuleConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunModuleConstructor)
ICALL(RUNH_5h, "SufficientExecutionStack", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_SufficientExecutionStack)
ICALL(RUNH_6, "get_OffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)

ICALL_TYPE(GCH, "System.Runtime.InteropServices.GCHandle", GCH_1)
ICALL(GCH_1, "CheckCurrentDomain", GCHandle_CheckCurrentDomain)
ICALL(GCH_2, "FreeHandle", ves_icall_System_GCHandle_FreeHandle)
ICALL(GCH_3, "GetAddrOfPinnedObject", ves_icall_System_GCHandle_GetAddrOfPinnedObject)
ICALL(GCH_4, "GetTarget", ves_icall_System_GCHandle_GetTarget)
ICALL(GCH_5, "GetTargetHandle", ves_icall_System_GCHandle_GetTargetHandle)

#ifndef DISABLE_COM
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_1)
ICALL(MARSHAL_1, "AddRefInternal", ves_icall_System_Runtime_InteropServices_Marshal_AddRefInternal)
#else
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_2)
#endif
ICALL(MARSHAL_2, "AllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_AllocCoTaskMem)
ICALL(MARSHAL_3, "AllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_AllocHGlobal)
ICALL(MARSHAL_4, "DestroyStructure", ves_icall_System_Runtime_InteropServices_Marshal_DestroyStructure)
ICALL(MARSHAL_5, "FreeBSTR", ves_icall_System_Runtime_InteropServices_Marshal_FreeBSTR)
ICALL(MARSHAL_6, "FreeCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_FreeCoTaskMem)
ICALL(MARSHAL_7, "FreeHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_FreeHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_44, "GetCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetCCW)
ICALL(MARSHAL_8, "GetComSlotForMethodInfoInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetComSlotForMethodInfoInternal)
#endif
ICALL(MARSHAL_9, "GetDelegateForFunctionPointerInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetDelegateForFunctionPointerInternal)
ICALL(MARSHAL_10, "GetFunctionPointerForDelegateInternal", mono_delegate_to_ftnptr)
#ifndef DISABLE_COM
ICALL(MARSHAL_45, "GetIDispatchForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIDispatchForObjectInternal)
ICALL(MARSHAL_46, "GetIUnknownForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIUnknownForObjectInternal)
#endif
ICALL(MARSHAL_11, "GetLastWin32Error", ves_icall_System_Runtime_InteropServices_Marshal_GetLastWin32Error)
#ifndef DISABLE_COM
ICALL(MARSHAL_47, "GetObjectForCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetObjectForCCW)
ICALL(MARSHAL_48, "IsComObject", ves_icall_System_Runtime_InteropServices_Marshal_IsComObject)
#endif
ICALL(MARSHAL_12, "OffsetOf", ves_icall_System_Runtime_InteropServices_Marshal_OffsetOf)
ICALL(MARSHAL_13, "Prelink", ves_icall_System_Runtime_InteropServices_Marshal_Prelink)
ICALL(MARSHAL_14, "PrelinkAll", ves_icall_System_Runtime_InteropServices_Marshal_PrelinkAll)
ICALL(MARSHAL_15, "PtrToStringAnsi(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi)
ICALL(MARSHAL_16, "PtrToStringAnsi(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi_len)
#ifndef DISABLE_COM
ICALL(MARSHAL_17, "PtrToStringBSTR", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringBSTR)
#endif
ICALL(MARSHAL_18, "PtrToStringUni(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni)
ICALL(MARSHAL_19, "PtrToStringUni(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni_len)
ICALL(MARSHAL_20, "PtrToStructure(intptr,System.Type)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure_type)
ICALL(MARSHAL_21, "PtrToStructure(intptr,object)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure)
#ifndef DISABLE_COM
ICALL(MARSHAL_22, "QueryInterfaceInternal", ves_icall_System_Runtime_InteropServices_Marshal_QueryInterfaceInternal)
#endif
ICALL(MARSHAL_43, "ReAllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocCoTaskMem)
ICALL(MARSHAL_23, "ReAllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_49, "ReleaseComObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseComObjectInternal)
ICALL(MARSHAL_29, "ReleaseInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseInternal)
#endif
ICALL(MARSHAL_30, "SizeOf", ves_icall_System_Runtime_InteropServices_Marshal_SizeOf)
ICALL(MARSHAL_31, "StringToBSTR", ves_icall_System_Runtime_InteropServices_Marshal_StringToBSTR)
ICALL(MARSHAL_32, "StringToHGlobalAnsi", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalAnsi)
ICALL(MARSHAL_33, "StringToHGlobalUni", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalUni)
ICALL(MARSHAL_34, "StructureToPtr", ves_icall_System_Runtime_InteropServices_Marshal_StructureToPtr)
ICALL(MARSHAL_35, "UnsafeAddrOfPinnedArrayElement", ves_icall_System_Runtime_InteropServices_Marshal_UnsafeAddrOfPinnedArrayElement)
ICALL(MARSHAL_41, "copy_from_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_from_unmanaged)
ICALL(MARSHAL_42, "copy_to_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_to_unmanaged)

ICALL_TYPE(ACTS, "System.Runtime.Remoting.Activation.ActivationServices", ACTS_1)
ICALL(ACTS_1, "AllocateUninitializedClassInstance", ves_icall_System_Runtime_Activation_ActivationServices_AllocateUninitializedClassInstance)
ICALL(ACTS_2, "EnableProxyActivation", ves_icall_System_Runtime_Activation_ActivationServices_EnableProxyActivation)

ICALL_TYPE(MONOMM, "System.Runtime.Remoting.Messaging.MonoMethodMessage", MONOMM_1)
ICALL(MONOMM_1, "InitMessage", ves_icall_MonoMethodMessage_InitMessage)

#ifndef DISABLE_REMOTING
ICALL_TYPE(REALP, "System.Runtime.Remoting.Proxies.RealProxy", REALP_1)
ICALL(REALP_1, "InternalGetProxyType", ves_icall_Remoting_RealProxy_InternalGetProxyType)
ICALL(REALP_2, "InternalGetTransparentProxy", ves_icall_Remoting_RealProxy_GetTransparentProxy)

ICALL_TYPE(REMSER, "System.Runtime.Remoting.RemotingServices", REMSER_0)
ICALL(REMSER_0, "GetVirtualMethod", ves_icall_Remoting_RemotingServices_GetVirtualMethod)
ICALL(REMSER_1, "InternalExecute", ves_icall_InternalExecute)
ICALL(REMSER_2, "IsTransparentProxy", ves_icall_IsTransparentProxy)
#endif

ICALL_TYPE(MHAN, "System.RuntimeMethodHandle", MHAN_1)
ICALL(MHAN_1, "GetFunctionPointer", ves_icall_RuntimeMethod_GetFunctionPointer)

ICALL_TYPE(RNG, "System.Security.Cryptography.RNGCryptoServiceProvider", RNG_1)
ICALL(RNG_1, "RngClose", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngClose)
ICALL(RNG_2, "RngGetBytes", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngGetBytes)
ICALL(RNG_3, "RngInitialize", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngInitialize)
ICALL(RNG_4, "RngOpen", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngOpen)

#ifndef DISABLE_POLICY_EVIDENCE
ICALL_TYPE(EVID, "System.Security.Policy.Evidence", EVID_1)
ICALL(EVID_1, "IsAuthenticodePresent", ves_icall_System_Security_Policy_Evidence_IsAuthenticodePresent)

ICALL_TYPE(WINID, "System.Security.Principal.WindowsIdentity", WINID_1)
ICALL(WINID_1, "GetCurrentToken", ves_icall_System_Security_Principal_WindowsIdentity_GetCurrentToken)
ICALL(WINID_2, "GetTokenName", ves_icall_System_Security_Principal_WindowsIdentity_GetTokenName)
ICALL(WINID_3, "GetUserToken", ves_icall_System_Security_Principal_WindowsIdentity_GetUserToken)
ICALL(WINID_4, "_GetRoles", ves_icall_System_Security_Principal_WindowsIdentity_GetRoles)

ICALL_TYPE(WINIMP, "System.Security.Principal.WindowsImpersonationContext", WINIMP_1)
ICALL(WINIMP_1, "CloseToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_CloseToken)
ICALL(WINIMP_2, "DuplicateToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_DuplicateToken)
ICALL(WINIMP_3, "RevertToSelf", ves_icall_System_Security_Principal_WindowsImpersonationContext_RevertToSelf)
ICALL(WINIMP_4, "SetCurrentToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_SetCurrentToken)

ICALL_TYPE(WINPRIN, "System.Security.Principal.WindowsPrincipal", WINPRIN_1)
ICALL(WINPRIN_1, "IsMemberOfGroupId", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupId)
ICALL(WINPRIN_2, "IsMemberOfGroupName", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupName)

ICALL_TYPE(SECSTRING, "System.Security.SecureString", SECSTRING_1)
ICALL(SECSTRING_1, "DecryptInternal", ves_icall_System_Security_SecureString_DecryptInternal)
ICALL(SECSTRING_2, "EncryptInternal", ves_icall_System_Security_SecureString_EncryptInternal)
#endif /* !DISABLE_POLICY_EVIDENCE */

ICALL_TYPE(SECMAN, "System.Security.SecurityManager", SECMAN_1)
ICALL(SECMAN_1, "GetLinkDemandSecurity", ves_icall_System_Security_SecurityManager_GetLinkDemandSecurity)
ICALL(SECMAN_2, "get_CheckExecutionRights", ves_icall_System_Security_SecurityManager_get_CheckExecutionRights)
ICALL(SECMAN_3, "get_RequiresElevatedPermissions", mono_security_core_clr_require_elevated_permissions)
ICALL(SECMAN_4, "get_SecurityEnabled", ves_icall_System_Security_SecurityManager_get_SecurityEnabled)
ICALL(SECMAN_5, "set_CheckExecutionRights", ves_icall_System_Security_SecurityManager_set_CheckExecutionRights)
ICALL(SECMAN_6, "set_SecurityEnabled", ves_icall_System_Security_SecurityManager_set_SecurityEnabled)

ICALL_TYPE(STRING, "System.String", STRING_1)
ICALL(STRING_1, ".ctor(char*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_2, ".ctor(char*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_3, ".ctor(char,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_4, ".ctor(char[])", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_5, ".ctor(char[],int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_6, ".ctor(sbyte*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_7, ".ctor(sbyte*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8, ".ctor(sbyte*,int,int,System.Text.Encoding)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8a, "GetLOSLimit", ves_icall_System_String_GetLOSLimit)
ICALL(STRING_9, "InternalAllocateStr", ves_icall_System_String_InternalAllocateStr)
ICALL(STRING_10, "InternalIntern", ves_icall_System_String_InternalIntern)
ICALL(STRING_11, "InternalIsInterned", ves_icall_System_String_InternalIsInterned)

ICALL_TYPE(TENC, "System.Text.Encoding", TENC_1)
ICALL(TENC_1, "InternalCodePage", ves_icall_System_Text_Encoding_InternalCodePage)

ICALL_TYPE(ILOCK, "System.Threading.Interlocked", ILOCK_1)
ICALL(ILOCK_1, "Add(int&,int)", ves_icall_System_Threading_Interlocked_Add_Int)
ICALL(ILOCK_2, "Add(long&,long)", ves_icall_System_Threading_Interlocked_Add_Long)
ICALL(ILOCK_3, "CompareExchange(T&,T,T)", ves_icall_System_Threading_Interlocked_CompareExchange_T)
ICALL(ILOCK_4, "CompareExchange(double&,double,double)", ves_icall_System_Threading_Interlocked_CompareExchange_Double)
ICALL(ILOCK_5, "CompareExchange(int&,int,int)", ves_icall_System_Threading_Interlocked_CompareExchange_Int)
ICALL(ILOCK_6, "CompareExchange(intptr&,intptr,intptr)", ves_icall_System_Threading_Interlocked_CompareExchange_IntPtr)
ICALL(ILOCK_7, "CompareExchange(long&,long,long)", ves_icall_System_Threading_Interlocked_CompareExchange_Long)
ICALL(ILOCK_8, "CompareExchange(object&,object,object)", ves_icall_System_Threading_Interlocked_CompareExchange_Object)
ICALL(ILOCK_9, "CompareExchange(single&,single,single)", ves_icall_System_Threading_Interlocked_CompareExchange_Single)
ICALL(ILOCK_10, "Decrement(int&)", ves_icall_System_Threading_Interlocked_Decrement_Int)
ICALL(ILOCK_11, "Decrement(long&)", ves_icall_System_Threading_Interlocked_Decrement_Long)
ICALL(ILOCK_12, "Exchange(T&,T)", ves_icall_System_Threading_Interlocked_Exchange_T)
ICALL(ILOCK_13, "Exchange(double&,double)", ves_icall_System_Threading_Interlocked_Exchange_Double)
ICALL(ILOCK_14, "Exchange(int&,int)", ves_icall_System_Threading_Interlocked_Exchange_Int)
ICALL(ILOCK_15, "Exchange(intptr&,intptr)", ves_icall_System_Threading_Interlocked_Exchange_IntPtr)
ICALL(ILOCK_16, "Exchange(long&,long)", ves_icall_System_Threading_Interlocked_Exchange_Long)
ICALL(ILOCK_17, "Exchange(object&,object)", ves_icall_System_Threading_Interlocked_Exchange_Object)
ICALL(ILOCK_18, "Exchange(single&,single)", ves_icall_System_Threading_Interlocked_Exchange_Single)
ICALL(ILOCK_19, "Increment(int&)", ves_icall_System_Threading_Interlocked_Increment_Int)
ICALL(ILOCK_20, "Increment(long&)", ves_icall_System_Threading_Interlocked_Increment_Long)
ICALL(ILOCK_21, "Read(long&)", ves_icall_System_Threading_Interlocked_Read_Long)

ICALL_TYPE(ITHREAD, "System.Threading.InternalThread", ITHREAD_1)
ICALL(ITHREAD_1, "Thread_free_internal", ves_icall_System_Threading_InternalThread_Thread_free_internal)

ICALL_TYPE(MONIT, "System.Threading.Monitor", MONIT_8)
ICALL(MONIT_8, "Enter", mono_monitor_enter)
ICALL(MONIT_1, "Exit", mono_monitor_exit)
ICALL(MONIT_2, "Monitor_pulse", ves_icall_System_Threading_Monitor_Monitor_pulse)
ICALL(MONIT_3, "Monitor_pulse_all", ves_icall_System_Threading_Monitor_Monitor_pulse_all)
ICALL(MONIT_4, "Monitor_test_owner", ves_icall_System_Threading_Monitor_Monitor_test_owner)
ICALL(MONIT_5, "Monitor_test_synchronised", ves_icall_System_Threading_Monitor_Monitor_test_synchronised)
ICALL(MONIT_6, "Monitor_try_enter", ves_icall_System_Threading_Monitor_Monitor_try_enter)
ICALL(MONIT_7, "Monitor_wait", ves_icall_System_Threading_Monitor_Monitor_wait)
ICALL(MONIT_9, "try_enter_with_atomic_var", ves_icall_System_Threading_Monitor_Monitor_try_enter_with_atomic_var)

ICALL_TYPE(MUTEX, "System.Threading.Mutex", MUTEX_1)
ICALL(MUTEX_1, "CreateMutex_internal(bool,string,bool&)", ves_icall_System_Threading_Mutex_CreateMutex_internal)
ICALL(MUTEX_2, "OpenMutex_internal(string,System.Security.AccessControl.MutexRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Mutex_OpenMutex_internal)
ICALL(MUTEX_3, "ReleaseMutex_internal(intptr)", ves_icall_System_Threading_Mutex_ReleaseMutex_internal)

ICALL_TYPE(NATIVEC, "System.Threading.NativeEventCalls", NATIVEC_1)
ICALL(NATIVEC_1, "CloseEvent_internal", ves_icall_System_Threading_Events_CloseEvent_internal)
ICALL(NATIVEC_2, "CreateEvent_internal(bool,bool,string,bool&)", ves_icall_System_Threading_Events_CreateEvent_internal)
ICALL(NATIVEC_3, "OpenEvent_internal(string,System.Security.AccessControl.EventWaitHandleRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Events_OpenEvent_internal)
ICALL(NATIVEC_4, "ResetEvent_internal",  ves_icall_System_Threading_Events_ResetEvent_internal)
ICALL(NATIVEC_5, "SetEvent_internal",    ves_icall_System_Threading_Events_SetEvent_internal)

ICALL_TYPE(SEMA, "System.Threading.Semaphore", SEMA_1)
ICALL(SEMA_1, "CreateSemaphore_internal(int,int,string,bool&)", ves_icall_System_Threading_Semaphore_CreateSemaphore_internal)
ICALL(SEMA_2, "OpenSemaphore_internal(string,System.Security.AccessControl.SemaphoreRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Semaphore_OpenSemaphore_internal)
ICALL(SEMA_3, "ReleaseSemaphore_internal(intptr,int,bool&)", ves_icall_System_Threading_Semaphore_ReleaseSemaphore_internal)

ICALL_TYPE(THREAD, "System.Threading.Thread", THREAD_1)
ICALL(THREAD_1, "Abort_internal(System.Threading.InternalThread,object)", ves_icall_System_Threading_Thread_Abort)
ICALL(THREAD_1aa, "AllocTlsData", mono_thread_alloc_tls)
ICALL(THREAD_1a, "ByteArrayToCurrentDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToCurrentDomain)
ICALL(THREAD_1b, "ByteArrayToRootDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToRootDomain)
ICALL(THREAD_2, "ClrState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_ClrState)
ICALL(THREAD_2a, "ConstructInternalThread", ves_icall_System_Threading_Thread_ConstructInternalThread)
ICALL(THREAD_3, "CurrentInternalThread_internal", mono_thread_internal_current)
ICALL(THREAD_3a, "DestroyTlsData", mono_thread_destroy_tls)
ICALL(THREAD_4, "FreeLocalSlotValues", mono_thread_free_local_slot_values)
ICALL(THREAD_55, "GetAbortExceptionState", ves_icall_System_Threading_Thread_GetAbortExceptionState)
ICALL(THREAD_7, "GetDomainID", ves_icall_System_Threading_Thread_GetDomainID)
ICALL(THREAD_8, "GetName_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetName_internal)
ICALL(THREAD_11, "GetState(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetState)
ICALL(THREAD_53, "Interrupt_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Interrupt_internal)
ICALL(THREAD_12, "Join_internal(System.Threading.InternalThread,int,intptr)", ves_icall_System_Threading_Thread_Join_internal)
ICALL(THREAD_13, "MemoryBarrier", ves_icall_System_Threading_Thread_MemoryBarrier)
ICALL(THREAD_14, "ResetAbort_internal()", ves_icall_System_Threading_Thread_ResetAbort)
ICALL(THREAD_15, "Resume_internal()", ves_icall_System_Threading_Thread_Resume)
ICALL(THREAD_18, "SetName_internal(System.Threading.InternalThread,string)", ves_icall_System_Threading_Thread_SetName_internal)
ICALL(THREAD_21, "SetState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_SetState)
ICALL(THREAD_22, "Sleep_internal", ves_icall_System_Threading_Thread_Sleep_internal)
ICALL(THREAD_54, "SpinWait_nop", ves_icall_System_Threading_Thread_SpinWait_nop)
ICALL(THREAD_23, "Suspend_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Suspend)
ICALL(THREAD_25, "Thread_internal", ves_icall_System_Threading_Thread_Thread_internal)
ICALL(THREAD_26, "VolatileRead(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_27, "VolatileRead(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(THREAD_28, "VolatileRead(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_29, "VolatileRead(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_30, "VolatileRead(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_31, "VolatileRead(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_32, "VolatileRead(object&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_33, "VolatileRead(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_34, "VolatileRead(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(THREAD_35, "VolatileRead(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_36, "VolatileRead(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_37, "VolatileRead(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_38, "VolatileRead(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_39, "VolatileWrite(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_40, "VolatileWrite(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(THREAD_41, "VolatileWrite(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_42, "VolatileWrite(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_43, "VolatileWrite(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_44, "VolatileWrite(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_45, "VolatileWrite(object&,object)", ves_icall_System_Threading_Thread_VolatileWriteObject)
ICALL(THREAD_46, "VolatileWrite(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_47, "VolatileWrite(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(THREAD_48, "VolatileWrite(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_49, "VolatileWrite(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_50, "VolatileWrite(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_51, "VolatileWrite(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_9, "Yield", ves_icall_System_Threading_Thread_Yield)
ICALL(THREAD_52, "current_lcid()", ves_icall_System_Threading_Thread_current_lcid)

ICALL_TYPE(THREADP, "System.Threading.ThreadPool", THREADP_1)
ICALL(THREADP_1, "GetAvailableThreads", ves_icall_System_Threading_ThreadPool_GetAvailableThreads)
ICALL(THREADP_2, "GetMaxThreads", ves_icall_System_Threading_ThreadPool_GetMaxThreads)
ICALL(THREADP_3, "GetMinThreads", ves_icall_System_Threading_ThreadPool_GetMinThreads)
ICALL(THREADP_35, "SetMaxThreads", ves_icall_System_Threading_ThreadPool_SetMaxThreads)
ICALL(THREADP_4, "SetMinThreads", ves_icall_System_Threading_ThreadPool_SetMinThreads)
ICALL(THREADP_5, "pool_queue", icall_append_job)

ICALL_TYPE(VOLATILE, "System.Threading.Volatile", VOLATILE_28)
ICALL(VOLATILE_28, "Read(T&)", ves_icall_System_Threading_Volatile_Read_T)
ICALL(VOLATILE_1, "Read(bool&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_2, "Read(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_3, "Read(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(VOLATILE_4, "Read(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_5, "Read(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_6, "Read(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_7, "Read(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_8, "Read(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_9, "Read(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(VOLATILE_10, "Read(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_11, "Read(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_12, "Read(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_13, "Read(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_27, "Write(T&,T)", ves_icall_System_Threading_Volatile_Write_T)
ICALL(VOLATILE_14, "Write(bool&,bool)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_15, "Write(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_16, "Write(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(VOLATILE_17, "Write(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_18, "Write(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_19, "Write(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_20, "Write(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(VOLATILE_21, "Write(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_22, "Write(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(VOLATILE_23, "Write(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_24, "Write(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_25, "Write(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_26, "Write(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)

ICALL_TYPE(WAITH, "System.Threading.WaitHandle", WAITH_1)
ICALL(WAITH_1, "SignalAndWait_Internal", ves_icall_System_Threading_WaitHandle_SignalAndWait_Internal)
ICALL(WAITH_2, "WaitAll_internal", ves_icall_System_Threading_WaitHandle_WaitAll_internal)
ICALL(WAITH_3, "WaitAny_internal", ves_icall_System_Threading_WaitHandle_WaitAny_internal)
ICALL(WAITH_4, "WaitOne_internal", ves_icall_System_Threading_WaitHandle_WaitOne_internal)

ICALL_TYPE(TYPE, "System.Type", TYPE_1)
ICALL(TYPE_1, "EqualsInternal", ves_icall_System_Type_EqualsInternal)
ICALL(TYPE_2, "GetGenericParameterAttributes", ves_icall_Type_GetGenericParameterAttributes)
ICALL(TYPE_3, "GetGenericParameterConstraints_impl", ves_icall_Type_GetGenericParameterConstraints)
ICALL(TYPE_4, "GetGenericParameterPosition", ves_icall_Type_GetGenericParameterPosition)
ICALL(TYPE_5, "GetGenericTypeDefinition_impl", ves_icall_Type_GetGenericTypeDefinition_impl)
ICALL(TYPE_6, "GetInterfaceMapData", ves_icall_Type_GetInterfaceMapData)
ICALL(TYPE_7, "GetPacking", ves_icall_Type_GetPacking)
ICALL(TYPE_8, "GetTypeCode", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_9, "GetTypeCodeInternal", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_10, "IsArrayImpl", ves_icall_Type_IsArrayImpl)
ICALL(TYPE_11, "IsInstanceOfType", ves_icall_type_IsInstanceOfType)
ICALL(TYPE_12, "MakeGenericType", ves_icall_Type_MakeGenericType)
ICALL(TYPE_13, "MakePointerType", ves_icall_Type_MakePointerType)
ICALL(TYPE_14, "get_IsGenericInstance", ves_icall_Type_get_IsGenericInstance)
ICALL(TYPE_15, "get_IsGenericType", ves_icall_Type_get_IsGenericType)
ICALL(TYPE_16, "get_IsGenericTypeDefinition", ves_icall_Type_get_IsGenericTypeDefinition)
ICALL(TYPE_17, "internal_from_handle", ves_icall_type_from_handle)
ICALL(TYPE_18, "internal_from_name", ves_icall_type_from_name)
ICALL(TYPE_19, "make_array_type", ves_icall_Type_make_array_type)
ICALL(TYPE_20, "make_byref_type", ves_icall_Type_make_byref_type)
ICALL(TYPE_21, "type_is_assignable_from", ves_icall_type_is_assignable_from)
ICALL(TYPE_22, "type_is_subtype_of", ves_icall_type_is_subtype_of)

ICALL_TYPE(TYPEDR, "System.TypedReference", TYPEDR_1)
ICALL(TYPEDR_1, "ToObject",	mono_TypedReference_ToObject)
ICALL(TYPEDR_2, "ToObjectInternal",	mono_TypedReference_ToObjectInternal)

ICALL_TYPE(VALUET, "System.ValueType", VALUET_1)
ICALL(VALUET_1, "InternalEquals", ves_icall_System_ValueType_Equals)
ICALL(VALUET_2, "InternalGetHashCode", ves_icall_System_ValueType_InternalGetHashCode)

ICALL_TYPE(WEBIC, "System.Web.Util.ICalls", WEBIC_1)
ICALL(WEBIC_1, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(WEBIC_2, "GetMachineInstallDirectory", ves_icall_System_Web_Util_ICalls_get_machine_install_dir)
ICALL(WEBIC_3, "GetUnmanagedResourcesPtr", ves_icall_get_resources_ptr)

#ifndef DISABLE_COM
ICALL_TYPE(COMOBJ, "System.__ComObject", COMOBJ_1)
ICALL(COMOBJ_1, "CreateRCW", ves_icall_System_ComObject_CreateRCW)
ICALL(COMOBJ_2, "GetInterfaceInternal", ves_icall_System_ComObject_GetInterfaceInternal)
ICALL(COMOBJ_3, "ReleaseInterfaces", ves_icall_System_ComObject_ReleaseInterfaces)
#endif
#undef ICALL_TYPE
};


static const guint16 icall_type_names_idx [] = {
#define ICALL_TYPE(id,name,first) [Icall_type_ ## id] = offsetof (struct msgstrtn_t, MSGSTRFIELD(__LINE__)),
// #include "metadata/icall-def.h"
[Icall_type_UNORM] = __builtin_offsetof (struct msgstrtn_t, str39),

[Icall_type_COMPROX] = __builtin_offsetof (struct msgstrtn_t, str43),


[Icall_type_RUNTIME] = __builtin_offsetof (struct msgstrtn_t, str48),



[Icall_type_KPAIR] = __builtin_offsetof (struct msgstrtn_t, str54),





[Icall_type_ACTIV] = __builtin_offsetof (struct msgstrtn_t, str62),

[Icall_type_APPDOM] = __builtin_offsetof (struct msgstrtn_t, str65),






















[Icall_type_ARGI] = __builtin_offsetof (struct msgstrtn_t, str89),




[Icall_type_ARRAY] = __builtin_offsetof (struct msgstrtn_t, str95),















[Icall_type_BUFFER] = __builtin_offsetof (struct msgstrtn_t, str112),




[Icall_type_CHAR] = __builtin_offsetof (struct msgstrtn_t, str118),

[Icall_type_COMPO_W] = __builtin_offsetof (struct msgstrtn_t, str121),

[Icall_type_DEFAULTC] = __builtin_offsetof (struct msgstrtn_t, str124),


[Icall_type_INTCFGHOST] = __builtin_offsetof (struct msgstrtn_t, str129),


[Icall_type_CONSOLE] = __builtin_offsetof (struct msgstrtn_t, str133),





[Icall_type_CONVERT] = __builtin_offsetof (struct msgstrtn_t, str140),


[Icall_type_TZONE] = __builtin_offsetof (struct msgstrtn_t, str144),

[Icall_type_DTIME] = __builtin_offsetof (struct msgstrtn_t, str147),


[Icall_type_DECIMAL] = __builtin_offsetof (struct msgstrtn_t, str152),













[Icall_type_DELEGATE] = __builtin_offsetof (struct msgstrtn_t, str169),


[Icall_type_DEBUGR] = __builtin_offsetof (struct msgstrtn_t, str173),



[Icall_type_TRACEL] = __builtin_offsetof (struct msgstrtn_t, str178),

[Icall_type_FILEV] = __builtin_offsetof (struct msgstrtn_t, str181),

[Icall_type_PERFCTR] = __builtin_offsetof (struct msgstrtn_t, str185),




[Icall_type_PERFCTRCAT] = __builtin_offsetof (struct msgstrtn_t, str191),








[Icall_type_PROCESS] = __builtin_offsetof (struct msgstrtn_t, str201),




















[Icall_type_PROCESSHANDLE] = __builtin_offsetof (struct msgstrtn_t, str223),


[Icall_type_STOPWATCH] = __builtin_offsetof (struct msgstrtn_t, str228),

[Icall_type_DOUBLE] = __builtin_offsetof (struct msgstrtn_t, str231),

[Icall_type_ENUM] = __builtin_offsetof (struct msgstrtn_t, str234),





[Icall_type_ENV] = __builtin_offsetof (struct msgstrtn_t, str241),






















[Icall_type_GC] = __builtin_offsetof (struct msgstrtn_t, str265),












[Icall_type_COMPINF] = __builtin_offsetof (struct msgstrtn_t, str279),






[Icall_type_CULINF] = __builtin_offsetof (struct msgstrtn_t, str287),







[Icall_type_REGINF] = __builtin_offsetof (struct msgstrtn_t, str297),


[Icall_type_IODRIVEINFO] = __builtin_offsetof (struct msgstrtn_t, str302),



[Icall_type_FAMW] = __builtin_offsetof (struct msgstrtn_t, str308),

[Icall_type_FILEW] = __builtin_offsetof (struct msgstrtn_t, str311),

[Icall_type_INOW] = __builtin_offsetof (struct msgstrtn_t, str314),



[Icall_type_MONOIO] = __builtin_offsetof (struct msgstrtn_t, str326),






































[Icall_type_IOPATH] = __builtin_offsetof (struct msgstrtn_t, str374),

[Icall_type_MATH] = __builtin_offsetof (struct msgstrtn_t, str377),


















[Icall_type_MCATTR] = __builtin_offsetof (struct msgstrtn_t, str397),



[Icall_type_MENUM] = __builtin_offsetof (struct msgstrtn_t, str402),

[Icall_type_MTYPE] = __builtin_offsetof (struct msgstrtn_t, str405),
































[Icall_type_NDNS] = __builtin_offsetof (struct msgstrtn_t, str440),



[Icall_type_SOCK] = __builtin_offsetof (struct msgstrtn_t, str445),



























[Icall_type_SOCKEX] = __builtin_offsetof (struct msgstrtn_t, str474),

[Icall_type_NUMBER_FORMATTER] = __builtin_offsetof (struct msgstrtn_t, str478),

[Icall_type_OBJ] = __builtin_offsetof (struct msgstrtn_t, str481),




[Icall_type_ASSEM] = __builtin_offsetof (struct msgstrtn_t, str487),

























[Icall_type_ASSEMN] = __builtin_offsetof (struct msgstrtn_t, str520),


[Icall_type_CATTR_DATA] = __builtin_offsetof (struct msgstrtn_t, str524),

[Icall_type_ASSEMB] = __builtin_offsetof (struct msgstrtn_t, str527),


[Icall_type_CATTRB] = __builtin_offsetof (struct msgstrtn_t, str531),

[Icall_type_DERIVEDTYPE] = __builtin_offsetof (struct msgstrtn_t, str535),

[Icall_type_DYNM] = __builtin_offsetof (struct msgstrtn_t, str539),

[Icall_type_ENUMB] = __builtin_offsetof (struct msgstrtn_t, str542),

[Icall_type_GPARB] = __builtin_offsetof (struct msgstrtn_t, str545),

[Icall_type_METHODB] = __builtin_offsetof (struct msgstrtn_t, str548),

[Icall_type_MODULEB] = __builtin_offsetof (struct msgstrtn_t, str551),









[Icall_type_SIGH] = __builtin_offsetof (struct msgstrtn_t, str562),


[Icall_type_TYPEB] = __builtin_offsetof (struct msgstrtn_t, str566),







[Icall_type_FIELDI] = __builtin_offsetof (struct msgstrtn_t, str575),



[Icall_type_MEMBERI] = __builtin_offsetof (struct msgstrtn_t, str580),

[Icall_type_MBASE] = __builtin_offsetof (struct msgstrtn_t, str583),




[Icall_type_MODULE] = __builtin_offsetof (struct msgstrtn_t, str589),














[Icall_type_MCMETH] = __builtin_offsetof (struct msgstrtn_t, str605),


[Icall_type_MEVIN] = __builtin_offsetof (struct msgstrtn_t, str609),

[Icall_type_MFIELD] = __builtin_offsetof (struct msgstrtn_t, str612),






[Icall_type_MGENCM] = __builtin_offsetof (struct msgstrtn_t, str620),

[Icall_type_MGENCL] = __builtin_offsetof (struct msgstrtn_t, str623),


[Icall_type_MGENM] = __builtin_offsetof (struct msgstrtn_t, str628),

[Icall_type_MMETH] = __builtin_offsetof (struct msgstrtn_t, str631),









[Icall_type_MMETHI] = __builtin_offsetof (struct msgstrtn_t, str642),




[Icall_type_MPROPI] = __builtin_offsetof (struct msgstrtn_t, str648),



[Icall_type_PARAMI] = __builtin_offsetof (struct msgstrtn_t, str653),


[Icall_type_RUNH] = __builtin_offsetof (struct msgstrtn_t, str657),







[Icall_type_GCH] = __builtin_offsetof (struct msgstrtn_t, str667),





[Icall_type_MARSHAL] = __builtin_offsetof (struct msgstrtn_t, str675),







































[Icall_type_ACTS] = __builtin_offsetof (struct msgstrtn_t, str731),


[Icall_type_MONOMM] = __builtin_offsetof (struct msgstrtn_t, str735),

[Icall_type_REALP] = __builtin_offsetof (struct msgstrtn_t, str739),


[Icall_type_REMSER] = __builtin_offsetof (struct msgstrtn_t, str743),



[Icall_type_MHAN] = __builtin_offsetof (struct msgstrtn_t, str749),

[Icall_type_RNG] = __builtin_offsetof (struct msgstrtn_t, str752),




[Icall_type_EVID] = __builtin_offsetof (struct msgstrtn_t, str759),

[Icall_type_WINID] = __builtin_offsetof (struct msgstrtn_t, str762),




[Icall_type_WINIMP] = __builtin_offsetof (struct msgstrtn_t, str768),




[Icall_type_WINPRIN] = __builtin_offsetof (struct msgstrtn_t, str774),


[Icall_type_SECSTRING] = __builtin_offsetof (struct msgstrtn_t, str778),


[Icall_type_SECMAN] = __builtin_offsetof (struct msgstrtn_t, str783),






[Icall_type_STRING] = __builtin_offsetof (struct msgstrtn_t, str791),












[Icall_type_TENC] = __builtin_offsetof (struct msgstrtn_t, str805),

[Icall_type_ILOCK] = __builtin_offsetof (struct msgstrtn_t, str808),





















[Icall_type_ITHREAD] = __builtin_offsetof (struct msgstrtn_t, str831),

[Icall_type_MONIT] = __builtin_offsetof (struct msgstrtn_t, str834),









[Icall_type_MUTEX] = __builtin_offsetof (struct msgstrtn_t, str845),



[Icall_type_NATIVEC] = __builtin_offsetof (struct msgstrtn_t, str850),





[Icall_type_SEMA] = __builtin_offsetof (struct msgstrtn_t, str857),



[Icall_type_THREAD] = __builtin_offsetof (struct msgstrtn_t, str862),




















































[Icall_type_THREADP] = __builtin_offsetof (struct msgstrtn_t, str916),






[Icall_type_VOLATILE] = __builtin_offsetof (struct msgstrtn_t, str924),




























[Icall_type_WAITH] = __builtin_offsetof (struct msgstrtn_t, str954),




[Icall_type_TYPE] = __builtin_offsetof (struct msgstrtn_t, str960),






















[Icall_type_TYPEDR] = __builtin_offsetof (struct msgstrtn_t, str984),


[Icall_type_VALUET] = __builtin_offsetof (struct msgstrtn_t, str988),


[Icall_type_WEBIC] = __builtin_offsetof (struct msgstrtn_t, str992),



[Icall_type_COMOBJ] = __builtin_offsetof (struct msgstrtn_t, str998),

#undef ICALL_TYPE
};
#define icall_type_name_get(id) ((const char*)&icall_type_names_str + icall_type_names_idx [(id)])


static const struct msgstr_t {
#undef ICALL
#define ICALL_TYPE(id,name,first)
#define ICALL(id,name,func) char MSGSTRFIELD(__LINE__) [sizeof (name)];
// #include "metadata/icall-def.h"
char str40 [sizeof ("load_normalization_resource")];

char str44 [sizeof ("AddProxy")];
char str45 [sizeof ("FindProxy")];

char str49 [sizeof ("GetDisplayName")];
char str50 [sizeof ("GetNativeStackTrace")];
char str51 [sizeof ("SetGCAllowSynchronousMajor")];

char str55 [sizeof ("_CanSecure")];
char str56 [sizeof ("_IsMachineProtected")];
char str57 [sizeof ("_IsUserProtected")];
char str58 [sizeof ("_ProtectMachine")];
char str59 [sizeof ("_ProtectUser")];

char str63 [sizeof ("CreateInstanceInternal")];

char str66 [sizeof ("ExecuteAssembly")];
char str67 [sizeof ("GetAssemblies")];
char str68 [sizeof ("GetData")];
char str69 [sizeof ("InternalGetContext")];
char str70 [sizeof ("InternalGetDefaultContext")];
char str71 [sizeof ("InternalGetProcessGuid")];
char str72 [sizeof ("InternalIsFinalizingForUnload")];
char str73 [sizeof ("InternalPopDomainRef")];
char str74 [sizeof ("InternalPushDomainRef")];
char str75 [sizeof ("InternalPushDomainRefByID")];
char str76 [sizeof ("InternalSetContext")];
char str77 [sizeof ("InternalSetDomain")];
char str78 [sizeof ("InternalSetDomainByID")];
char str79 [sizeof ("InternalUnload")];
char str80 [sizeof ("LoadAssembly")];
char str81 [sizeof ("LoadAssemblyRaw")];
char str82 [sizeof ("SetData")];
char str83 [sizeof ("createDomain")];
char str84 [sizeof ("getCurDomain")];
char str85 [sizeof ("getFriendlyName")];
char str86 [sizeof ("getRootDomain")];
char str87 [sizeof ("getSetup")];

char str90 [sizeof ("IntGetNextArg()")];
char str91 [sizeof ("IntGetNextArg(intptr)")];
char str92 [sizeof ("IntGetNextArgType")];
char str93 [sizeof ("Setup")];

char str96 [sizeof ("ClearInternal")];
char str97 [sizeof ("Clone")];
char str98 [sizeof ("CreateInstanceImpl")];
char str99 [sizeof ("CreateInstanceImpl64")];
char str100 [sizeof ("FastCopy")];
char str101 [sizeof ("GetGenericValueImpl")];
char str102 [sizeof ("GetLength")];
char str103 [sizeof ("GetLongLength")];
char str104 [sizeof ("GetLowerBound")];
char str105 [sizeof ("GetRank")];
char str106 [sizeof ("GetValue")];
char str107 [sizeof ("GetValueImpl")];
char str108 [sizeof ("SetGenericValueImpl")];
char str109 [sizeof ("SetValue")];
char str110 [sizeof ("SetValueImpl")];

char str113 [sizeof ("BlockCopyInternal")];
char str114 [sizeof ("ByteLengthInternal")];
char str115 [sizeof ("GetByteInternal")];
char str116 [sizeof ("SetByteInternal")];

char str119 [sizeof ("GetDataTablePointers")];

char str122 [sizeof ("W32ErrorMessage")];

char str125 [sizeof ("get_bundled_machine_config")];
char str126 [sizeof ("get_machine_config_path")];

char str130 [sizeof ("get_bundled_app_config")];
char str131 [sizeof ("get_bundled_machine_config")];

char str134 [sizeof ("InternalKeyAvailable")];
char str135 [sizeof ("Isatty")];
char str136 [sizeof ("SetBreak")];
char str137 [sizeof ("SetEcho")];
char str138 [sizeof ("TtySetup")];

char str141 [sizeof ("InternalFromBase64CharArray")];
char str142 [sizeof ("InternalFromBase64String")];

char str145 [sizeof ("GetTimeZoneData")];

char str148 [sizeof ("GetNow")];
char str149 [sizeof ("GetTimeMonotonic")];

char str153 [sizeof ("decimal2Int64")];
char str154 [sizeof ("decimal2UInt64")];
char str155 [sizeof ("decimal2double")];
char str157 [sizeof ("decimalCompare")];
char str158 [sizeof ("decimalDiv")];
char str159 [sizeof ("decimalFloorAndTrunc")];
char str160 [sizeof ("decimalIncr")];
char str161 [sizeof ("decimalIntDiv")];
char str162 [sizeof ("decimalMult")];
char str163 [sizeof ("decimalRound")];
char str164 [sizeof ("decimalSetExponent")];
char str165 [sizeof ("double2decimal")];
char str166 [sizeof ("string2decimal")];

char str170 [sizeof ("CreateDelegate_internal")];
char str171 [sizeof ("SetMulticastInvoke")];

char str174 [sizeof ("IsAttached_internal")];
char str175 [sizeof ("IsLogging")];
char str176 [sizeof ("Log")];

char str179 [sizeof ("WriteWindowsDebugString")];

char str182 [sizeof ("GetVersionInfo_internal(string)")];

char str186 [sizeof ("FreeData")];
char str187 [sizeof ("GetImpl")];
char str188 [sizeof ("GetSample")];
char str189 [sizeof ("UpdateValue")];

char str192 [sizeof ("CategoryDelete")];
char str193 [sizeof ("CategoryHelpInternal")];
char str194 [sizeof ("CounterCategoryExists")];
char str195 [sizeof ("Create")];
char str196 [sizeof ("GetCategoryNames")];
char str197 [sizeof ("GetCounterNames")];
char str198 [sizeof ("GetInstanceNames")];
char str199 [sizeof ("InstanceExistsInternal")];

char str202 [sizeof ("CreateProcess_internal(System.Diagnostics.ProcessStartInfo,intptr,intptr,intptr,System.Diagnostics.Process/ProcInfo&)")];
char str203 [sizeof ("ExitCode_internal(intptr)")];
char str204 [sizeof ("ExitTime_internal(intptr)")];
char str205 [sizeof ("GetModules_internal(intptr)")];
char str206 [sizeof ("GetPid_internal()")];
char str207 [sizeof ("GetPriorityClass(intptr,int&)")];
char str208 [sizeof ("GetProcessData")];
char str209 [sizeof ("GetProcess_internal(int)")];
char str210 [sizeof ("GetProcesses_internal()")];
char str211 [sizeof ("GetWorkingSet_internal(intptr,int&,int&)")];
char str212 [sizeof ("Kill_internal")];
char str213 [sizeof ("ProcessName_internal(intptr)")];
char str214 [sizeof ("Process_free_internal(intptr)")];
char str215 [sizeof ("SetPriorityClass(intptr,int,int&)")];
char str216 [sizeof ("SetWorkingSet_internal(intptr,int,int,bool)")];
char str217 [sizeof ("ShellExecuteEx_internal(System.Diagnostics.ProcessStartInfo,System.Diagnostics.Process/ProcInfo&)")];
char str218 [sizeof ("StartTime_internal(intptr)")];
char str219 [sizeof ("Times")];
char str220 [sizeof ("WaitForExit_internal(intptr,int)")];
char str221 [sizeof ("WaitForInputIdle_internal(intptr,int)")];

char str224 [sizeof ("ProcessHandle_close(intptr)")];
char str225 [sizeof ("ProcessHandle_duplicate(intptr)")];

char str229 [sizeof ("GetTimestamp")];

char str232 [sizeof ("ParseImpl")];

char str235 [sizeof ("ToObject")];
char str236 [sizeof ("compare_value_to")];
char str237 [sizeof ("get_hashcode")];
char str238 [sizeof ("get_underlying_type")];
char str239 [sizeof ("get_value")];

char str242 [sizeof ("Exit")];
char str243 [sizeof ("GetCommandLineArgs")];
char str244 [sizeof ("GetEnvironmentVariableNames")];
char str245 [sizeof ("GetLogicalDrivesInternal")];
char str246 [sizeof ("GetMachineConfigPath")];
char str247 [sizeof ("GetNewLine")];
char str248 [sizeof ("GetOSVersionString")];
char str249 [sizeof ("GetPageSize")];
char str250 [sizeof ("GetWindowsFolderPath")];
char str251 [sizeof ("InternalSetEnvironmentVariable")];
char str252 [sizeof ("get_ExitCode")];
char str253 [sizeof ("get_HasShutdownStarted")];
char str254 [sizeof ("get_MachineName")];
char str255 [sizeof ("get_Platform")];
char str256 [sizeof ("get_ProcessorCount")];
char str257 [sizeof ("get_TickCount")];
char str258 [sizeof ("get_UserName")];
char str259 [sizeof ("internalBroadcastSettingChange")];
char str260 [sizeof ("internalGetEnvironmentVariable")];
char str261 [sizeof ("internalGetGacPath")];
char str262 [sizeof ("internalGetHome")];
char str263 [sizeof ("set_ExitCode")];

char str266 [sizeof ("CollectionCount")];
char str267 [sizeof ("GetGeneration")];
char str268 [sizeof ("GetTotalMemory")];
char str269 [sizeof ("InternalCollect")];
char str270 [sizeof ("KeepAlive")];
char str271 [sizeof ("ReRegisterForFinalize")];
char str272 [sizeof ("RecordPressure")];
char str273 [sizeof ("SuppressFinalize")];
char str274 [sizeof ("WaitForPendingFinalizers")];
char str275 [sizeof ("get_MaxGeneration")];
char str276 [sizeof ("get_ephemeron_tombstone")];
char str277 [sizeof ("register_ephemeron_array")];

char str280 [sizeof ("assign_sortkey(object,string,System.Globalization.CompareOptions)")];
char str281 [sizeof ("construct_compareinfo(string)")];
char str282 [sizeof ("free_internal_collator()")];
char str283 [sizeof ("internal_compare(string,int,int,string,int,int,System.Globalization.CompareOptions)")];
char str284 [sizeof ("internal_index(string,int,int,char,System.Globalization.CompareOptions,bool)")];
char str285 [sizeof ("internal_index(string,int,int,string,System.Globalization.CompareOptions,bool)")];

char str288 [sizeof ("construct_datetime_format")];
char str289 [sizeof ("construct_internal_locale_from_current_locale")];
char str290 [sizeof ("construct_internal_locale_from_lcid")];
char str291 [sizeof ("construct_internal_locale_from_name")];
char str292 [sizeof ("construct_internal_locale_from_specific_name")];
char str293 [sizeof ("construct_number_format")];
char str294 [sizeof ("internal_get_cultures")];

char str298 [sizeof ("construct_internal_region_from_lcid")];
char str299 [sizeof ("construct_internal_region_from_name")];

char str303 [sizeof ("GetDiskFreeSpaceInternal")];
char str304 [sizeof ("GetDriveFormat")];
char str305 [sizeof ("GetDriveTypeInternal")];

char str309 [sizeof ("InternalFAMNextEvent")];

char str312 [sizeof ("InternalSupportsFSW")];

char str315 [sizeof ("AddWatch")];
char str316 [sizeof ("GetInotifyInstance")];
char str317 [sizeof ("RemoveWatch")];

char str327 [sizeof ("Close(intptr,System.IO.MonoIOError&)")];
char str329 [sizeof ("CopyFile(string,string,bool,System.IO.MonoIOError&)")];
char str330 [sizeof ("CreateDirectory(string,System.IO.MonoIOError&)")];
char str331 [sizeof ("CreatePipe(intptr&,intptr&)")];
char str332 [sizeof ("DeleteFile(string,System.IO.MonoIOError&)")];
char str334 [sizeof ("DuplicateHandle")];
char str335 [sizeof ("FindClose")];
char str336 [sizeof ("FindFirst")];
char str337 [sizeof ("FindNext")];
char str338 [sizeof ("Flush(intptr,System.IO.MonoIOError&)")];
char str339 [sizeof ("GetCurrentDirectory(System.IO.MonoIOError&)")];
char str340 [sizeof ("GetFileAttributes(string,System.IO.MonoIOError&)")];
char str341 [sizeof ("GetFileStat(string,System.IO.MonoIOStat&,System.IO.MonoIOError&)")];
char str342 [sizeof ("GetFileSystemEntries")];
char str343 [sizeof ("GetFileType(intptr,System.IO.MonoIOError&)")];
char str344 [sizeof ("GetLength(intptr,System.IO.MonoIOError&)")];
char str346 [sizeof ("GetTempPath(string&)")];
char str347 [sizeof ("Lock(intptr,long,long,System.IO.MonoIOError&)")];
char str348 [sizeof ("MoveFile(string,string,System.IO.MonoIOError&)")];
char str350 [sizeof ("Open(string,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.IO.FileOptions,System.IO.MonoIOError&)")];
char str351 [sizeof ("Read(intptr,byte[],int,int,System.IO.MonoIOError&)")];
char str353 [sizeof ("RemoveDirectory(string,System.IO.MonoIOError&)")];
char str354 [sizeof ("ReplaceFile(string,string,string,bool,System.IO.MonoIOError&)")];
char str356 [sizeof ("Seek(intptr,long,System.IO.SeekOrigin,System.IO.MonoIOError&)")];
char str357 [sizeof ("SetCurrentDirectory(string,System.IO.MonoIOError&)")];
char str358 [sizeof ("SetFileAttributes(string,System.IO.FileAttributes,System.IO.MonoIOError&)")];
char str359 [sizeof ("SetFileTime(intptr,long,long,long,System.IO.MonoIOError&)")];
char str360 [sizeof ("SetLength(intptr,long,System.IO.MonoIOError&)")];
char str362 [sizeof ("Unlock(intptr,long,long,System.IO.MonoIOError&)")];
char str364 [sizeof ("Write(intptr,byte[],int,int,System.IO.MonoIOError&)")];
char str365 [sizeof ("get_AltDirectorySeparatorChar")];
char str366 [sizeof ("get_ConsoleError")];
char str367 [sizeof ("get_ConsoleInput")];
char str368 [sizeof ("get_ConsoleOutput")];
char str369 [sizeof ("get_DirectorySeparatorChar")];
char str370 [sizeof ("get_InvalidPathChars")];
char str371 [sizeof ("get_PathSeparator")];
char str372 [sizeof ("get_VolumeSeparatorChar")];

char str375 [sizeof ("get_temp_path")];

char str378 [sizeof ("Acos")];
char str379 [sizeof ("Asin")];
char str380 [sizeof ("Atan")];
char str381 [sizeof ("Atan2")];
char str382 [sizeof ("Cos")];
char str383 [sizeof ("Cosh")];
char str384 [sizeof ("Exp")];
char str385 [sizeof ("Floor")];
char str386 [sizeof ("Log")];
char str387 [sizeof ("Log10")];
char str388 [sizeof ("Pow")];
char str389 [sizeof ("Round")];
char str390 [sizeof ("Round2")];
char str391 [sizeof ("Sin")];
char str392 [sizeof ("Sinh")];
char str393 [sizeof ("Sqrt")];
char str394 [sizeof ("Tan")];
char str395 [sizeof ("Tanh")];

char str398 [sizeof ("GetCustomAttributesDataInternal")];
char str399 [sizeof ("GetCustomAttributesInternal")];
char str400 [sizeof ("IsDefinedInternal")];

char str403 [sizeof ("get_enum_info")];

char str406 [sizeof ("GetArrayRank")];
char str407 [sizeof ("GetConstructors")];
char str408 [sizeof ("GetConstructors_internal")];
char str409 [sizeof ("GetCorrespondingInflatedConstructor")];
char str410 [sizeof ("GetCorrespondingInflatedMethod")];
char str411 [sizeof ("GetElementType")];
char str412 [sizeof ("GetEvents_internal")];
char str413 [sizeof ("GetField")];
char str414 [sizeof ("GetFields_internal")];
char str415 [sizeof ("GetGenericArguments")];
char str416 [sizeof ("GetInterfaces")];
char str417 [sizeof ("GetMethodsByName")];
char str418 [sizeof ("GetNestedType")];
char str419 [sizeof ("GetNestedTypes")];
char str420 [sizeof ("GetPropertiesByName")];
char str421 [sizeof ("InternalGetEvent")];
char str422 [sizeof ("IsByRefImpl")];
char str423 [sizeof ("IsCOMObjectImpl")];
char str424 [sizeof ("IsPointerImpl")];
char str425 [sizeof ("IsPrimitiveImpl")];
char str426 [sizeof ("getFullName")];
char str427 [sizeof ("get_Assembly")];
char str428 [sizeof ("get_BaseType")];
char str429 [sizeof ("get_DeclaringMethod")];
char str430 [sizeof ("get_DeclaringType")];
char str431 [sizeof ("get_IsGenericParameter")];
char str432 [sizeof ("get_Module")];
char str433 [sizeof ("get_Name")];
char str434 [sizeof ("get_Namespace")];
char str435 [sizeof ("get_attributes")];
char str436 [sizeof ("get_core_clr_security_level")];
char str437 [sizeof ("type_from_obj")];

char str441 [sizeof ("GetHostByAddr_internal(string,string&,string[]&,string[]&)")];
char str442 [sizeof ("GetHostByName_internal(string,string&,string[]&,string[]&)")];
char str443 [sizeof ("GetHostName_internal(string&)")];

char str446 [sizeof ("Accept_internal(intptr,int&,bool)")];
char str447 [sizeof ("Available_internal(intptr,int&)")];
char str448 [sizeof ("Bind_internal(intptr,System.Net.SocketAddress,int&)")];
char str449 [sizeof ("Blocking_internal(intptr,bool,int&)")];
char str450 [sizeof ("Close_internal(intptr,int&)")];
char str451 [sizeof ("Connect_internal(intptr,System.Net.SocketAddress,int&)")];
char str452 [sizeof ("Disconnect_internal(intptr,bool,int&)")];
char str453 [sizeof ("GetSocketOption_arr_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,byte[]&,int&)")];
char str454 [sizeof ("GetSocketOption_obj_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object&,int&)")];
char str455 [sizeof ("Listen_internal(intptr,int,int&)")];
char str456 [sizeof ("LocalEndPoint_internal(intptr,int,int&)")];
char str457 [sizeof ("Poll_internal")];
char str458 [sizeof ("Receive_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)")];
char str459 [sizeof ("Receive_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)")];
char str460 [sizeof ("RecvFrom_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress&,int&)")];
char str461 [sizeof ("RemoteEndPoint_internal(intptr,int,int&)")];
char str462 [sizeof ("Select_internal(System.Net.Sockets.Socket[]&,int,int&)")];
char str463 [sizeof ("SendFile(intptr,string,byte[],byte[],System.Net.Sockets.TransmitFileOptions)")];
char str464 [sizeof ("SendTo_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress,int&)")];
char str465 [sizeof ("Send_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)")];
char str466 [sizeof ("Send_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)")];
char str467 [sizeof ("SetSocketOption_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object,byte[],int,int&)")];
char str468 [sizeof ("Shutdown_internal(intptr,System.Net.Sockets.SocketShutdown,int&)")];
char str469 [sizeof ("Socket_internal(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType,int&)")];
char str470 [sizeof ("WSAIoctl(intptr,int,byte[],byte[],int&)")];
char str471 [sizeof ("cancel_blocking_socket_operation")];
char str472 [sizeof ("socket_pool_queue")];

char str475 [sizeof ("WSAGetLastError_internal")];

char str479 [sizeof ("GetFormatterTables")];

char str482 [sizeof ("GetType")];
char str483 [sizeof ("InternalGetHashCode")];
char str484 [sizeof ("MemberwiseClone")];
char str485 [sizeof ("obj_address")];

char str488 [sizeof ("FillName")];
char str489 [sizeof ("GetCallingAssembly")];
char str490 [sizeof ("GetEntryAssembly")];
char str491 [sizeof ("GetExecutingAssembly")];
char str492 [sizeof ("GetFilesInternal")];
char str493 [sizeof ("GetManifestModuleInternal")];
char str494 [sizeof ("GetManifestResourceInfoInternal")];
char str495 [sizeof ("GetManifestResourceInternal")];
char str496 [sizeof ("GetManifestResourceNames")];
char str497 [sizeof ("GetModulesInternal")];
char str499 [sizeof ("GetReferencedAssemblies")];
char str500 [sizeof ("GetTypes")];
char str501 [sizeof ("InternalGetAssemblyName")];
char str502 [sizeof ("InternalGetType")];
char str503 [sizeof ("InternalImageRuntimeVersion")];
char str504 [sizeof ("LoadFrom")];
char str505 [sizeof ("LoadPermissions")];
char str509 [sizeof ("MonoDebugger_GetMethodToken")];
char str512 [sizeof ("get_EntryPoint")];
char str513 [sizeof ("get_ReflectionOnly")];
char str514 [sizeof ("get_code_base")];
char str515 [sizeof ("get_fullname")];
char str516 [sizeof ("get_global_assembly_cache")];
char str517 [sizeof ("get_location")];
char str518 [sizeof ("load_with_partial_name")];

char str521 [sizeof ("ParseName")];
char str522 [sizeof ("get_public_token")];

char str525 [sizeof ("ResolveArgumentsInternal")];

char str528 [sizeof ("InternalAddModule")];
char str529 [sizeof ("basic_init")];

char str532 [sizeof ("GetBlob")];

char str536 [sizeof ("create_unmanaged_type")];

char str540 [sizeof ("create_dynamic_method")];

char str543 [sizeof ("setup_enum_type")];

char str546 [sizeof ("initialize")];

char str549 [sizeof ("MakeGenericMethod")];

char str552 [sizeof ("RegisterToken")];
char str553 [sizeof ("WriteToFile")];
char str554 [sizeof ("basic_init")];
char str555 [sizeof ("build_metadata")];
char str556 [sizeof ("create_modified_type")];
char str557 [sizeof ("getMethodToken")];
char str558 [sizeof ("getToken")];
char str559 [sizeof ("getUSIndex")];
char str560 [sizeof ("set_wrappers_type")];

char str563 [sizeof ("get_signature_field")];
char str564 [sizeof ("get_signature_local")];

char str567 [sizeof ("create_generic_class")];
char str568 [sizeof ("create_internal_class")];
char str569 [sizeof ("create_runtime_class")];
char str570 [sizeof ("get_IsGenericParameter")];
char str571 [sizeof ("get_event_info")];
char str572 [sizeof ("setup_generic_class")];
char str573 [sizeof ("setup_internal_class")];

char str576 [sizeof ("GetTypeModifiers")];
char str577 [sizeof ("get_marshal_info")];
char str578 [sizeof ("internal_from_handle_type")];

char str581 [sizeof ("get_MetadataToken")];

char str584 [sizeof ("GetCurrentMethod")];
char str585 [sizeof ("GetMethodBodyInternal")];
char str586 [sizeof ("GetMethodFromHandleInternal")];
char str587 [sizeof ("GetMethodFromHandleInternalType")];

char str590 [sizeof ("Close")];
char str591 [sizeof ("GetGlobalType")];
char str592 [sizeof ("GetGuidInternal")];
char str593 [sizeof ("GetHINSTANCE")];
char str594 [sizeof ("GetMDStreamVersion")];
char str595 [sizeof ("GetPEKind")];
char str596 [sizeof ("InternalGetTypes")];
char str597 [sizeof ("ResolveFieldToken")];
char str598 [sizeof ("ResolveMemberToken")];
char str599 [sizeof ("ResolveMethodToken")];
char str600 [sizeof ("ResolveSignature")];
char str601 [sizeof ("ResolveStringToken")];
char str602 [sizeof ("ResolveTypeToken")];
char str603 [sizeof ("get_MetadataToken")];

char str606 [sizeof ("GetGenericMethodDefinition_impl")];
char str607 [sizeof ("InternalInvoke")];

char str610 [sizeof ("get_event_info")];

char str613 [sizeof ("GetFieldOffset")];
char str614 [sizeof ("GetParentType")];
char str615 [sizeof ("GetRawConstantValue")];
char str616 [sizeof ("GetValueInternal")];
char str617 [sizeof ("ResolveType")];
char str618 [sizeof ("SetValueInternal")];

char str621 [sizeof ("get_ReflectedType")];

char str624 [sizeof ("initialize")];
char str625 [sizeof ("register_with_runtime")];

char str629 [sizeof ("get_ReflectedType")];

char str632 [sizeof ("GetDllImportAttribute")];
char str633 [sizeof ("GetGenericArguments")];
char str634 [sizeof ("GetGenericMethodDefinition_impl")];
char str635 [sizeof ("InternalInvoke")];
char str636 [sizeof ("MakeGenericMethod_impl")];
char str637 [sizeof ("get_IsGenericMethod")];
char str638 [sizeof ("get_IsGenericMethodDefinition")];
char str639 [sizeof ("get_base_method")];
char str640 [sizeof ("get_name")];

char str643 [sizeof ("get_method_attributes")];
char str644 [sizeof ("get_method_info")];
char str645 [sizeof ("get_parameter_info")];
char str646 [sizeof ("get_retval_marshal")];

char str649 [sizeof ("GetTypeModifiers")];
char str650 [sizeof ("get_default_value")];
char str651 [sizeof ("get_property_info")];

char str654 [sizeof ("GetMetadataToken")];
char str655 [sizeof ("GetTypeModifiers")];

char str658 [sizeof ("GetObjectValue")];
char str660 [sizeof ("GetOffsetToStringData")];
char str661 [sizeof ("InitializeArray")];
char str662 [sizeof ("RunClassConstructor")];
char str663 [sizeof ("RunModuleConstructor")];
char str664 [sizeof ("SufficientExecutionStack")];
char str665 [sizeof ("get_OffsetToStringData")];

char str668 [sizeof ("CheckCurrentDomain")];
char str669 [sizeof ("FreeHandle")];
char str670 [sizeof ("GetAddrOfPinnedObject")];
char str671 [sizeof ("GetTarget")];
char str672 [sizeof ("GetTargetHandle")];

char str676 [sizeof ("AddRefInternal")];
char str680 [sizeof ("AllocCoTaskMem")];
char str681 [sizeof ("AllocHGlobal")];
char str682 [sizeof ("DestroyStructure")];
char str683 [sizeof ("FreeBSTR")];
char str684 [sizeof ("FreeCoTaskMem")];
char str685 [sizeof ("FreeHGlobal")];
char str687 [sizeof ("GetCCW")];
char str688 [sizeof ("GetComSlotForMethodInfoInternal")];
char str690 [sizeof ("GetDelegateForFunctionPointerInternal")];
char str691 [sizeof ("GetFunctionPointerForDelegateInternal")];
char str693 [sizeof ("GetIDispatchForObjectInternal")];
char str694 [sizeof ("GetIUnknownForObjectInternal")];
char str696 [sizeof ("GetLastWin32Error")];
char str698 [sizeof ("GetObjectForCCW")];
char str699 [sizeof ("IsComObject")];
char str701 [sizeof ("OffsetOf")];
char str702 [sizeof ("Prelink")];
char str703 [sizeof ("PrelinkAll")];
char str704 [sizeof ("PtrToStringAnsi(intptr)")];
char str705 [sizeof ("PtrToStringAnsi(intptr,int)")];
char str707 [sizeof ("PtrToStringBSTR")];
char str709 [sizeof ("PtrToStringUni(intptr)")];
char str710 [sizeof ("PtrToStringUni(intptr,int)")];
char str711 [sizeof ("PtrToStructure(intptr,System.Type)")];
char str712 [sizeof ("PtrToStructure(intptr,object)")];
char str714 [sizeof ("QueryInterfaceInternal")];
char str716 [sizeof ("ReAllocCoTaskMem")];
char str717 [sizeof ("ReAllocHGlobal")];
char str719 [sizeof ("ReleaseComObjectInternal")];
char str720 [sizeof ("ReleaseInternal")];
char str722 [sizeof ("SizeOf")];
char str723 [sizeof ("StringToBSTR")];
char str724 [sizeof ("StringToHGlobalAnsi")];
char str725 [sizeof ("StringToHGlobalUni")];
char str726 [sizeof ("StructureToPtr")];
char str727 [sizeof ("UnsafeAddrOfPinnedArrayElement")];
char str728 [sizeof ("copy_from_unmanaged")];
char str729 [sizeof ("copy_to_unmanaged")];

char str732 [sizeof ("AllocateUninitializedClassInstance")];
char str733 [sizeof ("EnableProxyActivation")];

char str736 [sizeof ("InitMessage")];

char str740 [sizeof ("InternalGetProxyType")];
char str741 [sizeof ("InternalGetTransparentProxy")];

char str744 [sizeof ("GetVirtualMethod")];
char str745 [sizeof ("InternalExecute")];
char str746 [sizeof ("IsTransparentProxy")];

char str750 [sizeof ("GetFunctionPointer")];

char str753 [sizeof ("RngClose")];
char str754 [sizeof ("RngGetBytes")];
char str755 [sizeof ("RngInitialize")];
char str756 [sizeof ("RngOpen")];

char str760 [sizeof ("IsAuthenticodePresent")];

char str763 [sizeof ("GetCurrentToken")];
char str764 [sizeof ("GetTokenName")];
char str765 [sizeof ("GetUserToken")];
char str766 [sizeof ("_GetRoles")];

char str769 [sizeof ("CloseToken")];
char str770 [sizeof ("DuplicateToken")];
char str771 [sizeof ("RevertToSelf")];
char str772 [sizeof ("SetCurrentToken")];

char str775 [sizeof ("IsMemberOfGroupId")];
char str776 [sizeof ("IsMemberOfGroupName")];

char str779 [sizeof ("DecryptInternal")];
char str780 [sizeof ("EncryptInternal")];

char str784 [sizeof ("GetLinkDemandSecurity")];
char str785 [sizeof ("get_CheckExecutionRights")];
char str786 [sizeof ("get_RequiresElevatedPermissions")];
char str787 [sizeof ("get_SecurityEnabled")];
char str788 [sizeof ("set_CheckExecutionRights")];
char str789 [sizeof ("set_SecurityEnabled")];

char str792 [sizeof (".ctor(char*)")];
char str793 [sizeof (".ctor(char*,int,int)")];
char str794 [sizeof (".ctor(char,int)")];
char str795 [sizeof (".ctor(char[])")];
char str796 [sizeof (".ctor(char[],int,int)")];
char str797 [sizeof (".ctor(sbyte*)")];
char str798 [sizeof (".ctor(sbyte*,int,int)")];
char str799 [sizeof (".ctor(sbyte*,int,int,System.Text.Encoding)")];
char str800 [sizeof ("GetLOSLimit")];
char str801 [sizeof ("InternalAllocateStr")];
char str802 [sizeof ("InternalIntern")];
char str803 [sizeof ("InternalIsInterned")];

char str806 [sizeof ("InternalCodePage")];

char str809 [sizeof ("Add(int&,int)")];
char str810 [sizeof ("Add(long&,long)")];
char str811 [sizeof ("CompareExchange(T&,T,T)")];
char str812 [sizeof ("CompareExchange(double&,double,double)")];
char str813 [sizeof ("CompareExchange(int&,int,int)")];
char str814 [sizeof ("CompareExchange(intptr&,intptr,intptr)")];
char str815 [sizeof ("CompareExchange(long&,long,long)")];
char str816 [sizeof ("CompareExchange(object&,object,object)")];
char str817 [sizeof ("CompareExchange(single&,single,single)")];
char str818 [sizeof ("Decrement(int&)")];
char str819 [sizeof ("Decrement(long&)")];
char str820 [sizeof ("Exchange(T&,T)")];
char str821 [sizeof ("Exchange(double&,double)")];
char str822 [sizeof ("Exchange(int&,int)")];
char str823 [sizeof ("Exchange(intptr&,intptr)")];
char str824 [sizeof ("Exchange(long&,long)")];
char str825 [sizeof ("Exchange(object&,object)")];
char str826 [sizeof ("Exchange(single&,single)")];
char str827 [sizeof ("Increment(int&)")];
char str828 [sizeof ("Increment(long&)")];
char str829 [sizeof ("Read(long&)")];

char str832 [sizeof ("Thread_free_internal")];

char str835 [sizeof ("Enter")];
char str836 [sizeof ("Exit")];
char str837 [sizeof ("Monitor_pulse")];
char str838 [sizeof ("Monitor_pulse_all")];
char str839 [sizeof ("Monitor_test_owner")];
char str840 [sizeof ("Monitor_test_synchronised")];
char str841 [sizeof ("Monitor_try_enter")];
char str842 [sizeof ("Monitor_wait")];
char str843 [sizeof ("try_enter_with_atomic_var")];

char str846 [sizeof ("CreateMutex_internal(bool,string,bool&)")];
char str847 [sizeof ("OpenMutex_internal(string,System.Security.AccessControl.MutexRights,System.IO.MonoIOError&)")];
char str848 [sizeof ("ReleaseMutex_internal(intptr)")];

char str851 [sizeof ("CloseEvent_internal")];
char str852 [sizeof ("CreateEvent_internal(bool,bool,string,bool&)")];
char str853 [sizeof ("OpenEvent_internal(string,System.Security.AccessControl.EventWaitHandleRights,System.IO.MonoIOError&)")];
char str854 [sizeof ("ResetEvent_internal")];
char str855 [sizeof ("SetEvent_internal")];

char str858 [sizeof ("CreateSemaphore_internal(int,int,string,bool&)")];
char str859 [sizeof ("OpenSemaphore_internal(string,System.Security.AccessControl.SemaphoreRights,System.IO.MonoIOError&)")];
char str860 [sizeof ("ReleaseSemaphore_internal(intptr,int,bool&)")];

char str863 [sizeof ("Abort_internal(System.Threading.InternalThread,object)")];
char str864 [sizeof ("AllocTlsData")];
char str865 [sizeof ("ByteArrayToCurrentDomain(byte[])")];
char str866 [sizeof ("ByteArrayToRootDomain(byte[])")];
char str867 [sizeof ("ClrState(System.Threading.InternalThread,System.Threading.ThreadState)")];
char str868 [sizeof ("ConstructInternalThread")];
char str869 [sizeof ("CurrentInternalThread_internal")];
char str870 [sizeof ("DestroyTlsData")];
char str871 [sizeof ("FreeLocalSlotValues")];
char str872 [sizeof ("GetAbortExceptionState")];
char str873 [sizeof ("GetDomainID")];
char str874 [sizeof ("GetName_internal(System.Threading.InternalThread)")];
char str875 [sizeof ("GetState(System.Threading.InternalThread)")];
char str876 [sizeof ("Interrupt_internal(System.Threading.InternalThread)")];
char str877 [sizeof ("Join_internal(System.Threading.InternalThread,int,intptr)")];
char str878 [sizeof ("MemoryBarrier")];
char str879 [sizeof ("ResetAbort_internal()")];
char str880 [sizeof ("Resume_internal()")];
char str881 [sizeof ("SetName_internal(System.Threading.InternalThread,string)")];
char str882 [sizeof ("SetState(System.Threading.InternalThread,System.Threading.ThreadState)")];
char str883 [sizeof ("Sleep_internal")];
char str884 [sizeof ("SpinWait_nop")];
char str885 [sizeof ("Suspend_internal(System.Threading.InternalThread)")];
char str886 [sizeof ("Thread_internal")];
char str887 [sizeof ("VolatileRead(byte&)")];
char str888 [sizeof ("VolatileRead(double&)")];
char str889 [sizeof ("VolatileRead(int&)")];
char str890 [sizeof ("VolatileRead(int16&)")];
char str891 [sizeof ("VolatileRead(intptr&)")];
char str892 [sizeof ("VolatileRead(long&)")];
char str893 [sizeof ("VolatileRead(object&)")];
char str894 [sizeof ("VolatileRead(sbyte&)")];
char str895 [sizeof ("VolatileRead(single&)")];
char str896 [sizeof ("VolatileRead(uint&)")];
char str897 [sizeof ("VolatileRead(uint16&)")];
char str898 [sizeof ("VolatileRead(uintptr&)")];
char str899 [sizeof ("VolatileRead(ulong&)")];
char str900 [sizeof ("VolatileWrite(byte&,byte)")];
char str901 [sizeof ("VolatileWrite(double&,double)")];
char str902 [sizeof ("VolatileWrite(int&,int)")];
char str903 [sizeof ("VolatileWrite(int16&,int16)")];
char str904 [sizeof ("VolatileWrite(intptr&,intptr)")];
char str905 [sizeof ("VolatileWrite(long&,long)")];
char str906 [sizeof ("VolatileWrite(object&,object)")];
char str907 [sizeof ("VolatileWrite(sbyte&,sbyte)")];
char str908 [sizeof ("VolatileWrite(single&,single)")];
char str909 [sizeof ("VolatileWrite(uint&,uint)")];
char str910 [sizeof ("VolatileWrite(uint16&,uint16)")];
char str911 [sizeof ("VolatileWrite(uintptr&,uintptr)")];
char str912 [sizeof ("VolatileWrite(ulong&,ulong)")];
char str913 [sizeof ("Yield")];
char str914 [sizeof ("current_lcid()")];

char str917 [sizeof ("GetAvailableThreads")];
char str918 [sizeof ("GetMaxThreads")];
char str919 [sizeof ("GetMinThreads")];
char str920 [sizeof ("SetMaxThreads")];
char str921 [sizeof ("SetMinThreads")];
char str922 [sizeof ("pool_queue")];

char str925 [sizeof ("Read(T&)")];
char str926 [sizeof ("Read(bool&)")];
char str927 [sizeof ("Read(byte&)")];
char str928 [sizeof ("Read(double&)")];
char str929 [sizeof ("Read(int&)")];
char str930 [sizeof ("Read(int16&)")];
char str931 [sizeof ("Read(intptr&)")];
char str932 [sizeof ("Read(long&)")];
char str933 [sizeof ("Read(sbyte&)")];
char str934 [sizeof ("Read(single&)")];
char str935 [sizeof ("Read(uint&)")];
char str936 [sizeof ("Read(uint16&)")];
char str937 [sizeof ("Read(uintptr&)")];
char str938 [sizeof ("Read(ulong&)")];
char str939 [sizeof ("Write(T&,T)")];
char str940 [sizeof ("Write(bool&,bool)")];
char str941 [sizeof ("Write(byte&,byte)")];
char str942 [sizeof ("Write(double&,double)")];
char str943 [sizeof ("Write(int&,int)")];
char str944 [sizeof ("Write(int16&,int16)")];
char str945 [sizeof ("Write(intptr&,intptr)")];
char str946 [sizeof ("Write(long&,long)")];
char str947 [sizeof ("Write(sbyte&,sbyte)")];
char str948 [sizeof ("Write(single&,single)")];
char str949 [sizeof ("Write(uint&,uint)")];
char str950 [sizeof ("Write(uint16&,uint16)")];
char str951 [sizeof ("Write(uintptr&,uintptr)")];
char str952 [sizeof ("Write(ulong&,ulong)")];

char str955 [sizeof ("SignalAndWait_Internal")];
char str956 [sizeof ("WaitAll_internal")];
char str957 [sizeof ("WaitAny_internal")];
char str958 [sizeof ("WaitOne_internal")];

char str961 [sizeof ("EqualsInternal")];
char str962 [sizeof ("GetGenericParameterAttributes")];
char str963 [sizeof ("GetGenericParameterConstraints_impl")];
char str964 [sizeof ("GetGenericParameterPosition")];
char str965 [sizeof ("GetGenericTypeDefinition_impl")];
char str966 [sizeof ("GetInterfaceMapData")];
char str967 [sizeof ("GetPacking")];
char str968 [sizeof ("GetTypeCode")];
char str969 [sizeof ("GetTypeCodeInternal")];
char str970 [sizeof ("IsArrayImpl")];
char str971 [sizeof ("IsInstanceOfType")];
char str972 [sizeof ("MakeGenericType")];
char str973 [sizeof ("MakePointerType")];
char str974 [sizeof ("get_IsGenericInstance")];
char str975 [sizeof ("get_IsGenericType")];
char str976 [sizeof ("get_IsGenericTypeDefinition")];
char str977 [sizeof ("internal_from_handle")];
char str978 [sizeof ("internal_from_name")];
char str979 [sizeof ("make_array_type")];
char str980 [sizeof ("make_byref_type")];
char str981 [sizeof ("type_is_assignable_from")];
char str982 [sizeof ("type_is_subtype_of")];

char str985 [sizeof ("ToObject")];
char str986 [sizeof ("ToObjectInternal")];

char str989 [sizeof ("InternalEquals")];
char str990 [sizeof ("InternalGetHashCode")];

char str993 [sizeof ("GetMachineConfigPath")];
char str994 [sizeof ("GetMachineInstallDirectory")];
char str995 [sizeof ("GetUnmanagedResourcesPtr")];

char str999 [sizeof ("CreateRCW")];
char str1000 [sizeof ("GetInterfaceInternal")];
char str1001 [sizeof ("ReleaseInterfaces")];
#undef ICALL
} icall_names_str = {
#define ICALL(id,name,func) (name),
// #include "metadata/icall-def.h"
ICALL_TYPE(UNORM, "Mono.Globalization.Unicode.Normalization", UNORM_1)
ICALL(UNORM_1, "load_normalization_resource", load_normalization_resource)

#ifndef DISABLE_COM
ICALL_TYPE(COMPROX, "Mono.Interop.ComInteropProxy", COMPROX_1)
ICALL(COMPROX_1, "AddProxy", ves_icall_Mono_Interop_ComInteropProxy_AddProxy)
ICALL(COMPROX_2, "FindProxy", ves_icall_Mono_Interop_ComInteropProxy_FindProxy)
#endif

ICALL_TYPE(RUNTIME, "Mono.Runtime", RUNTIME_1)
ICALL(RUNTIME_1, "GetDisplayName", ves_icall_Mono_Runtime_GetDisplayName)
ICALL(RUNTIME_12, "GetNativeStackTrace", ves_icall_Mono_Runtime_GetNativeStackTrace)
ICALL(RUNTIME_13, "SetGCAllowSynchronousMajor", ves_icall_Mono_Runtime_SetGCAllowSynchronousMajor)

#ifndef PLATFORM_RO_FS
ICALL_TYPE(KPAIR, "Mono.Security.Cryptography.KeyPairPersistence", KPAIR_1)
ICALL(KPAIR_1, "_CanSecure", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_CanSecure)
ICALL(KPAIR_2, "_IsMachineProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsMachineProtected)
ICALL(KPAIR_3, "_IsUserProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsUserProtected)
ICALL(KPAIR_4, "_ProtectMachine", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectMachine)
ICALL(KPAIR_5, "_ProtectUser", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectUser)
#endif /* !PLATFORM_RO_FS */

ICALL_TYPE(ACTIV, "System.Activator", ACTIV_1)
ICALL(ACTIV_1, "CreateInstanceInternal", ves_icall_System_Activator_CreateInstanceInternal)

ICALL_TYPE(APPDOM, "System.AppDomain", APPDOM_1)
ICALL(APPDOM_1, "ExecuteAssembly", ves_icall_System_AppDomain_ExecuteAssembly)
ICALL(APPDOM_2, "GetAssemblies", ves_icall_System_AppDomain_GetAssemblies)
ICALL(APPDOM_3, "GetData", ves_icall_System_AppDomain_GetData)
ICALL(APPDOM_4, "InternalGetContext", ves_icall_System_AppDomain_InternalGetContext)
ICALL(APPDOM_5, "InternalGetDefaultContext", ves_icall_System_AppDomain_InternalGetDefaultContext)
ICALL(APPDOM_6, "InternalGetProcessGuid", ves_icall_System_AppDomain_InternalGetProcessGuid)
ICALL(APPDOM_7, "InternalIsFinalizingForUnload", ves_icall_System_AppDomain_InternalIsFinalizingForUnload)
ICALL(APPDOM_8, "InternalPopDomainRef", ves_icall_System_AppDomain_InternalPopDomainRef)
ICALL(APPDOM_9, "InternalPushDomainRef", ves_icall_System_AppDomain_InternalPushDomainRef)
ICALL(APPDOM_10, "InternalPushDomainRefByID", ves_icall_System_AppDomain_InternalPushDomainRefByID)
ICALL(APPDOM_11, "InternalSetContext", ves_icall_System_AppDomain_InternalSetContext)
ICALL(APPDOM_12, "InternalSetDomain", ves_icall_System_AppDomain_InternalSetDomain)
ICALL(APPDOM_13, "InternalSetDomainByID", ves_icall_System_AppDomain_InternalSetDomainByID)
ICALL(APPDOM_14, "InternalUnload", ves_icall_System_AppDomain_InternalUnload)
ICALL(APPDOM_15, "LoadAssembly", ves_icall_System_AppDomain_LoadAssembly)
ICALL(APPDOM_16, "LoadAssemblyRaw", ves_icall_System_AppDomain_LoadAssemblyRaw)
ICALL(APPDOM_17, "SetData", ves_icall_System_AppDomain_SetData)
ICALL(APPDOM_18, "createDomain", ves_icall_System_AppDomain_createDomain)
ICALL(APPDOM_19, "getCurDomain", ves_icall_System_AppDomain_getCurDomain)
ICALL(APPDOM_20, "getFriendlyName", ves_icall_System_AppDomain_getFriendlyName)
ICALL(APPDOM_21, "getRootDomain", ves_icall_System_AppDomain_getRootDomain)
ICALL(APPDOM_22, "getSetup", ves_icall_System_AppDomain_getSetup)

ICALL_TYPE(ARGI, "System.ArgIterator", ARGI_1)
ICALL(ARGI_1, "IntGetNextArg()",                  mono_ArgIterator_IntGetNextArg)
ICALL(ARGI_2, "IntGetNextArg(intptr)", mono_ArgIterator_IntGetNextArgT)
ICALL(ARGI_3, "IntGetNextArgType",                mono_ArgIterator_IntGetNextArgType)
ICALL(ARGI_4, "Setup",                            mono_ArgIterator_Setup)

ICALL_TYPE(ARRAY, "System.Array", ARRAY_1)
ICALL(ARRAY_1, "ClearInternal",    ves_icall_System_Array_ClearInternal)
ICALL(ARRAY_2, "Clone",            mono_array_clone)
ICALL(ARRAY_3, "CreateInstanceImpl",   ves_icall_System_Array_CreateInstanceImpl)
ICALL(ARRAY_14, "CreateInstanceImpl64",   ves_icall_System_Array_CreateInstanceImpl64)
ICALL(ARRAY_4, "FastCopy",         ves_icall_System_Array_FastCopy)
ICALL(ARRAY_5, "GetGenericValueImpl", ves_icall_System_Array_GetGenericValueImpl)
ICALL(ARRAY_6, "GetLength",        ves_icall_System_Array_GetLength)
ICALL(ARRAY_15, "GetLongLength",        ves_icall_System_Array_GetLongLength)
ICALL(ARRAY_7, "GetLowerBound",    ves_icall_System_Array_GetLowerBound)
ICALL(ARRAY_8, "GetRank",          ves_icall_System_Array_GetRank)
ICALL(ARRAY_9, "GetValue",         ves_icall_System_Array_GetValue)
ICALL(ARRAY_10, "GetValueImpl",     ves_icall_System_Array_GetValueImpl)
ICALL(ARRAY_11, "SetGenericValueImpl", ves_icall_System_Array_SetGenericValueImpl)
ICALL(ARRAY_12, "SetValue",         ves_icall_System_Array_SetValue)
ICALL(ARRAY_13, "SetValueImpl",     ves_icall_System_Array_SetValueImpl)

ICALL_TYPE(BUFFER, "System.Buffer", BUFFER_1)
ICALL(BUFFER_1, "BlockCopyInternal", ves_icall_System_Buffer_BlockCopyInternal)
ICALL(BUFFER_2, "ByteLengthInternal", ves_icall_System_Buffer_ByteLengthInternal)
ICALL(BUFFER_3, "GetByteInternal", ves_icall_System_Buffer_GetByteInternal)
ICALL(BUFFER_4, "SetByteInternal", ves_icall_System_Buffer_SetByteInternal)

ICALL_TYPE(CHAR, "System.Char", CHAR_1)
ICALL(CHAR_1, "GetDataTablePointers", ves_icall_System_Char_GetDataTablePointers)

ICALL_TYPE (COMPO_W, "System.ComponentModel.Win32Exception", COMPO_W_1)
ICALL (COMPO_W_1, "W32ErrorMessage", ves_icall_System_ComponentModel_Win32Exception_W32ErrorMessage)

ICALL_TYPE(DEFAULTC, "System.Configuration.DefaultConfig", DEFAULTC_1)
ICALL(DEFAULTC_1, "get_bundled_machine_config", get_bundled_machine_config)
ICALL(DEFAULTC_2, "get_machine_config_path", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)

/* Note that the below icall shares the same function as DefaultConfig uses */
ICALL_TYPE(INTCFGHOST, "System.Configuration.InternalConfigurationHost", INTCFGHOST_1)
ICALL(INTCFGHOST_1, "get_bundled_app_config", get_bundled_app_config)
ICALL(INTCFGHOST_2, "get_bundled_machine_config", get_bundled_machine_config)

ICALL_TYPE(CONSOLE, "System.ConsoleDriver", CONSOLE_1)
ICALL(CONSOLE_1, "InternalKeyAvailable", ves_icall_System_ConsoleDriver_InternalKeyAvailable )
ICALL(CONSOLE_2, "Isatty", ves_icall_System_ConsoleDriver_Isatty )
ICALL(CONSOLE_3, "SetBreak", ves_icall_System_ConsoleDriver_SetBreak )
ICALL(CONSOLE_4, "SetEcho", ves_icall_System_ConsoleDriver_SetEcho )
ICALL(CONSOLE_5, "TtySetup", ves_icall_System_ConsoleDriver_TtySetup )

ICALL_TYPE(CONVERT, "System.Convert", CONVERT_1)
ICALL(CONVERT_1, "InternalFromBase64CharArray", InternalFromBase64CharArray )
ICALL(CONVERT_2, "InternalFromBase64String", InternalFromBase64String )

ICALL_TYPE(TZONE, "System.CurrentSystemTimeZone", TZONE_1)
ICALL(TZONE_1, "GetTimeZoneData", ves_icall_System_CurrentSystemTimeZone_GetTimeZoneData)

ICALL_TYPE(DTIME, "System.DateTime", DTIME_1)
ICALL(DTIME_1, "GetNow", mono_100ns_datetime)
ICALL(DTIME_2, "GetTimeMonotonic", mono_100ns_ticks)

#ifndef DISABLE_DECIMAL
ICALL_TYPE(DECIMAL, "System.Decimal", DECIMAL_1)
ICALL(DECIMAL_1, "decimal2Int64", mono_decimal2Int64)
ICALL(DECIMAL_2, "decimal2UInt64", mono_decimal2UInt64)
ICALL(DECIMAL_3, "decimal2double", mono_decimal2double)
//ICALL(DECIMAL_4, "decimal2string", mono_decimal2string)
ICALL(DECIMAL_5, "decimalCompare", mono_decimalCompare)
ICALL(DECIMAL_6, "decimalDiv", mono_decimalDiv)
ICALL(DECIMAL_7, "decimalFloorAndTrunc", mono_decimalFloorAndTrunc)
ICALL(DECIMAL_8, "decimalIncr", mono_decimalIncr)
ICALL(DECIMAL_9, "decimalIntDiv", mono_decimalIntDiv)
ICALL(DECIMAL_10, "decimalMult", mono_decimalMult)
ICALL(DECIMAL_11, "decimalRound", mono_decimalRound)
ICALL(DECIMAL_12, "decimalSetExponent", mono_decimalSetExponent)
ICALL(DECIMAL_13, "double2decimal", mono_double2decimal) /* FIXME: wrong signature. */
ICALL(DECIMAL_14, "string2decimal", mono_string2decimal)
#endif

ICALL_TYPE(DELEGATE, "System.Delegate", DELEGATE_1)
ICALL(DELEGATE_1, "CreateDelegate_internal", ves_icall_System_Delegate_CreateDelegate_internal)
ICALL(DELEGATE_2, "SetMulticastInvoke", ves_icall_System_Delegate_SetMulticastInvoke)

ICALL_TYPE(DEBUGR, "System.Diagnostics.Debugger", DEBUGR_1)
ICALL(DEBUGR_1, "IsAttached_internal", ves_icall_System_Diagnostics_Debugger_IsAttached_internal)
ICALL(DEBUGR_2, "IsLogging", ves_icall_System_Diagnostics_Debugger_IsLogging)
ICALL(DEBUGR_3, "Log", ves_icall_System_Diagnostics_Debugger_Log)

ICALL_TYPE(TRACEL, "System.Diagnostics.DefaultTraceListener", TRACEL_1)
ICALL(TRACEL_1, "WriteWindowsDebugString", ves_icall_System_Diagnostics_DefaultTraceListener_WriteWindowsDebugString)

ICALL_TYPE(FILEV, "System.Diagnostics.FileVersionInfo", FILEV_1)
ICALL(FILEV_1, "GetVersionInfo_internal(string)", ves_icall_System_Diagnostics_FileVersionInfo_GetVersionInfo_internal)

#ifndef DISABLE_PROCESS_HANDLING
ICALL_TYPE(PERFCTR, "System.Diagnostics.PerformanceCounter", PERFCTR_1)
ICALL(PERFCTR_1, "FreeData", mono_perfcounter_free_data)
ICALL(PERFCTR_2, "GetImpl", mono_perfcounter_get_impl)
ICALL(PERFCTR_3, "GetSample", mono_perfcounter_get_sample)
ICALL(PERFCTR_4, "UpdateValue", mono_perfcounter_update_value)

ICALL_TYPE(PERFCTRCAT, "System.Diagnostics.PerformanceCounterCategory", PERFCTRCAT_1)
ICALL(PERFCTRCAT_1, "CategoryDelete", mono_perfcounter_category_del)
ICALL(PERFCTRCAT_2, "CategoryHelpInternal",   mono_perfcounter_category_help)
ICALL(PERFCTRCAT_3, "CounterCategoryExists", mono_perfcounter_category_exists)
ICALL(PERFCTRCAT_4, "Create",         mono_perfcounter_create)
ICALL(PERFCTRCAT_5, "GetCategoryNames", mono_perfcounter_category_names)
ICALL(PERFCTRCAT_6, "GetCounterNames", mono_perfcounter_counter_names)
ICALL(PERFCTRCAT_7, "GetInstanceNames", mono_perfcounter_instance_names)
ICALL(PERFCTRCAT_8, "InstanceExistsInternal", mono_perfcounter_instance_exists)

ICALL_TYPE(PROCESS, "System.Diagnostics.Process", PROCESS_1)
ICALL(PROCESS_1, "CreateProcess_internal(System.Diagnostics.ProcessStartInfo,intptr,intptr,intptr,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_CreateProcess_internal)
ICALL(PROCESS_2, "ExitCode_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitCode_internal)
ICALL(PROCESS_3, "ExitTime_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitTime_internal)
ICALL(PROCESS_4, "GetModules_internal(intptr)", ves_icall_System_Diagnostics_Process_GetModules_internal)
ICALL(PROCESS_5, "GetPid_internal()", ves_icall_System_Diagnostics_Process_GetPid_internal)
ICALL(PROCESS_5B, "GetPriorityClass(intptr,int&)", ves_icall_System_Diagnostics_Process_GetPriorityClass)
ICALL(PROCESS_5H, "GetProcessData", ves_icall_System_Diagnostics_Process_GetProcessData)
ICALL(PROCESS_6, "GetProcess_internal(int)", ves_icall_System_Diagnostics_Process_GetProcess_internal)
ICALL(PROCESS_7, "GetProcesses_internal()", ves_icall_System_Diagnostics_Process_GetProcesses_internal)
ICALL(PROCESS_8, "GetWorkingSet_internal(intptr,int&,int&)", ves_icall_System_Diagnostics_Process_GetWorkingSet_internal)
ICALL(PROCESS_9, "Kill_internal", ves_icall_System_Diagnostics_Process_Kill_internal)
ICALL(PROCESS_10, "ProcessName_internal(intptr)", ves_icall_System_Diagnostics_Process_ProcessName_internal)
ICALL(PROCESS_11, "Process_free_internal(intptr)", ves_icall_System_Diagnostics_Process_Process_free_internal)
ICALL(PROCESS_11B, "SetPriorityClass(intptr,int,int&)", ves_icall_System_Diagnostics_Process_SetPriorityClass)
ICALL(PROCESS_12, "SetWorkingSet_internal(intptr,int,int,bool)", ves_icall_System_Diagnostics_Process_SetWorkingSet_internal)
ICALL(PROCESS_13, "ShellExecuteEx_internal(System.Diagnostics.ProcessStartInfo,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_ShellExecuteEx_internal)
ICALL(PROCESS_14, "StartTime_internal(intptr)", ves_icall_System_Diagnostics_Process_StartTime_internal)
ICALL(PROCESS_14M, "Times", ves_icall_System_Diagnostics_Process_Times)
ICALL(PROCESS_15, "WaitForExit_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForExit_internal)
ICALL(PROCESS_16, "WaitForInputIdle_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForInputIdle_internal)

ICALL_TYPE (PROCESSHANDLE, "System.Diagnostics.Process/ProcessWaitHandle", PROCESSHANDLE_1)
ICALL (PROCESSHANDLE_1, "ProcessHandle_close(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_close)
ICALL (PROCESSHANDLE_2, "ProcessHandle_duplicate(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_duplicate)
#endif /* !DISABLE_PROCESS_HANDLING */

ICALL_TYPE(STOPWATCH, "System.Diagnostics.Stopwatch", STOPWATCH_1)
ICALL(STOPWATCH_1, "GetTimestamp", mono_100ns_ticks)

ICALL_TYPE(DOUBLE, "System.Double", DOUBLE_1)
ICALL(DOUBLE_1, "ParseImpl",    mono_double_ParseImpl)

ICALL_TYPE(ENUM, "System.Enum", ENUM_1)
ICALL(ENUM_1, "ToObject", ves_icall_System_Enum_ToObject)
ICALL(ENUM_5, "compare_value_to", ves_icall_System_Enum_compare_value_to)
ICALL(ENUM_4, "get_hashcode", ves_icall_System_Enum_get_hashcode)
ICALL(ENUM_3, "get_underlying_type", ves_icall_System_Enum_get_underlying_type)
ICALL(ENUM_2, "get_value", ves_icall_System_Enum_get_value)

ICALL_TYPE(ENV, "System.Environment", ENV_1)
ICALL(ENV_1, "Exit", ves_icall_System_Environment_Exit)
ICALL(ENV_2, "GetCommandLineArgs", mono_runtime_get_main_args)
ICALL(ENV_3, "GetEnvironmentVariableNames", ves_icall_System_Environment_GetEnvironmentVariableNames)
ICALL(ENV_4, "GetLogicalDrivesInternal", ves_icall_System_Environment_GetLogicalDrives )
ICALL(ENV_5, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(ENV_51, "GetNewLine", ves_icall_System_Environment_get_NewLine)
ICALL(ENV_6, "GetOSVersionString", ves_icall_System_Environment_GetOSVersionString)
ICALL(ENV_6a, "GetPageSize", mono_pagesize)
ICALL(ENV_7, "GetWindowsFolderPath", ves_icall_System_Environment_GetWindowsFolderPath)
ICALL(ENV_8, "InternalSetEnvironmentVariable", ves_icall_System_Environment_InternalSetEnvironmentVariable)
ICALL(ENV_9, "get_ExitCode", mono_environment_exitcode_get)
ICALL(ENV_10, "get_HasShutdownStarted", ves_icall_System_Environment_get_HasShutdownStarted)
ICALL(ENV_11, "get_MachineName", ves_icall_System_Environment_get_MachineName)
ICALL(ENV_13, "get_Platform", ves_icall_System_Environment_get_Platform)
ICALL(ENV_14, "get_ProcessorCount", mono_cpu_count)
ICALL(ENV_15, "get_TickCount", mono_msec_ticks)
ICALL(ENV_16, "get_UserName", ves_icall_System_Environment_get_UserName)
ICALL(ENV_16m, "internalBroadcastSettingChange", ves_icall_System_Environment_BroadcastSettingChange)
ICALL(ENV_17, "internalGetEnvironmentVariable", ves_icall_System_Environment_GetEnvironmentVariable)
ICALL(ENV_18, "internalGetGacPath", ves_icall_System_Environment_GetGacPath)
ICALL(ENV_19, "internalGetHome", ves_icall_System_Environment_InternalGetHome)
ICALL(ENV_20, "set_ExitCode", mono_environment_exitcode_set)

ICALL_TYPE(GC, "System.GC", GC_0)
ICALL(GC_0, "CollectionCount", mono_gc_collection_count)
ICALL(GC_0a, "GetGeneration", mono_gc_get_generation)
ICALL(GC_1, "GetTotalMemory", ves_icall_System_GC_GetTotalMemory)
ICALL(GC_2, "InternalCollect", ves_icall_System_GC_InternalCollect)
ICALL(GC_3, "KeepAlive", ves_icall_System_GC_KeepAlive)
ICALL(GC_4, "ReRegisterForFinalize", ves_icall_System_GC_ReRegisterForFinalize)
ICALL(GC_4a, "RecordPressure", mono_gc_add_memory_pressure)
ICALL(GC_5, "SuppressFinalize", ves_icall_System_GC_SuppressFinalize)
ICALL(GC_6, "WaitForPendingFinalizers", ves_icall_System_GC_WaitForPendingFinalizers)
ICALL(GC_7, "get_MaxGeneration", mono_gc_max_generation)
ICALL(GC_9, "get_ephemeron_tombstone", ves_icall_System_GC_get_ephemeron_tombstone)
ICALL(GC_8, "register_ephemeron_array", ves_icall_System_GC_register_ephemeron_array)

ICALL_TYPE(COMPINF, "System.Globalization.CompareInfo", COMPINF_1)
ICALL(COMPINF_1, "assign_sortkey(object,string,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_assign_sortkey)
ICALL(COMPINF_2, "construct_compareinfo(string)", ves_icall_System_Globalization_CompareInfo_construct_compareinfo)
ICALL(COMPINF_3, "free_internal_collator()", ves_icall_System_Globalization_CompareInfo_free_internal_collator)
ICALL(COMPINF_4, "internal_compare(string,int,int,string,int,int,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_internal_compare)
ICALL(COMPINF_5, "internal_index(string,int,int,char,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index_char)
ICALL(COMPINF_6, "internal_index(string,int,int,string,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index)

ICALL_TYPE(CULINF, "System.Globalization.CultureInfo", CULINF_2)
ICALL(CULINF_2, "construct_datetime_format", ves_icall_System_Globalization_CultureInfo_construct_datetime_format)
ICALL(CULINF_4, "construct_internal_locale_from_current_locale", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_current_locale)
ICALL(CULINF_5, "construct_internal_locale_from_lcid", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_lcid)
ICALL(CULINF_6, "construct_internal_locale_from_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_name)
ICALL(CULINF_7, "construct_internal_locale_from_specific_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_specific_name)
ICALL(CULINF_8, "construct_number_format", ves_icall_System_Globalization_CultureInfo_construct_number_format)
ICALL(CULINF_9, "internal_get_cultures", ves_icall_System_Globalization_CultureInfo_internal_get_cultures)
//ICALL(CULINF_10, "internal_is_lcid_neutral", ves_icall_System_Globalization_CultureInfo_internal_is_lcid_neutral)

ICALL_TYPE(REGINF, "System.Globalization.RegionInfo", REGINF_1)
ICALL(REGINF_1, "construct_internal_region_from_lcid", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_lcid)
ICALL(REGINF_2, "construct_internal_region_from_name", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_name)

#ifndef PLATFORM_NO_DRIVEINFO
ICALL_TYPE(IODRIVEINFO, "System.IO.DriveInfo", IODRIVEINFO_1)
ICALL(IODRIVEINFO_1, "GetDiskFreeSpaceInternal", ves_icall_System_IO_DriveInfo_GetDiskFreeSpace)
ICALL(IODRIVEINFO_2, "GetDriveFormat", ves_icall_System_IO_DriveInfo_GetDriveFormat)
ICALL(IODRIVEINFO_3, "GetDriveTypeInternal", ves_icall_System_IO_DriveInfo_GetDriveType)
#endif

ICALL_TYPE(FAMW, "System.IO.FAMWatcher", FAMW_1)
ICALL(FAMW_1, "InternalFAMNextEvent", ves_icall_System_IO_FAMW_InternalFAMNextEvent)

ICALL_TYPE(FILEW, "System.IO.FileSystemWatcher", FILEW_4)
ICALL(FILEW_4, "InternalSupportsFSW", ves_icall_System_IO_FSW_SupportsFSW)

ICALL_TYPE(INOW, "System.IO.InotifyWatcher", INOW_1)
ICALL(INOW_1, "AddWatch", ves_icall_System_IO_InotifyWatcher_AddWatch)
ICALL(INOW_2, "GetInotifyInstance", ves_icall_System_IO_InotifyWatcher_GetInotifyInstance)
ICALL(INOW_3, "RemoveWatch", ves_icall_System_IO_InotifyWatcher_RemoveWatch)

#if defined (TARGET_IOS) || defined (TARGET_ANDROID)
ICALL_TYPE(MMAPIMPL, "System.IO.MemoryMappedFiles.MemoryMapImpl", MMAPIMPL_1)
ICALL(MMAPIMPL_1, "mono_filesize_from_fd", mono_filesize_from_fd)
ICALL(MMAPIMPL_2, "mono_filesize_from_path", mono_filesize_from_path)
#endif


ICALL_TYPE(MONOIO, "System.IO.MonoIO", MONOIO_1)
ICALL(MONOIO_1, "Close(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Close)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_2, "CopyFile(string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CopyFile)
ICALL(MONOIO_3, "CreateDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CreateDirectory)
ICALL(MONOIO_4, "CreatePipe(intptr&,intptr&)", ves_icall_System_IO_MonoIO_CreatePipe)
ICALL(MONOIO_5, "DeleteFile(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_DeleteFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_34, "DuplicateHandle", ves_icall_System_IO_MonoIO_DuplicateHandle)
ICALL(MONOIO_37, "FindClose", ves_icall_System_IO_MonoIO_FindClose)
ICALL(MONOIO_35, "FindFirst", ves_icall_System_IO_MonoIO_FindFirst)
ICALL(MONOIO_36, "FindNext", ves_icall_System_IO_MonoIO_FindNext)
ICALL(MONOIO_6, "Flush(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Flush)
ICALL(MONOIO_7, "GetCurrentDirectory(System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetCurrentDirectory)
ICALL(MONOIO_8, "GetFileAttributes(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileAttributes)
ICALL(MONOIO_9, "GetFileStat(string,System.IO.MonoIOStat&,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileStat)
ICALL(MONOIO_10, "GetFileSystemEntries", ves_icall_System_IO_MonoIO_GetFileSystemEntries)
ICALL(MONOIO_11, "GetFileType(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileType)
ICALL(MONOIO_12, "GetLength(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_13, "GetTempPath(string&)", ves_icall_System_IO_MonoIO_GetTempPath)
ICALL(MONOIO_14, "Lock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Lock)
ICALL(MONOIO_15, "MoveFile(string,string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_MoveFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_16, "Open(string,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.IO.FileOptions,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Open)
ICALL(MONOIO_17, "Read(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Read)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_18, "RemoveDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_RemoveDirectory)
ICALL(MONOIO_18M, "ReplaceFile(string,string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_ReplaceFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_19, "Seek(intptr,long,System.IO.SeekOrigin,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Seek)
ICALL(MONOIO_20, "SetCurrentDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetCurrentDirectory)
ICALL(MONOIO_21, "SetFileAttributes(string,System.IO.FileAttributes,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileAttributes)
ICALL(MONOIO_22, "SetFileTime(intptr,long,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileTime)
ICALL(MONOIO_23, "SetLength(intptr,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_24, "Unlock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Unlock)
#endif
ICALL(MONOIO_25, "Write(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Write)
ICALL(MONOIO_26, "get_AltDirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_AltDirectorySeparatorChar)
ICALL(MONOIO_27, "get_ConsoleError", ves_icall_System_IO_MonoIO_get_ConsoleError)
ICALL(MONOIO_28, "get_ConsoleInput", ves_icall_System_IO_MonoIO_get_ConsoleInput)
ICALL(MONOIO_29, "get_ConsoleOutput", ves_icall_System_IO_MonoIO_get_ConsoleOutput)
ICALL(MONOIO_30, "get_DirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_DirectorySeparatorChar)
ICALL(MONOIO_31, "get_InvalidPathChars", ves_icall_System_IO_MonoIO_get_InvalidPathChars)
ICALL(MONOIO_32, "get_PathSeparator", ves_icall_System_IO_MonoIO_get_PathSeparator)
ICALL(MONOIO_33, "get_VolumeSeparatorChar", ves_icall_System_IO_MonoIO_get_VolumeSeparatorChar)

ICALL_TYPE(IOPATH, "System.IO.Path", IOPATH_1)
ICALL(IOPATH_1, "get_temp_path", ves_icall_System_IO_get_temp_path)

ICALL_TYPE(MATH, "System.Math", MATH_1)
ICALL(MATH_1, "Acos", ves_icall_System_Math_Acos)
ICALL(MATH_2, "Asin", ves_icall_System_Math_Asin)
ICALL(MATH_3, "Atan", ves_icall_System_Math_Atan)
ICALL(MATH_4, "Atan2", ves_icall_System_Math_Atan2)
ICALL(MATH_5, "Cos", ves_icall_System_Math_Cos)
ICALL(MATH_6, "Cosh", ves_icall_System_Math_Cosh)
ICALL(MATH_7, "Exp", ves_icall_System_Math_Exp)
ICALL(MATH_8, "Floor", ves_icall_System_Math_Floor)
ICALL(MATH_9, "Log", ves_icall_System_Math_Log)
ICALL(MATH_10, "Log10", ves_icall_System_Math_Log10)
ICALL(MATH_11, "Pow", ves_icall_System_Math_Pow)
ICALL(MATH_12, "Round", ves_icall_System_Math_Round)
ICALL(MATH_13, "Round2", ves_icall_System_Math_Round2)
ICALL(MATH_14, "Sin", ves_icall_System_Math_Sin)
ICALL(MATH_15, "Sinh", ves_icall_System_Math_Sinh)
ICALL(MATH_16, "Sqrt", ves_icall_System_Math_Sqrt)
ICALL(MATH_17, "Tan", ves_icall_System_Math_Tan)
ICALL(MATH_18, "Tanh", ves_icall_System_Math_Tanh)

ICALL_TYPE(MCATTR, "System.MonoCustomAttrs", MCATTR_1)
ICALL(MCATTR_1, "GetCustomAttributesDataInternal", mono_reflection_get_custom_attrs_data)
ICALL(MCATTR_2, "GetCustomAttributesInternal", custom_attrs_get_by_type)
ICALL(MCATTR_3, "IsDefinedInternal", custom_attrs_defined_internal)

ICALL_TYPE(MENUM, "System.MonoEnumInfo", MENUM_1)
ICALL(MENUM_1, "get_enum_info", ves_icall_get_enum_info)

ICALL_TYPE(MTYPE, "System.MonoType", MTYPE_1)
ICALL(MTYPE_1, "GetArrayRank", ves_icall_MonoType_GetArrayRank)
ICALL(MTYPE_2, "GetConstructors", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_3, "GetConstructors_internal", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_4, "GetCorrespondingInflatedConstructor", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_5, "GetCorrespondingInflatedMethod", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_6, "GetElementType", ves_icall_MonoType_GetElementType)
ICALL(MTYPE_7, "GetEvents_internal", ves_icall_Type_GetEvents_internal)
ICALL(MTYPE_8, "GetField", ves_icall_Type_GetField)
ICALL(MTYPE_9, "GetFields_internal", ves_icall_Type_GetFields_internal)
ICALL(MTYPE_10, "GetGenericArguments", ves_icall_MonoType_GetGenericArguments)
ICALL(MTYPE_11, "GetInterfaces", ves_icall_Type_GetInterfaces)
ICALL(MTYPE_12, "GetMethodsByName", ves_icall_Type_GetMethodsByName)
ICALL(MTYPE_13, "GetNestedType", ves_icall_Type_GetNestedType)
ICALL(MTYPE_14, "GetNestedTypes", ves_icall_Type_GetNestedTypes)
ICALL(MTYPE_15, "GetPropertiesByName", ves_icall_Type_GetPropertiesByName)
ICALL(MTYPE_16, "InternalGetEvent", ves_icall_MonoType_GetEvent)
ICALL(MTYPE_17, "IsByRefImpl", ves_icall_type_isbyref)
ICALL(MTYPE_18, "IsCOMObjectImpl", ves_icall_type_iscomobject)
ICALL(MTYPE_19, "IsPointerImpl", ves_icall_type_ispointer)
ICALL(MTYPE_20, "IsPrimitiveImpl", ves_icall_type_isprimitive)
ICALL(MTYPE_21, "getFullName", ves_icall_System_MonoType_getFullName)
ICALL(MTYPE_22, "get_Assembly", ves_icall_MonoType_get_Assembly)
ICALL(MTYPE_23, "get_BaseType", ves_icall_get_type_parent)
ICALL(MTYPE_24, "get_DeclaringMethod", ves_icall_MonoType_get_DeclaringMethod)
ICALL(MTYPE_25, "get_DeclaringType", ves_icall_MonoType_get_DeclaringType)
ICALL(MTYPE_26, "get_IsGenericParameter", ves_icall_MonoType_get_IsGenericParameter)
ICALL(MTYPE_27, "get_Module", ves_icall_MonoType_get_Module)
ICALL(MTYPE_28, "get_Name", ves_icall_MonoType_get_Name)
ICALL(MTYPE_29, "get_Namespace", ves_icall_MonoType_get_Namespace)
ICALL(MTYPE_31, "get_attributes", ves_icall_get_attributes)
ICALL(MTYPE_33, "get_core_clr_security_level", vell_icall_MonoType_get_core_clr_security_level)
ICALL(MTYPE_32, "type_from_obj", mono_type_type_from_obj)

#ifndef DISABLE_SOCKETS
ICALL_TYPE(NDNS, "System.Net.Dns", NDNS_1)
ICALL(NDNS_1, "GetHostByAddr_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByAddr_internal)
ICALL(NDNS_2, "GetHostByName_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByName_internal)
ICALL(NDNS_3, "GetHostName_internal(string&)", ves_icall_System_Net_Dns_GetHostName_internal)

ICALL_TYPE(SOCK, "System.Net.Sockets.Socket", SOCK_1)
ICALL(SOCK_1, "Accept_internal(intptr,int&,bool)", ves_icall_System_Net_Sockets_Socket_Accept_internal)
ICALL(SOCK_2, "Available_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Available_internal)
ICALL(SOCK_3, "Bind_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Bind_internal)
ICALL(SOCK_4, "Blocking_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Blocking_internal)
ICALL(SOCK_5, "Close_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Close_internal)
ICALL(SOCK_6, "Connect_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Connect_internal)
ICALL (SOCK_6a, "Disconnect_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Disconnect_internal)
ICALL(SOCK_7, "GetSocketOption_arr_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,byte[]&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_arr_internal)
ICALL(SOCK_8, "GetSocketOption_obj_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_obj_internal)
ICALL(SOCK_9, "Listen_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_Listen_internal)
ICALL(SOCK_10, "LocalEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_LocalEndPoint_internal)
ICALL(SOCK_11, "Poll_internal", ves_icall_System_Net_Sockets_Socket_Poll_internal)
ICALL(SOCK_11a, "Receive_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_array_internal)
ICALL(SOCK_12, "Receive_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_internal)
ICALL(SOCK_13, "RecvFrom_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress&,int&)", ves_icall_System_Net_Sockets_Socket_RecvFrom_internal)
ICALL(SOCK_14, "RemoteEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_RemoteEndPoint_internal)
ICALL(SOCK_15, "Select_internal(System.Net.Sockets.Socket[]&,int,int&)", ves_icall_System_Net_Sockets_Socket_Select_internal)
ICALL(SOCK_15a, "SendFile(intptr,string,byte[],byte[],System.Net.Sockets.TransmitFileOptions)", ves_icall_System_Net_Sockets_Socket_SendFile)
ICALL(SOCK_16, "SendTo_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_SendTo_internal)
ICALL(SOCK_16a, "Send_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_array_internal)
ICALL(SOCK_17, "Send_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_internal)
ICALL(SOCK_18, "SetSocketOption_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object,byte[],int,int&)", ves_icall_System_Net_Sockets_Socket_SetSocketOption_internal)
ICALL(SOCK_19, "Shutdown_internal(intptr,System.Net.Sockets.SocketShutdown,int&)", ves_icall_System_Net_Sockets_Socket_Shutdown_internal)
ICALL(SOCK_20, "Socket_internal(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType,int&)", ves_icall_System_Net_Sockets_Socket_Socket_internal)
ICALL(SOCK_21, "WSAIoctl(intptr,int,byte[],byte[],int&)", ves_icall_System_Net_Sockets_Socket_WSAIoctl)
ICALL(SOCK_21a, "cancel_blocking_socket_operation", icall_cancel_blocking_socket_operation)
ICALL(SOCK_22, "socket_pool_queue", icall_append_io_job)

ICALL_TYPE(SOCKEX, "System.Net.Sockets.SocketException", SOCKEX_1)
ICALL(SOCKEX_1, "WSAGetLastError_internal", ves_icall_System_Net_Sockets_SocketException_WSAGetLastError_internal)
#endif /* !DISABLE_SOCKETS */

ICALL_TYPE(NUMBER_FORMATTER, "System.NumberFormatter", NUMBER_FORMATTER_1)
ICALL(NUMBER_FORMATTER_1, "GetFormatterTables", ves_icall_System_NumberFormatter_GetFormatterTables)

ICALL_TYPE(OBJ, "System.Object", OBJ_1)
ICALL(OBJ_1, "GetType", ves_icall_System_Object_GetType)
ICALL(OBJ_2, "InternalGetHashCode", mono_object_hash)
ICALL(OBJ_3, "MemberwiseClone", ves_icall_System_Object_MemberwiseClone)
ICALL(OBJ_4, "obj_address", ves_icall_System_Object_obj_address)

ICALL_TYPE(ASSEM, "System.Reflection.Assembly", ASSEM_1)
ICALL(ASSEM_1, "FillName", ves_icall_System_Reflection_Assembly_FillName)
ICALL(ASSEM_2, "GetCallingAssembly", ves_icall_System_Reflection_Assembly_GetCallingAssembly)
ICALL(ASSEM_3, "GetEntryAssembly", ves_icall_System_Reflection_Assembly_GetEntryAssembly)
ICALL(ASSEM_4, "GetExecutingAssembly", ves_icall_System_Reflection_Assembly_GetExecutingAssembly)
ICALL(ASSEM_5, "GetFilesInternal", ves_icall_System_Reflection_Assembly_GetFilesInternal)
ICALL(ASSEM_6, "GetManifestModuleInternal", ves_icall_System_Reflection_Assembly_GetManifestModuleInternal)
ICALL(ASSEM_7, "GetManifestResourceInfoInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInfoInternal)
ICALL(ASSEM_8, "GetManifestResourceInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInternal)
ICALL(ASSEM_9, "GetManifestResourceNames", ves_icall_System_Reflection_Assembly_GetManifestResourceNames)
ICALL(ASSEM_10, "GetModulesInternal", ves_icall_System_Reflection_Assembly_GetModulesInternal)
//ICALL(ASSEM_11, "GetNamespaces", ves_icall_System_Reflection_Assembly_GetNamespaces)
ICALL(ASSEM_12, "GetReferencedAssemblies", ves_icall_System_Reflection_Assembly_GetReferencedAssemblies)
ICALL(ASSEM_13, "GetTypes", ves_icall_System_Reflection_Assembly_GetTypes)
ICALL(ASSEM_14, "InternalGetAssemblyName", ves_icall_System_Reflection_Assembly_InternalGetAssemblyName)
ICALL(ASSEM_15, "InternalGetType", ves_icall_System_Reflection_Assembly_InternalGetType)
ICALL(ASSEM_16, "InternalImageRuntimeVersion", ves_icall_System_Reflection_Assembly_InternalImageRuntimeVersion)
ICALL(ASSEM_17, "LoadFrom", ves_icall_System_Reflection_Assembly_LoadFrom)
ICALL(ASSEM_18, "LoadPermissions", ves_icall_System_Reflection_Assembly_LoadPermissions)
	/*
	 * Private icalls for the Mono Debugger
	 */
ICALL(ASSEM_19, "MonoDebugger_GetMethodToken", ves_icall_MonoDebugger_GetMethodToken)

	/* normal icalls again */
ICALL(ASSEM_20, "get_EntryPoint", ves_icall_System_Reflection_Assembly_get_EntryPoint)
ICALL(ASSEM_21, "get_ReflectionOnly", ves_icall_System_Reflection_Assembly_get_ReflectionOnly)
ICALL(ASSEM_22, "get_code_base", ves_icall_System_Reflection_Assembly_get_code_base)
ICALL(ASSEM_23, "get_fullname", ves_icall_System_Reflection_Assembly_get_fullName)
ICALL(ASSEM_24, "get_global_assembly_cache", ves_icall_System_Reflection_Assembly_get_global_assembly_cache)
ICALL(ASSEM_25, "get_location", ves_icall_System_Reflection_Assembly_get_location)
ICALL(ASSEM_26, "load_with_partial_name", ves_icall_System_Reflection_Assembly_load_with_partial_name)

ICALL_TYPE(ASSEMN, "System.Reflection.AssemblyName", ASSEMN_1)
ICALL(ASSEMN_1, "ParseName", ves_icall_System_Reflection_AssemblyName_ParseName)
ICALL(ASSEMN_2, "get_public_token", mono_digest_get_public_token)

ICALL_TYPE(CATTR_DATA, "System.Reflection.CustomAttributeData", CATTR_DATA_1)
ICALL(CATTR_DATA_1, "ResolveArgumentsInternal", mono_reflection_resolve_custom_attribute_data)

ICALL_TYPE(ASSEMB, "System.Reflection.Emit.AssemblyBuilder", ASSEMB_1)
ICALL(ASSEMB_1, "InternalAddModule", mono_image_load_module_dynamic)
ICALL(ASSEMB_2, "basic_init", mono_image_basic_init)

ICALL_TYPE(CATTRB, "System.Reflection.Emit.CustomAttributeBuilder", CATTRB_1)
ICALL(CATTRB_1, "GetBlob", mono_reflection_get_custom_attrs_blob)

#ifndef DISABLE_REFLECTION_EMIT
ICALL_TYPE(DERIVEDTYPE, "System.Reflection.Emit.DerivedType", DERIVEDTYPE_1)
ICALL(DERIVEDTYPE_1, "create_unmanaged_type", mono_reflection_create_unmanaged_type)
#endif

ICALL_TYPE(DYNM, "System.Reflection.Emit.DynamicMethod", DYNM_1)
ICALL(DYNM_1, "create_dynamic_method", mono_reflection_create_dynamic_method)

ICALL_TYPE(ENUMB, "System.Reflection.Emit.EnumBuilder", ENUMB_1)
ICALL(ENUMB_1, "setup_enum_type", ves_icall_EnumBuilder_setup_enum_type)

ICALL_TYPE(GPARB, "System.Reflection.Emit.GenericTypeParameterBuilder", GPARB_1)
ICALL(GPARB_1, "initialize", mono_reflection_initialize_generic_parameter)

ICALL_TYPE(METHODB, "System.Reflection.Emit.MethodBuilder", METHODB_1)
ICALL(METHODB_1, "MakeGenericMethod", mono_reflection_bind_generic_method_parameters)

ICALL_TYPE(MODULEB, "System.Reflection.Emit.ModuleBuilder", MODULEB_8)
ICALL(MODULEB_8, "RegisterToken", ves_icall_ModuleBuilder_RegisterToken)
ICALL(MODULEB_1, "WriteToFile", ves_icall_ModuleBuilder_WriteToFile)
ICALL(MODULEB_2, "basic_init", mono_image_module_basic_init)
ICALL(MODULEB_3, "build_metadata", ves_icall_ModuleBuilder_build_metadata)
ICALL(MODULEB_4, "create_modified_type", ves_icall_ModuleBuilder_create_modified_type)
ICALL(MODULEB_5, "getMethodToken", ves_icall_ModuleBuilder_getMethodToken)
ICALL(MODULEB_6, "getToken", ves_icall_ModuleBuilder_getToken)
ICALL(MODULEB_7, "getUSIndex", mono_image_insert_string)
ICALL(MODULEB_9, "set_wrappers_type", mono_image_set_wrappers_type)

ICALL_TYPE(SIGH, "System.Reflection.Emit.SignatureHelper", SIGH_1)
ICALL(SIGH_1, "get_signature_field", mono_reflection_sighelper_get_signature_field)
ICALL(SIGH_2, "get_signature_local", mono_reflection_sighelper_get_signature_local)

ICALL_TYPE(TYPEB, "System.Reflection.Emit.TypeBuilder", TYPEB_1)
ICALL(TYPEB_1, "create_generic_class", mono_reflection_create_generic_class)
ICALL(TYPEB_2, "create_internal_class", mono_reflection_create_internal_class)
ICALL(TYPEB_3, "create_runtime_class", mono_reflection_create_runtime_class)
ICALL(TYPEB_4, "get_IsGenericParameter", ves_icall_TypeBuilder_get_IsGenericParameter)
ICALL(TYPEB_5, "get_event_info", mono_reflection_event_builder_get_event_info)
ICALL(TYPEB_6, "setup_generic_class", mono_reflection_setup_generic_class)
ICALL(TYPEB_7, "setup_internal_class", mono_reflection_setup_internal_class)

ICALL_TYPE(FIELDI, "System.Reflection.FieldInfo", FILEDI_1)
ICALL(FILEDI_1, "GetTypeModifiers", ves_icall_System_Reflection_FieldInfo_GetTypeModifiers)
ICALL(FILEDI_2, "get_marshal_info", ves_icall_System_Reflection_FieldInfo_get_marshal_info)
ICALL(FILEDI_3, "internal_from_handle_type", ves_icall_System_Reflection_FieldInfo_internal_from_handle_type)

ICALL_TYPE(MEMBERI, "System.Reflection.MemberInfo", MEMBERI_1)
ICALL(MEMBERI_1, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MBASE, "System.Reflection.MethodBase", MBASE_1)
ICALL(MBASE_1, "GetCurrentMethod", ves_icall_GetCurrentMethod)
ICALL(MBASE_2, "GetMethodBodyInternal", ves_icall_System_Reflection_MethodBase_GetMethodBodyInternal)
ICALL(MBASE_3, "GetMethodFromHandleInternal", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternal)
ICALL(MBASE_4, "GetMethodFromHandleInternalType", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternalType)

ICALL_TYPE(MODULE, "System.Reflection.Module", MODULE_1)
ICALL(MODULE_1, "Close", ves_icall_System_Reflection_Module_Close)
ICALL(MODULE_2, "GetGlobalType", ves_icall_System_Reflection_Module_GetGlobalType)
ICALL(MODULE_3, "GetGuidInternal", ves_icall_System_Reflection_Module_GetGuidInternal)
ICALL(MODULE_14, "GetHINSTANCE", ves_icall_System_Reflection_Module_GetHINSTANCE)
ICALL(MODULE_4, "GetMDStreamVersion", ves_icall_System_Reflection_Module_GetMDStreamVersion)
ICALL(MODULE_5, "GetPEKind", ves_icall_System_Reflection_Module_GetPEKind)
ICALL(MODULE_6, "InternalGetTypes", ves_icall_System_Reflection_Module_InternalGetTypes)
ICALL(MODULE_7, "ResolveFieldToken", ves_icall_System_Reflection_Module_ResolveFieldToken)
ICALL(MODULE_8, "ResolveMemberToken", ves_icall_System_Reflection_Module_ResolveMemberToken)
ICALL(MODULE_9, "ResolveMethodToken", ves_icall_System_Reflection_Module_ResolveMethodToken)
ICALL(MODULE_10, "ResolveSignature", ves_icall_System_Reflection_Module_ResolveSignature)
ICALL(MODULE_11, "ResolveStringToken", ves_icall_System_Reflection_Module_ResolveStringToken)
ICALL(MODULE_12, "ResolveTypeToken", ves_icall_System_Reflection_Module_ResolveTypeToken)
ICALL(MODULE_13, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MCMETH, "System.Reflection.MonoCMethod", MCMETH_1)
ICALL(MCMETH_1, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MCMETH_2, "InternalInvoke", ves_icall_InternalInvoke)

ICALL_TYPE(MEVIN, "System.Reflection.MonoEventInfo", MEVIN_1)
ICALL(MEVIN_1, "get_event_info", ves_icall_get_event_info)

ICALL_TYPE(MFIELD, "System.Reflection.MonoField", MFIELD_1)
ICALL(MFIELD_1, "GetFieldOffset", ves_icall_MonoField_GetFieldOffset)
ICALL(MFIELD_2, "GetParentType", ves_icall_MonoField_GetParentType)
ICALL(MFIELD_5, "GetRawConstantValue", ves_icall_MonoField_GetRawConstantValue)
ICALL(MFIELD_3, "GetValueInternal", ves_icall_MonoField_GetValueInternal)
ICALL(MFIELD_6, "ResolveType", ves_icall_MonoField_ResolveType)
ICALL(MFIELD_4, "SetValueInternal", ves_icall_MonoField_SetValueInternal)

ICALL_TYPE(MGENCM, "System.Reflection.MonoGenericCMethod", MGENCM_1)
ICALL(MGENCM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MGENCL, "System.Reflection.MonoGenericClass", MGENCL_5)
ICALL(MGENCL_5, "initialize", mono_reflection_generic_class_initialize)
ICALL(MGENCL_6, "register_with_runtime", mono_reflection_register_with_runtime)

/* note this is the same as above: unify */
ICALL_TYPE(MGENM, "System.Reflection.MonoGenericMethod", MGENM_1)
ICALL(MGENM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MMETH, "System.Reflection.MonoMethod", MMETH_1)
ICALL(MMETH_1, "GetDllImportAttribute", ves_icall_MonoMethod_GetDllImportAttribute)
ICALL(MMETH_2, "GetGenericArguments", ves_icall_MonoMethod_GetGenericArguments)
ICALL(MMETH_3, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MMETH_4, "InternalInvoke", ves_icall_InternalInvoke)
ICALL(MMETH_5, "MakeGenericMethod_impl", mono_reflection_bind_generic_method_parameters)
ICALL(MMETH_6, "get_IsGenericMethod", ves_icall_MonoMethod_get_IsGenericMethod)
ICALL(MMETH_7, "get_IsGenericMethodDefinition", ves_icall_MonoMethod_get_IsGenericMethodDefinition)
ICALL(MMETH_8, "get_base_method", ves_icall_MonoMethod_get_base_method)
ICALL(MMETH_9, "get_name", ves_icall_MonoMethod_get_name)

ICALL_TYPE(MMETHI, "System.Reflection.MonoMethodInfo", MMETHI_4)
ICALL(MMETHI_4, "get_method_attributes", vell_icall_get_method_attributes)
ICALL(MMETHI_1, "get_method_info", ves_icall_get_method_info)
ICALL(MMETHI_2, "get_parameter_info", ves_icall_get_parameter_info)
ICALL(MMETHI_3, "get_retval_marshal", ves_icall_System_MonoMethodInfo_get_retval_marshal)

ICALL_TYPE(MPROPI, "System.Reflection.MonoPropertyInfo", MPROPI_1)
ICALL(MPROPI_1, "GetTypeModifiers", property_info_get_type_modifiers)
ICALL(MPROPI_3, "get_default_value", property_info_get_default_value)
ICALL(MPROPI_2, "get_property_info", ves_icall_get_property_info)

ICALL_TYPE(PARAMI, "System.Reflection.ParameterInfo", PARAMI_1)
ICALL(PARAMI_1, "GetMetadataToken", mono_reflection_get_token)
ICALL(PARAMI_2, "GetTypeModifiers", param_info_get_type_modifiers)

ICALL_TYPE(RUNH, "System.Runtime.CompilerServices.RuntimeHelpers", RUNH_1)
ICALL(RUNH_1, "GetObjectValue", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetObjectValue)
	 /* REMOVEME: no longer needed, just so we dont break things when not needed */
ICALL(RUNH_2, "GetOffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)
ICALL(RUNH_3, "InitializeArray", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_InitializeArray)
ICALL(RUNH_4, "RunClassConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunClassConstructor)
ICALL(RUNH_5, "RunModuleConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunModuleConstructor)
ICALL(RUNH_5h, "SufficientExecutionStack", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_SufficientExecutionStack)
ICALL(RUNH_6, "get_OffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)

ICALL_TYPE(GCH, "System.Runtime.InteropServices.GCHandle", GCH_1)
ICALL(GCH_1, "CheckCurrentDomain", GCHandle_CheckCurrentDomain)
ICALL(GCH_2, "FreeHandle", ves_icall_System_GCHandle_FreeHandle)
ICALL(GCH_3, "GetAddrOfPinnedObject", ves_icall_System_GCHandle_GetAddrOfPinnedObject)
ICALL(GCH_4, "GetTarget", ves_icall_System_GCHandle_GetTarget)
ICALL(GCH_5, "GetTargetHandle", ves_icall_System_GCHandle_GetTargetHandle)

#ifndef DISABLE_COM
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_1)
ICALL(MARSHAL_1, "AddRefInternal", ves_icall_System_Runtime_InteropServices_Marshal_AddRefInternal)
#else
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_2)
#endif
ICALL(MARSHAL_2, "AllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_AllocCoTaskMem)
ICALL(MARSHAL_3, "AllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_AllocHGlobal)
ICALL(MARSHAL_4, "DestroyStructure", ves_icall_System_Runtime_InteropServices_Marshal_DestroyStructure)
ICALL(MARSHAL_5, "FreeBSTR", ves_icall_System_Runtime_InteropServices_Marshal_FreeBSTR)
ICALL(MARSHAL_6, "FreeCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_FreeCoTaskMem)
ICALL(MARSHAL_7, "FreeHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_FreeHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_44, "GetCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetCCW)
ICALL(MARSHAL_8, "GetComSlotForMethodInfoInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetComSlotForMethodInfoInternal)
#endif
ICALL(MARSHAL_9, "GetDelegateForFunctionPointerInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetDelegateForFunctionPointerInternal)
ICALL(MARSHAL_10, "GetFunctionPointerForDelegateInternal", mono_delegate_to_ftnptr)
#ifndef DISABLE_COM
ICALL(MARSHAL_45, "GetIDispatchForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIDispatchForObjectInternal)
ICALL(MARSHAL_46, "GetIUnknownForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIUnknownForObjectInternal)
#endif
ICALL(MARSHAL_11, "GetLastWin32Error", ves_icall_System_Runtime_InteropServices_Marshal_GetLastWin32Error)
#ifndef DISABLE_COM
ICALL(MARSHAL_47, "GetObjectForCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetObjectForCCW)
ICALL(MARSHAL_48, "IsComObject", ves_icall_System_Runtime_InteropServices_Marshal_IsComObject)
#endif
ICALL(MARSHAL_12, "OffsetOf", ves_icall_System_Runtime_InteropServices_Marshal_OffsetOf)
ICALL(MARSHAL_13, "Prelink", ves_icall_System_Runtime_InteropServices_Marshal_Prelink)
ICALL(MARSHAL_14, "PrelinkAll", ves_icall_System_Runtime_InteropServices_Marshal_PrelinkAll)
ICALL(MARSHAL_15, "PtrToStringAnsi(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi)
ICALL(MARSHAL_16, "PtrToStringAnsi(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi_len)
#ifndef DISABLE_COM
ICALL(MARSHAL_17, "PtrToStringBSTR", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringBSTR)
#endif
ICALL(MARSHAL_18, "PtrToStringUni(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni)
ICALL(MARSHAL_19, "PtrToStringUni(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni_len)
ICALL(MARSHAL_20, "PtrToStructure(intptr,System.Type)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure_type)
ICALL(MARSHAL_21, "PtrToStructure(intptr,object)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure)
#ifndef DISABLE_COM
ICALL(MARSHAL_22, "QueryInterfaceInternal", ves_icall_System_Runtime_InteropServices_Marshal_QueryInterfaceInternal)
#endif
ICALL(MARSHAL_43, "ReAllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocCoTaskMem)
ICALL(MARSHAL_23, "ReAllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_49, "ReleaseComObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseComObjectInternal)
ICALL(MARSHAL_29, "ReleaseInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseInternal)
#endif
ICALL(MARSHAL_30, "SizeOf", ves_icall_System_Runtime_InteropServices_Marshal_SizeOf)
ICALL(MARSHAL_31, "StringToBSTR", ves_icall_System_Runtime_InteropServices_Marshal_StringToBSTR)
ICALL(MARSHAL_32, "StringToHGlobalAnsi", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalAnsi)
ICALL(MARSHAL_33, "StringToHGlobalUni", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalUni)
ICALL(MARSHAL_34, "StructureToPtr", ves_icall_System_Runtime_InteropServices_Marshal_StructureToPtr)
ICALL(MARSHAL_35, "UnsafeAddrOfPinnedArrayElement", ves_icall_System_Runtime_InteropServices_Marshal_UnsafeAddrOfPinnedArrayElement)
ICALL(MARSHAL_41, "copy_from_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_from_unmanaged)
ICALL(MARSHAL_42, "copy_to_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_to_unmanaged)

ICALL_TYPE(ACTS, "System.Runtime.Remoting.Activation.ActivationServices", ACTS_1)
ICALL(ACTS_1, "AllocateUninitializedClassInstance", ves_icall_System_Runtime_Activation_ActivationServices_AllocateUninitializedClassInstance)
ICALL(ACTS_2, "EnableProxyActivation", ves_icall_System_Runtime_Activation_ActivationServices_EnableProxyActivation)

ICALL_TYPE(MONOMM, "System.Runtime.Remoting.Messaging.MonoMethodMessage", MONOMM_1)
ICALL(MONOMM_1, "InitMessage", ves_icall_MonoMethodMessage_InitMessage)

#ifndef DISABLE_REMOTING
ICALL_TYPE(REALP, "System.Runtime.Remoting.Proxies.RealProxy", REALP_1)
ICALL(REALP_1, "InternalGetProxyType", ves_icall_Remoting_RealProxy_InternalGetProxyType)
ICALL(REALP_2, "InternalGetTransparentProxy", ves_icall_Remoting_RealProxy_GetTransparentProxy)

ICALL_TYPE(REMSER, "System.Runtime.Remoting.RemotingServices", REMSER_0)
ICALL(REMSER_0, "GetVirtualMethod", ves_icall_Remoting_RemotingServices_GetVirtualMethod)
ICALL(REMSER_1, "InternalExecute", ves_icall_InternalExecute)
ICALL(REMSER_2, "IsTransparentProxy", ves_icall_IsTransparentProxy)
#endif

ICALL_TYPE(MHAN, "System.RuntimeMethodHandle", MHAN_1)
ICALL(MHAN_1, "GetFunctionPointer", ves_icall_RuntimeMethod_GetFunctionPointer)

ICALL_TYPE(RNG, "System.Security.Cryptography.RNGCryptoServiceProvider", RNG_1)
ICALL(RNG_1, "RngClose", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngClose)
ICALL(RNG_2, "RngGetBytes", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngGetBytes)
ICALL(RNG_3, "RngInitialize", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngInitialize)
ICALL(RNG_4, "RngOpen", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngOpen)

#ifndef DISABLE_POLICY_EVIDENCE
ICALL_TYPE(EVID, "System.Security.Policy.Evidence", EVID_1)
ICALL(EVID_1, "IsAuthenticodePresent", ves_icall_System_Security_Policy_Evidence_IsAuthenticodePresent)

ICALL_TYPE(WINID, "System.Security.Principal.WindowsIdentity", WINID_1)
ICALL(WINID_1, "GetCurrentToken", ves_icall_System_Security_Principal_WindowsIdentity_GetCurrentToken)
ICALL(WINID_2, "GetTokenName", ves_icall_System_Security_Principal_WindowsIdentity_GetTokenName)
ICALL(WINID_3, "GetUserToken", ves_icall_System_Security_Principal_WindowsIdentity_GetUserToken)
ICALL(WINID_4, "_GetRoles", ves_icall_System_Security_Principal_WindowsIdentity_GetRoles)

ICALL_TYPE(WINIMP, "System.Security.Principal.WindowsImpersonationContext", WINIMP_1)
ICALL(WINIMP_1, "CloseToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_CloseToken)
ICALL(WINIMP_2, "DuplicateToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_DuplicateToken)
ICALL(WINIMP_3, "RevertToSelf", ves_icall_System_Security_Principal_WindowsImpersonationContext_RevertToSelf)
ICALL(WINIMP_4, "SetCurrentToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_SetCurrentToken)

ICALL_TYPE(WINPRIN, "System.Security.Principal.WindowsPrincipal", WINPRIN_1)
ICALL(WINPRIN_1, "IsMemberOfGroupId", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupId)
ICALL(WINPRIN_2, "IsMemberOfGroupName", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupName)

ICALL_TYPE(SECSTRING, "System.Security.SecureString", SECSTRING_1)
ICALL(SECSTRING_1, "DecryptInternal", ves_icall_System_Security_SecureString_DecryptInternal)
ICALL(SECSTRING_2, "EncryptInternal", ves_icall_System_Security_SecureString_EncryptInternal)
#endif /* !DISABLE_POLICY_EVIDENCE */

ICALL_TYPE(SECMAN, "System.Security.SecurityManager", SECMAN_1)
ICALL(SECMAN_1, "GetLinkDemandSecurity", ves_icall_System_Security_SecurityManager_GetLinkDemandSecurity)
ICALL(SECMAN_2, "get_CheckExecutionRights", ves_icall_System_Security_SecurityManager_get_CheckExecutionRights)
ICALL(SECMAN_3, "get_RequiresElevatedPermissions", mono_security_core_clr_require_elevated_permissions)
ICALL(SECMAN_4, "get_SecurityEnabled", ves_icall_System_Security_SecurityManager_get_SecurityEnabled)
ICALL(SECMAN_5, "set_CheckExecutionRights", ves_icall_System_Security_SecurityManager_set_CheckExecutionRights)
ICALL(SECMAN_6, "set_SecurityEnabled", ves_icall_System_Security_SecurityManager_set_SecurityEnabled)

ICALL_TYPE(STRING, "System.String", STRING_1)
ICALL(STRING_1, ".ctor(char*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_2, ".ctor(char*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_3, ".ctor(char,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_4, ".ctor(char[])", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_5, ".ctor(char[],int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_6, ".ctor(sbyte*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_7, ".ctor(sbyte*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8, ".ctor(sbyte*,int,int,System.Text.Encoding)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8a, "GetLOSLimit", ves_icall_System_String_GetLOSLimit)
ICALL(STRING_9, "InternalAllocateStr", ves_icall_System_String_InternalAllocateStr)
ICALL(STRING_10, "InternalIntern", ves_icall_System_String_InternalIntern)
ICALL(STRING_11, "InternalIsInterned", ves_icall_System_String_InternalIsInterned)

ICALL_TYPE(TENC, "System.Text.Encoding", TENC_1)
ICALL(TENC_1, "InternalCodePage", ves_icall_System_Text_Encoding_InternalCodePage)

ICALL_TYPE(ILOCK, "System.Threading.Interlocked", ILOCK_1)
ICALL(ILOCK_1, "Add(int&,int)", ves_icall_System_Threading_Interlocked_Add_Int)
ICALL(ILOCK_2, "Add(long&,long)", ves_icall_System_Threading_Interlocked_Add_Long)
ICALL(ILOCK_3, "CompareExchange(T&,T,T)", ves_icall_System_Threading_Interlocked_CompareExchange_T)
ICALL(ILOCK_4, "CompareExchange(double&,double,double)", ves_icall_System_Threading_Interlocked_CompareExchange_Double)
ICALL(ILOCK_5, "CompareExchange(int&,int,int)", ves_icall_System_Threading_Interlocked_CompareExchange_Int)
ICALL(ILOCK_6, "CompareExchange(intptr&,intptr,intptr)", ves_icall_System_Threading_Interlocked_CompareExchange_IntPtr)
ICALL(ILOCK_7, "CompareExchange(long&,long,long)", ves_icall_System_Threading_Interlocked_CompareExchange_Long)
ICALL(ILOCK_8, "CompareExchange(object&,object,object)", ves_icall_System_Threading_Interlocked_CompareExchange_Object)
ICALL(ILOCK_9, "CompareExchange(single&,single,single)", ves_icall_System_Threading_Interlocked_CompareExchange_Single)
ICALL(ILOCK_10, "Decrement(int&)", ves_icall_System_Threading_Interlocked_Decrement_Int)
ICALL(ILOCK_11, "Decrement(long&)", ves_icall_System_Threading_Interlocked_Decrement_Long)
ICALL(ILOCK_12, "Exchange(T&,T)", ves_icall_System_Threading_Interlocked_Exchange_T)
ICALL(ILOCK_13, "Exchange(double&,double)", ves_icall_System_Threading_Interlocked_Exchange_Double)
ICALL(ILOCK_14, "Exchange(int&,int)", ves_icall_System_Threading_Interlocked_Exchange_Int)
ICALL(ILOCK_15, "Exchange(intptr&,intptr)", ves_icall_System_Threading_Interlocked_Exchange_IntPtr)
ICALL(ILOCK_16, "Exchange(long&,long)", ves_icall_System_Threading_Interlocked_Exchange_Long)
ICALL(ILOCK_17, "Exchange(object&,object)", ves_icall_System_Threading_Interlocked_Exchange_Object)
ICALL(ILOCK_18, "Exchange(single&,single)", ves_icall_System_Threading_Interlocked_Exchange_Single)
ICALL(ILOCK_19, "Increment(int&)", ves_icall_System_Threading_Interlocked_Increment_Int)
ICALL(ILOCK_20, "Increment(long&)", ves_icall_System_Threading_Interlocked_Increment_Long)
ICALL(ILOCK_21, "Read(long&)", ves_icall_System_Threading_Interlocked_Read_Long)

ICALL_TYPE(ITHREAD, "System.Threading.InternalThread", ITHREAD_1)
ICALL(ITHREAD_1, "Thread_free_internal", ves_icall_System_Threading_InternalThread_Thread_free_internal)

ICALL_TYPE(MONIT, "System.Threading.Monitor", MONIT_8)
ICALL(MONIT_8, "Enter", mono_monitor_enter)
ICALL(MONIT_1, "Exit", mono_monitor_exit)
ICALL(MONIT_2, "Monitor_pulse", ves_icall_System_Threading_Monitor_Monitor_pulse)
ICALL(MONIT_3, "Monitor_pulse_all", ves_icall_System_Threading_Monitor_Monitor_pulse_all)
ICALL(MONIT_4, "Monitor_test_owner", ves_icall_System_Threading_Monitor_Monitor_test_owner)
ICALL(MONIT_5, "Monitor_test_synchronised", ves_icall_System_Threading_Monitor_Monitor_test_synchronised)
ICALL(MONIT_6, "Monitor_try_enter", ves_icall_System_Threading_Monitor_Monitor_try_enter)
ICALL(MONIT_7, "Monitor_wait", ves_icall_System_Threading_Monitor_Monitor_wait)
ICALL(MONIT_9, "try_enter_with_atomic_var", ves_icall_System_Threading_Monitor_Monitor_try_enter_with_atomic_var)

ICALL_TYPE(MUTEX, "System.Threading.Mutex", MUTEX_1)
ICALL(MUTEX_1, "CreateMutex_internal(bool,string,bool&)", ves_icall_System_Threading_Mutex_CreateMutex_internal)
ICALL(MUTEX_2, "OpenMutex_internal(string,System.Security.AccessControl.MutexRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Mutex_OpenMutex_internal)
ICALL(MUTEX_3, "ReleaseMutex_internal(intptr)", ves_icall_System_Threading_Mutex_ReleaseMutex_internal)

ICALL_TYPE(NATIVEC, "System.Threading.NativeEventCalls", NATIVEC_1)
ICALL(NATIVEC_1, "CloseEvent_internal", ves_icall_System_Threading_Events_CloseEvent_internal)
ICALL(NATIVEC_2, "CreateEvent_internal(bool,bool,string,bool&)", ves_icall_System_Threading_Events_CreateEvent_internal)
ICALL(NATIVEC_3, "OpenEvent_internal(string,System.Security.AccessControl.EventWaitHandleRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Events_OpenEvent_internal)
ICALL(NATIVEC_4, "ResetEvent_internal",  ves_icall_System_Threading_Events_ResetEvent_internal)
ICALL(NATIVEC_5, "SetEvent_internal",    ves_icall_System_Threading_Events_SetEvent_internal)

ICALL_TYPE(SEMA, "System.Threading.Semaphore", SEMA_1)
ICALL(SEMA_1, "CreateSemaphore_internal(int,int,string,bool&)", ves_icall_System_Threading_Semaphore_CreateSemaphore_internal)
ICALL(SEMA_2, "OpenSemaphore_internal(string,System.Security.AccessControl.SemaphoreRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Semaphore_OpenSemaphore_internal)
ICALL(SEMA_3, "ReleaseSemaphore_internal(intptr,int,bool&)", ves_icall_System_Threading_Semaphore_ReleaseSemaphore_internal)

ICALL_TYPE(THREAD, "System.Threading.Thread", THREAD_1)
ICALL(THREAD_1, "Abort_internal(System.Threading.InternalThread,object)", ves_icall_System_Threading_Thread_Abort)
ICALL(THREAD_1aa, "AllocTlsData", mono_thread_alloc_tls)
ICALL(THREAD_1a, "ByteArrayToCurrentDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToCurrentDomain)
ICALL(THREAD_1b, "ByteArrayToRootDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToRootDomain)
ICALL(THREAD_2, "ClrState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_ClrState)
ICALL(THREAD_2a, "ConstructInternalThread", ves_icall_System_Threading_Thread_ConstructInternalThread)
ICALL(THREAD_3, "CurrentInternalThread_internal", mono_thread_internal_current)
ICALL(THREAD_3a, "DestroyTlsData", mono_thread_destroy_tls)
ICALL(THREAD_4, "FreeLocalSlotValues", mono_thread_free_local_slot_values)
ICALL(THREAD_55, "GetAbortExceptionState", ves_icall_System_Threading_Thread_GetAbortExceptionState)
ICALL(THREAD_7, "GetDomainID", ves_icall_System_Threading_Thread_GetDomainID)
ICALL(THREAD_8, "GetName_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetName_internal)
ICALL(THREAD_11, "GetState(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetState)
ICALL(THREAD_53, "Interrupt_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Interrupt_internal)
ICALL(THREAD_12, "Join_internal(System.Threading.InternalThread,int,intptr)", ves_icall_System_Threading_Thread_Join_internal)
ICALL(THREAD_13, "MemoryBarrier", ves_icall_System_Threading_Thread_MemoryBarrier)
ICALL(THREAD_14, "ResetAbort_internal()", ves_icall_System_Threading_Thread_ResetAbort)
ICALL(THREAD_15, "Resume_internal()", ves_icall_System_Threading_Thread_Resume)
ICALL(THREAD_18, "SetName_internal(System.Threading.InternalThread,string)", ves_icall_System_Threading_Thread_SetName_internal)
ICALL(THREAD_21, "SetState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_SetState)
ICALL(THREAD_22, "Sleep_internal", ves_icall_System_Threading_Thread_Sleep_internal)
ICALL(THREAD_54, "SpinWait_nop", ves_icall_System_Threading_Thread_SpinWait_nop)
ICALL(THREAD_23, "Suspend_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Suspend)
ICALL(THREAD_25, "Thread_internal", ves_icall_System_Threading_Thread_Thread_internal)
ICALL(THREAD_26, "VolatileRead(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_27, "VolatileRead(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(THREAD_28, "VolatileRead(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_29, "VolatileRead(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_30, "VolatileRead(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_31, "VolatileRead(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_32, "VolatileRead(object&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_33, "VolatileRead(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_34, "VolatileRead(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(THREAD_35, "VolatileRead(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_36, "VolatileRead(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_37, "VolatileRead(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_38, "VolatileRead(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_39, "VolatileWrite(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_40, "VolatileWrite(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(THREAD_41, "VolatileWrite(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_42, "VolatileWrite(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_43, "VolatileWrite(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_44, "VolatileWrite(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_45, "VolatileWrite(object&,object)", ves_icall_System_Threading_Thread_VolatileWriteObject)
ICALL(THREAD_46, "VolatileWrite(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_47, "VolatileWrite(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(THREAD_48, "VolatileWrite(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_49, "VolatileWrite(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_50, "VolatileWrite(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_51, "VolatileWrite(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_9, "Yield", ves_icall_System_Threading_Thread_Yield)
ICALL(THREAD_52, "current_lcid()", ves_icall_System_Threading_Thread_current_lcid)

ICALL_TYPE(THREADP, "System.Threading.ThreadPool", THREADP_1)
ICALL(THREADP_1, "GetAvailableThreads", ves_icall_System_Threading_ThreadPool_GetAvailableThreads)
ICALL(THREADP_2, "GetMaxThreads", ves_icall_System_Threading_ThreadPool_GetMaxThreads)
ICALL(THREADP_3, "GetMinThreads", ves_icall_System_Threading_ThreadPool_GetMinThreads)
ICALL(THREADP_35, "SetMaxThreads", ves_icall_System_Threading_ThreadPool_SetMaxThreads)
ICALL(THREADP_4, "SetMinThreads", ves_icall_System_Threading_ThreadPool_SetMinThreads)
ICALL(THREADP_5, "pool_queue", icall_append_job)

ICALL_TYPE(VOLATILE, "System.Threading.Volatile", VOLATILE_28)
ICALL(VOLATILE_28, "Read(T&)", ves_icall_System_Threading_Volatile_Read_T)
ICALL(VOLATILE_1, "Read(bool&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_2, "Read(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_3, "Read(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(VOLATILE_4, "Read(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_5, "Read(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_6, "Read(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_7, "Read(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_8, "Read(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_9, "Read(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(VOLATILE_10, "Read(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_11, "Read(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_12, "Read(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_13, "Read(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_27, "Write(T&,T)", ves_icall_System_Threading_Volatile_Write_T)
ICALL(VOLATILE_14, "Write(bool&,bool)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_15, "Write(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_16, "Write(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(VOLATILE_17, "Write(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_18, "Write(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_19, "Write(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_20, "Write(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(VOLATILE_21, "Write(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_22, "Write(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(VOLATILE_23, "Write(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_24, "Write(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_25, "Write(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_26, "Write(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)

ICALL_TYPE(WAITH, "System.Threading.WaitHandle", WAITH_1)
ICALL(WAITH_1, "SignalAndWait_Internal", ves_icall_System_Threading_WaitHandle_SignalAndWait_Internal)
ICALL(WAITH_2, "WaitAll_internal", ves_icall_System_Threading_WaitHandle_WaitAll_internal)
ICALL(WAITH_3, "WaitAny_internal", ves_icall_System_Threading_WaitHandle_WaitAny_internal)
ICALL(WAITH_4, "WaitOne_internal", ves_icall_System_Threading_WaitHandle_WaitOne_internal)

ICALL_TYPE(TYPE, "System.Type", TYPE_1)
ICALL(TYPE_1, "EqualsInternal", ves_icall_System_Type_EqualsInternal)
ICALL(TYPE_2, "GetGenericParameterAttributes", ves_icall_Type_GetGenericParameterAttributes)
ICALL(TYPE_3, "GetGenericParameterConstraints_impl", ves_icall_Type_GetGenericParameterConstraints)
ICALL(TYPE_4, "GetGenericParameterPosition", ves_icall_Type_GetGenericParameterPosition)
ICALL(TYPE_5, "GetGenericTypeDefinition_impl", ves_icall_Type_GetGenericTypeDefinition_impl)
ICALL(TYPE_6, "GetInterfaceMapData", ves_icall_Type_GetInterfaceMapData)
ICALL(TYPE_7, "GetPacking", ves_icall_Type_GetPacking)
ICALL(TYPE_8, "GetTypeCode", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_9, "GetTypeCodeInternal", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_10, "IsArrayImpl", ves_icall_Type_IsArrayImpl)
ICALL(TYPE_11, "IsInstanceOfType", ves_icall_type_IsInstanceOfType)
ICALL(TYPE_12, "MakeGenericType", ves_icall_Type_MakeGenericType)
ICALL(TYPE_13, "MakePointerType", ves_icall_Type_MakePointerType)
ICALL(TYPE_14, "get_IsGenericInstance", ves_icall_Type_get_IsGenericInstance)
ICALL(TYPE_15, "get_IsGenericType", ves_icall_Type_get_IsGenericType)
ICALL(TYPE_16, "get_IsGenericTypeDefinition", ves_icall_Type_get_IsGenericTypeDefinition)
ICALL(TYPE_17, "internal_from_handle", ves_icall_type_from_handle)
ICALL(TYPE_18, "internal_from_name", ves_icall_type_from_name)
ICALL(TYPE_19, "make_array_type", ves_icall_Type_make_array_type)
ICALL(TYPE_20, "make_byref_type", ves_icall_Type_make_byref_type)
ICALL(TYPE_21, "type_is_assignable_from", ves_icall_type_is_assignable_from)
ICALL(TYPE_22, "type_is_subtype_of", ves_icall_type_is_subtype_of)

ICALL_TYPE(TYPEDR, "System.TypedReference", TYPEDR_1)
ICALL(TYPEDR_1, "ToObject",	mono_TypedReference_ToObject)
ICALL(TYPEDR_2, "ToObjectInternal",	mono_TypedReference_ToObjectInternal)

ICALL_TYPE(VALUET, "System.ValueType", VALUET_1)
ICALL(VALUET_1, "InternalEquals", ves_icall_System_ValueType_Equals)
ICALL(VALUET_2, "InternalGetHashCode", ves_icall_System_ValueType_InternalGetHashCode)

ICALL_TYPE(WEBIC, "System.Web.Util.ICalls", WEBIC_1)
ICALL(WEBIC_1, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(WEBIC_2, "GetMachineInstallDirectory", ves_icall_System_Web_Util_ICalls_get_machine_install_dir)
ICALL(WEBIC_3, "GetUnmanagedResourcesPtr", ves_icall_get_resources_ptr)

#ifndef DISABLE_COM
ICALL_TYPE(COMOBJ, "System.__ComObject", COMOBJ_1)
ICALL(COMOBJ_1, "CreateRCW", ves_icall_System_ComObject_CreateRCW)
ICALL(COMOBJ_2, "GetInterfaceInternal", ves_icall_System_ComObject_GetInterfaceInternal)
ICALL(COMOBJ_3, "ReleaseInterfaces", ves_icall_System_ComObject_ReleaseInterfaces)
#endif
#undef ICALL
};
static const guint16 icall_names_idx [] = {
#define ICALL(id,name,func) [Icall_ ## id] = offsetof (struct msgstr_t, MSGSTRFIELD(__LINE__)),
// #include "metadata/icall-def.h"
[Icall_UNORM_1] = __builtin_offsetof (struct msgstr_t, str40),

[Icall_COMPROX_1] = __builtin_offsetof (struct msgstr_t, str44),
[Icall_COMPROX_2] = __builtin_offsetof (struct msgstr_t, str45),

[Icall_RUNTIME_1] = __builtin_offsetof (struct msgstr_t, str49),
[Icall_RUNTIME_12] = __builtin_offsetof (struct msgstr_t, str50),
[Icall_RUNTIME_13] = __builtin_offsetof (struct msgstr_t, str51),

[Icall_KPAIR_1] = __builtin_offsetof (struct msgstr_t, str55),
[Icall_KPAIR_2] = __builtin_offsetof (struct msgstr_t, str56),
[Icall_KPAIR_3] = __builtin_offsetof (struct msgstr_t, str57),
[Icall_KPAIR_4] = __builtin_offsetof (struct msgstr_t, str58),
[Icall_KPAIR_5] = __builtin_offsetof (struct msgstr_t, str59),

[Icall_ACTIV_1] = __builtin_offsetof (struct msgstr_t, str63),

[Icall_APPDOM_1] = __builtin_offsetof (struct msgstr_t, str66),
[Icall_APPDOM_2] = __builtin_offsetof (struct msgstr_t, str67),
[Icall_APPDOM_3] = __builtin_offsetof (struct msgstr_t, str68),
[Icall_APPDOM_4] = __builtin_offsetof (struct msgstr_t, str69),
[Icall_APPDOM_5] = __builtin_offsetof (struct msgstr_t, str70),
[Icall_APPDOM_6] = __builtin_offsetof (struct msgstr_t, str71),
[Icall_APPDOM_7] = __builtin_offsetof (struct msgstr_t, str72),
[Icall_APPDOM_8] = __builtin_offsetof (struct msgstr_t, str73),
[Icall_APPDOM_9] = __builtin_offsetof (struct msgstr_t, str74),
[Icall_APPDOM_10] = __builtin_offsetof (struct msgstr_t, str75),
[Icall_APPDOM_11] = __builtin_offsetof (struct msgstr_t, str76),
[Icall_APPDOM_12] = __builtin_offsetof (struct msgstr_t, str77),
[Icall_APPDOM_13] = __builtin_offsetof (struct msgstr_t, str78),
[Icall_APPDOM_14] = __builtin_offsetof (struct msgstr_t, str79),
[Icall_APPDOM_15] = __builtin_offsetof (struct msgstr_t, str80),
[Icall_APPDOM_16] = __builtin_offsetof (struct msgstr_t, str81),
[Icall_APPDOM_17] = __builtin_offsetof (struct msgstr_t, str82),
[Icall_APPDOM_18] = __builtin_offsetof (struct msgstr_t, str83),
[Icall_APPDOM_19] = __builtin_offsetof (struct msgstr_t, str84),
[Icall_APPDOM_20] = __builtin_offsetof (struct msgstr_t, str85),
[Icall_APPDOM_21] = __builtin_offsetof (struct msgstr_t, str86),
[Icall_APPDOM_22] = __builtin_offsetof (struct msgstr_t, str87),

[Icall_ARGI_1] = __builtin_offsetof (struct msgstr_t, str90),
[Icall_ARGI_2] = __builtin_offsetof (struct msgstr_t, str91),
[Icall_ARGI_3] = __builtin_offsetof (struct msgstr_t, str92),
[Icall_ARGI_4] = __builtin_offsetof (struct msgstr_t, str93),

[Icall_ARRAY_1] = __builtin_offsetof (struct msgstr_t, str96),
[Icall_ARRAY_2] = __builtin_offsetof (struct msgstr_t, str97),
[Icall_ARRAY_3] = __builtin_offsetof (struct msgstr_t, str98),
[Icall_ARRAY_14] = __builtin_offsetof (struct msgstr_t, str99),
[Icall_ARRAY_4] = __builtin_offsetof (struct msgstr_t, str100),
[Icall_ARRAY_5] = __builtin_offsetof (struct msgstr_t, str101),
[Icall_ARRAY_6] = __builtin_offsetof (struct msgstr_t, str102),
[Icall_ARRAY_15] = __builtin_offsetof (struct msgstr_t, str103),
[Icall_ARRAY_7] = __builtin_offsetof (struct msgstr_t, str104),
[Icall_ARRAY_8] = __builtin_offsetof (struct msgstr_t, str105),
[Icall_ARRAY_9] = __builtin_offsetof (struct msgstr_t, str106),
[Icall_ARRAY_10] = __builtin_offsetof (struct msgstr_t, str107),
[Icall_ARRAY_11] = __builtin_offsetof (struct msgstr_t, str108),
[Icall_ARRAY_12] = __builtin_offsetof (struct msgstr_t, str109),
[Icall_ARRAY_13] = __builtin_offsetof (struct msgstr_t, str110),

[Icall_BUFFER_1] = __builtin_offsetof (struct msgstr_t, str113),
[Icall_BUFFER_2] = __builtin_offsetof (struct msgstr_t, str114),
[Icall_BUFFER_3] = __builtin_offsetof (struct msgstr_t, str115),
[Icall_BUFFER_4] = __builtin_offsetof (struct msgstr_t, str116),

[Icall_CHAR_1] = __builtin_offsetof (struct msgstr_t, str119),

[Icall_COMPO_W_1] = __builtin_offsetof (struct msgstr_t, str122),

[Icall_DEFAULTC_1] = __builtin_offsetof (struct msgstr_t, str125),
[Icall_DEFAULTC_2] = __builtin_offsetof (struct msgstr_t, str126),

[Icall_INTCFGHOST_1] = __builtin_offsetof (struct msgstr_t, str130),
[Icall_INTCFGHOST_2] = __builtin_offsetof (struct msgstr_t, str131),

[Icall_CONSOLE_1] = __builtin_offsetof (struct msgstr_t, str134),
[Icall_CONSOLE_2] = __builtin_offsetof (struct msgstr_t, str135),
[Icall_CONSOLE_3] = __builtin_offsetof (struct msgstr_t, str136),
[Icall_CONSOLE_4] = __builtin_offsetof (struct msgstr_t, str137),
[Icall_CONSOLE_5] = __builtin_offsetof (struct msgstr_t, str138),

[Icall_CONVERT_1] = __builtin_offsetof (struct msgstr_t, str141),
[Icall_CONVERT_2] = __builtin_offsetof (struct msgstr_t, str142),

[Icall_TZONE_1] = __builtin_offsetof (struct msgstr_t, str145),

[Icall_DTIME_1] = __builtin_offsetof (struct msgstr_t, str148),
[Icall_DTIME_2] = __builtin_offsetof (struct msgstr_t, str149),

[Icall_DECIMAL_1] = __builtin_offsetof (struct msgstr_t, str153),
[Icall_DECIMAL_2] = __builtin_offsetof (struct msgstr_t, str154),
[Icall_DECIMAL_3] = __builtin_offsetof (struct msgstr_t, str155),
[Icall_DECIMAL_5] = __builtin_offsetof (struct msgstr_t, str157),
[Icall_DECIMAL_6] = __builtin_offsetof (struct msgstr_t, str158),
[Icall_DECIMAL_7] = __builtin_offsetof (struct msgstr_t, str159),
[Icall_DECIMAL_8] = __builtin_offsetof (struct msgstr_t, str160),
[Icall_DECIMAL_9] = __builtin_offsetof (struct msgstr_t, str161),
[Icall_DECIMAL_10] = __builtin_offsetof (struct msgstr_t, str162),
[Icall_DECIMAL_11] = __builtin_offsetof (struct msgstr_t, str163),
[Icall_DECIMAL_12] = __builtin_offsetof (struct msgstr_t, str164),
[Icall_DECIMAL_13] = __builtin_offsetof (struct msgstr_t, str165),
[Icall_DECIMAL_14] = __builtin_offsetof (struct msgstr_t, str166),

[Icall_DELEGATE_1] = __builtin_offsetof (struct msgstr_t, str170),
[Icall_DELEGATE_2] = __builtin_offsetof (struct msgstr_t, str171),

[Icall_DEBUGR_1] = __builtin_offsetof (struct msgstr_t, str174),
[Icall_DEBUGR_2] = __builtin_offsetof (struct msgstr_t, str175),
[Icall_DEBUGR_3] = __builtin_offsetof (struct msgstr_t, str176),

[Icall_TRACEL_1] = __builtin_offsetof (struct msgstr_t, str179),

[Icall_FILEV_1] = __builtin_offsetof (struct msgstr_t, str182),

[Icall_PERFCTR_1] = __builtin_offsetof (struct msgstr_t, str186),
[Icall_PERFCTR_2] = __builtin_offsetof (struct msgstr_t, str187),
[Icall_PERFCTR_3] = __builtin_offsetof (struct msgstr_t, str188),
[Icall_PERFCTR_4] = __builtin_offsetof (struct msgstr_t, str189),

[Icall_PERFCTRCAT_1] = __builtin_offsetof (struct msgstr_t, str192),
[Icall_PERFCTRCAT_2] = __builtin_offsetof (struct msgstr_t, str193),
[Icall_PERFCTRCAT_3] = __builtin_offsetof (struct msgstr_t, str194),
[Icall_PERFCTRCAT_4] = __builtin_offsetof (struct msgstr_t, str195),
[Icall_PERFCTRCAT_5] = __builtin_offsetof (struct msgstr_t, str196),
[Icall_PERFCTRCAT_6] = __builtin_offsetof (struct msgstr_t, str197),
[Icall_PERFCTRCAT_7] = __builtin_offsetof (struct msgstr_t, str198),
[Icall_PERFCTRCAT_8] = __builtin_offsetof (struct msgstr_t, str199),

[Icall_PROCESS_1] = __builtin_offsetof (struct msgstr_t, str202),
[Icall_PROCESS_2] = __builtin_offsetof (struct msgstr_t, str203),
[Icall_PROCESS_3] = __builtin_offsetof (struct msgstr_t, str204),
[Icall_PROCESS_4] = __builtin_offsetof (struct msgstr_t, str205),
[Icall_PROCESS_5] = __builtin_offsetof (struct msgstr_t, str206),
[Icall_PROCESS_5B] = __builtin_offsetof (struct msgstr_t, str207),
[Icall_PROCESS_5H] = __builtin_offsetof (struct msgstr_t, str208),
[Icall_PROCESS_6] = __builtin_offsetof (struct msgstr_t, str209),
[Icall_PROCESS_7] = __builtin_offsetof (struct msgstr_t, str210),
[Icall_PROCESS_8] = __builtin_offsetof (struct msgstr_t, str211),
[Icall_PROCESS_9] = __builtin_offsetof (struct msgstr_t, str212),
[Icall_PROCESS_10] = __builtin_offsetof (struct msgstr_t, str213),
[Icall_PROCESS_11] = __builtin_offsetof (struct msgstr_t, str214),
[Icall_PROCESS_11B] = __builtin_offsetof (struct msgstr_t, str215),
[Icall_PROCESS_12] = __builtin_offsetof (struct msgstr_t, str216),
[Icall_PROCESS_13] = __builtin_offsetof (struct msgstr_t, str217),
[Icall_PROCESS_14] = __builtin_offsetof (struct msgstr_t, str218),
[Icall_PROCESS_14M] = __builtin_offsetof (struct msgstr_t, str219),
[Icall_PROCESS_15] = __builtin_offsetof (struct msgstr_t, str220),
[Icall_PROCESS_16] = __builtin_offsetof (struct msgstr_t, str221),

[Icall_PROCESSHANDLE_1] = __builtin_offsetof (struct msgstr_t, str224),
[Icall_PROCESSHANDLE_2] = __builtin_offsetof (struct msgstr_t, str225),

[Icall_STOPWATCH_1] = __builtin_offsetof (struct msgstr_t, str229),

[Icall_DOUBLE_1] = __builtin_offsetof (struct msgstr_t, str232),

[Icall_ENUM_1] = __builtin_offsetof (struct msgstr_t, str235),
[Icall_ENUM_5] = __builtin_offsetof (struct msgstr_t, str236),
[Icall_ENUM_4] = __builtin_offsetof (struct msgstr_t, str237),
[Icall_ENUM_3] = __builtin_offsetof (struct msgstr_t, str238),
[Icall_ENUM_2] = __builtin_offsetof (struct msgstr_t, str239),

[Icall_ENV_1] = __builtin_offsetof (struct msgstr_t, str242),
[Icall_ENV_2] = __builtin_offsetof (struct msgstr_t, str243),
[Icall_ENV_3] = __builtin_offsetof (struct msgstr_t, str244),
[Icall_ENV_4] = __builtin_offsetof (struct msgstr_t, str245),
[Icall_ENV_5] = __builtin_offsetof (struct msgstr_t, str246),
[Icall_ENV_51] = __builtin_offsetof (struct msgstr_t, str247),
[Icall_ENV_6] = __builtin_offsetof (struct msgstr_t, str248),
[Icall_ENV_6a] = __builtin_offsetof (struct msgstr_t, str249),
[Icall_ENV_7] = __builtin_offsetof (struct msgstr_t, str250),
[Icall_ENV_8] = __builtin_offsetof (struct msgstr_t, str251),
[Icall_ENV_9] = __builtin_offsetof (struct msgstr_t, str252),
[Icall_ENV_10] = __builtin_offsetof (struct msgstr_t, str253),
[Icall_ENV_11] = __builtin_offsetof (struct msgstr_t, str254),
[Icall_ENV_13] = __builtin_offsetof (struct msgstr_t, str255),
[Icall_ENV_14] = __builtin_offsetof (struct msgstr_t, str256),
[Icall_ENV_15] = __builtin_offsetof (struct msgstr_t, str257),
[Icall_ENV_16] = __builtin_offsetof (struct msgstr_t, str258),
[Icall_ENV_16m] = __builtin_offsetof (struct msgstr_t, str259),
[Icall_ENV_17] = __builtin_offsetof (struct msgstr_t, str260),
[Icall_ENV_18] = __builtin_offsetof (struct msgstr_t, str261),
[Icall_ENV_19] = __builtin_offsetof (struct msgstr_t, str262),
[Icall_ENV_20] = __builtin_offsetof (struct msgstr_t, str263),

[Icall_GC_0] = __builtin_offsetof (struct msgstr_t, str266),
[Icall_GC_0a] = __builtin_offsetof (struct msgstr_t, str267),
[Icall_GC_1] = __builtin_offsetof (struct msgstr_t, str268),
[Icall_GC_2] = __builtin_offsetof (struct msgstr_t, str269),
[Icall_GC_3] = __builtin_offsetof (struct msgstr_t, str270),
[Icall_GC_4] = __builtin_offsetof (struct msgstr_t, str271),
[Icall_GC_4a] = __builtin_offsetof (struct msgstr_t, str272),
[Icall_GC_5] = __builtin_offsetof (struct msgstr_t, str273),
[Icall_GC_6] = __builtin_offsetof (struct msgstr_t, str274),
[Icall_GC_7] = __builtin_offsetof (struct msgstr_t, str275),
[Icall_GC_9] = __builtin_offsetof (struct msgstr_t, str276),
[Icall_GC_8] = __builtin_offsetof (struct msgstr_t, str277),

[Icall_COMPINF_1] = __builtin_offsetof (struct msgstr_t, str280),
[Icall_COMPINF_2] = __builtin_offsetof (struct msgstr_t, str281),
[Icall_COMPINF_3] = __builtin_offsetof (struct msgstr_t, str282),
[Icall_COMPINF_4] = __builtin_offsetof (struct msgstr_t, str283),
[Icall_COMPINF_5] = __builtin_offsetof (struct msgstr_t, str284),
[Icall_COMPINF_6] = __builtin_offsetof (struct msgstr_t, str285),

[Icall_CULINF_2] = __builtin_offsetof (struct msgstr_t, str288),
[Icall_CULINF_4] = __builtin_offsetof (struct msgstr_t, str289),
[Icall_CULINF_5] = __builtin_offsetof (struct msgstr_t, str290),
[Icall_CULINF_6] = __builtin_offsetof (struct msgstr_t, str291),
[Icall_CULINF_7] = __builtin_offsetof (struct msgstr_t, str292),
[Icall_CULINF_8] = __builtin_offsetof (struct msgstr_t, str293),
[Icall_CULINF_9] = __builtin_offsetof (struct msgstr_t, str294),

[Icall_REGINF_1] = __builtin_offsetof (struct msgstr_t, str298),
[Icall_REGINF_2] = __builtin_offsetof (struct msgstr_t, str299),

[Icall_IODRIVEINFO_1] = __builtin_offsetof (struct msgstr_t, str303),
[Icall_IODRIVEINFO_2] = __builtin_offsetof (struct msgstr_t, str304),
[Icall_IODRIVEINFO_3] = __builtin_offsetof (struct msgstr_t, str305),

[Icall_FAMW_1] = __builtin_offsetof (struct msgstr_t, str309),

[Icall_FILEW_4] = __builtin_offsetof (struct msgstr_t, str312),

[Icall_INOW_1] = __builtin_offsetof (struct msgstr_t, str315),
[Icall_INOW_2] = __builtin_offsetof (struct msgstr_t, str316),
[Icall_INOW_3] = __builtin_offsetof (struct msgstr_t, str317),

[Icall_MONOIO_1] = __builtin_offsetof (struct msgstr_t, str327),
[Icall_MONOIO_2] = __builtin_offsetof (struct msgstr_t, str329),
[Icall_MONOIO_3] = __builtin_offsetof (struct msgstr_t, str330),
[Icall_MONOIO_4] = __builtin_offsetof (struct msgstr_t, str331),
[Icall_MONOIO_5] = __builtin_offsetof (struct msgstr_t, str332),
[Icall_MONOIO_34] = __builtin_offsetof (struct msgstr_t, str334),
[Icall_MONOIO_37] = __builtin_offsetof (struct msgstr_t, str335),
[Icall_MONOIO_35] = __builtin_offsetof (struct msgstr_t, str336),
[Icall_MONOIO_36] = __builtin_offsetof (struct msgstr_t, str337),
[Icall_MONOIO_6] = __builtin_offsetof (struct msgstr_t, str338),
[Icall_MONOIO_7] = __builtin_offsetof (struct msgstr_t, str339),
[Icall_MONOIO_8] = __builtin_offsetof (struct msgstr_t, str340),
[Icall_MONOIO_9] = __builtin_offsetof (struct msgstr_t, str341),
[Icall_MONOIO_10] = __builtin_offsetof (struct msgstr_t, str342),
[Icall_MONOIO_11] = __builtin_offsetof (struct msgstr_t, str343),
[Icall_MONOIO_12] = __builtin_offsetof (struct msgstr_t, str344),
[Icall_MONOIO_13] = __builtin_offsetof (struct msgstr_t, str346),
[Icall_MONOIO_14] = __builtin_offsetof (struct msgstr_t, str347),
[Icall_MONOIO_15] = __builtin_offsetof (struct msgstr_t, str348),
[Icall_MONOIO_16] = __builtin_offsetof (struct msgstr_t, str350),
[Icall_MONOIO_17] = __builtin_offsetof (struct msgstr_t, str351),
[Icall_MONOIO_18] = __builtin_offsetof (struct msgstr_t, str353),
[Icall_MONOIO_18M] = __builtin_offsetof (struct msgstr_t, str354),
[Icall_MONOIO_19] = __builtin_offsetof (struct msgstr_t, str356),
[Icall_MONOIO_20] = __builtin_offsetof (struct msgstr_t, str357),
[Icall_MONOIO_21] = __builtin_offsetof (struct msgstr_t, str358),
[Icall_MONOIO_22] = __builtin_offsetof (struct msgstr_t, str359),
[Icall_MONOIO_23] = __builtin_offsetof (struct msgstr_t, str360),
[Icall_MONOIO_24] = __builtin_offsetof (struct msgstr_t, str362),
[Icall_MONOIO_25] = __builtin_offsetof (struct msgstr_t, str364),
[Icall_MONOIO_26] = __builtin_offsetof (struct msgstr_t, str365),
[Icall_MONOIO_27] = __builtin_offsetof (struct msgstr_t, str366),
[Icall_MONOIO_28] = __builtin_offsetof (struct msgstr_t, str367),
[Icall_MONOIO_29] = __builtin_offsetof (struct msgstr_t, str368),
[Icall_MONOIO_30] = __builtin_offsetof (struct msgstr_t, str369),
[Icall_MONOIO_31] = __builtin_offsetof (struct msgstr_t, str370),
[Icall_MONOIO_32] = __builtin_offsetof (struct msgstr_t, str371),
[Icall_MONOIO_33] = __builtin_offsetof (struct msgstr_t, str372),

[Icall_IOPATH_1] = __builtin_offsetof (struct msgstr_t, str375),

[Icall_MATH_1] = __builtin_offsetof (struct msgstr_t, str378),
[Icall_MATH_2] = __builtin_offsetof (struct msgstr_t, str379),
[Icall_MATH_3] = __builtin_offsetof (struct msgstr_t, str380),
[Icall_MATH_4] = __builtin_offsetof (struct msgstr_t, str381),
[Icall_MATH_5] = __builtin_offsetof (struct msgstr_t, str382),
[Icall_MATH_6] = __builtin_offsetof (struct msgstr_t, str383),
[Icall_MATH_7] = __builtin_offsetof (struct msgstr_t, str384),
[Icall_MATH_8] = __builtin_offsetof (struct msgstr_t, str385),
[Icall_MATH_9] = __builtin_offsetof (struct msgstr_t, str386),
[Icall_MATH_10] = __builtin_offsetof (struct msgstr_t, str387),
[Icall_MATH_11] = __builtin_offsetof (struct msgstr_t, str388),
[Icall_MATH_12] = __builtin_offsetof (struct msgstr_t, str389),
[Icall_MATH_13] = __builtin_offsetof (struct msgstr_t, str390),
[Icall_MATH_14] = __builtin_offsetof (struct msgstr_t, str391),
[Icall_MATH_15] = __builtin_offsetof (struct msgstr_t, str392),
[Icall_MATH_16] = __builtin_offsetof (struct msgstr_t, str393),
[Icall_MATH_17] = __builtin_offsetof (struct msgstr_t, str394),
[Icall_MATH_18] = __builtin_offsetof (struct msgstr_t, str395),

[Icall_MCATTR_1] = __builtin_offsetof (struct msgstr_t, str398),
[Icall_MCATTR_2] = __builtin_offsetof (struct msgstr_t, str399),
[Icall_MCATTR_3] = __builtin_offsetof (struct msgstr_t, str400),

[Icall_MENUM_1] = __builtin_offsetof (struct msgstr_t, str403),

[Icall_MTYPE_1] = __builtin_offsetof (struct msgstr_t, str406),
[Icall_MTYPE_2] = __builtin_offsetof (struct msgstr_t, str407),
[Icall_MTYPE_3] = __builtin_offsetof (struct msgstr_t, str408),
[Icall_MTYPE_4] = __builtin_offsetof (struct msgstr_t, str409),
[Icall_MTYPE_5] = __builtin_offsetof (struct msgstr_t, str410),
[Icall_MTYPE_6] = __builtin_offsetof (struct msgstr_t, str411),
[Icall_MTYPE_7] = __builtin_offsetof (struct msgstr_t, str412),
[Icall_MTYPE_8] = __builtin_offsetof (struct msgstr_t, str413),
[Icall_MTYPE_9] = __builtin_offsetof (struct msgstr_t, str414),
[Icall_MTYPE_10] = __builtin_offsetof (struct msgstr_t, str415),
[Icall_MTYPE_11] = __builtin_offsetof (struct msgstr_t, str416),
[Icall_MTYPE_12] = __builtin_offsetof (struct msgstr_t, str417),
[Icall_MTYPE_13] = __builtin_offsetof (struct msgstr_t, str418),
[Icall_MTYPE_14] = __builtin_offsetof (struct msgstr_t, str419),
[Icall_MTYPE_15] = __builtin_offsetof (struct msgstr_t, str420),
[Icall_MTYPE_16] = __builtin_offsetof (struct msgstr_t, str421),
[Icall_MTYPE_17] = __builtin_offsetof (struct msgstr_t, str422),
[Icall_MTYPE_18] = __builtin_offsetof (struct msgstr_t, str423),
[Icall_MTYPE_19] = __builtin_offsetof (struct msgstr_t, str424),
[Icall_MTYPE_20] = __builtin_offsetof (struct msgstr_t, str425),
[Icall_MTYPE_21] = __builtin_offsetof (struct msgstr_t, str426),
[Icall_MTYPE_22] = __builtin_offsetof (struct msgstr_t, str427),
[Icall_MTYPE_23] = __builtin_offsetof (struct msgstr_t, str428),
[Icall_MTYPE_24] = __builtin_offsetof (struct msgstr_t, str429),
[Icall_MTYPE_25] = __builtin_offsetof (struct msgstr_t, str430),
[Icall_MTYPE_26] = __builtin_offsetof (struct msgstr_t, str431),
[Icall_MTYPE_27] = __builtin_offsetof (struct msgstr_t, str432),
[Icall_MTYPE_28] = __builtin_offsetof (struct msgstr_t, str433),
[Icall_MTYPE_29] = __builtin_offsetof (struct msgstr_t, str434),
[Icall_MTYPE_31] = __builtin_offsetof (struct msgstr_t, str435),
[Icall_MTYPE_33] = __builtin_offsetof (struct msgstr_t, str436),
[Icall_MTYPE_32] = __builtin_offsetof (struct msgstr_t, str437),

[Icall_NDNS_1] = __builtin_offsetof (struct msgstr_t, str441),
[Icall_NDNS_2] = __builtin_offsetof (struct msgstr_t, str442),
[Icall_NDNS_3] = __builtin_offsetof (struct msgstr_t, str443),

[Icall_SOCK_1] = __builtin_offsetof (struct msgstr_t, str446),
[Icall_SOCK_2] = __builtin_offsetof (struct msgstr_t, str447),
[Icall_SOCK_3] = __builtin_offsetof (struct msgstr_t, str448),
[Icall_SOCK_4] = __builtin_offsetof (struct msgstr_t, str449),
[Icall_SOCK_5] = __builtin_offsetof (struct msgstr_t, str450),
[Icall_SOCK_6] = __builtin_offsetof (struct msgstr_t, str451),
[Icall_SOCK_6a] = __builtin_offsetof (struct msgstr_t, str452),
[Icall_SOCK_7] = __builtin_offsetof (struct msgstr_t, str453),
[Icall_SOCK_8] = __builtin_offsetof (struct msgstr_t, str454),
[Icall_SOCK_9] = __builtin_offsetof (struct msgstr_t, str455),
[Icall_SOCK_10] = __builtin_offsetof (struct msgstr_t, str456),
[Icall_SOCK_11] = __builtin_offsetof (struct msgstr_t, str457),
[Icall_SOCK_11a] = __builtin_offsetof (struct msgstr_t, str458),
[Icall_SOCK_12] = __builtin_offsetof (struct msgstr_t, str459),
[Icall_SOCK_13] = __builtin_offsetof (struct msgstr_t, str460),
[Icall_SOCK_14] = __builtin_offsetof (struct msgstr_t, str461),
[Icall_SOCK_15] = __builtin_offsetof (struct msgstr_t, str462),
[Icall_SOCK_15a] = __builtin_offsetof (struct msgstr_t, str463),
[Icall_SOCK_16] = __builtin_offsetof (struct msgstr_t, str464),
[Icall_SOCK_16a] = __builtin_offsetof (struct msgstr_t, str465),
[Icall_SOCK_17] = __builtin_offsetof (struct msgstr_t, str466),
[Icall_SOCK_18] = __builtin_offsetof (struct msgstr_t, str467),
[Icall_SOCK_19] = __builtin_offsetof (struct msgstr_t, str468),
[Icall_SOCK_20] = __builtin_offsetof (struct msgstr_t, str469),
[Icall_SOCK_21] = __builtin_offsetof (struct msgstr_t, str470),
[Icall_SOCK_21a] = __builtin_offsetof (struct msgstr_t, str471),
[Icall_SOCK_22] = __builtin_offsetof (struct msgstr_t, str472),

[Icall_SOCKEX_1] = __builtin_offsetof (struct msgstr_t, str475),

[Icall_NUMBER_FORMATTER_1] = __builtin_offsetof (struct msgstr_t, str479),

[Icall_OBJ_1] = __builtin_offsetof (struct msgstr_t, str482),
[Icall_OBJ_2] = __builtin_offsetof (struct msgstr_t, str483),
[Icall_OBJ_3] = __builtin_offsetof (struct msgstr_t, str484),
[Icall_OBJ_4] = __builtin_offsetof (struct msgstr_t, str485),

[Icall_ASSEM_1] = __builtin_offsetof (struct msgstr_t, str488),
[Icall_ASSEM_2] = __builtin_offsetof (struct msgstr_t, str489),
[Icall_ASSEM_3] = __builtin_offsetof (struct msgstr_t, str490),
[Icall_ASSEM_4] = __builtin_offsetof (struct msgstr_t, str491),
[Icall_ASSEM_5] = __builtin_offsetof (struct msgstr_t, str492),
[Icall_ASSEM_6] = __builtin_offsetof (struct msgstr_t, str493),
[Icall_ASSEM_7] = __builtin_offsetof (struct msgstr_t, str494),
[Icall_ASSEM_8] = __builtin_offsetof (struct msgstr_t, str495),
[Icall_ASSEM_9] = __builtin_offsetof (struct msgstr_t, str496),
[Icall_ASSEM_10] = __builtin_offsetof (struct msgstr_t, str497),
[Icall_ASSEM_12] = __builtin_offsetof (struct msgstr_t, str499),
[Icall_ASSEM_13] = __builtin_offsetof (struct msgstr_t, str500),
[Icall_ASSEM_14] = __builtin_offsetof (struct msgstr_t, str501),
[Icall_ASSEM_15] = __builtin_offsetof (struct msgstr_t, str502),
[Icall_ASSEM_16] = __builtin_offsetof (struct msgstr_t, str503),
[Icall_ASSEM_17] = __builtin_offsetof (struct msgstr_t, str504),
[Icall_ASSEM_18] = __builtin_offsetof (struct msgstr_t, str505),
[Icall_ASSEM_19] = __builtin_offsetof (struct msgstr_t, str509),
[Icall_ASSEM_20] = __builtin_offsetof (struct msgstr_t, str512),
[Icall_ASSEM_21] = __builtin_offsetof (struct msgstr_t, str513),
[Icall_ASSEM_22] = __builtin_offsetof (struct msgstr_t, str514),
[Icall_ASSEM_23] = __builtin_offsetof (struct msgstr_t, str515),
[Icall_ASSEM_24] = __builtin_offsetof (struct msgstr_t, str516),
[Icall_ASSEM_25] = __builtin_offsetof (struct msgstr_t, str517),
[Icall_ASSEM_26] = __builtin_offsetof (struct msgstr_t, str518),

[Icall_ASSEMN_1] = __builtin_offsetof (struct msgstr_t, str521),
[Icall_ASSEMN_2] = __builtin_offsetof (struct msgstr_t, str522),

[Icall_CATTR_DATA_1] = __builtin_offsetof (struct msgstr_t, str525),

[Icall_ASSEMB_1] = __builtin_offsetof (struct msgstr_t, str528),
[Icall_ASSEMB_2] = __builtin_offsetof (struct msgstr_t, str529),

[Icall_CATTRB_1] = __builtin_offsetof (struct msgstr_t, str532),

[Icall_DERIVEDTYPE_1] = __builtin_offsetof (struct msgstr_t, str536),

[Icall_DYNM_1] = __builtin_offsetof (struct msgstr_t, str540),

[Icall_ENUMB_1] = __builtin_offsetof (struct msgstr_t, str543),

[Icall_GPARB_1] = __builtin_offsetof (struct msgstr_t, str546),

[Icall_METHODB_1] = __builtin_offsetof (struct msgstr_t, str549),

[Icall_MODULEB_8] = __builtin_offsetof (struct msgstr_t, str552),
[Icall_MODULEB_1] = __builtin_offsetof (struct msgstr_t, str553),
[Icall_MODULEB_2] = __builtin_offsetof (struct msgstr_t, str554),
[Icall_MODULEB_3] = __builtin_offsetof (struct msgstr_t, str555),
[Icall_MODULEB_4] = __builtin_offsetof (struct msgstr_t, str556),
[Icall_MODULEB_5] = __builtin_offsetof (struct msgstr_t, str557),
[Icall_MODULEB_6] = __builtin_offsetof (struct msgstr_t, str558),
[Icall_MODULEB_7] = __builtin_offsetof (struct msgstr_t, str559),
[Icall_MODULEB_9] = __builtin_offsetof (struct msgstr_t, str560),

[Icall_SIGH_1] = __builtin_offsetof (struct msgstr_t, str563),
[Icall_SIGH_2] = __builtin_offsetof (struct msgstr_t, str564),

[Icall_TYPEB_1] = __builtin_offsetof (struct msgstr_t, str567),
[Icall_TYPEB_2] = __builtin_offsetof (struct msgstr_t, str568),
[Icall_TYPEB_3] = __builtin_offsetof (struct msgstr_t, str569),
[Icall_TYPEB_4] = __builtin_offsetof (struct msgstr_t, str570),
[Icall_TYPEB_5] = __builtin_offsetof (struct msgstr_t, str571),
[Icall_TYPEB_6] = __builtin_offsetof (struct msgstr_t, str572),
[Icall_TYPEB_7] = __builtin_offsetof (struct msgstr_t, str573),

[Icall_FILEDI_1] = __builtin_offsetof (struct msgstr_t, str576),
[Icall_FILEDI_2] = __builtin_offsetof (struct msgstr_t, str577),
[Icall_FILEDI_3] = __builtin_offsetof (struct msgstr_t, str578),

[Icall_MEMBERI_1] = __builtin_offsetof (struct msgstr_t, str581),

[Icall_MBASE_1] = __builtin_offsetof (struct msgstr_t, str584),
[Icall_MBASE_2] = __builtin_offsetof (struct msgstr_t, str585),
[Icall_MBASE_3] = __builtin_offsetof (struct msgstr_t, str586),
[Icall_MBASE_4] = __builtin_offsetof (struct msgstr_t, str587),

[Icall_MODULE_1] = __builtin_offsetof (struct msgstr_t, str590),
[Icall_MODULE_2] = __builtin_offsetof (struct msgstr_t, str591),
[Icall_MODULE_3] = __builtin_offsetof (struct msgstr_t, str592),
[Icall_MODULE_14] = __builtin_offsetof (struct msgstr_t, str593),
[Icall_MODULE_4] = __builtin_offsetof (struct msgstr_t, str594),
[Icall_MODULE_5] = __builtin_offsetof (struct msgstr_t, str595),
[Icall_MODULE_6] = __builtin_offsetof (struct msgstr_t, str596),
[Icall_MODULE_7] = __builtin_offsetof (struct msgstr_t, str597),
[Icall_MODULE_8] = __builtin_offsetof (struct msgstr_t, str598),
[Icall_MODULE_9] = __builtin_offsetof (struct msgstr_t, str599),
[Icall_MODULE_10] = __builtin_offsetof (struct msgstr_t, str600),
[Icall_MODULE_11] = __builtin_offsetof (struct msgstr_t, str601),
[Icall_MODULE_12] = __builtin_offsetof (struct msgstr_t, str602),
[Icall_MODULE_13] = __builtin_offsetof (struct msgstr_t, str603),

[Icall_MCMETH_1] = __builtin_offsetof (struct msgstr_t, str606),
[Icall_MCMETH_2] = __builtin_offsetof (struct msgstr_t, str607),

[Icall_MEVIN_1] = __builtin_offsetof (struct msgstr_t, str610),

[Icall_MFIELD_1] = __builtin_offsetof (struct msgstr_t, str613),
[Icall_MFIELD_2] = __builtin_offsetof (struct msgstr_t, str614),
[Icall_MFIELD_5] = __builtin_offsetof (struct msgstr_t, str615),
[Icall_MFIELD_3] = __builtin_offsetof (struct msgstr_t, str616),
[Icall_MFIELD_6] = __builtin_offsetof (struct msgstr_t, str617),
[Icall_MFIELD_4] = __builtin_offsetof (struct msgstr_t, str618),

[Icall_MGENCM_1] = __builtin_offsetof (struct msgstr_t, str621),

[Icall_MGENCL_5] = __builtin_offsetof (struct msgstr_t, str624),
[Icall_MGENCL_6] = __builtin_offsetof (struct msgstr_t, str625),

[Icall_MGENM_1] = __builtin_offsetof (struct msgstr_t, str629),

[Icall_MMETH_1] = __builtin_offsetof (struct msgstr_t, str632),
[Icall_MMETH_2] = __builtin_offsetof (struct msgstr_t, str633),
[Icall_MMETH_3] = __builtin_offsetof (struct msgstr_t, str634),
[Icall_MMETH_4] = __builtin_offsetof (struct msgstr_t, str635),
[Icall_MMETH_5] = __builtin_offsetof (struct msgstr_t, str636),
[Icall_MMETH_6] = __builtin_offsetof (struct msgstr_t, str637),
[Icall_MMETH_7] = __builtin_offsetof (struct msgstr_t, str638),
[Icall_MMETH_8] = __builtin_offsetof (struct msgstr_t, str639),
[Icall_MMETH_9] = __builtin_offsetof (struct msgstr_t, str640),

[Icall_MMETHI_4] = __builtin_offsetof (struct msgstr_t, str643),
[Icall_MMETHI_1] = __builtin_offsetof (struct msgstr_t, str644),
[Icall_MMETHI_2] = __builtin_offsetof (struct msgstr_t, str645),
[Icall_MMETHI_3] = __builtin_offsetof (struct msgstr_t, str646),

[Icall_MPROPI_1] = __builtin_offsetof (struct msgstr_t, str649),
[Icall_MPROPI_3] = __builtin_offsetof (struct msgstr_t, str650),
[Icall_MPROPI_2] = __builtin_offsetof (struct msgstr_t, str651),

[Icall_PARAMI_1] = __builtin_offsetof (struct msgstr_t, str654),
[Icall_PARAMI_2] = __builtin_offsetof (struct msgstr_t, str655),

[Icall_RUNH_1] = __builtin_offsetof (struct msgstr_t, str658),
[Icall_RUNH_2] = __builtin_offsetof (struct msgstr_t, str660),
[Icall_RUNH_3] = __builtin_offsetof (struct msgstr_t, str661),
[Icall_RUNH_4] = __builtin_offsetof (struct msgstr_t, str662),
[Icall_RUNH_5] = __builtin_offsetof (struct msgstr_t, str663),
[Icall_RUNH_5h] = __builtin_offsetof (struct msgstr_t, str664),
[Icall_RUNH_6] = __builtin_offsetof (struct msgstr_t, str665),

[Icall_GCH_1] = __builtin_offsetof (struct msgstr_t, str668),
[Icall_GCH_2] = __builtin_offsetof (struct msgstr_t, str669),
[Icall_GCH_3] = __builtin_offsetof (struct msgstr_t, str670),
[Icall_GCH_4] = __builtin_offsetof (struct msgstr_t, str671),
[Icall_GCH_5] = __builtin_offsetof (struct msgstr_t, str672),

[Icall_MARSHAL_1] = __builtin_offsetof (struct msgstr_t, str676),
[Icall_MARSHAL_2] = __builtin_offsetof (struct msgstr_t, str680),
[Icall_MARSHAL_3] = __builtin_offsetof (struct msgstr_t, str681),
[Icall_MARSHAL_4] = __builtin_offsetof (struct msgstr_t, str682),
[Icall_MARSHAL_5] = __builtin_offsetof (struct msgstr_t, str683),
[Icall_MARSHAL_6] = __builtin_offsetof (struct msgstr_t, str684),
[Icall_MARSHAL_7] = __builtin_offsetof (struct msgstr_t, str685),
[Icall_MARSHAL_44] = __builtin_offsetof (struct msgstr_t, str687),
[Icall_MARSHAL_8] = __builtin_offsetof (struct msgstr_t, str688),
[Icall_MARSHAL_9] = __builtin_offsetof (struct msgstr_t, str690),
[Icall_MARSHAL_10] = __builtin_offsetof (struct msgstr_t, str691),
[Icall_MARSHAL_45] = __builtin_offsetof (struct msgstr_t, str693),
[Icall_MARSHAL_46] = __builtin_offsetof (struct msgstr_t, str694),
[Icall_MARSHAL_11] = __builtin_offsetof (struct msgstr_t, str696),
[Icall_MARSHAL_47] = __builtin_offsetof (struct msgstr_t, str698),
[Icall_MARSHAL_48] = __builtin_offsetof (struct msgstr_t, str699),
[Icall_MARSHAL_12] = __builtin_offsetof (struct msgstr_t, str701),
[Icall_MARSHAL_13] = __builtin_offsetof (struct msgstr_t, str702),
[Icall_MARSHAL_14] = __builtin_offsetof (struct msgstr_t, str703),
[Icall_MARSHAL_15] = __builtin_offsetof (struct msgstr_t, str704),
[Icall_MARSHAL_16] = __builtin_offsetof (struct msgstr_t, str705),
[Icall_MARSHAL_17] = __builtin_offsetof (struct msgstr_t, str707),
[Icall_MARSHAL_18] = __builtin_offsetof (struct msgstr_t, str709),
[Icall_MARSHAL_19] = __builtin_offsetof (struct msgstr_t, str710),
[Icall_MARSHAL_20] = __builtin_offsetof (struct msgstr_t, str711),
[Icall_MARSHAL_21] = __builtin_offsetof (struct msgstr_t, str712),
[Icall_MARSHAL_22] = __builtin_offsetof (struct msgstr_t, str714),
[Icall_MARSHAL_43] = __builtin_offsetof (struct msgstr_t, str716),
[Icall_MARSHAL_23] = __builtin_offsetof (struct msgstr_t, str717),
[Icall_MARSHAL_49] = __builtin_offsetof (struct msgstr_t, str719),
[Icall_MARSHAL_29] = __builtin_offsetof (struct msgstr_t, str720),
[Icall_MARSHAL_30] = __builtin_offsetof (struct msgstr_t, str722),
[Icall_MARSHAL_31] = __builtin_offsetof (struct msgstr_t, str723),
[Icall_MARSHAL_32] = __builtin_offsetof (struct msgstr_t, str724),
[Icall_MARSHAL_33] = __builtin_offsetof (struct msgstr_t, str725),
[Icall_MARSHAL_34] = __builtin_offsetof (struct msgstr_t, str726),
[Icall_MARSHAL_35] = __builtin_offsetof (struct msgstr_t, str727),
[Icall_MARSHAL_41] = __builtin_offsetof (struct msgstr_t, str728),
[Icall_MARSHAL_42] = __builtin_offsetof (struct msgstr_t, str729),

[Icall_ACTS_1] = __builtin_offsetof (struct msgstr_t, str732),
[Icall_ACTS_2] = __builtin_offsetof (struct msgstr_t, str733),

[Icall_MONOMM_1] = __builtin_offsetof (struct msgstr_t, str736),

[Icall_REALP_1] = __builtin_offsetof (struct msgstr_t, str740),
[Icall_REALP_2] = __builtin_offsetof (struct msgstr_t, str741),

[Icall_REMSER_0] = __builtin_offsetof (struct msgstr_t, str744),
[Icall_REMSER_1] = __builtin_offsetof (struct msgstr_t, str745),
[Icall_REMSER_2] = __builtin_offsetof (struct msgstr_t, str746),

[Icall_MHAN_1] = __builtin_offsetof (struct msgstr_t, str750),

[Icall_RNG_1] = __builtin_offsetof (struct msgstr_t, str753),
[Icall_RNG_2] = __builtin_offsetof (struct msgstr_t, str754),
[Icall_RNG_3] = __builtin_offsetof (struct msgstr_t, str755),
[Icall_RNG_4] = __builtin_offsetof (struct msgstr_t, str756),

[Icall_EVID_1] = __builtin_offsetof (struct msgstr_t, str760),

[Icall_WINID_1] = __builtin_offsetof (struct msgstr_t, str763),
[Icall_WINID_2] = __builtin_offsetof (struct msgstr_t, str764),
[Icall_WINID_3] = __builtin_offsetof (struct msgstr_t, str765),
[Icall_WINID_4] = __builtin_offsetof (struct msgstr_t, str766),

[Icall_WINIMP_1] = __builtin_offsetof (struct msgstr_t, str769),
[Icall_WINIMP_2] = __builtin_offsetof (struct msgstr_t, str770),
[Icall_WINIMP_3] = __builtin_offsetof (struct msgstr_t, str771),
[Icall_WINIMP_4] = __builtin_offsetof (struct msgstr_t, str772),

[Icall_WINPRIN_1] = __builtin_offsetof (struct msgstr_t, str775),
[Icall_WINPRIN_2] = __builtin_offsetof (struct msgstr_t, str776),

[Icall_SECSTRING_1] = __builtin_offsetof (struct msgstr_t, str779),
[Icall_SECSTRING_2] = __builtin_offsetof (struct msgstr_t, str780),

[Icall_SECMAN_1] = __builtin_offsetof (struct msgstr_t, str784),
[Icall_SECMAN_2] = __builtin_offsetof (struct msgstr_t, str785),
[Icall_SECMAN_3] = __builtin_offsetof (struct msgstr_t, str786),
[Icall_SECMAN_4] = __builtin_offsetof (struct msgstr_t, str787),
[Icall_SECMAN_5] = __builtin_offsetof (struct msgstr_t, str788),
[Icall_SECMAN_6] = __builtin_offsetof (struct msgstr_t, str789),

[Icall_STRING_1] = __builtin_offsetof (struct msgstr_t, str792),
[Icall_STRING_2] = __builtin_offsetof (struct msgstr_t, str793),
[Icall_STRING_3] = __builtin_offsetof (struct msgstr_t, str794),
[Icall_STRING_4] = __builtin_offsetof (struct msgstr_t, str795),
[Icall_STRING_5] = __builtin_offsetof (struct msgstr_t, str796),
[Icall_STRING_6] = __builtin_offsetof (struct msgstr_t, str797),
[Icall_STRING_7] = __builtin_offsetof (struct msgstr_t, str798),
[Icall_STRING_8] = __builtin_offsetof (struct msgstr_t, str799),
[Icall_STRING_8a] = __builtin_offsetof (struct msgstr_t, str800),
[Icall_STRING_9] = __builtin_offsetof (struct msgstr_t, str801),
[Icall_STRING_10] = __builtin_offsetof (struct msgstr_t, str802),
[Icall_STRING_11] = __builtin_offsetof (struct msgstr_t, str803),

[Icall_TENC_1] = __builtin_offsetof (struct msgstr_t, str806),

[Icall_ILOCK_1] = __builtin_offsetof (struct msgstr_t, str809),
[Icall_ILOCK_2] = __builtin_offsetof (struct msgstr_t, str810),
[Icall_ILOCK_3] = __builtin_offsetof (struct msgstr_t, str811),
[Icall_ILOCK_4] = __builtin_offsetof (struct msgstr_t, str812),
[Icall_ILOCK_5] = __builtin_offsetof (struct msgstr_t, str813),
[Icall_ILOCK_6] = __builtin_offsetof (struct msgstr_t, str814),
[Icall_ILOCK_7] = __builtin_offsetof (struct msgstr_t, str815),
[Icall_ILOCK_8] = __builtin_offsetof (struct msgstr_t, str816),
[Icall_ILOCK_9] = __builtin_offsetof (struct msgstr_t, str817),
[Icall_ILOCK_10] = __builtin_offsetof (struct msgstr_t, str818),
[Icall_ILOCK_11] = __builtin_offsetof (struct msgstr_t, str819),
[Icall_ILOCK_12] = __builtin_offsetof (struct msgstr_t, str820),
[Icall_ILOCK_13] = __builtin_offsetof (struct msgstr_t, str821),
[Icall_ILOCK_14] = __builtin_offsetof (struct msgstr_t, str822),
[Icall_ILOCK_15] = __builtin_offsetof (struct msgstr_t, str823),
[Icall_ILOCK_16] = __builtin_offsetof (struct msgstr_t, str824),
[Icall_ILOCK_17] = __builtin_offsetof (struct msgstr_t, str825),
[Icall_ILOCK_18] = __builtin_offsetof (struct msgstr_t, str826),
[Icall_ILOCK_19] = __builtin_offsetof (struct msgstr_t, str827),
[Icall_ILOCK_20] = __builtin_offsetof (struct msgstr_t, str828),
[Icall_ILOCK_21] = __builtin_offsetof (struct msgstr_t, str829),

[Icall_ITHREAD_1] = __builtin_offsetof (struct msgstr_t, str832),

[Icall_MONIT_8] = __builtin_offsetof (struct msgstr_t, str835),
[Icall_MONIT_1] = __builtin_offsetof (struct msgstr_t, str836),
[Icall_MONIT_2] = __builtin_offsetof (struct msgstr_t, str837),
[Icall_MONIT_3] = __builtin_offsetof (struct msgstr_t, str838),
[Icall_MONIT_4] = __builtin_offsetof (struct msgstr_t, str839),
[Icall_MONIT_5] = __builtin_offsetof (struct msgstr_t, str840),
[Icall_MONIT_6] = __builtin_offsetof (struct msgstr_t, str841),
[Icall_MONIT_7] = __builtin_offsetof (struct msgstr_t, str842),
[Icall_MONIT_9] = __builtin_offsetof (struct msgstr_t, str843),

[Icall_MUTEX_1] = __builtin_offsetof (struct msgstr_t, str846),
[Icall_MUTEX_2] = __builtin_offsetof (struct msgstr_t, str847),
[Icall_MUTEX_3] = __builtin_offsetof (struct msgstr_t, str848),

[Icall_NATIVEC_1] = __builtin_offsetof (struct msgstr_t, str851),
[Icall_NATIVEC_2] = __builtin_offsetof (struct msgstr_t, str852),
[Icall_NATIVEC_3] = __builtin_offsetof (struct msgstr_t, str853),
[Icall_NATIVEC_4] = __builtin_offsetof (struct msgstr_t, str854),
[Icall_NATIVEC_5] = __builtin_offsetof (struct msgstr_t, str855),

[Icall_SEMA_1] = __builtin_offsetof (struct msgstr_t, str858),
[Icall_SEMA_2] = __builtin_offsetof (struct msgstr_t, str859),
[Icall_SEMA_3] = __builtin_offsetof (struct msgstr_t, str860),

[Icall_THREAD_1] = __builtin_offsetof (struct msgstr_t, str863),
[Icall_THREAD_1aa] = __builtin_offsetof (struct msgstr_t, str864),
[Icall_THREAD_1a] = __builtin_offsetof (struct msgstr_t, str865),
[Icall_THREAD_1b] = __builtin_offsetof (struct msgstr_t, str866),
[Icall_THREAD_2] = __builtin_offsetof (struct msgstr_t, str867),
[Icall_THREAD_2a] = __builtin_offsetof (struct msgstr_t, str868),
[Icall_THREAD_3] = __builtin_offsetof (struct msgstr_t, str869),
[Icall_THREAD_3a] = __builtin_offsetof (struct msgstr_t, str870),
[Icall_THREAD_4] = __builtin_offsetof (struct msgstr_t, str871),
[Icall_THREAD_55] = __builtin_offsetof (struct msgstr_t, str872),
[Icall_THREAD_7] = __builtin_offsetof (struct msgstr_t, str873),
[Icall_THREAD_8] = __builtin_offsetof (struct msgstr_t, str874),
[Icall_THREAD_11] = __builtin_offsetof (struct msgstr_t, str875),
[Icall_THREAD_53] = __builtin_offsetof (struct msgstr_t, str876),
[Icall_THREAD_12] = __builtin_offsetof (struct msgstr_t, str877),
[Icall_THREAD_13] = __builtin_offsetof (struct msgstr_t, str878),
[Icall_THREAD_14] = __builtin_offsetof (struct msgstr_t, str879),
[Icall_THREAD_15] = __builtin_offsetof (struct msgstr_t, str880),
[Icall_THREAD_18] = __builtin_offsetof (struct msgstr_t, str881),
[Icall_THREAD_21] = __builtin_offsetof (struct msgstr_t, str882),
[Icall_THREAD_22] = __builtin_offsetof (struct msgstr_t, str883),
[Icall_THREAD_54] = __builtin_offsetof (struct msgstr_t, str884),
[Icall_THREAD_23] = __builtin_offsetof (struct msgstr_t, str885),
[Icall_THREAD_25] = __builtin_offsetof (struct msgstr_t, str886),
[Icall_THREAD_26] = __builtin_offsetof (struct msgstr_t, str887),
[Icall_THREAD_27] = __builtin_offsetof (struct msgstr_t, str888),
[Icall_THREAD_28] = __builtin_offsetof (struct msgstr_t, str889),
[Icall_THREAD_29] = __builtin_offsetof (struct msgstr_t, str890),
[Icall_THREAD_30] = __builtin_offsetof (struct msgstr_t, str891),
[Icall_THREAD_31] = __builtin_offsetof (struct msgstr_t, str892),
[Icall_THREAD_32] = __builtin_offsetof (struct msgstr_t, str893),
[Icall_THREAD_33] = __builtin_offsetof (struct msgstr_t, str894),
[Icall_THREAD_34] = __builtin_offsetof (struct msgstr_t, str895),
[Icall_THREAD_35] = __builtin_offsetof (struct msgstr_t, str896),
[Icall_THREAD_36] = __builtin_offsetof (struct msgstr_t, str897),
[Icall_THREAD_37] = __builtin_offsetof (struct msgstr_t, str898),
[Icall_THREAD_38] = __builtin_offsetof (struct msgstr_t, str899),
[Icall_THREAD_39] = __builtin_offsetof (struct msgstr_t, str900),
[Icall_THREAD_40] = __builtin_offsetof (struct msgstr_t, str901),
[Icall_THREAD_41] = __builtin_offsetof (struct msgstr_t, str902),
[Icall_THREAD_42] = __builtin_offsetof (struct msgstr_t, str903),
[Icall_THREAD_43] = __builtin_offsetof (struct msgstr_t, str904),
[Icall_THREAD_44] = __builtin_offsetof (struct msgstr_t, str905),
[Icall_THREAD_45] = __builtin_offsetof (struct msgstr_t, str906),
[Icall_THREAD_46] = __builtin_offsetof (struct msgstr_t, str907),
[Icall_THREAD_47] = __builtin_offsetof (struct msgstr_t, str908),
[Icall_THREAD_48] = __builtin_offsetof (struct msgstr_t, str909),
[Icall_THREAD_49] = __builtin_offsetof (struct msgstr_t, str910),
[Icall_THREAD_50] = __builtin_offsetof (struct msgstr_t, str911),
[Icall_THREAD_51] = __builtin_offsetof (struct msgstr_t, str912),
[Icall_THREAD_9] = __builtin_offsetof (struct msgstr_t, str913),
[Icall_THREAD_52] = __builtin_offsetof (struct msgstr_t, str914),

[Icall_THREADP_1] = __builtin_offsetof (struct msgstr_t, str917),
[Icall_THREADP_2] = __builtin_offsetof (struct msgstr_t, str918),
[Icall_THREADP_3] = __builtin_offsetof (struct msgstr_t, str919),
[Icall_THREADP_35] = __builtin_offsetof (struct msgstr_t, str920),
[Icall_THREADP_4] = __builtin_offsetof (struct msgstr_t, str921),
[Icall_THREADP_5] = __builtin_offsetof (struct msgstr_t, str922),

[Icall_VOLATILE_28] = __builtin_offsetof (struct msgstr_t, str925),
[Icall_VOLATILE_1] = __builtin_offsetof (struct msgstr_t, str926),
[Icall_VOLATILE_2] = __builtin_offsetof (struct msgstr_t, str927),
[Icall_VOLATILE_3] = __builtin_offsetof (struct msgstr_t, str928),
[Icall_VOLATILE_4] = __builtin_offsetof (struct msgstr_t, str929),
[Icall_VOLATILE_5] = __builtin_offsetof (struct msgstr_t, str930),
[Icall_VOLATILE_6] = __builtin_offsetof (struct msgstr_t, str931),
[Icall_VOLATILE_7] = __builtin_offsetof (struct msgstr_t, str932),
[Icall_VOLATILE_8] = __builtin_offsetof (struct msgstr_t, str933),
[Icall_VOLATILE_9] = __builtin_offsetof (struct msgstr_t, str934),
[Icall_VOLATILE_10] = __builtin_offsetof (struct msgstr_t, str935),
[Icall_VOLATILE_11] = __builtin_offsetof (struct msgstr_t, str936),
[Icall_VOLATILE_12] = __builtin_offsetof (struct msgstr_t, str937),
[Icall_VOLATILE_13] = __builtin_offsetof (struct msgstr_t, str938),
[Icall_VOLATILE_27] = __builtin_offsetof (struct msgstr_t, str939),
[Icall_VOLATILE_14] = __builtin_offsetof (struct msgstr_t, str940),
[Icall_VOLATILE_15] = __builtin_offsetof (struct msgstr_t, str941),
[Icall_VOLATILE_16] = __builtin_offsetof (struct msgstr_t, str942),
[Icall_VOLATILE_17] = __builtin_offsetof (struct msgstr_t, str943),
[Icall_VOLATILE_18] = __builtin_offsetof (struct msgstr_t, str944),
[Icall_VOLATILE_19] = __builtin_offsetof (struct msgstr_t, str945),
[Icall_VOLATILE_20] = __builtin_offsetof (struct msgstr_t, str946),
[Icall_VOLATILE_21] = __builtin_offsetof (struct msgstr_t, str947),
[Icall_VOLATILE_22] = __builtin_offsetof (struct msgstr_t, str948),
[Icall_VOLATILE_23] = __builtin_offsetof (struct msgstr_t, str949),
[Icall_VOLATILE_24] = __builtin_offsetof (struct msgstr_t, str950),
[Icall_VOLATILE_25] = __builtin_offsetof (struct msgstr_t, str951),
[Icall_VOLATILE_26] = __builtin_offsetof (struct msgstr_t, str952),

[Icall_WAITH_1] = __builtin_offsetof (struct msgstr_t, str955),
[Icall_WAITH_2] = __builtin_offsetof (struct msgstr_t, str956),
[Icall_WAITH_3] = __builtin_offsetof (struct msgstr_t, str957),
[Icall_WAITH_4] = __builtin_offsetof (struct msgstr_t, str958),

[Icall_TYPE_1] = __builtin_offsetof (struct msgstr_t, str961),
[Icall_TYPE_2] = __builtin_offsetof (struct msgstr_t, str962),
[Icall_TYPE_3] = __builtin_offsetof (struct msgstr_t, str963),
[Icall_TYPE_4] = __builtin_offsetof (struct msgstr_t, str964),
[Icall_TYPE_5] = __builtin_offsetof (struct msgstr_t, str965),
[Icall_TYPE_6] = __builtin_offsetof (struct msgstr_t, str966),
[Icall_TYPE_7] = __builtin_offsetof (struct msgstr_t, str967),
[Icall_TYPE_8] = __builtin_offsetof (struct msgstr_t, str968),
[Icall_TYPE_9] = __builtin_offsetof (struct msgstr_t, str969),
[Icall_TYPE_10] = __builtin_offsetof (struct msgstr_t, str970),
[Icall_TYPE_11] = __builtin_offsetof (struct msgstr_t, str971),
[Icall_TYPE_12] = __builtin_offsetof (struct msgstr_t, str972),
[Icall_TYPE_13] = __builtin_offsetof (struct msgstr_t, str973),
[Icall_TYPE_14] = __builtin_offsetof (struct msgstr_t, str974),
[Icall_TYPE_15] = __builtin_offsetof (struct msgstr_t, str975),
[Icall_TYPE_16] = __builtin_offsetof (struct msgstr_t, str976),
[Icall_TYPE_17] = __builtin_offsetof (struct msgstr_t, str977),
[Icall_TYPE_18] = __builtin_offsetof (struct msgstr_t, str978),
[Icall_TYPE_19] = __builtin_offsetof (struct msgstr_t, str979),
[Icall_TYPE_20] = __builtin_offsetof (struct msgstr_t, str980),
[Icall_TYPE_21] = __builtin_offsetof (struct msgstr_t, str981),
[Icall_TYPE_22] = __builtin_offsetof (struct msgstr_t, str982),

[Icall_TYPEDR_1] = __builtin_offsetof (struct msgstr_t, str985),
[Icall_TYPEDR_2] = __builtin_offsetof (struct msgstr_t, str986),

[Icall_VALUET_1] = __builtin_offsetof (struct msgstr_t, str989),
[Icall_VALUET_2] = __builtin_offsetof (struct msgstr_t, str990),

[Icall_WEBIC_1] = __builtin_offsetof (struct msgstr_t, str993),
[Icall_WEBIC_2] = __builtin_offsetof (struct msgstr_t, str994),
[Icall_WEBIC_3] = __builtin_offsetof (struct msgstr_t, str995),

[Icall_COMOBJ_1] = __builtin_offsetof (struct msgstr_t, str999),
[Icall_COMOBJ_2] = __builtin_offsetof (struct msgstr_t, str1000),
[Icall_COMOBJ_3] = __builtin_offsetof (struct msgstr_t, str1001),
#undef ICALL
};

#define icall_name_get(id) ((const char*)&icall_names_str + icall_names_idx [(id)])

#else

#undef ICALL_TYPE
#undef ICALL
#define ICALL_TYPE(id,name,first) name,
#define ICALL(id,name,func)
static const char* const
icall_type_names [] = {
// #include "metadata/icall-def.h"
ICALL_TYPE(UNORM, "Mono.Globalization.Unicode.Normalization", UNORM_1)
ICALL(UNORM_1, "load_normalization_resource", load_normalization_resource)

#ifndef DISABLE_COM
ICALL_TYPE(COMPROX, "Mono.Interop.ComInteropProxy", COMPROX_1)
ICALL(COMPROX_1, "AddProxy", ves_icall_Mono_Interop_ComInteropProxy_AddProxy)
ICALL(COMPROX_2, "FindProxy", ves_icall_Mono_Interop_ComInteropProxy_FindProxy)
#endif

ICALL_TYPE(RUNTIME, "Mono.Runtime", RUNTIME_1)
ICALL(RUNTIME_1, "GetDisplayName", ves_icall_Mono_Runtime_GetDisplayName)
ICALL(RUNTIME_12, "GetNativeStackTrace", ves_icall_Mono_Runtime_GetNativeStackTrace)
ICALL(RUNTIME_13, "SetGCAllowSynchronousMajor", ves_icall_Mono_Runtime_SetGCAllowSynchronousMajor)

#ifndef PLATFORM_RO_FS
ICALL_TYPE(KPAIR, "Mono.Security.Cryptography.KeyPairPersistence", KPAIR_1)
ICALL(KPAIR_1, "_CanSecure", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_CanSecure)
ICALL(KPAIR_2, "_IsMachineProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsMachineProtected)
ICALL(KPAIR_3, "_IsUserProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsUserProtected)
ICALL(KPAIR_4, "_ProtectMachine", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectMachine)
ICALL(KPAIR_5, "_ProtectUser", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectUser)
#endif /* !PLATFORM_RO_FS */

ICALL_TYPE(ACTIV, "System.Activator", ACTIV_1)
ICALL(ACTIV_1, "CreateInstanceInternal", ves_icall_System_Activator_CreateInstanceInternal)

ICALL_TYPE(APPDOM, "System.AppDomain", APPDOM_1)
ICALL(APPDOM_1, "ExecuteAssembly", ves_icall_System_AppDomain_ExecuteAssembly)
ICALL(APPDOM_2, "GetAssemblies", ves_icall_System_AppDomain_GetAssemblies)
ICALL(APPDOM_3, "GetData", ves_icall_System_AppDomain_GetData)
ICALL(APPDOM_4, "InternalGetContext", ves_icall_System_AppDomain_InternalGetContext)
ICALL(APPDOM_5, "InternalGetDefaultContext", ves_icall_System_AppDomain_InternalGetDefaultContext)
ICALL(APPDOM_6, "InternalGetProcessGuid", ves_icall_System_AppDomain_InternalGetProcessGuid)
ICALL(APPDOM_7, "InternalIsFinalizingForUnload", ves_icall_System_AppDomain_InternalIsFinalizingForUnload)
ICALL(APPDOM_8, "InternalPopDomainRef", ves_icall_System_AppDomain_InternalPopDomainRef)
ICALL(APPDOM_9, "InternalPushDomainRef", ves_icall_System_AppDomain_InternalPushDomainRef)
ICALL(APPDOM_10, "InternalPushDomainRefByID", ves_icall_System_AppDomain_InternalPushDomainRefByID)
ICALL(APPDOM_11, "InternalSetContext", ves_icall_System_AppDomain_InternalSetContext)
ICALL(APPDOM_12, "InternalSetDomain", ves_icall_System_AppDomain_InternalSetDomain)
ICALL(APPDOM_13, "InternalSetDomainByID", ves_icall_System_AppDomain_InternalSetDomainByID)
ICALL(APPDOM_14, "InternalUnload", ves_icall_System_AppDomain_InternalUnload)
ICALL(APPDOM_15, "LoadAssembly", ves_icall_System_AppDomain_LoadAssembly)
ICALL(APPDOM_16, "LoadAssemblyRaw", ves_icall_System_AppDomain_LoadAssemblyRaw)
ICALL(APPDOM_17, "SetData", ves_icall_System_AppDomain_SetData)
ICALL(APPDOM_18, "createDomain", ves_icall_System_AppDomain_createDomain)
ICALL(APPDOM_19, "getCurDomain", ves_icall_System_AppDomain_getCurDomain)
ICALL(APPDOM_20, "getFriendlyName", ves_icall_System_AppDomain_getFriendlyName)
ICALL(APPDOM_21, "getRootDomain", ves_icall_System_AppDomain_getRootDomain)
ICALL(APPDOM_22, "getSetup", ves_icall_System_AppDomain_getSetup)

ICALL_TYPE(ARGI, "System.ArgIterator", ARGI_1)
ICALL(ARGI_1, "IntGetNextArg()",                  mono_ArgIterator_IntGetNextArg)
ICALL(ARGI_2, "IntGetNextArg(intptr)", mono_ArgIterator_IntGetNextArgT)
ICALL(ARGI_3, "IntGetNextArgType",                mono_ArgIterator_IntGetNextArgType)
ICALL(ARGI_4, "Setup",                            mono_ArgIterator_Setup)

ICALL_TYPE(ARRAY, "System.Array", ARRAY_1)
ICALL(ARRAY_1, "ClearInternal",    ves_icall_System_Array_ClearInternal)
ICALL(ARRAY_2, "Clone",            mono_array_clone)
ICALL(ARRAY_3, "CreateInstanceImpl",   ves_icall_System_Array_CreateInstanceImpl)
ICALL(ARRAY_14, "CreateInstanceImpl64",   ves_icall_System_Array_CreateInstanceImpl64)
ICALL(ARRAY_4, "FastCopy",         ves_icall_System_Array_FastCopy)
ICALL(ARRAY_5, "GetGenericValueImpl", ves_icall_System_Array_GetGenericValueImpl)
ICALL(ARRAY_6, "GetLength",        ves_icall_System_Array_GetLength)
ICALL(ARRAY_15, "GetLongLength",        ves_icall_System_Array_GetLongLength)
ICALL(ARRAY_7, "GetLowerBound",    ves_icall_System_Array_GetLowerBound)
ICALL(ARRAY_8, "GetRank",          ves_icall_System_Array_GetRank)
ICALL(ARRAY_9, "GetValue",         ves_icall_System_Array_GetValue)
ICALL(ARRAY_10, "GetValueImpl",     ves_icall_System_Array_GetValueImpl)
ICALL(ARRAY_11, "SetGenericValueImpl", ves_icall_System_Array_SetGenericValueImpl)
ICALL(ARRAY_12, "SetValue",         ves_icall_System_Array_SetValue)
ICALL(ARRAY_13, "SetValueImpl",     ves_icall_System_Array_SetValueImpl)

ICALL_TYPE(BUFFER, "System.Buffer", BUFFER_1)
ICALL(BUFFER_1, "BlockCopyInternal", ves_icall_System_Buffer_BlockCopyInternal)
ICALL(BUFFER_2, "ByteLengthInternal", ves_icall_System_Buffer_ByteLengthInternal)
ICALL(BUFFER_3, "GetByteInternal", ves_icall_System_Buffer_GetByteInternal)
ICALL(BUFFER_4, "SetByteInternal", ves_icall_System_Buffer_SetByteInternal)

ICALL_TYPE(CHAR, "System.Char", CHAR_1)
ICALL(CHAR_1, "GetDataTablePointers", ves_icall_System_Char_GetDataTablePointers)

ICALL_TYPE (COMPO_W, "System.ComponentModel.Win32Exception", COMPO_W_1)
ICALL (COMPO_W_1, "W32ErrorMessage", ves_icall_System_ComponentModel_Win32Exception_W32ErrorMessage)

ICALL_TYPE(DEFAULTC, "System.Configuration.DefaultConfig", DEFAULTC_1)
ICALL(DEFAULTC_1, "get_bundled_machine_config", get_bundled_machine_config)
ICALL(DEFAULTC_2, "get_machine_config_path", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)

/* Note that the below icall shares the same function as DefaultConfig uses */
ICALL_TYPE(INTCFGHOST, "System.Configuration.InternalConfigurationHost", INTCFGHOST_1)
ICALL(INTCFGHOST_1, "get_bundled_app_config", get_bundled_app_config)
ICALL(INTCFGHOST_2, "get_bundled_machine_config", get_bundled_machine_config)

ICALL_TYPE(CONSOLE, "System.ConsoleDriver", CONSOLE_1)
ICALL(CONSOLE_1, "InternalKeyAvailable", ves_icall_System_ConsoleDriver_InternalKeyAvailable )
ICALL(CONSOLE_2, "Isatty", ves_icall_System_ConsoleDriver_Isatty )
ICALL(CONSOLE_3, "SetBreak", ves_icall_System_ConsoleDriver_SetBreak )
ICALL(CONSOLE_4, "SetEcho", ves_icall_System_ConsoleDriver_SetEcho )
ICALL(CONSOLE_5, "TtySetup", ves_icall_System_ConsoleDriver_TtySetup )

ICALL_TYPE(CONVERT, "System.Convert", CONVERT_1)
ICALL(CONVERT_1, "InternalFromBase64CharArray", InternalFromBase64CharArray )
ICALL(CONVERT_2, "InternalFromBase64String", InternalFromBase64String )

ICALL_TYPE(TZONE, "System.CurrentSystemTimeZone", TZONE_1)
ICALL(TZONE_1, "GetTimeZoneData", ves_icall_System_CurrentSystemTimeZone_GetTimeZoneData)

ICALL_TYPE(DTIME, "System.DateTime", DTIME_1)
ICALL(DTIME_1, "GetNow", mono_100ns_datetime)
ICALL(DTIME_2, "GetTimeMonotonic", mono_100ns_ticks)

#ifndef DISABLE_DECIMAL
ICALL_TYPE(DECIMAL, "System.Decimal", DECIMAL_1)
ICALL(DECIMAL_1, "decimal2Int64", mono_decimal2Int64)
ICALL(DECIMAL_2, "decimal2UInt64", mono_decimal2UInt64)
ICALL(DECIMAL_3, "decimal2double", mono_decimal2double)
//ICALL(DECIMAL_4, "decimal2string", mono_decimal2string)
ICALL(DECIMAL_5, "decimalCompare", mono_decimalCompare)
ICALL(DECIMAL_6, "decimalDiv", mono_decimalDiv)
ICALL(DECIMAL_7, "decimalFloorAndTrunc", mono_decimalFloorAndTrunc)
ICALL(DECIMAL_8, "decimalIncr", mono_decimalIncr)
ICALL(DECIMAL_9, "decimalIntDiv", mono_decimalIntDiv)
ICALL(DECIMAL_10, "decimalMult", mono_decimalMult)
ICALL(DECIMAL_11, "decimalRound", mono_decimalRound)
ICALL(DECIMAL_12, "decimalSetExponent", mono_decimalSetExponent)
ICALL(DECIMAL_13, "double2decimal", mono_double2decimal) /* FIXME: wrong signature. */
ICALL(DECIMAL_14, "string2decimal", mono_string2decimal)
#endif

ICALL_TYPE(DELEGATE, "System.Delegate", DELEGATE_1)
ICALL(DELEGATE_1, "CreateDelegate_internal", ves_icall_System_Delegate_CreateDelegate_internal)
ICALL(DELEGATE_2, "SetMulticastInvoke", ves_icall_System_Delegate_SetMulticastInvoke)

ICALL_TYPE(DEBUGR, "System.Diagnostics.Debugger", DEBUGR_1)
ICALL(DEBUGR_1, "IsAttached_internal", ves_icall_System_Diagnostics_Debugger_IsAttached_internal)
ICALL(DEBUGR_2, "IsLogging", ves_icall_System_Diagnostics_Debugger_IsLogging)
ICALL(DEBUGR_3, "Log", ves_icall_System_Diagnostics_Debugger_Log)

ICALL_TYPE(TRACEL, "System.Diagnostics.DefaultTraceListener", TRACEL_1)
ICALL(TRACEL_1, "WriteWindowsDebugString", ves_icall_System_Diagnostics_DefaultTraceListener_WriteWindowsDebugString)

ICALL_TYPE(FILEV, "System.Diagnostics.FileVersionInfo", FILEV_1)
ICALL(FILEV_1, "GetVersionInfo_internal(string)", ves_icall_System_Diagnostics_FileVersionInfo_GetVersionInfo_internal)

#ifndef DISABLE_PROCESS_HANDLING
ICALL_TYPE(PERFCTR, "System.Diagnostics.PerformanceCounter", PERFCTR_1)
ICALL(PERFCTR_1, "FreeData", mono_perfcounter_free_data)
ICALL(PERFCTR_2, "GetImpl", mono_perfcounter_get_impl)
ICALL(PERFCTR_3, "GetSample", mono_perfcounter_get_sample)
ICALL(PERFCTR_4, "UpdateValue", mono_perfcounter_update_value)

ICALL_TYPE(PERFCTRCAT, "System.Diagnostics.PerformanceCounterCategory", PERFCTRCAT_1)
ICALL(PERFCTRCAT_1, "CategoryDelete", mono_perfcounter_category_del)
ICALL(PERFCTRCAT_2, "CategoryHelpInternal",   mono_perfcounter_category_help)
ICALL(PERFCTRCAT_3, "CounterCategoryExists", mono_perfcounter_category_exists)
ICALL(PERFCTRCAT_4, "Create",         mono_perfcounter_create)
ICALL(PERFCTRCAT_5, "GetCategoryNames", mono_perfcounter_category_names)
ICALL(PERFCTRCAT_6, "GetCounterNames", mono_perfcounter_counter_names)
ICALL(PERFCTRCAT_7, "GetInstanceNames", mono_perfcounter_instance_names)
ICALL(PERFCTRCAT_8, "InstanceExistsInternal", mono_perfcounter_instance_exists)

ICALL_TYPE(PROCESS, "System.Diagnostics.Process", PROCESS_1)
ICALL(PROCESS_1, "CreateProcess_internal(System.Diagnostics.ProcessStartInfo,intptr,intptr,intptr,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_CreateProcess_internal)
ICALL(PROCESS_2, "ExitCode_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitCode_internal)
ICALL(PROCESS_3, "ExitTime_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitTime_internal)
ICALL(PROCESS_4, "GetModules_internal(intptr)", ves_icall_System_Diagnostics_Process_GetModules_internal)
ICALL(PROCESS_5, "GetPid_internal()", ves_icall_System_Diagnostics_Process_GetPid_internal)
ICALL(PROCESS_5B, "GetPriorityClass(intptr,int&)", ves_icall_System_Diagnostics_Process_GetPriorityClass)
ICALL(PROCESS_5H, "GetProcessData", ves_icall_System_Diagnostics_Process_GetProcessData)
ICALL(PROCESS_6, "GetProcess_internal(int)", ves_icall_System_Diagnostics_Process_GetProcess_internal)
ICALL(PROCESS_7, "GetProcesses_internal()", ves_icall_System_Diagnostics_Process_GetProcesses_internal)
ICALL(PROCESS_8, "GetWorkingSet_internal(intptr,int&,int&)", ves_icall_System_Diagnostics_Process_GetWorkingSet_internal)
ICALL(PROCESS_9, "Kill_internal", ves_icall_System_Diagnostics_Process_Kill_internal)
ICALL(PROCESS_10, "ProcessName_internal(intptr)", ves_icall_System_Diagnostics_Process_ProcessName_internal)
ICALL(PROCESS_11, "Process_free_internal(intptr)", ves_icall_System_Diagnostics_Process_Process_free_internal)
ICALL(PROCESS_11B, "SetPriorityClass(intptr,int,int&)", ves_icall_System_Diagnostics_Process_SetPriorityClass)
ICALL(PROCESS_12, "SetWorkingSet_internal(intptr,int,int,bool)", ves_icall_System_Diagnostics_Process_SetWorkingSet_internal)
ICALL(PROCESS_13, "ShellExecuteEx_internal(System.Diagnostics.ProcessStartInfo,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_ShellExecuteEx_internal)
ICALL(PROCESS_14, "StartTime_internal(intptr)", ves_icall_System_Diagnostics_Process_StartTime_internal)
ICALL(PROCESS_14M, "Times", ves_icall_System_Diagnostics_Process_Times)
ICALL(PROCESS_15, "WaitForExit_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForExit_internal)
ICALL(PROCESS_16, "WaitForInputIdle_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForInputIdle_internal)

ICALL_TYPE (PROCESSHANDLE, "System.Diagnostics.Process/ProcessWaitHandle", PROCESSHANDLE_1)
ICALL (PROCESSHANDLE_1, "ProcessHandle_close(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_close)
ICALL (PROCESSHANDLE_2, "ProcessHandle_duplicate(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_duplicate)
#endif /* !DISABLE_PROCESS_HANDLING */

ICALL_TYPE(STOPWATCH, "System.Diagnostics.Stopwatch", STOPWATCH_1)
ICALL(STOPWATCH_1, "GetTimestamp", mono_100ns_ticks)

ICALL_TYPE(DOUBLE, "System.Double", DOUBLE_1)
ICALL(DOUBLE_1, "ParseImpl",    mono_double_ParseImpl)

ICALL_TYPE(ENUM, "System.Enum", ENUM_1)
ICALL(ENUM_1, "ToObject", ves_icall_System_Enum_ToObject)
ICALL(ENUM_5, "compare_value_to", ves_icall_System_Enum_compare_value_to)
ICALL(ENUM_4, "get_hashcode", ves_icall_System_Enum_get_hashcode)
ICALL(ENUM_3, "get_underlying_type", ves_icall_System_Enum_get_underlying_type)
ICALL(ENUM_2, "get_value", ves_icall_System_Enum_get_value)

ICALL_TYPE(ENV, "System.Environment", ENV_1)
ICALL(ENV_1, "Exit", ves_icall_System_Environment_Exit)
ICALL(ENV_2, "GetCommandLineArgs", mono_runtime_get_main_args)
ICALL(ENV_3, "GetEnvironmentVariableNames", ves_icall_System_Environment_GetEnvironmentVariableNames)
ICALL(ENV_4, "GetLogicalDrivesInternal", ves_icall_System_Environment_GetLogicalDrives )
ICALL(ENV_5, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(ENV_51, "GetNewLine", ves_icall_System_Environment_get_NewLine)
ICALL(ENV_6, "GetOSVersionString", ves_icall_System_Environment_GetOSVersionString)
ICALL(ENV_6a, "GetPageSize", mono_pagesize)
ICALL(ENV_7, "GetWindowsFolderPath", ves_icall_System_Environment_GetWindowsFolderPath)
ICALL(ENV_8, "InternalSetEnvironmentVariable", ves_icall_System_Environment_InternalSetEnvironmentVariable)
ICALL(ENV_9, "get_ExitCode", mono_environment_exitcode_get)
ICALL(ENV_10, "get_HasShutdownStarted", ves_icall_System_Environment_get_HasShutdownStarted)
ICALL(ENV_11, "get_MachineName", ves_icall_System_Environment_get_MachineName)
ICALL(ENV_13, "get_Platform", ves_icall_System_Environment_get_Platform)
ICALL(ENV_14, "get_ProcessorCount", mono_cpu_count)
ICALL(ENV_15, "get_TickCount", mono_msec_ticks)
ICALL(ENV_16, "get_UserName", ves_icall_System_Environment_get_UserName)
ICALL(ENV_16m, "internalBroadcastSettingChange", ves_icall_System_Environment_BroadcastSettingChange)
ICALL(ENV_17, "internalGetEnvironmentVariable", ves_icall_System_Environment_GetEnvironmentVariable)
ICALL(ENV_18, "internalGetGacPath", ves_icall_System_Environment_GetGacPath)
ICALL(ENV_19, "internalGetHome", ves_icall_System_Environment_InternalGetHome)
ICALL(ENV_20, "set_ExitCode", mono_environment_exitcode_set)

ICALL_TYPE(GC, "System.GC", GC_0)
ICALL(GC_0, "CollectionCount", mono_gc_collection_count)
ICALL(GC_0a, "GetGeneration", mono_gc_get_generation)
ICALL(GC_1, "GetTotalMemory", ves_icall_System_GC_GetTotalMemory)
ICALL(GC_2, "InternalCollect", ves_icall_System_GC_InternalCollect)
ICALL(GC_3, "KeepAlive", ves_icall_System_GC_KeepAlive)
ICALL(GC_4, "ReRegisterForFinalize", ves_icall_System_GC_ReRegisterForFinalize)
ICALL(GC_4a, "RecordPressure", mono_gc_add_memory_pressure)
ICALL(GC_5, "SuppressFinalize", ves_icall_System_GC_SuppressFinalize)
ICALL(GC_6, "WaitForPendingFinalizers", ves_icall_System_GC_WaitForPendingFinalizers)
ICALL(GC_7, "get_MaxGeneration", mono_gc_max_generation)
ICALL(GC_9, "get_ephemeron_tombstone", ves_icall_System_GC_get_ephemeron_tombstone)
ICALL(GC_8, "register_ephemeron_array", ves_icall_System_GC_register_ephemeron_array)

ICALL_TYPE(COMPINF, "System.Globalization.CompareInfo", COMPINF_1)
ICALL(COMPINF_1, "assign_sortkey(object,string,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_assign_sortkey)
ICALL(COMPINF_2, "construct_compareinfo(string)", ves_icall_System_Globalization_CompareInfo_construct_compareinfo)
ICALL(COMPINF_3, "free_internal_collator()", ves_icall_System_Globalization_CompareInfo_free_internal_collator)
ICALL(COMPINF_4, "internal_compare(string,int,int,string,int,int,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_internal_compare)
ICALL(COMPINF_5, "internal_index(string,int,int,char,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index_char)
ICALL(COMPINF_6, "internal_index(string,int,int,string,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index)

ICALL_TYPE(CULINF, "System.Globalization.CultureInfo", CULINF_2)
ICALL(CULINF_2, "construct_datetime_format", ves_icall_System_Globalization_CultureInfo_construct_datetime_format)
ICALL(CULINF_4, "construct_internal_locale_from_current_locale", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_current_locale)
ICALL(CULINF_5, "construct_internal_locale_from_lcid", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_lcid)
ICALL(CULINF_6, "construct_internal_locale_from_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_name)
ICALL(CULINF_7, "construct_internal_locale_from_specific_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_specific_name)
ICALL(CULINF_8, "construct_number_format", ves_icall_System_Globalization_CultureInfo_construct_number_format)
ICALL(CULINF_9, "internal_get_cultures", ves_icall_System_Globalization_CultureInfo_internal_get_cultures)
//ICALL(CULINF_10, "internal_is_lcid_neutral", ves_icall_System_Globalization_CultureInfo_internal_is_lcid_neutral)

ICALL_TYPE(REGINF, "System.Globalization.RegionInfo", REGINF_1)
ICALL(REGINF_1, "construct_internal_region_from_lcid", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_lcid)
ICALL(REGINF_2, "construct_internal_region_from_name", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_name)

#ifndef PLATFORM_NO_DRIVEINFO
ICALL_TYPE(IODRIVEINFO, "System.IO.DriveInfo", IODRIVEINFO_1)
ICALL(IODRIVEINFO_1, "GetDiskFreeSpaceInternal", ves_icall_System_IO_DriveInfo_GetDiskFreeSpace)
ICALL(IODRIVEINFO_2, "GetDriveFormat", ves_icall_System_IO_DriveInfo_GetDriveFormat)
ICALL(IODRIVEINFO_3, "GetDriveTypeInternal", ves_icall_System_IO_DriveInfo_GetDriveType)
#endif

ICALL_TYPE(FAMW, "System.IO.FAMWatcher", FAMW_1)
ICALL(FAMW_1, "InternalFAMNextEvent", ves_icall_System_IO_FAMW_InternalFAMNextEvent)

ICALL_TYPE(FILEW, "System.IO.FileSystemWatcher", FILEW_4)
ICALL(FILEW_4, "InternalSupportsFSW", ves_icall_System_IO_FSW_SupportsFSW)

ICALL_TYPE(INOW, "System.IO.InotifyWatcher", INOW_1)
ICALL(INOW_1, "AddWatch", ves_icall_System_IO_InotifyWatcher_AddWatch)
ICALL(INOW_2, "GetInotifyInstance", ves_icall_System_IO_InotifyWatcher_GetInotifyInstance)
ICALL(INOW_3, "RemoveWatch", ves_icall_System_IO_InotifyWatcher_RemoveWatch)

#if defined (TARGET_IOS) || defined (TARGET_ANDROID)
ICALL_TYPE(MMAPIMPL, "System.IO.MemoryMappedFiles.MemoryMapImpl", MMAPIMPL_1)
ICALL(MMAPIMPL_1, "mono_filesize_from_fd", mono_filesize_from_fd)
ICALL(MMAPIMPL_2, "mono_filesize_from_path", mono_filesize_from_path)
#endif


ICALL_TYPE(MONOIO, "System.IO.MonoIO", MONOIO_1)
ICALL(MONOIO_1, "Close(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Close)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_2, "CopyFile(string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CopyFile)
ICALL(MONOIO_3, "CreateDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CreateDirectory)
ICALL(MONOIO_4, "CreatePipe(intptr&,intptr&)", ves_icall_System_IO_MonoIO_CreatePipe)
ICALL(MONOIO_5, "DeleteFile(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_DeleteFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_34, "DuplicateHandle", ves_icall_System_IO_MonoIO_DuplicateHandle)
ICALL(MONOIO_37, "FindClose", ves_icall_System_IO_MonoIO_FindClose)
ICALL(MONOIO_35, "FindFirst", ves_icall_System_IO_MonoIO_FindFirst)
ICALL(MONOIO_36, "FindNext", ves_icall_System_IO_MonoIO_FindNext)
ICALL(MONOIO_6, "Flush(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Flush)
ICALL(MONOIO_7, "GetCurrentDirectory(System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetCurrentDirectory)
ICALL(MONOIO_8, "GetFileAttributes(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileAttributes)
ICALL(MONOIO_9, "GetFileStat(string,System.IO.MonoIOStat&,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileStat)
ICALL(MONOIO_10, "GetFileSystemEntries", ves_icall_System_IO_MonoIO_GetFileSystemEntries)
ICALL(MONOIO_11, "GetFileType(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileType)
ICALL(MONOIO_12, "GetLength(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_13, "GetTempPath(string&)", ves_icall_System_IO_MonoIO_GetTempPath)
ICALL(MONOIO_14, "Lock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Lock)
ICALL(MONOIO_15, "MoveFile(string,string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_MoveFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_16, "Open(string,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.IO.FileOptions,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Open)
ICALL(MONOIO_17, "Read(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Read)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_18, "RemoveDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_RemoveDirectory)
ICALL(MONOIO_18M, "ReplaceFile(string,string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_ReplaceFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_19, "Seek(intptr,long,System.IO.SeekOrigin,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Seek)
ICALL(MONOIO_20, "SetCurrentDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetCurrentDirectory)
ICALL(MONOIO_21, "SetFileAttributes(string,System.IO.FileAttributes,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileAttributes)
ICALL(MONOIO_22, "SetFileTime(intptr,long,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileTime)
ICALL(MONOIO_23, "SetLength(intptr,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_24, "Unlock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Unlock)
#endif
ICALL(MONOIO_25, "Write(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Write)
ICALL(MONOIO_26, "get_AltDirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_AltDirectorySeparatorChar)
ICALL(MONOIO_27, "get_ConsoleError", ves_icall_System_IO_MonoIO_get_ConsoleError)
ICALL(MONOIO_28, "get_ConsoleInput", ves_icall_System_IO_MonoIO_get_ConsoleInput)
ICALL(MONOIO_29, "get_ConsoleOutput", ves_icall_System_IO_MonoIO_get_ConsoleOutput)
ICALL(MONOIO_30, "get_DirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_DirectorySeparatorChar)
ICALL(MONOIO_31, "get_InvalidPathChars", ves_icall_System_IO_MonoIO_get_InvalidPathChars)
ICALL(MONOIO_32, "get_PathSeparator", ves_icall_System_IO_MonoIO_get_PathSeparator)
ICALL(MONOIO_33, "get_VolumeSeparatorChar", ves_icall_System_IO_MonoIO_get_VolumeSeparatorChar)

ICALL_TYPE(IOPATH, "System.IO.Path", IOPATH_1)
ICALL(IOPATH_1, "get_temp_path", ves_icall_System_IO_get_temp_path)

ICALL_TYPE(MATH, "System.Math", MATH_1)
ICALL(MATH_1, "Acos", ves_icall_System_Math_Acos)
ICALL(MATH_2, "Asin", ves_icall_System_Math_Asin)
ICALL(MATH_3, "Atan", ves_icall_System_Math_Atan)
ICALL(MATH_4, "Atan2", ves_icall_System_Math_Atan2)
ICALL(MATH_5, "Cos", ves_icall_System_Math_Cos)
ICALL(MATH_6, "Cosh", ves_icall_System_Math_Cosh)
ICALL(MATH_7, "Exp", ves_icall_System_Math_Exp)
ICALL(MATH_8, "Floor", ves_icall_System_Math_Floor)
ICALL(MATH_9, "Log", ves_icall_System_Math_Log)
ICALL(MATH_10, "Log10", ves_icall_System_Math_Log10)
ICALL(MATH_11, "Pow", ves_icall_System_Math_Pow)
ICALL(MATH_12, "Round", ves_icall_System_Math_Round)
ICALL(MATH_13, "Round2", ves_icall_System_Math_Round2)
ICALL(MATH_14, "Sin", ves_icall_System_Math_Sin)
ICALL(MATH_15, "Sinh", ves_icall_System_Math_Sinh)
ICALL(MATH_16, "Sqrt", ves_icall_System_Math_Sqrt)
ICALL(MATH_17, "Tan", ves_icall_System_Math_Tan)
ICALL(MATH_18, "Tanh", ves_icall_System_Math_Tanh)

ICALL_TYPE(MCATTR, "System.MonoCustomAttrs", MCATTR_1)
ICALL(MCATTR_1, "GetCustomAttributesDataInternal", mono_reflection_get_custom_attrs_data)
ICALL(MCATTR_2, "GetCustomAttributesInternal", custom_attrs_get_by_type)
ICALL(MCATTR_3, "IsDefinedInternal", custom_attrs_defined_internal)

ICALL_TYPE(MENUM, "System.MonoEnumInfo", MENUM_1)
ICALL(MENUM_1, "get_enum_info", ves_icall_get_enum_info)

ICALL_TYPE(MTYPE, "System.MonoType", MTYPE_1)
ICALL(MTYPE_1, "GetArrayRank", ves_icall_MonoType_GetArrayRank)
ICALL(MTYPE_2, "GetConstructors", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_3, "GetConstructors_internal", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_4, "GetCorrespondingInflatedConstructor", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_5, "GetCorrespondingInflatedMethod", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_6, "GetElementType", ves_icall_MonoType_GetElementType)
ICALL(MTYPE_7, "GetEvents_internal", ves_icall_Type_GetEvents_internal)
ICALL(MTYPE_8, "GetField", ves_icall_Type_GetField)
ICALL(MTYPE_9, "GetFields_internal", ves_icall_Type_GetFields_internal)
ICALL(MTYPE_10, "GetGenericArguments", ves_icall_MonoType_GetGenericArguments)
ICALL(MTYPE_11, "GetInterfaces", ves_icall_Type_GetInterfaces)
ICALL(MTYPE_12, "GetMethodsByName", ves_icall_Type_GetMethodsByName)
ICALL(MTYPE_13, "GetNestedType", ves_icall_Type_GetNestedType)
ICALL(MTYPE_14, "GetNestedTypes", ves_icall_Type_GetNestedTypes)
ICALL(MTYPE_15, "GetPropertiesByName", ves_icall_Type_GetPropertiesByName)
ICALL(MTYPE_16, "InternalGetEvent", ves_icall_MonoType_GetEvent)
ICALL(MTYPE_17, "IsByRefImpl", ves_icall_type_isbyref)
ICALL(MTYPE_18, "IsCOMObjectImpl", ves_icall_type_iscomobject)
ICALL(MTYPE_19, "IsPointerImpl", ves_icall_type_ispointer)
ICALL(MTYPE_20, "IsPrimitiveImpl", ves_icall_type_isprimitive)
ICALL(MTYPE_21, "getFullName", ves_icall_System_MonoType_getFullName)
ICALL(MTYPE_22, "get_Assembly", ves_icall_MonoType_get_Assembly)
ICALL(MTYPE_23, "get_BaseType", ves_icall_get_type_parent)
ICALL(MTYPE_24, "get_DeclaringMethod", ves_icall_MonoType_get_DeclaringMethod)
ICALL(MTYPE_25, "get_DeclaringType", ves_icall_MonoType_get_DeclaringType)
ICALL(MTYPE_26, "get_IsGenericParameter", ves_icall_MonoType_get_IsGenericParameter)
ICALL(MTYPE_27, "get_Module", ves_icall_MonoType_get_Module)
ICALL(MTYPE_28, "get_Name", ves_icall_MonoType_get_Name)
ICALL(MTYPE_29, "get_Namespace", ves_icall_MonoType_get_Namespace)
ICALL(MTYPE_31, "get_attributes", ves_icall_get_attributes)
ICALL(MTYPE_33, "get_core_clr_security_level", vell_icall_MonoType_get_core_clr_security_level)
ICALL(MTYPE_32, "type_from_obj", mono_type_type_from_obj)

#ifndef DISABLE_SOCKETS
ICALL_TYPE(NDNS, "System.Net.Dns", NDNS_1)
ICALL(NDNS_1, "GetHostByAddr_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByAddr_internal)
ICALL(NDNS_2, "GetHostByName_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByName_internal)
ICALL(NDNS_3, "GetHostName_internal(string&)", ves_icall_System_Net_Dns_GetHostName_internal)

ICALL_TYPE(SOCK, "System.Net.Sockets.Socket", SOCK_1)
ICALL(SOCK_1, "Accept_internal(intptr,int&,bool)", ves_icall_System_Net_Sockets_Socket_Accept_internal)
ICALL(SOCK_2, "Available_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Available_internal)
ICALL(SOCK_3, "Bind_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Bind_internal)
ICALL(SOCK_4, "Blocking_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Blocking_internal)
ICALL(SOCK_5, "Close_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Close_internal)
ICALL(SOCK_6, "Connect_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Connect_internal)
ICALL (SOCK_6a, "Disconnect_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Disconnect_internal)
ICALL(SOCK_7, "GetSocketOption_arr_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,byte[]&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_arr_internal)
ICALL(SOCK_8, "GetSocketOption_obj_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_obj_internal)
ICALL(SOCK_9, "Listen_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_Listen_internal)
ICALL(SOCK_10, "LocalEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_LocalEndPoint_internal)
ICALL(SOCK_11, "Poll_internal", ves_icall_System_Net_Sockets_Socket_Poll_internal)
ICALL(SOCK_11a, "Receive_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_array_internal)
ICALL(SOCK_12, "Receive_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_internal)
ICALL(SOCK_13, "RecvFrom_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress&,int&)", ves_icall_System_Net_Sockets_Socket_RecvFrom_internal)
ICALL(SOCK_14, "RemoteEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_RemoteEndPoint_internal)
ICALL(SOCK_15, "Select_internal(System.Net.Sockets.Socket[]&,int,int&)", ves_icall_System_Net_Sockets_Socket_Select_internal)
ICALL(SOCK_15a, "SendFile(intptr,string,byte[],byte[],System.Net.Sockets.TransmitFileOptions)", ves_icall_System_Net_Sockets_Socket_SendFile)
ICALL(SOCK_16, "SendTo_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_SendTo_internal)
ICALL(SOCK_16a, "Send_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_array_internal)
ICALL(SOCK_17, "Send_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_internal)
ICALL(SOCK_18, "SetSocketOption_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object,byte[],int,int&)", ves_icall_System_Net_Sockets_Socket_SetSocketOption_internal)
ICALL(SOCK_19, "Shutdown_internal(intptr,System.Net.Sockets.SocketShutdown,int&)", ves_icall_System_Net_Sockets_Socket_Shutdown_internal)
ICALL(SOCK_20, "Socket_internal(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType,int&)", ves_icall_System_Net_Sockets_Socket_Socket_internal)
ICALL(SOCK_21, "WSAIoctl(intptr,int,byte[],byte[],int&)", ves_icall_System_Net_Sockets_Socket_WSAIoctl)
ICALL(SOCK_21a, "cancel_blocking_socket_operation", icall_cancel_blocking_socket_operation)
ICALL(SOCK_22, "socket_pool_queue", icall_append_io_job)

ICALL_TYPE(SOCKEX, "System.Net.Sockets.SocketException", SOCKEX_1)
ICALL(SOCKEX_1, "WSAGetLastError_internal", ves_icall_System_Net_Sockets_SocketException_WSAGetLastError_internal)
#endif /* !DISABLE_SOCKETS */

ICALL_TYPE(NUMBER_FORMATTER, "System.NumberFormatter", NUMBER_FORMATTER_1)
ICALL(NUMBER_FORMATTER_1, "GetFormatterTables", ves_icall_System_NumberFormatter_GetFormatterTables)

ICALL_TYPE(OBJ, "System.Object", OBJ_1)
ICALL(OBJ_1, "GetType", ves_icall_System_Object_GetType)
ICALL(OBJ_2, "InternalGetHashCode", mono_object_hash)
ICALL(OBJ_3, "MemberwiseClone", ves_icall_System_Object_MemberwiseClone)
ICALL(OBJ_4, "obj_address", ves_icall_System_Object_obj_address)

ICALL_TYPE(ASSEM, "System.Reflection.Assembly", ASSEM_1)
ICALL(ASSEM_1, "FillName", ves_icall_System_Reflection_Assembly_FillName)
ICALL(ASSEM_2, "GetCallingAssembly", ves_icall_System_Reflection_Assembly_GetCallingAssembly)
ICALL(ASSEM_3, "GetEntryAssembly", ves_icall_System_Reflection_Assembly_GetEntryAssembly)
ICALL(ASSEM_4, "GetExecutingAssembly", ves_icall_System_Reflection_Assembly_GetExecutingAssembly)
ICALL(ASSEM_5, "GetFilesInternal", ves_icall_System_Reflection_Assembly_GetFilesInternal)
ICALL(ASSEM_6, "GetManifestModuleInternal", ves_icall_System_Reflection_Assembly_GetManifestModuleInternal)
ICALL(ASSEM_7, "GetManifestResourceInfoInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInfoInternal)
ICALL(ASSEM_8, "GetManifestResourceInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInternal)
ICALL(ASSEM_9, "GetManifestResourceNames", ves_icall_System_Reflection_Assembly_GetManifestResourceNames)
ICALL(ASSEM_10, "GetModulesInternal", ves_icall_System_Reflection_Assembly_GetModulesInternal)
//ICALL(ASSEM_11, "GetNamespaces", ves_icall_System_Reflection_Assembly_GetNamespaces)
ICALL(ASSEM_12, "GetReferencedAssemblies", ves_icall_System_Reflection_Assembly_GetReferencedAssemblies)
ICALL(ASSEM_13, "GetTypes", ves_icall_System_Reflection_Assembly_GetTypes)
ICALL(ASSEM_14, "InternalGetAssemblyName", ves_icall_System_Reflection_Assembly_InternalGetAssemblyName)
ICALL(ASSEM_15, "InternalGetType", ves_icall_System_Reflection_Assembly_InternalGetType)
ICALL(ASSEM_16, "InternalImageRuntimeVersion", ves_icall_System_Reflection_Assembly_InternalImageRuntimeVersion)
ICALL(ASSEM_17, "LoadFrom", ves_icall_System_Reflection_Assembly_LoadFrom)
ICALL(ASSEM_18, "LoadPermissions", ves_icall_System_Reflection_Assembly_LoadPermissions)
	/*
	 * Private icalls for the Mono Debugger
	 */
ICALL(ASSEM_19, "MonoDebugger_GetMethodToken", ves_icall_MonoDebugger_GetMethodToken)

	/* normal icalls again */
ICALL(ASSEM_20, "get_EntryPoint", ves_icall_System_Reflection_Assembly_get_EntryPoint)
ICALL(ASSEM_21, "get_ReflectionOnly", ves_icall_System_Reflection_Assembly_get_ReflectionOnly)
ICALL(ASSEM_22, "get_code_base", ves_icall_System_Reflection_Assembly_get_code_base)
ICALL(ASSEM_23, "get_fullname", ves_icall_System_Reflection_Assembly_get_fullName)
ICALL(ASSEM_24, "get_global_assembly_cache", ves_icall_System_Reflection_Assembly_get_global_assembly_cache)
ICALL(ASSEM_25, "get_location", ves_icall_System_Reflection_Assembly_get_location)
ICALL(ASSEM_26, "load_with_partial_name", ves_icall_System_Reflection_Assembly_load_with_partial_name)

ICALL_TYPE(ASSEMN, "System.Reflection.AssemblyName", ASSEMN_1)
ICALL(ASSEMN_1, "ParseName", ves_icall_System_Reflection_AssemblyName_ParseName)
ICALL(ASSEMN_2, "get_public_token", mono_digest_get_public_token)

ICALL_TYPE(CATTR_DATA, "System.Reflection.CustomAttributeData", CATTR_DATA_1)
ICALL(CATTR_DATA_1, "ResolveArgumentsInternal", mono_reflection_resolve_custom_attribute_data)

ICALL_TYPE(ASSEMB, "System.Reflection.Emit.AssemblyBuilder", ASSEMB_1)
ICALL(ASSEMB_1, "InternalAddModule", mono_image_load_module_dynamic)
ICALL(ASSEMB_2, "basic_init", mono_image_basic_init)

ICALL_TYPE(CATTRB, "System.Reflection.Emit.CustomAttributeBuilder", CATTRB_1)
ICALL(CATTRB_1, "GetBlob", mono_reflection_get_custom_attrs_blob)

#ifndef DISABLE_REFLECTION_EMIT
ICALL_TYPE(DERIVEDTYPE, "System.Reflection.Emit.DerivedType", DERIVEDTYPE_1)
ICALL(DERIVEDTYPE_1, "create_unmanaged_type", mono_reflection_create_unmanaged_type)
#endif

ICALL_TYPE(DYNM, "System.Reflection.Emit.DynamicMethod", DYNM_1)
ICALL(DYNM_1, "create_dynamic_method", mono_reflection_create_dynamic_method)

ICALL_TYPE(ENUMB, "System.Reflection.Emit.EnumBuilder", ENUMB_1)
ICALL(ENUMB_1, "setup_enum_type", ves_icall_EnumBuilder_setup_enum_type)

ICALL_TYPE(GPARB, "System.Reflection.Emit.GenericTypeParameterBuilder", GPARB_1)
ICALL(GPARB_1, "initialize", mono_reflection_initialize_generic_parameter)

ICALL_TYPE(METHODB, "System.Reflection.Emit.MethodBuilder", METHODB_1)
ICALL(METHODB_1, "MakeGenericMethod", mono_reflection_bind_generic_method_parameters)

ICALL_TYPE(MODULEB, "System.Reflection.Emit.ModuleBuilder", MODULEB_8)
ICALL(MODULEB_8, "RegisterToken", ves_icall_ModuleBuilder_RegisterToken)
ICALL(MODULEB_1, "WriteToFile", ves_icall_ModuleBuilder_WriteToFile)
ICALL(MODULEB_2, "basic_init", mono_image_module_basic_init)
ICALL(MODULEB_3, "build_metadata", ves_icall_ModuleBuilder_build_metadata)
ICALL(MODULEB_4, "create_modified_type", ves_icall_ModuleBuilder_create_modified_type)
ICALL(MODULEB_5, "getMethodToken", ves_icall_ModuleBuilder_getMethodToken)
ICALL(MODULEB_6, "getToken", ves_icall_ModuleBuilder_getToken)
ICALL(MODULEB_7, "getUSIndex", mono_image_insert_string)
ICALL(MODULEB_9, "set_wrappers_type", mono_image_set_wrappers_type)

ICALL_TYPE(SIGH, "System.Reflection.Emit.SignatureHelper", SIGH_1)
ICALL(SIGH_1, "get_signature_field", mono_reflection_sighelper_get_signature_field)
ICALL(SIGH_2, "get_signature_local", mono_reflection_sighelper_get_signature_local)

ICALL_TYPE(TYPEB, "System.Reflection.Emit.TypeBuilder", TYPEB_1)
ICALL(TYPEB_1, "create_generic_class", mono_reflection_create_generic_class)
ICALL(TYPEB_2, "create_internal_class", mono_reflection_create_internal_class)
ICALL(TYPEB_3, "create_runtime_class", mono_reflection_create_runtime_class)
ICALL(TYPEB_4, "get_IsGenericParameter", ves_icall_TypeBuilder_get_IsGenericParameter)
ICALL(TYPEB_5, "get_event_info", mono_reflection_event_builder_get_event_info)
ICALL(TYPEB_6, "setup_generic_class", mono_reflection_setup_generic_class)
ICALL(TYPEB_7, "setup_internal_class", mono_reflection_setup_internal_class)

ICALL_TYPE(FIELDI, "System.Reflection.FieldInfo", FILEDI_1)
ICALL(FILEDI_1, "GetTypeModifiers", ves_icall_System_Reflection_FieldInfo_GetTypeModifiers)
ICALL(FILEDI_2, "get_marshal_info", ves_icall_System_Reflection_FieldInfo_get_marshal_info)
ICALL(FILEDI_3, "internal_from_handle_type", ves_icall_System_Reflection_FieldInfo_internal_from_handle_type)

ICALL_TYPE(MEMBERI, "System.Reflection.MemberInfo", MEMBERI_1)
ICALL(MEMBERI_1, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MBASE, "System.Reflection.MethodBase", MBASE_1)
ICALL(MBASE_1, "GetCurrentMethod", ves_icall_GetCurrentMethod)
ICALL(MBASE_2, "GetMethodBodyInternal", ves_icall_System_Reflection_MethodBase_GetMethodBodyInternal)
ICALL(MBASE_3, "GetMethodFromHandleInternal", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternal)
ICALL(MBASE_4, "GetMethodFromHandleInternalType", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternalType)

ICALL_TYPE(MODULE, "System.Reflection.Module", MODULE_1)
ICALL(MODULE_1, "Close", ves_icall_System_Reflection_Module_Close)
ICALL(MODULE_2, "GetGlobalType", ves_icall_System_Reflection_Module_GetGlobalType)
ICALL(MODULE_3, "GetGuidInternal", ves_icall_System_Reflection_Module_GetGuidInternal)
ICALL(MODULE_14, "GetHINSTANCE", ves_icall_System_Reflection_Module_GetHINSTANCE)
ICALL(MODULE_4, "GetMDStreamVersion", ves_icall_System_Reflection_Module_GetMDStreamVersion)
ICALL(MODULE_5, "GetPEKind", ves_icall_System_Reflection_Module_GetPEKind)
ICALL(MODULE_6, "InternalGetTypes", ves_icall_System_Reflection_Module_InternalGetTypes)
ICALL(MODULE_7, "ResolveFieldToken", ves_icall_System_Reflection_Module_ResolveFieldToken)
ICALL(MODULE_8, "ResolveMemberToken", ves_icall_System_Reflection_Module_ResolveMemberToken)
ICALL(MODULE_9, "ResolveMethodToken", ves_icall_System_Reflection_Module_ResolveMethodToken)
ICALL(MODULE_10, "ResolveSignature", ves_icall_System_Reflection_Module_ResolveSignature)
ICALL(MODULE_11, "ResolveStringToken", ves_icall_System_Reflection_Module_ResolveStringToken)
ICALL(MODULE_12, "ResolveTypeToken", ves_icall_System_Reflection_Module_ResolveTypeToken)
ICALL(MODULE_13, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MCMETH, "System.Reflection.MonoCMethod", MCMETH_1)
ICALL(MCMETH_1, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MCMETH_2, "InternalInvoke", ves_icall_InternalInvoke)

ICALL_TYPE(MEVIN, "System.Reflection.MonoEventInfo", MEVIN_1)
ICALL(MEVIN_1, "get_event_info", ves_icall_get_event_info)

ICALL_TYPE(MFIELD, "System.Reflection.MonoField", MFIELD_1)
ICALL(MFIELD_1, "GetFieldOffset", ves_icall_MonoField_GetFieldOffset)
ICALL(MFIELD_2, "GetParentType", ves_icall_MonoField_GetParentType)
ICALL(MFIELD_5, "GetRawConstantValue", ves_icall_MonoField_GetRawConstantValue)
ICALL(MFIELD_3, "GetValueInternal", ves_icall_MonoField_GetValueInternal)
ICALL(MFIELD_6, "ResolveType", ves_icall_MonoField_ResolveType)
ICALL(MFIELD_4, "SetValueInternal", ves_icall_MonoField_SetValueInternal)

ICALL_TYPE(MGENCM, "System.Reflection.MonoGenericCMethod", MGENCM_1)
ICALL(MGENCM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MGENCL, "System.Reflection.MonoGenericClass", MGENCL_5)
ICALL(MGENCL_5, "initialize", mono_reflection_generic_class_initialize)
ICALL(MGENCL_6, "register_with_runtime", mono_reflection_register_with_runtime)

/* note this is the same as above: unify */
ICALL_TYPE(MGENM, "System.Reflection.MonoGenericMethod", MGENM_1)
ICALL(MGENM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MMETH, "System.Reflection.MonoMethod", MMETH_1)
ICALL(MMETH_1, "GetDllImportAttribute", ves_icall_MonoMethod_GetDllImportAttribute)
ICALL(MMETH_2, "GetGenericArguments", ves_icall_MonoMethod_GetGenericArguments)
ICALL(MMETH_3, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MMETH_4, "InternalInvoke", ves_icall_InternalInvoke)
ICALL(MMETH_5, "MakeGenericMethod_impl", mono_reflection_bind_generic_method_parameters)
ICALL(MMETH_6, "get_IsGenericMethod", ves_icall_MonoMethod_get_IsGenericMethod)
ICALL(MMETH_7, "get_IsGenericMethodDefinition", ves_icall_MonoMethod_get_IsGenericMethodDefinition)
ICALL(MMETH_8, "get_base_method", ves_icall_MonoMethod_get_base_method)
ICALL(MMETH_9, "get_name", ves_icall_MonoMethod_get_name)

ICALL_TYPE(MMETHI, "System.Reflection.MonoMethodInfo", MMETHI_4)
ICALL(MMETHI_4, "get_method_attributes", vell_icall_get_method_attributes)
ICALL(MMETHI_1, "get_method_info", ves_icall_get_method_info)
ICALL(MMETHI_2, "get_parameter_info", ves_icall_get_parameter_info)
ICALL(MMETHI_3, "get_retval_marshal", ves_icall_System_MonoMethodInfo_get_retval_marshal)

ICALL_TYPE(MPROPI, "System.Reflection.MonoPropertyInfo", MPROPI_1)
ICALL(MPROPI_1, "GetTypeModifiers", property_info_get_type_modifiers)
ICALL(MPROPI_3, "get_default_value", property_info_get_default_value)
ICALL(MPROPI_2, "get_property_info", ves_icall_get_property_info)

ICALL_TYPE(PARAMI, "System.Reflection.ParameterInfo", PARAMI_1)
ICALL(PARAMI_1, "GetMetadataToken", mono_reflection_get_token)
ICALL(PARAMI_2, "GetTypeModifiers", param_info_get_type_modifiers)

ICALL_TYPE(RUNH, "System.Runtime.CompilerServices.RuntimeHelpers", RUNH_1)
ICALL(RUNH_1, "GetObjectValue", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetObjectValue)
	 /* REMOVEME: no longer needed, just so we dont break things when not needed */
ICALL(RUNH_2, "GetOffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)
ICALL(RUNH_3, "InitializeArray", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_InitializeArray)
ICALL(RUNH_4, "RunClassConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunClassConstructor)
ICALL(RUNH_5, "RunModuleConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunModuleConstructor)
ICALL(RUNH_5h, "SufficientExecutionStack", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_SufficientExecutionStack)
ICALL(RUNH_6, "get_OffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)

ICALL_TYPE(GCH, "System.Runtime.InteropServices.GCHandle", GCH_1)
ICALL(GCH_1, "CheckCurrentDomain", GCHandle_CheckCurrentDomain)
ICALL(GCH_2, "FreeHandle", ves_icall_System_GCHandle_FreeHandle)
ICALL(GCH_3, "GetAddrOfPinnedObject", ves_icall_System_GCHandle_GetAddrOfPinnedObject)
ICALL(GCH_4, "GetTarget", ves_icall_System_GCHandle_GetTarget)
ICALL(GCH_5, "GetTargetHandle", ves_icall_System_GCHandle_GetTargetHandle)

#ifndef DISABLE_COM
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_1)
ICALL(MARSHAL_1, "AddRefInternal", ves_icall_System_Runtime_InteropServices_Marshal_AddRefInternal)
#else
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_2)
#endif
ICALL(MARSHAL_2, "AllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_AllocCoTaskMem)
ICALL(MARSHAL_3, "AllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_AllocHGlobal)
ICALL(MARSHAL_4, "DestroyStructure", ves_icall_System_Runtime_InteropServices_Marshal_DestroyStructure)
ICALL(MARSHAL_5, "FreeBSTR", ves_icall_System_Runtime_InteropServices_Marshal_FreeBSTR)
ICALL(MARSHAL_6, "FreeCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_FreeCoTaskMem)
ICALL(MARSHAL_7, "FreeHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_FreeHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_44, "GetCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetCCW)
ICALL(MARSHAL_8, "GetComSlotForMethodInfoInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetComSlotForMethodInfoInternal)
#endif
ICALL(MARSHAL_9, "GetDelegateForFunctionPointerInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetDelegateForFunctionPointerInternal)
ICALL(MARSHAL_10, "GetFunctionPointerForDelegateInternal", mono_delegate_to_ftnptr)
#ifndef DISABLE_COM
ICALL(MARSHAL_45, "GetIDispatchForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIDispatchForObjectInternal)
ICALL(MARSHAL_46, "GetIUnknownForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIUnknownForObjectInternal)
#endif
ICALL(MARSHAL_11, "GetLastWin32Error", ves_icall_System_Runtime_InteropServices_Marshal_GetLastWin32Error)
#ifndef DISABLE_COM
ICALL(MARSHAL_47, "GetObjectForCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetObjectForCCW)
ICALL(MARSHAL_48, "IsComObject", ves_icall_System_Runtime_InteropServices_Marshal_IsComObject)
#endif
ICALL(MARSHAL_12, "OffsetOf", ves_icall_System_Runtime_InteropServices_Marshal_OffsetOf)
ICALL(MARSHAL_13, "Prelink", ves_icall_System_Runtime_InteropServices_Marshal_Prelink)
ICALL(MARSHAL_14, "PrelinkAll", ves_icall_System_Runtime_InteropServices_Marshal_PrelinkAll)
ICALL(MARSHAL_15, "PtrToStringAnsi(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi)
ICALL(MARSHAL_16, "PtrToStringAnsi(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi_len)
#ifndef DISABLE_COM
ICALL(MARSHAL_17, "PtrToStringBSTR", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringBSTR)
#endif
ICALL(MARSHAL_18, "PtrToStringUni(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni)
ICALL(MARSHAL_19, "PtrToStringUni(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni_len)
ICALL(MARSHAL_20, "PtrToStructure(intptr,System.Type)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure_type)
ICALL(MARSHAL_21, "PtrToStructure(intptr,object)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure)
#ifndef DISABLE_COM
ICALL(MARSHAL_22, "QueryInterfaceInternal", ves_icall_System_Runtime_InteropServices_Marshal_QueryInterfaceInternal)
#endif
ICALL(MARSHAL_43, "ReAllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocCoTaskMem)
ICALL(MARSHAL_23, "ReAllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_49, "ReleaseComObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseComObjectInternal)
ICALL(MARSHAL_29, "ReleaseInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseInternal)
#endif
ICALL(MARSHAL_30, "SizeOf", ves_icall_System_Runtime_InteropServices_Marshal_SizeOf)
ICALL(MARSHAL_31, "StringToBSTR", ves_icall_System_Runtime_InteropServices_Marshal_StringToBSTR)
ICALL(MARSHAL_32, "StringToHGlobalAnsi", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalAnsi)
ICALL(MARSHAL_33, "StringToHGlobalUni", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalUni)
ICALL(MARSHAL_34, "StructureToPtr", ves_icall_System_Runtime_InteropServices_Marshal_StructureToPtr)
ICALL(MARSHAL_35, "UnsafeAddrOfPinnedArrayElement", ves_icall_System_Runtime_InteropServices_Marshal_UnsafeAddrOfPinnedArrayElement)
ICALL(MARSHAL_41, "copy_from_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_from_unmanaged)
ICALL(MARSHAL_42, "copy_to_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_to_unmanaged)

ICALL_TYPE(ACTS, "System.Runtime.Remoting.Activation.ActivationServices", ACTS_1)
ICALL(ACTS_1, "AllocateUninitializedClassInstance", ves_icall_System_Runtime_Activation_ActivationServices_AllocateUninitializedClassInstance)
ICALL(ACTS_2, "EnableProxyActivation", ves_icall_System_Runtime_Activation_ActivationServices_EnableProxyActivation)

ICALL_TYPE(MONOMM, "System.Runtime.Remoting.Messaging.MonoMethodMessage", MONOMM_1)
ICALL(MONOMM_1, "InitMessage", ves_icall_MonoMethodMessage_InitMessage)

#ifndef DISABLE_REMOTING
ICALL_TYPE(REALP, "System.Runtime.Remoting.Proxies.RealProxy", REALP_1)
ICALL(REALP_1, "InternalGetProxyType", ves_icall_Remoting_RealProxy_InternalGetProxyType)
ICALL(REALP_2, "InternalGetTransparentProxy", ves_icall_Remoting_RealProxy_GetTransparentProxy)

ICALL_TYPE(REMSER, "System.Runtime.Remoting.RemotingServices", REMSER_0)
ICALL(REMSER_0, "GetVirtualMethod", ves_icall_Remoting_RemotingServices_GetVirtualMethod)
ICALL(REMSER_1, "InternalExecute", ves_icall_InternalExecute)
ICALL(REMSER_2, "IsTransparentProxy", ves_icall_IsTransparentProxy)
#endif

ICALL_TYPE(MHAN, "System.RuntimeMethodHandle", MHAN_1)
ICALL(MHAN_1, "GetFunctionPointer", ves_icall_RuntimeMethod_GetFunctionPointer)

ICALL_TYPE(RNG, "System.Security.Cryptography.RNGCryptoServiceProvider", RNG_1)
ICALL(RNG_1, "RngClose", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngClose)
ICALL(RNG_2, "RngGetBytes", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngGetBytes)
ICALL(RNG_3, "RngInitialize", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngInitialize)
ICALL(RNG_4, "RngOpen", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngOpen)

#ifndef DISABLE_POLICY_EVIDENCE
ICALL_TYPE(EVID, "System.Security.Policy.Evidence", EVID_1)
ICALL(EVID_1, "IsAuthenticodePresent", ves_icall_System_Security_Policy_Evidence_IsAuthenticodePresent)

ICALL_TYPE(WINID, "System.Security.Principal.WindowsIdentity", WINID_1)
ICALL(WINID_1, "GetCurrentToken", ves_icall_System_Security_Principal_WindowsIdentity_GetCurrentToken)
ICALL(WINID_2, "GetTokenName", ves_icall_System_Security_Principal_WindowsIdentity_GetTokenName)
ICALL(WINID_3, "GetUserToken", ves_icall_System_Security_Principal_WindowsIdentity_GetUserToken)
ICALL(WINID_4, "_GetRoles", ves_icall_System_Security_Principal_WindowsIdentity_GetRoles)

ICALL_TYPE(WINIMP, "System.Security.Principal.WindowsImpersonationContext", WINIMP_1)
ICALL(WINIMP_1, "CloseToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_CloseToken)
ICALL(WINIMP_2, "DuplicateToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_DuplicateToken)
ICALL(WINIMP_3, "RevertToSelf", ves_icall_System_Security_Principal_WindowsImpersonationContext_RevertToSelf)
ICALL(WINIMP_4, "SetCurrentToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_SetCurrentToken)

ICALL_TYPE(WINPRIN, "System.Security.Principal.WindowsPrincipal", WINPRIN_1)
ICALL(WINPRIN_1, "IsMemberOfGroupId", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupId)
ICALL(WINPRIN_2, "IsMemberOfGroupName", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupName)

ICALL_TYPE(SECSTRING, "System.Security.SecureString", SECSTRING_1)
ICALL(SECSTRING_1, "DecryptInternal", ves_icall_System_Security_SecureString_DecryptInternal)
ICALL(SECSTRING_2, "EncryptInternal", ves_icall_System_Security_SecureString_EncryptInternal)
#endif /* !DISABLE_POLICY_EVIDENCE */

ICALL_TYPE(SECMAN, "System.Security.SecurityManager", SECMAN_1)
ICALL(SECMAN_1, "GetLinkDemandSecurity", ves_icall_System_Security_SecurityManager_GetLinkDemandSecurity)
ICALL(SECMAN_2, "get_CheckExecutionRights", ves_icall_System_Security_SecurityManager_get_CheckExecutionRights)
ICALL(SECMAN_3, "get_RequiresElevatedPermissions", mono_security_core_clr_require_elevated_permissions)
ICALL(SECMAN_4, "get_SecurityEnabled", ves_icall_System_Security_SecurityManager_get_SecurityEnabled)
ICALL(SECMAN_5, "set_CheckExecutionRights", ves_icall_System_Security_SecurityManager_set_CheckExecutionRights)
ICALL(SECMAN_6, "set_SecurityEnabled", ves_icall_System_Security_SecurityManager_set_SecurityEnabled)

ICALL_TYPE(STRING, "System.String", STRING_1)
ICALL(STRING_1, ".ctor(char*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_2, ".ctor(char*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_3, ".ctor(char,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_4, ".ctor(char[])", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_5, ".ctor(char[],int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_6, ".ctor(sbyte*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_7, ".ctor(sbyte*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8, ".ctor(sbyte*,int,int,System.Text.Encoding)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8a, "GetLOSLimit", ves_icall_System_String_GetLOSLimit)
ICALL(STRING_9, "InternalAllocateStr", ves_icall_System_String_InternalAllocateStr)
ICALL(STRING_10, "InternalIntern", ves_icall_System_String_InternalIntern)
ICALL(STRING_11, "InternalIsInterned", ves_icall_System_String_InternalIsInterned)

ICALL_TYPE(TENC, "System.Text.Encoding", TENC_1)
ICALL(TENC_1, "InternalCodePage", ves_icall_System_Text_Encoding_InternalCodePage)

ICALL_TYPE(ILOCK, "System.Threading.Interlocked", ILOCK_1)
ICALL(ILOCK_1, "Add(int&,int)", ves_icall_System_Threading_Interlocked_Add_Int)
ICALL(ILOCK_2, "Add(long&,long)", ves_icall_System_Threading_Interlocked_Add_Long)
ICALL(ILOCK_3, "CompareExchange(T&,T,T)", ves_icall_System_Threading_Interlocked_CompareExchange_T)
ICALL(ILOCK_4, "CompareExchange(double&,double,double)", ves_icall_System_Threading_Interlocked_CompareExchange_Double)
ICALL(ILOCK_5, "CompareExchange(int&,int,int)", ves_icall_System_Threading_Interlocked_CompareExchange_Int)
ICALL(ILOCK_6, "CompareExchange(intptr&,intptr,intptr)", ves_icall_System_Threading_Interlocked_CompareExchange_IntPtr)
ICALL(ILOCK_7, "CompareExchange(long&,long,long)", ves_icall_System_Threading_Interlocked_CompareExchange_Long)
ICALL(ILOCK_8, "CompareExchange(object&,object,object)", ves_icall_System_Threading_Interlocked_CompareExchange_Object)
ICALL(ILOCK_9, "CompareExchange(single&,single,single)", ves_icall_System_Threading_Interlocked_CompareExchange_Single)
ICALL(ILOCK_10, "Decrement(int&)", ves_icall_System_Threading_Interlocked_Decrement_Int)
ICALL(ILOCK_11, "Decrement(long&)", ves_icall_System_Threading_Interlocked_Decrement_Long)
ICALL(ILOCK_12, "Exchange(T&,T)", ves_icall_System_Threading_Interlocked_Exchange_T)
ICALL(ILOCK_13, "Exchange(double&,double)", ves_icall_System_Threading_Interlocked_Exchange_Double)
ICALL(ILOCK_14, "Exchange(int&,int)", ves_icall_System_Threading_Interlocked_Exchange_Int)
ICALL(ILOCK_15, "Exchange(intptr&,intptr)", ves_icall_System_Threading_Interlocked_Exchange_IntPtr)
ICALL(ILOCK_16, "Exchange(long&,long)", ves_icall_System_Threading_Interlocked_Exchange_Long)
ICALL(ILOCK_17, "Exchange(object&,object)", ves_icall_System_Threading_Interlocked_Exchange_Object)
ICALL(ILOCK_18, "Exchange(single&,single)", ves_icall_System_Threading_Interlocked_Exchange_Single)
ICALL(ILOCK_19, "Increment(int&)", ves_icall_System_Threading_Interlocked_Increment_Int)
ICALL(ILOCK_20, "Increment(long&)", ves_icall_System_Threading_Interlocked_Increment_Long)
ICALL(ILOCK_21, "Read(long&)", ves_icall_System_Threading_Interlocked_Read_Long)

ICALL_TYPE(ITHREAD, "System.Threading.InternalThread", ITHREAD_1)
ICALL(ITHREAD_1, "Thread_free_internal", ves_icall_System_Threading_InternalThread_Thread_free_internal)

ICALL_TYPE(MONIT, "System.Threading.Monitor", MONIT_8)
ICALL(MONIT_8, "Enter", mono_monitor_enter)
ICALL(MONIT_1, "Exit", mono_monitor_exit)
ICALL(MONIT_2, "Monitor_pulse", ves_icall_System_Threading_Monitor_Monitor_pulse)
ICALL(MONIT_3, "Monitor_pulse_all", ves_icall_System_Threading_Monitor_Monitor_pulse_all)
ICALL(MONIT_4, "Monitor_test_owner", ves_icall_System_Threading_Monitor_Monitor_test_owner)
ICALL(MONIT_5, "Monitor_test_synchronised", ves_icall_System_Threading_Monitor_Monitor_test_synchronised)
ICALL(MONIT_6, "Monitor_try_enter", ves_icall_System_Threading_Monitor_Monitor_try_enter)
ICALL(MONIT_7, "Monitor_wait", ves_icall_System_Threading_Monitor_Monitor_wait)
ICALL(MONIT_9, "try_enter_with_atomic_var", ves_icall_System_Threading_Monitor_Monitor_try_enter_with_atomic_var)

ICALL_TYPE(MUTEX, "System.Threading.Mutex", MUTEX_1)
ICALL(MUTEX_1, "CreateMutex_internal(bool,string,bool&)", ves_icall_System_Threading_Mutex_CreateMutex_internal)
ICALL(MUTEX_2, "OpenMutex_internal(string,System.Security.AccessControl.MutexRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Mutex_OpenMutex_internal)
ICALL(MUTEX_3, "ReleaseMutex_internal(intptr)", ves_icall_System_Threading_Mutex_ReleaseMutex_internal)

ICALL_TYPE(NATIVEC, "System.Threading.NativeEventCalls", NATIVEC_1)
ICALL(NATIVEC_1, "CloseEvent_internal", ves_icall_System_Threading_Events_CloseEvent_internal)
ICALL(NATIVEC_2, "CreateEvent_internal(bool,bool,string,bool&)", ves_icall_System_Threading_Events_CreateEvent_internal)
ICALL(NATIVEC_3, "OpenEvent_internal(string,System.Security.AccessControl.EventWaitHandleRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Events_OpenEvent_internal)
ICALL(NATIVEC_4, "ResetEvent_internal",  ves_icall_System_Threading_Events_ResetEvent_internal)
ICALL(NATIVEC_5, "SetEvent_internal",    ves_icall_System_Threading_Events_SetEvent_internal)

ICALL_TYPE(SEMA, "System.Threading.Semaphore", SEMA_1)
ICALL(SEMA_1, "CreateSemaphore_internal(int,int,string,bool&)", ves_icall_System_Threading_Semaphore_CreateSemaphore_internal)
ICALL(SEMA_2, "OpenSemaphore_internal(string,System.Security.AccessControl.SemaphoreRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Semaphore_OpenSemaphore_internal)
ICALL(SEMA_3, "ReleaseSemaphore_internal(intptr,int,bool&)", ves_icall_System_Threading_Semaphore_ReleaseSemaphore_internal)

ICALL_TYPE(THREAD, "System.Threading.Thread", THREAD_1)
ICALL(THREAD_1, "Abort_internal(System.Threading.InternalThread,object)", ves_icall_System_Threading_Thread_Abort)
ICALL(THREAD_1aa, "AllocTlsData", mono_thread_alloc_tls)
ICALL(THREAD_1a, "ByteArrayToCurrentDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToCurrentDomain)
ICALL(THREAD_1b, "ByteArrayToRootDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToRootDomain)
ICALL(THREAD_2, "ClrState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_ClrState)
ICALL(THREAD_2a, "ConstructInternalThread", ves_icall_System_Threading_Thread_ConstructInternalThread)
ICALL(THREAD_3, "CurrentInternalThread_internal", mono_thread_internal_current)
ICALL(THREAD_3a, "DestroyTlsData", mono_thread_destroy_tls)
ICALL(THREAD_4, "FreeLocalSlotValues", mono_thread_free_local_slot_values)
ICALL(THREAD_55, "GetAbortExceptionState", ves_icall_System_Threading_Thread_GetAbortExceptionState)
ICALL(THREAD_7, "GetDomainID", ves_icall_System_Threading_Thread_GetDomainID)
ICALL(THREAD_8, "GetName_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetName_internal)
ICALL(THREAD_11, "GetState(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetState)
ICALL(THREAD_53, "Interrupt_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Interrupt_internal)
ICALL(THREAD_12, "Join_internal(System.Threading.InternalThread,int,intptr)", ves_icall_System_Threading_Thread_Join_internal)
ICALL(THREAD_13, "MemoryBarrier", ves_icall_System_Threading_Thread_MemoryBarrier)
ICALL(THREAD_14, "ResetAbort_internal()", ves_icall_System_Threading_Thread_ResetAbort)
ICALL(THREAD_15, "Resume_internal()", ves_icall_System_Threading_Thread_Resume)
ICALL(THREAD_18, "SetName_internal(System.Threading.InternalThread,string)", ves_icall_System_Threading_Thread_SetName_internal)
ICALL(THREAD_21, "SetState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_SetState)
ICALL(THREAD_22, "Sleep_internal", ves_icall_System_Threading_Thread_Sleep_internal)
ICALL(THREAD_54, "SpinWait_nop", ves_icall_System_Threading_Thread_SpinWait_nop)
ICALL(THREAD_23, "Suspend_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Suspend)
ICALL(THREAD_25, "Thread_internal", ves_icall_System_Threading_Thread_Thread_internal)
ICALL(THREAD_26, "VolatileRead(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_27, "VolatileRead(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(THREAD_28, "VolatileRead(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_29, "VolatileRead(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_30, "VolatileRead(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_31, "VolatileRead(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_32, "VolatileRead(object&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_33, "VolatileRead(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_34, "VolatileRead(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(THREAD_35, "VolatileRead(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_36, "VolatileRead(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_37, "VolatileRead(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_38, "VolatileRead(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_39, "VolatileWrite(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_40, "VolatileWrite(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(THREAD_41, "VolatileWrite(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_42, "VolatileWrite(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_43, "VolatileWrite(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_44, "VolatileWrite(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_45, "VolatileWrite(object&,object)", ves_icall_System_Threading_Thread_VolatileWriteObject)
ICALL(THREAD_46, "VolatileWrite(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_47, "VolatileWrite(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(THREAD_48, "VolatileWrite(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_49, "VolatileWrite(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_50, "VolatileWrite(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_51, "VolatileWrite(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_9, "Yield", ves_icall_System_Threading_Thread_Yield)
ICALL(THREAD_52, "current_lcid()", ves_icall_System_Threading_Thread_current_lcid)

ICALL_TYPE(THREADP, "System.Threading.ThreadPool", THREADP_1)
ICALL(THREADP_1, "GetAvailableThreads", ves_icall_System_Threading_ThreadPool_GetAvailableThreads)
ICALL(THREADP_2, "GetMaxThreads", ves_icall_System_Threading_ThreadPool_GetMaxThreads)
ICALL(THREADP_3, "GetMinThreads", ves_icall_System_Threading_ThreadPool_GetMinThreads)
ICALL(THREADP_35, "SetMaxThreads", ves_icall_System_Threading_ThreadPool_SetMaxThreads)
ICALL(THREADP_4, "SetMinThreads", ves_icall_System_Threading_ThreadPool_SetMinThreads)
ICALL(THREADP_5, "pool_queue", icall_append_job)

ICALL_TYPE(VOLATILE, "System.Threading.Volatile", VOLATILE_28)
ICALL(VOLATILE_28, "Read(T&)", ves_icall_System_Threading_Volatile_Read_T)
ICALL(VOLATILE_1, "Read(bool&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_2, "Read(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_3, "Read(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(VOLATILE_4, "Read(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_5, "Read(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_6, "Read(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_7, "Read(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_8, "Read(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_9, "Read(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(VOLATILE_10, "Read(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_11, "Read(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_12, "Read(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_13, "Read(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_27, "Write(T&,T)", ves_icall_System_Threading_Volatile_Write_T)
ICALL(VOLATILE_14, "Write(bool&,bool)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_15, "Write(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_16, "Write(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(VOLATILE_17, "Write(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_18, "Write(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_19, "Write(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_20, "Write(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(VOLATILE_21, "Write(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_22, "Write(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(VOLATILE_23, "Write(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_24, "Write(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_25, "Write(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_26, "Write(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)

ICALL_TYPE(WAITH, "System.Threading.WaitHandle", WAITH_1)
ICALL(WAITH_1, "SignalAndWait_Internal", ves_icall_System_Threading_WaitHandle_SignalAndWait_Internal)
ICALL(WAITH_2, "WaitAll_internal", ves_icall_System_Threading_WaitHandle_WaitAll_internal)
ICALL(WAITH_3, "WaitAny_internal", ves_icall_System_Threading_WaitHandle_WaitAny_internal)
ICALL(WAITH_4, "WaitOne_internal", ves_icall_System_Threading_WaitHandle_WaitOne_internal)

ICALL_TYPE(TYPE, "System.Type", TYPE_1)
ICALL(TYPE_1, "EqualsInternal", ves_icall_System_Type_EqualsInternal)
ICALL(TYPE_2, "GetGenericParameterAttributes", ves_icall_Type_GetGenericParameterAttributes)
ICALL(TYPE_3, "GetGenericParameterConstraints_impl", ves_icall_Type_GetGenericParameterConstraints)
ICALL(TYPE_4, "GetGenericParameterPosition", ves_icall_Type_GetGenericParameterPosition)
ICALL(TYPE_5, "GetGenericTypeDefinition_impl", ves_icall_Type_GetGenericTypeDefinition_impl)
ICALL(TYPE_6, "GetInterfaceMapData", ves_icall_Type_GetInterfaceMapData)
ICALL(TYPE_7, "GetPacking", ves_icall_Type_GetPacking)
ICALL(TYPE_8, "GetTypeCode", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_9, "GetTypeCodeInternal", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_10, "IsArrayImpl", ves_icall_Type_IsArrayImpl)
ICALL(TYPE_11, "IsInstanceOfType", ves_icall_type_IsInstanceOfType)
ICALL(TYPE_12, "MakeGenericType", ves_icall_Type_MakeGenericType)
ICALL(TYPE_13, "MakePointerType", ves_icall_Type_MakePointerType)
ICALL(TYPE_14, "get_IsGenericInstance", ves_icall_Type_get_IsGenericInstance)
ICALL(TYPE_15, "get_IsGenericType", ves_icall_Type_get_IsGenericType)
ICALL(TYPE_16, "get_IsGenericTypeDefinition", ves_icall_Type_get_IsGenericTypeDefinition)
ICALL(TYPE_17, "internal_from_handle", ves_icall_type_from_handle)
ICALL(TYPE_18, "internal_from_name", ves_icall_type_from_name)
ICALL(TYPE_19, "make_array_type", ves_icall_Type_make_array_type)
ICALL(TYPE_20, "make_byref_type", ves_icall_Type_make_byref_type)
ICALL(TYPE_21, "type_is_assignable_from", ves_icall_type_is_assignable_from)
ICALL(TYPE_22, "type_is_subtype_of", ves_icall_type_is_subtype_of)

ICALL_TYPE(TYPEDR, "System.TypedReference", TYPEDR_1)
ICALL(TYPEDR_1, "ToObject",	mono_TypedReference_ToObject)
ICALL(TYPEDR_2, "ToObjectInternal",	mono_TypedReference_ToObjectInternal)

ICALL_TYPE(VALUET, "System.ValueType", VALUET_1)
ICALL(VALUET_1, "InternalEquals", ves_icall_System_ValueType_Equals)
ICALL(VALUET_2, "InternalGetHashCode", ves_icall_System_ValueType_InternalGetHashCode)

ICALL_TYPE(WEBIC, "System.Web.Util.ICalls", WEBIC_1)
ICALL(WEBIC_1, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(WEBIC_2, "GetMachineInstallDirectory", ves_icall_System_Web_Util_ICalls_get_machine_install_dir)
ICALL(WEBIC_3, "GetUnmanagedResourcesPtr", ves_icall_get_resources_ptr)

#ifndef DISABLE_COM
ICALL_TYPE(COMOBJ, "System.__ComObject", COMOBJ_1)
ICALL(COMOBJ_1, "CreateRCW", ves_icall_System_ComObject_CreateRCW)
ICALL(COMOBJ_2, "GetInterfaceInternal", ves_icall_System_ComObject_GetInterfaceInternal)
ICALL(COMOBJ_3, "ReleaseInterfaces", ves_icall_System_ComObject_ReleaseInterfaces)
#endif
	NULL
};

#define icall_type_name_get(id) (icall_type_names [(id)])

#undef ICALL_TYPE
#undef ICALL
#define ICALL_TYPE(id,name,first)
#define ICALL(id,name,func) name,
static const char* const
icall_names [] = {
// #include "metadata/icall-def.h"
ICALL_TYPE(UNORM, "Mono.Globalization.Unicode.Normalization", UNORM_1)
ICALL(UNORM_1, "load_normalization_resource", load_normalization_resource)

#ifndef DISABLE_COM
ICALL_TYPE(COMPROX, "Mono.Interop.ComInteropProxy", COMPROX_1)
ICALL(COMPROX_1, "AddProxy", ves_icall_Mono_Interop_ComInteropProxy_AddProxy)
ICALL(COMPROX_2, "FindProxy", ves_icall_Mono_Interop_ComInteropProxy_FindProxy)
#endif

ICALL_TYPE(RUNTIME, "Mono.Runtime", RUNTIME_1)
ICALL(RUNTIME_1, "GetDisplayName", ves_icall_Mono_Runtime_GetDisplayName)
ICALL(RUNTIME_12, "GetNativeStackTrace", ves_icall_Mono_Runtime_GetNativeStackTrace)
ICALL(RUNTIME_13, "SetGCAllowSynchronousMajor", ves_icall_Mono_Runtime_SetGCAllowSynchronousMajor)

#ifndef PLATFORM_RO_FS
ICALL_TYPE(KPAIR, "Mono.Security.Cryptography.KeyPairPersistence", KPAIR_1)
ICALL(KPAIR_1, "_CanSecure", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_CanSecure)
ICALL(KPAIR_2, "_IsMachineProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsMachineProtected)
ICALL(KPAIR_3, "_IsUserProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsUserProtected)
ICALL(KPAIR_4, "_ProtectMachine", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectMachine)
ICALL(KPAIR_5, "_ProtectUser", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectUser)
#endif /* !PLATFORM_RO_FS */

ICALL_TYPE(ACTIV, "System.Activator", ACTIV_1)
ICALL(ACTIV_1, "CreateInstanceInternal", ves_icall_System_Activator_CreateInstanceInternal)

ICALL_TYPE(APPDOM, "System.AppDomain", APPDOM_1)
ICALL(APPDOM_1, "ExecuteAssembly", ves_icall_System_AppDomain_ExecuteAssembly)
ICALL(APPDOM_2, "GetAssemblies", ves_icall_System_AppDomain_GetAssemblies)
ICALL(APPDOM_3, "GetData", ves_icall_System_AppDomain_GetData)
ICALL(APPDOM_4, "InternalGetContext", ves_icall_System_AppDomain_InternalGetContext)
ICALL(APPDOM_5, "InternalGetDefaultContext", ves_icall_System_AppDomain_InternalGetDefaultContext)
ICALL(APPDOM_6, "InternalGetProcessGuid", ves_icall_System_AppDomain_InternalGetProcessGuid)
ICALL(APPDOM_7, "InternalIsFinalizingForUnload", ves_icall_System_AppDomain_InternalIsFinalizingForUnload)
ICALL(APPDOM_8, "InternalPopDomainRef", ves_icall_System_AppDomain_InternalPopDomainRef)
ICALL(APPDOM_9, "InternalPushDomainRef", ves_icall_System_AppDomain_InternalPushDomainRef)
ICALL(APPDOM_10, "InternalPushDomainRefByID", ves_icall_System_AppDomain_InternalPushDomainRefByID)
ICALL(APPDOM_11, "InternalSetContext", ves_icall_System_AppDomain_InternalSetContext)
ICALL(APPDOM_12, "InternalSetDomain", ves_icall_System_AppDomain_InternalSetDomain)
ICALL(APPDOM_13, "InternalSetDomainByID", ves_icall_System_AppDomain_InternalSetDomainByID)
ICALL(APPDOM_14, "InternalUnload", ves_icall_System_AppDomain_InternalUnload)
ICALL(APPDOM_15, "LoadAssembly", ves_icall_System_AppDomain_LoadAssembly)
ICALL(APPDOM_16, "LoadAssemblyRaw", ves_icall_System_AppDomain_LoadAssemblyRaw)
ICALL(APPDOM_17, "SetData", ves_icall_System_AppDomain_SetData)
ICALL(APPDOM_18, "createDomain", ves_icall_System_AppDomain_createDomain)
ICALL(APPDOM_19, "getCurDomain", ves_icall_System_AppDomain_getCurDomain)
ICALL(APPDOM_20, "getFriendlyName", ves_icall_System_AppDomain_getFriendlyName)
ICALL(APPDOM_21, "getRootDomain", ves_icall_System_AppDomain_getRootDomain)
ICALL(APPDOM_22, "getSetup", ves_icall_System_AppDomain_getSetup)

ICALL_TYPE(ARGI, "System.ArgIterator", ARGI_1)
ICALL(ARGI_1, "IntGetNextArg()",                  mono_ArgIterator_IntGetNextArg)
ICALL(ARGI_2, "IntGetNextArg(intptr)", mono_ArgIterator_IntGetNextArgT)
ICALL(ARGI_3, "IntGetNextArgType",                mono_ArgIterator_IntGetNextArgType)
ICALL(ARGI_4, "Setup",                            mono_ArgIterator_Setup)

ICALL_TYPE(ARRAY, "System.Array", ARRAY_1)
ICALL(ARRAY_1, "ClearInternal",    ves_icall_System_Array_ClearInternal)
ICALL(ARRAY_2, "Clone",            mono_array_clone)
ICALL(ARRAY_3, "CreateInstanceImpl",   ves_icall_System_Array_CreateInstanceImpl)
ICALL(ARRAY_14, "CreateInstanceImpl64",   ves_icall_System_Array_CreateInstanceImpl64)
ICALL(ARRAY_4, "FastCopy",         ves_icall_System_Array_FastCopy)
ICALL(ARRAY_5, "GetGenericValueImpl", ves_icall_System_Array_GetGenericValueImpl)
ICALL(ARRAY_6, "GetLength",        ves_icall_System_Array_GetLength)
ICALL(ARRAY_15, "GetLongLength",        ves_icall_System_Array_GetLongLength)
ICALL(ARRAY_7, "GetLowerBound",    ves_icall_System_Array_GetLowerBound)
ICALL(ARRAY_8, "GetRank",          ves_icall_System_Array_GetRank)
ICALL(ARRAY_9, "GetValue",         ves_icall_System_Array_GetValue)
ICALL(ARRAY_10, "GetValueImpl",     ves_icall_System_Array_GetValueImpl)
ICALL(ARRAY_11, "SetGenericValueImpl", ves_icall_System_Array_SetGenericValueImpl)
ICALL(ARRAY_12, "SetValue",         ves_icall_System_Array_SetValue)
ICALL(ARRAY_13, "SetValueImpl",     ves_icall_System_Array_SetValueImpl)

ICALL_TYPE(BUFFER, "System.Buffer", BUFFER_1)
ICALL(BUFFER_1, "BlockCopyInternal", ves_icall_System_Buffer_BlockCopyInternal)
ICALL(BUFFER_2, "ByteLengthInternal", ves_icall_System_Buffer_ByteLengthInternal)
ICALL(BUFFER_3, "GetByteInternal", ves_icall_System_Buffer_GetByteInternal)
ICALL(BUFFER_4, "SetByteInternal", ves_icall_System_Buffer_SetByteInternal)

ICALL_TYPE(CHAR, "System.Char", CHAR_1)
ICALL(CHAR_1, "GetDataTablePointers", ves_icall_System_Char_GetDataTablePointers)

ICALL_TYPE (COMPO_W, "System.ComponentModel.Win32Exception", COMPO_W_1)
ICALL (COMPO_W_1, "W32ErrorMessage", ves_icall_System_ComponentModel_Win32Exception_W32ErrorMessage)

ICALL_TYPE(DEFAULTC, "System.Configuration.DefaultConfig", DEFAULTC_1)
ICALL(DEFAULTC_1, "get_bundled_machine_config", get_bundled_machine_config)
ICALL(DEFAULTC_2, "get_machine_config_path", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)

/* Note that the below icall shares the same function as DefaultConfig uses */
ICALL_TYPE(INTCFGHOST, "System.Configuration.InternalConfigurationHost", INTCFGHOST_1)
ICALL(INTCFGHOST_1, "get_bundled_app_config", get_bundled_app_config)
ICALL(INTCFGHOST_2, "get_bundled_machine_config", get_bundled_machine_config)

ICALL_TYPE(CONSOLE, "System.ConsoleDriver", CONSOLE_1)
ICALL(CONSOLE_1, "InternalKeyAvailable", ves_icall_System_ConsoleDriver_InternalKeyAvailable )
ICALL(CONSOLE_2, "Isatty", ves_icall_System_ConsoleDriver_Isatty )
ICALL(CONSOLE_3, "SetBreak", ves_icall_System_ConsoleDriver_SetBreak )
ICALL(CONSOLE_4, "SetEcho", ves_icall_System_ConsoleDriver_SetEcho )
ICALL(CONSOLE_5, "TtySetup", ves_icall_System_ConsoleDriver_TtySetup )

ICALL_TYPE(CONVERT, "System.Convert", CONVERT_1)
ICALL(CONVERT_1, "InternalFromBase64CharArray", InternalFromBase64CharArray )
ICALL(CONVERT_2, "InternalFromBase64String", InternalFromBase64String )

ICALL_TYPE(TZONE, "System.CurrentSystemTimeZone", TZONE_1)
ICALL(TZONE_1, "GetTimeZoneData", ves_icall_System_CurrentSystemTimeZone_GetTimeZoneData)

ICALL_TYPE(DTIME, "System.DateTime", DTIME_1)
ICALL(DTIME_1, "GetNow", mono_100ns_datetime)
ICALL(DTIME_2, "GetTimeMonotonic", mono_100ns_ticks)

#ifndef DISABLE_DECIMAL
ICALL_TYPE(DECIMAL, "System.Decimal", DECIMAL_1)
ICALL(DECIMAL_1, "decimal2Int64", mono_decimal2Int64)
ICALL(DECIMAL_2, "decimal2UInt64", mono_decimal2UInt64)
ICALL(DECIMAL_3, "decimal2double", mono_decimal2double)
//ICALL(DECIMAL_4, "decimal2string", mono_decimal2string)
ICALL(DECIMAL_5, "decimalCompare", mono_decimalCompare)
ICALL(DECIMAL_6, "decimalDiv", mono_decimalDiv)
ICALL(DECIMAL_7, "decimalFloorAndTrunc", mono_decimalFloorAndTrunc)
ICALL(DECIMAL_8, "decimalIncr", mono_decimalIncr)
ICALL(DECIMAL_9, "decimalIntDiv", mono_decimalIntDiv)
ICALL(DECIMAL_10, "decimalMult", mono_decimalMult)
ICALL(DECIMAL_11, "decimalRound", mono_decimalRound)
ICALL(DECIMAL_12, "decimalSetExponent", mono_decimalSetExponent)
ICALL(DECIMAL_13, "double2decimal", mono_double2decimal) /* FIXME: wrong signature. */
ICALL(DECIMAL_14, "string2decimal", mono_string2decimal)
#endif

ICALL_TYPE(DELEGATE, "System.Delegate", DELEGATE_1)
ICALL(DELEGATE_1, "CreateDelegate_internal", ves_icall_System_Delegate_CreateDelegate_internal)
ICALL(DELEGATE_2, "SetMulticastInvoke", ves_icall_System_Delegate_SetMulticastInvoke)

ICALL_TYPE(DEBUGR, "System.Diagnostics.Debugger", DEBUGR_1)
ICALL(DEBUGR_1, "IsAttached_internal", ves_icall_System_Diagnostics_Debugger_IsAttached_internal)
ICALL(DEBUGR_2, "IsLogging", ves_icall_System_Diagnostics_Debugger_IsLogging)
ICALL(DEBUGR_3, "Log", ves_icall_System_Diagnostics_Debugger_Log)

ICALL_TYPE(TRACEL, "System.Diagnostics.DefaultTraceListener", TRACEL_1)
ICALL(TRACEL_1, "WriteWindowsDebugString", ves_icall_System_Diagnostics_DefaultTraceListener_WriteWindowsDebugString)

ICALL_TYPE(FILEV, "System.Diagnostics.FileVersionInfo", FILEV_1)
ICALL(FILEV_1, "GetVersionInfo_internal(string)", ves_icall_System_Diagnostics_FileVersionInfo_GetVersionInfo_internal)

#ifndef DISABLE_PROCESS_HANDLING
ICALL_TYPE(PERFCTR, "System.Diagnostics.PerformanceCounter", PERFCTR_1)
ICALL(PERFCTR_1, "FreeData", mono_perfcounter_free_data)
ICALL(PERFCTR_2, "GetImpl", mono_perfcounter_get_impl)
ICALL(PERFCTR_3, "GetSample", mono_perfcounter_get_sample)
ICALL(PERFCTR_4, "UpdateValue", mono_perfcounter_update_value)

ICALL_TYPE(PERFCTRCAT, "System.Diagnostics.PerformanceCounterCategory", PERFCTRCAT_1)
ICALL(PERFCTRCAT_1, "CategoryDelete", mono_perfcounter_category_del)
ICALL(PERFCTRCAT_2, "CategoryHelpInternal",   mono_perfcounter_category_help)
ICALL(PERFCTRCAT_3, "CounterCategoryExists", mono_perfcounter_category_exists)
ICALL(PERFCTRCAT_4, "Create",         mono_perfcounter_create)
ICALL(PERFCTRCAT_5, "GetCategoryNames", mono_perfcounter_category_names)
ICALL(PERFCTRCAT_6, "GetCounterNames", mono_perfcounter_counter_names)
ICALL(PERFCTRCAT_7, "GetInstanceNames", mono_perfcounter_instance_names)
ICALL(PERFCTRCAT_8, "InstanceExistsInternal", mono_perfcounter_instance_exists)

ICALL_TYPE(PROCESS, "System.Diagnostics.Process", PROCESS_1)
ICALL(PROCESS_1, "CreateProcess_internal(System.Diagnostics.ProcessStartInfo,intptr,intptr,intptr,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_CreateProcess_internal)
ICALL(PROCESS_2, "ExitCode_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitCode_internal)
ICALL(PROCESS_3, "ExitTime_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitTime_internal)
ICALL(PROCESS_4, "GetModules_internal(intptr)", ves_icall_System_Diagnostics_Process_GetModules_internal)
ICALL(PROCESS_5, "GetPid_internal()", ves_icall_System_Diagnostics_Process_GetPid_internal)
ICALL(PROCESS_5B, "GetPriorityClass(intptr,int&)", ves_icall_System_Diagnostics_Process_GetPriorityClass)
ICALL(PROCESS_5H, "GetProcessData", ves_icall_System_Diagnostics_Process_GetProcessData)
ICALL(PROCESS_6, "GetProcess_internal(int)", ves_icall_System_Diagnostics_Process_GetProcess_internal)
ICALL(PROCESS_7, "GetProcesses_internal()", ves_icall_System_Diagnostics_Process_GetProcesses_internal)
ICALL(PROCESS_8, "GetWorkingSet_internal(intptr,int&,int&)", ves_icall_System_Diagnostics_Process_GetWorkingSet_internal)
ICALL(PROCESS_9, "Kill_internal", ves_icall_System_Diagnostics_Process_Kill_internal)
ICALL(PROCESS_10, "ProcessName_internal(intptr)", ves_icall_System_Diagnostics_Process_ProcessName_internal)
ICALL(PROCESS_11, "Process_free_internal(intptr)", ves_icall_System_Diagnostics_Process_Process_free_internal)
ICALL(PROCESS_11B, "SetPriorityClass(intptr,int,int&)", ves_icall_System_Diagnostics_Process_SetPriorityClass)
ICALL(PROCESS_12, "SetWorkingSet_internal(intptr,int,int,bool)", ves_icall_System_Diagnostics_Process_SetWorkingSet_internal)
ICALL(PROCESS_13, "ShellExecuteEx_internal(System.Diagnostics.ProcessStartInfo,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_ShellExecuteEx_internal)
ICALL(PROCESS_14, "StartTime_internal(intptr)", ves_icall_System_Diagnostics_Process_StartTime_internal)
ICALL(PROCESS_14M, "Times", ves_icall_System_Diagnostics_Process_Times)
ICALL(PROCESS_15, "WaitForExit_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForExit_internal)
ICALL(PROCESS_16, "WaitForInputIdle_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForInputIdle_internal)

ICALL_TYPE (PROCESSHANDLE, "System.Diagnostics.Process/ProcessWaitHandle", PROCESSHANDLE_1)
ICALL (PROCESSHANDLE_1, "ProcessHandle_close(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_close)
ICALL (PROCESSHANDLE_2, "ProcessHandle_duplicate(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_duplicate)
#endif /* !DISABLE_PROCESS_HANDLING */

ICALL_TYPE(STOPWATCH, "System.Diagnostics.Stopwatch", STOPWATCH_1)
ICALL(STOPWATCH_1, "GetTimestamp", mono_100ns_ticks)

ICALL_TYPE(DOUBLE, "System.Double", DOUBLE_1)
ICALL(DOUBLE_1, "ParseImpl",    mono_double_ParseImpl)

ICALL_TYPE(ENUM, "System.Enum", ENUM_1)
ICALL(ENUM_1, "ToObject", ves_icall_System_Enum_ToObject)
ICALL(ENUM_5, "compare_value_to", ves_icall_System_Enum_compare_value_to)
ICALL(ENUM_4, "get_hashcode", ves_icall_System_Enum_get_hashcode)
ICALL(ENUM_3, "get_underlying_type", ves_icall_System_Enum_get_underlying_type)
ICALL(ENUM_2, "get_value", ves_icall_System_Enum_get_value)

ICALL_TYPE(ENV, "System.Environment", ENV_1)
ICALL(ENV_1, "Exit", ves_icall_System_Environment_Exit)
ICALL(ENV_2, "GetCommandLineArgs", mono_runtime_get_main_args)
ICALL(ENV_3, "GetEnvironmentVariableNames", ves_icall_System_Environment_GetEnvironmentVariableNames)
ICALL(ENV_4, "GetLogicalDrivesInternal", ves_icall_System_Environment_GetLogicalDrives )
ICALL(ENV_5, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(ENV_51, "GetNewLine", ves_icall_System_Environment_get_NewLine)
ICALL(ENV_6, "GetOSVersionString", ves_icall_System_Environment_GetOSVersionString)
ICALL(ENV_6a, "GetPageSize", mono_pagesize)
ICALL(ENV_7, "GetWindowsFolderPath", ves_icall_System_Environment_GetWindowsFolderPath)
ICALL(ENV_8, "InternalSetEnvironmentVariable", ves_icall_System_Environment_InternalSetEnvironmentVariable)
ICALL(ENV_9, "get_ExitCode", mono_environment_exitcode_get)
ICALL(ENV_10, "get_HasShutdownStarted", ves_icall_System_Environment_get_HasShutdownStarted)
ICALL(ENV_11, "get_MachineName", ves_icall_System_Environment_get_MachineName)
ICALL(ENV_13, "get_Platform", ves_icall_System_Environment_get_Platform)
ICALL(ENV_14, "get_ProcessorCount", mono_cpu_count)
ICALL(ENV_15, "get_TickCount", mono_msec_ticks)
ICALL(ENV_16, "get_UserName", ves_icall_System_Environment_get_UserName)
ICALL(ENV_16m, "internalBroadcastSettingChange", ves_icall_System_Environment_BroadcastSettingChange)
ICALL(ENV_17, "internalGetEnvironmentVariable", ves_icall_System_Environment_GetEnvironmentVariable)
ICALL(ENV_18, "internalGetGacPath", ves_icall_System_Environment_GetGacPath)
ICALL(ENV_19, "internalGetHome", ves_icall_System_Environment_InternalGetHome)
ICALL(ENV_20, "set_ExitCode", mono_environment_exitcode_set)

ICALL_TYPE(GC, "System.GC", GC_0)
ICALL(GC_0, "CollectionCount", mono_gc_collection_count)
ICALL(GC_0a, "GetGeneration", mono_gc_get_generation)
ICALL(GC_1, "GetTotalMemory", ves_icall_System_GC_GetTotalMemory)
ICALL(GC_2, "InternalCollect", ves_icall_System_GC_InternalCollect)
ICALL(GC_3, "KeepAlive", ves_icall_System_GC_KeepAlive)
ICALL(GC_4, "ReRegisterForFinalize", ves_icall_System_GC_ReRegisterForFinalize)
ICALL(GC_4a, "RecordPressure", mono_gc_add_memory_pressure)
ICALL(GC_5, "SuppressFinalize", ves_icall_System_GC_SuppressFinalize)
ICALL(GC_6, "WaitForPendingFinalizers", ves_icall_System_GC_WaitForPendingFinalizers)
ICALL(GC_7, "get_MaxGeneration", mono_gc_max_generation)
ICALL(GC_9, "get_ephemeron_tombstone", ves_icall_System_GC_get_ephemeron_tombstone)
ICALL(GC_8, "register_ephemeron_array", ves_icall_System_GC_register_ephemeron_array)

ICALL_TYPE(COMPINF, "System.Globalization.CompareInfo", COMPINF_1)
ICALL(COMPINF_1, "assign_sortkey(object,string,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_assign_sortkey)
ICALL(COMPINF_2, "construct_compareinfo(string)", ves_icall_System_Globalization_CompareInfo_construct_compareinfo)
ICALL(COMPINF_3, "free_internal_collator()", ves_icall_System_Globalization_CompareInfo_free_internal_collator)
ICALL(COMPINF_4, "internal_compare(string,int,int,string,int,int,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_internal_compare)
ICALL(COMPINF_5, "internal_index(string,int,int,char,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index_char)
ICALL(COMPINF_6, "internal_index(string,int,int,string,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index)

ICALL_TYPE(CULINF, "System.Globalization.CultureInfo", CULINF_2)
ICALL(CULINF_2, "construct_datetime_format", ves_icall_System_Globalization_CultureInfo_construct_datetime_format)
ICALL(CULINF_4, "construct_internal_locale_from_current_locale", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_current_locale)
ICALL(CULINF_5, "construct_internal_locale_from_lcid", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_lcid)
ICALL(CULINF_6, "construct_internal_locale_from_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_name)
ICALL(CULINF_7, "construct_internal_locale_from_specific_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_specific_name)
ICALL(CULINF_8, "construct_number_format", ves_icall_System_Globalization_CultureInfo_construct_number_format)
ICALL(CULINF_9, "internal_get_cultures", ves_icall_System_Globalization_CultureInfo_internal_get_cultures)
//ICALL(CULINF_10, "internal_is_lcid_neutral", ves_icall_System_Globalization_CultureInfo_internal_is_lcid_neutral)

ICALL_TYPE(REGINF, "System.Globalization.RegionInfo", REGINF_1)
ICALL(REGINF_1, "construct_internal_region_from_lcid", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_lcid)
ICALL(REGINF_2, "construct_internal_region_from_name", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_name)

#ifndef PLATFORM_NO_DRIVEINFO
ICALL_TYPE(IODRIVEINFO, "System.IO.DriveInfo", IODRIVEINFO_1)
ICALL(IODRIVEINFO_1, "GetDiskFreeSpaceInternal", ves_icall_System_IO_DriveInfo_GetDiskFreeSpace)
ICALL(IODRIVEINFO_2, "GetDriveFormat", ves_icall_System_IO_DriveInfo_GetDriveFormat)
ICALL(IODRIVEINFO_3, "GetDriveTypeInternal", ves_icall_System_IO_DriveInfo_GetDriveType)
#endif

ICALL_TYPE(FAMW, "System.IO.FAMWatcher", FAMW_1)
ICALL(FAMW_1, "InternalFAMNextEvent", ves_icall_System_IO_FAMW_InternalFAMNextEvent)

ICALL_TYPE(FILEW, "System.IO.FileSystemWatcher", FILEW_4)
ICALL(FILEW_4, "InternalSupportsFSW", ves_icall_System_IO_FSW_SupportsFSW)

ICALL_TYPE(INOW, "System.IO.InotifyWatcher", INOW_1)
ICALL(INOW_1, "AddWatch", ves_icall_System_IO_InotifyWatcher_AddWatch)
ICALL(INOW_2, "GetInotifyInstance", ves_icall_System_IO_InotifyWatcher_GetInotifyInstance)
ICALL(INOW_3, "RemoveWatch", ves_icall_System_IO_InotifyWatcher_RemoveWatch)

#if defined (TARGET_IOS) || defined (TARGET_ANDROID)
ICALL_TYPE(MMAPIMPL, "System.IO.MemoryMappedFiles.MemoryMapImpl", MMAPIMPL_1)
ICALL(MMAPIMPL_1, "mono_filesize_from_fd", mono_filesize_from_fd)
ICALL(MMAPIMPL_2, "mono_filesize_from_path", mono_filesize_from_path)
#endif


ICALL_TYPE(MONOIO, "System.IO.MonoIO", MONOIO_1)
ICALL(MONOIO_1, "Close(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Close)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_2, "CopyFile(string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CopyFile)
ICALL(MONOIO_3, "CreateDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CreateDirectory)
ICALL(MONOIO_4, "CreatePipe(intptr&,intptr&)", ves_icall_System_IO_MonoIO_CreatePipe)
ICALL(MONOIO_5, "DeleteFile(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_DeleteFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_34, "DuplicateHandle", ves_icall_System_IO_MonoIO_DuplicateHandle)
ICALL(MONOIO_37, "FindClose", ves_icall_System_IO_MonoIO_FindClose)
ICALL(MONOIO_35, "FindFirst", ves_icall_System_IO_MonoIO_FindFirst)
ICALL(MONOIO_36, "FindNext", ves_icall_System_IO_MonoIO_FindNext)
ICALL(MONOIO_6, "Flush(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Flush)
ICALL(MONOIO_7, "GetCurrentDirectory(System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetCurrentDirectory)
ICALL(MONOIO_8, "GetFileAttributes(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileAttributes)
ICALL(MONOIO_9, "GetFileStat(string,System.IO.MonoIOStat&,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileStat)
ICALL(MONOIO_10, "GetFileSystemEntries", ves_icall_System_IO_MonoIO_GetFileSystemEntries)
ICALL(MONOIO_11, "GetFileType(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileType)
ICALL(MONOIO_12, "GetLength(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_13, "GetTempPath(string&)", ves_icall_System_IO_MonoIO_GetTempPath)
ICALL(MONOIO_14, "Lock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Lock)
ICALL(MONOIO_15, "MoveFile(string,string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_MoveFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_16, "Open(string,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.IO.FileOptions,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Open)
ICALL(MONOIO_17, "Read(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Read)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_18, "RemoveDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_RemoveDirectory)
ICALL(MONOIO_18M, "ReplaceFile(string,string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_ReplaceFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_19, "Seek(intptr,long,System.IO.SeekOrigin,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Seek)
ICALL(MONOIO_20, "SetCurrentDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetCurrentDirectory)
ICALL(MONOIO_21, "SetFileAttributes(string,System.IO.FileAttributes,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileAttributes)
ICALL(MONOIO_22, "SetFileTime(intptr,long,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileTime)
ICALL(MONOIO_23, "SetLength(intptr,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_24, "Unlock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Unlock)
#endif
ICALL(MONOIO_25, "Write(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Write)
ICALL(MONOIO_26, "get_AltDirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_AltDirectorySeparatorChar)
ICALL(MONOIO_27, "get_ConsoleError", ves_icall_System_IO_MonoIO_get_ConsoleError)
ICALL(MONOIO_28, "get_ConsoleInput", ves_icall_System_IO_MonoIO_get_ConsoleInput)
ICALL(MONOIO_29, "get_ConsoleOutput", ves_icall_System_IO_MonoIO_get_ConsoleOutput)
ICALL(MONOIO_30, "get_DirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_DirectorySeparatorChar)
ICALL(MONOIO_31, "get_InvalidPathChars", ves_icall_System_IO_MonoIO_get_InvalidPathChars)
ICALL(MONOIO_32, "get_PathSeparator", ves_icall_System_IO_MonoIO_get_PathSeparator)
ICALL(MONOIO_33, "get_VolumeSeparatorChar", ves_icall_System_IO_MonoIO_get_VolumeSeparatorChar)

ICALL_TYPE(IOPATH, "System.IO.Path", IOPATH_1)
ICALL(IOPATH_1, "get_temp_path", ves_icall_System_IO_get_temp_path)

ICALL_TYPE(MATH, "System.Math", MATH_1)
ICALL(MATH_1, "Acos", ves_icall_System_Math_Acos)
ICALL(MATH_2, "Asin", ves_icall_System_Math_Asin)
ICALL(MATH_3, "Atan", ves_icall_System_Math_Atan)
ICALL(MATH_4, "Atan2", ves_icall_System_Math_Atan2)
ICALL(MATH_5, "Cos", ves_icall_System_Math_Cos)
ICALL(MATH_6, "Cosh", ves_icall_System_Math_Cosh)
ICALL(MATH_7, "Exp", ves_icall_System_Math_Exp)
ICALL(MATH_8, "Floor", ves_icall_System_Math_Floor)
ICALL(MATH_9, "Log", ves_icall_System_Math_Log)
ICALL(MATH_10, "Log10", ves_icall_System_Math_Log10)
ICALL(MATH_11, "Pow", ves_icall_System_Math_Pow)
ICALL(MATH_12, "Round", ves_icall_System_Math_Round)
ICALL(MATH_13, "Round2", ves_icall_System_Math_Round2)
ICALL(MATH_14, "Sin", ves_icall_System_Math_Sin)
ICALL(MATH_15, "Sinh", ves_icall_System_Math_Sinh)
ICALL(MATH_16, "Sqrt", ves_icall_System_Math_Sqrt)
ICALL(MATH_17, "Tan", ves_icall_System_Math_Tan)
ICALL(MATH_18, "Tanh", ves_icall_System_Math_Tanh)

ICALL_TYPE(MCATTR, "System.MonoCustomAttrs", MCATTR_1)
ICALL(MCATTR_1, "GetCustomAttributesDataInternal", mono_reflection_get_custom_attrs_data)
ICALL(MCATTR_2, "GetCustomAttributesInternal", custom_attrs_get_by_type)
ICALL(MCATTR_3, "IsDefinedInternal", custom_attrs_defined_internal)

ICALL_TYPE(MENUM, "System.MonoEnumInfo", MENUM_1)
ICALL(MENUM_1, "get_enum_info", ves_icall_get_enum_info)

ICALL_TYPE(MTYPE, "System.MonoType", MTYPE_1)
ICALL(MTYPE_1, "GetArrayRank", ves_icall_MonoType_GetArrayRank)
ICALL(MTYPE_2, "GetConstructors", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_3, "GetConstructors_internal", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_4, "GetCorrespondingInflatedConstructor", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_5, "GetCorrespondingInflatedMethod", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_6, "GetElementType", ves_icall_MonoType_GetElementType)
ICALL(MTYPE_7, "GetEvents_internal", ves_icall_Type_GetEvents_internal)
ICALL(MTYPE_8, "GetField", ves_icall_Type_GetField)
ICALL(MTYPE_9, "GetFields_internal", ves_icall_Type_GetFields_internal)
ICALL(MTYPE_10, "GetGenericArguments", ves_icall_MonoType_GetGenericArguments)
ICALL(MTYPE_11, "GetInterfaces", ves_icall_Type_GetInterfaces)
ICALL(MTYPE_12, "GetMethodsByName", ves_icall_Type_GetMethodsByName)
ICALL(MTYPE_13, "GetNestedType", ves_icall_Type_GetNestedType)
ICALL(MTYPE_14, "GetNestedTypes", ves_icall_Type_GetNestedTypes)
ICALL(MTYPE_15, "GetPropertiesByName", ves_icall_Type_GetPropertiesByName)
ICALL(MTYPE_16, "InternalGetEvent", ves_icall_MonoType_GetEvent)
ICALL(MTYPE_17, "IsByRefImpl", ves_icall_type_isbyref)
ICALL(MTYPE_18, "IsCOMObjectImpl", ves_icall_type_iscomobject)
ICALL(MTYPE_19, "IsPointerImpl", ves_icall_type_ispointer)
ICALL(MTYPE_20, "IsPrimitiveImpl", ves_icall_type_isprimitive)
ICALL(MTYPE_21, "getFullName", ves_icall_System_MonoType_getFullName)
ICALL(MTYPE_22, "get_Assembly", ves_icall_MonoType_get_Assembly)
ICALL(MTYPE_23, "get_BaseType", ves_icall_get_type_parent)
ICALL(MTYPE_24, "get_DeclaringMethod", ves_icall_MonoType_get_DeclaringMethod)
ICALL(MTYPE_25, "get_DeclaringType", ves_icall_MonoType_get_DeclaringType)
ICALL(MTYPE_26, "get_IsGenericParameter", ves_icall_MonoType_get_IsGenericParameter)
ICALL(MTYPE_27, "get_Module", ves_icall_MonoType_get_Module)
ICALL(MTYPE_28, "get_Name", ves_icall_MonoType_get_Name)
ICALL(MTYPE_29, "get_Namespace", ves_icall_MonoType_get_Namespace)
ICALL(MTYPE_31, "get_attributes", ves_icall_get_attributes)
ICALL(MTYPE_33, "get_core_clr_security_level", vell_icall_MonoType_get_core_clr_security_level)
ICALL(MTYPE_32, "type_from_obj", mono_type_type_from_obj)

#ifndef DISABLE_SOCKETS
ICALL_TYPE(NDNS, "System.Net.Dns", NDNS_1)
ICALL(NDNS_1, "GetHostByAddr_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByAddr_internal)
ICALL(NDNS_2, "GetHostByName_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByName_internal)
ICALL(NDNS_3, "GetHostName_internal(string&)", ves_icall_System_Net_Dns_GetHostName_internal)

ICALL_TYPE(SOCK, "System.Net.Sockets.Socket", SOCK_1)
ICALL(SOCK_1, "Accept_internal(intptr,int&,bool)", ves_icall_System_Net_Sockets_Socket_Accept_internal)
ICALL(SOCK_2, "Available_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Available_internal)
ICALL(SOCK_3, "Bind_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Bind_internal)
ICALL(SOCK_4, "Blocking_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Blocking_internal)
ICALL(SOCK_5, "Close_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Close_internal)
ICALL(SOCK_6, "Connect_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Connect_internal)
ICALL (SOCK_6a, "Disconnect_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Disconnect_internal)
ICALL(SOCK_7, "GetSocketOption_arr_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,byte[]&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_arr_internal)
ICALL(SOCK_8, "GetSocketOption_obj_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_obj_internal)
ICALL(SOCK_9, "Listen_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_Listen_internal)
ICALL(SOCK_10, "LocalEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_LocalEndPoint_internal)
ICALL(SOCK_11, "Poll_internal", ves_icall_System_Net_Sockets_Socket_Poll_internal)
ICALL(SOCK_11a, "Receive_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_array_internal)
ICALL(SOCK_12, "Receive_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_internal)
ICALL(SOCK_13, "RecvFrom_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress&,int&)", ves_icall_System_Net_Sockets_Socket_RecvFrom_internal)
ICALL(SOCK_14, "RemoteEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_RemoteEndPoint_internal)
ICALL(SOCK_15, "Select_internal(System.Net.Sockets.Socket[]&,int,int&)", ves_icall_System_Net_Sockets_Socket_Select_internal)
ICALL(SOCK_15a, "SendFile(intptr,string,byte[],byte[],System.Net.Sockets.TransmitFileOptions)", ves_icall_System_Net_Sockets_Socket_SendFile)
ICALL(SOCK_16, "SendTo_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_SendTo_internal)
ICALL(SOCK_16a, "Send_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_array_internal)
ICALL(SOCK_17, "Send_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_internal)
ICALL(SOCK_18, "SetSocketOption_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object,byte[],int,int&)", ves_icall_System_Net_Sockets_Socket_SetSocketOption_internal)
ICALL(SOCK_19, "Shutdown_internal(intptr,System.Net.Sockets.SocketShutdown,int&)", ves_icall_System_Net_Sockets_Socket_Shutdown_internal)
ICALL(SOCK_20, "Socket_internal(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType,int&)", ves_icall_System_Net_Sockets_Socket_Socket_internal)
ICALL(SOCK_21, "WSAIoctl(intptr,int,byte[],byte[],int&)", ves_icall_System_Net_Sockets_Socket_WSAIoctl)
ICALL(SOCK_21a, "cancel_blocking_socket_operation", icall_cancel_blocking_socket_operation)
ICALL(SOCK_22, "socket_pool_queue", icall_append_io_job)

ICALL_TYPE(SOCKEX, "System.Net.Sockets.SocketException", SOCKEX_1)
ICALL(SOCKEX_1, "WSAGetLastError_internal", ves_icall_System_Net_Sockets_SocketException_WSAGetLastError_internal)
#endif /* !DISABLE_SOCKETS */

ICALL_TYPE(NUMBER_FORMATTER, "System.NumberFormatter", NUMBER_FORMATTER_1)
ICALL(NUMBER_FORMATTER_1, "GetFormatterTables", ves_icall_System_NumberFormatter_GetFormatterTables)

ICALL_TYPE(OBJ, "System.Object", OBJ_1)
ICALL(OBJ_1, "GetType", ves_icall_System_Object_GetType)
ICALL(OBJ_2, "InternalGetHashCode", mono_object_hash)
ICALL(OBJ_3, "MemberwiseClone", ves_icall_System_Object_MemberwiseClone)
ICALL(OBJ_4, "obj_address", ves_icall_System_Object_obj_address)

ICALL_TYPE(ASSEM, "System.Reflection.Assembly", ASSEM_1)
ICALL(ASSEM_1, "FillName", ves_icall_System_Reflection_Assembly_FillName)
ICALL(ASSEM_2, "GetCallingAssembly", ves_icall_System_Reflection_Assembly_GetCallingAssembly)
ICALL(ASSEM_3, "GetEntryAssembly", ves_icall_System_Reflection_Assembly_GetEntryAssembly)
ICALL(ASSEM_4, "GetExecutingAssembly", ves_icall_System_Reflection_Assembly_GetExecutingAssembly)
ICALL(ASSEM_5, "GetFilesInternal", ves_icall_System_Reflection_Assembly_GetFilesInternal)
ICALL(ASSEM_6, "GetManifestModuleInternal", ves_icall_System_Reflection_Assembly_GetManifestModuleInternal)
ICALL(ASSEM_7, "GetManifestResourceInfoInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInfoInternal)
ICALL(ASSEM_8, "GetManifestResourceInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInternal)
ICALL(ASSEM_9, "GetManifestResourceNames", ves_icall_System_Reflection_Assembly_GetManifestResourceNames)
ICALL(ASSEM_10, "GetModulesInternal", ves_icall_System_Reflection_Assembly_GetModulesInternal)
//ICALL(ASSEM_11, "GetNamespaces", ves_icall_System_Reflection_Assembly_GetNamespaces)
ICALL(ASSEM_12, "GetReferencedAssemblies", ves_icall_System_Reflection_Assembly_GetReferencedAssemblies)
ICALL(ASSEM_13, "GetTypes", ves_icall_System_Reflection_Assembly_GetTypes)
ICALL(ASSEM_14, "InternalGetAssemblyName", ves_icall_System_Reflection_Assembly_InternalGetAssemblyName)
ICALL(ASSEM_15, "InternalGetType", ves_icall_System_Reflection_Assembly_InternalGetType)
ICALL(ASSEM_16, "InternalImageRuntimeVersion", ves_icall_System_Reflection_Assembly_InternalImageRuntimeVersion)
ICALL(ASSEM_17, "LoadFrom", ves_icall_System_Reflection_Assembly_LoadFrom)
ICALL(ASSEM_18, "LoadPermissions", ves_icall_System_Reflection_Assembly_LoadPermissions)
	/*
	 * Private icalls for the Mono Debugger
	 */
ICALL(ASSEM_19, "MonoDebugger_GetMethodToken", ves_icall_MonoDebugger_GetMethodToken)

	/* normal icalls again */
ICALL(ASSEM_20, "get_EntryPoint", ves_icall_System_Reflection_Assembly_get_EntryPoint)
ICALL(ASSEM_21, "get_ReflectionOnly", ves_icall_System_Reflection_Assembly_get_ReflectionOnly)
ICALL(ASSEM_22, "get_code_base", ves_icall_System_Reflection_Assembly_get_code_base)
ICALL(ASSEM_23, "get_fullname", ves_icall_System_Reflection_Assembly_get_fullName)
ICALL(ASSEM_24, "get_global_assembly_cache", ves_icall_System_Reflection_Assembly_get_global_assembly_cache)
ICALL(ASSEM_25, "get_location", ves_icall_System_Reflection_Assembly_get_location)
ICALL(ASSEM_26, "load_with_partial_name", ves_icall_System_Reflection_Assembly_load_with_partial_name)

ICALL_TYPE(ASSEMN, "System.Reflection.AssemblyName", ASSEMN_1)
ICALL(ASSEMN_1, "ParseName", ves_icall_System_Reflection_AssemblyName_ParseName)
ICALL(ASSEMN_2, "get_public_token", mono_digest_get_public_token)

ICALL_TYPE(CATTR_DATA, "System.Reflection.CustomAttributeData", CATTR_DATA_1)
ICALL(CATTR_DATA_1, "ResolveArgumentsInternal", mono_reflection_resolve_custom_attribute_data)

ICALL_TYPE(ASSEMB, "System.Reflection.Emit.AssemblyBuilder", ASSEMB_1)
ICALL(ASSEMB_1, "InternalAddModule", mono_image_load_module_dynamic)
ICALL(ASSEMB_2, "basic_init", mono_image_basic_init)

ICALL_TYPE(CATTRB, "System.Reflection.Emit.CustomAttributeBuilder", CATTRB_1)
ICALL(CATTRB_1, "GetBlob", mono_reflection_get_custom_attrs_blob)

#ifndef DISABLE_REFLECTION_EMIT
ICALL_TYPE(DERIVEDTYPE, "System.Reflection.Emit.DerivedType", DERIVEDTYPE_1)
ICALL(DERIVEDTYPE_1, "create_unmanaged_type", mono_reflection_create_unmanaged_type)
#endif

ICALL_TYPE(DYNM, "System.Reflection.Emit.DynamicMethod", DYNM_1)
ICALL(DYNM_1, "create_dynamic_method", mono_reflection_create_dynamic_method)

ICALL_TYPE(ENUMB, "System.Reflection.Emit.EnumBuilder", ENUMB_1)
ICALL(ENUMB_1, "setup_enum_type", ves_icall_EnumBuilder_setup_enum_type)

ICALL_TYPE(GPARB, "System.Reflection.Emit.GenericTypeParameterBuilder", GPARB_1)
ICALL(GPARB_1, "initialize", mono_reflection_initialize_generic_parameter)

ICALL_TYPE(METHODB, "System.Reflection.Emit.MethodBuilder", METHODB_1)
ICALL(METHODB_1, "MakeGenericMethod", mono_reflection_bind_generic_method_parameters)

ICALL_TYPE(MODULEB, "System.Reflection.Emit.ModuleBuilder", MODULEB_8)
ICALL(MODULEB_8, "RegisterToken", ves_icall_ModuleBuilder_RegisterToken)
ICALL(MODULEB_1, "WriteToFile", ves_icall_ModuleBuilder_WriteToFile)
ICALL(MODULEB_2, "basic_init", mono_image_module_basic_init)
ICALL(MODULEB_3, "build_metadata", ves_icall_ModuleBuilder_build_metadata)
ICALL(MODULEB_4, "create_modified_type", ves_icall_ModuleBuilder_create_modified_type)
ICALL(MODULEB_5, "getMethodToken", ves_icall_ModuleBuilder_getMethodToken)
ICALL(MODULEB_6, "getToken", ves_icall_ModuleBuilder_getToken)
ICALL(MODULEB_7, "getUSIndex", mono_image_insert_string)
ICALL(MODULEB_9, "set_wrappers_type", mono_image_set_wrappers_type)

ICALL_TYPE(SIGH, "System.Reflection.Emit.SignatureHelper", SIGH_1)
ICALL(SIGH_1, "get_signature_field", mono_reflection_sighelper_get_signature_field)
ICALL(SIGH_2, "get_signature_local", mono_reflection_sighelper_get_signature_local)

ICALL_TYPE(TYPEB, "System.Reflection.Emit.TypeBuilder", TYPEB_1)
ICALL(TYPEB_1, "create_generic_class", mono_reflection_create_generic_class)
ICALL(TYPEB_2, "create_internal_class", mono_reflection_create_internal_class)
ICALL(TYPEB_3, "create_runtime_class", mono_reflection_create_runtime_class)
ICALL(TYPEB_4, "get_IsGenericParameter", ves_icall_TypeBuilder_get_IsGenericParameter)
ICALL(TYPEB_5, "get_event_info", mono_reflection_event_builder_get_event_info)
ICALL(TYPEB_6, "setup_generic_class", mono_reflection_setup_generic_class)
ICALL(TYPEB_7, "setup_internal_class", mono_reflection_setup_internal_class)

ICALL_TYPE(FIELDI, "System.Reflection.FieldInfo", FILEDI_1)
ICALL(FILEDI_1, "GetTypeModifiers", ves_icall_System_Reflection_FieldInfo_GetTypeModifiers)
ICALL(FILEDI_2, "get_marshal_info", ves_icall_System_Reflection_FieldInfo_get_marshal_info)
ICALL(FILEDI_3, "internal_from_handle_type", ves_icall_System_Reflection_FieldInfo_internal_from_handle_type)

ICALL_TYPE(MEMBERI, "System.Reflection.MemberInfo", MEMBERI_1)
ICALL(MEMBERI_1, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MBASE, "System.Reflection.MethodBase", MBASE_1)
ICALL(MBASE_1, "GetCurrentMethod", ves_icall_GetCurrentMethod)
ICALL(MBASE_2, "GetMethodBodyInternal", ves_icall_System_Reflection_MethodBase_GetMethodBodyInternal)
ICALL(MBASE_3, "GetMethodFromHandleInternal", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternal)
ICALL(MBASE_4, "GetMethodFromHandleInternalType", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternalType)

ICALL_TYPE(MODULE, "System.Reflection.Module", MODULE_1)
ICALL(MODULE_1, "Close", ves_icall_System_Reflection_Module_Close)
ICALL(MODULE_2, "GetGlobalType", ves_icall_System_Reflection_Module_GetGlobalType)
ICALL(MODULE_3, "GetGuidInternal", ves_icall_System_Reflection_Module_GetGuidInternal)
ICALL(MODULE_14, "GetHINSTANCE", ves_icall_System_Reflection_Module_GetHINSTANCE)
ICALL(MODULE_4, "GetMDStreamVersion", ves_icall_System_Reflection_Module_GetMDStreamVersion)
ICALL(MODULE_5, "GetPEKind", ves_icall_System_Reflection_Module_GetPEKind)
ICALL(MODULE_6, "InternalGetTypes", ves_icall_System_Reflection_Module_InternalGetTypes)
ICALL(MODULE_7, "ResolveFieldToken", ves_icall_System_Reflection_Module_ResolveFieldToken)
ICALL(MODULE_8, "ResolveMemberToken", ves_icall_System_Reflection_Module_ResolveMemberToken)
ICALL(MODULE_9, "ResolveMethodToken", ves_icall_System_Reflection_Module_ResolveMethodToken)
ICALL(MODULE_10, "ResolveSignature", ves_icall_System_Reflection_Module_ResolveSignature)
ICALL(MODULE_11, "ResolveStringToken", ves_icall_System_Reflection_Module_ResolveStringToken)
ICALL(MODULE_12, "ResolveTypeToken", ves_icall_System_Reflection_Module_ResolveTypeToken)
ICALL(MODULE_13, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MCMETH, "System.Reflection.MonoCMethod", MCMETH_1)
ICALL(MCMETH_1, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MCMETH_2, "InternalInvoke", ves_icall_InternalInvoke)

ICALL_TYPE(MEVIN, "System.Reflection.MonoEventInfo", MEVIN_1)
ICALL(MEVIN_1, "get_event_info", ves_icall_get_event_info)

ICALL_TYPE(MFIELD, "System.Reflection.MonoField", MFIELD_1)
ICALL(MFIELD_1, "GetFieldOffset", ves_icall_MonoField_GetFieldOffset)
ICALL(MFIELD_2, "GetParentType", ves_icall_MonoField_GetParentType)
ICALL(MFIELD_5, "GetRawConstantValue", ves_icall_MonoField_GetRawConstantValue)
ICALL(MFIELD_3, "GetValueInternal", ves_icall_MonoField_GetValueInternal)
ICALL(MFIELD_6, "ResolveType", ves_icall_MonoField_ResolveType)
ICALL(MFIELD_4, "SetValueInternal", ves_icall_MonoField_SetValueInternal)

ICALL_TYPE(MGENCM, "System.Reflection.MonoGenericCMethod", MGENCM_1)
ICALL(MGENCM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MGENCL, "System.Reflection.MonoGenericClass", MGENCL_5)
ICALL(MGENCL_5, "initialize", mono_reflection_generic_class_initialize)
ICALL(MGENCL_6, "register_with_runtime", mono_reflection_register_with_runtime)

/* note this is the same as above: unify */
ICALL_TYPE(MGENM, "System.Reflection.MonoGenericMethod", MGENM_1)
ICALL(MGENM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MMETH, "System.Reflection.MonoMethod", MMETH_1)
ICALL(MMETH_1, "GetDllImportAttribute", ves_icall_MonoMethod_GetDllImportAttribute)
ICALL(MMETH_2, "GetGenericArguments", ves_icall_MonoMethod_GetGenericArguments)
ICALL(MMETH_3, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MMETH_4, "InternalInvoke", ves_icall_InternalInvoke)
ICALL(MMETH_5, "MakeGenericMethod_impl", mono_reflection_bind_generic_method_parameters)
ICALL(MMETH_6, "get_IsGenericMethod", ves_icall_MonoMethod_get_IsGenericMethod)
ICALL(MMETH_7, "get_IsGenericMethodDefinition", ves_icall_MonoMethod_get_IsGenericMethodDefinition)
ICALL(MMETH_8, "get_base_method", ves_icall_MonoMethod_get_base_method)
ICALL(MMETH_9, "get_name", ves_icall_MonoMethod_get_name)

ICALL_TYPE(MMETHI, "System.Reflection.MonoMethodInfo", MMETHI_4)
ICALL(MMETHI_4, "get_method_attributes", vell_icall_get_method_attributes)
ICALL(MMETHI_1, "get_method_info", ves_icall_get_method_info)
ICALL(MMETHI_2, "get_parameter_info", ves_icall_get_parameter_info)
ICALL(MMETHI_3, "get_retval_marshal", ves_icall_System_MonoMethodInfo_get_retval_marshal)

ICALL_TYPE(MPROPI, "System.Reflection.MonoPropertyInfo", MPROPI_1)
ICALL(MPROPI_1, "GetTypeModifiers", property_info_get_type_modifiers)
ICALL(MPROPI_3, "get_default_value", property_info_get_default_value)
ICALL(MPROPI_2, "get_property_info", ves_icall_get_property_info)

ICALL_TYPE(PARAMI, "System.Reflection.ParameterInfo", PARAMI_1)
ICALL(PARAMI_1, "GetMetadataToken", mono_reflection_get_token)
ICALL(PARAMI_2, "GetTypeModifiers", param_info_get_type_modifiers)

ICALL_TYPE(RUNH, "System.Runtime.CompilerServices.RuntimeHelpers", RUNH_1)
ICALL(RUNH_1, "GetObjectValue", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetObjectValue)
	 /* REMOVEME: no longer needed, just so we dont break things when not needed */
ICALL(RUNH_2, "GetOffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)
ICALL(RUNH_3, "InitializeArray", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_InitializeArray)
ICALL(RUNH_4, "RunClassConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunClassConstructor)
ICALL(RUNH_5, "RunModuleConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunModuleConstructor)
ICALL(RUNH_5h, "SufficientExecutionStack", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_SufficientExecutionStack)
ICALL(RUNH_6, "get_OffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)

ICALL_TYPE(GCH, "System.Runtime.InteropServices.GCHandle", GCH_1)
ICALL(GCH_1, "CheckCurrentDomain", GCHandle_CheckCurrentDomain)
ICALL(GCH_2, "FreeHandle", ves_icall_System_GCHandle_FreeHandle)
ICALL(GCH_3, "GetAddrOfPinnedObject", ves_icall_System_GCHandle_GetAddrOfPinnedObject)
ICALL(GCH_4, "GetTarget", ves_icall_System_GCHandle_GetTarget)
ICALL(GCH_5, "GetTargetHandle", ves_icall_System_GCHandle_GetTargetHandle)

#ifndef DISABLE_COM
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_1)
ICALL(MARSHAL_1, "AddRefInternal", ves_icall_System_Runtime_InteropServices_Marshal_AddRefInternal)
#else
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_2)
#endif
ICALL(MARSHAL_2, "AllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_AllocCoTaskMem)
ICALL(MARSHAL_3, "AllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_AllocHGlobal)
ICALL(MARSHAL_4, "DestroyStructure", ves_icall_System_Runtime_InteropServices_Marshal_DestroyStructure)
ICALL(MARSHAL_5, "FreeBSTR", ves_icall_System_Runtime_InteropServices_Marshal_FreeBSTR)
ICALL(MARSHAL_6, "FreeCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_FreeCoTaskMem)
ICALL(MARSHAL_7, "FreeHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_FreeHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_44, "GetCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetCCW)
ICALL(MARSHAL_8, "GetComSlotForMethodInfoInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetComSlotForMethodInfoInternal)
#endif
ICALL(MARSHAL_9, "GetDelegateForFunctionPointerInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetDelegateForFunctionPointerInternal)
ICALL(MARSHAL_10, "GetFunctionPointerForDelegateInternal", mono_delegate_to_ftnptr)
#ifndef DISABLE_COM
ICALL(MARSHAL_45, "GetIDispatchForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIDispatchForObjectInternal)
ICALL(MARSHAL_46, "GetIUnknownForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIUnknownForObjectInternal)
#endif
ICALL(MARSHAL_11, "GetLastWin32Error", ves_icall_System_Runtime_InteropServices_Marshal_GetLastWin32Error)
#ifndef DISABLE_COM
ICALL(MARSHAL_47, "GetObjectForCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetObjectForCCW)
ICALL(MARSHAL_48, "IsComObject", ves_icall_System_Runtime_InteropServices_Marshal_IsComObject)
#endif
ICALL(MARSHAL_12, "OffsetOf", ves_icall_System_Runtime_InteropServices_Marshal_OffsetOf)
ICALL(MARSHAL_13, "Prelink", ves_icall_System_Runtime_InteropServices_Marshal_Prelink)
ICALL(MARSHAL_14, "PrelinkAll", ves_icall_System_Runtime_InteropServices_Marshal_PrelinkAll)
ICALL(MARSHAL_15, "PtrToStringAnsi(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi)
ICALL(MARSHAL_16, "PtrToStringAnsi(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi_len)
#ifndef DISABLE_COM
ICALL(MARSHAL_17, "PtrToStringBSTR", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringBSTR)
#endif
ICALL(MARSHAL_18, "PtrToStringUni(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni)
ICALL(MARSHAL_19, "PtrToStringUni(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni_len)
ICALL(MARSHAL_20, "PtrToStructure(intptr,System.Type)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure_type)
ICALL(MARSHAL_21, "PtrToStructure(intptr,object)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure)
#ifndef DISABLE_COM
ICALL(MARSHAL_22, "QueryInterfaceInternal", ves_icall_System_Runtime_InteropServices_Marshal_QueryInterfaceInternal)
#endif
ICALL(MARSHAL_43, "ReAllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocCoTaskMem)
ICALL(MARSHAL_23, "ReAllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_49, "ReleaseComObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseComObjectInternal)
ICALL(MARSHAL_29, "ReleaseInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseInternal)
#endif
ICALL(MARSHAL_30, "SizeOf", ves_icall_System_Runtime_InteropServices_Marshal_SizeOf)
ICALL(MARSHAL_31, "StringToBSTR", ves_icall_System_Runtime_InteropServices_Marshal_StringToBSTR)
ICALL(MARSHAL_32, "StringToHGlobalAnsi", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalAnsi)
ICALL(MARSHAL_33, "StringToHGlobalUni", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalUni)
ICALL(MARSHAL_34, "StructureToPtr", ves_icall_System_Runtime_InteropServices_Marshal_StructureToPtr)
ICALL(MARSHAL_35, "UnsafeAddrOfPinnedArrayElement", ves_icall_System_Runtime_InteropServices_Marshal_UnsafeAddrOfPinnedArrayElement)
ICALL(MARSHAL_41, "copy_from_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_from_unmanaged)
ICALL(MARSHAL_42, "copy_to_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_to_unmanaged)

ICALL_TYPE(ACTS, "System.Runtime.Remoting.Activation.ActivationServices", ACTS_1)
ICALL(ACTS_1, "AllocateUninitializedClassInstance", ves_icall_System_Runtime_Activation_ActivationServices_AllocateUninitializedClassInstance)
ICALL(ACTS_2, "EnableProxyActivation", ves_icall_System_Runtime_Activation_ActivationServices_EnableProxyActivation)

ICALL_TYPE(MONOMM, "System.Runtime.Remoting.Messaging.MonoMethodMessage", MONOMM_1)
ICALL(MONOMM_1, "InitMessage", ves_icall_MonoMethodMessage_InitMessage)

#ifndef DISABLE_REMOTING
ICALL_TYPE(REALP, "System.Runtime.Remoting.Proxies.RealProxy", REALP_1)
ICALL(REALP_1, "InternalGetProxyType", ves_icall_Remoting_RealProxy_InternalGetProxyType)
ICALL(REALP_2, "InternalGetTransparentProxy", ves_icall_Remoting_RealProxy_GetTransparentProxy)

ICALL_TYPE(REMSER, "System.Runtime.Remoting.RemotingServices", REMSER_0)
ICALL(REMSER_0, "GetVirtualMethod", ves_icall_Remoting_RemotingServices_GetVirtualMethod)
ICALL(REMSER_1, "InternalExecute", ves_icall_InternalExecute)
ICALL(REMSER_2, "IsTransparentProxy", ves_icall_IsTransparentProxy)
#endif

ICALL_TYPE(MHAN, "System.RuntimeMethodHandle", MHAN_1)
ICALL(MHAN_1, "GetFunctionPointer", ves_icall_RuntimeMethod_GetFunctionPointer)

ICALL_TYPE(RNG, "System.Security.Cryptography.RNGCryptoServiceProvider", RNG_1)
ICALL(RNG_1, "RngClose", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngClose)
ICALL(RNG_2, "RngGetBytes", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngGetBytes)
ICALL(RNG_3, "RngInitialize", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngInitialize)
ICALL(RNG_4, "RngOpen", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngOpen)

#ifndef DISABLE_POLICY_EVIDENCE
ICALL_TYPE(EVID, "System.Security.Policy.Evidence", EVID_1)
ICALL(EVID_1, "IsAuthenticodePresent", ves_icall_System_Security_Policy_Evidence_IsAuthenticodePresent)

ICALL_TYPE(WINID, "System.Security.Principal.WindowsIdentity", WINID_1)
ICALL(WINID_1, "GetCurrentToken", ves_icall_System_Security_Principal_WindowsIdentity_GetCurrentToken)
ICALL(WINID_2, "GetTokenName", ves_icall_System_Security_Principal_WindowsIdentity_GetTokenName)
ICALL(WINID_3, "GetUserToken", ves_icall_System_Security_Principal_WindowsIdentity_GetUserToken)
ICALL(WINID_4, "_GetRoles", ves_icall_System_Security_Principal_WindowsIdentity_GetRoles)

ICALL_TYPE(WINIMP, "System.Security.Principal.WindowsImpersonationContext", WINIMP_1)
ICALL(WINIMP_1, "CloseToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_CloseToken)
ICALL(WINIMP_2, "DuplicateToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_DuplicateToken)
ICALL(WINIMP_3, "RevertToSelf", ves_icall_System_Security_Principal_WindowsImpersonationContext_RevertToSelf)
ICALL(WINIMP_4, "SetCurrentToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_SetCurrentToken)

ICALL_TYPE(WINPRIN, "System.Security.Principal.WindowsPrincipal", WINPRIN_1)
ICALL(WINPRIN_1, "IsMemberOfGroupId", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupId)
ICALL(WINPRIN_2, "IsMemberOfGroupName", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupName)

ICALL_TYPE(SECSTRING, "System.Security.SecureString", SECSTRING_1)
ICALL(SECSTRING_1, "DecryptInternal", ves_icall_System_Security_SecureString_DecryptInternal)
ICALL(SECSTRING_2, "EncryptInternal", ves_icall_System_Security_SecureString_EncryptInternal)
#endif /* !DISABLE_POLICY_EVIDENCE */

ICALL_TYPE(SECMAN, "System.Security.SecurityManager", SECMAN_1)
ICALL(SECMAN_1, "GetLinkDemandSecurity", ves_icall_System_Security_SecurityManager_GetLinkDemandSecurity)
ICALL(SECMAN_2, "get_CheckExecutionRights", ves_icall_System_Security_SecurityManager_get_CheckExecutionRights)
ICALL(SECMAN_3, "get_RequiresElevatedPermissions", mono_security_core_clr_require_elevated_permissions)
ICALL(SECMAN_4, "get_SecurityEnabled", ves_icall_System_Security_SecurityManager_get_SecurityEnabled)
ICALL(SECMAN_5, "set_CheckExecutionRights", ves_icall_System_Security_SecurityManager_set_CheckExecutionRights)
ICALL(SECMAN_6, "set_SecurityEnabled", ves_icall_System_Security_SecurityManager_set_SecurityEnabled)

ICALL_TYPE(STRING, "System.String", STRING_1)
ICALL(STRING_1, ".ctor(char*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_2, ".ctor(char*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_3, ".ctor(char,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_4, ".ctor(char[])", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_5, ".ctor(char[],int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_6, ".ctor(sbyte*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_7, ".ctor(sbyte*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8, ".ctor(sbyte*,int,int,System.Text.Encoding)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8a, "GetLOSLimit", ves_icall_System_String_GetLOSLimit)
ICALL(STRING_9, "InternalAllocateStr", ves_icall_System_String_InternalAllocateStr)
ICALL(STRING_10, "InternalIntern", ves_icall_System_String_InternalIntern)
ICALL(STRING_11, "InternalIsInterned", ves_icall_System_String_InternalIsInterned)

ICALL_TYPE(TENC, "System.Text.Encoding", TENC_1)
ICALL(TENC_1, "InternalCodePage", ves_icall_System_Text_Encoding_InternalCodePage)

ICALL_TYPE(ILOCK, "System.Threading.Interlocked", ILOCK_1)
ICALL(ILOCK_1, "Add(int&,int)", ves_icall_System_Threading_Interlocked_Add_Int)
ICALL(ILOCK_2, "Add(long&,long)", ves_icall_System_Threading_Interlocked_Add_Long)
ICALL(ILOCK_3, "CompareExchange(T&,T,T)", ves_icall_System_Threading_Interlocked_CompareExchange_T)
ICALL(ILOCK_4, "CompareExchange(double&,double,double)", ves_icall_System_Threading_Interlocked_CompareExchange_Double)
ICALL(ILOCK_5, "CompareExchange(int&,int,int)", ves_icall_System_Threading_Interlocked_CompareExchange_Int)
ICALL(ILOCK_6, "CompareExchange(intptr&,intptr,intptr)", ves_icall_System_Threading_Interlocked_CompareExchange_IntPtr)
ICALL(ILOCK_7, "CompareExchange(long&,long,long)", ves_icall_System_Threading_Interlocked_CompareExchange_Long)
ICALL(ILOCK_8, "CompareExchange(object&,object,object)", ves_icall_System_Threading_Interlocked_CompareExchange_Object)
ICALL(ILOCK_9, "CompareExchange(single&,single,single)", ves_icall_System_Threading_Interlocked_CompareExchange_Single)
ICALL(ILOCK_10, "Decrement(int&)", ves_icall_System_Threading_Interlocked_Decrement_Int)
ICALL(ILOCK_11, "Decrement(long&)", ves_icall_System_Threading_Interlocked_Decrement_Long)
ICALL(ILOCK_12, "Exchange(T&,T)", ves_icall_System_Threading_Interlocked_Exchange_T)
ICALL(ILOCK_13, "Exchange(double&,double)", ves_icall_System_Threading_Interlocked_Exchange_Double)
ICALL(ILOCK_14, "Exchange(int&,int)", ves_icall_System_Threading_Interlocked_Exchange_Int)
ICALL(ILOCK_15, "Exchange(intptr&,intptr)", ves_icall_System_Threading_Interlocked_Exchange_IntPtr)
ICALL(ILOCK_16, "Exchange(long&,long)", ves_icall_System_Threading_Interlocked_Exchange_Long)
ICALL(ILOCK_17, "Exchange(object&,object)", ves_icall_System_Threading_Interlocked_Exchange_Object)
ICALL(ILOCK_18, "Exchange(single&,single)", ves_icall_System_Threading_Interlocked_Exchange_Single)
ICALL(ILOCK_19, "Increment(int&)", ves_icall_System_Threading_Interlocked_Increment_Int)
ICALL(ILOCK_20, "Increment(long&)", ves_icall_System_Threading_Interlocked_Increment_Long)
ICALL(ILOCK_21, "Read(long&)", ves_icall_System_Threading_Interlocked_Read_Long)

ICALL_TYPE(ITHREAD, "System.Threading.InternalThread", ITHREAD_1)
ICALL(ITHREAD_1, "Thread_free_internal", ves_icall_System_Threading_InternalThread_Thread_free_internal)

ICALL_TYPE(MONIT, "System.Threading.Monitor", MONIT_8)
ICALL(MONIT_8, "Enter", mono_monitor_enter)
ICALL(MONIT_1, "Exit", mono_monitor_exit)
ICALL(MONIT_2, "Monitor_pulse", ves_icall_System_Threading_Monitor_Monitor_pulse)
ICALL(MONIT_3, "Monitor_pulse_all", ves_icall_System_Threading_Monitor_Monitor_pulse_all)
ICALL(MONIT_4, "Monitor_test_owner", ves_icall_System_Threading_Monitor_Monitor_test_owner)
ICALL(MONIT_5, "Monitor_test_synchronised", ves_icall_System_Threading_Monitor_Monitor_test_synchronised)
ICALL(MONIT_6, "Monitor_try_enter", ves_icall_System_Threading_Monitor_Monitor_try_enter)
ICALL(MONIT_7, "Monitor_wait", ves_icall_System_Threading_Monitor_Monitor_wait)
ICALL(MONIT_9, "try_enter_with_atomic_var", ves_icall_System_Threading_Monitor_Monitor_try_enter_with_atomic_var)

ICALL_TYPE(MUTEX, "System.Threading.Mutex", MUTEX_1)
ICALL(MUTEX_1, "CreateMutex_internal(bool,string,bool&)", ves_icall_System_Threading_Mutex_CreateMutex_internal)
ICALL(MUTEX_2, "OpenMutex_internal(string,System.Security.AccessControl.MutexRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Mutex_OpenMutex_internal)
ICALL(MUTEX_3, "ReleaseMutex_internal(intptr)", ves_icall_System_Threading_Mutex_ReleaseMutex_internal)

ICALL_TYPE(NATIVEC, "System.Threading.NativeEventCalls", NATIVEC_1)
ICALL(NATIVEC_1, "CloseEvent_internal", ves_icall_System_Threading_Events_CloseEvent_internal)
ICALL(NATIVEC_2, "CreateEvent_internal(bool,bool,string,bool&)", ves_icall_System_Threading_Events_CreateEvent_internal)
ICALL(NATIVEC_3, "OpenEvent_internal(string,System.Security.AccessControl.EventWaitHandleRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Events_OpenEvent_internal)
ICALL(NATIVEC_4, "ResetEvent_internal",  ves_icall_System_Threading_Events_ResetEvent_internal)
ICALL(NATIVEC_5, "SetEvent_internal",    ves_icall_System_Threading_Events_SetEvent_internal)

ICALL_TYPE(SEMA, "System.Threading.Semaphore", SEMA_1)
ICALL(SEMA_1, "CreateSemaphore_internal(int,int,string,bool&)", ves_icall_System_Threading_Semaphore_CreateSemaphore_internal)
ICALL(SEMA_2, "OpenSemaphore_internal(string,System.Security.AccessControl.SemaphoreRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Semaphore_OpenSemaphore_internal)
ICALL(SEMA_3, "ReleaseSemaphore_internal(intptr,int,bool&)", ves_icall_System_Threading_Semaphore_ReleaseSemaphore_internal)

ICALL_TYPE(THREAD, "System.Threading.Thread", THREAD_1)
ICALL(THREAD_1, "Abort_internal(System.Threading.InternalThread,object)", ves_icall_System_Threading_Thread_Abort)
ICALL(THREAD_1aa, "AllocTlsData", mono_thread_alloc_tls)
ICALL(THREAD_1a, "ByteArrayToCurrentDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToCurrentDomain)
ICALL(THREAD_1b, "ByteArrayToRootDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToRootDomain)
ICALL(THREAD_2, "ClrState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_ClrState)
ICALL(THREAD_2a, "ConstructInternalThread", ves_icall_System_Threading_Thread_ConstructInternalThread)
ICALL(THREAD_3, "CurrentInternalThread_internal", mono_thread_internal_current)
ICALL(THREAD_3a, "DestroyTlsData", mono_thread_destroy_tls)
ICALL(THREAD_4, "FreeLocalSlotValues", mono_thread_free_local_slot_values)
ICALL(THREAD_55, "GetAbortExceptionState", ves_icall_System_Threading_Thread_GetAbortExceptionState)
ICALL(THREAD_7, "GetDomainID", ves_icall_System_Threading_Thread_GetDomainID)
ICALL(THREAD_8, "GetName_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetName_internal)
ICALL(THREAD_11, "GetState(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetState)
ICALL(THREAD_53, "Interrupt_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Interrupt_internal)
ICALL(THREAD_12, "Join_internal(System.Threading.InternalThread,int,intptr)", ves_icall_System_Threading_Thread_Join_internal)
ICALL(THREAD_13, "MemoryBarrier", ves_icall_System_Threading_Thread_MemoryBarrier)
ICALL(THREAD_14, "ResetAbort_internal()", ves_icall_System_Threading_Thread_ResetAbort)
ICALL(THREAD_15, "Resume_internal()", ves_icall_System_Threading_Thread_Resume)
ICALL(THREAD_18, "SetName_internal(System.Threading.InternalThread,string)", ves_icall_System_Threading_Thread_SetName_internal)
ICALL(THREAD_21, "SetState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_SetState)
ICALL(THREAD_22, "Sleep_internal", ves_icall_System_Threading_Thread_Sleep_internal)
ICALL(THREAD_54, "SpinWait_nop", ves_icall_System_Threading_Thread_SpinWait_nop)
ICALL(THREAD_23, "Suspend_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Suspend)
ICALL(THREAD_25, "Thread_internal", ves_icall_System_Threading_Thread_Thread_internal)
ICALL(THREAD_26, "VolatileRead(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_27, "VolatileRead(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(THREAD_28, "VolatileRead(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_29, "VolatileRead(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_30, "VolatileRead(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_31, "VolatileRead(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_32, "VolatileRead(object&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_33, "VolatileRead(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_34, "VolatileRead(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(THREAD_35, "VolatileRead(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_36, "VolatileRead(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_37, "VolatileRead(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_38, "VolatileRead(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_39, "VolatileWrite(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_40, "VolatileWrite(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(THREAD_41, "VolatileWrite(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_42, "VolatileWrite(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_43, "VolatileWrite(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_44, "VolatileWrite(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_45, "VolatileWrite(object&,object)", ves_icall_System_Threading_Thread_VolatileWriteObject)
ICALL(THREAD_46, "VolatileWrite(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_47, "VolatileWrite(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(THREAD_48, "VolatileWrite(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_49, "VolatileWrite(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_50, "VolatileWrite(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_51, "VolatileWrite(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_9, "Yield", ves_icall_System_Threading_Thread_Yield)
ICALL(THREAD_52, "current_lcid()", ves_icall_System_Threading_Thread_current_lcid)

ICALL_TYPE(THREADP, "System.Threading.ThreadPool", THREADP_1)
ICALL(THREADP_1, "GetAvailableThreads", ves_icall_System_Threading_ThreadPool_GetAvailableThreads)
ICALL(THREADP_2, "GetMaxThreads", ves_icall_System_Threading_ThreadPool_GetMaxThreads)
ICALL(THREADP_3, "GetMinThreads", ves_icall_System_Threading_ThreadPool_GetMinThreads)
ICALL(THREADP_35, "SetMaxThreads", ves_icall_System_Threading_ThreadPool_SetMaxThreads)
ICALL(THREADP_4, "SetMinThreads", ves_icall_System_Threading_ThreadPool_SetMinThreads)
ICALL(THREADP_5, "pool_queue", icall_append_job)

ICALL_TYPE(VOLATILE, "System.Threading.Volatile", VOLATILE_28)
ICALL(VOLATILE_28, "Read(T&)", ves_icall_System_Threading_Volatile_Read_T)
ICALL(VOLATILE_1, "Read(bool&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_2, "Read(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_3, "Read(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(VOLATILE_4, "Read(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_5, "Read(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_6, "Read(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_7, "Read(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_8, "Read(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_9, "Read(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(VOLATILE_10, "Read(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_11, "Read(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_12, "Read(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_13, "Read(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_27, "Write(T&,T)", ves_icall_System_Threading_Volatile_Write_T)
ICALL(VOLATILE_14, "Write(bool&,bool)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_15, "Write(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_16, "Write(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(VOLATILE_17, "Write(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_18, "Write(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_19, "Write(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_20, "Write(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(VOLATILE_21, "Write(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_22, "Write(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(VOLATILE_23, "Write(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_24, "Write(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_25, "Write(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_26, "Write(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)

ICALL_TYPE(WAITH, "System.Threading.WaitHandle", WAITH_1)
ICALL(WAITH_1, "SignalAndWait_Internal", ves_icall_System_Threading_WaitHandle_SignalAndWait_Internal)
ICALL(WAITH_2, "WaitAll_internal", ves_icall_System_Threading_WaitHandle_WaitAll_internal)
ICALL(WAITH_3, "WaitAny_internal", ves_icall_System_Threading_WaitHandle_WaitAny_internal)
ICALL(WAITH_4, "WaitOne_internal", ves_icall_System_Threading_WaitHandle_WaitOne_internal)

ICALL_TYPE(TYPE, "System.Type", TYPE_1)
ICALL(TYPE_1, "EqualsInternal", ves_icall_System_Type_EqualsInternal)
ICALL(TYPE_2, "GetGenericParameterAttributes", ves_icall_Type_GetGenericParameterAttributes)
ICALL(TYPE_3, "GetGenericParameterConstraints_impl", ves_icall_Type_GetGenericParameterConstraints)
ICALL(TYPE_4, "GetGenericParameterPosition", ves_icall_Type_GetGenericParameterPosition)
ICALL(TYPE_5, "GetGenericTypeDefinition_impl", ves_icall_Type_GetGenericTypeDefinition_impl)
ICALL(TYPE_6, "GetInterfaceMapData", ves_icall_Type_GetInterfaceMapData)
ICALL(TYPE_7, "GetPacking", ves_icall_Type_GetPacking)
ICALL(TYPE_8, "GetTypeCode", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_9, "GetTypeCodeInternal", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_10, "IsArrayImpl", ves_icall_Type_IsArrayImpl)
ICALL(TYPE_11, "IsInstanceOfType", ves_icall_type_IsInstanceOfType)
ICALL(TYPE_12, "MakeGenericType", ves_icall_Type_MakeGenericType)
ICALL(TYPE_13, "MakePointerType", ves_icall_Type_MakePointerType)
ICALL(TYPE_14, "get_IsGenericInstance", ves_icall_Type_get_IsGenericInstance)
ICALL(TYPE_15, "get_IsGenericType", ves_icall_Type_get_IsGenericType)
ICALL(TYPE_16, "get_IsGenericTypeDefinition", ves_icall_Type_get_IsGenericTypeDefinition)
ICALL(TYPE_17, "internal_from_handle", ves_icall_type_from_handle)
ICALL(TYPE_18, "internal_from_name", ves_icall_type_from_name)
ICALL(TYPE_19, "make_array_type", ves_icall_Type_make_array_type)
ICALL(TYPE_20, "make_byref_type", ves_icall_Type_make_byref_type)
ICALL(TYPE_21, "type_is_assignable_from", ves_icall_type_is_assignable_from)
ICALL(TYPE_22, "type_is_subtype_of", ves_icall_type_is_subtype_of)

ICALL_TYPE(TYPEDR, "System.TypedReference", TYPEDR_1)
ICALL(TYPEDR_1, "ToObject",	mono_TypedReference_ToObject)
ICALL(TYPEDR_2, "ToObjectInternal",	mono_TypedReference_ToObjectInternal)

ICALL_TYPE(VALUET, "System.ValueType", VALUET_1)
ICALL(VALUET_1, "InternalEquals", ves_icall_System_ValueType_Equals)
ICALL(VALUET_2, "InternalGetHashCode", ves_icall_System_ValueType_InternalGetHashCode)

ICALL_TYPE(WEBIC, "System.Web.Util.ICalls", WEBIC_1)
ICALL(WEBIC_1, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(WEBIC_2, "GetMachineInstallDirectory", ves_icall_System_Web_Util_ICalls_get_machine_install_dir)
ICALL(WEBIC_3, "GetUnmanagedResourcesPtr", ves_icall_get_resources_ptr)

#ifndef DISABLE_COM
ICALL_TYPE(COMOBJ, "System.__ComObject", COMOBJ_1)
ICALL(COMOBJ_1, "CreateRCW", ves_icall_System_ComObject_CreateRCW)
ICALL(COMOBJ_2, "GetInterfaceInternal", ves_icall_System_ComObject_GetInterfaceInternal)
ICALL(COMOBJ_3, "ReleaseInterfaces", ves_icall_System_ComObject_ReleaseInterfaces)
#endif
	NULL
};
#define icall_name_get(id) icall_names [(id)]

#endif /* !HAVE_ARRAY_ELEM_INIT */

#undef ICALL_TYPE
#undef ICALL
#define ICALL_TYPE(id,name,first)
#define ICALL(id,name,func) func,
static const gconstpointer
icall_functions [] = {
// #include "metadata/icall-def.h"
ICALL_TYPE(UNORM, "Mono.Globalization.Unicode.Normalization", UNORM_1)
ICALL(UNORM_1, "load_normalization_resource", load_normalization_resource)

#ifndef DISABLE_COM
ICALL_TYPE(COMPROX, "Mono.Interop.ComInteropProxy", COMPROX_1)
ICALL(COMPROX_1, "AddProxy", ves_icall_Mono_Interop_ComInteropProxy_AddProxy)
ICALL(COMPROX_2, "FindProxy", ves_icall_Mono_Interop_ComInteropProxy_FindProxy)
#endif

ICALL_TYPE(RUNTIME, "Mono.Runtime", RUNTIME_1)
ICALL(RUNTIME_1, "GetDisplayName", ves_icall_Mono_Runtime_GetDisplayName)
ICALL(RUNTIME_12, "GetNativeStackTrace", ves_icall_Mono_Runtime_GetNativeStackTrace)
ICALL(RUNTIME_13, "SetGCAllowSynchronousMajor", ves_icall_Mono_Runtime_SetGCAllowSynchronousMajor)

#ifndef PLATFORM_RO_FS
ICALL_TYPE(KPAIR, "Mono.Security.Cryptography.KeyPairPersistence", KPAIR_1)
ICALL(KPAIR_1, "_CanSecure", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_CanSecure)
ICALL(KPAIR_2, "_IsMachineProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsMachineProtected)
ICALL(KPAIR_3, "_IsUserProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsUserProtected)
ICALL(KPAIR_4, "_ProtectMachine", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectMachine)
ICALL(KPAIR_5, "_ProtectUser", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectUser)
#endif /* !PLATFORM_RO_FS */

ICALL_TYPE(ACTIV, "System.Activator", ACTIV_1)
ICALL(ACTIV_1, "CreateInstanceInternal", ves_icall_System_Activator_CreateInstanceInternal)

ICALL_TYPE(APPDOM, "System.AppDomain", APPDOM_1)
ICALL(APPDOM_1, "ExecuteAssembly", ves_icall_System_AppDomain_ExecuteAssembly)
ICALL(APPDOM_2, "GetAssemblies", ves_icall_System_AppDomain_GetAssemblies)
ICALL(APPDOM_3, "GetData", ves_icall_System_AppDomain_GetData)
ICALL(APPDOM_4, "InternalGetContext", ves_icall_System_AppDomain_InternalGetContext)
ICALL(APPDOM_5, "InternalGetDefaultContext", ves_icall_System_AppDomain_InternalGetDefaultContext)
ICALL(APPDOM_6, "InternalGetProcessGuid", ves_icall_System_AppDomain_InternalGetProcessGuid)
ICALL(APPDOM_7, "InternalIsFinalizingForUnload", ves_icall_System_AppDomain_InternalIsFinalizingForUnload)
ICALL(APPDOM_8, "InternalPopDomainRef", ves_icall_System_AppDomain_InternalPopDomainRef)
ICALL(APPDOM_9, "InternalPushDomainRef", ves_icall_System_AppDomain_InternalPushDomainRef)
ICALL(APPDOM_10, "InternalPushDomainRefByID", ves_icall_System_AppDomain_InternalPushDomainRefByID)
ICALL(APPDOM_11, "InternalSetContext", ves_icall_System_AppDomain_InternalSetContext)
ICALL(APPDOM_12, "InternalSetDomain", ves_icall_System_AppDomain_InternalSetDomain)
ICALL(APPDOM_13, "InternalSetDomainByID", ves_icall_System_AppDomain_InternalSetDomainByID)
ICALL(APPDOM_14, "InternalUnload", ves_icall_System_AppDomain_InternalUnload)
ICALL(APPDOM_15, "LoadAssembly", ves_icall_System_AppDomain_LoadAssembly)
ICALL(APPDOM_16, "LoadAssemblyRaw", ves_icall_System_AppDomain_LoadAssemblyRaw)
ICALL(APPDOM_17, "SetData", ves_icall_System_AppDomain_SetData)
ICALL(APPDOM_18, "createDomain", ves_icall_System_AppDomain_createDomain)
ICALL(APPDOM_19, "getCurDomain", ves_icall_System_AppDomain_getCurDomain)
ICALL(APPDOM_20, "getFriendlyName", ves_icall_System_AppDomain_getFriendlyName)
ICALL(APPDOM_21, "getRootDomain", ves_icall_System_AppDomain_getRootDomain)
ICALL(APPDOM_22, "getSetup", ves_icall_System_AppDomain_getSetup)

ICALL_TYPE(ARGI, "System.ArgIterator", ARGI_1)
ICALL(ARGI_1, "IntGetNextArg()",                  mono_ArgIterator_IntGetNextArg)
ICALL(ARGI_2, "IntGetNextArg(intptr)", mono_ArgIterator_IntGetNextArgT)
ICALL(ARGI_3, "IntGetNextArgType",                mono_ArgIterator_IntGetNextArgType)
ICALL(ARGI_4, "Setup",                            mono_ArgIterator_Setup)

ICALL_TYPE(ARRAY, "System.Array", ARRAY_1)
ICALL(ARRAY_1, "ClearInternal",    ves_icall_System_Array_ClearInternal)
ICALL(ARRAY_2, "Clone",            mono_array_clone)
ICALL(ARRAY_3, "CreateInstanceImpl",   ves_icall_System_Array_CreateInstanceImpl)
ICALL(ARRAY_14, "CreateInstanceImpl64",   ves_icall_System_Array_CreateInstanceImpl64)
ICALL(ARRAY_4, "FastCopy",         ves_icall_System_Array_FastCopy)
ICALL(ARRAY_5, "GetGenericValueImpl", ves_icall_System_Array_GetGenericValueImpl)
ICALL(ARRAY_6, "GetLength",        ves_icall_System_Array_GetLength)
ICALL(ARRAY_15, "GetLongLength",        ves_icall_System_Array_GetLongLength)
ICALL(ARRAY_7, "GetLowerBound",    ves_icall_System_Array_GetLowerBound)
ICALL(ARRAY_8, "GetRank",          ves_icall_System_Array_GetRank)
ICALL(ARRAY_9, "GetValue",         ves_icall_System_Array_GetValue)
ICALL(ARRAY_10, "GetValueImpl",     ves_icall_System_Array_GetValueImpl)
ICALL(ARRAY_11, "SetGenericValueImpl", ves_icall_System_Array_SetGenericValueImpl)
ICALL(ARRAY_12, "SetValue",         ves_icall_System_Array_SetValue)
ICALL(ARRAY_13, "SetValueImpl",     ves_icall_System_Array_SetValueImpl)

ICALL_TYPE(BUFFER, "System.Buffer", BUFFER_1)
ICALL(BUFFER_1, "BlockCopyInternal", ves_icall_System_Buffer_BlockCopyInternal)
ICALL(BUFFER_2, "ByteLengthInternal", ves_icall_System_Buffer_ByteLengthInternal)
ICALL(BUFFER_3, "GetByteInternal", ves_icall_System_Buffer_GetByteInternal)
ICALL(BUFFER_4, "SetByteInternal", ves_icall_System_Buffer_SetByteInternal)

ICALL_TYPE(CHAR, "System.Char", CHAR_1)
ICALL(CHAR_1, "GetDataTablePointers", ves_icall_System_Char_GetDataTablePointers)

ICALL_TYPE (COMPO_W, "System.ComponentModel.Win32Exception", COMPO_W_1)
ICALL (COMPO_W_1, "W32ErrorMessage", ves_icall_System_ComponentModel_Win32Exception_W32ErrorMessage)

ICALL_TYPE(DEFAULTC, "System.Configuration.DefaultConfig", DEFAULTC_1)
ICALL(DEFAULTC_1, "get_bundled_machine_config", get_bundled_machine_config)
ICALL(DEFAULTC_2, "get_machine_config_path", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)

/* Note that the below icall shares the same function as DefaultConfig uses */
ICALL_TYPE(INTCFGHOST, "System.Configuration.InternalConfigurationHost", INTCFGHOST_1)
ICALL(INTCFGHOST_1, "get_bundled_app_config", get_bundled_app_config)
ICALL(INTCFGHOST_2, "get_bundled_machine_config", get_bundled_machine_config)

ICALL_TYPE(CONSOLE, "System.ConsoleDriver", CONSOLE_1)
ICALL(CONSOLE_1, "InternalKeyAvailable", ves_icall_System_ConsoleDriver_InternalKeyAvailable )
ICALL(CONSOLE_2, "Isatty", ves_icall_System_ConsoleDriver_Isatty )
ICALL(CONSOLE_3, "SetBreak", ves_icall_System_ConsoleDriver_SetBreak )
ICALL(CONSOLE_4, "SetEcho", ves_icall_System_ConsoleDriver_SetEcho )
ICALL(CONSOLE_5, "TtySetup", ves_icall_System_ConsoleDriver_TtySetup )

ICALL_TYPE(CONVERT, "System.Convert", CONVERT_1)
ICALL(CONVERT_1, "InternalFromBase64CharArray", InternalFromBase64CharArray )
ICALL(CONVERT_2, "InternalFromBase64String", InternalFromBase64String )

ICALL_TYPE(TZONE, "System.CurrentSystemTimeZone", TZONE_1)
ICALL(TZONE_1, "GetTimeZoneData", ves_icall_System_CurrentSystemTimeZone_GetTimeZoneData)

ICALL_TYPE(DTIME, "System.DateTime", DTIME_1)
ICALL(DTIME_1, "GetNow", mono_100ns_datetime)
ICALL(DTIME_2, "GetTimeMonotonic", mono_100ns_ticks)

#ifndef DISABLE_DECIMAL
ICALL_TYPE(DECIMAL, "System.Decimal", DECIMAL_1)
ICALL(DECIMAL_1, "decimal2Int64", mono_decimal2Int64)
ICALL(DECIMAL_2, "decimal2UInt64", mono_decimal2UInt64)
ICALL(DECIMAL_3, "decimal2double", mono_decimal2double)
//ICALL(DECIMAL_4, "decimal2string", mono_decimal2string)
ICALL(DECIMAL_5, "decimalCompare", mono_decimalCompare)
ICALL(DECIMAL_6, "decimalDiv", mono_decimalDiv)
ICALL(DECIMAL_7, "decimalFloorAndTrunc", mono_decimalFloorAndTrunc)
ICALL(DECIMAL_8, "decimalIncr", mono_decimalIncr)
ICALL(DECIMAL_9, "decimalIntDiv", mono_decimalIntDiv)
ICALL(DECIMAL_10, "decimalMult", mono_decimalMult)
ICALL(DECIMAL_11, "decimalRound", mono_decimalRound)
ICALL(DECIMAL_12, "decimalSetExponent", mono_decimalSetExponent)
ICALL(DECIMAL_13, "double2decimal", mono_double2decimal) /* FIXME: wrong signature. */
ICALL(DECIMAL_14, "string2decimal", mono_string2decimal)
#endif

ICALL_TYPE(DELEGATE, "System.Delegate", DELEGATE_1)
ICALL(DELEGATE_1, "CreateDelegate_internal", ves_icall_System_Delegate_CreateDelegate_internal)
ICALL(DELEGATE_2, "SetMulticastInvoke", ves_icall_System_Delegate_SetMulticastInvoke)

ICALL_TYPE(DEBUGR, "System.Diagnostics.Debugger", DEBUGR_1)
ICALL(DEBUGR_1, "IsAttached_internal", ves_icall_System_Diagnostics_Debugger_IsAttached_internal)
ICALL(DEBUGR_2, "IsLogging", ves_icall_System_Diagnostics_Debugger_IsLogging)
ICALL(DEBUGR_3, "Log", ves_icall_System_Diagnostics_Debugger_Log)

ICALL_TYPE(TRACEL, "System.Diagnostics.DefaultTraceListener", TRACEL_1)
ICALL(TRACEL_1, "WriteWindowsDebugString", ves_icall_System_Diagnostics_DefaultTraceListener_WriteWindowsDebugString)

ICALL_TYPE(FILEV, "System.Diagnostics.FileVersionInfo", FILEV_1)
ICALL(FILEV_1, "GetVersionInfo_internal(string)", ves_icall_System_Diagnostics_FileVersionInfo_GetVersionInfo_internal)

#ifndef DISABLE_PROCESS_HANDLING
ICALL_TYPE(PERFCTR, "System.Diagnostics.PerformanceCounter", PERFCTR_1)
ICALL(PERFCTR_1, "FreeData", mono_perfcounter_free_data)
ICALL(PERFCTR_2, "GetImpl", mono_perfcounter_get_impl)
ICALL(PERFCTR_3, "GetSample", mono_perfcounter_get_sample)
ICALL(PERFCTR_4, "UpdateValue", mono_perfcounter_update_value)

ICALL_TYPE(PERFCTRCAT, "System.Diagnostics.PerformanceCounterCategory", PERFCTRCAT_1)
ICALL(PERFCTRCAT_1, "CategoryDelete", mono_perfcounter_category_del)
ICALL(PERFCTRCAT_2, "CategoryHelpInternal",   mono_perfcounter_category_help)
ICALL(PERFCTRCAT_3, "CounterCategoryExists", mono_perfcounter_category_exists)
ICALL(PERFCTRCAT_4, "Create",         mono_perfcounter_create)
ICALL(PERFCTRCAT_5, "GetCategoryNames", mono_perfcounter_category_names)
ICALL(PERFCTRCAT_6, "GetCounterNames", mono_perfcounter_counter_names)
ICALL(PERFCTRCAT_7, "GetInstanceNames", mono_perfcounter_instance_names)
ICALL(PERFCTRCAT_8, "InstanceExistsInternal", mono_perfcounter_instance_exists)

ICALL_TYPE(PROCESS, "System.Diagnostics.Process", PROCESS_1)
ICALL(PROCESS_1, "CreateProcess_internal(System.Diagnostics.ProcessStartInfo,intptr,intptr,intptr,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_CreateProcess_internal)
ICALL(PROCESS_2, "ExitCode_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitCode_internal)
ICALL(PROCESS_3, "ExitTime_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitTime_internal)
ICALL(PROCESS_4, "GetModules_internal(intptr)", ves_icall_System_Diagnostics_Process_GetModules_internal)
ICALL(PROCESS_5, "GetPid_internal()", ves_icall_System_Diagnostics_Process_GetPid_internal)
ICALL(PROCESS_5B, "GetPriorityClass(intptr,int&)", ves_icall_System_Diagnostics_Process_GetPriorityClass)
ICALL(PROCESS_5H, "GetProcessData", ves_icall_System_Diagnostics_Process_GetProcessData)
ICALL(PROCESS_6, "GetProcess_internal(int)", ves_icall_System_Diagnostics_Process_GetProcess_internal)
ICALL(PROCESS_7, "GetProcesses_internal()", ves_icall_System_Diagnostics_Process_GetProcesses_internal)
ICALL(PROCESS_8, "GetWorkingSet_internal(intptr,int&,int&)", ves_icall_System_Diagnostics_Process_GetWorkingSet_internal)
ICALL(PROCESS_9, "Kill_internal", ves_icall_System_Diagnostics_Process_Kill_internal)
ICALL(PROCESS_10, "ProcessName_internal(intptr)", ves_icall_System_Diagnostics_Process_ProcessName_internal)
ICALL(PROCESS_11, "Process_free_internal(intptr)", ves_icall_System_Diagnostics_Process_Process_free_internal)
ICALL(PROCESS_11B, "SetPriorityClass(intptr,int,int&)", ves_icall_System_Diagnostics_Process_SetPriorityClass)
ICALL(PROCESS_12, "SetWorkingSet_internal(intptr,int,int,bool)", ves_icall_System_Diagnostics_Process_SetWorkingSet_internal)
ICALL(PROCESS_13, "ShellExecuteEx_internal(System.Diagnostics.ProcessStartInfo,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_ShellExecuteEx_internal)
ICALL(PROCESS_14, "StartTime_internal(intptr)", ves_icall_System_Diagnostics_Process_StartTime_internal)
ICALL(PROCESS_14M, "Times", ves_icall_System_Diagnostics_Process_Times)
ICALL(PROCESS_15, "WaitForExit_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForExit_internal)
ICALL(PROCESS_16, "WaitForInputIdle_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForInputIdle_internal)

ICALL_TYPE (PROCESSHANDLE, "System.Diagnostics.Process/ProcessWaitHandle", PROCESSHANDLE_1)
ICALL (PROCESSHANDLE_1, "ProcessHandle_close(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_close)
ICALL (PROCESSHANDLE_2, "ProcessHandle_duplicate(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_duplicate)
#endif /* !DISABLE_PROCESS_HANDLING */

ICALL_TYPE(STOPWATCH, "System.Diagnostics.Stopwatch", STOPWATCH_1)
ICALL(STOPWATCH_1, "GetTimestamp", mono_100ns_ticks)

ICALL_TYPE(DOUBLE, "System.Double", DOUBLE_1)
ICALL(DOUBLE_1, "ParseImpl",    mono_double_ParseImpl)

ICALL_TYPE(ENUM, "System.Enum", ENUM_1)
ICALL(ENUM_1, "ToObject", ves_icall_System_Enum_ToObject)
ICALL(ENUM_5, "compare_value_to", ves_icall_System_Enum_compare_value_to)
ICALL(ENUM_4, "get_hashcode", ves_icall_System_Enum_get_hashcode)
ICALL(ENUM_3, "get_underlying_type", ves_icall_System_Enum_get_underlying_type)
ICALL(ENUM_2, "get_value", ves_icall_System_Enum_get_value)

ICALL_TYPE(ENV, "System.Environment", ENV_1)
ICALL(ENV_1, "Exit", ves_icall_System_Environment_Exit)
ICALL(ENV_2, "GetCommandLineArgs", mono_runtime_get_main_args)
ICALL(ENV_3, "GetEnvironmentVariableNames", ves_icall_System_Environment_GetEnvironmentVariableNames)
ICALL(ENV_4, "GetLogicalDrivesInternal", ves_icall_System_Environment_GetLogicalDrives )
ICALL(ENV_5, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(ENV_51, "GetNewLine", ves_icall_System_Environment_get_NewLine)
ICALL(ENV_6, "GetOSVersionString", ves_icall_System_Environment_GetOSVersionString)
ICALL(ENV_6a, "GetPageSize", mono_pagesize)
ICALL(ENV_7, "GetWindowsFolderPath", ves_icall_System_Environment_GetWindowsFolderPath)
ICALL(ENV_8, "InternalSetEnvironmentVariable", ves_icall_System_Environment_InternalSetEnvironmentVariable)
ICALL(ENV_9, "get_ExitCode", mono_environment_exitcode_get)
ICALL(ENV_10, "get_HasShutdownStarted", ves_icall_System_Environment_get_HasShutdownStarted)
ICALL(ENV_11, "get_MachineName", ves_icall_System_Environment_get_MachineName)
ICALL(ENV_13, "get_Platform", ves_icall_System_Environment_get_Platform)
ICALL(ENV_14, "get_ProcessorCount", mono_cpu_count)
ICALL(ENV_15, "get_TickCount", mono_msec_ticks)
ICALL(ENV_16, "get_UserName", ves_icall_System_Environment_get_UserName)
ICALL(ENV_16m, "internalBroadcastSettingChange", ves_icall_System_Environment_BroadcastSettingChange)
ICALL(ENV_17, "internalGetEnvironmentVariable", ves_icall_System_Environment_GetEnvironmentVariable)
ICALL(ENV_18, "internalGetGacPath", ves_icall_System_Environment_GetGacPath)
ICALL(ENV_19, "internalGetHome", ves_icall_System_Environment_InternalGetHome)
ICALL(ENV_20, "set_ExitCode", mono_environment_exitcode_set)

ICALL_TYPE(GC, "System.GC", GC_0)
ICALL(GC_0, "CollectionCount", mono_gc_collection_count)
ICALL(GC_0a, "GetGeneration", mono_gc_get_generation)
ICALL(GC_1, "GetTotalMemory", ves_icall_System_GC_GetTotalMemory)
ICALL(GC_2, "InternalCollect", ves_icall_System_GC_InternalCollect)
ICALL(GC_3, "KeepAlive", ves_icall_System_GC_KeepAlive)
ICALL(GC_4, "ReRegisterForFinalize", ves_icall_System_GC_ReRegisterForFinalize)
ICALL(GC_4a, "RecordPressure", mono_gc_add_memory_pressure)
ICALL(GC_5, "SuppressFinalize", ves_icall_System_GC_SuppressFinalize)
ICALL(GC_6, "WaitForPendingFinalizers", ves_icall_System_GC_WaitForPendingFinalizers)
ICALL(GC_7, "get_MaxGeneration", mono_gc_max_generation)
ICALL(GC_9, "get_ephemeron_tombstone", ves_icall_System_GC_get_ephemeron_tombstone)
ICALL(GC_8, "register_ephemeron_array", ves_icall_System_GC_register_ephemeron_array)

ICALL_TYPE(COMPINF, "System.Globalization.CompareInfo", COMPINF_1)
ICALL(COMPINF_1, "assign_sortkey(object,string,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_assign_sortkey)
ICALL(COMPINF_2, "construct_compareinfo(string)", ves_icall_System_Globalization_CompareInfo_construct_compareinfo)
ICALL(COMPINF_3, "free_internal_collator()", ves_icall_System_Globalization_CompareInfo_free_internal_collator)
ICALL(COMPINF_4, "internal_compare(string,int,int,string,int,int,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_internal_compare)
ICALL(COMPINF_5, "internal_index(string,int,int,char,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index_char)
ICALL(COMPINF_6, "internal_index(string,int,int,string,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index)

ICALL_TYPE(CULINF, "System.Globalization.CultureInfo", CULINF_2)
ICALL(CULINF_2, "construct_datetime_format", ves_icall_System_Globalization_CultureInfo_construct_datetime_format)
ICALL(CULINF_4, "construct_internal_locale_from_current_locale", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_current_locale)
ICALL(CULINF_5, "construct_internal_locale_from_lcid", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_lcid)
ICALL(CULINF_6, "construct_internal_locale_from_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_name)
ICALL(CULINF_7, "construct_internal_locale_from_specific_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_specific_name)
ICALL(CULINF_8, "construct_number_format", ves_icall_System_Globalization_CultureInfo_construct_number_format)
ICALL(CULINF_9, "internal_get_cultures", ves_icall_System_Globalization_CultureInfo_internal_get_cultures)
//ICALL(CULINF_10, "internal_is_lcid_neutral", ves_icall_System_Globalization_CultureInfo_internal_is_lcid_neutral)

ICALL_TYPE(REGINF, "System.Globalization.RegionInfo", REGINF_1)
ICALL(REGINF_1, "construct_internal_region_from_lcid", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_lcid)
ICALL(REGINF_2, "construct_internal_region_from_name", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_name)

#ifndef PLATFORM_NO_DRIVEINFO
ICALL_TYPE(IODRIVEINFO, "System.IO.DriveInfo", IODRIVEINFO_1)
ICALL(IODRIVEINFO_1, "GetDiskFreeSpaceInternal", ves_icall_System_IO_DriveInfo_GetDiskFreeSpace)
ICALL(IODRIVEINFO_2, "GetDriveFormat", ves_icall_System_IO_DriveInfo_GetDriveFormat)
ICALL(IODRIVEINFO_3, "GetDriveTypeInternal", ves_icall_System_IO_DriveInfo_GetDriveType)
#endif

ICALL_TYPE(FAMW, "System.IO.FAMWatcher", FAMW_1)
ICALL(FAMW_1, "InternalFAMNextEvent", ves_icall_System_IO_FAMW_InternalFAMNextEvent)

ICALL_TYPE(FILEW, "System.IO.FileSystemWatcher", FILEW_4)
ICALL(FILEW_4, "InternalSupportsFSW", ves_icall_System_IO_FSW_SupportsFSW)

ICALL_TYPE(INOW, "System.IO.InotifyWatcher", INOW_1)
ICALL(INOW_1, "AddWatch", ves_icall_System_IO_InotifyWatcher_AddWatch)
ICALL(INOW_2, "GetInotifyInstance", ves_icall_System_IO_InotifyWatcher_GetInotifyInstance)
ICALL(INOW_3, "RemoveWatch", ves_icall_System_IO_InotifyWatcher_RemoveWatch)

#if defined (TARGET_IOS) || defined (TARGET_ANDROID)
ICALL_TYPE(MMAPIMPL, "System.IO.MemoryMappedFiles.MemoryMapImpl", MMAPIMPL_1)
ICALL(MMAPIMPL_1, "mono_filesize_from_fd", mono_filesize_from_fd)
ICALL(MMAPIMPL_2, "mono_filesize_from_path", mono_filesize_from_path)
#endif


ICALL_TYPE(MONOIO, "System.IO.MonoIO", MONOIO_1)
ICALL(MONOIO_1, "Close(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Close)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_2, "CopyFile(string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CopyFile)
ICALL(MONOIO_3, "CreateDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CreateDirectory)
ICALL(MONOIO_4, "CreatePipe(intptr&,intptr&)", ves_icall_System_IO_MonoIO_CreatePipe)
ICALL(MONOIO_5, "DeleteFile(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_DeleteFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_34, "DuplicateHandle", ves_icall_System_IO_MonoIO_DuplicateHandle)
ICALL(MONOIO_37, "FindClose", ves_icall_System_IO_MonoIO_FindClose)
ICALL(MONOIO_35, "FindFirst", ves_icall_System_IO_MonoIO_FindFirst)
ICALL(MONOIO_36, "FindNext", ves_icall_System_IO_MonoIO_FindNext)
ICALL(MONOIO_6, "Flush(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Flush)
ICALL(MONOIO_7, "GetCurrentDirectory(System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetCurrentDirectory)
ICALL(MONOIO_8, "GetFileAttributes(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileAttributes)
ICALL(MONOIO_9, "GetFileStat(string,System.IO.MonoIOStat&,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileStat)
ICALL(MONOIO_10, "GetFileSystemEntries", ves_icall_System_IO_MonoIO_GetFileSystemEntries)
ICALL(MONOIO_11, "GetFileType(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileType)
ICALL(MONOIO_12, "GetLength(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_13, "GetTempPath(string&)", ves_icall_System_IO_MonoIO_GetTempPath)
ICALL(MONOIO_14, "Lock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Lock)
ICALL(MONOIO_15, "MoveFile(string,string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_MoveFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_16, "Open(string,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.IO.FileOptions,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Open)
ICALL(MONOIO_17, "Read(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Read)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_18, "RemoveDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_RemoveDirectory)
ICALL(MONOIO_18M, "ReplaceFile(string,string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_ReplaceFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_19, "Seek(intptr,long,System.IO.SeekOrigin,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Seek)
ICALL(MONOIO_20, "SetCurrentDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetCurrentDirectory)
ICALL(MONOIO_21, "SetFileAttributes(string,System.IO.FileAttributes,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileAttributes)
ICALL(MONOIO_22, "SetFileTime(intptr,long,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileTime)
ICALL(MONOIO_23, "SetLength(intptr,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_24, "Unlock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Unlock)
#endif
ICALL(MONOIO_25, "Write(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Write)
ICALL(MONOIO_26, "get_AltDirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_AltDirectorySeparatorChar)
ICALL(MONOIO_27, "get_ConsoleError", ves_icall_System_IO_MonoIO_get_ConsoleError)
ICALL(MONOIO_28, "get_ConsoleInput", ves_icall_System_IO_MonoIO_get_ConsoleInput)
ICALL(MONOIO_29, "get_ConsoleOutput", ves_icall_System_IO_MonoIO_get_ConsoleOutput)
ICALL(MONOIO_30, "get_DirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_DirectorySeparatorChar)
ICALL(MONOIO_31, "get_InvalidPathChars", ves_icall_System_IO_MonoIO_get_InvalidPathChars)
ICALL(MONOIO_32, "get_PathSeparator", ves_icall_System_IO_MonoIO_get_PathSeparator)
ICALL(MONOIO_33, "get_VolumeSeparatorChar", ves_icall_System_IO_MonoIO_get_VolumeSeparatorChar)

ICALL_TYPE(IOPATH, "System.IO.Path", IOPATH_1)
ICALL(IOPATH_1, "get_temp_path", ves_icall_System_IO_get_temp_path)

ICALL_TYPE(MATH, "System.Math", MATH_1)
ICALL(MATH_1, "Acos", ves_icall_System_Math_Acos)
ICALL(MATH_2, "Asin", ves_icall_System_Math_Asin)
ICALL(MATH_3, "Atan", ves_icall_System_Math_Atan)
ICALL(MATH_4, "Atan2", ves_icall_System_Math_Atan2)
ICALL(MATH_5, "Cos", ves_icall_System_Math_Cos)
ICALL(MATH_6, "Cosh", ves_icall_System_Math_Cosh)
ICALL(MATH_7, "Exp", ves_icall_System_Math_Exp)
ICALL(MATH_8, "Floor", ves_icall_System_Math_Floor)
ICALL(MATH_9, "Log", ves_icall_System_Math_Log)
ICALL(MATH_10, "Log10", ves_icall_System_Math_Log10)
ICALL(MATH_11, "Pow", ves_icall_System_Math_Pow)
ICALL(MATH_12, "Round", ves_icall_System_Math_Round)
ICALL(MATH_13, "Round2", ves_icall_System_Math_Round2)
ICALL(MATH_14, "Sin", ves_icall_System_Math_Sin)
ICALL(MATH_15, "Sinh", ves_icall_System_Math_Sinh)
ICALL(MATH_16, "Sqrt", ves_icall_System_Math_Sqrt)
ICALL(MATH_17, "Tan", ves_icall_System_Math_Tan)
ICALL(MATH_18, "Tanh", ves_icall_System_Math_Tanh)

ICALL_TYPE(MCATTR, "System.MonoCustomAttrs", MCATTR_1)
ICALL(MCATTR_1, "GetCustomAttributesDataInternal", mono_reflection_get_custom_attrs_data)
ICALL(MCATTR_2, "GetCustomAttributesInternal", custom_attrs_get_by_type)
ICALL(MCATTR_3, "IsDefinedInternal", custom_attrs_defined_internal)

ICALL_TYPE(MENUM, "System.MonoEnumInfo", MENUM_1)
ICALL(MENUM_1, "get_enum_info", ves_icall_get_enum_info)

ICALL_TYPE(MTYPE, "System.MonoType", MTYPE_1)
ICALL(MTYPE_1, "GetArrayRank", ves_icall_MonoType_GetArrayRank)
ICALL(MTYPE_2, "GetConstructors", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_3, "GetConstructors_internal", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_4, "GetCorrespondingInflatedConstructor", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_5, "GetCorrespondingInflatedMethod", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_6, "GetElementType", ves_icall_MonoType_GetElementType)
ICALL(MTYPE_7, "GetEvents_internal", ves_icall_Type_GetEvents_internal)
ICALL(MTYPE_8, "GetField", ves_icall_Type_GetField)
ICALL(MTYPE_9, "GetFields_internal", ves_icall_Type_GetFields_internal)
ICALL(MTYPE_10, "GetGenericArguments", ves_icall_MonoType_GetGenericArguments)
ICALL(MTYPE_11, "GetInterfaces", ves_icall_Type_GetInterfaces)
ICALL(MTYPE_12, "GetMethodsByName", ves_icall_Type_GetMethodsByName)
ICALL(MTYPE_13, "GetNestedType", ves_icall_Type_GetNestedType)
ICALL(MTYPE_14, "GetNestedTypes", ves_icall_Type_GetNestedTypes)
ICALL(MTYPE_15, "GetPropertiesByName", ves_icall_Type_GetPropertiesByName)
ICALL(MTYPE_16, "InternalGetEvent", ves_icall_MonoType_GetEvent)
ICALL(MTYPE_17, "IsByRefImpl", ves_icall_type_isbyref)
ICALL(MTYPE_18, "IsCOMObjectImpl", ves_icall_type_iscomobject)
ICALL(MTYPE_19, "IsPointerImpl", ves_icall_type_ispointer)
ICALL(MTYPE_20, "IsPrimitiveImpl", ves_icall_type_isprimitive)
ICALL(MTYPE_21, "getFullName", ves_icall_System_MonoType_getFullName)
ICALL(MTYPE_22, "get_Assembly", ves_icall_MonoType_get_Assembly)
ICALL(MTYPE_23, "get_BaseType", ves_icall_get_type_parent)
ICALL(MTYPE_24, "get_DeclaringMethod", ves_icall_MonoType_get_DeclaringMethod)
ICALL(MTYPE_25, "get_DeclaringType", ves_icall_MonoType_get_DeclaringType)
ICALL(MTYPE_26, "get_IsGenericParameter", ves_icall_MonoType_get_IsGenericParameter)
ICALL(MTYPE_27, "get_Module", ves_icall_MonoType_get_Module)
ICALL(MTYPE_28, "get_Name", ves_icall_MonoType_get_Name)
ICALL(MTYPE_29, "get_Namespace", ves_icall_MonoType_get_Namespace)
ICALL(MTYPE_31, "get_attributes", ves_icall_get_attributes)
ICALL(MTYPE_33, "get_core_clr_security_level", vell_icall_MonoType_get_core_clr_security_level)
ICALL(MTYPE_32, "type_from_obj", mono_type_type_from_obj)

#ifndef DISABLE_SOCKETS
ICALL_TYPE(NDNS, "System.Net.Dns", NDNS_1)
ICALL(NDNS_1, "GetHostByAddr_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByAddr_internal)
ICALL(NDNS_2, "GetHostByName_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByName_internal)
ICALL(NDNS_3, "GetHostName_internal(string&)", ves_icall_System_Net_Dns_GetHostName_internal)

ICALL_TYPE(SOCK, "System.Net.Sockets.Socket", SOCK_1)
ICALL(SOCK_1, "Accept_internal(intptr,int&,bool)", ves_icall_System_Net_Sockets_Socket_Accept_internal)
ICALL(SOCK_2, "Available_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Available_internal)
ICALL(SOCK_3, "Bind_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Bind_internal)
ICALL(SOCK_4, "Blocking_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Blocking_internal)
ICALL(SOCK_5, "Close_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Close_internal)
ICALL(SOCK_6, "Connect_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Connect_internal)
ICALL (SOCK_6a, "Disconnect_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Disconnect_internal)
ICALL(SOCK_7, "GetSocketOption_arr_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,byte[]&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_arr_internal)
ICALL(SOCK_8, "GetSocketOption_obj_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_obj_internal)
ICALL(SOCK_9, "Listen_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_Listen_internal)
ICALL(SOCK_10, "LocalEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_LocalEndPoint_internal)
ICALL(SOCK_11, "Poll_internal", ves_icall_System_Net_Sockets_Socket_Poll_internal)
ICALL(SOCK_11a, "Receive_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_array_internal)
ICALL(SOCK_12, "Receive_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_internal)
ICALL(SOCK_13, "RecvFrom_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress&,int&)", ves_icall_System_Net_Sockets_Socket_RecvFrom_internal)
ICALL(SOCK_14, "RemoteEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_RemoteEndPoint_internal)
ICALL(SOCK_15, "Select_internal(System.Net.Sockets.Socket[]&,int,int&)", ves_icall_System_Net_Sockets_Socket_Select_internal)
ICALL(SOCK_15a, "SendFile(intptr,string,byte[],byte[],System.Net.Sockets.TransmitFileOptions)", ves_icall_System_Net_Sockets_Socket_SendFile)
ICALL(SOCK_16, "SendTo_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_SendTo_internal)
ICALL(SOCK_16a, "Send_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_array_internal)
ICALL(SOCK_17, "Send_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_internal)
ICALL(SOCK_18, "SetSocketOption_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object,byte[],int,int&)", ves_icall_System_Net_Sockets_Socket_SetSocketOption_internal)
ICALL(SOCK_19, "Shutdown_internal(intptr,System.Net.Sockets.SocketShutdown,int&)", ves_icall_System_Net_Sockets_Socket_Shutdown_internal)
ICALL(SOCK_20, "Socket_internal(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType,int&)", ves_icall_System_Net_Sockets_Socket_Socket_internal)
ICALL(SOCK_21, "WSAIoctl(intptr,int,byte[],byte[],int&)", ves_icall_System_Net_Sockets_Socket_WSAIoctl)
ICALL(SOCK_21a, "cancel_blocking_socket_operation", icall_cancel_blocking_socket_operation)
ICALL(SOCK_22, "socket_pool_queue", icall_append_io_job)

ICALL_TYPE(SOCKEX, "System.Net.Sockets.SocketException", SOCKEX_1)
ICALL(SOCKEX_1, "WSAGetLastError_internal", ves_icall_System_Net_Sockets_SocketException_WSAGetLastError_internal)
#endif /* !DISABLE_SOCKETS */

ICALL_TYPE(NUMBER_FORMATTER, "System.NumberFormatter", NUMBER_FORMATTER_1)
ICALL(NUMBER_FORMATTER_1, "GetFormatterTables", ves_icall_System_NumberFormatter_GetFormatterTables)

ICALL_TYPE(OBJ, "System.Object", OBJ_1)
ICALL(OBJ_1, "GetType", ves_icall_System_Object_GetType)
ICALL(OBJ_2, "InternalGetHashCode", mono_object_hash)
ICALL(OBJ_3, "MemberwiseClone", ves_icall_System_Object_MemberwiseClone)
ICALL(OBJ_4, "obj_address", ves_icall_System_Object_obj_address)

ICALL_TYPE(ASSEM, "System.Reflection.Assembly", ASSEM_1)
ICALL(ASSEM_1, "FillName", ves_icall_System_Reflection_Assembly_FillName)
ICALL(ASSEM_2, "GetCallingAssembly", ves_icall_System_Reflection_Assembly_GetCallingAssembly)
ICALL(ASSEM_3, "GetEntryAssembly", ves_icall_System_Reflection_Assembly_GetEntryAssembly)
ICALL(ASSEM_4, "GetExecutingAssembly", ves_icall_System_Reflection_Assembly_GetExecutingAssembly)
ICALL(ASSEM_5, "GetFilesInternal", ves_icall_System_Reflection_Assembly_GetFilesInternal)
ICALL(ASSEM_6, "GetManifestModuleInternal", ves_icall_System_Reflection_Assembly_GetManifestModuleInternal)
ICALL(ASSEM_7, "GetManifestResourceInfoInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInfoInternal)
ICALL(ASSEM_8, "GetManifestResourceInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInternal)
ICALL(ASSEM_9, "GetManifestResourceNames", ves_icall_System_Reflection_Assembly_GetManifestResourceNames)
ICALL(ASSEM_10, "GetModulesInternal", ves_icall_System_Reflection_Assembly_GetModulesInternal)
//ICALL(ASSEM_11, "GetNamespaces", ves_icall_System_Reflection_Assembly_GetNamespaces)
ICALL(ASSEM_12, "GetReferencedAssemblies", ves_icall_System_Reflection_Assembly_GetReferencedAssemblies)
ICALL(ASSEM_13, "GetTypes", ves_icall_System_Reflection_Assembly_GetTypes)
ICALL(ASSEM_14, "InternalGetAssemblyName", ves_icall_System_Reflection_Assembly_InternalGetAssemblyName)
ICALL(ASSEM_15, "InternalGetType", ves_icall_System_Reflection_Assembly_InternalGetType)
ICALL(ASSEM_16, "InternalImageRuntimeVersion", ves_icall_System_Reflection_Assembly_InternalImageRuntimeVersion)
ICALL(ASSEM_17, "LoadFrom", ves_icall_System_Reflection_Assembly_LoadFrom)
ICALL(ASSEM_18, "LoadPermissions", ves_icall_System_Reflection_Assembly_LoadPermissions)
	/*
	 * Private icalls for the Mono Debugger
	 */
ICALL(ASSEM_19, "MonoDebugger_GetMethodToken", ves_icall_MonoDebugger_GetMethodToken)

	/* normal icalls again */
ICALL(ASSEM_20, "get_EntryPoint", ves_icall_System_Reflection_Assembly_get_EntryPoint)
ICALL(ASSEM_21, "get_ReflectionOnly", ves_icall_System_Reflection_Assembly_get_ReflectionOnly)
ICALL(ASSEM_22, "get_code_base", ves_icall_System_Reflection_Assembly_get_code_base)
ICALL(ASSEM_23, "get_fullname", ves_icall_System_Reflection_Assembly_get_fullName)
ICALL(ASSEM_24, "get_global_assembly_cache", ves_icall_System_Reflection_Assembly_get_global_assembly_cache)
ICALL(ASSEM_25, "get_location", ves_icall_System_Reflection_Assembly_get_location)
ICALL(ASSEM_26, "load_with_partial_name", ves_icall_System_Reflection_Assembly_load_with_partial_name)

ICALL_TYPE(ASSEMN, "System.Reflection.AssemblyName", ASSEMN_1)
ICALL(ASSEMN_1, "ParseName", ves_icall_System_Reflection_AssemblyName_ParseName)
ICALL(ASSEMN_2, "get_public_token", mono_digest_get_public_token)

ICALL_TYPE(CATTR_DATA, "System.Reflection.CustomAttributeData", CATTR_DATA_1)
ICALL(CATTR_DATA_1, "ResolveArgumentsInternal", mono_reflection_resolve_custom_attribute_data)

ICALL_TYPE(ASSEMB, "System.Reflection.Emit.AssemblyBuilder", ASSEMB_1)
ICALL(ASSEMB_1, "InternalAddModule", mono_image_load_module_dynamic)
ICALL(ASSEMB_2, "basic_init", mono_image_basic_init)

ICALL_TYPE(CATTRB, "System.Reflection.Emit.CustomAttributeBuilder", CATTRB_1)
ICALL(CATTRB_1, "GetBlob", mono_reflection_get_custom_attrs_blob)

#ifndef DISABLE_REFLECTION_EMIT
ICALL_TYPE(DERIVEDTYPE, "System.Reflection.Emit.DerivedType", DERIVEDTYPE_1)
ICALL(DERIVEDTYPE_1, "create_unmanaged_type", mono_reflection_create_unmanaged_type)
#endif

ICALL_TYPE(DYNM, "System.Reflection.Emit.DynamicMethod", DYNM_1)
ICALL(DYNM_1, "create_dynamic_method", mono_reflection_create_dynamic_method)

ICALL_TYPE(ENUMB, "System.Reflection.Emit.EnumBuilder", ENUMB_1)
ICALL(ENUMB_1, "setup_enum_type", ves_icall_EnumBuilder_setup_enum_type)

ICALL_TYPE(GPARB, "System.Reflection.Emit.GenericTypeParameterBuilder", GPARB_1)
ICALL(GPARB_1, "initialize", mono_reflection_initialize_generic_parameter)

ICALL_TYPE(METHODB, "System.Reflection.Emit.MethodBuilder", METHODB_1)
ICALL(METHODB_1, "MakeGenericMethod", mono_reflection_bind_generic_method_parameters)

ICALL_TYPE(MODULEB, "System.Reflection.Emit.ModuleBuilder", MODULEB_8)
ICALL(MODULEB_8, "RegisterToken", ves_icall_ModuleBuilder_RegisterToken)
ICALL(MODULEB_1, "WriteToFile", ves_icall_ModuleBuilder_WriteToFile)
ICALL(MODULEB_2, "basic_init", mono_image_module_basic_init)
ICALL(MODULEB_3, "build_metadata", ves_icall_ModuleBuilder_build_metadata)
ICALL(MODULEB_4, "create_modified_type", ves_icall_ModuleBuilder_create_modified_type)
ICALL(MODULEB_5, "getMethodToken", ves_icall_ModuleBuilder_getMethodToken)
ICALL(MODULEB_6, "getToken", ves_icall_ModuleBuilder_getToken)
ICALL(MODULEB_7, "getUSIndex", mono_image_insert_string)
ICALL(MODULEB_9, "set_wrappers_type", mono_image_set_wrappers_type)

ICALL_TYPE(SIGH, "System.Reflection.Emit.SignatureHelper", SIGH_1)
ICALL(SIGH_1, "get_signature_field", mono_reflection_sighelper_get_signature_field)
ICALL(SIGH_2, "get_signature_local", mono_reflection_sighelper_get_signature_local)

ICALL_TYPE(TYPEB, "System.Reflection.Emit.TypeBuilder", TYPEB_1)
ICALL(TYPEB_1, "create_generic_class", mono_reflection_create_generic_class)
ICALL(TYPEB_2, "create_internal_class", mono_reflection_create_internal_class)
ICALL(TYPEB_3, "create_runtime_class", mono_reflection_create_runtime_class)
ICALL(TYPEB_4, "get_IsGenericParameter", ves_icall_TypeBuilder_get_IsGenericParameter)
ICALL(TYPEB_5, "get_event_info", mono_reflection_event_builder_get_event_info)
ICALL(TYPEB_6, "setup_generic_class", mono_reflection_setup_generic_class)
ICALL(TYPEB_7, "setup_internal_class", mono_reflection_setup_internal_class)

ICALL_TYPE(FIELDI, "System.Reflection.FieldInfo", FILEDI_1)
ICALL(FILEDI_1, "GetTypeModifiers", ves_icall_System_Reflection_FieldInfo_GetTypeModifiers)
ICALL(FILEDI_2, "get_marshal_info", ves_icall_System_Reflection_FieldInfo_get_marshal_info)
ICALL(FILEDI_3, "internal_from_handle_type", ves_icall_System_Reflection_FieldInfo_internal_from_handle_type)

ICALL_TYPE(MEMBERI, "System.Reflection.MemberInfo", MEMBERI_1)
ICALL(MEMBERI_1, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MBASE, "System.Reflection.MethodBase", MBASE_1)
ICALL(MBASE_1, "GetCurrentMethod", ves_icall_GetCurrentMethod)
ICALL(MBASE_2, "GetMethodBodyInternal", ves_icall_System_Reflection_MethodBase_GetMethodBodyInternal)
ICALL(MBASE_3, "GetMethodFromHandleInternal", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternal)
ICALL(MBASE_4, "GetMethodFromHandleInternalType", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternalType)

ICALL_TYPE(MODULE, "System.Reflection.Module", MODULE_1)
ICALL(MODULE_1, "Close", ves_icall_System_Reflection_Module_Close)
ICALL(MODULE_2, "GetGlobalType", ves_icall_System_Reflection_Module_GetGlobalType)
ICALL(MODULE_3, "GetGuidInternal", ves_icall_System_Reflection_Module_GetGuidInternal)
ICALL(MODULE_14, "GetHINSTANCE", ves_icall_System_Reflection_Module_GetHINSTANCE)
ICALL(MODULE_4, "GetMDStreamVersion", ves_icall_System_Reflection_Module_GetMDStreamVersion)
ICALL(MODULE_5, "GetPEKind", ves_icall_System_Reflection_Module_GetPEKind)
ICALL(MODULE_6, "InternalGetTypes", ves_icall_System_Reflection_Module_InternalGetTypes)
ICALL(MODULE_7, "ResolveFieldToken", ves_icall_System_Reflection_Module_ResolveFieldToken)
ICALL(MODULE_8, "ResolveMemberToken", ves_icall_System_Reflection_Module_ResolveMemberToken)
ICALL(MODULE_9, "ResolveMethodToken", ves_icall_System_Reflection_Module_ResolveMethodToken)
ICALL(MODULE_10, "ResolveSignature", ves_icall_System_Reflection_Module_ResolveSignature)
ICALL(MODULE_11, "ResolveStringToken", ves_icall_System_Reflection_Module_ResolveStringToken)
ICALL(MODULE_12, "ResolveTypeToken", ves_icall_System_Reflection_Module_ResolveTypeToken)
ICALL(MODULE_13, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MCMETH, "System.Reflection.MonoCMethod", MCMETH_1)
ICALL(MCMETH_1, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MCMETH_2, "InternalInvoke", ves_icall_InternalInvoke)

ICALL_TYPE(MEVIN, "System.Reflection.MonoEventInfo", MEVIN_1)
ICALL(MEVIN_1, "get_event_info", ves_icall_get_event_info)

ICALL_TYPE(MFIELD, "System.Reflection.MonoField", MFIELD_1)
ICALL(MFIELD_1, "GetFieldOffset", ves_icall_MonoField_GetFieldOffset)
ICALL(MFIELD_2, "GetParentType", ves_icall_MonoField_GetParentType)
ICALL(MFIELD_5, "GetRawConstantValue", ves_icall_MonoField_GetRawConstantValue)
ICALL(MFIELD_3, "GetValueInternal", ves_icall_MonoField_GetValueInternal)
ICALL(MFIELD_6, "ResolveType", ves_icall_MonoField_ResolveType)
ICALL(MFIELD_4, "SetValueInternal", ves_icall_MonoField_SetValueInternal)

ICALL_TYPE(MGENCM, "System.Reflection.MonoGenericCMethod", MGENCM_1)
ICALL(MGENCM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MGENCL, "System.Reflection.MonoGenericClass", MGENCL_5)
ICALL(MGENCL_5, "initialize", mono_reflection_generic_class_initialize)
ICALL(MGENCL_6, "register_with_runtime", mono_reflection_register_with_runtime)

/* note this is the same as above: unify */
ICALL_TYPE(MGENM, "System.Reflection.MonoGenericMethod", MGENM_1)
ICALL(MGENM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MMETH, "System.Reflection.MonoMethod", MMETH_1)
ICALL(MMETH_1, "GetDllImportAttribute", ves_icall_MonoMethod_GetDllImportAttribute)
ICALL(MMETH_2, "GetGenericArguments", ves_icall_MonoMethod_GetGenericArguments)
ICALL(MMETH_3, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MMETH_4, "InternalInvoke", ves_icall_InternalInvoke)
ICALL(MMETH_5, "MakeGenericMethod_impl", mono_reflection_bind_generic_method_parameters)
ICALL(MMETH_6, "get_IsGenericMethod", ves_icall_MonoMethod_get_IsGenericMethod)
ICALL(MMETH_7, "get_IsGenericMethodDefinition", ves_icall_MonoMethod_get_IsGenericMethodDefinition)
ICALL(MMETH_8, "get_base_method", ves_icall_MonoMethod_get_base_method)
ICALL(MMETH_9, "get_name", ves_icall_MonoMethod_get_name)

ICALL_TYPE(MMETHI, "System.Reflection.MonoMethodInfo", MMETHI_4)
ICALL(MMETHI_4, "get_method_attributes", vell_icall_get_method_attributes)
ICALL(MMETHI_1, "get_method_info", ves_icall_get_method_info)
ICALL(MMETHI_2, "get_parameter_info", ves_icall_get_parameter_info)
ICALL(MMETHI_3, "get_retval_marshal", ves_icall_System_MonoMethodInfo_get_retval_marshal)

ICALL_TYPE(MPROPI, "System.Reflection.MonoPropertyInfo", MPROPI_1)
ICALL(MPROPI_1, "GetTypeModifiers", property_info_get_type_modifiers)
ICALL(MPROPI_3, "get_default_value", property_info_get_default_value)
ICALL(MPROPI_2, "get_property_info", ves_icall_get_property_info)

ICALL_TYPE(PARAMI, "System.Reflection.ParameterInfo", PARAMI_1)
ICALL(PARAMI_1, "GetMetadataToken", mono_reflection_get_token)
ICALL(PARAMI_2, "GetTypeModifiers", param_info_get_type_modifiers)

ICALL_TYPE(RUNH, "System.Runtime.CompilerServices.RuntimeHelpers", RUNH_1)
ICALL(RUNH_1, "GetObjectValue", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetObjectValue)
	 /* REMOVEME: no longer needed, just so we dont break things when not needed */
ICALL(RUNH_2, "GetOffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)
ICALL(RUNH_3, "InitializeArray", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_InitializeArray)
ICALL(RUNH_4, "RunClassConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunClassConstructor)
ICALL(RUNH_5, "RunModuleConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunModuleConstructor)
ICALL(RUNH_5h, "SufficientExecutionStack", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_SufficientExecutionStack)
ICALL(RUNH_6, "get_OffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)

ICALL_TYPE(GCH, "System.Runtime.InteropServices.GCHandle", GCH_1)
ICALL(GCH_1, "CheckCurrentDomain", GCHandle_CheckCurrentDomain)
ICALL(GCH_2, "FreeHandle", ves_icall_System_GCHandle_FreeHandle)
ICALL(GCH_3, "GetAddrOfPinnedObject", ves_icall_System_GCHandle_GetAddrOfPinnedObject)
ICALL(GCH_4, "GetTarget", ves_icall_System_GCHandle_GetTarget)
ICALL(GCH_5, "GetTargetHandle", ves_icall_System_GCHandle_GetTargetHandle)

#ifndef DISABLE_COM
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_1)
ICALL(MARSHAL_1, "AddRefInternal", ves_icall_System_Runtime_InteropServices_Marshal_AddRefInternal)
#else
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_2)
#endif
ICALL(MARSHAL_2, "AllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_AllocCoTaskMem)
ICALL(MARSHAL_3, "AllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_AllocHGlobal)
ICALL(MARSHAL_4, "DestroyStructure", ves_icall_System_Runtime_InteropServices_Marshal_DestroyStructure)
ICALL(MARSHAL_5, "FreeBSTR", ves_icall_System_Runtime_InteropServices_Marshal_FreeBSTR)
ICALL(MARSHAL_6, "FreeCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_FreeCoTaskMem)
ICALL(MARSHAL_7, "FreeHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_FreeHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_44, "GetCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetCCW)
ICALL(MARSHAL_8, "GetComSlotForMethodInfoInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetComSlotForMethodInfoInternal)
#endif
ICALL(MARSHAL_9, "GetDelegateForFunctionPointerInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetDelegateForFunctionPointerInternal)
ICALL(MARSHAL_10, "GetFunctionPointerForDelegateInternal", mono_delegate_to_ftnptr)
#ifndef DISABLE_COM
ICALL(MARSHAL_45, "GetIDispatchForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIDispatchForObjectInternal)
ICALL(MARSHAL_46, "GetIUnknownForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIUnknownForObjectInternal)
#endif
ICALL(MARSHAL_11, "GetLastWin32Error", ves_icall_System_Runtime_InteropServices_Marshal_GetLastWin32Error)
#ifndef DISABLE_COM
ICALL(MARSHAL_47, "GetObjectForCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetObjectForCCW)
ICALL(MARSHAL_48, "IsComObject", ves_icall_System_Runtime_InteropServices_Marshal_IsComObject)
#endif
ICALL(MARSHAL_12, "OffsetOf", ves_icall_System_Runtime_InteropServices_Marshal_OffsetOf)
ICALL(MARSHAL_13, "Prelink", ves_icall_System_Runtime_InteropServices_Marshal_Prelink)
ICALL(MARSHAL_14, "PrelinkAll", ves_icall_System_Runtime_InteropServices_Marshal_PrelinkAll)
ICALL(MARSHAL_15, "PtrToStringAnsi(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi)
ICALL(MARSHAL_16, "PtrToStringAnsi(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi_len)
#ifndef DISABLE_COM
ICALL(MARSHAL_17, "PtrToStringBSTR", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringBSTR)
#endif
ICALL(MARSHAL_18, "PtrToStringUni(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni)
ICALL(MARSHAL_19, "PtrToStringUni(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni_len)
ICALL(MARSHAL_20, "PtrToStructure(intptr,System.Type)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure_type)
ICALL(MARSHAL_21, "PtrToStructure(intptr,object)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure)
#ifndef DISABLE_COM
ICALL(MARSHAL_22, "QueryInterfaceInternal", ves_icall_System_Runtime_InteropServices_Marshal_QueryInterfaceInternal)
#endif
ICALL(MARSHAL_43, "ReAllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocCoTaskMem)
ICALL(MARSHAL_23, "ReAllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_49, "ReleaseComObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseComObjectInternal)
ICALL(MARSHAL_29, "ReleaseInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseInternal)
#endif
ICALL(MARSHAL_30, "SizeOf", ves_icall_System_Runtime_InteropServices_Marshal_SizeOf)
ICALL(MARSHAL_31, "StringToBSTR", ves_icall_System_Runtime_InteropServices_Marshal_StringToBSTR)
ICALL(MARSHAL_32, "StringToHGlobalAnsi", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalAnsi)
ICALL(MARSHAL_33, "StringToHGlobalUni", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalUni)
ICALL(MARSHAL_34, "StructureToPtr", ves_icall_System_Runtime_InteropServices_Marshal_StructureToPtr)
ICALL(MARSHAL_35, "UnsafeAddrOfPinnedArrayElement", ves_icall_System_Runtime_InteropServices_Marshal_UnsafeAddrOfPinnedArrayElement)
ICALL(MARSHAL_41, "copy_from_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_from_unmanaged)
ICALL(MARSHAL_42, "copy_to_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_to_unmanaged)

ICALL_TYPE(ACTS, "System.Runtime.Remoting.Activation.ActivationServices", ACTS_1)
ICALL(ACTS_1, "AllocateUninitializedClassInstance", ves_icall_System_Runtime_Activation_ActivationServices_AllocateUninitializedClassInstance)
ICALL(ACTS_2, "EnableProxyActivation", ves_icall_System_Runtime_Activation_ActivationServices_EnableProxyActivation)

ICALL_TYPE(MONOMM, "System.Runtime.Remoting.Messaging.MonoMethodMessage", MONOMM_1)
ICALL(MONOMM_1, "InitMessage", ves_icall_MonoMethodMessage_InitMessage)

#ifndef DISABLE_REMOTING
ICALL_TYPE(REALP, "System.Runtime.Remoting.Proxies.RealProxy", REALP_1)
ICALL(REALP_1, "InternalGetProxyType", ves_icall_Remoting_RealProxy_InternalGetProxyType)
ICALL(REALP_2, "InternalGetTransparentProxy", ves_icall_Remoting_RealProxy_GetTransparentProxy)

ICALL_TYPE(REMSER, "System.Runtime.Remoting.RemotingServices", REMSER_0)
ICALL(REMSER_0, "GetVirtualMethod", ves_icall_Remoting_RemotingServices_GetVirtualMethod)
ICALL(REMSER_1, "InternalExecute", ves_icall_InternalExecute)
ICALL(REMSER_2, "IsTransparentProxy", ves_icall_IsTransparentProxy)
#endif

ICALL_TYPE(MHAN, "System.RuntimeMethodHandle", MHAN_1)
ICALL(MHAN_1, "GetFunctionPointer", ves_icall_RuntimeMethod_GetFunctionPointer)

ICALL_TYPE(RNG, "System.Security.Cryptography.RNGCryptoServiceProvider", RNG_1)
ICALL(RNG_1, "RngClose", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngClose)
ICALL(RNG_2, "RngGetBytes", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngGetBytes)
ICALL(RNG_3, "RngInitialize", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngInitialize)
ICALL(RNG_4, "RngOpen", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngOpen)

#ifndef DISABLE_POLICY_EVIDENCE
ICALL_TYPE(EVID, "System.Security.Policy.Evidence", EVID_1)
ICALL(EVID_1, "IsAuthenticodePresent", ves_icall_System_Security_Policy_Evidence_IsAuthenticodePresent)

ICALL_TYPE(WINID, "System.Security.Principal.WindowsIdentity", WINID_1)
ICALL(WINID_1, "GetCurrentToken", ves_icall_System_Security_Principal_WindowsIdentity_GetCurrentToken)
ICALL(WINID_2, "GetTokenName", ves_icall_System_Security_Principal_WindowsIdentity_GetTokenName)
ICALL(WINID_3, "GetUserToken", ves_icall_System_Security_Principal_WindowsIdentity_GetUserToken)
ICALL(WINID_4, "_GetRoles", ves_icall_System_Security_Principal_WindowsIdentity_GetRoles)

ICALL_TYPE(WINIMP, "System.Security.Principal.WindowsImpersonationContext", WINIMP_1)
ICALL(WINIMP_1, "CloseToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_CloseToken)
ICALL(WINIMP_2, "DuplicateToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_DuplicateToken)
ICALL(WINIMP_3, "RevertToSelf", ves_icall_System_Security_Principal_WindowsImpersonationContext_RevertToSelf)
ICALL(WINIMP_4, "SetCurrentToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_SetCurrentToken)

ICALL_TYPE(WINPRIN, "System.Security.Principal.WindowsPrincipal", WINPRIN_1)
ICALL(WINPRIN_1, "IsMemberOfGroupId", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupId)
ICALL(WINPRIN_2, "IsMemberOfGroupName", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupName)

ICALL_TYPE(SECSTRING, "System.Security.SecureString", SECSTRING_1)
ICALL(SECSTRING_1, "DecryptInternal", ves_icall_System_Security_SecureString_DecryptInternal)
ICALL(SECSTRING_2, "EncryptInternal", ves_icall_System_Security_SecureString_EncryptInternal)
#endif /* !DISABLE_POLICY_EVIDENCE */

ICALL_TYPE(SECMAN, "System.Security.SecurityManager", SECMAN_1)
ICALL(SECMAN_1, "GetLinkDemandSecurity", ves_icall_System_Security_SecurityManager_GetLinkDemandSecurity)
ICALL(SECMAN_2, "get_CheckExecutionRights", ves_icall_System_Security_SecurityManager_get_CheckExecutionRights)
ICALL(SECMAN_3, "get_RequiresElevatedPermissions", mono_security_core_clr_require_elevated_permissions)
ICALL(SECMAN_4, "get_SecurityEnabled", ves_icall_System_Security_SecurityManager_get_SecurityEnabled)
ICALL(SECMAN_5, "set_CheckExecutionRights", ves_icall_System_Security_SecurityManager_set_CheckExecutionRights)
ICALL(SECMAN_6, "set_SecurityEnabled", ves_icall_System_Security_SecurityManager_set_SecurityEnabled)

ICALL_TYPE(STRING, "System.String", STRING_1)
ICALL(STRING_1, ".ctor(char*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_2, ".ctor(char*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_3, ".ctor(char,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_4, ".ctor(char[])", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_5, ".ctor(char[],int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_6, ".ctor(sbyte*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_7, ".ctor(sbyte*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8, ".ctor(sbyte*,int,int,System.Text.Encoding)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8a, "GetLOSLimit", ves_icall_System_String_GetLOSLimit)
ICALL(STRING_9, "InternalAllocateStr", ves_icall_System_String_InternalAllocateStr)
ICALL(STRING_10, "InternalIntern", ves_icall_System_String_InternalIntern)
ICALL(STRING_11, "InternalIsInterned", ves_icall_System_String_InternalIsInterned)

ICALL_TYPE(TENC, "System.Text.Encoding", TENC_1)
ICALL(TENC_1, "InternalCodePage", ves_icall_System_Text_Encoding_InternalCodePage)

ICALL_TYPE(ILOCK, "System.Threading.Interlocked", ILOCK_1)
ICALL(ILOCK_1, "Add(int&,int)", ves_icall_System_Threading_Interlocked_Add_Int)
ICALL(ILOCK_2, "Add(long&,long)", ves_icall_System_Threading_Interlocked_Add_Long)
ICALL(ILOCK_3, "CompareExchange(T&,T,T)", ves_icall_System_Threading_Interlocked_CompareExchange_T)
ICALL(ILOCK_4, "CompareExchange(double&,double,double)", ves_icall_System_Threading_Interlocked_CompareExchange_Double)
ICALL(ILOCK_5, "CompareExchange(int&,int,int)", ves_icall_System_Threading_Interlocked_CompareExchange_Int)
ICALL(ILOCK_6, "CompareExchange(intptr&,intptr,intptr)", ves_icall_System_Threading_Interlocked_CompareExchange_IntPtr)
ICALL(ILOCK_7, "CompareExchange(long&,long,long)", ves_icall_System_Threading_Interlocked_CompareExchange_Long)
ICALL(ILOCK_8, "CompareExchange(object&,object,object)", ves_icall_System_Threading_Interlocked_CompareExchange_Object)
ICALL(ILOCK_9, "CompareExchange(single&,single,single)", ves_icall_System_Threading_Interlocked_CompareExchange_Single)
ICALL(ILOCK_10, "Decrement(int&)", ves_icall_System_Threading_Interlocked_Decrement_Int)
ICALL(ILOCK_11, "Decrement(long&)", ves_icall_System_Threading_Interlocked_Decrement_Long)
ICALL(ILOCK_12, "Exchange(T&,T)", ves_icall_System_Threading_Interlocked_Exchange_T)
ICALL(ILOCK_13, "Exchange(double&,double)", ves_icall_System_Threading_Interlocked_Exchange_Double)
ICALL(ILOCK_14, "Exchange(int&,int)", ves_icall_System_Threading_Interlocked_Exchange_Int)
ICALL(ILOCK_15, "Exchange(intptr&,intptr)", ves_icall_System_Threading_Interlocked_Exchange_IntPtr)
ICALL(ILOCK_16, "Exchange(long&,long)", ves_icall_System_Threading_Interlocked_Exchange_Long)
ICALL(ILOCK_17, "Exchange(object&,object)", ves_icall_System_Threading_Interlocked_Exchange_Object)
ICALL(ILOCK_18, "Exchange(single&,single)", ves_icall_System_Threading_Interlocked_Exchange_Single)
ICALL(ILOCK_19, "Increment(int&)", ves_icall_System_Threading_Interlocked_Increment_Int)
ICALL(ILOCK_20, "Increment(long&)", ves_icall_System_Threading_Interlocked_Increment_Long)
ICALL(ILOCK_21, "Read(long&)", ves_icall_System_Threading_Interlocked_Read_Long)

ICALL_TYPE(ITHREAD, "System.Threading.InternalThread", ITHREAD_1)
ICALL(ITHREAD_1, "Thread_free_internal", ves_icall_System_Threading_InternalThread_Thread_free_internal)

ICALL_TYPE(MONIT, "System.Threading.Monitor", MONIT_8)
ICALL(MONIT_8, "Enter", mono_monitor_enter)
ICALL(MONIT_1, "Exit", mono_monitor_exit)
ICALL(MONIT_2, "Monitor_pulse", ves_icall_System_Threading_Monitor_Monitor_pulse)
ICALL(MONIT_3, "Monitor_pulse_all", ves_icall_System_Threading_Monitor_Monitor_pulse_all)
ICALL(MONIT_4, "Monitor_test_owner", ves_icall_System_Threading_Monitor_Monitor_test_owner)
ICALL(MONIT_5, "Monitor_test_synchronised", ves_icall_System_Threading_Monitor_Monitor_test_synchronised)
ICALL(MONIT_6, "Monitor_try_enter", ves_icall_System_Threading_Monitor_Monitor_try_enter)
ICALL(MONIT_7, "Monitor_wait", ves_icall_System_Threading_Monitor_Monitor_wait)
ICALL(MONIT_9, "try_enter_with_atomic_var", ves_icall_System_Threading_Monitor_Monitor_try_enter_with_atomic_var)

ICALL_TYPE(MUTEX, "System.Threading.Mutex", MUTEX_1)
ICALL(MUTEX_1, "CreateMutex_internal(bool,string,bool&)", ves_icall_System_Threading_Mutex_CreateMutex_internal)
ICALL(MUTEX_2, "OpenMutex_internal(string,System.Security.AccessControl.MutexRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Mutex_OpenMutex_internal)
ICALL(MUTEX_3, "ReleaseMutex_internal(intptr)", ves_icall_System_Threading_Mutex_ReleaseMutex_internal)

ICALL_TYPE(NATIVEC, "System.Threading.NativeEventCalls", NATIVEC_1)
ICALL(NATIVEC_1, "CloseEvent_internal", ves_icall_System_Threading_Events_CloseEvent_internal)
ICALL(NATIVEC_2, "CreateEvent_internal(bool,bool,string,bool&)", ves_icall_System_Threading_Events_CreateEvent_internal)
ICALL(NATIVEC_3, "OpenEvent_internal(string,System.Security.AccessControl.EventWaitHandleRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Events_OpenEvent_internal)
ICALL(NATIVEC_4, "ResetEvent_internal",  ves_icall_System_Threading_Events_ResetEvent_internal)
ICALL(NATIVEC_5, "SetEvent_internal",    ves_icall_System_Threading_Events_SetEvent_internal)

ICALL_TYPE(SEMA, "System.Threading.Semaphore", SEMA_1)
ICALL(SEMA_1, "CreateSemaphore_internal(int,int,string,bool&)", ves_icall_System_Threading_Semaphore_CreateSemaphore_internal)
ICALL(SEMA_2, "OpenSemaphore_internal(string,System.Security.AccessControl.SemaphoreRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Semaphore_OpenSemaphore_internal)
ICALL(SEMA_3, "ReleaseSemaphore_internal(intptr,int,bool&)", ves_icall_System_Threading_Semaphore_ReleaseSemaphore_internal)

ICALL_TYPE(THREAD, "System.Threading.Thread", THREAD_1)
ICALL(THREAD_1, "Abort_internal(System.Threading.InternalThread,object)", ves_icall_System_Threading_Thread_Abort)
ICALL(THREAD_1aa, "AllocTlsData", mono_thread_alloc_tls)
ICALL(THREAD_1a, "ByteArrayToCurrentDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToCurrentDomain)
ICALL(THREAD_1b, "ByteArrayToRootDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToRootDomain)
ICALL(THREAD_2, "ClrState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_ClrState)
ICALL(THREAD_2a, "ConstructInternalThread", ves_icall_System_Threading_Thread_ConstructInternalThread)
ICALL(THREAD_3, "CurrentInternalThread_internal", mono_thread_internal_current)
ICALL(THREAD_3a, "DestroyTlsData", mono_thread_destroy_tls)
ICALL(THREAD_4, "FreeLocalSlotValues", mono_thread_free_local_slot_values)
ICALL(THREAD_55, "GetAbortExceptionState", ves_icall_System_Threading_Thread_GetAbortExceptionState)
ICALL(THREAD_7, "GetDomainID", ves_icall_System_Threading_Thread_GetDomainID)
ICALL(THREAD_8, "GetName_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetName_internal)
ICALL(THREAD_11, "GetState(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetState)
ICALL(THREAD_53, "Interrupt_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Interrupt_internal)
ICALL(THREAD_12, "Join_internal(System.Threading.InternalThread,int,intptr)", ves_icall_System_Threading_Thread_Join_internal)
ICALL(THREAD_13, "MemoryBarrier", ves_icall_System_Threading_Thread_MemoryBarrier)
ICALL(THREAD_14, "ResetAbort_internal()", ves_icall_System_Threading_Thread_ResetAbort)
ICALL(THREAD_15, "Resume_internal()", ves_icall_System_Threading_Thread_Resume)
ICALL(THREAD_18, "SetName_internal(System.Threading.InternalThread,string)", ves_icall_System_Threading_Thread_SetName_internal)
ICALL(THREAD_21, "SetState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_SetState)
ICALL(THREAD_22, "Sleep_internal", ves_icall_System_Threading_Thread_Sleep_internal)
ICALL(THREAD_54, "SpinWait_nop", ves_icall_System_Threading_Thread_SpinWait_nop)
ICALL(THREAD_23, "Suspend_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Suspend)
ICALL(THREAD_25, "Thread_internal", ves_icall_System_Threading_Thread_Thread_internal)
ICALL(THREAD_26, "VolatileRead(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_27, "VolatileRead(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(THREAD_28, "VolatileRead(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_29, "VolatileRead(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_30, "VolatileRead(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_31, "VolatileRead(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_32, "VolatileRead(object&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_33, "VolatileRead(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_34, "VolatileRead(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(THREAD_35, "VolatileRead(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_36, "VolatileRead(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_37, "VolatileRead(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_38, "VolatileRead(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_39, "VolatileWrite(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_40, "VolatileWrite(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(THREAD_41, "VolatileWrite(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_42, "VolatileWrite(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_43, "VolatileWrite(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_44, "VolatileWrite(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_45, "VolatileWrite(object&,object)", ves_icall_System_Threading_Thread_VolatileWriteObject)
ICALL(THREAD_46, "VolatileWrite(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_47, "VolatileWrite(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(THREAD_48, "VolatileWrite(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_49, "VolatileWrite(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_50, "VolatileWrite(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_51, "VolatileWrite(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_9, "Yield", ves_icall_System_Threading_Thread_Yield)
ICALL(THREAD_52, "current_lcid()", ves_icall_System_Threading_Thread_current_lcid)

ICALL_TYPE(THREADP, "System.Threading.ThreadPool", THREADP_1)
ICALL(THREADP_1, "GetAvailableThreads", ves_icall_System_Threading_ThreadPool_GetAvailableThreads)
ICALL(THREADP_2, "GetMaxThreads", ves_icall_System_Threading_ThreadPool_GetMaxThreads)
ICALL(THREADP_3, "GetMinThreads", ves_icall_System_Threading_ThreadPool_GetMinThreads)
ICALL(THREADP_35, "SetMaxThreads", ves_icall_System_Threading_ThreadPool_SetMaxThreads)
ICALL(THREADP_4, "SetMinThreads", ves_icall_System_Threading_ThreadPool_SetMinThreads)
ICALL(THREADP_5, "pool_queue", icall_append_job)

ICALL_TYPE(VOLATILE, "System.Threading.Volatile", VOLATILE_28)
ICALL(VOLATILE_28, "Read(T&)", ves_icall_System_Threading_Volatile_Read_T)
ICALL(VOLATILE_1, "Read(bool&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_2, "Read(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_3, "Read(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(VOLATILE_4, "Read(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_5, "Read(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_6, "Read(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_7, "Read(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_8, "Read(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_9, "Read(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(VOLATILE_10, "Read(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_11, "Read(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_12, "Read(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_13, "Read(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_27, "Write(T&,T)", ves_icall_System_Threading_Volatile_Write_T)
ICALL(VOLATILE_14, "Write(bool&,bool)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_15, "Write(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_16, "Write(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(VOLATILE_17, "Write(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_18, "Write(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_19, "Write(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_20, "Write(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(VOLATILE_21, "Write(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_22, "Write(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(VOLATILE_23, "Write(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_24, "Write(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_25, "Write(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_26, "Write(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)

ICALL_TYPE(WAITH, "System.Threading.WaitHandle", WAITH_1)
ICALL(WAITH_1, "SignalAndWait_Internal", ves_icall_System_Threading_WaitHandle_SignalAndWait_Internal)
ICALL(WAITH_2, "WaitAll_internal", ves_icall_System_Threading_WaitHandle_WaitAll_internal)
ICALL(WAITH_3, "WaitAny_internal", ves_icall_System_Threading_WaitHandle_WaitAny_internal)
ICALL(WAITH_4, "WaitOne_internal", ves_icall_System_Threading_WaitHandle_WaitOne_internal)

ICALL_TYPE(TYPE, "System.Type", TYPE_1)
ICALL(TYPE_1, "EqualsInternal", ves_icall_System_Type_EqualsInternal)
ICALL(TYPE_2, "GetGenericParameterAttributes", ves_icall_Type_GetGenericParameterAttributes)
ICALL(TYPE_3, "GetGenericParameterConstraints_impl", ves_icall_Type_GetGenericParameterConstraints)
ICALL(TYPE_4, "GetGenericParameterPosition", ves_icall_Type_GetGenericParameterPosition)
ICALL(TYPE_5, "GetGenericTypeDefinition_impl", ves_icall_Type_GetGenericTypeDefinition_impl)
ICALL(TYPE_6, "GetInterfaceMapData", ves_icall_Type_GetInterfaceMapData)
ICALL(TYPE_7, "GetPacking", ves_icall_Type_GetPacking)
ICALL(TYPE_8, "GetTypeCode", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_9, "GetTypeCodeInternal", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_10, "IsArrayImpl", ves_icall_Type_IsArrayImpl)
ICALL(TYPE_11, "IsInstanceOfType", ves_icall_type_IsInstanceOfType)
ICALL(TYPE_12, "MakeGenericType", ves_icall_Type_MakeGenericType)
ICALL(TYPE_13, "MakePointerType", ves_icall_Type_MakePointerType)
ICALL(TYPE_14, "get_IsGenericInstance", ves_icall_Type_get_IsGenericInstance)
ICALL(TYPE_15, "get_IsGenericType", ves_icall_Type_get_IsGenericType)
ICALL(TYPE_16, "get_IsGenericTypeDefinition", ves_icall_Type_get_IsGenericTypeDefinition)
ICALL(TYPE_17, "internal_from_handle", ves_icall_type_from_handle)
ICALL(TYPE_18, "internal_from_name", ves_icall_type_from_name)
ICALL(TYPE_19, "make_array_type", ves_icall_Type_make_array_type)
ICALL(TYPE_20, "make_byref_type", ves_icall_Type_make_byref_type)
ICALL(TYPE_21, "type_is_assignable_from", ves_icall_type_is_assignable_from)
ICALL(TYPE_22, "type_is_subtype_of", ves_icall_type_is_subtype_of)

ICALL_TYPE(TYPEDR, "System.TypedReference", TYPEDR_1)
ICALL(TYPEDR_1, "ToObject",	mono_TypedReference_ToObject)
ICALL(TYPEDR_2, "ToObjectInternal",	mono_TypedReference_ToObjectInternal)

ICALL_TYPE(VALUET, "System.ValueType", VALUET_1)
ICALL(VALUET_1, "InternalEquals", ves_icall_System_ValueType_Equals)
ICALL(VALUET_2, "InternalGetHashCode", ves_icall_System_ValueType_InternalGetHashCode)

ICALL_TYPE(WEBIC, "System.Web.Util.ICalls", WEBIC_1)
ICALL(WEBIC_1, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(WEBIC_2, "GetMachineInstallDirectory", ves_icall_System_Web_Util_ICalls_get_machine_install_dir)
ICALL(WEBIC_3, "GetUnmanagedResourcesPtr", ves_icall_get_resources_ptr)

#ifndef DISABLE_COM
ICALL_TYPE(COMOBJ, "System.__ComObject", COMOBJ_1)
ICALL(COMOBJ_1, "CreateRCW", ves_icall_System_ComObject_CreateRCW)
ICALL(COMOBJ_2, "GetInterfaceInternal", ves_icall_System_ComObject_GetInterfaceInternal)
ICALL(COMOBJ_3, "ReleaseInterfaces", ves_icall_System_ComObject_ReleaseInterfaces)
#endif
	NULL
};

#ifdef ENABLE_ICALL_SYMBOL_MAP
#undef ICALL_TYPE
#undef ICALL
#define ICALL_TYPE(id,name,first)
#define ICALL(id,name,func) #func,
static const gconstpointer
icall_symbols [] = {
// #include "metadata/icall-def.h"
ICALL_TYPE(UNORM, "Mono.Globalization.Unicode.Normalization", UNORM_1)
ICALL(UNORM_1, "load_normalization_resource", load_normalization_resource)

#ifndef DISABLE_COM
ICALL_TYPE(COMPROX, "Mono.Interop.ComInteropProxy", COMPROX_1)
ICALL(COMPROX_1, "AddProxy", ves_icall_Mono_Interop_ComInteropProxy_AddProxy)
ICALL(COMPROX_2, "FindProxy", ves_icall_Mono_Interop_ComInteropProxy_FindProxy)
#endif

ICALL_TYPE(RUNTIME, "Mono.Runtime", RUNTIME_1)
ICALL(RUNTIME_1, "GetDisplayName", ves_icall_Mono_Runtime_GetDisplayName)
ICALL(RUNTIME_12, "GetNativeStackTrace", ves_icall_Mono_Runtime_GetNativeStackTrace)
ICALL(RUNTIME_13, "SetGCAllowSynchronousMajor", ves_icall_Mono_Runtime_SetGCAllowSynchronousMajor)

#ifndef PLATFORM_RO_FS
ICALL_TYPE(KPAIR, "Mono.Security.Cryptography.KeyPairPersistence", KPAIR_1)
ICALL(KPAIR_1, "_CanSecure", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_CanSecure)
ICALL(KPAIR_2, "_IsMachineProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsMachineProtected)
ICALL(KPAIR_3, "_IsUserProtected", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_IsUserProtected)
ICALL(KPAIR_4, "_ProtectMachine", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectMachine)
ICALL(KPAIR_5, "_ProtectUser", ves_icall_Mono_Security_Cryptography_KeyPairPersistence_ProtectUser)
#endif /* !PLATFORM_RO_FS */

ICALL_TYPE(ACTIV, "System.Activator", ACTIV_1)
ICALL(ACTIV_1, "CreateInstanceInternal", ves_icall_System_Activator_CreateInstanceInternal)

ICALL_TYPE(APPDOM, "System.AppDomain", APPDOM_1)
ICALL(APPDOM_1, "ExecuteAssembly", ves_icall_System_AppDomain_ExecuteAssembly)
ICALL(APPDOM_2, "GetAssemblies", ves_icall_System_AppDomain_GetAssemblies)
ICALL(APPDOM_3, "GetData", ves_icall_System_AppDomain_GetData)
ICALL(APPDOM_4, "InternalGetContext", ves_icall_System_AppDomain_InternalGetContext)
ICALL(APPDOM_5, "InternalGetDefaultContext", ves_icall_System_AppDomain_InternalGetDefaultContext)
ICALL(APPDOM_6, "InternalGetProcessGuid", ves_icall_System_AppDomain_InternalGetProcessGuid)
ICALL(APPDOM_7, "InternalIsFinalizingForUnload", ves_icall_System_AppDomain_InternalIsFinalizingForUnload)
ICALL(APPDOM_8, "InternalPopDomainRef", ves_icall_System_AppDomain_InternalPopDomainRef)
ICALL(APPDOM_9, "InternalPushDomainRef", ves_icall_System_AppDomain_InternalPushDomainRef)
ICALL(APPDOM_10, "InternalPushDomainRefByID", ves_icall_System_AppDomain_InternalPushDomainRefByID)
ICALL(APPDOM_11, "InternalSetContext", ves_icall_System_AppDomain_InternalSetContext)
ICALL(APPDOM_12, "InternalSetDomain", ves_icall_System_AppDomain_InternalSetDomain)
ICALL(APPDOM_13, "InternalSetDomainByID", ves_icall_System_AppDomain_InternalSetDomainByID)
ICALL(APPDOM_14, "InternalUnload", ves_icall_System_AppDomain_InternalUnload)
ICALL(APPDOM_15, "LoadAssembly", ves_icall_System_AppDomain_LoadAssembly)
ICALL(APPDOM_16, "LoadAssemblyRaw", ves_icall_System_AppDomain_LoadAssemblyRaw)
ICALL(APPDOM_17, "SetData", ves_icall_System_AppDomain_SetData)
ICALL(APPDOM_18, "createDomain", ves_icall_System_AppDomain_createDomain)
ICALL(APPDOM_19, "getCurDomain", ves_icall_System_AppDomain_getCurDomain)
ICALL(APPDOM_20, "getFriendlyName", ves_icall_System_AppDomain_getFriendlyName)
ICALL(APPDOM_21, "getRootDomain", ves_icall_System_AppDomain_getRootDomain)
ICALL(APPDOM_22, "getSetup", ves_icall_System_AppDomain_getSetup)

ICALL_TYPE(ARGI, "System.ArgIterator", ARGI_1)
ICALL(ARGI_1, "IntGetNextArg()",                  mono_ArgIterator_IntGetNextArg)
ICALL(ARGI_2, "IntGetNextArg(intptr)", mono_ArgIterator_IntGetNextArgT)
ICALL(ARGI_3, "IntGetNextArgType",                mono_ArgIterator_IntGetNextArgType)
ICALL(ARGI_4, "Setup",                            mono_ArgIterator_Setup)

ICALL_TYPE(ARRAY, "System.Array", ARRAY_1)
ICALL(ARRAY_1, "ClearInternal",    ves_icall_System_Array_ClearInternal)
ICALL(ARRAY_2, "Clone",            mono_array_clone)
ICALL(ARRAY_3, "CreateInstanceImpl",   ves_icall_System_Array_CreateInstanceImpl)
ICALL(ARRAY_14, "CreateInstanceImpl64",   ves_icall_System_Array_CreateInstanceImpl64)
ICALL(ARRAY_4, "FastCopy",         ves_icall_System_Array_FastCopy)
ICALL(ARRAY_5, "GetGenericValueImpl", ves_icall_System_Array_GetGenericValueImpl)
ICALL(ARRAY_6, "GetLength",        ves_icall_System_Array_GetLength)
ICALL(ARRAY_15, "GetLongLength",        ves_icall_System_Array_GetLongLength)
ICALL(ARRAY_7, "GetLowerBound",    ves_icall_System_Array_GetLowerBound)
ICALL(ARRAY_8, "GetRank",          ves_icall_System_Array_GetRank)
ICALL(ARRAY_9, "GetValue",         ves_icall_System_Array_GetValue)
ICALL(ARRAY_10, "GetValueImpl",     ves_icall_System_Array_GetValueImpl)
ICALL(ARRAY_11, "SetGenericValueImpl", ves_icall_System_Array_SetGenericValueImpl)
ICALL(ARRAY_12, "SetValue",         ves_icall_System_Array_SetValue)
ICALL(ARRAY_13, "SetValueImpl",     ves_icall_System_Array_SetValueImpl)

ICALL_TYPE(BUFFER, "System.Buffer", BUFFER_1)
ICALL(BUFFER_1, "BlockCopyInternal", ves_icall_System_Buffer_BlockCopyInternal)
ICALL(BUFFER_2, "ByteLengthInternal", ves_icall_System_Buffer_ByteLengthInternal)
ICALL(BUFFER_3, "GetByteInternal", ves_icall_System_Buffer_GetByteInternal)
ICALL(BUFFER_4, "SetByteInternal", ves_icall_System_Buffer_SetByteInternal)

ICALL_TYPE(CHAR, "System.Char", CHAR_1)
ICALL(CHAR_1, "GetDataTablePointers", ves_icall_System_Char_GetDataTablePointers)

ICALL_TYPE (COMPO_W, "System.ComponentModel.Win32Exception", COMPO_W_1)
ICALL (COMPO_W_1, "W32ErrorMessage", ves_icall_System_ComponentModel_Win32Exception_W32ErrorMessage)

ICALL_TYPE(DEFAULTC, "System.Configuration.DefaultConfig", DEFAULTC_1)
ICALL(DEFAULTC_1, "get_bundled_machine_config", get_bundled_machine_config)
ICALL(DEFAULTC_2, "get_machine_config_path", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)

/* Note that the below icall shares the same function as DefaultConfig uses */
ICALL_TYPE(INTCFGHOST, "System.Configuration.InternalConfigurationHost", INTCFGHOST_1)
ICALL(INTCFGHOST_1, "get_bundled_app_config", get_bundled_app_config)
ICALL(INTCFGHOST_2, "get_bundled_machine_config", get_bundled_machine_config)

ICALL_TYPE(CONSOLE, "System.ConsoleDriver", CONSOLE_1)
ICALL(CONSOLE_1, "InternalKeyAvailable", ves_icall_System_ConsoleDriver_InternalKeyAvailable )
ICALL(CONSOLE_2, "Isatty", ves_icall_System_ConsoleDriver_Isatty )
ICALL(CONSOLE_3, "SetBreak", ves_icall_System_ConsoleDriver_SetBreak )
ICALL(CONSOLE_4, "SetEcho", ves_icall_System_ConsoleDriver_SetEcho )
ICALL(CONSOLE_5, "TtySetup", ves_icall_System_ConsoleDriver_TtySetup )

ICALL_TYPE(CONVERT, "System.Convert", CONVERT_1)
ICALL(CONVERT_1, "InternalFromBase64CharArray", InternalFromBase64CharArray )
ICALL(CONVERT_2, "InternalFromBase64String", InternalFromBase64String )

ICALL_TYPE(TZONE, "System.CurrentSystemTimeZone", TZONE_1)
ICALL(TZONE_1, "GetTimeZoneData", ves_icall_System_CurrentSystemTimeZone_GetTimeZoneData)

ICALL_TYPE(DTIME, "System.DateTime", DTIME_1)
ICALL(DTIME_1, "GetNow", mono_100ns_datetime)
ICALL(DTIME_2, "GetTimeMonotonic", mono_100ns_ticks)

#ifndef DISABLE_DECIMAL
ICALL_TYPE(DECIMAL, "System.Decimal", DECIMAL_1)
ICALL(DECIMAL_1, "decimal2Int64", mono_decimal2Int64)
ICALL(DECIMAL_2, "decimal2UInt64", mono_decimal2UInt64)
ICALL(DECIMAL_3, "decimal2double", mono_decimal2double)
//ICALL(DECIMAL_4, "decimal2string", mono_decimal2string)
ICALL(DECIMAL_5, "decimalCompare", mono_decimalCompare)
ICALL(DECIMAL_6, "decimalDiv", mono_decimalDiv)
ICALL(DECIMAL_7, "decimalFloorAndTrunc", mono_decimalFloorAndTrunc)
ICALL(DECIMAL_8, "decimalIncr", mono_decimalIncr)
ICALL(DECIMAL_9, "decimalIntDiv", mono_decimalIntDiv)
ICALL(DECIMAL_10, "decimalMult", mono_decimalMult)
ICALL(DECIMAL_11, "decimalRound", mono_decimalRound)
ICALL(DECIMAL_12, "decimalSetExponent", mono_decimalSetExponent)
ICALL(DECIMAL_13, "double2decimal", mono_double2decimal) /* FIXME: wrong signature. */
ICALL(DECIMAL_14, "string2decimal", mono_string2decimal)
#endif

ICALL_TYPE(DELEGATE, "System.Delegate", DELEGATE_1)
ICALL(DELEGATE_1, "CreateDelegate_internal", ves_icall_System_Delegate_CreateDelegate_internal)
ICALL(DELEGATE_2, "SetMulticastInvoke", ves_icall_System_Delegate_SetMulticastInvoke)

ICALL_TYPE(DEBUGR, "System.Diagnostics.Debugger", DEBUGR_1)
ICALL(DEBUGR_1, "IsAttached_internal", ves_icall_System_Diagnostics_Debugger_IsAttached_internal)
ICALL(DEBUGR_2, "IsLogging", ves_icall_System_Diagnostics_Debugger_IsLogging)
ICALL(DEBUGR_3, "Log", ves_icall_System_Diagnostics_Debugger_Log)

ICALL_TYPE(TRACEL, "System.Diagnostics.DefaultTraceListener", TRACEL_1)
ICALL(TRACEL_1, "WriteWindowsDebugString", ves_icall_System_Diagnostics_DefaultTraceListener_WriteWindowsDebugString)

ICALL_TYPE(FILEV, "System.Diagnostics.FileVersionInfo", FILEV_1)
ICALL(FILEV_1, "GetVersionInfo_internal(string)", ves_icall_System_Diagnostics_FileVersionInfo_GetVersionInfo_internal)

#ifndef DISABLE_PROCESS_HANDLING
ICALL_TYPE(PERFCTR, "System.Diagnostics.PerformanceCounter", PERFCTR_1)
ICALL(PERFCTR_1, "FreeData", mono_perfcounter_free_data)
ICALL(PERFCTR_2, "GetImpl", mono_perfcounter_get_impl)
ICALL(PERFCTR_3, "GetSample", mono_perfcounter_get_sample)
ICALL(PERFCTR_4, "UpdateValue", mono_perfcounter_update_value)

ICALL_TYPE(PERFCTRCAT, "System.Diagnostics.PerformanceCounterCategory", PERFCTRCAT_1)
ICALL(PERFCTRCAT_1, "CategoryDelete", mono_perfcounter_category_del)
ICALL(PERFCTRCAT_2, "CategoryHelpInternal",   mono_perfcounter_category_help)
ICALL(PERFCTRCAT_3, "CounterCategoryExists", mono_perfcounter_category_exists)
ICALL(PERFCTRCAT_4, "Create",         mono_perfcounter_create)
ICALL(PERFCTRCAT_5, "GetCategoryNames", mono_perfcounter_category_names)
ICALL(PERFCTRCAT_6, "GetCounterNames", mono_perfcounter_counter_names)
ICALL(PERFCTRCAT_7, "GetInstanceNames", mono_perfcounter_instance_names)
ICALL(PERFCTRCAT_8, "InstanceExistsInternal", mono_perfcounter_instance_exists)

ICALL_TYPE(PROCESS, "System.Diagnostics.Process", PROCESS_1)
ICALL(PROCESS_1, "CreateProcess_internal(System.Diagnostics.ProcessStartInfo,intptr,intptr,intptr,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_CreateProcess_internal)
ICALL(PROCESS_2, "ExitCode_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitCode_internal)
ICALL(PROCESS_3, "ExitTime_internal(intptr)", ves_icall_System_Diagnostics_Process_ExitTime_internal)
ICALL(PROCESS_4, "GetModules_internal(intptr)", ves_icall_System_Diagnostics_Process_GetModules_internal)
ICALL(PROCESS_5, "GetPid_internal()", ves_icall_System_Diagnostics_Process_GetPid_internal)
ICALL(PROCESS_5B, "GetPriorityClass(intptr,int&)", ves_icall_System_Diagnostics_Process_GetPriorityClass)
ICALL(PROCESS_5H, "GetProcessData", ves_icall_System_Diagnostics_Process_GetProcessData)
ICALL(PROCESS_6, "GetProcess_internal(int)", ves_icall_System_Diagnostics_Process_GetProcess_internal)
ICALL(PROCESS_7, "GetProcesses_internal()", ves_icall_System_Diagnostics_Process_GetProcesses_internal)
ICALL(PROCESS_8, "GetWorkingSet_internal(intptr,int&,int&)", ves_icall_System_Diagnostics_Process_GetWorkingSet_internal)
ICALL(PROCESS_9, "Kill_internal", ves_icall_System_Diagnostics_Process_Kill_internal)
ICALL(PROCESS_10, "ProcessName_internal(intptr)", ves_icall_System_Diagnostics_Process_ProcessName_internal)
ICALL(PROCESS_11, "Process_free_internal(intptr)", ves_icall_System_Diagnostics_Process_Process_free_internal)
ICALL(PROCESS_11B, "SetPriorityClass(intptr,int,int&)", ves_icall_System_Diagnostics_Process_SetPriorityClass)
ICALL(PROCESS_12, "SetWorkingSet_internal(intptr,int,int,bool)", ves_icall_System_Diagnostics_Process_SetWorkingSet_internal)
ICALL(PROCESS_13, "ShellExecuteEx_internal(System.Diagnostics.ProcessStartInfo,System.Diagnostics.Process/ProcInfo&)", ves_icall_System_Diagnostics_Process_ShellExecuteEx_internal)
ICALL(PROCESS_14, "StartTime_internal(intptr)", ves_icall_System_Diagnostics_Process_StartTime_internal)
ICALL(PROCESS_14M, "Times", ves_icall_System_Diagnostics_Process_Times)
ICALL(PROCESS_15, "WaitForExit_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForExit_internal)
ICALL(PROCESS_16, "WaitForInputIdle_internal(intptr,int)", ves_icall_System_Diagnostics_Process_WaitForInputIdle_internal)

ICALL_TYPE (PROCESSHANDLE, "System.Diagnostics.Process/ProcessWaitHandle", PROCESSHANDLE_1)
ICALL (PROCESSHANDLE_1, "ProcessHandle_close(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_close)
ICALL (PROCESSHANDLE_2, "ProcessHandle_duplicate(intptr)", ves_icall_System_Diagnostics_Process_ProcessHandle_duplicate)
#endif /* !DISABLE_PROCESS_HANDLING */

ICALL_TYPE(STOPWATCH, "System.Diagnostics.Stopwatch", STOPWATCH_1)
ICALL(STOPWATCH_1, "GetTimestamp", mono_100ns_ticks)

ICALL_TYPE(DOUBLE, "System.Double", DOUBLE_1)
ICALL(DOUBLE_1, "ParseImpl",    mono_double_ParseImpl)

ICALL_TYPE(ENUM, "System.Enum", ENUM_1)
ICALL(ENUM_1, "ToObject", ves_icall_System_Enum_ToObject)
ICALL(ENUM_5, "compare_value_to", ves_icall_System_Enum_compare_value_to)
ICALL(ENUM_4, "get_hashcode", ves_icall_System_Enum_get_hashcode)
ICALL(ENUM_3, "get_underlying_type", ves_icall_System_Enum_get_underlying_type)
ICALL(ENUM_2, "get_value", ves_icall_System_Enum_get_value)

ICALL_TYPE(ENV, "System.Environment", ENV_1)
ICALL(ENV_1, "Exit", ves_icall_System_Environment_Exit)
ICALL(ENV_2, "GetCommandLineArgs", mono_runtime_get_main_args)
ICALL(ENV_3, "GetEnvironmentVariableNames", ves_icall_System_Environment_GetEnvironmentVariableNames)
ICALL(ENV_4, "GetLogicalDrivesInternal", ves_icall_System_Environment_GetLogicalDrives )
ICALL(ENV_5, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(ENV_51, "GetNewLine", ves_icall_System_Environment_get_NewLine)
ICALL(ENV_6, "GetOSVersionString", ves_icall_System_Environment_GetOSVersionString)
ICALL(ENV_6a, "GetPageSize", mono_pagesize)
ICALL(ENV_7, "GetWindowsFolderPath", ves_icall_System_Environment_GetWindowsFolderPath)
ICALL(ENV_8, "InternalSetEnvironmentVariable", ves_icall_System_Environment_InternalSetEnvironmentVariable)
ICALL(ENV_9, "get_ExitCode", mono_environment_exitcode_get)
ICALL(ENV_10, "get_HasShutdownStarted", ves_icall_System_Environment_get_HasShutdownStarted)
ICALL(ENV_11, "get_MachineName", ves_icall_System_Environment_get_MachineName)
ICALL(ENV_13, "get_Platform", ves_icall_System_Environment_get_Platform)
ICALL(ENV_14, "get_ProcessorCount", mono_cpu_count)
ICALL(ENV_15, "get_TickCount", mono_msec_ticks)
ICALL(ENV_16, "get_UserName", ves_icall_System_Environment_get_UserName)
ICALL(ENV_16m, "internalBroadcastSettingChange", ves_icall_System_Environment_BroadcastSettingChange)
ICALL(ENV_17, "internalGetEnvironmentVariable", ves_icall_System_Environment_GetEnvironmentVariable)
ICALL(ENV_18, "internalGetGacPath", ves_icall_System_Environment_GetGacPath)
ICALL(ENV_19, "internalGetHome", ves_icall_System_Environment_InternalGetHome)
ICALL(ENV_20, "set_ExitCode", mono_environment_exitcode_set)

ICALL_TYPE(GC, "System.GC", GC_0)
ICALL(GC_0, "CollectionCount", mono_gc_collection_count)
ICALL(GC_0a, "GetGeneration", mono_gc_get_generation)
ICALL(GC_1, "GetTotalMemory", ves_icall_System_GC_GetTotalMemory)
ICALL(GC_2, "InternalCollect", ves_icall_System_GC_InternalCollect)
ICALL(GC_3, "KeepAlive", ves_icall_System_GC_KeepAlive)
ICALL(GC_4, "ReRegisterForFinalize", ves_icall_System_GC_ReRegisterForFinalize)
ICALL(GC_4a, "RecordPressure", mono_gc_add_memory_pressure)
ICALL(GC_5, "SuppressFinalize", ves_icall_System_GC_SuppressFinalize)
ICALL(GC_6, "WaitForPendingFinalizers", ves_icall_System_GC_WaitForPendingFinalizers)
ICALL(GC_7, "get_MaxGeneration", mono_gc_max_generation)
ICALL(GC_9, "get_ephemeron_tombstone", ves_icall_System_GC_get_ephemeron_tombstone)
ICALL(GC_8, "register_ephemeron_array", ves_icall_System_GC_register_ephemeron_array)

ICALL_TYPE(COMPINF, "System.Globalization.CompareInfo", COMPINF_1)
ICALL(COMPINF_1, "assign_sortkey(object,string,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_assign_sortkey)
ICALL(COMPINF_2, "construct_compareinfo(string)", ves_icall_System_Globalization_CompareInfo_construct_compareinfo)
ICALL(COMPINF_3, "free_internal_collator()", ves_icall_System_Globalization_CompareInfo_free_internal_collator)
ICALL(COMPINF_4, "internal_compare(string,int,int,string,int,int,System.Globalization.CompareOptions)", ves_icall_System_Globalization_CompareInfo_internal_compare)
ICALL(COMPINF_5, "internal_index(string,int,int,char,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index_char)
ICALL(COMPINF_6, "internal_index(string,int,int,string,System.Globalization.CompareOptions,bool)", ves_icall_System_Globalization_CompareInfo_internal_index)

ICALL_TYPE(CULINF, "System.Globalization.CultureInfo", CULINF_2)
ICALL(CULINF_2, "construct_datetime_format", ves_icall_System_Globalization_CultureInfo_construct_datetime_format)
ICALL(CULINF_4, "construct_internal_locale_from_current_locale", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_current_locale)
ICALL(CULINF_5, "construct_internal_locale_from_lcid", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_lcid)
ICALL(CULINF_6, "construct_internal_locale_from_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_name)
ICALL(CULINF_7, "construct_internal_locale_from_specific_name", ves_icall_System_Globalization_CultureInfo_construct_internal_locale_from_specific_name)
ICALL(CULINF_8, "construct_number_format", ves_icall_System_Globalization_CultureInfo_construct_number_format)
ICALL(CULINF_9, "internal_get_cultures", ves_icall_System_Globalization_CultureInfo_internal_get_cultures)
//ICALL(CULINF_10, "internal_is_lcid_neutral", ves_icall_System_Globalization_CultureInfo_internal_is_lcid_neutral)

ICALL_TYPE(REGINF, "System.Globalization.RegionInfo", REGINF_1)
ICALL(REGINF_1, "construct_internal_region_from_lcid", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_lcid)
ICALL(REGINF_2, "construct_internal_region_from_name", ves_icall_System_Globalization_RegionInfo_construct_internal_region_from_name)

#ifndef PLATFORM_NO_DRIVEINFO
ICALL_TYPE(IODRIVEINFO, "System.IO.DriveInfo", IODRIVEINFO_1)
ICALL(IODRIVEINFO_1, "GetDiskFreeSpaceInternal", ves_icall_System_IO_DriveInfo_GetDiskFreeSpace)
ICALL(IODRIVEINFO_2, "GetDriveFormat", ves_icall_System_IO_DriveInfo_GetDriveFormat)
ICALL(IODRIVEINFO_3, "GetDriveTypeInternal", ves_icall_System_IO_DriveInfo_GetDriveType)
#endif

ICALL_TYPE(FAMW, "System.IO.FAMWatcher", FAMW_1)
ICALL(FAMW_1, "InternalFAMNextEvent", ves_icall_System_IO_FAMW_InternalFAMNextEvent)

ICALL_TYPE(FILEW, "System.IO.FileSystemWatcher", FILEW_4)
ICALL(FILEW_4, "InternalSupportsFSW", ves_icall_System_IO_FSW_SupportsFSW)

ICALL_TYPE(INOW, "System.IO.InotifyWatcher", INOW_1)
ICALL(INOW_1, "AddWatch", ves_icall_System_IO_InotifyWatcher_AddWatch)
ICALL(INOW_2, "GetInotifyInstance", ves_icall_System_IO_InotifyWatcher_GetInotifyInstance)
ICALL(INOW_3, "RemoveWatch", ves_icall_System_IO_InotifyWatcher_RemoveWatch)

#if defined (TARGET_IOS) || defined (TARGET_ANDROID)
ICALL_TYPE(MMAPIMPL, "System.IO.MemoryMappedFiles.MemoryMapImpl", MMAPIMPL_1)
ICALL(MMAPIMPL_1, "mono_filesize_from_fd", mono_filesize_from_fd)
ICALL(MMAPIMPL_2, "mono_filesize_from_path", mono_filesize_from_path)
#endif


ICALL_TYPE(MONOIO, "System.IO.MonoIO", MONOIO_1)
ICALL(MONOIO_1, "Close(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Close)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_2, "CopyFile(string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CopyFile)
ICALL(MONOIO_3, "CreateDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_CreateDirectory)
ICALL(MONOIO_4, "CreatePipe(intptr&,intptr&)", ves_icall_System_IO_MonoIO_CreatePipe)
ICALL(MONOIO_5, "DeleteFile(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_DeleteFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_34, "DuplicateHandle", ves_icall_System_IO_MonoIO_DuplicateHandle)
ICALL(MONOIO_37, "FindClose", ves_icall_System_IO_MonoIO_FindClose)
ICALL(MONOIO_35, "FindFirst", ves_icall_System_IO_MonoIO_FindFirst)
ICALL(MONOIO_36, "FindNext", ves_icall_System_IO_MonoIO_FindNext)
ICALL(MONOIO_6, "Flush(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Flush)
ICALL(MONOIO_7, "GetCurrentDirectory(System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetCurrentDirectory)
ICALL(MONOIO_8, "GetFileAttributes(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileAttributes)
ICALL(MONOIO_9, "GetFileStat(string,System.IO.MonoIOStat&,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileStat)
ICALL(MONOIO_10, "GetFileSystemEntries", ves_icall_System_IO_MonoIO_GetFileSystemEntries)
ICALL(MONOIO_11, "GetFileType(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetFileType)
ICALL(MONOIO_12, "GetLength(intptr,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_GetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_13, "GetTempPath(string&)", ves_icall_System_IO_MonoIO_GetTempPath)
ICALL(MONOIO_14, "Lock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Lock)
ICALL(MONOIO_15, "MoveFile(string,string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_MoveFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_16, "Open(string,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.IO.FileOptions,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Open)
ICALL(MONOIO_17, "Read(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Read)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_18, "RemoveDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_RemoveDirectory)
ICALL(MONOIO_18M, "ReplaceFile(string,string,string,bool,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_ReplaceFile)
#endif /* !PLATFORM_RO_FS */
ICALL(MONOIO_19, "Seek(intptr,long,System.IO.SeekOrigin,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Seek)
ICALL(MONOIO_20, "SetCurrentDirectory(string,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetCurrentDirectory)
ICALL(MONOIO_21, "SetFileAttributes(string,System.IO.FileAttributes,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileAttributes)
ICALL(MONOIO_22, "SetFileTime(intptr,long,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetFileTime)
ICALL(MONOIO_23, "SetLength(intptr,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_SetLength)
#ifndef PLATFORM_RO_FS
ICALL(MONOIO_24, "Unlock(intptr,long,long,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Unlock)
#endif
ICALL(MONOIO_25, "Write(intptr,byte[],int,int,System.IO.MonoIOError&)", ves_icall_System_IO_MonoIO_Write)
ICALL(MONOIO_26, "get_AltDirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_AltDirectorySeparatorChar)
ICALL(MONOIO_27, "get_ConsoleError", ves_icall_System_IO_MonoIO_get_ConsoleError)
ICALL(MONOIO_28, "get_ConsoleInput", ves_icall_System_IO_MonoIO_get_ConsoleInput)
ICALL(MONOIO_29, "get_ConsoleOutput", ves_icall_System_IO_MonoIO_get_ConsoleOutput)
ICALL(MONOIO_30, "get_DirectorySeparatorChar", ves_icall_System_IO_MonoIO_get_DirectorySeparatorChar)
ICALL(MONOIO_31, "get_InvalidPathChars", ves_icall_System_IO_MonoIO_get_InvalidPathChars)
ICALL(MONOIO_32, "get_PathSeparator", ves_icall_System_IO_MonoIO_get_PathSeparator)
ICALL(MONOIO_33, "get_VolumeSeparatorChar", ves_icall_System_IO_MonoIO_get_VolumeSeparatorChar)

ICALL_TYPE(IOPATH, "System.IO.Path", IOPATH_1)
ICALL(IOPATH_1, "get_temp_path", ves_icall_System_IO_get_temp_path)

ICALL_TYPE(MATH, "System.Math", MATH_1)
ICALL(MATH_1, "Acos", ves_icall_System_Math_Acos)
ICALL(MATH_2, "Asin", ves_icall_System_Math_Asin)
ICALL(MATH_3, "Atan", ves_icall_System_Math_Atan)
ICALL(MATH_4, "Atan2", ves_icall_System_Math_Atan2)
ICALL(MATH_5, "Cos", ves_icall_System_Math_Cos)
ICALL(MATH_6, "Cosh", ves_icall_System_Math_Cosh)
ICALL(MATH_7, "Exp", ves_icall_System_Math_Exp)
ICALL(MATH_8, "Floor", ves_icall_System_Math_Floor)
ICALL(MATH_9, "Log", ves_icall_System_Math_Log)
ICALL(MATH_10, "Log10", ves_icall_System_Math_Log10)
ICALL(MATH_11, "Pow", ves_icall_System_Math_Pow)
ICALL(MATH_12, "Round", ves_icall_System_Math_Round)
ICALL(MATH_13, "Round2", ves_icall_System_Math_Round2)
ICALL(MATH_14, "Sin", ves_icall_System_Math_Sin)
ICALL(MATH_15, "Sinh", ves_icall_System_Math_Sinh)
ICALL(MATH_16, "Sqrt", ves_icall_System_Math_Sqrt)
ICALL(MATH_17, "Tan", ves_icall_System_Math_Tan)
ICALL(MATH_18, "Tanh", ves_icall_System_Math_Tanh)

ICALL_TYPE(MCATTR, "System.MonoCustomAttrs", MCATTR_1)
ICALL(MCATTR_1, "GetCustomAttributesDataInternal", mono_reflection_get_custom_attrs_data)
ICALL(MCATTR_2, "GetCustomAttributesInternal", custom_attrs_get_by_type)
ICALL(MCATTR_3, "IsDefinedInternal", custom_attrs_defined_internal)

ICALL_TYPE(MENUM, "System.MonoEnumInfo", MENUM_1)
ICALL(MENUM_1, "get_enum_info", ves_icall_get_enum_info)

ICALL_TYPE(MTYPE, "System.MonoType", MTYPE_1)
ICALL(MTYPE_1, "GetArrayRank", ves_icall_MonoType_GetArrayRank)
ICALL(MTYPE_2, "GetConstructors", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_3, "GetConstructors_internal", ves_icall_Type_GetConstructors_internal)
ICALL(MTYPE_4, "GetCorrespondingInflatedConstructor", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_5, "GetCorrespondingInflatedMethod", ves_icall_MonoType_GetCorrespondingInflatedMethod)
ICALL(MTYPE_6, "GetElementType", ves_icall_MonoType_GetElementType)
ICALL(MTYPE_7, "GetEvents_internal", ves_icall_Type_GetEvents_internal)
ICALL(MTYPE_8, "GetField", ves_icall_Type_GetField)
ICALL(MTYPE_9, "GetFields_internal", ves_icall_Type_GetFields_internal)
ICALL(MTYPE_10, "GetGenericArguments", ves_icall_MonoType_GetGenericArguments)
ICALL(MTYPE_11, "GetInterfaces", ves_icall_Type_GetInterfaces)
ICALL(MTYPE_12, "GetMethodsByName", ves_icall_Type_GetMethodsByName)
ICALL(MTYPE_13, "GetNestedType", ves_icall_Type_GetNestedType)
ICALL(MTYPE_14, "GetNestedTypes", ves_icall_Type_GetNestedTypes)
ICALL(MTYPE_15, "GetPropertiesByName", ves_icall_Type_GetPropertiesByName)
ICALL(MTYPE_16, "InternalGetEvent", ves_icall_MonoType_GetEvent)
ICALL(MTYPE_17, "IsByRefImpl", ves_icall_type_isbyref)
ICALL(MTYPE_18, "IsCOMObjectImpl", ves_icall_type_iscomobject)
ICALL(MTYPE_19, "IsPointerImpl", ves_icall_type_ispointer)
ICALL(MTYPE_20, "IsPrimitiveImpl", ves_icall_type_isprimitive)
ICALL(MTYPE_21, "getFullName", ves_icall_System_MonoType_getFullName)
ICALL(MTYPE_22, "get_Assembly", ves_icall_MonoType_get_Assembly)
ICALL(MTYPE_23, "get_BaseType", ves_icall_get_type_parent)
ICALL(MTYPE_24, "get_DeclaringMethod", ves_icall_MonoType_get_DeclaringMethod)
ICALL(MTYPE_25, "get_DeclaringType", ves_icall_MonoType_get_DeclaringType)
ICALL(MTYPE_26, "get_IsGenericParameter", ves_icall_MonoType_get_IsGenericParameter)
ICALL(MTYPE_27, "get_Module", ves_icall_MonoType_get_Module)
ICALL(MTYPE_28, "get_Name", ves_icall_MonoType_get_Name)
ICALL(MTYPE_29, "get_Namespace", ves_icall_MonoType_get_Namespace)
ICALL(MTYPE_31, "get_attributes", ves_icall_get_attributes)
ICALL(MTYPE_33, "get_core_clr_security_level", vell_icall_MonoType_get_core_clr_security_level)
ICALL(MTYPE_32, "type_from_obj", mono_type_type_from_obj)

#ifndef DISABLE_SOCKETS
ICALL_TYPE(NDNS, "System.Net.Dns", NDNS_1)
ICALL(NDNS_1, "GetHostByAddr_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByAddr_internal)
ICALL(NDNS_2, "GetHostByName_internal(string,string&,string[]&,string[]&)", ves_icall_System_Net_Dns_GetHostByName_internal)
ICALL(NDNS_3, "GetHostName_internal(string&)", ves_icall_System_Net_Dns_GetHostName_internal)

ICALL_TYPE(SOCK, "System.Net.Sockets.Socket", SOCK_1)
ICALL(SOCK_1, "Accept_internal(intptr,int&,bool)", ves_icall_System_Net_Sockets_Socket_Accept_internal)
ICALL(SOCK_2, "Available_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Available_internal)
ICALL(SOCK_3, "Bind_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Bind_internal)
ICALL(SOCK_4, "Blocking_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Blocking_internal)
ICALL(SOCK_5, "Close_internal(intptr,int&)", ves_icall_System_Net_Sockets_Socket_Close_internal)
ICALL(SOCK_6, "Connect_internal(intptr,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_Connect_internal)
ICALL (SOCK_6a, "Disconnect_internal(intptr,bool,int&)", ves_icall_System_Net_Sockets_Socket_Disconnect_internal)
ICALL(SOCK_7, "GetSocketOption_arr_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,byte[]&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_arr_internal)
ICALL(SOCK_8, "GetSocketOption_obj_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object&,int&)", ves_icall_System_Net_Sockets_Socket_GetSocketOption_obj_internal)
ICALL(SOCK_9, "Listen_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_Listen_internal)
ICALL(SOCK_10, "LocalEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_LocalEndPoint_internal)
ICALL(SOCK_11, "Poll_internal", ves_icall_System_Net_Sockets_Socket_Poll_internal)
ICALL(SOCK_11a, "Receive_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_array_internal)
ICALL(SOCK_12, "Receive_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Receive_internal)
ICALL(SOCK_13, "RecvFrom_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress&,int&)", ves_icall_System_Net_Sockets_Socket_RecvFrom_internal)
ICALL(SOCK_14, "RemoteEndPoint_internal(intptr,int,int&)", ves_icall_System_Net_Sockets_Socket_RemoteEndPoint_internal)
ICALL(SOCK_15, "Select_internal(System.Net.Sockets.Socket[]&,int,int&)", ves_icall_System_Net_Sockets_Socket_Select_internal)
ICALL(SOCK_15a, "SendFile(intptr,string,byte[],byte[],System.Net.Sockets.TransmitFileOptions)", ves_icall_System_Net_Sockets_Socket_SendFile)
ICALL(SOCK_16, "SendTo_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,System.Net.SocketAddress,int&)", ves_icall_System_Net_Sockets_Socket_SendTo_internal)
ICALL(SOCK_16a, "Send_internal(intptr,System.Net.Sockets.Socket/WSABUF[],System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_array_internal)
ICALL(SOCK_17, "Send_internal(intptr,byte[],int,int,System.Net.Sockets.SocketFlags,int&)", ves_icall_System_Net_Sockets_Socket_Send_internal)
ICALL(SOCK_18, "SetSocketOption_internal(intptr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,object,byte[],int,int&)", ves_icall_System_Net_Sockets_Socket_SetSocketOption_internal)
ICALL(SOCK_19, "Shutdown_internal(intptr,System.Net.Sockets.SocketShutdown,int&)", ves_icall_System_Net_Sockets_Socket_Shutdown_internal)
ICALL(SOCK_20, "Socket_internal(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType,int&)", ves_icall_System_Net_Sockets_Socket_Socket_internal)
ICALL(SOCK_21, "WSAIoctl(intptr,int,byte[],byte[],int&)", ves_icall_System_Net_Sockets_Socket_WSAIoctl)
ICALL(SOCK_21a, "cancel_blocking_socket_operation", icall_cancel_blocking_socket_operation)
ICALL(SOCK_22, "socket_pool_queue", icall_append_io_job)

ICALL_TYPE(SOCKEX, "System.Net.Sockets.SocketException", SOCKEX_1)
ICALL(SOCKEX_1, "WSAGetLastError_internal", ves_icall_System_Net_Sockets_SocketException_WSAGetLastError_internal)
#endif /* !DISABLE_SOCKETS */

ICALL_TYPE(NUMBER_FORMATTER, "System.NumberFormatter", NUMBER_FORMATTER_1)
ICALL(NUMBER_FORMATTER_1, "GetFormatterTables", ves_icall_System_NumberFormatter_GetFormatterTables)

ICALL_TYPE(OBJ, "System.Object", OBJ_1)
ICALL(OBJ_1, "GetType", ves_icall_System_Object_GetType)
ICALL(OBJ_2, "InternalGetHashCode", mono_object_hash)
ICALL(OBJ_3, "MemberwiseClone", ves_icall_System_Object_MemberwiseClone)
ICALL(OBJ_4, "obj_address", ves_icall_System_Object_obj_address)

ICALL_TYPE(ASSEM, "System.Reflection.Assembly", ASSEM_1)
ICALL(ASSEM_1, "FillName", ves_icall_System_Reflection_Assembly_FillName)
ICALL(ASSEM_2, "GetCallingAssembly", ves_icall_System_Reflection_Assembly_GetCallingAssembly)
ICALL(ASSEM_3, "GetEntryAssembly", ves_icall_System_Reflection_Assembly_GetEntryAssembly)
ICALL(ASSEM_4, "GetExecutingAssembly", ves_icall_System_Reflection_Assembly_GetExecutingAssembly)
ICALL(ASSEM_5, "GetFilesInternal", ves_icall_System_Reflection_Assembly_GetFilesInternal)
ICALL(ASSEM_6, "GetManifestModuleInternal", ves_icall_System_Reflection_Assembly_GetManifestModuleInternal)
ICALL(ASSEM_7, "GetManifestResourceInfoInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInfoInternal)
ICALL(ASSEM_8, "GetManifestResourceInternal", ves_icall_System_Reflection_Assembly_GetManifestResourceInternal)
ICALL(ASSEM_9, "GetManifestResourceNames", ves_icall_System_Reflection_Assembly_GetManifestResourceNames)
ICALL(ASSEM_10, "GetModulesInternal", ves_icall_System_Reflection_Assembly_GetModulesInternal)
//ICALL(ASSEM_11, "GetNamespaces", ves_icall_System_Reflection_Assembly_GetNamespaces)
ICALL(ASSEM_12, "GetReferencedAssemblies", ves_icall_System_Reflection_Assembly_GetReferencedAssemblies)
ICALL(ASSEM_13, "GetTypes", ves_icall_System_Reflection_Assembly_GetTypes)
ICALL(ASSEM_14, "InternalGetAssemblyName", ves_icall_System_Reflection_Assembly_InternalGetAssemblyName)
ICALL(ASSEM_15, "InternalGetType", ves_icall_System_Reflection_Assembly_InternalGetType)
ICALL(ASSEM_16, "InternalImageRuntimeVersion", ves_icall_System_Reflection_Assembly_InternalImageRuntimeVersion)
ICALL(ASSEM_17, "LoadFrom", ves_icall_System_Reflection_Assembly_LoadFrom)
ICALL(ASSEM_18, "LoadPermissions", ves_icall_System_Reflection_Assembly_LoadPermissions)
	/*
	 * Private icalls for the Mono Debugger
	 */
ICALL(ASSEM_19, "MonoDebugger_GetMethodToken", ves_icall_MonoDebugger_GetMethodToken)

	/* normal icalls again */
ICALL(ASSEM_20, "get_EntryPoint", ves_icall_System_Reflection_Assembly_get_EntryPoint)
ICALL(ASSEM_21, "get_ReflectionOnly", ves_icall_System_Reflection_Assembly_get_ReflectionOnly)
ICALL(ASSEM_22, "get_code_base", ves_icall_System_Reflection_Assembly_get_code_base)
ICALL(ASSEM_23, "get_fullname", ves_icall_System_Reflection_Assembly_get_fullName)
ICALL(ASSEM_24, "get_global_assembly_cache", ves_icall_System_Reflection_Assembly_get_global_assembly_cache)
ICALL(ASSEM_25, "get_location", ves_icall_System_Reflection_Assembly_get_location)
ICALL(ASSEM_26, "load_with_partial_name", ves_icall_System_Reflection_Assembly_load_with_partial_name)

ICALL_TYPE(ASSEMN, "System.Reflection.AssemblyName", ASSEMN_1)
ICALL(ASSEMN_1, "ParseName", ves_icall_System_Reflection_AssemblyName_ParseName)
ICALL(ASSEMN_2, "get_public_token", mono_digest_get_public_token)

ICALL_TYPE(CATTR_DATA, "System.Reflection.CustomAttributeData", CATTR_DATA_1)
ICALL(CATTR_DATA_1, "ResolveArgumentsInternal", mono_reflection_resolve_custom_attribute_data)

ICALL_TYPE(ASSEMB, "System.Reflection.Emit.AssemblyBuilder", ASSEMB_1)
ICALL(ASSEMB_1, "InternalAddModule", mono_image_load_module_dynamic)
ICALL(ASSEMB_2, "basic_init", mono_image_basic_init)

ICALL_TYPE(CATTRB, "System.Reflection.Emit.CustomAttributeBuilder", CATTRB_1)
ICALL(CATTRB_1, "GetBlob", mono_reflection_get_custom_attrs_blob)

#ifndef DISABLE_REFLECTION_EMIT
ICALL_TYPE(DERIVEDTYPE, "System.Reflection.Emit.DerivedType", DERIVEDTYPE_1)
ICALL(DERIVEDTYPE_1, "create_unmanaged_type", mono_reflection_create_unmanaged_type)
#endif

ICALL_TYPE(DYNM, "System.Reflection.Emit.DynamicMethod", DYNM_1)
ICALL(DYNM_1, "create_dynamic_method", mono_reflection_create_dynamic_method)

ICALL_TYPE(ENUMB, "System.Reflection.Emit.EnumBuilder", ENUMB_1)
ICALL(ENUMB_1, "setup_enum_type", ves_icall_EnumBuilder_setup_enum_type)

ICALL_TYPE(GPARB, "System.Reflection.Emit.GenericTypeParameterBuilder", GPARB_1)
ICALL(GPARB_1, "initialize", mono_reflection_initialize_generic_parameter)

ICALL_TYPE(METHODB, "System.Reflection.Emit.MethodBuilder", METHODB_1)
ICALL(METHODB_1, "MakeGenericMethod", mono_reflection_bind_generic_method_parameters)

ICALL_TYPE(MODULEB, "System.Reflection.Emit.ModuleBuilder", MODULEB_8)
ICALL(MODULEB_8, "RegisterToken", ves_icall_ModuleBuilder_RegisterToken)
ICALL(MODULEB_1, "WriteToFile", ves_icall_ModuleBuilder_WriteToFile)
ICALL(MODULEB_2, "basic_init", mono_image_module_basic_init)
ICALL(MODULEB_3, "build_metadata", ves_icall_ModuleBuilder_build_metadata)
ICALL(MODULEB_4, "create_modified_type", ves_icall_ModuleBuilder_create_modified_type)
ICALL(MODULEB_5, "getMethodToken", ves_icall_ModuleBuilder_getMethodToken)
ICALL(MODULEB_6, "getToken", ves_icall_ModuleBuilder_getToken)
ICALL(MODULEB_7, "getUSIndex", mono_image_insert_string)
ICALL(MODULEB_9, "set_wrappers_type", mono_image_set_wrappers_type)

ICALL_TYPE(SIGH, "System.Reflection.Emit.SignatureHelper", SIGH_1)
ICALL(SIGH_1, "get_signature_field", mono_reflection_sighelper_get_signature_field)
ICALL(SIGH_2, "get_signature_local", mono_reflection_sighelper_get_signature_local)

ICALL_TYPE(TYPEB, "System.Reflection.Emit.TypeBuilder", TYPEB_1)
ICALL(TYPEB_1, "create_generic_class", mono_reflection_create_generic_class)
ICALL(TYPEB_2, "create_internal_class", mono_reflection_create_internal_class)
ICALL(TYPEB_3, "create_runtime_class", mono_reflection_create_runtime_class)
ICALL(TYPEB_4, "get_IsGenericParameter", ves_icall_TypeBuilder_get_IsGenericParameter)
ICALL(TYPEB_5, "get_event_info", mono_reflection_event_builder_get_event_info)
ICALL(TYPEB_6, "setup_generic_class", mono_reflection_setup_generic_class)
ICALL(TYPEB_7, "setup_internal_class", mono_reflection_setup_internal_class)

ICALL_TYPE(FIELDI, "System.Reflection.FieldInfo", FILEDI_1)
ICALL(FILEDI_1, "GetTypeModifiers", ves_icall_System_Reflection_FieldInfo_GetTypeModifiers)
ICALL(FILEDI_2, "get_marshal_info", ves_icall_System_Reflection_FieldInfo_get_marshal_info)
ICALL(FILEDI_3, "internal_from_handle_type", ves_icall_System_Reflection_FieldInfo_internal_from_handle_type)

ICALL_TYPE(MEMBERI, "System.Reflection.MemberInfo", MEMBERI_1)
ICALL(MEMBERI_1, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MBASE, "System.Reflection.MethodBase", MBASE_1)
ICALL(MBASE_1, "GetCurrentMethod", ves_icall_GetCurrentMethod)
ICALL(MBASE_2, "GetMethodBodyInternal", ves_icall_System_Reflection_MethodBase_GetMethodBodyInternal)
ICALL(MBASE_3, "GetMethodFromHandleInternal", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternal)
ICALL(MBASE_4, "GetMethodFromHandleInternalType", ves_icall_System_Reflection_MethodBase_GetMethodFromHandleInternalType)

ICALL_TYPE(MODULE, "System.Reflection.Module", MODULE_1)
ICALL(MODULE_1, "Close", ves_icall_System_Reflection_Module_Close)
ICALL(MODULE_2, "GetGlobalType", ves_icall_System_Reflection_Module_GetGlobalType)
ICALL(MODULE_3, "GetGuidInternal", ves_icall_System_Reflection_Module_GetGuidInternal)
ICALL(MODULE_14, "GetHINSTANCE", ves_icall_System_Reflection_Module_GetHINSTANCE)
ICALL(MODULE_4, "GetMDStreamVersion", ves_icall_System_Reflection_Module_GetMDStreamVersion)
ICALL(MODULE_5, "GetPEKind", ves_icall_System_Reflection_Module_GetPEKind)
ICALL(MODULE_6, "InternalGetTypes", ves_icall_System_Reflection_Module_InternalGetTypes)
ICALL(MODULE_7, "ResolveFieldToken", ves_icall_System_Reflection_Module_ResolveFieldToken)
ICALL(MODULE_8, "ResolveMemberToken", ves_icall_System_Reflection_Module_ResolveMemberToken)
ICALL(MODULE_9, "ResolveMethodToken", ves_icall_System_Reflection_Module_ResolveMethodToken)
ICALL(MODULE_10, "ResolveSignature", ves_icall_System_Reflection_Module_ResolveSignature)
ICALL(MODULE_11, "ResolveStringToken", ves_icall_System_Reflection_Module_ResolveStringToken)
ICALL(MODULE_12, "ResolveTypeToken", ves_icall_System_Reflection_Module_ResolveTypeToken)
ICALL(MODULE_13, "get_MetadataToken", mono_reflection_get_token)

ICALL_TYPE(MCMETH, "System.Reflection.MonoCMethod", MCMETH_1)
ICALL(MCMETH_1, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MCMETH_2, "InternalInvoke", ves_icall_InternalInvoke)

ICALL_TYPE(MEVIN, "System.Reflection.MonoEventInfo", MEVIN_1)
ICALL(MEVIN_1, "get_event_info", ves_icall_get_event_info)

ICALL_TYPE(MFIELD, "System.Reflection.MonoField", MFIELD_1)
ICALL(MFIELD_1, "GetFieldOffset", ves_icall_MonoField_GetFieldOffset)
ICALL(MFIELD_2, "GetParentType", ves_icall_MonoField_GetParentType)
ICALL(MFIELD_5, "GetRawConstantValue", ves_icall_MonoField_GetRawConstantValue)
ICALL(MFIELD_3, "GetValueInternal", ves_icall_MonoField_GetValueInternal)
ICALL(MFIELD_6, "ResolveType", ves_icall_MonoField_ResolveType)
ICALL(MFIELD_4, "SetValueInternal", ves_icall_MonoField_SetValueInternal)

ICALL_TYPE(MGENCM, "System.Reflection.MonoGenericCMethod", MGENCM_1)
ICALL(MGENCM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MGENCL, "System.Reflection.MonoGenericClass", MGENCL_5)
ICALL(MGENCL_5, "initialize", mono_reflection_generic_class_initialize)
ICALL(MGENCL_6, "register_with_runtime", mono_reflection_register_with_runtime)

/* note this is the same as above: unify */
ICALL_TYPE(MGENM, "System.Reflection.MonoGenericMethod", MGENM_1)
ICALL(MGENM_1, "get_ReflectedType", ves_icall_MonoGenericMethod_get_ReflectedType)

ICALL_TYPE(MMETH, "System.Reflection.MonoMethod", MMETH_1)
ICALL(MMETH_1, "GetDllImportAttribute", ves_icall_MonoMethod_GetDllImportAttribute)
ICALL(MMETH_2, "GetGenericArguments", ves_icall_MonoMethod_GetGenericArguments)
ICALL(MMETH_3, "GetGenericMethodDefinition_impl", ves_icall_MonoMethod_GetGenericMethodDefinition)
ICALL(MMETH_4, "InternalInvoke", ves_icall_InternalInvoke)
ICALL(MMETH_5, "MakeGenericMethod_impl", mono_reflection_bind_generic_method_parameters)
ICALL(MMETH_6, "get_IsGenericMethod", ves_icall_MonoMethod_get_IsGenericMethod)
ICALL(MMETH_7, "get_IsGenericMethodDefinition", ves_icall_MonoMethod_get_IsGenericMethodDefinition)
ICALL(MMETH_8, "get_base_method", ves_icall_MonoMethod_get_base_method)
ICALL(MMETH_9, "get_name", ves_icall_MonoMethod_get_name)

ICALL_TYPE(MMETHI, "System.Reflection.MonoMethodInfo", MMETHI_4)
ICALL(MMETHI_4, "get_method_attributes", vell_icall_get_method_attributes)
ICALL(MMETHI_1, "get_method_info", ves_icall_get_method_info)
ICALL(MMETHI_2, "get_parameter_info", ves_icall_get_parameter_info)
ICALL(MMETHI_3, "get_retval_marshal", ves_icall_System_MonoMethodInfo_get_retval_marshal)

ICALL_TYPE(MPROPI, "System.Reflection.MonoPropertyInfo", MPROPI_1)
ICALL(MPROPI_1, "GetTypeModifiers", property_info_get_type_modifiers)
ICALL(MPROPI_3, "get_default_value", property_info_get_default_value)
ICALL(MPROPI_2, "get_property_info", ves_icall_get_property_info)

ICALL_TYPE(PARAMI, "System.Reflection.ParameterInfo", PARAMI_1)
ICALL(PARAMI_1, "GetMetadataToken", mono_reflection_get_token)
ICALL(PARAMI_2, "GetTypeModifiers", param_info_get_type_modifiers)

ICALL_TYPE(RUNH, "System.Runtime.CompilerServices.RuntimeHelpers", RUNH_1)
ICALL(RUNH_1, "GetObjectValue", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetObjectValue)
	 /* REMOVEME: no longer needed, just so we dont break things when not needed */
ICALL(RUNH_2, "GetOffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)
ICALL(RUNH_3, "InitializeArray", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_InitializeArray)
ICALL(RUNH_4, "RunClassConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunClassConstructor)
ICALL(RUNH_5, "RunModuleConstructor", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_RunModuleConstructor)
ICALL(RUNH_5h, "SufficientExecutionStack", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_SufficientExecutionStack)
ICALL(RUNH_6, "get_OffsetToStringData", ves_icall_System_Runtime_CompilerServices_RuntimeHelpers_GetOffsetToStringData)

ICALL_TYPE(GCH, "System.Runtime.InteropServices.GCHandle", GCH_1)
ICALL(GCH_1, "CheckCurrentDomain", GCHandle_CheckCurrentDomain)
ICALL(GCH_2, "FreeHandle", ves_icall_System_GCHandle_FreeHandle)
ICALL(GCH_3, "GetAddrOfPinnedObject", ves_icall_System_GCHandle_GetAddrOfPinnedObject)
ICALL(GCH_4, "GetTarget", ves_icall_System_GCHandle_GetTarget)
ICALL(GCH_5, "GetTargetHandle", ves_icall_System_GCHandle_GetTargetHandle)

#ifndef DISABLE_COM
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_1)
ICALL(MARSHAL_1, "AddRefInternal", ves_icall_System_Runtime_InteropServices_Marshal_AddRefInternal)
#else
ICALL_TYPE(MARSHAL, "System.Runtime.InteropServices.Marshal", MARSHAL_2)
#endif
ICALL(MARSHAL_2, "AllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_AllocCoTaskMem)
ICALL(MARSHAL_3, "AllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_AllocHGlobal)
ICALL(MARSHAL_4, "DestroyStructure", ves_icall_System_Runtime_InteropServices_Marshal_DestroyStructure)
ICALL(MARSHAL_5, "FreeBSTR", ves_icall_System_Runtime_InteropServices_Marshal_FreeBSTR)
ICALL(MARSHAL_6, "FreeCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_FreeCoTaskMem)
ICALL(MARSHAL_7, "FreeHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_FreeHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_44, "GetCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetCCW)
ICALL(MARSHAL_8, "GetComSlotForMethodInfoInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetComSlotForMethodInfoInternal)
#endif
ICALL(MARSHAL_9, "GetDelegateForFunctionPointerInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetDelegateForFunctionPointerInternal)
ICALL(MARSHAL_10, "GetFunctionPointerForDelegateInternal", mono_delegate_to_ftnptr)
#ifndef DISABLE_COM
ICALL(MARSHAL_45, "GetIDispatchForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIDispatchForObjectInternal)
ICALL(MARSHAL_46, "GetIUnknownForObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_GetIUnknownForObjectInternal)
#endif
ICALL(MARSHAL_11, "GetLastWin32Error", ves_icall_System_Runtime_InteropServices_Marshal_GetLastWin32Error)
#ifndef DISABLE_COM
ICALL(MARSHAL_47, "GetObjectForCCW", ves_icall_System_Runtime_InteropServices_Marshal_GetObjectForCCW)
ICALL(MARSHAL_48, "IsComObject", ves_icall_System_Runtime_InteropServices_Marshal_IsComObject)
#endif
ICALL(MARSHAL_12, "OffsetOf", ves_icall_System_Runtime_InteropServices_Marshal_OffsetOf)
ICALL(MARSHAL_13, "Prelink", ves_icall_System_Runtime_InteropServices_Marshal_Prelink)
ICALL(MARSHAL_14, "PrelinkAll", ves_icall_System_Runtime_InteropServices_Marshal_PrelinkAll)
ICALL(MARSHAL_15, "PtrToStringAnsi(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi)
ICALL(MARSHAL_16, "PtrToStringAnsi(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringAnsi_len)
#ifndef DISABLE_COM
ICALL(MARSHAL_17, "PtrToStringBSTR", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringBSTR)
#endif
ICALL(MARSHAL_18, "PtrToStringUni(intptr)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni)
ICALL(MARSHAL_19, "PtrToStringUni(intptr,int)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStringUni_len)
ICALL(MARSHAL_20, "PtrToStructure(intptr,System.Type)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure_type)
ICALL(MARSHAL_21, "PtrToStructure(intptr,object)", ves_icall_System_Runtime_InteropServices_Marshal_PtrToStructure)
#ifndef DISABLE_COM
ICALL(MARSHAL_22, "QueryInterfaceInternal", ves_icall_System_Runtime_InteropServices_Marshal_QueryInterfaceInternal)
#endif
ICALL(MARSHAL_43, "ReAllocCoTaskMem", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocCoTaskMem)
ICALL(MARSHAL_23, "ReAllocHGlobal", ves_icall_System_Runtime_InteropServices_Marshal_ReAllocHGlobal)
#ifndef DISABLE_COM
ICALL(MARSHAL_49, "ReleaseComObjectInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseComObjectInternal)
ICALL(MARSHAL_29, "ReleaseInternal", ves_icall_System_Runtime_InteropServices_Marshal_ReleaseInternal)
#endif
ICALL(MARSHAL_30, "SizeOf", ves_icall_System_Runtime_InteropServices_Marshal_SizeOf)
ICALL(MARSHAL_31, "StringToBSTR", ves_icall_System_Runtime_InteropServices_Marshal_StringToBSTR)
ICALL(MARSHAL_32, "StringToHGlobalAnsi", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalAnsi)
ICALL(MARSHAL_33, "StringToHGlobalUni", ves_icall_System_Runtime_InteropServices_Marshal_StringToHGlobalUni)
ICALL(MARSHAL_34, "StructureToPtr", ves_icall_System_Runtime_InteropServices_Marshal_StructureToPtr)
ICALL(MARSHAL_35, "UnsafeAddrOfPinnedArrayElement", ves_icall_System_Runtime_InteropServices_Marshal_UnsafeAddrOfPinnedArrayElement)
ICALL(MARSHAL_41, "copy_from_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_from_unmanaged)
ICALL(MARSHAL_42, "copy_to_unmanaged", ves_icall_System_Runtime_InteropServices_Marshal_copy_to_unmanaged)

ICALL_TYPE(ACTS, "System.Runtime.Remoting.Activation.ActivationServices", ACTS_1)
ICALL(ACTS_1, "AllocateUninitializedClassInstance", ves_icall_System_Runtime_Activation_ActivationServices_AllocateUninitializedClassInstance)
ICALL(ACTS_2, "EnableProxyActivation", ves_icall_System_Runtime_Activation_ActivationServices_EnableProxyActivation)

ICALL_TYPE(MONOMM, "System.Runtime.Remoting.Messaging.MonoMethodMessage", MONOMM_1)
ICALL(MONOMM_1, "InitMessage", ves_icall_MonoMethodMessage_InitMessage)

#ifndef DISABLE_REMOTING
ICALL_TYPE(REALP, "System.Runtime.Remoting.Proxies.RealProxy", REALP_1)
ICALL(REALP_1, "InternalGetProxyType", ves_icall_Remoting_RealProxy_InternalGetProxyType)
ICALL(REALP_2, "InternalGetTransparentProxy", ves_icall_Remoting_RealProxy_GetTransparentProxy)

ICALL_TYPE(REMSER, "System.Runtime.Remoting.RemotingServices", REMSER_0)
ICALL(REMSER_0, "GetVirtualMethod", ves_icall_Remoting_RemotingServices_GetVirtualMethod)
ICALL(REMSER_1, "InternalExecute", ves_icall_InternalExecute)
ICALL(REMSER_2, "IsTransparentProxy", ves_icall_IsTransparentProxy)
#endif

ICALL_TYPE(MHAN, "System.RuntimeMethodHandle", MHAN_1)
ICALL(MHAN_1, "GetFunctionPointer", ves_icall_RuntimeMethod_GetFunctionPointer)

ICALL_TYPE(RNG, "System.Security.Cryptography.RNGCryptoServiceProvider", RNG_1)
ICALL(RNG_1, "RngClose", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngClose)
ICALL(RNG_2, "RngGetBytes", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngGetBytes)
ICALL(RNG_3, "RngInitialize", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngInitialize)
ICALL(RNG_4, "RngOpen", ves_icall_System_Security_Cryptography_RNGCryptoServiceProvider_RngOpen)

#ifndef DISABLE_POLICY_EVIDENCE
ICALL_TYPE(EVID, "System.Security.Policy.Evidence", EVID_1)
ICALL(EVID_1, "IsAuthenticodePresent", ves_icall_System_Security_Policy_Evidence_IsAuthenticodePresent)

ICALL_TYPE(WINID, "System.Security.Principal.WindowsIdentity", WINID_1)
ICALL(WINID_1, "GetCurrentToken", ves_icall_System_Security_Principal_WindowsIdentity_GetCurrentToken)
ICALL(WINID_2, "GetTokenName", ves_icall_System_Security_Principal_WindowsIdentity_GetTokenName)
ICALL(WINID_3, "GetUserToken", ves_icall_System_Security_Principal_WindowsIdentity_GetUserToken)
ICALL(WINID_4, "_GetRoles", ves_icall_System_Security_Principal_WindowsIdentity_GetRoles)

ICALL_TYPE(WINIMP, "System.Security.Principal.WindowsImpersonationContext", WINIMP_1)
ICALL(WINIMP_1, "CloseToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_CloseToken)
ICALL(WINIMP_2, "DuplicateToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_DuplicateToken)
ICALL(WINIMP_3, "RevertToSelf", ves_icall_System_Security_Principal_WindowsImpersonationContext_RevertToSelf)
ICALL(WINIMP_4, "SetCurrentToken", ves_icall_System_Security_Principal_WindowsImpersonationContext_SetCurrentToken)

ICALL_TYPE(WINPRIN, "System.Security.Principal.WindowsPrincipal", WINPRIN_1)
ICALL(WINPRIN_1, "IsMemberOfGroupId", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupId)
ICALL(WINPRIN_2, "IsMemberOfGroupName", ves_icall_System_Security_Principal_WindowsPrincipal_IsMemberOfGroupName)

ICALL_TYPE(SECSTRING, "System.Security.SecureString", SECSTRING_1)
ICALL(SECSTRING_1, "DecryptInternal", ves_icall_System_Security_SecureString_DecryptInternal)
ICALL(SECSTRING_2, "EncryptInternal", ves_icall_System_Security_SecureString_EncryptInternal)
#endif /* !DISABLE_POLICY_EVIDENCE */

ICALL_TYPE(SECMAN, "System.Security.SecurityManager", SECMAN_1)
ICALL(SECMAN_1, "GetLinkDemandSecurity", ves_icall_System_Security_SecurityManager_GetLinkDemandSecurity)
ICALL(SECMAN_2, "get_CheckExecutionRights", ves_icall_System_Security_SecurityManager_get_CheckExecutionRights)
ICALL(SECMAN_3, "get_RequiresElevatedPermissions", mono_security_core_clr_require_elevated_permissions)
ICALL(SECMAN_4, "get_SecurityEnabled", ves_icall_System_Security_SecurityManager_get_SecurityEnabled)
ICALL(SECMAN_5, "set_CheckExecutionRights", ves_icall_System_Security_SecurityManager_set_CheckExecutionRights)
ICALL(SECMAN_6, "set_SecurityEnabled", ves_icall_System_Security_SecurityManager_set_SecurityEnabled)

ICALL_TYPE(STRING, "System.String", STRING_1)
ICALL(STRING_1, ".ctor(char*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_2, ".ctor(char*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_3, ".ctor(char,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_4, ".ctor(char[])", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_5, ".ctor(char[],int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_6, ".ctor(sbyte*)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_7, ".ctor(sbyte*,int,int)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8, ".ctor(sbyte*,int,int,System.Text.Encoding)", ves_icall_System_String_ctor_RedirectToCreateString)
ICALL(STRING_8a, "GetLOSLimit", ves_icall_System_String_GetLOSLimit)
ICALL(STRING_9, "InternalAllocateStr", ves_icall_System_String_InternalAllocateStr)
ICALL(STRING_10, "InternalIntern", ves_icall_System_String_InternalIntern)
ICALL(STRING_11, "InternalIsInterned", ves_icall_System_String_InternalIsInterned)

ICALL_TYPE(TENC, "System.Text.Encoding", TENC_1)
ICALL(TENC_1, "InternalCodePage", ves_icall_System_Text_Encoding_InternalCodePage)

ICALL_TYPE(ILOCK, "System.Threading.Interlocked", ILOCK_1)
ICALL(ILOCK_1, "Add(int&,int)", ves_icall_System_Threading_Interlocked_Add_Int)
ICALL(ILOCK_2, "Add(long&,long)", ves_icall_System_Threading_Interlocked_Add_Long)
ICALL(ILOCK_3, "CompareExchange(T&,T,T)", ves_icall_System_Threading_Interlocked_CompareExchange_T)
ICALL(ILOCK_4, "CompareExchange(double&,double,double)", ves_icall_System_Threading_Interlocked_CompareExchange_Double)
ICALL(ILOCK_5, "CompareExchange(int&,int,int)", ves_icall_System_Threading_Interlocked_CompareExchange_Int)
ICALL(ILOCK_6, "CompareExchange(intptr&,intptr,intptr)", ves_icall_System_Threading_Interlocked_CompareExchange_IntPtr)
ICALL(ILOCK_7, "CompareExchange(long&,long,long)", ves_icall_System_Threading_Interlocked_CompareExchange_Long)
ICALL(ILOCK_8, "CompareExchange(object&,object,object)", ves_icall_System_Threading_Interlocked_CompareExchange_Object)
ICALL(ILOCK_9, "CompareExchange(single&,single,single)", ves_icall_System_Threading_Interlocked_CompareExchange_Single)
ICALL(ILOCK_10, "Decrement(int&)", ves_icall_System_Threading_Interlocked_Decrement_Int)
ICALL(ILOCK_11, "Decrement(long&)", ves_icall_System_Threading_Interlocked_Decrement_Long)
ICALL(ILOCK_12, "Exchange(T&,T)", ves_icall_System_Threading_Interlocked_Exchange_T)
ICALL(ILOCK_13, "Exchange(double&,double)", ves_icall_System_Threading_Interlocked_Exchange_Double)
ICALL(ILOCK_14, "Exchange(int&,int)", ves_icall_System_Threading_Interlocked_Exchange_Int)
ICALL(ILOCK_15, "Exchange(intptr&,intptr)", ves_icall_System_Threading_Interlocked_Exchange_IntPtr)
ICALL(ILOCK_16, "Exchange(long&,long)", ves_icall_System_Threading_Interlocked_Exchange_Long)
ICALL(ILOCK_17, "Exchange(object&,object)", ves_icall_System_Threading_Interlocked_Exchange_Object)
ICALL(ILOCK_18, "Exchange(single&,single)", ves_icall_System_Threading_Interlocked_Exchange_Single)
ICALL(ILOCK_19, "Increment(int&)", ves_icall_System_Threading_Interlocked_Increment_Int)
ICALL(ILOCK_20, "Increment(long&)", ves_icall_System_Threading_Interlocked_Increment_Long)
ICALL(ILOCK_21, "Read(long&)", ves_icall_System_Threading_Interlocked_Read_Long)

ICALL_TYPE(ITHREAD, "System.Threading.InternalThread", ITHREAD_1)
ICALL(ITHREAD_1, "Thread_free_internal", ves_icall_System_Threading_InternalThread_Thread_free_internal)

ICALL_TYPE(MONIT, "System.Threading.Monitor", MONIT_8)
ICALL(MONIT_8, "Enter", mono_monitor_enter)
ICALL(MONIT_1, "Exit", mono_monitor_exit)
ICALL(MONIT_2, "Monitor_pulse", ves_icall_System_Threading_Monitor_Monitor_pulse)
ICALL(MONIT_3, "Monitor_pulse_all", ves_icall_System_Threading_Monitor_Monitor_pulse_all)
ICALL(MONIT_4, "Monitor_test_owner", ves_icall_System_Threading_Monitor_Monitor_test_owner)
ICALL(MONIT_5, "Monitor_test_synchronised", ves_icall_System_Threading_Monitor_Monitor_test_synchronised)
ICALL(MONIT_6, "Monitor_try_enter", ves_icall_System_Threading_Monitor_Monitor_try_enter)
ICALL(MONIT_7, "Monitor_wait", ves_icall_System_Threading_Monitor_Monitor_wait)
ICALL(MONIT_9, "try_enter_with_atomic_var", ves_icall_System_Threading_Monitor_Monitor_try_enter_with_atomic_var)

ICALL_TYPE(MUTEX, "System.Threading.Mutex", MUTEX_1)
ICALL(MUTEX_1, "CreateMutex_internal(bool,string,bool&)", ves_icall_System_Threading_Mutex_CreateMutex_internal)
ICALL(MUTEX_2, "OpenMutex_internal(string,System.Security.AccessControl.MutexRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Mutex_OpenMutex_internal)
ICALL(MUTEX_3, "ReleaseMutex_internal(intptr)", ves_icall_System_Threading_Mutex_ReleaseMutex_internal)

ICALL_TYPE(NATIVEC, "System.Threading.NativeEventCalls", NATIVEC_1)
ICALL(NATIVEC_1, "CloseEvent_internal", ves_icall_System_Threading_Events_CloseEvent_internal)
ICALL(NATIVEC_2, "CreateEvent_internal(bool,bool,string,bool&)", ves_icall_System_Threading_Events_CreateEvent_internal)
ICALL(NATIVEC_3, "OpenEvent_internal(string,System.Security.AccessControl.EventWaitHandleRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Events_OpenEvent_internal)
ICALL(NATIVEC_4, "ResetEvent_internal",  ves_icall_System_Threading_Events_ResetEvent_internal)
ICALL(NATIVEC_5, "SetEvent_internal",    ves_icall_System_Threading_Events_SetEvent_internal)

ICALL_TYPE(SEMA, "System.Threading.Semaphore", SEMA_1)
ICALL(SEMA_1, "CreateSemaphore_internal(int,int,string,bool&)", ves_icall_System_Threading_Semaphore_CreateSemaphore_internal)
ICALL(SEMA_2, "OpenSemaphore_internal(string,System.Security.AccessControl.SemaphoreRights,System.IO.MonoIOError&)", ves_icall_System_Threading_Semaphore_OpenSemaphore_internal)
ICALL(SEMA_3, "ReleaseSemaphore_internal(intptr,int,bool&)", ves_icall_System_Threading_Semaphore_ReleaseSemaphore_internal)

ICALL_TYPE(THREAD, "System.Threading.Thread", THREAD_1)
ICALL(THREAD_1, "Abort_internal(System.Threading.InternalThread,object)", ves_icall_System_Threading_Thread_Abort)
ICALL(THREAD_1aa, "AllocTlsData", mono_thread_alloc_tls)
ICALL(THREAD_1a, "ByteArrayToCurrentDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToCurrentDomain)
ICALL(THREAD_1b, "ByteArrayToRootDomain(byte[])", ves_icall_System_Threading_Thread_ByteArrayToRootDomain)
ICALL(THREAD_2, "ClrState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_ClrState)
ICALL(THREAD_2a, "ConstructInternalThread", ves_icall_System_Threading_Thread_ConstructInternalThread)
ICALL(THREAD_3, "CurrentInternalThread_internal", mono_thread_internal_current)
ICALL(THREAD_3a, "DestroyTlsData", mono_thread_destroy_tls)
ICALL(THREAD_4, "FreeLocalSlotValues", mono_thread_free_local_slot_values)
ICALL(THREAD_55, "GetAbortExceptionState", ves_icall_System_Threading_Thread_GetAbortExceptionState)
ICALL(THREAD_7, "GetDomainID", ves_icall_System_Threading_Thread_GetDomainID)
ICALL(THREAD_8, "GetName_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetName_internal)
ICALL(THREAD_11, "GetState(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_GetState)
ICALL(THREAD_53, "Interrupt_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Interrupt_internal)
ICALL(THREAD_12, "Join_internal(System.Threading.InternalThread,int,intptr)", ves_icall_System_Threading_Thread_Join_internal)
ICALL(THREAD_13, "MemoryBarrier", ves_icall_System_Threading_Thread_MemoryBarrier)
ICALL(THREAD_14, "ResetAbort_internal()", ves_icall_System_Threading_Thread_ResetAbort)
ICALL(THREAD_15, "Resume_internal()", ves_icall_System_Threading_Thread_Resume)
ICALL(THREAD_18, "SetName_internal(System.Threading.InternalThread,string)", ves_icall_System_Threading_Thread_SetName_internal)
ICALL(THREAD_21, "SetState(System.Threading.InternalThread,System.Threading.ThreadState)", ves_icall_System_Threading_Thread_SetState)
ICALL(THREAD_22, "Sleep_internal", ves_icall_System_Threading_Thread_Sleep_internal)
ICALL(THREAD_54, "SpinWait_nop", ves_icall_System_Threading_Thread_SpinWait_nop)
ICALL(THREAD_23, "Suspend_internal(System.Threading.InternalThread)", ves_icall_System_Threading_Thread_Suspend)
ICALL(THREAD_25, "Thread_internal", ves_icall_System_Threading_Thread_Thread_internal)
ICALL(THREAD_26, "VolatileRead(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_27, "VolatileRead(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(THREAD_28, "VolatileRead(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_29, "VolatileRead(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_30, "VolatileRead(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_31, "VolatileRead(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_32, "VolatileRead(object&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_33, "VolatileRead(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(THREAD_34, "VolatileRead(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(THREAD_35, "VolatileRead(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(THREAD_36, "VolatileRead(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(THREAD_37, "VolatileRead(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(THREAD_38, "VolatileRead(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(THREAD_39, "VolatileWrite(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_40, "VolatileWrite(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(THREAD_41, "VolatileWrite(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_42, "VolatileWrite(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_43, "VolatileWrite(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_44, "VolatileWrite(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_45, "VolatileWrite(object&,object)", ves_icall_System_Threading_Thread_VolatileWriteObject)
ICALL(THREAD_46, "VolatileWrite(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(THREAD_47, "VolatileWrite(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(THREAD_48, "VolatileWrite(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(THREAD_49, "VolatileWrite(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(THREAD_50, "VolatileWrite(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(THREAD_51, "VolatileWrite(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(THREAD_9, "Yield", ves_icall_System_Threading_Thread_Yield)
ICALL(THREAD_52, "current_lcid()", ves_icall_System_Threading_Thread_current_lcid)

ICALL_TYPE(THREADP, "System.Threading.ThreadPool", THREADP_1)
ICALL(THREADP_1, "GetAvailableThreads", ves_icall_System_Threading_ThreadPool_GetAvailableThreads)
ICALL(THREADP_2, "GetMaxThreads", ves_icall_System_Threading_ThreadPool_GetMaxThreads)
ICALL(THREADP_3, "GetMinThreads", ves_icall_System_Threading_ThreadPool_GetMinThreads)
ICALL(THREADP_35, "SetMaxThreads", ves_icall_System_Threading_ThreadPool_SetMaxThreads)
ICALL(THREADP_4, "SetMinThreads", ves_icall_System_Threading_ThreadPool_SetMinThreads)
ICALL(THREADP_5, "pool_queue", icall_append_job)

ICALL_TYPE(VOLATILE, "System.Threading.Volatile", VOLATILE_28)
ICALL(VOLATILE_28, "Read(T&)", ves_icall_System_Threading_Volatile_Read_T)
ICALL(VOLATILE_1, "Read(bool&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_2, "Read(byte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_3, "Read(double&)", ves_icall_System_Threading_Thread_VolatileReadDouble)
ICALL(VOLATILE_4, "Read(int&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_5, "Read(int16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_6, "Read(intptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_7, "Read(long&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_8, "Read(sbyte&)", ves_icall_System_Threading_Thread_VolatileRead1)
ICALL(VOLATILE_9, "Read(single&)", ves_icall_System_Threading_Thread_VolatileReadFloat)
ICALL(VOLATILE_10, "Read(uint&)", ves_icall_System_Threading_Thread_VolatileRead4)
ICALL(VOLATILE_11, "Read(uint16&)", ves_icall_System_Threading_Thread_VolatileRead2)
ICALL(VOLATILE_12, "Read(uintptr&)", ves_icall_System_Threading_Thread_VolatileReadIntPtr)
ICALL(VOLATILE_13, "Read(ulong&)", ves_icall_System_Threading_Thread_VolatileRead8)
ICALL(VOLATILE_27, "Write(T&,T)", ves_icall_System_Threading_Volatile_Write_T)
ICALL(VOLATILE_14, "Write(bool&,bool)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_15, "Write(byte&,byte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_16, "Write(double&,double)", ves_icall_System_Threading_Thread_VolatileWriteDouble)
ICALL(VOLATILE_17, "Write(int&,int)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_18, "Write(int16&,int16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_19, "Write(intptr&,intptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_20, "Write(long&,long)", ves_icall_System_Threading_Thread_VolatileWrite8)
ICALL(VOLATILE_21, "Write(sbyte&,sbyte)", ves_icall_System_Threading_Thread_VolatileWrite1)
ICALL(VOLATILE_22, "Write(single&,single)", ves_icall_System_Threading_Thread_VolatileWriteFloat)
ICALL(VOLATILE_23, "Write(uint&,uint)", ves_icall_System_Threading_Thread_VolatileWrite4)
ICALL(VOLATILE_24, "Write(uint16&,uint16)", ves_icall_System_Threading_Thread_VolatileWrite2)
ICALL(VOLATILE_25, "Write(uintptr&,uintptr)", ves_icall_System_Threading_Thread_VolatileWriteIntPtr)
ICALL(VOLATILE_26, "Write(ulong&,ulong)", ves_icall_System_Threading_Thread_VolatileWrite8)

ICALL_TYPE(WAITH, "System.Threading.WaitHandle", WAITH_1)
ICALL(WAITH_1, "SignalAndWait_Internal", ves_icall_System_Threading_WaitHandle_SignalAndWait_Internal)
ICALL(WAITH_2, "WaitAll_internal", ves_icall_System_Threading_WaitHandle_WaitAll_internal)
ICALL(WAITH_3, "WaitAny_internal", ves_icall_System_Threading_WaitHandle_WaitAny_internal)
ICALL(WAITH_4, "WaitOne_internal", ves_icall_System_Threading_WaitHandle_WaitOne_internal)

ICALL_TYPE(TYPE, "System.Type", TYPE_1)
ICALL(TYPE_1, "EqualsInternal", ves_icall_System_Type_EqualsInternal)
ICALL(TYPE_2, "GetGenericParameterAttributes", ves_icall_Type_GetGenericParameterAttributes)
ICALL(TYPE_3, "GetGenericParameterConstraints_impl", ves_icall_Type_GetGenericParameterConstraints)
ICALL(TYPE_4, "GetGenericParameterPosition", ves_icall_Type_GetGenericParameterPosition)
ICALL(TYPE_5, "GetGenericTypeDefinition_impl", ves_icall_Type_GetGenericTypeDefinition_impl)
ICALL(TYPE_6, "GetInterfaceMapData", ves_icall_Type_GetInterfaceMapData)
ICALL(TYPE_7, "GetPacking", ves_icall_Type_GetPacking)
ICALL(TYPE_8, "GetTypeCode", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_9, "GetTypeCodeInternal", ves_icall_type_GetTypeCodeInternal)
ICALL(TYPE_10, "IsArrayImpl", ves_icall_Type_IsArrayImpl)
ICALL(TYPE_11, "IsInstanceOfType", ves_icall_type_IsInstanceOfType)
ICALL(TYPE_12, "MakeGenericType", ves_icall_Type_MakeGenericType)
ICALL(TYPE_13, "MakePointerType", ves_icall_Type_MakePointerType)
ICALL(TYPE_14, "get_IsGenericInstance", ves_icall_Type_get_IsGenericInstance)
ICALL(TYPE_15, "get_IsGenericType", ves_icall_Type_get_IsGenericType)
ICALL(TYPE_16, "get_IsGenericTypeDefinition", ves_icall_Type_get_IsGenericTypeDefinition)
ICALL(TYPE_17, "internal_from_handle", ves_icall_type_from_handle)
ICALL(TYPE_18, "internal_from_name", ves_icall_type_from_name)
ICALL(TYPE_19, "make_array_type", ves_icall_Type_make_array_type)
ICALL(TYPE_20, "make_byref_type", ves_icall_Type_make_byref_type)
ICALL(TYPE_21, "type_is_assignable_from", ves_icall_type_is_assignable_from)
ICALL(TYPE_22, "type_is_subtype_of", ves_icall_type_is_subtype_of)

ICALL_TYPE(TYPEDR, "System.TypedReference", TYPEDR_1)
ICALL(TYPEDR_1, "ToObject",	mono_TypedReference_ToObject)
ICALL(TYPEDR_2, "ToObjectInternal",	mono_TypedReference_ToObjectInternal)

ICALL_TYPE(VALUET, "System.ValueType", VALUET_1)
ICALL(VALUET_1, "InternalEquals", ves_icall_System_ValueType_Equals)
ICALL(VALUET_2, "InternalGetHashCode", ves_icall_System_ValueType_InternalGetHashCode)

ICALL_TYPE(WEBIC, "System.Web.Util.ICalls", WEBIC_1)
ICALL(WEBIC_1, "GetMachineConfigPath", ves_icall_System_Configuration_DefaultConfig_get_machine_config_path)
ICALL(WEBIC_2, "GetMachineInstallDirectory", ves_icall_System_Web_Util_ICalls_get_machine_install_dir)
ICALL(WEBIC_3, "GetUnmanagedResourcesPtr", ves_icall_get_resources_ptr)

#ifndef DISABLE_COM
ICALL_TYPE(COMOBJ, "System.__ComObject", COMOBJ_1)
ICALL(COMOBJ_1, "CreateRCW", ves_icall_System_ComObject_CreateRCW)
ICALL(COMOBJ_2, "GetInterfaceInternal", ves_icall_System_ComObject_GetInterfaceInternal)
ICALL(COMOBJ_3, "ReleaseInterfaces", ves_icall_System_ComObject_ReleaseInterfaces)
#endif
	NULL
};
#endif

#endif /* DISABLE_ICALL_TABLES */

static GHashTable *icall_hash = NULL;
static GHashTable *jit_icall_hash_name = NULL;
static GHashTable *jit_icall_hash_addr = NULL;

void
mono_icall_init (void)
{
#ifndef DISABLE_ICALL_TABLES
	int i = 0;

	/* check that tables are sorted: disable in release */
	if (TRUE) {
		int j;
		const char *prev_class = NULL;
		const char *prev_method;
		
		for (i = 0; i < Icall_type_num; ++i) {
			const IcallTypeDesc *desc;
			int num_icalls;
			prev_method = NULL;
			if (prev_class && strcmp (prev_class, icall_type_name_get (i)) >= 0)
				g_print ("class %s should come before class %s\n", icall_type_name_get (i), prev_class);
			prev_class = icall_type_name_get (i);
			desc = &icall_type_descs [i];
			num_icalls = icall_desc_num_icalls (desc);
			/*g_print ("class %s has %d icalls starting at %d\n", prev_class, num_icalls, desc->first_icall);*/
			for (j = 0; j < num_icalls; ++j) {
				const char *methodn = icall_name_get (desc->first_icall + j);
				if (prev_method && strcmp (prev_method, methodn) >= 0)
					g_print ("method %s should come before method %s\n", methodn, prev_method);
				prev_method = methodn;
			}
		}
	}
#endif

	icall_hash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
}

void
mono_icall_cleanup (void)
{
	g_hash_table_destroy (icall_hash);
	g_hash_table_destroy (jit_icall_hash_name);
	g_hash_table_destroy (jit_icall_hash_addr);
}

void
mono_add_internal_call (const char *name, gconstpointer method)
{
	mono_loader_lock ();

	g_hash_table_insert (icall_hash, g_strdup (name), (gpointer) method);

	mono_loader_unlock ();
}

#ifndef DISABLE_ICALL_TABLES

#ifdef HAVE_ARRAY_ELEM_INIT
static int
compare_method_imap (const void *key, const void *elem)
{
	const char* method_name = (const char*)&icall_names_str + (*(guint16*)elem);
	return strcmp (key, method_name);
}

static gpointer
find_method_icall (const IcallTypeDesc *imap, const char *name)
{
	const guint16 *nameslot = mono_binary_search (name, icall_names_idx + imap->first_icall, icall_desc_num_icalls (imap), sizeof (icall_names_idx [0]), compare_method_imap);
	if (!nameslot)
		return NULL;
	return (gpointer)icall_functions [(nameslot - &icall_names_idx [0])];
}

static int
compare_class_imap (const void *key, const void *elem)
{
	const char* class_name = (const char*)&icall_type_names_str + (*(guint16*)elem);
	return strcmp (key, class_name);
}

static const IcallTypeDesc*
find_class_icalls (const char *name)
{
	const guint16 *nameslot = mono_binary_search (name, icall_type_names_idx, Icall_type_num, sizeof (icall_type_names_idx [0]), compare_class_imap);
	if (!nameslot)
		return NULL;
	return &icall_type_descs [nameslot - &icall_type_names_idx [0]];
}

#else /* HAVE_ARRAY_ELEM_INIT */

static int
compare_method_imap (const void *key, const void *elem)
{
	const char** method_name = (const char**)elem;
	return strcmp (key, *method_name);
}

static gpointer
find_method_icall (const IcallTypeDesc *imap, const char *name)
{
	const char **nameslot = mono_binary_search (name, icall_names + imap->first_icall, icall_desc_num_icalls (imap), sizeof (icall_names [0]), compare_method_imap);
	if (!nameslot)
		return NULL;
	return (gpointer)icall_functions [(nameslot - icall_names)];
}

static int
compare_class_imap (const void *key, const void *elem)
{
	const char** class_name = (const char**)elem;
	return strcmp (key, *class_name);
}

static const IcallTypeDesc*
find_class_icalls (const char *name)
{
	const char **nameslot = mono_binary_search (name, icall_type_names, Icall_type_num, sizeof (icall_type_names [0]), compare_class_imap);
	if (!nameslot)
		return NULL;
	return &icall_type_descs [nameslot - icall_type_names];
}

#endif /* HAVE_ARRAY_ELEM_INIT */

#endif /* DISABLE_ICALL_TABLES */

/* 
 * we should probably export this as an helper (handle nested types).
 * Returns the number of chars written in buf.
 */
static int
concat_class_name (char *buf, int bufsize, MonoClass *klass)
{
	int nspacelen, cnamelen;
	nspacelen = strlen (klass->name_space);
	cnamelen = strlen (klass->name);
	if (nspacelen + cnamelen + 2 > bufsize)
		return 0;
	if (nspacelen) {
		memcpy (buf, klass->name_space, nspacelen);
		buf [nspacelen ++] = '.';
	}
	memcpy (buf + nspacelen, klass->name, cnamelen);
	buf [nspacelen + cnamelen] = 0;
	return nspacelen + cnamelen;
}

#ifdef DISABLE_ICALL_TABLES
static void
no_icall_table (void)
{
	g_assert_not_reached ();
}
#endif

gpointer
mono_lookup_internal_call (MonoMethod *method)
{
	char *sigstart;
	char *tmpsig;
	char mname [2048];
	int typelen = 0, mlen, siglen;
	gpointer res;
#ifndef DISABLE_ICALL_TABLES
	const IcallTypeDesc *imap = NULL;
#endif

	g_assert (method != NULL);

	if (method->is_inflated)
		method = ((MonoMethodInflated *) method)->declaring;

	if (method->klass->nested_in) {
		int pos = concat_class_name (mname, sizeof (mname)-2, method->klass->nested_in);
		if (!pos)
			return NULL;

		mname [pos++] = '/';
		mname [pos] = 0;

		typelen = concat_class_name (mname+pos, sizeof (mname)-pos-1, method->klass);
		if (!typelen)
			return NULL;

		typelen += pos;
	} else {
		typelen = concat_class_name (mname, sizeof (mname), method->klass);
		if (!typelen)
			return NULL;
	}

#ifndef DISABLE_ICALL_TABLES
	imap = find_class_icalls (mname);
#endif

	mname [typelen] = ':';
	mname [typelen + 1] = ':';

	mlen = strlen (method->name);
	memcpy (mname + typelen + 2, method->name, mlen);
	sigstart = mname + typelen + 2 + mlen;
	*sigstart = 0;

	tmpsig = mono_signature_get_desc (mono_method_signature (method), TRUE);
	siglen = strlen (tmpsig);
	if (typelen + mlen + siglen + 6 > sizeof (mname))
		return NULL;
	sigstart [0] = '(';
	memcpy (sigstart + 1, tmpsig, siglen);
	sigstart [siglen + 1] = ')';
	sigstart [siglen + 2] = 0;
	g_free (tmpsig);
	
	mono_loader_lock ();

	res = g_hash_table_lookup (icall_hash, mname);
	if (res) {
		mono_loader_unlock ();
		return res;
	}
	/* try without signature */
	*sigstart = 0;
	res = g_hash_table_lookup (icall_hash, mname);
	if (res) {
		mono_loader_unlock ();
		return res;
	}

#ifdef DISABLE_ICALL_TABLES
	mono_loader_unlock ();
	/* Fail only when the result is actually used */
	/* mono_marshal_get_native_wrapper () depends on this */
	if (method->klass == mono_defaults.string_class && !strcmp (method->name, ".ctor"))
		return ves_icall_System_String_ctor_RedirectToCreateString;
	else
		return no_icall_table;
#else
	/* it wasn't found in the static call tables */
	if (!imap) {
		mono_loader_unlock ();
		return NULL;
	}
	res = find_method_icall (imap, sigstart - mlen);
	if (res) {
		mono_loader_unlock ();
		return res;
	}
	/* try _with_ signature */
	*sigstart = '(';
	res = find_method_icall (imap, sigstart - mlen);
	if (res) {
		mono_loader_unlock ();
		return res;
	}

	g_warning ("cant resolve internal call to \"%s\" (tested without signature also)", mname);
	g_print ("\nYour mono runtime and class libraries are out of sync.\n");
	g_print ("The out of sync library is: %s\n", method->klass->image->name);
	g_print ("\nWhen you update one from svn you need to update, compile and install\nthe other too.\n");
	g_print ("Do not report this as a bug unless you're sure you have updated correctly:\nyou probably have a broken mono install.\n");
	g_print ("If you see other errors or faults after this message they are probably related\n");
	g_print ("and you need to fix your mono install first.\n");

	mono_loader_unlock ();

	return NULL;
#endif
}

#ifdef ENABLE_ICALL_SYMBOL_MAP
static int
func_cmp (gconstpointer key, gconstpointer p)
{
	return (gsize)key - (gsize)*(gsize*)p;
}
#endif

/*
 * mono_lookup_icall_symbol:
 *
 *   Given the icall METHOD, returns its C symbol.
 */
const char*
mono_lookup_icall_symbol (MonoMethod *m)
{
#ifdef DISABLE_ICALL_TABLES
	g_assert_not_reached ();
	return NULL;
#else
#ifdef ENABLE_ICALL_SYMBOL_MAP
	gpointer func;
	int i;
	gpointer slot;
	static gconstpointer *functions_sorted;
	static const char**symbols_sorted;
	static gboolean inited;

	if (!inited) {
		gboolean changed;

		functions_sorted = g_malloc (G_N_ELEMENTS (icall_functions) * sizeof (gpointer));
		memcpy (functions_sorted, icall_functions, G_N_ELEMENTS (icall_functions) * sizeof (gpointer));
		symbols_sorted = g_malloc (G_N_ELEMENTS (icall_functions) * sizeof (gpointer));
		memcpy (symbols_sorted, icall_symbols, G_N_ELEMENTS (icall_functions) * sizeof (gpointer));
		/* Bubble sort the two arrays */
		changed = TRUE;
		while (changed) {
			changed = FALSE;
			for (i = 0; i < G_N_ELEMENTS (icall_functions) - 1; ++i) {
				if (functions_sorted [i] > functions_sorted [i + 1]) {
					gconstpointer tmp;

					tmp = functions_sorted [i];
					functions_sorted [i] = functions_sorted [i + 1];
					functions_sorted [i + 1] = tmp;
					tmp = symbols_sorted [i];
					symbols_sorted [i] = symbols_sorted [i + 1];
					symbols_sorted [i + 1] = tmp;
					changed = TRUE;
				}
			}
		}
	}

	func = mono_lookup_internal_call (m);
	if (!func)
		return NULL;
	slot = mono_binary_search (func, functions_sorted, G_N_ELEMENTS (icall_functions), sizeof (gpointer), func_cmp);
	if (!slot)
		return NULL;
	g_assert (slot);
	return symbols_sorted [(gpointer*)slot - (gpointer*)functions_sorted];
#else
	fprintf (stderr, "icall symbol maps not enabled, pass --enable-icall-symbol-map to configure.\n");
	g_assert_not_reached ();
	return 0;
#endif
#endif
}

static MonoType*
type_from_typename (char *typename)
{
	MonoClass *klass = NULL;	/* assignment to shut GCC warning up */

	if (!strcmp (typename, "int"))
		klass = mono_defaults.int_class;
	else if (!strcmp (typename, "ptr"))
		klass = mono_defaults.int_class;
	else if (!strcmp (typename, "void"))
		klass = mono_defaults.void_class;
	else if (!strcmp (typename, "int32"))
		klass = mono_defaults.int32_class;
	else if (!strcmp (typename, "uint32"))
		klass = mono_defaults.uint32_class;
	else if (!strcmp (typename, "int8"))
		klass = mono_defaults.sbyte_class;
	else if (!strcmp (typename, "uint8"))
		klass = mono_defaults.byte_class;
	else if (!strcmp (typename, "int16"))
		klass = mono_defaults.int16_class;
	else if (!strcmp (typename, "uint16"))
		klass = mono_defaults.uint16_class;
	else if (!strcmp (typename, "long"))
		klass = mono_defaults.int64_class;
	else if (!strcmp (typename, "ulong"))
		klass = mono_defaults.uint64_class;
	else if (!strcmp (typename, "float"))
		klass = mono_defaults.single_class;
	else if (!strcmp (typename, "double"))
		klass = mono_defaults.double_class;
	else if (!strcmp (typename, "object"))
		klass = mono_defaults.object_class;
	else if (!strcmp (typename, "obj"))
		klass = mono_defaults.object_class;
	else if (!strcmp (typename, "string"))
		klass = mono_defaults.string_class;
	else if (!strcmp (typename, "bool"))
		klass = mono_defaults.boolean_class;
	else if (!strcmp (typename, "boolean"))
		klass = mono_defaults.boolean_class;
	else {
		g_error ("%s", typename);
		g_assert_not_reached ();
	}
	return &klass->byval_arg;
}

MonoMethodSignature*
mono_create_icall_signature (const char *sigstr)
{
	gchar **parts;
	int i, len;
	gchar **tmp;
	MonoMethodSignature *res;

	mono_loader_lock ();
	res = g_hash_table_lookup (mono_defaults.corlib->helper_signatures, sigstr);
	if (res) {
		mono_loader_unlock ();
		return res;
	}

	parts = g_strsplit (sigstr, " ", 256);

	tmp = parts;
	len = 0;
	while (*tmp) {
		len ++;
		tmp ++;
	}

	res = mono_metadata_signature_alloc (mono_defaults.corlib, len - 1);
	res->pinvoke = 1;

#ifdef HOST_WIN32
	/* 
	 * Under windows, the default pinvoke calling convention is STDCALL but
	 * we need CDECL.
	 */
	res->call_convention = MONO_CALL_C;
#endif

	res->ret = type_from_typename (parts [0]);
	for (i = 1; i < len; ++i) {
		res->params [i - 1] = type_from_typename (parts [i]);
	}

	g_strfreev (parts);

	g_hash_table_insert (mono_defaults.corlib->helper_signatures, (gpointer)sigstr, res);

	mono_loader_unlock ();

	return res;
}

MonoJitICallInfo *
mono_find_jit_icall_by_name (const char *name)
{
	MonoJitICallInfo *info;
	g_assert (jit_icall_hash_name);

	mono_loader_lock ();
	info = g_hash_table_lookup (jit_icall_hash_name, name);
	mono_loader_unlock ();
	return info;
}

MonoJitICallInfo *
mono_find_jit_icall_by_addr (gconstpointer addr)
{
	MonoJitICallInfo *info;
	g_assert (jit_icall_hash_addr);

	mono_loader_lock ();
	info = g_hash_table_lookup (jit_icall_hash_addr, (gpointer)addr);
	mono_loader_unlock ();

	return info;
}

/*
 * mono_get_jit_icall_info:
 *
 *   Return the hashtable mapping JIT icall names to MonoJitICallInfo structures. The
 * caller should access it while holding the loader lock.
 */
GHashTable*
mono_get_jit_icall_info (void)
{
	return jit_icall_hash_name;
}

/*
 * mono_lookup_jit_icall_symbol:
 *
 *   Given the jit icall NAME, returns its C symbol if possible, or NULL.
 */
const char*
mono_lookup_jit_icall_symbol (const char *name)
{
	MonoJitICallInfo *info;
	const char *res = NULL;

	mono_loader_lock ();
	info = g_hash_table_lookup (jit_icall_hash_name, name);
	if (info)
		res = info->c_symbol;
	mono_loader_unlock ();
	return res;
}

void
mono_register_jit_icall_wrapper (MonoJitICallInfo *info, gconstpointer wrapper)
{
	mono_loader_lock ();
	g_hash_table_insert (jit_icall_hash_addr, (gpointer)wrapper, info);
	mono_loader_unlock ();
}

MonoJitICallInfo *
mono_register_jit_icall_full (gconstpointer func, const char *name, MonoMethodSignature *sig, gboolean is_save, const char *c_symbol)
{
	MonoJitICallInfo *info;
	
	g_assert (func);
	g_assert (name);

	mono_loader_lock ();

	if (!jit_icall_hash_name) {
		jit_icall_hash_name = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, g_free);
		jit_icall_hash_addr = g_hash_table_new (NULL, NULL);
	}

	if (g_hash_table_lookup (jit_icall_hash_name, name)) {
		g_warning ("jit icall already defined \"%s\"\n", name);
		g_assert_not_reached ();
	}

	info = g_new0 (MonoJitICallInfo, 1);
	
	info->name = name;
	info->func = func;
	info->sig = sig;
	info->c_symbol = c_symbol;

	if (is_save) {
		info->wrapper = func;
	} else {
		info->wrapper = NULL;
	}

	g_hash_table_insert (jit_icall_hash_name, (gpointer)info->name, info);
	g_hash_table_insert (jit_icall_hash_addr, (gpointer)func, info);

	mono_loader_unlock ();
	return info;
}

MonoJitICallInfo *
mono_register_jit_icall (gconstpointer func, const char *name, MonoMethodSignature *sig, gboolean is_save)
{
	return mono_register_jit_icall_full (func, name, sig, is_save, NULL);
}

