/*
 * opcodes.c: CIL instruction information
 *
 * Author:
 *   Paolo Molaro (lupus@ximian.com)
 *
 * Copyright 2002-2003 Ximian, Inc (http://www.ximian.com)
 * Copyright 2004-2009 Novell, Inc (http://www.novell.com)
 */
#include <mono/metadata/opcodes.h>
#include <stddef.h> /* for NULL */
#include <config.h>

#define MONO_PREFIX1_OFFSET MONO_CEE_ARGLIST
#define MONO_CUSTOM_PREFIX_OFFSET MONO_CEE_MONO_ICALL

#define OPDEF(a,b,c,d,e,f,g,h,i,j) \
	{ Mono ## e, MONO_FLOW_ ## j, MONO_ ## a },

const MonoOpcode
mono_opcodes [MONO_CEE_LAST + 1] = {
// #include "mono/cil/opcode.def"
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_NOP },
{ MonoInlineNone, MONO_FLOW_ERROR, MONO_CEE_BREAK },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDARG_0 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDARG_1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDARG_2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDARG_3 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDLOC_0 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDLOC_1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDLOC_2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDLOC_3 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STLOC_0 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STLOC_1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STLOC_2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STLOC_3 },
{ MonoShortInlineVar, MONO_FLOW_NEXT, MONO_CEE_LDARG_S },
{ MonoShortInlineVar, MONO_FLOW_NEXT, MONO_CEE_LDARGA_S },
{ MonoShortInlineVar, MONO_FLOW_NEXT, MONO_CEE_STARG_S },
{ MonoShortInlineVar, MONO_FLOW_NEXT, MONO_CEE_LDLOC_S },
{ MonoShortInlineVar, MONO_FLOW_NEXT, MONO_CEE_LDLOCA_S },
{ MonoShortInlineVar, MONO_FLOW_NEXT, MONO_CEE_STLOC_S },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDNULL },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDC_I4_M1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDC_I4_0 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDC_I4_1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDC_I4_2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDC_I4_3 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDC_I4_4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDC_I4_5 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDC_I4_6 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDC_I4_7 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDC_I4_8 },
{ MonoShortInlineI, MONO_FLOW_NEXT, MONO_CEE_LDC_I4_S },
{ MonoInlineI, MONO_FLOW_NEXT, MONO_CEE_LDC_I4 },
{ MonoInlineI8, MONO_FLOW_NEXT, MONO_CEE_LDC_I8 },
{ MonoShortInlineR, MONO_FLOW_NEXT, MONO_CEE_LDC_R4 },
{ MonoInlineR, MONO_FLOW_NEXT, MONO_CEE_LDC_R8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED99 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_DUP },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_POP },
{ MonoInlineMethod, MONO_FLOW_CALL, MONO_CEE_JMP },
{ MonoInlineMethod, MONO_FLOW_CALL, MONO_CEE_CALL },
{ MonoInlineSig, MONO_FLOW_CALL, MONO_CEE_CALLI },
{ MonoInlineNone, MONO_FLOW_RETURN, MONO_CEE_RET },
{ MonoShortInlineBrTarget, MONO_FLOW_BRANCH, MONO_CEE_BR_S },
{ MonoShortInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BRFALSE_S },
{ MonoShortInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BRTRUE_S },
{ MonoShortInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BEQ_S },
{ MonoShortInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BGE_S },
{ MonoShortInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BGT_S },
{ MonoShortInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BLE_S },
{ MonoShortInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BLT_S },
{ MonoShortInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BNE_UN_S },
{ MonoShortInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BGE_UN_S },
{ MonoShortInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BGT_UN_S },
{ MonoShortInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BLE_UN_S },
{ MonoShortInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BLT_UN_S },
{ MonoInlineBrTarget, MONO_FLOW_BRANCH, MONO_CEE_BR },
{ MonoInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BRFALSE },
{ MonoInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BRTRUE },
{ MonoInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BEQ },
{ MonoInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BGE },
{ MonoInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BGT },
{ MonoInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BLE },
{ MonoInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BLT },
{ MonoInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BNE_UN },
{ MonoInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BGE_UN },
{ MonoInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BGT_UN },
{ MonoInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BLE_UN },
{ MonoInlineBrTarget, MONO_FLOW_COND_BRANCH, MONO_CEE_BLT_UN },
{ MonoInlineSwitch, MONO_FLOW_COND_BRANCH, MONO_CEE_SWITCH },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDIND_I1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDIND_U1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDIND_I2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDIND_U2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDIND_I4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDIND_U4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDIND_I8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDIND_I },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDIND_R4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDIND_R8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDIND_REF },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STIND_REF },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STIND_I1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STIND_I2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STIND_I4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STIND_I8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STIND_R4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STIND_R8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_ADD },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_SUB },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_MUL },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_DIV },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_DIV_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_REM },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_REM_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_AND },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_OR },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_XOR },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_SHL },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_SHR },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_SHR_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_NEG },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_NOT },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_I1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_I2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_I4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_I8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_R4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_R8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_U4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_U8 },
{ MonoInlineMethod, MONO_FLOW_CALL, MONO_CEE_CALLVIRT },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_CPOBJ },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_LDOBJ },
{ MonoInlineString, MONO_FLOW_NEXT, MONO_CEE_LDSTR },
{ MonoInlineMethod, MONO_FLOW_CALL, MONO_CEE_NEWOBJ },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_CASTCLASS },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_ISINST },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_R_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED58 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED1 },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_UNBOX },
{ MonoInlineNone, MONO_FLOW_ERROR, MONO_CEE_THROW },
{ MonoInlineField, MONO_FLOW_NEXT, MONO_CEE_LDFLD },
{ MonoInlineField, MONO_FLOW_NEXT, MONO_CEE_LDFLDA },
{ MonoInlineField, MONO_FLOW_NEXT, MONO_CEE_STFLD },
{ MonoInlineField, MONO_FLOW_NEXT, MONO_CEE_LDSFLD },
{ MonoInlineField, MONO_FLOW_NEXT, MONO_CEE_LDSFLDA },
{ MonoInlineField, MONO_FLOW_NEXT, MONO_CEE_STSFLD },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_STOBJ },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_I1_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_I2_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_I4_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_I8_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_U1_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_U2_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_U4_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_U8_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_I_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_U_UN },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_BOX },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_NEWARR },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDLEN },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_LDELEMA },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDELEM_I1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDELEM_U1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDELEM_I2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDELEM_U2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDELEM_I4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDELEM_U4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDELEM_I8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDELEM_I },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDELEM_R4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDELEM_R8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LDELEM_REF },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STELEM_I },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STELEM_I1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STELEM_I2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STELEM_I4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STELEM_I8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STELEM_R4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STELEM_R8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STELEM_REF },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_LDELEM },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_STELEM },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_UNBOX_ANY },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED5 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED6 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED7 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED9 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED10 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED11 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED12 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED13 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED14 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED15 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED16 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED17 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_I1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_U1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_I2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_U2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_I4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_U4 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_I8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_U8 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED50 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED18 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED19 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED20 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED21 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED22 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED23 },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_REFANYVAL },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CKFINITE },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED24 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED25 },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_MKREFANY },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED59 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED60 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED61 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED62 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED63 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED64 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED65 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED66 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED67 },
{ MonoInlineTok, MONO_FLOW_NEXT, MONO_CEE_LDTOKEN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_U2 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_U1 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_I },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_I },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_OVF_U },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_ADD_OVF },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_ADD_OVF_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_MUL_OVF },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_MUL_OVF_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_SUB_OVF },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_SUB_OVF_UN },
{ MonoInlineNone, MONO_FLOW_RETURN, MONO_CEE_ENDFINALLY },
{ MonoInlineBrTarget, MONO_FLOW_BRANCH, MONO_CEE_LEAVE },
{ MonoShortInlineBrTarget, MONO_FLOW_BRANCH, MONO_CEE_LEAVE_S },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_STIND_I },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CONV_U },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED26 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED27 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED28 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED29 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED30 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED31 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED32 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED33 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED34 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED35 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED36 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED37 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED38 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED39 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED40 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED41 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED42 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED43 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED44 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED45 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED46 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED47 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED48 },
{ MonoInlineNone, MONO_FLOW_META, MONO_CEE_PREFIX7 },
{ MonoInlineNone, MONO_FLOW_META, MONO_CEE_PREFIX6 },
{ MonoInlineNone, MONO_FLOW_META, MONO_CEE_PREFIX5 },
{ MonoInlineNone, MONO_FLOW_META, MONO_CEE_PREFIX4 },
{ MonoInlineNone, MONO_FLOW_META, MONO_CEE_PREFIX3 },
{ MonoInlineNone, MONO_FLOW_META, MONO_CEE_PREFIX2 },
{ MonoInlineNone, MONO_FLOW_META, MONO_CEE_PREFIX1 },
{ MonoInlineNone, MONO_FLOW_META, MONO_CEE_PREFIXREF },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_ARGLIST },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CEQ },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CGT },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CGT_UN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CLT },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CLT_UN },
{ MonoInlineMethod, MONO_FLOW_NEXT, MONO_CEE_LDFTN },
{ MonoInlineMethod, MONO_FLOW_NEXT, MONO_CEE_LDVIRTFTN },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED56 },
{ MonoInlineVar, MONO_FLOW_NEXT, MONO_CEE_LDARG },
{ MonoInlineVar, MONO_FLOW_NEXT, MONO_CEE_LDARGA },
{ MonoInlineVar, MONO_FLOW_NEXT, MONO_CEE_STARG },
{ MonoInlineVar, MONO_FLOW_NEXT, MONO_CEE_LDLOC },
{ MonoInlineVar, MONO_FLOW_NEXT, MONO_CEE_LDLOCA },
{ MonoInlineVar, MONO_FLOW_NEXT, MONO_CEE_STLOC },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_LOCALLOC },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED57 },
{ MonoInlineNone, MONO_FLOW_RETURN, MONO_CEE_ENDFILTER },
{ MonoShortInlineI, MONO_FLOW_META, MONO_CEE_UNALIGNED_ },
{ MonoInlineNone, MONO_FLOW_META, MONO_CEE_VOLATILE_ },
{ MonoInlineNone, MONO_FLOW_META, MONO_CEE_TAIL_ },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_INITOBJ },
{ MonoInlineType, MONO_FLOW_META, MONO_CEE_CONSTRAINED_ },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_CPBLK },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_INITBLK },
{ MonoShortInlineI, MONO_FLOW_NEXT, MONO_CEE_NO_ },
{ MonoInlineNone, MONO_FLOW_ERROR, MONO_CEE_RETHROW },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_SIZEOF },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_REFANYTYPE },
{ MonoInlineNone, MONO_FLOW_META, MONO_CEE_READONLY_ },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED53 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED54 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED55 },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_UNUSED70 },
{ MonoInlineNone, MONO_FLOW_META, MONO_CEE_ILLEGAL },
{ MonoInlineNone, MONO_FLOW_META, MONO_CEE_ENDMAC },
{ MonoShortInlineI, MONO_FLOW_NEXT, MONO_CEE_MONO_ICALL },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_MONO_OBJADDR },
{ MonoInlineI, MONO_FLOW_NEXT, MONO_CEE_MONO_LDPTR },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_MONO_VTADDR },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_MONO_NEWOBJ },
{ MonoInlineType, MONO_FLOW_RETURN, MONO_CEE_MONO_RETOBJ },
{ MonoInlineType, MONO_FLOW_RETURN, MONO_CEE_MONO_LDNATIVEOBJ },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_MONO_CISINST },
{ MonoInlineType, MONO_FLOW_NEXT, MONO_CEE_MONO_CCASTCLASS },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_MONO_SAVE_LMF },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_MONO_RESTORE_LMF },
{ MonoInlineI, MONO_FLOW_NEXT, MONO_CEE_MONO_CLASSCONST },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_MONO_NOT_TAKEN },
{ MonoInlineI, MONO_FLOW_NEXT, MONO_CEE_MONO_TLS },
{ MonoInlineI, MONO_FLOW_NEXT, MONO_CEE_MONO_ICALL_ADDR },
{ MonoInlineI, MONO_FLOW_NEXT, MONO_CEE_MONO_DYN_CALL },
{ MonoInlineI, MONO_FLOW_NEXT, MONO_CEE_MONO_MEMORY_BARRIER },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_MONO_JIT_ATTACH },
{ MonoInlineNone, MONO_FLOW_NEXT, MONO_CEE_MONO_JIT_DETACH },
{ MonoInlineI, MONO_FLOW_NEXT, MONO_CEE_MONO_JIT_ICALL_ADDR },

 {0}
};

#undef OPDEF

#ifdef HAVE_ARRAY_ELEM_INIT
#define MSGSTRFIELD(line) MSGSTRFIELD1(line)
#define MSGSTRFIELD1(line) str##line
static const struct msgstr_t {
#define OPDEF(a,b,c,d,e,f,g,h,i,j) char MSGSTRFIELD(__LINE__) [sizeof (b)];
// #include "mono/cil/opcode.def"
char str2 [sizeof ("nop")];
char str3 [sizeof ("break")];
char str4 [sizeof ("ldarg.0")];
char str5 [sizeof ("ldarg.1")];
char str6 [sizeof ("ldarg.2")];
char str7 [sizeof ("ldarg.3")];
char str8 [sizeof ("ldloc.0")];
char str9 [sizeof ("ldloc.1")];
char str10 [sizeof ("ldloc.2")];
char str11 [sizeof ("ldloc.3")];
char str12 [sizeof ("stloc.0")];
char str13 [sizeof ("stloc.1")];
char str14 [sizeof ("stloc.2")];
char str15 [sizeof ("stloc.3")];
char str16 [sizeof ("ldarg.s")];
char str17 [sizeof ("ldarga.s")];
char str18 [sizeof ("starg.s")];
char str19 [sizeof ("ldloc.s")];
char str20 [sizeof ("ldloca.s")];
char str21 [sizeof ("stloc.s")];
char str22 [sizeof ("ldnull")];
char str23 [sizeof ("ldc.i4.m1")];
char str24 [sizeof ("ldc.i4.0")];
char str25 [sizeof ("ldc.i4.1")];
char str26 [sizeof ("ldc.i4.2")];
char str27 [sizeof ("ldc.i4.3")];
char str28 [sizeof ("ldc.i4.4")];
char str29 [sizeof ("ldc.i4.5")];
char str30 [sizeof ("ldc.i4.6")];
char str31 [sizeof ("ldc.i4.7")];
char str32 [sizeof ("ldc.i4.8")];
char str33 [sizeof ("ldc.i4.s")];
char str34 [sizeof ("ldc.i4")];
char str35 [sizeof ("ldc.i8")];
char str36 [sizeof ("ldc.r4")];
char str37 [sizeof ("ldc.r8")];
char str38 [sizeof ("unused99")];
char str39 [sizeof ("dup")];
char str40 [sizeof ("pop")];
char str41 [sizeof ("jmp")];
char str42 [sizeof ("call")];
char str43 [sizeof ("calli")];
char str44 [sizeof ("ret")];
char str45 [sizeof ("br.s")];
char str46 [sizeof ("brfalse.s")];
char str47 [sizeof ("brtrue.s")];
char str48 [sizeof ("beq.s")];
char str49 [sizeof ("bge.s")];
char str50 [sizeof ("bgt.s")];
char str51 [sizeof ("ble.s")];
char str52 [sizeof ("blt.s")];
char str53 [sizeof ("bne.un.s")];
char str54 [sizeof ("bge.un.s")];
char str55 [sizeof ("bgt.un.s")];
char str56 [sizeof ("ble.un.s")];
char str57 [sizeof ("blt.un.s")];
char str58 [sizeof ("br")];
char str59 [sizeof ("brfalse")];
char str60 [sizeof ("brtrue")];
char str61 [sizeof ("beq")];
char str62 [sizeof ("bge")];
char str63 [sizeof ("bgt")];
char str64 [sizeof ("ble")];
char str65 [sizeof ("blt")];
char str66 [sizeof ("bne.un")];
char str67 [sizeof ("bge.un")];
char str68 [sizeof ("bgt.un")];
char str69 [sizeof ("ble.un")];
char str70 [sizeof ("blt.un")];
char str71 [sizeof ("switch")];
char str72 [sizeof ("ldind.i1")];
char str73 [sizeof ("ldind.u1")];
char str74 [sizeof ("ldind.i2")];
char str75 [sizeof ("ldind.u2")];
char str76 [sizeof ("ldind.i4")];
char str77 [sizeof ("ldind.u4")];
char str78 [sizeof ("ldind.i8")];
char str79 [sizeof ("ldind.i")];
char str80 [sizeof ("ldind.r4")];
char str81 [sizeof ("ldind.r8")];
char str82 [sizeof ("ldind.ref")];
char str83 [sizeof ("stind.ref")];
char str84 [sizeof ("stind.i1")];
char str85 [sizeof ("stind.i2")];
char str86 [sizeof ("stind.i4")];
char str87 [sizeof ("stind.i8")];
char str88 [sizeof ("stind.r4")];
char str89 [sizeof ("stind.r8")];
char str90 [sizeof ("add")];
char str91 [sizeof ("sub")];
char str92 [sizeof ("mul")];
char str93 [sizeof ("div")];
char str94 [sizeof ("div.un")];
char str95 [sizeof ("rem")];
char str96 [sizeof ("rem.un")];
char str97 [sizeof ("and")];
char str98 [sizeof ("or")];
char str99 [sizeof ("xor")];
char str100 [sizeof ("shl")];
char str101 [sizeof ("shr")];
char str102 [sizeof ("shr.un")];
char str103 [sizeof ("neg")];
char str104 [sizeof ("not")];
char str105 [sizeof ("conv.i1")];
char str106 [sizeof ("conv.i2")];
char str107 [sizeof ("conv.i4")];
char str108 [sizeof ("conv.i8")];
char str109 [sizeof ("conv.r4")];
char str110 [sizeof ("conv.r8")];
char str111 [sizeof ("conv.u4")];
char str112 [sizeof ("conv.u8")];
char str113 [sizeof ("callvirt")];
char str114 [sizeof ("cpobj")];
char str115 [sizeof ("ldobj")];
char str116 [sizeof ("ldstr")];
char str117 [sizeof ("newobj")];
char str118 [sizeof ("castclass")];
char str119 [sizeof ("isinst")];
char str120 [sizeof ("conv.r.un")];
char str121 [sizeof ("unused58")];
char str122 [sizeof ("unused1")];
char str123 [sizeof ("unbox")];
char str124 [sizeof ("throw")];
char str125 [sizeof ("ldfld")];
char str126 [sizeof ("ldflda")];
char str127 [sizeof ("stfld")];
char str128 [sizeof ("ldsfld")];
char str129 [sizeof ("ldsflda")];
char str130 [sizeof ("stsfld")];
char str131 [sizeof ("stobj")];
char str132 [sizeof ("conv.ovf.i1.un")];
char str133 [sizeof ("conv.ovf.i2.un")];
char str134 [sizeof ("conv.ovf.i4.un")];
char str135 [sizeof ("conv.ovf.i8.un")];
char str136 [sizeof ("conv.ovf.u1.un")];
char str137 [sizeof ("conv.ovf.u2.un")];
char str138 [sizeof ("conv.ovf.u4.un")];
char str139 [sizeof ("conv.ovf.u8.un")];
char str140 [sizeof ("conv.ovf.i.un")];
char str141 [sizeof ("conv.ovf.u.un")];
char str142 [sizeof ("box")];
char str143 [sizeof ("newarr")];
char str144 [sizeof ("ldlen")];
char str145 [sizeof ("ldelema")];
char str146 [sizeof ("ldelem.i1")];
char str147 [sizeof ("ldelem.u1")];
char str148 [sizeof ("ldelem.i2")];
char str149 [sizeof ("ldelem.u2")];
char str150 [sizeof ("ldelem.i4")];
char str151 [sizeof ("ldelem.u4")];
char str152 [sizeof ("ldelem.i8")];
char str153 [sizeof ("ldelem.i")];
char str154 [sizeof ("ldelem.r4")];
char str155 [sizeof ("ldelem.r8")];
char str156 [sizeof ("ldelem.ref")];
char str157 [sizeof ("stelem.i")];
char str158 [sizeof ("stelem.i1")];
char str159 [sizeof ("stelem.i2")];
char str160 [sizeof ("stelem.i4")];
char str161 [sizeof ("stelem.i8")];
char str162 [sizeof ("stelem.r4")];
char str163 [sizeof ("stelem.r8")];
char str164 [sizeof ("stelem.ref")];
char str165 [sizeof ("ldelem")];
char str166 [sizeof ("stelem")];
char str167 [sizeof ("unbox.any")];
char str168 [sizeof ("unused5")];
char str169 [sizeof ("unused6")];
char str170 [sizeof ("unused7")];
char str171 [sizeof ("unused8")];
char str172 [sizeof ("unused9")];
char str173 [sizeof ("unused10")];
char str174 [sizeof ("unused11")];
char str175 [sizeof ("unused12")];
char str176 [sizeof ("unused13")];
char str177 [sizeof ("unused14")];
char str178 [sizeof ("unused15")];
char str179 [sizeof ("unused16")];
char str180 [sizeof ("unused17")];
char str181 [sizeof ("conv.ovf.i1")];
char str182 [sizeof ("conv.ovf.u1")];
char str183 [sizeof ("conv.ovf.i2")];
char str184 [sizeof ("conv.ovf.u2")];
char str185 [sizeof ("conv.ovf.i4")];
char str186 [sizeof ("conv.ovf.u4")];
char str187 [sizeof ("conv.ovf.i8")];
char str188 [sizeof ("conv.ovf.u8")];
char str189 [sizeof ("unused50")];
char str190 [sizeof ("unused18")];
char str191 [sizeof ("unused19")];
char str192 [sizeof ("unused20")];
char str193 [sizeof ("unused21")];
char str194 [sizeof ("unused22")];
char str195 [sizeof ("unused23")];
char str196 [sizeof ("refanyval")];
char str197 [sizeof ("ckfinite")];
char str198 [sizeof ("unused24")];
char str199 [sizeof ("unused25")];
char str200 [sizeof ("mkrefany")];
char str201 [sizeof ("unused59")];
char str202 [sizeof ("unused60")];
char str203 [sizeof ("unused61")];
char str204 [sizeof ("unused62")];
char str205 [sizeof ("unused63")];
char str206 [sizeof ("unused64")];
char str207 [sizeof ("unused65")];
char str208 [sizeof ("unused66")];
char str209 [sizeof ("unused67")];
char str210 [sizeof ("ldtoken")];
char str211 [sizeof ("conv.u2")];
char str212 [sizeof ("conv.u1")];
char str213 [sizeof ("conv.i")];
char str214 [sizeof ("conv.ovf.i")];
char str215 [sizeof ("conv.ovf.u")];
char str216 [sizeof ("add.ovf")];
char str217 [sizeof ("add.ovf.un")];
char str218 [sizeof ("mul.ovf")];
char str219 [sizeof ("mul.ovf.un")];
char str220 [sizeof ("sub.ovf")];
char str221 [sizeof ("sub.ovf.un")];
char str222 [sizeof ("endfinally")];
char str223 [sizeof ("leave")];
char str224 [sizeof ("leave.s")];
char str225 [sizeof ("stind.i")];
char str226 [sizeof ("conv.u")];
char str227 [sizeof ("unused26")];
char str228 [sizeof ("unused27")];
char str229 [sizeof ("unused28")];
char str230 [sizeof ("unused29")];
char str231 [sizeof ("unused30")];
char str232 [sizeof ("unused31")];
char str233 [sizeof ("unused32")];
char str234 [sizeof ("unused33")];
char str235 [sizeof ("unused34")];
char str236 [sizeof ("unused35")];
char str237 [sizeof ("unused36")];
char str238 [sizeof ("unused37")];
char str239 [sizeof ("unused38")];
char str240 [sizeof ("unused39")];
char str241 [sizeof ("unused40")];
char str242 [sizeof ("unused41")];
char str243 [sizeof ("unused42")];
char str244 [sizeof ("unused43")];
char str245 [sizeof ("unused44")];
char str246 [sizeof ("unused45")];
char str247 [sizeof ("unused46")];
char str248 [sizeof ("unused47")];
char str249 [sizeof ("unused48")];
char str250 [sizeof ("prefix7")];
char str251 [sizeof ("prefix6")];
char str252 [sizeof ("prefix5")];
char str253 [sizeof ("prefix4")];
char str254 [sizeof ("prefix3")];
char str255 [sizeof ("prefix2")];
char str256 [sizeof ("prefix1")];
char str257 [sizeof ("prefixref")];
char str258 [sizeof ("arglist")];
char str259 [sizeof ("ceq")];
char str260 [sizeof ("cgt")];
char str261 [sizeof ("cgt.un")];
char str262 [sizeof ("clt")];
char str263 [sizeof ("clt.un")];
char str264 [sizeof ("ldftn")];
char str265 [sizeof ("ldvirtftn")];
char str266 [sizeof ("unused56")];
char str267 [sizeof ("ldarg")];
char str268 [sizeof ("ldarga")];
char str269 [sizeof ("starg")];
char str270 [sizeof ("ldloc")];
char str271 [sizeof ("ldloca")];
char str272 [sizeof ("stloc")];
char str273 [sizeof ("localloc")];
char str274 [sizeof ("unused57")];
char str275 [sizeof ("endfilter")];
char str276 [sizeof ("unaligned.")];
char str277 [sizeof ("volatile.")];
char str278 [sizeof ("tail.")];
char str279 [sizeof ("initobj")];
char str280 [sizeof ("constrained.")];
char str281 [sizeof ("cpblk")];
char str282 [sizeof ("initblk")];
char str283 [sizeof ("no.")];
char str284 [sizeof ("rethrow")];
char str285 [sizeof ("unused")];
char str286 [sizeof ("sizeof")];
char str287 [sizeof ("refanytype")];
char str288 [sizeof ("readonly.")];
char str289 [sizeof ("unused53")];
char str290 [sizeof ("unused54")];
char str291 [sizeof ("unused55")];
char str292 [sizeof ("unused70")];
char str293 [sizeof ("illegal")];
char str294 [sizeof ("endmac")];
char str295 [sizeof ("mono_icall")];
char str296 [sizeof ("mono_objaddr")];
char str297 [sizeof ("mono_ldptr")];
char str298 [sizeof ("mono_vtaddr")];
char str299 [sizeof ("mono_newobj")];
char str300 [sizeof ("mono_retobj")];
char str301 [sizeof ("mono_ldnativeobj")];
char str302 [sizeof ("mono_cisinst")];
char str303 [sizeof ("mono_ccastclass")];
char str304 [sizeof ("mono_save_lmf")];
char str305 [sizeof ("mono_restore_lmf")];
char str306 [sizeof ("mono_classconst")];
char str307 [sizeof ("mono_not_taken")];
char str308 [sizeof ("mono_tls")];
char str309 [sizeof ("mono_icall_addr")];
char str310 [sizeof ("mono_dyn_call")];
char str311 [sizeof ("mono_memory_barrier")];
char str312 [sizeof ("mono_jit_attach")];
char str313 [sizeof ("mono_jit_detach")];
char str314 [sizeof ("mono_jit_icall_addr")];

#undef OPDEF
} opstr = {
#define OPDEF(a,b,c,d,e,f,g,h,i,j) b,
// #include "mono/cil/opcode.def"
"nop",
"break",
"ldarg.0",
"ldarg.1",
"ldarg.2",
"ldarg.3",
"ldloc.0",
"ldloc.1",
"ldloc.2",
"ldloc.3",
"stloc.0",
"stloc.1",
"stloc.2",
"stloc.3",
"ldarg.s",
"ldarga.s",
"starg.s",
"ldloc.s",
"ldloca.s",
"stloc.s",
"ldnull",
"ldc.i4.m1",
"ldc.i4.0",
"ldc.i4.1",
"ldc.i4.2",
"ldc.i4.3",
"ldc.i4.4",
"ldc.i4.5",
"ldc.i4.6",
"ldc.i4.7",
"ldc.i4.8",
"ldc.i4.s",
"ldc.i4",
"ldc.i8",
"ldc.r4",
"ldc.r8",
"unused99",
"dup",
"pop",
"jmp",
"call",
"calli",
"ret",
"br.s",
"brfalse.s",
"brtrue.s",
"beq.s",
"bge.s",
"bgt.s",
"ble.s",
"blt.s",
"bne.un.s",
"bge.un.s",
"bgt.un.s",
"ble.un.s",
"blt.un.s",
"br",
"brfalse",
"brtrue",
"beq",
"bge",
"bgt",
"ble",
"blt",
"bne.un",
"bge.un",
"bgt.un",
"ble.un",
"blt.un",
"switch",
"ldind.i1",
"ldind.u1",
"ldind.i2",
"ldind.u2",
"ldind.i4",
"ldind.u4",
"ldind.i8",
"ldind.i",
"ldind.r4",
"ldind.r8",
"ldind.ref",
"stind.ref",
"stind.i1",
"stind.i2",
"stind.i4",
"stind.i8",
"stind.r4",
"stind.r8",
"add",
"sub",
"mul",
"div",
"div.un",
"rem",
"rem.un",
"and",
"or",
"xor",
"shl",
"shr",
"shr.un",
"neg",
"not",
"conv.i1",
"conv.i2",
"conv.i4",
"conv.i8",
"conv.r4",
"conv.r8",
"conv.u4",
"conv.u8",
"callvirt",
"cpobj",
"ldobj",
"ldstr",
"newobj",
"castclass",
"isinst",
"conv.r.un",
"unused58",
"unused1",
"unbox",
"throw",
"ldfld",
"ldflda",
"stfld",
"ldsfld",
"ldsflda",
"stsfld",
"stobj",
"conv.ovf.i1.un",
"conv.ovf.i2.un",
"conv.ovf.i4.un",
"conv.ovf.i8.un",
"conv.ovf.u1.un",
"conv.ovf.u2.un",
"conv.ovf.u4.un",
"conv.ovf.u8.un",
"conv.ovf.i.un",
"conv.ovf.u.un",
"box",
"newarr",
"ldlen",
"ldelema",
"ldelem.i1",
"ldelem.u1",
"ldelem.i2",
"ldelem.u2",
"ldelem.i4",
"ldelem.u4",
"ldelem.i8",
"ldelem.i",
"ldelem.r4",
"ldelem.r8",
"ldelem.ref",
"stelem.i",
"stelem.i1",
"stelem.i2",
"stelem.i4",
"stelem.i8",
"stelem.r4",
"stelem.r8",
"stelem.ref",
"ldelem",
"stelem",
"unbox.any",
"unused5",
"unused6",
"unused7",
"unused8",
"unused9",
"unused10",
"unused11",
"unused12",
"unused13",
"unused14",
"unused15",
"unused16",
"unused17",
"conv.ovf.i1",
"conv.ovf.u1",
"conv.ovf.i2",
"conv.ovf.u2",
"conv.ovf.i4",
"conv.ovf.u4",
"conv.ovf.i8",
"conv.ovf.u8",
"unused50",
"unused18",
"unused19",
"unused20",
"unused21",
"unused22",
"unused23",
"refanyval",
"ckfinite",
"unused24",
"unused25",
"mkrefany",
"unused59",
"unused60",
"unused61",
"unused62",
"unused63",
"unused64",
"unused65",
"unused66",
"unused67",
"ldtoken",
"conv.u2",
"conv.u1",
"conv.i",
"conv.ovf.i",
"conv.ovf.u",
"add.ovf",
"add.ovf.un",
"mul.ovf",
"mul.ovf.un",
"sub.ovf",
"sub.ovf.un",
"endfinally",
"leave",
"leave.s",
"stind.i",
"conv.u",
"unused26",
"unused27",
"unused28",
"unused29",
"unused30",
"unused31",
"unused32",
"unused33",
"unused34",
"unused35",
"unused36",
"unused37",
"unused38",
"unused39",
"unused40",
"unused41",
"unused42",
"unused43",
"unused44",
"unused45",
"unused46",
"unused47",
"unused48",
"prefix7",
"prefix6",
"prefix5",
"prefix4",
"prefix3",
"prefix2",
"prefix1",
"prefixref",
"arglist",
"ceq",
"cgt",
"cgt.un",
"clt",
"clt.un",
"ldftn",
"ldvirtftn",
"unused56",
"ldarg",
"ldarga",
"starg",
"ldloc",
"ldloca",
"stloc",
"localloc",
"unused57",
"endfilter",
"unaligned.",
"volatile.",
"tail.",
"initobj",
"constrained.",
"cpblk",
"initblk",
"no.",
"rethrow",
"unused",
"sizeof",
"refanytype",
"readonly.",
"unused53",
"unused54",
"unused55",
"unused70",
"illegal",
"endmac",
"mono_icall",
"mono_objaddr",
"mono_ldptr",
"mono_vtaddr",
"mono_newobj",
"mono_retobj",
"mono_ldnativeobj",
"mono_cisinst",
"mono_ccastclass",
"mono_save_lmf",
"mono_restore_lmf",
"mono_classconst",
"mono_not_taken",
"mono_tls",
"mono_icall_addr",
"mono_dyn_call",
"mono_memory_barrier",
"mono_jit_attach",
"mono_jit_detach",
"mono_jit_icall_addr",
#undef OPDEF
};
static const int16_t opidx [] = {
#define OPDEF(a,b,c,d,e,f,g,h,i,j) [MONO_ ## a] = offsetof (struct msgstr_t, MSGSTRFIELD(__LINE__)),
// #include "mono/cil/opcode.def"
[MONO_CEE_NOP] = __builtin_offsetof (struct msgstr_t, str2),
[MONO_CEE_BREAK] = __builtin_offsetof (struct msgstr_t, str3),
[MONO_CEE_LDARG_0] = __builtin_offsetof (struct msgstr_t, str4),
[MONO_CEE_LDARG_1] = __builtin_offsetof (struct msgstr_t, str5),
[MONO_CEE_LDARG_2] = __builtin_offsetof (struct msgstr_t, str6),
[MONO_CEE_LDARG_3] = __builtin_offsetof (struct msgstr_t, str7),
[MONO_CEE_LDLOC_0] = __builtin_offsetof (struct msgstr_t, str8),
[MONO_CEE_LDLOC_1] = __builtin_offsetof (struct msgstr_t, str9),
[MONO_CEE_LDLOC_2] = __builtin_offsetof (struct msgstr_t, str10),
[MONO_CEE_LDLOC_3] = __builtin_offsetof (struct msgstr_t, str11),
[MONO_CEE_STLOC_0] = __builtin_offsetof (struct msgstr_t, str12),
[MONO_CEE_STLOC_1] = __builtin_offsetof (struct msgstr_t, str13),
[MONO_CEE_STLOC_2] = __builtin_offsetof (struct msgstr_t, str14),
[MONO_CEE_STLOC_3] = __builtin_offsetof (struct msgstr_t, str15),
[MONO_CEE_LDARG_S] = __builtin_offsetof (struct msgstr_t, str16),
[MONO_CEE_LDARGA_S] = __builtin_offsetof (struct msgstr_t, str17),
[MONO_CEE_STARG_S] = __builtin_offsetof (struct msgstr_t, str18),
[MONO_CEE_LDLOC_S] = __builtin_offsetof (struct msgstr_t, str19),
[MONO_CEE_LDLOCA_S] = __builtin_offsetof (struct msgstr_t, str20),
[MONO_CEE_STLOC_S] = __builtin_offsetof (struct msgstr_t, str21),
[MONO_CEE_LDNULL] = __builtin_offsetof (struct msgstr_t, str22),
[MONO_CEE_LDC_I4_M1] = __builtin_offsetof (struct msgstr_t, str23),
[MONO_CEE_LDC_I4_0] = __builtin_offsetof (struct msgstr_t, str24),
[MONO_CEE_LDC_I4_1] = __builtin_offsetof (struct msgstr_t, str25),
[MONO_CEE_LDC_I4_2] = __builtin_offsetof (struct msgstr_t, str26),
[MONO_CEE_LDC_I4_3] = __builtin_offsetof (struct msgstr_t, str27),
[MONO_CEE_LDC_I4_4] = __builtin_offsetof (struct msgstr_t, str28),
[MONO_CEE_LDC_I4_5] = __builtin_offsetof (struct msgstr_t, str29),
[MONO_CEE_LDC_I4_6] = __builtin_offsetof (struct msgstr_t, str30),
[MONO_CEE_LDC_I4_7] = __builtin_offsetof (struct msgstr_t, str31),
[MONO_CEE_LDC_I4_8] = __builtin_offsetof (struct msgstr_t, str32),
[MONO_CEE_LDC_I4_S] = __builtin_offsetof (struct msgstr_t, str33),
[MONO_CEE_LDC_I4] = __builtin_offsetof (struct msgstr_t, str34),
[MONO_CEE_LDC_I8] = __builtin_offsetof (struct msgstr_t, str35),
[MONO_CEE_LDC_R4] = __builtin_offsetof (struct msgstr_t, str36),
[MONO_CEE_LDC_R8] = __builtin_offsetof (struct msgstr_t, str37),
[MONO_CEE_UNUSED99] = __builtin_offsetof (struct msgstr_t, str38),
[MONO_CEE_DUP] = __builtin_offsetof (struct msgstr_t, str39),
[MONO_CEE_POP] = __builtin_offsetof (struct msgstr_t, str40),
[MONO_CEE_JMP] = __builtin_offsetof (struct msgstr_t, str41),
[MONO_CEE_CALL] = __builtin_offsetof (struct msgstr_t, str42),
[MONO_CEE_CALLI] = __builtin_offsetof (struct msgstr_t, str43),
[MONO_CEE_RET] = __builtin_offsetof (struct msgstr_t, str44),
[MONO_CEE_BR_S] = __builtin_offsetof (struct msgstr_t, str45),
[MONO_CEE_BRFALSE_S] = __builtin_offsetof (struct msgstr_t, str46),
[MONO_CEE_BRTRUE_S] = __builtin_offsetof (struct msgstr_t, str47),
[MONO_CEE_BEQ_S] = __builtin_offsetof (struct msgstr_t, str48),
[MONO_CEE_BGE_S] = __builtin_offsetof (struct msgstr_t, str49),
[MONO_CEE_BGT_S] = __builtin_offsetof (struct msgstr_t, str50),
[MONO_CEE_BLE_S] = __builtin_offsetof (struct msgstr_t, str51),
[MONO_CEE_BLT_S] = __builtin_offsetof (struct msgstr_t, str52),
[MONO_CEE_BNE_UN_S] = __builtin_offsetof (struct msgstr_t, str53),
[MONO_CEE_BGE_UN_S] = __builtin_offsetof (struct msgstr_t, str54),
[MONO_CEE_BGT_UN_S] = __builtin_offsetof (struct msgstr_t, str55),
[MONO_CEE_BLE_UN_S] = __builtin_offsetof (struct msgstr_t, str56),
[MONO_CEE_BLT_UN_S] = __builtin_offsetof (struct msgstr_t, str57),
[MONO_CEE_BR] = __builtin_offsetof (struct msgstr_t, str58),
[MONO_CEE_BRFALSE] = __builtin_offsetof (struct msgstr_t, str59),
[MONO_CEE_BRTRUE] = __builtin_offsetof (struct msgstr_t, str60),
[MONO_CEE_BEQ] = __builtin_offsetof (struct msgstr_t, str61),
[MONO_CEE_BGE] = __builtin_offsetof (struct msgstr_t, str62),
[MONO_CEE_BGT] = __builtin_offsetof (struct msgstr_t, str63),
[MONO_CEE_BLE] = __builtin_offsetof (struct msgstr_t, str64),
[MONO_CEE_BLT] = __builtin_offsetof (struct msgstr_t, str65),
[MONO_CEE_BNE_UN] = __builtin_offsetof (struct msgstr_t, str66),
[MONO_CEE_BGE_UN] = __builtin_offsetof (struct msgstr_t, str67),
[MONO_CEE_BGT_UN] = __builtin_offsetof (struct msgstr_t, str68),
[MONO_CEE_BLE_UN] = __builtin_offsetof (struct msgstr_t, str69),
[MONO_CEE_BLT_UN] = __builtin_offsetof (struct msgstr_t, str70),
[MONO_CEE_SWITCH] = __builtin_offsetof (struct msgstr_t, str71),
[MONO_CEE_LDIND_I1] = __builtin_offsetof (struct msgstr_t, str72),
[MONO_CEE_LDIND_U1] = __builtin_offsetof (struct msgstr_t, str73),
[MONO_CEE_LDIND_I2] = __builtin_offsetof (struct msgstr_t, str74),
[MONO_CEE_LDIND_U2] = __builtin_offsetof (struct msgstr_t, str75),
[MONO_CEE_LDIND_I4] = __builtin_offsetof (struct msgstr_t, str76),
[MONO_CEE_LDIND_U4] = __builtin_offsetof (struct msgstr_t, str77),
[MONO_CEE_LDIND_I8] = __builtin_offsetof (struct msgstr_t, str78),
[MONO_CEE_LDIND_I] = __builtin_offsetof (struct msgstr_t, str79),
[MONO_CEE_LDIND_R4] = __builtin_offsetof (struct msgstr_t, str80),
[MONO_CEE_LDIND_R8] = __builtin_offsetof (struct msgstr_t, str81),
[MONO_CEE_LDIND_REF] = __builtin_offsetof (struct msgstr_t, str82),
[MONO_CEE_STIND_REF] = __builtin_offsetof (struct msgstr_t, str83),
[MONO_CEE_STIND_I1] = __builtin_offsetof (struct msgstr_t, str84),
[MONO_CEE_STIND_I2] = __builtin_offsetof (struct msgstr_t, str85),
[MONO_CEE_STIND_I4] = __builtin_offsetof (struct msgstr_t, str86),
[MONO_CEE_STIND_I8] = __builtin_offsetof (struct msgstr_t, str87),
[MONO_CEE_STIND_R4] = __builtin_offsetof (struct msgstr_t, str88),
[MONO_CEE_STIND_R8] = __builtin_offsetof (struct msgstr_t, str89),
[MONO_CEE_ADD] = __builtin_offsetof (struct msgstr_t, str90),
[MONO_CEE_SUB] = __builtin_offsetof (struct msgstr_t, str91),
[MONO_CEE_MUL] = __builtin_offsetof (struct msgstr_t, str92),
[MONO_CEE_DIV] = __builtin_offsetof (struct msgstr_t, str93),
[MONO_CEE_DIV_UN] = __builtin_offsetof (struct msgstr_t, str94),
[MONO_CEE_REM] = __builtin_offsetof (struct msgstr_t, str95),
[MONO_CEE_REM_UN] = __builtin_offsetof (struct msgstr_t, str96),
[MONO_CEE_AND] = __builtin_offsetof (struct msgstr_t, str97),
[MONO_CEE_OR] = __builtin_offsetof (struct msgstr_t, str98),
[MONO_CEE_XOR] = __builtin_offsetof (struct msgstr_t, str99),
[MONO_CEE_SHL] = __builtin_offsetof (struct msgstr_t, str100),
[MONO_CEE_SHR] = __builtin_offsetof (struct msgstr_t, str101),
[MONO_CEE_SHR_UN] = __builtin_offsetof (struct msgstr_t, str102),
[MONO_CEE_NEG] = __builtin_offsetof (struct msgstr_t, str103),
[MONO_CEE_NOT] = __builtin_offsetof (struct msgstr_t, str104),
[MONO_CEE_CONV_I1] = __builtin_offsetof (struct msgstr_t, str105),
[MONO_CEE_CONV_I2] = __builtin_offsetof (struct msgstr_t, str106),
[MONO_CEE_CONV_I4] = __builtin_offsetof (struct msgstr_t, str107),
[MONO_CEE_CONV_I8] = __builtin_offsetof (struct msgstr_t, str108),
[MONO_CEE_CONV_R4] = __builtin_offsetof (struct msgstr_t, str109),
[MONO_CEE_CONV_R8] = __builtin_offsetof (struct msgstr_t, str110),
[MONO_CEE_CONV_U4] = __builtin_offsetof (struct msgstr_t, str111),
[MONO_CEE_CONV_U8] = __builtin_offsetof (struct msgstr_t, str112),
[MONO_CEE_CALLVIRT] = __builtin_offsetof (struct msgstr_t, str113),
[MONO_CEE_CPOBJ] = __builtin_offsetof (struct msgstr_t, str114),
[MONO_CEE_LDOBJ] = __builtin_offsetof (struct msgstr_t, str115),
[MONO_CEE_LDSTR] = __builtin_offsetof (struct msgstr_t, str116),
[MONO_CEE_NEWOBJ] = __builtin_offsetof (struct msgstr_t, str117),
[MONO_CEE_CASTCLASS] = __builtin_offsetof (struct msgstr_t, str118),
[MONO_CEE_ISINST] = __builtin_offsetof (struct msgstr_t, str119),
[MONO_CEE_CONV_R_UN] = __builtin_offsetof (struct msgstr_t, str120),
[MONO_CEE_UNUSED58] = __builtin_offsetof (struct msgstr_t, str121),
[MONO_CEE_UNUSED1] = __builtin_offsetof (struct msgstr_t, str122),
[MONO_CEE_UNBOX] = __builtin_offsetof (struct msgstr_t, str123),
[MONO_CEE_THROW] = __builtin_offsetof (struct msgstr_t, str124),
[MONO_CEE_LDFLD] = __builtin_offsetof (struct msgstr_t, str125),
[MONO_CEE_LDFLDA] = __builtin_offsetof (struct msgstr_t, str126),
[MONO_CEE_STFLD] = __builtin_offsetof (struct msgstr_t, str127),
[MONO_CEE_LDSFLD] = __builtin_offsetof (struct msgstr_t, str128),
[MONO_CEE_LDSFLDA] = __builtin_offsetof (struct msgstr_t, str129),
[MONO_CEE_STSFLD] = __builtin_offsetof (struct msgstr_t, str130),
[MONO_CEE_STOBJ] = __builtin_offsetof (struct msgstr_t, str131),
[MONO_CEE_CONV_OVF_I1_UN] = __builtin_offsetof (struct msgstr_t, str132),
[MONO_CEE_CONV_OVF_I2_UN] = __builtin_offsetof (struct msgstr_t, str133),
[MONO_CEE_CONV_OVF_I4_UN] = __builtin_offsetof (struct msgstr_t, str134),
[MONO_CEE_CONV_OVF_I8_UN] = __builtin_offsetof (struct msgstr_t, str135),
[MONO_CEE_CONV_OVF_U1_UN] = __builtin_offsetof (struct msgstr_t, str136),
[MONO_CEE_CONV_OVF_U2_UN] = __builtin_offsetof (struct msgstr_t, str137),
[MONO_CEE_CONV_OVF_U4_UN] = __builtin_offsetof (struct msgstr_t, str138),
[MONO_CEE_CONV_OVF_U8_UN] = __builtin_offsetof (struct msgstr_t, str139),
[MONO_CEE_CONV_OVF_I_UN] = __builtin_offsetof (struct msgstr_t, str140),
[MONO_CEE_CONV_OVF_U_UN] = __builtin_offsetof (struct msgstr_t, str141),
[MONO_CEE_BOX] = __builtin_offsetof (struct msgstr_t, str142),
[MONO_CEE_NEWARR] = __builtin_offsetof (struct msgstr_t, str143),
[MONO_CEE_LDLEN] = __builtin_offsetof (struct msgstr_t, str144),
[MONO_CEE_LDELEMA] = __builtin_offsetof (struct msgstr_t, str145),
[MONO_CEE_LDELEM_I1] = __builtin_offsetof (struct msgstr_t, str146),
[MONO_CEE_LDELEM_U1] = __builtin_offsetof (struct msgstr_t, str147),
[MONO_CEE_LDELEM_I2] = __builtin_offsetof (struct msgstr_t, str148),
[MONO_CEE_LDELEM_U2] = __builtin_offsetof (struct msgstr_t, str149),
[MONO_CEE_LDELEM_I4] = __builtin_offsetof (struct msgstr_t, str150),
[MONO_CEE_LDELEM_U4] = __builtin_offsetof (struct msgstr_t, str151),
[MONO_CEE_LDELEM_I8] = __builtin_offsetof (struct msgstr_t, str152),
[MONO_CEE_LDELEM_I] = __builtin_offsetof (struct msgstr_t, str153),
[MONO_CEE_LDELEM_R4] = __builtin_offsetof (struct msgstr_t, str154),
[MONO_CEE_LDELEM_R8] = __builtin_offsetof (struct msgstr_t, str155),
[MONO_CEE_LDELEM_REF] = __builtin_offsetof (struct msgstr_t, str156),
[MONO_CEE_STELEM_I] = __builtin_offsetof (struct msgstr_t, str157),
[MONO_CEE_STELEM_I1] = __builtin_offsetof (struct msgstr_t, str158),
[MONO_CEE_STELEM_I2] = __builtin_offsetof (struct msgstr_t, str159),
[MONO_CEE_STELEM_I4] = __builtin_offsetof (struct msgstr_t, str160),
[MONO_CEE_STELEM_I8] = __builtin_offsetof (struct msgstr_t, str161),
[MONO_CEE_STELEM_R4] = __builtin_offsetof (struct msgstr_t, str162),
[MONO_CEE_STELEM_R8] = __builtin_offsetof (struct msgstr_t, str163),
[MONO_CEE_STELEM_REF] = __builtin_offsetof (struct msgstr_t, str164),
[MONO_CEE_LDELEM] = __builtin_offsetof (struct msgstr_t, str165),
[MONO_CEE_STELEM] = __builtin_offsetof (struct msgstr_t, str166),
[MONO_CEE_UNBOX_ANY] = __builtin_offsetof (struct msgstr_t, str167),
[MONO_CEE_UNUSED5] = __builtin_offsetof (struct msgstr_t, str168),
[MONO_CEE_UNUSED6] = __builtin_offsetof (struct msgstr_t, str169),
[MONO_CEE_UNUSED7] = __builtin_offsetof (struct msgstr_t, str170),
[MONO_CEE_UNUSED8] = __builtin_offsetof (struct msgstr_t, str171),
[MONO_CEE_UNUSED9] = __builtin_offsetof (struct msgstr_t, str172),
[MONO_CEE_UNUSED10] = __builtin_offsetof (struct msgstr_t, str173),
[MONO_CEE_UNUSED11] = __builtin_offsetof (struct msgstr_t, str174),
[MONO_CEE_UNUSED12] = __builtin_offsetof (struct msgstr_t, str175),
[MONO_CEE_UNUSED13] = __builtin_offsetof (struct msgstr_t, str176),
[MONO_CEE_UNUSED14] = __builtin_offsetof (struct msgstr_t, str177),
[MONO_CEE_UNUSED15] = __builtin_offsetof (struct msgstr_t, str178),
[MONO_CEE_UNUSED16] = __builtin_offsetof (struct msgstr_t, str179),
[MONO_CEE_UNUSED17] = __builtin_offsetof (struct msgstr_t, str180),
[MONO_CEE_CONV_OVF_I1] = __builtin_offsetof (struct msgstr_t, str181),
[MONO_CEE_CONV_OVF_U1] = __builtin_offsetof (struct msgstr_t, str182),
[MONO_CEE_CONV_OVF_I2] = __builtin_offsetof (struct msgstr_t, str183),
[MONO_CEE_CONV_OVF_U2] = __builtin_offsetof (struct msgstr_t, str184),
[MONO_CEE_CONV_OVF_I4] = __builtin_offsetof (struct msgstr_t, str185),
[MONO_CEE_CONV_OVF_U4] = __builtin_offsetof (struct msgstr_t, str186),
[MONO_CEE_CONV_OVF_I8] = __builtin_offsetof (struct msgstr_t, str187),
[MONO_CEE_CONV_OVF_U8] = __builtin_offsetof (struct msgstr_t, str188),
[MONO_CEE_UNUSED50] = __builtin_offsetof (struct msgstr_t, str189),
[MONO_CEE_UNUSED18] = __builtin_offsetof (struct msgstr_t, str190),
[MONO_CEE_UNUSED19] = __builtin_offsetof (struct msgstr_t, str191),
[MONO_CEE_UNUSED20] = __builtin_offsetof (struct msgstr_t, str192),
[MONO_CEE_UNUSED21] = __builtin_offsetof (struct msgstr_t, str193),
[MONO_CEE_UNUSED22] = __builtin_offsetof (struct msgstr_t, str194),
[MONO_CEE_UNUSED23] = __builtin_offsetof (struct msgstr_t, str195),
[MONO_CEE_REFANYVAL] = __builtin_offsetof (struct msgstr_t, str196),
[MONO_CEE_CKFINITE] = __builtin_offsetof (struct msgstr_t, str197),
[MONO_CEE_UNUSED24] = __builtin_offsetof (struct msgstr_t, str198),
[MONO_CEE_UNUSED25] = __builtin_offsetof (struct msgstr_t, str199),
[MONO_CEE_MKREFANY] = __builtin_offsetof (struct msgstr_t, str200),
[MONO_CEE_UNUSED59] = __builtin_offsetof (struct msgstr_t, str201),
[MONO_CEE_UNUSED60] = __builtin_offsetof (struct msgstr_t, str202),
[MONO_CEE_UNUSED61] = __builtin_offsetof (struct msgstr_t, str203),
[MONO_CEE_UNUSED62] = __builtin_offsetof (struct msgstr_t, str204),
[MONO_CEE_UNUSED63] = __builtin_offsetof (struct msgstr_t, str205),
[MONO_CEE_UNUSED64] = __builtin_offsetof (struct msgstr_t, str206),
[MONO_CEE_UNUSED65] = __builtin_offsetof (struct msgstr_t, str207),
[MONO_CEE_UNUSED66] = __builtin_offsetof (struct msgstr_t, str208),
[MONO_CEE_UNUSED67] = __builtin_offsetof (struct msgstr_t, str209),
[MONO_CEE_LDTOKEN] = __builtin_offsetof (struct msgstr_t, str210),
[MONO_CEE_CONV_U2] = __builtin_offsetof (struct msgstr_t, str211),
[MONO_CEE_CONV_U1] = __builtin_offsetof (struct msgstr_t, str212),
[MONO_CEE_CONV_I] = __builtin_offsetof (struct msgstr_t, str213),
[MONO_CEE_CONV_OVF_I] = __builtin_offsetof (struct msgstr_t, str214),
[MONO_CEE_CONV_OVF_U] = __builtin_offsetof (struct msgstr_t, str215),
[MONO_CEE_ADD_OVF] = __builtin_offsetof (struct msgstr_t, str216),
[MONO_CEE_ADD_OVF_UN] = __builtin_offsetof (struct msgstr_t, str217),
[MONO_CEE_MUL_OVF] = __builtin_offsetof (struct msgstr_t, str218),
[MONO_CEE_MUL_OVF_UN] = __builtin_offsetof (struct msgstr_t, str219),
[MONO_CEE_SUB_OVF] = __builtin_offsetof (struct msgstr_t, str220),
[MONO_CEE_SUB_OVF_UN] = __builtin_offsetof (struct msgstr_t, str221),
[MONO_CEE_ENDFINALLY] = __builtin_offsetof (struct msgstr_t, str222),
[MONO_CEE_LEAVE] = __builtin_offsetof (struct msgstr_t, str223),
[MONO_CEE_LEAVE_S] = __builtin_offsetof (struct msgstr_t, str224),
[MONO_CEE_STIND_I] = __builtin_offsetof (struct msgstr_t, str225),
[MONO_CEE_CONV_U] = __builtin_offsetof (struct msgstr_t, str226),
[MONO_CEE_UNUSED26] = __builtin_offsetof (struct msgstr_t, str227),
[MONO_CEE_UNUSED27] = __builtin_offsetof (struct msgstr_t, str228),
[MONO_CEE_UNUSED28] = __builtin_offsetof (struct msgstr_t, str229),
[MONO_CEE_UNUSED29] = __builtin_offsetof (struct msgstr_t, str230),
[MONO_CEE_UNUSED30] = __builtin_offsetof (struct msgstr_t, str231),
[MONO_CEE_UNUSED31] = __builtin_offsetof (struct msgstr_t, str232),
[MONO_CEE_UNUSED32] = __builtin_offsetof (struct msgstr_t, str233),
[MONO_CEE_UNUSED33] = __builtin_offsetof (struct msgstr_t, str234),
[MONO_CEE_UNUSED34] = __builtin_offsetof (struct msgstr_t, str235),
[MONO_CEE_UNUSED35] = __builtin_offsetof (struct msgstr_t, str236),
[MONO_CEE_UNUSED36] = __builtin_offsetof (struct msgstr_t, str237),
[MONO_CEE_UNUSED37] = __builtin_offsetof (struct msgstr_t, str238),
[MONO_CEE_UNUSED38] = __builtin_offsetof (struct msgstr_t, str239),
[MONO_CEE_UNUSED39] = __builtin_offsetof (struct msgstr_t, str240),
[MONO_CEE_UNUSED40] = __builtin_offsetof (struct msgstr_t, str241),
[MONO_CEE_UNUSED41] = __builtin_offsetof (struct msgstr_t, str242),
[MONO_CEE_UNUSED42] = __builtin_offsetof (struct msgstr_t, str243),
[MONO_CEE_UNUSED43] = __builtin_offsetof (struct msgstr_t, str244),
[MONO_CEE_UNUSED44] = __builtin_offsetof (struct msgstr_t, str245),
[MONO_CEE_UNUSED45] = __builtin_offsetof (struct msgstr_t, str246),
[MONO_CEE_UNUSED46] = __builtin_offsetof (struct msgstr_t, str247),
[MONO_CEE_UNUSED47] = __builtin_offsetof (struct msgstr_t, str248),
[MONO_CEE_UNUSED48] = __builtin_offsetof (struct msgstr_t, str249),
[MONO_CEE_PREFIX7] = __builtin_offsetof (struct msgstr_t, str250),
[MONO_CEE_PREFIX6] = __builtin_offsetof (struct msgstr_t, str251),
[MONO_CEE_PREFIX5] = __builtin_offsetof (struct msgstr_t, str252),
[MONO_CEE_PREFIX4] = __builtin_offsetof (struct msgstr_t, str253),
[MONO_CEE_PREFIX3] = __builtin_offsetof (struct msgstr_t, str254),
[MONO_CEE_PREFIX2] = __builtin_offsetof (struct msgstr_t, str255),
[MONO_CEE_PREFIX1] = __builtin_offsetof (struct msgstr_t, str256),
[MONO_CEE_PREFIXREF] = __builtin_offsetof (struct msgstr_t, str257),
[MONO_CEE_ARGLIST] = __builtin_offsetof (struct msgstr_t, str258),
[MONO_CEE_CEQ] = __builtin_offsetof (struct msgstr_t, str259),
[MONO_CEE_CGT] = __builtin_offsetof (struct msgstr_t, str260),
[MONO_CEE_CGT_UN] = __builtin_offsetof (struct msgstr_t, str261),
[MONO_CEE_CLT] = __builtin_offsetof (struct msgstr_t, str262),
[MONO_CEE_CLT_UN] = __builtin_offsetof (struct msgstr_t, str263),
[MONO_CEE_LDFTN] = __builtin_offsetof (struct msgstr_t, str264),
[MONO_CEE_LDVIRTFTN] = __builtin_offsetof (struct msgstr_t, str265),
[MONO_CEE_UNUSED56] = __builtin_offsetof (struct msgstr_t, str266),
[MONO_CEE_LDARG] = __builtin_offsetof (struct msgstr_t, str267),
[MONO_CEE_LDARGA] = __builtin_offsetof (struct msgstr_t, str268),
[MONO_CEE_STARG] = __builtin_offsetof (struct msgstr_t, str269),
[MONO_CEE_LDLOC] = __builtin_offsetof (struct msgstr_t, str270),
[MONO_CEE_LDLOCA] = __builtin_offsetof (struct msgstr_t, str271),
[MONO_CEE_STLOC] = __builtin_offsetof (struct msgstr_t, str272),
[MONO_CEE_LOCALLOC] = __builtin_offsetof (struct msgstr_t, str273),
[MONO_CEE_UNUSED57] = __builtin_offsetof (struct msgstr_t, str274),
[MONO_CEE_ENDFILTER] = __builtin_offsetof (struct msgstr_t, str275),
[MONO_CEE_UNALIGNED_] = __builtin_offsetof (struct msgstr_t, str276),
[MONO_CEE_VOLATILE_] = __builtin_offsetof (struct msgstr_t, str277),
[MONO_CEE_TAIL_] = __builtin_offsetof (struct msgstr_t, str278),
[MONO_CEE_INITOBJ] = __builtin_offsetof (struct msgstr_t, str279),
[MONO_CEE_CONSTRAINED_] = __builtin_offsetof (struct msgstr_t, str280),
[MONO_CEE_CPBLK] = __builtin_offsetof (struct msgstr_t, str281),
[MONO_CEE_INITBLK] = __builtin_offsetof (struct msgstr_t, str282),
[MONO_CEE_NO_] = __builtin_offsetof (struct msgstr_t, str283),
[MONO_CEE_RETHROW] = __builtin_offsetof (struct msgstr_t, str284),
[MONO_CEE_UNUSED] = __builtin_offsetof (struct msgstr_t, str285),
[MONO_CEE_SIZEOF] = __builtin_offsetof (struct msgstr_t, str286),
[MONO_CEE_REFANYTYPE] = __builtin_offsetof (struct msgstr_t, str287),
[MONO_CEE_READONLY_] = __builtin_offsetof (struct msgstr_t, str288),
[MONO_CEE_UNUSED53] = __builtin_offsetof (struct msgstr_t, str289),
[MONO_CEE_UNUSED54] = __builtin_offsetof (struct msgstr_t, str290),
[MONO_CEE_UNUSED55] = __builtin_offsetof (struct msgstr_t, str291),
[MONO_CEE_UNUSED70] = __builtin_offsetof (struct msgstr_t, str292),
[MONO_CEE_ILLEGAL] = __builtin_offsetof (struct msgstr_t, str293),
[MONO_CEE_ENDMAC] = __builtin_offsetof (struct msgstr_t, str294),
[MONO_CEE_MONO_ICALL] = __builtin_offsetof (struct msgstr_t, str295),
[MONO_CEE_MONO_OBJADDR] = __builtin_offsetof (struct msgstr_t, str296),
[MONO_CEE_MONO_LDPTR] = __builtin_offsetof (struct msgstr_t, str297),
[MONO_CEE_MONO_VTADDR] = __builtin_offsetof (struct msgstr_t, str298),
[MONO_CEE_MONO_NEWOBJ] = __builtin_offsetof (struct msgstr_t, str299),
[MONO_CEE_MONO_RETOBJ] = __builtin_offsetof (struct msgstr_t, str300),
[MONO_CEE_MONO_LDNATIVEOBJ] = __builtin_offsetof (struct msgstr_t, str301),
[MONO_CEE_MONO_CISINST] = __builtin_offsetof (struct msgstr_t, str302),
[MONO_CEE_MONO_CCASTCLASS] = __builtin_offsetof (struct msgstr_t, str303),
[MONO_CEE_MONO_SAVE_LMF] = __builtin_offsetof (struct msgstr_t, str304),
[MONO_CEE_MONO_RESTORE_LMF] = __builtin_offsetof (struct msgstr_t, str305),
[MONO_CEE_MONO_CLASSCONST] = __builtin_offsetof (struct msgstr_t, str306),
[MONO_CEE_MONO_NOT_TAKEN] = __builtin_offsetof (struct msgstr_t, str307),
[MONO_CEE_MONO_TLS] = __builtin_offsetof (struct msgstr_t, str308),
[MONO_CEE_MONO_ICALL_ADDR] = __builtin_offsetof (struct msgstr_t, str309),
[MONO_CEE_MONO_DYN_CALL] = __builtin_offsetof (struct msgstr_t, str310),
[MONO_CEE_MONO_MEMORY_BARRIER] = __builtin_offsetof (struct msgstr_t, str311),
[MONO_CEE_MONO_JIT_ATTACH] = __builtin_offsetof (struct msgstr_t, str312),
[MONO_CEE_MONO_JIT_DETACH] = __builtin_offsetof (struct msgstr_t, str313),
[MONO_CEE_MONO_JIT_ICALL_ADDR] = __builtin_offsetof (struct msgstr_t, str314),

#undef OPDEF
};

const char*
mono_opcode_name (int opcode)
{
	return (const char*)&opstr + opidx [opcode];
}

#else
#define OPDEF(a,b,c,d,e,f,g,h,i,j) b,
static const char* const
mono_opcode_names [MONO_CEE_LAST + 1] = {
#include "mono/cil/opcode.def"
	NULL
};

const char*
mono_opcode_name (int opcode)
{
	return mono_opcode_names [opcode];
}

#endif

MonoOpcodeEnum
mono_opcode_value (const mono_byte **ip, const mono_byte *end)
{
	MonoOpcodeEnum res;
	const mono_byte *p = *ip;

	if (p >= end)
		return -1;
	if (*p == 0xfe) {
		++p;
		if (p >= end)
			return -1;
		res = *p + MONO_PREFIX1_OFFSET;
	} else if (*p == MONO_CUSTOM_PREFIX) {
		++p;
		if (p >= end)
			return -1;
		res = *p + MONO_CUSTOM_PREFIX_OFFSET;
	} else {
		res = *p;
	}
	*ip = p;
	return res;
}

